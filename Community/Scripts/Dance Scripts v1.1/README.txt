***  Dance Scripts v1.1 README  ***

Thank you for downloading the modified Zanthyna scripts from the GSP Dancers (www.gspdancers.org/forums).  This README is a list of the dance scripts included in this pack, a short description of each, and installation instructions.


To install them, go to the "Dance Scripts" folder where you found this "README.txt" and select all the files there (Ctrl-A) and copy them (Ctrl-C).  The README instructions aren't necessary to copy, but won't hurt either.  You can exclude that if you want.
  
Now open your AO Scripts folder.  It's no longer kept in the AO installation and the actual location varies.  The easy way to get there is to just start the Anarchy Online launcher and click "Settings".  You can even do this while logged in to AO, since it doesn't require logging in to open the settings.  On the settings window, click the "GUI" tab and look for "Scripts", followed by the location AO is expecting the scripts to be in and an "Open" button.  Click "Open" to open that folder and paste (Ctrl-V) the files you just copied into it.

That's it.  You should now be able to use these scripts in AO.


These scripts were originally created by Zanthyna back in 2003, 2004.  The only thing that has been edited is the removal of vicinity chat in one of the make dance scripts.

*********************************************************************************


/1
/2
/3
/4
/5
/6
/7
/8

	These are short dances, used to fill small periods of downtime. They are designed for female characters, but work pretty well for male characters, albiet with occasional pauses between some moves.

******************************************


/clapdance

	A very simple female 'dance' where your character repeatedly claps once then twice. This script loops and requires another script or the /stop script to stop it.

*******************************************

/dance
/dance1

	These are female dances. /dance is techno style dance (which may not work well in high lag) and /dance1 is an older dance. There will be lots of pauses in them if you try to use them with a male or neuter character.

*******************************************

/uberlongdance
/ultralongdance
/verylongdance
/longdance
/mediumdance
/shortdance

	This is a series of repeated female dances.  /uberlongdance is the longest of the group and each script calls up the script below it, so if you use /longdance, it will then automatically go into /mediumdance and then /shortdance.  These don't work very well for male and neuter characters due to the pauses.

*******************************************

/maledancelong
/maledanceloop

	These are the two male dance scripts were written after it was discovered that AO has different delays for male, female, and neuter characters. Despite being male scripts, they work good for Atroxes and even work pretty well for females.  Note that the /maledanceloop repeats endlessly until you use another script, such as the /stop script.

*******************************************

/stop

	Just like it sounds, it will stop any of the scripts.

*******************************************


Changes v1.1: updated installation instructions, no change to scripts
