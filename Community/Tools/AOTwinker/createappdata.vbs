Const APPLICATION_DATA = &H1a&

Set objShell = CreateObject("Shell.Application")
Set objFolder = objShell.Namespace(APPLICATION_DATA)
Set objFolderItem = objFolder.Self
aotPath = objFolderItem.Path & "\AOTwinker"

Set objFSO = CreateObject("Scripting.FileSystemObject")

If Not objFSO.FolderExists(aotPath) Then
    objFSO.CreateFolder aotPath
End If