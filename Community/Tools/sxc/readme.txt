What is SXC:

	SXC is a simple calculator to show the time needed to advance in levels in
	Anarchy Online. It is usable both for the regular levels and for the
	Shadowlevels.


Installation:

	Just copy the file somewhere you want it and run the program


How to use SXC:

	- Select your current level
	- Select your target level
	- Select how many hours you will get XP/SK per day
	- Enter how much XP/SK you get every hour (average this yourself)

	The calculator will then write out the needed XP/SK to reach your target level,
	and how much time it will take you based on the info you entered.


Notes:

	Because the SK is much lower than the XP-gain, you cant just enter a currentlevel
	below 200 and a target above 200. The estimated time will be wrong. If you want
	to see how much time it is from 1-220, enter 1-200 first and then 200-220 after.
	You should try to keep the interval between the current level and targetlevel
	as short as possible. A long interval (like 1-200) makes the time calculations
	inaccurate since you can't really enter a good average XP/hour value.


Known bugs:

	None


Distribution site:

	http://home.no.net/octos/sxc.htm

Version list:

	v1.00	Feb  5th 2004	Initial release
	v1.01	Feb  9th 2004	Minior bugfix, removed requirement of having MS Office installed :)
	v1.01	Feb 10th 2004	Revision of v1.01 where msstdfmt.dll was still making annoyance
	v1.02	Feb 12th 2004	Finally fixed the msstdfmt.dll error, and fixed empty SK/XP per hour crash

Other yadda yadda:

	SXC is not an internet application. Should your firewall detect that it is trying
	to connect somewhere, the executable has been tampered with and you should
	download from the above site again. If you do not run a firewall at all you should
	really consider getting one. I can personally recommend ZoneLabs ZoneAlarm (Which
	is free). Get that at http://www.zonelabs.com. WinXP's Firewall is NOT good enough
	to prevent access from programs on your own computer and out to the Internet.

	Anarchy Online is a registered trademark of Funcom. SXC is Copyrighted to Octo.
	SXC is to be considered Freeware and can be freely distributed provided it is in
	its original form. Please distribute only the entire zip-file including this readme
	file. 