VERSION 5.00
Begin VB.Form frmMain 
   Caption         =   "Pattern Matcher"
   ClientHeight    =   2850
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6120
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   2850
   ScaleWidth      =   6120
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtAccumulatedPatternPiece 
      Height          =   285
      Left            =   120
      TabIndex        =   13
      Top             =   600
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.CommandButton cmdAbout 
      Caption         =   "&About"
      Height          =   495
      Left            =   2393
      TabIndex        =   11
      Top             =   1800
      Width           =   1335
   End
   Begin VB.TextBox txtNumberOfPatterns 
      Height          =   285
      Left            =   120
      TabIndex        =   10
      Text            =   "Text1"
      Top             =   120
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.TextBox txtResultCode 
      Height          =   285
      Left            =   120
      TabIndex        =   9
      Text            =   "Text1"
      Top             =   120
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.TextBox txtPatternLocked4 
      Height          =   345
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.TextBox txtPatternLocked3 
      Height          =   285
      Left            =   120
      TabIndex        =   7
      Top             =   120
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.TextBox txtPatternLocked2 
      Height          =   285
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.TextBox txtPatternLocked1 
      Height          =   285
      Left            =   120
      TabIndex        =   5
      Top             =   120
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.TextBox txtOutputWin4 
      Height          =   285
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.TextBox txtOutputWin3 
      Height          =   285
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.CommandButton cmdPasteAndSort 
      Caption         =   "&Sort"
      Height          =   495
      Left            =   413
      TabIndex        =   2
      Top             =   1800
      Width           =   1335
   End
   Begin VB.TextBox txtPatternCurrentlyAnalyzing 
      Height          =   285
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "E&xit"
      Height          =   495
      Left            =   4373
      TabIndex        =   0
      Top             =   1800
      Width           =   1335
   End
   Begin VB.Label labelCurrentStatus 
      Alignment       =   2  'Center
      Caption         =   "Ready"
      Height          =   375
      Left            =   1433
      TabIndex        =   12
      Top             =   600
      Width           =   3255
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim PatternMatrix(7000) As Long
Dim PatternMatrixName(200) As String
          


Option Explicit


Private Sub cmdAbout_Click()
  frmAbout.Show
End Sub
  
Private Sub DeleteTheTemporaryFiles()
  On Error Resume Next
  Kill ("c:\results.inv")
  Kill ("c:\working.tmp")

Errortrap:
  If Err.Number = 53 Then
'       Who cares... do nothing at all
  End If
End Sub


Public Sub cmdPasteAndSort_Click()

PatternMatrixName(1) = "'Adobe Suzerain'"
PatternMatrixName(2) = "'Aesma Daeva'"
PatternMatrixName(3) = "'Agent of Decay'"
PatternMatrixName(4) = "'Agent of Putrefaction'"
PatternMatrixName(5) = "'Ahpta'"
PatternMatrixName(6) = "'Alatyr'"
PatternMatrixName(7) = "'Anansi"
PatternMatrixName(8) = "'Anarir'"
PatternMatrixName(9) = "'Anya'"
PatternMatrixName(10) = "'Aray'"
PatternMatrixName(11) = "'Arch Bigot Aliel'"
PatternMatrixName(12) = "'Arch Bigot Biap'"
PatternMatrixName(13) = "'Arch Bigot Lohel'"
PatternMatrixName(14) = "'Arch Demon of Inferno'"
PatternMatrixName(15) = "'Argil Suzerain'"
PatternMatrixName(16) = "'Asase Ya'"
PatternMatrixName(17) = "'Ashmara Ravin'"
PatternMatrixName(18) = "'Ats'u"
PatternMatrixName(19) = "'Auger'"
PatternMatrixName(20) = "'Awl'"
PatternMatrixName(21) = "'Bagaspati'"
PatternMatrixName(22) = "'Beatific Spirit'"
PatternMatrixName(23) = "'Bellowing Chimera'"
PatternMatrixName(24) = "'Bhinaji Navi'"
PatternMatrixName(25) = "'Bia'"
PatternMatrixName(26) = "'Black Fang'"
PatternMatrixName(27) = "'Blight'"
PatternMatrixName(28) = "'Borer'"
PatternMatrixName(29) = "'Breaker Teuvo'"
PatternMatrixName(30) = "'Brutal Rafter'"
PatternMatrixName(31) = "'Brutal Soul Dredge'"
PatternMatrixName(32) = "'Cama'"
PatternMatrixName(33) = "'Canceroid Cupid'"
PatternMatrixName(34) = "'Captured Spirit'"
PatternMatrixName(35) = "'Careening Blight'"
PatternMatrixName(36) = "'Careening Death'"
PatternMatrixName(37) = "'Churn'"
PatternMatrixName(38) = "'Circumbendibum'"
PatternMatrixName(39) = "'Circumbendibus'"
PatternMatrixName(40) = "'Contorted Soul Dredge'"
PatternMatrixName(41) = "'Dalja'"
PatternMatrixName(42) = "'Defiler of Scheol'"
PatternMatrixName(43) = "'Destroyer of Scheol'"
PatternMatrixName(44) = "'Devastator of Scheol'"
PatternMatrixName(45) = "'Devourer of Scheol'"
PatternMatrixName(46) = "'Diviner Gil Kald-Thar'"
PatternMatrixName(47) = "'Eidolean Soul Dredge'"
PatternMatrixName(48) = "'Empath Min-Ji Liu'"
PatternMatrixName(49) = "'Exsequiae'"
PatternMatrixName(50) = "'Fester Leila'"
PatternMatrixName(51) = "'Flinty'"
PatternMatrixName(52) = "'Glitter'"
PatternMatrixName(53) = "'Gracious Soul Dredge'"
PatternMatrixName(54) = "'Gunk'"
PatternMatrixName(55) = "'Hadur'"
PatternMatrixName(56) = "'Haqa'"
PatternMatrixName(57) = "'Hemut'"
PatternMatrixName(58) = "'Ho'"
PatternMatrixName(59) = "'Ignis Fatui'"
PatternMatrixName(60) = "'Ignis Fatuus'"
PatternMatrixName(61) = "'Imk'a"
PatternMatrixName(62) = "'Infernal Demon'"
PatternMatrixName(63) = "'Iunmin'"
PatternMatrixName(64) = "'Juma'"
PatternMatrixName(65) = "'K'a"
PatternMatrixName(66) = "'Kaleva'"
PatternMatrixName(67) = "'Kaoline Suzerain'"
PatternMatrixName(68) = "'Khemhet'"
PatternMatrixName(69) = "'Lady Genevra Di�Venague'"
PatternMatrixName(70) = "'Lethargic Spirit'"
PatternMatrixName(71) = "'Loessial Suzerain'"
PatternMatrixName(72) = "'Loltonunon'"
PatternMatrixName(73) = "'Lurky'"
PatternMatrixName(74) = "'Lya'"
PatternMatrixName(75) = "'Malah-Animus'"
PatternMatrixName(76) = "'Malah-At'"
PatternMatrixName(77) = "'Malah-Auris'"
PatternMatrixName(78) = "'Maledicta'"
PatternMatrixName(79) = "'Marem'"
PatternMatrixName(80) = "'Marly Suzerain'"
PatternMatrixName(81) = "'Mawi'"
PatternMatrixName(82) = "'Misery'"
PatternMatrixName(83) = "'Moochy'"
PatternMatrixName(84) = "'Morrow'"
PatternMatrixName(85) = "'Nyame'"
PatternMatrixName(86) = "'Ocra'"
PatternMatrixName(87) = "'Odqan'"
PatternMatrixName(88) = "'Old Salty'"
PatternMatrixName(89) = "'Ooze'"
PatternMatrixName(90) = "'Pazuzu'"
PatternMatrixName(91) = "'Quake'"
PatternMatrixName(92) = "'Quondam'"
PatternMatrixName(93) = "'Rallies Fete'"
PatternMatrixName(94) = "'Razor the Battletoad'"
PatternMatrixName(95) = "'Redeemed Cama'"
PatternMatrixName(96) = "'Redeemed Gilthar'"
PatternMatrixName(97) = "'Redeemed Lord Galahad'"
PatternMatrixName(98) = "'Redeemed Ocra'"
PatternMatrixName(99) = "'Relief Teals'"
PatternMatrixName(100) = "'Roch'"
PatternMatrixName(101) = "'Sabretooth Slicer'"
PatternMatrixName(102) = "'Sampsa'"
PatternMatrixName(103) = "'Sasabonsam'"
PatternMatrixName(104) = "'Sashu'"
PatternMatrixName(105) = "'Satkamear'"
PatternMatrixName(106) = "'Sawi'"
PatternMatrixName(107) = "'Scratch'"
PatternMatrixName(108) = "'Screech'"
' 109 is down below
PatternMatrixName(110) = "'Shiver'"
PatternMatrixName(111) = "'Shullat'"
PatternMatrixName(112) = "'Silver Fang'"
PatternMatrixName(113) = "'Skulky'"
PatternMatrixName(114) = "'Slinky'"
PatternMatrixName(115) = "'Smee'"
PatternMatrixName(116) = "'Spiritless Soul Dredge'"
PatternMatrixName(117) = "'Srahir'"
PatternMatrixName(118) = "'Taille Frees'"
PatternMatrixName(119) = "'Tcheser'"
PatternMatrixName(120) = "'The Abysmal Lord'"
PatternMatrixName(121) = "'The Abyssal Widow'"
PatternMatrixName(122) = "'The Achbile Guardian'"
PatternMatrixName(123) = "'The Adonian Soul Dredge'"
PatternMatrixName(124) = "'The Adonis Spirit Master'"
PatternMatrixName(125) = "'The Archbile Queen'"
PatternMatrixName(126) = "'The Brobdingnagian Mother'"
PatternMatrixName(127) = "'The Dredge Driver'"
PatternMatrixName(128) = "'The Dryad Demigod'"
PatternMatrixName(129) = "'The Dryad Shuffle'"
PatternMatrixName(130) = "'The Dune Suzerain'"
PatternMatrixName(131) = "'The Elysian Soul Dredge'"
PatternMatrixName(132) = "'The Enrapt One'"
PatternMatrixName(133) = "'The Great Ice Golem'"
PatternMatrixName(134) = "'The Indomitable Chimera'"
PatternMatrixName(135) = "'The  Infernal Soul Dredge'"
PatternMatrixName(136) = "'The Infernal Soul Dredge'"
PatternMatrixName(137) = "'The Maggot Lord'"
PatternMatrixName(138) = "'The Mortificator'"
PatternMatrixName(139) = "'The Numb One'"
PatternMatrixName(140) = "'The Penumbral Spirit Hunter'"
PatternMatrixName(141) = "'The Peristaltic Abomination'"
PatternMatrixName(142) = "'The Proprietrix'"
PatternMatrixName(143) = "'The Scheolian Soul Dredge'"
PatternMatrixName(144) = "'The Stupendous Breeder'"
PatternMatrixName(145) = "'The Talus Suzerain'"
PatternMatrixName(146) = "'The Watchdog'"
PatternMatrixName(147) = "'The Worm King'"
PatternMatrixName(148) = "'Thunderous Chimera'"
PatternMatrixName(149) = "'Toss'"
PatternMatrixName(150) = "'Tough'"
PatternMatrixName(151) = "'Ungulera'"
PatternMatrixName(152) = "'Unredeemed Dalja'"
PatternMatrixName(153) = "'Unredeemed Lord Mordeth'"
PatternMatrixName(154) = "'Unredeemed Roch'"
PatternMatrixName(155) = "'Unredeemed Vanya'"
PatternMatrixName(156) = "'Upenpet'"
PatternMatrixName(157) = "'Ushqa'"
PatternMatrixName(158) = "'Vanya'"
PatternMatrixName(159) = "'Viscious Visitant'"
PatternMatrixName(160) = "'Wacky Suzerain'"
PatternMatrixName(161) = "'Wala'"
PatternMatrixName(162) = "'Waqa'"
PatternMatrixName(163) = "'Weary Empath Min-Ji Liu'"
PatternMatrixName(164) = "'White'"
PatternMatrixName(165) = "'Xark the Battletoad'"
PatternMatrixName(166) = "'Zoetic Oak'"
PatternMatrixName(167) = "'The Peristaltic Aversion'"
  
' oops... forgot two and need to divide Shake into 192 and 195
PatternMatrixName(109) = "'Shake' (QL 192)"
PatternMatrixName(168) = "'Shake' (QL 195)"

  
  Dim CurrentPattern As Integer
  Dim PatternPieces(10001) As String
  Dim filecounter As Integer
  Dim UsedPatternCount As Integer
  Dim tempstring As String
  Dim TotalSuccesses As Integer
'  Dim PatternNamed As String
  Dim i As Integer
  
  Dim j As Integer
  Dim k As Integer
  Dim h As String
  
  
  
' Set up the FSO for reading in the file
  Const ForReading = 1, ForWriting = 2, ForAppending = 8
  Dim fso, f
  Set fso = CreateObject("Scripting.FileSystemObject")
  Set f = fso.OpenTextFile("c:\results.inv", ForReading)

  filecounter = 0
  TotalSuccesses = 0
  UsedPatternCount = 0
  subDelay (0.0001)
labelCurrentStatus.Caption = "Reading pattern pieces"
  
  Do While Not f.atendofstream
    
    tempstring = f.readline
    If Left(tempstring, 1) = "1" Then
      filecounter = filecounter + 1
    End If
    If Left(tempstring, 1) = "2" Then
      filecounter = filecounter + 1
    End If
    If Left(tempstring, 1) = "3" Then
      filecounter = filecounter + 1
    End If
    If Left(tempstring, 1) = "4" Then
      filecounter = filecounter + 1
    End If
    If Left(tempstring, 1) = "5" Then
      filecounter = filecounter + 1
    End If
    If Left(tempstring, 1) = "6" Then
      filecounter = filecounter + 1
    End If
    If Left(tempstring, 1) = "7" Then
      filecounter = filecounter + 1
    End If
    If Left(tempstring, 1) = "8" Then
      filecounter = filecounter + 1
    End If
    If Left(tempstring, 1) = "9" Then
      filecounter = filecounter + 1
    End If
  
    If Mid(tempstring, 2, 1) = " " Then
      PatternPieces(filecounter) = Mid(tempstring, 5, 6)
    End If

    If Mid(tempstring, 3, 1) = " " Then
      PatternPieces(filecounter) = Mid(tempstring, 6, 6)
    End If
  
  Loop
  f.Close
  
  If filecounter = 0 Then
    Kill ("c:\results.inv")
    MsgBox ("Your selection has no recognized pattern pieces to work with!")
    Dim Frm As Form
    For Each Frm In Forms
      Unload Frm
      Set Frm = Nothing
    Next Frm
    End
  End If
  
'  PatternPieces(filecounter + 1) = -1 ' last one is -1 to signal the end of it all
  
  txtNumberOfPatterns.Text = filecounter
  CurrentPattern = 1


  Set fso = CreateObject("Scripting.FileSystemObject")
  Set f = fso.OpenTextFile("patterns.txt", ForWriting, True)
  f.write "This list of pattern pieces was generated by the Pattern Matcher program (www.halorn.com/pmatch.html)" & vbCrLf
  f.write vbCrLf
  f.write "File created at " & Time & " on " & Date & vbCrLf & vbCrLf
  
labelCurrentStatus.Caption = "Searching for pattern matches"
  Do While Not (CurrentPattern = filecounter)

' Reset all the locked pattern boxes

    txtPatternLocked1.Text = 0
    
    txtPatternLocked2.Text = 0
    txtPatternLocked3.Text = 0
    txtPatternLocked4.Text = 0
    subDelay (0.0001)
    
    Select Case PatternPieces(CurrentPattern)

      Case "231525" ' Adobe Suzerain
        PatternPieces(10001) = "Adobe Suzerain"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232406" ' Aesma Daeva
        PatternPieces(10001) = "Aesma Daeva"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234255" ' Aesma Daeva
        PatternPieces(10001) = "Aesma Daeva"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232307" ' Agent of Decay
        PatternPieces(10001) = "Agent of Decay"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234156" ' Agent of Decay
        PatternPieces(10001) = "Agent of Decay"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232280" ' Agent of Putrefaction
        PatternPieces(10001) = "Agent of Putrefaction"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234129" ' Agent of Putrefaction
        PatternPieces(10001) = "Agent of Putrefaction"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232316" ' Ahpta
        PatternPieces(10001) = "Ahpta"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234165" ' Ahpta
        PatternPieces(10001) = "Ahpta"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231966" ' Alatyr
        PatternPieces(10001) = "Alatyr"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
'      Case "232379" ' Anansi's Adherent
'        PatternPieces(10001) = "Anansi's Adherent"
'        txtPatternLocked1.Text = CurrentPattern
'        SearchInventory (PatternPieces)
'      Case "234228" ' Anansi's Adherent
'        PatternPieces(10001) = "Anansi's Adherent"
'        txtPatternLocked1.Text = CurrentPattern
'        SearchInventory (PatternPieces)
'      Case "232514" ' Anansi
'        PatternPieces(10001) = "Anansi"
'        txtPatternLocked1.Text = CurrentPattern
'        SearchInventory (PatternPieces)
'      Case "234367" ' Anansi
'        PatternPieces(10001) = "Anansi"
'        txtPatternLocked1.Text = CurrentPattern
'        SearchInventory (PatternPieces)
      Case "239610" ' Anarir
        PatternPieces(10001) = "Anarir"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231858" ' Anya
        PatternPieces(10001) = "Anya"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231984" ' Aray
        PatternPieces(10001) = "Aray"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232469" ' Arch Bigot Aliel
        PatternPieces(10001) = "Arch Bigot Aliel"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234322" ' Arch Bigot Aliel
        PatternPieces(10001) = "Arch Bigot Aliel"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232415" ' Arch Bigot Biap
        PatternPieces(10001) = "Arch Bigot Biap"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234264" ' Arch Bigot Biap
        PatternPieces(10001) = "Arch Bigot Biap"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232451" ' Arch Bigot Lohel
        PatternPieces(10001) = "Arch Bigot Lohel"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234304" ' Arch Bigot Lohel
        PatternPieces(10001) = "Arch Bigot Lohel"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231732" ' Argil Suzerain
        PatternPieces(10001) = "Argil Suzerain"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232460" ' Asase Ya
        PatternPieces(10001) = "Asase Ya"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234313" ' Asase Ya
        PatternPieces(10001) = "Asase Ya"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231597" ' Ashmara Ravin
        PatternPieces(10001) = "Ashmara Ravin"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232433" ' Ats'u
        PatternPieces(10001) = "Ats'u"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234284" ' Ats'u
        PatternPieces(10001) = "Ats'u"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231543" ' Auger
        PatternPieces(10001) = "Auger"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231552" ' Awl
        PatternPieces(10001) = "Awl"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231912" ' Bagaspati
        PatternPieces(10001) = "Bagaspati"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231570" ' Beatific Spirit
        PatternPieces(10001) = "Beatific Spirit"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231723" ' Bellowing Chimera
        PatternPieces(10001) = "Bellowing Chimera"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231660" ' Bhinaji Navi
        PatternPieces(10001) = "Bhinaji Navi"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234210" ' Bia
        PatternPieces(10001) = "Bia"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "229165" ' Black Fang
        PatternPieces(10001) = "Black Fang"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231417" ' Black Fang
        PatternPieces(10001) = "Black Fang"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231624" ' Blight
        PatternPieces(10001) = "Blight"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231561" ' Borer
        PatternPieces(10001) = "Borer"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231930" ' Breaker Teuvo
        PatternPieces(10001) = "Breaker Teuvo"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231903" ' Brutal Rafter
        PatternPieces(10001) = "Brutal Rafter"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "229201" ' Brutal Soul Dredge
        PatternPieces(10001) = "Brutal Soul Dredge"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231453" ' Brutal Soul Dredge
        PatternPieces(10001) = "Brutal Soul Dredge"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232595" ' Cama
        PatternPieces(10001) = "Cama"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239538" ' Canceroid Cupid
        PatternPieces(10001) = "Canceroid Cupid"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232478" ' Captured Spirit
        PatternPieces(10001) = "Captured Spirit"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234331" ' Captured Spirit
        PatternPieces(10001) = "Captured Spirit"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239556" ' Careening Blight
        PatternPieces(10001) = "Careening Blight"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239565" ' Careening Death
        PatternPieces(10001) = "Careening Death"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232128" ' Churn
        PatternPieces(10001) = "Churn"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "229192" ' Circumbendibum
        PatternPieces(10001) = "Circumbendibum"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231444" ' Circumbendibum
        PatternPieces(10001) = "Circumbendibum"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "229156" ' Circumbendibus
        PatternPieces(10001) = "Circumbendibus"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231408" ' Circumbendibus
        PatternPieces(10001) = "Circumbendibus"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231651" ' Contorted Soul Dredge
        PatternPieces(10001) = "Contorted Soul Dredge"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232568" ' Dalja
        PatternPieces(10001) = "Dalja"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231822" ' Defiler of Scheol
        PatternPieces(10001) = "Defiler of Scheol"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231840" ' Destroyer of Scheol
        PatternPieces(10001) = "Destroyer of Scheol"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231831" ' Devastator of Scheol
        PatternPieces(10001) = "Devastator of Scheol"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231813" ' Devourer of Scheol
        PatternPieces(10001) = "Devourer of Scheol"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239637" ' Eidolean Soul Dredge
        PatternPieces(10001) = "Eidolean Soul Dredge"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232532" ' Empath Min-Ji Liu
        PatternPieces(10001) = "Empath Min-Ji Liu"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239683" ' Exsequiae
        PatternPieces(10001) = "Exsequiae"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232298" ' Fester Leila
        PatternPieces(10001) = "Fester Leila"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234147" ' Fester Leila
        PatternPieces(10001) = "Fester Leila"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231687" ' Flinty
        PatternPieces(10001) = "Flinty"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239646" ' Glitter
        PatternPieces(10001) = "Glitter"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231498" ' Gracious Soul Dredge
        PatternPieces(10001) = "Gracious Soul Dredge"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231705" ' Gunk
        PatternPieces(10001) = "Gunk"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231993" ' Hadur
        PatternPieces(10001) = "Hadur"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232334" ' Haqa
        PatternPieces(10001) = "Haqa"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234183" ' Haqa
        PatternPieces(10001) = "Haqa"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231633" ' Hemut
        PatternPieces(10001) = "Hemut"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239673" ' Ho
        PatternPieces(10001) = "Ho"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231516" ' Ignis Fatui
        PatternPieces(10001) = "Ignis Fatui"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231579" ' Ignis Fatuus
        PatternPieces(10001) = "Ignis Fatuus"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232002" ' Imk
        PatternPieces(10001) = "Imk'a"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232577" ' Infernal Demon
        PatternPieces(10001) = "Infernal Demon"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231534" ' Iunmin
        PatternPieces(10001) = "Iunmin"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232011" ' Juma
        PatternPieces(10001) = "Juma"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232227" '
        PatternPieces(10001) = "K'a"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234102" '
        PatternPieces(10001) = "K'a"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232020" ' Kaleva
        PatternPieces(10001) = "Kaleva"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231768" ' Kaoline Suzerain
        PatternPieces(10001) = "Kaoline Suzerain"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231642" ' Khemhet
        PatternPieces(10001) = "Khemhet"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231606" ' Lethargic Spirit
        PatternPieces(10001) = "Lethargic Spirit"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231741" ' Loessial Suzerain
        PatternPieces(10001) = "Loessial Suzerain"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239664" ' Loltonunon
        PatternPieces(10001) = "Loltonunon"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232074" ' Lurky
        PatternPieces(10001) = "Lurky"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232370" ' Lya
        PatternPieces(10001) = "Lya"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234219" ' Lya
        PatternPieces(10001) = "Lya"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239529" ' Malah-Animus
        PatternPieces(10001) = "Malah-Animus"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239520" ' Malah-At
        PatternPieces(10001) = "Malah-At"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239511" ' Malah-Auris
        PatternPieces(10001) = "Malah-Auris"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239574" ' Maledicta
        PatternPieces(10001) = "Maledicta"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231588" ' Marem
        PatternPieces(10001) = "Marem"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231669" ' Marly Suzerain
        PatternPieces(10001) = "Marly Suzerain"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239592" ' Mawi
        PatternPieces(10001) = "Mawi"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231615" ' Misery
        PatternPieces(10001) = "Misery"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232092" ' Moochy
        PatternPieces(10001) = "Moochy"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231885" ' Morrow
        PatternPieces(10001) = "Morrow"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239484" ' Nyame
        PatternPieces(10001) = "Nyame"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232523" ' Odqan
        PatternPieces(10001) = "Odqan"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234376" ' Odqan
        PatternPieces(10001) = "Odqan"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232029" ' Old Salty
        PatternPieces(10001) = "Old Salty"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231750" ' Ooze
        PatternPieces(10001) = "Ooze"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232397" ' Pazuzu
        PatternPieces(10001) = "Pazuzu"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234246" ' Pazuzu
        PatternPieces(10001) = "Pazuzu"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232119" ' Quake
        PatternPieces(10001) = "Quake"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231876" ' Quondam
        PatternPieces(10001) = "Quondam"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232442" ' Rallies Fete
        PatternPieces(10001) = "Rallies Fete"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234295" ' Rallies Fete
        PatternPieces(10001) = "Rallies Fete"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232496" ' Razor the Battletoad
        PatternPieces(10001) = "Razor the Battletoad"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234349" ' Razor the Battletoad
        PatternPieces(10001) = "Razor the Battletoad"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "242540" ' Redeemed Cama
        PatternPieces(10001) = "Redeemed Cama"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232559" ' Redeemed Gilthar
        PatternPieces(10001) = "Redeemed Gilthar"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "242513" ' Redeemed Gilthar
        PatternPieces(10001) = "Redeemed Gilthar"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234434" ' Redeemed Lord Galahad
        PatternPieces(10001) = "Redeemed Lord Galahad"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232271" ' Relief Teals
        PatternPieces(10001) = "Relief Teals"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234120" ' Relief Teals
        PatternPieces(10001) = "Relief Teals"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232550" ' Roch
        PatternPieces(10001) = "Roch"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239547" ' Sabretooth Slicer
        PatternPieces(10001) = "Sabretooth Slicer"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231849" ' Sampsa
        PatternPieces(10001) = "Sampsa"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232352" ' Sasabonsam
        PatternPieces(10001) = "Sasabonsam"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234201" ' Sasabonsam
        PatternPieces(10001) = "Sasabonsam"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232218" ' Sashu
        PatternPieces(10001) = "Sashu"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234093" ' Sashu
        PatternPieces(10001) = "Sashu"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239655" ' Satkamear
        PatternPieces(10001) = "Satkamear"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239601" ' Sawi
        PatternPieces(10001) = "Sawi"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "229174" ' Scratch
        PatternPieces(10001) = "Scratch"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231426" ' Scratch
        PatternPieces(10001) = "Scratch"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "229183" ' Screech
        PatternPieces(10001) = "Screech"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231435" ' Screech
        PatternPieces(10001) = "Screech"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232209" ' Shake
        PatternPieces(10001) = "Shake (QL 192)"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232101" ' Shake
        PatternPieces(10001) = "Shake (QL 195)"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232200" ' Shiver
        PatternPieces(10001) = "Shiver"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232505" ' Shullat
        PatternPieces(10001) = "Shullat"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234358" ' Shullat
        PatternPieces(10001) = "Shullat"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "229147" ' Silver Fang
        PatternPieces(10001) = "Silver Fang"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231399" ' Silver Fang
        PatternPieces(10001) = "Silver Fang"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232083" ' Skulky
        PatternPieces(10001) = "Skulky"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232065" ' Slinky
        PatternPieces(10001) = "Slinky"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232038" ' Smee
        PatternPieces(10001) = "Smee"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231507" ' Spiritless Soul Dredge
        PatternPieces(10001) = "Spiritless Soul Dredge"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231948" ' Srahir
        PatternPieces(10001) = "Srahir"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232343" ' Taille Frees
        PatternPieces(10001) = "Taille Frees"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234192" ' Taille Frees
        PatternPieces(10001) = "Taille Frees"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239701" ' Tcheser
        PatternPieces(10001) = "Tcheser"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239493" ' The Abysmal Lord
        PatternPieces(10001) = "The Abysmal Lord"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232191" ' The Abyssal Widow
        PatternPieces(10001) = "The Abyssal Widow"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239692" ' The Achbile Guardian
        PatternPieces(10001) = "The Achbile Guardian"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232146" ' The Adonian Soul Dredge
        PatternPieces(10001) = "The Adonian Soul Dredge"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232173" ' The Adonis Spirit Master
        PatternPieces(10001) = "The Adonis Spirit Master"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "229534" ' The Archbile Queen
        PatternPieces(10001) = "The Archbile Queen"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231777" ' The Archbile Queen
        PatternPieces(10001) = "The Archbile Queen"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239619" ' The Brobdingnagian Mother
        PatternPieces(10001) = "The Brobdingnagian Mother"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232164" ' The Dredge Driver
        PatternPieces(10001) = "The Dredge Driver"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232388" ' The Dryad Demigod
        PatternPieces(10001) = "The Dryad Demigod"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234237" ' The Dryad Demigod
        PatternPieces(10001) = "The Dryad Demigod"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231939" ' The Dryad Shuffle
        PatternPieces(10001) = "The Dryad Shuffle"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "229210" ' The Dune Suzerain
        PatternPieces(10001) = "The Dune Suzerain"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231462" ' The Dune Suzerain
        PatternPieces(10001) = "The Dune Suzerain"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "229237" ' The Elysian Soul Dredge
        PatternPieces(10001) = "The Elysian Soul Dredge"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231489" ' The Elysian Soul Dredge
        PatternPieces(10001) = "The Elysian Soul Dredge"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "229561" ' The Enrapt One
        PatternPieces(10001) = "The Enrapt One"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231804" ' The Enrapt One
        PatternPieces(10001) = "The Enrapt One"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232424" ' The Indomitable Chimera
        PatternPieces(10001) = "The Indomitable Chimera"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234275" ' The Indomitable Chimera
        PatternPieces(10001) = "The Indomitable Chimera"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239502" ' The  Infernal Soul Dredge
        PatternPieces(10001) = "The  Infernal Soul Dredge"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "242453" ' The Infernal Soul Dredge
        PatternPieces(10001) = "The Infernal Soul Dredge"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231867" ' The Maggot Lord
        PatternPieces(10001) = "The Maggot Lord"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232289" ' The Mortificator
        PatternPieces(10001) = "The Mortificator"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234138" ' The Mortificator
        PatternPieces(10001) = "The Mortificator"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "229552" ' The Numb One
        PatternPieces(10001) = "The Numb One"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231795" ' The Numb One
        PatternPieces(10001) = "The Numb One"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231894" ' The Penumbral Spirit Hunter
        PatternPieces(10001) = "The Penumbral Spirit Hunter"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232056" ' The Peristaltic Abomination
        PatternPieces(10001) = "The Peristaltic Abomination"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232047" ' The Peristaltic Aversion
        PatternPieces(10001) = "The Peristaltic Aversion"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232182" ' The Proprietrix
        PatternPieces(10001) = "The Proprietrix"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231759" ' The Scheolian Soul Dredge
        PatternPieces(10001) = "The Scheolian Soul Dredge"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239628" ' The Stupendous Breeder
        PatternPieces(10001) = "The Stupendous Breeder"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "229219" ' The Talus Suzerain
        PatternPieces(10001) = "The Talus Suzerain"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231471" ' The Talus Suzerain
        PatternPieces(10001) = "The Talus Suzerain"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232155" ' The Watchdog
        PatternPieces(10001) = "The Watchdog"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231975" ' The Worm King
        PatternPieces(10001) = "The Worm King"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231714" ' Thunderous Chimera
        PatternPieces(10001) = "Thunderous Chimera"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232110" ' Toss
        PatternPieces(10001) = "Toss"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231678" ' Tough
        PatternPieces(10001) = "Tough"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "229228" ' Ungulera
        PatternPieces(10001) = "Ungulera"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231480" ' Ungulera
        PatternPieces(10001) = "Ungulera"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "242522" ' Unredeemed Dalja
        PatternPieces(10001) = "Unredeemed Dalja"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234446" ' Unredeemed Lord Mordeth
        PatternPieces(10001) = "Unredeemed Lord Mordeth"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "242504" ' Unredeemed Roch
        PatternPieces(10001) = "Unredeemed Roch"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "242549" ' Unredeemed Vanya
        PatternPieces(10001) = "Unredeemed Vanya"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239710" ' Upenpet
        PatternPieces(10001) = "Upenpet"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234174" ' Ushqa
        PatternPieces(10001) = "Ushqa"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232604" ' Vanya
        PatternPieces(10001) = "Vanya"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232137" ' Viscious Visitant
        PatternPieces(10001) = "Viscious Visitant"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231696" ' Wacky Suzerain
        PatternPieces(10001) = "Wacky Suzerain"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "239583" ' Wala
        PatternPieces(10001) = "Wala"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234111" ' Waqa
        PatternPieces(10001) = "Waqa"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "242655" ' Weary Empath Min-Ji Liu
        PatternPieces(10001) = "Weary Empath Min-Ji Liu"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "229543" ' White
        PatternPieces(10001) = "White"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231786" ' White
        PatternPieces(10001) = "White"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "232487" ' Xark the Battletoad
        PatternPieces(10001) = "Xark the Battletoad"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "234340" ' Xark the Battletoad
        PatternPieces(10001) = "Xark the Battletoad"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
      Case "231957" ' Zoetic Oak
        PatternPieces(10001) = "Zoetic Oak"
        txtPatternLocked1.Text = CurrentPattern
        SearchInventory (PatternPieces)
    End Select
        
  
'  Success Codes -
'    0 = Not successful, could not build anything
'    1 = ABC + D
'    2 = Not successful, could not finish D-C
'    3 = AB + C + D
'    4 = A + B + C + D
'    5 = Not successful, could not finish D-C-B
  
  If txtResultCode.Text = "1" Then
    f.write "Match found -> '" & PatternPieces(10001) & "' ABC + D" & vbCrLf
    UsedPatternCount = UsedPatternCount + 2
    TotalSuccesses = TotalSuccesses + 1
    PatternPieces(txtPatternLocked1.Text) = 0
    PatternPieces(txtPatternLocked2.Text) = 0
  End If
  If txtResultCode.Text = "4" Then
    f.write "Match found -> '" & PatternPieces(10001) & "' A + B + C + D" & vbCrLf
    UsedPatternCount = UsedPatternCount + 4
    TotalSuccesses = TotalSuccesses + 1
    PatternPieces(txtPatternLocked1.Text) = 0
    PatternPieces(txtPatternLocked2.Text) = 0
    PatternPieces(txtPatternLocked3.Text) = 0
    PatternPieces(txtPatternLocked4.Text) = 0
  End If
  If txtResultCode.Text = "3" Then
    f.write "Match found -> '" & PatternPieces(10001) & "' AB + C + D" & vbCrLf
    UsedPatternCount = UsedPatternCount + 3
    TotalSuccesses = TotalSuccesses + 1
    PatternPieces(txtPatternLocked1.Text) = 0
    PatternPieces(txtPatternLocked2.Text) = 0
    PatternPieces(txtPatternLocked3.Text) = 0
  End If
  
''''''' Odd Text Here To Make It Easier To Jump To This Area '''''''
    
  txtResultCode.Text = 0
  CurrentPattern = CurrentPattern + 1

  Loop
  
  If TotalSuccesses = 0 Then f.write "****** No matches found ******" & vbCrLf
  
  f.write vbCrLf & "****** The following pieces were not used ******" & vbCrLf
  i = 1
  
  Dim MatrixCounter As Long
  
  Do While Not (i = (filecounter + 1))
    If Len(PatternPieces(i)) <> 0 Then
      PatternPieces(i) = PatternNamed(PatternPieces(i))
      MatrixCounter = Val(txtAccumulatedPatternPiece.Text)
      PatternMatrix(MatrixCounter) = PatternMatrix(MatrixCounter) + 1
   End If
    i = i + 1
    subDelay (0.0001)
  Loop
      
      
      
  For k = 1 To (filecounter)
'    For j = 1 To (filecounter - k - 1)
    For j = 1 To (filecounter - k)
      If PatternPieces(j + 1) < PatternPieces(j) Then
        h = PatternPieces(j) 'swap if the two items
        PatternPieces(j) = PatternPieces(j + 1) 'are out of order
        PatternPieces(j + 1) = h
      
        h = PatternMatrix(j) 'swap if the two items
        PatternMatrix(j) = PatternMatrix(j + 1) 'are out of order
        PatternMatrix(j + 1) = h
        subDelay (0.0001)
      End If
    Next j
  Next k
      
      
  i = 1
  Do While Not (i = (filecounter + 1))
    If (Len(PatternPieces(i)) <> 0) And ((PatternPieces(i) <> "0")) Then f.write PatternPieces(i) & vbCrLf
    i = i + 1
  Loop
    
  
  f.write vbCrLf & "*** Summary ***" & vbCrLf
  f.write "Items checked : " & (filecounter) & vbCrLf
  f.write "Patterns used in matches : " & UsedPatternCount & vbCrLf
  f.write "Unused items : " & (filecounter - UsedPatternCount) & vbCrLf & vbCrLf
  f.write "Total matches : " & (TotalSuccesses) & vbCrLf

  f.write vbCrLf
  subDelay (0.0001)

  labelCurrentStatus.Caption = "Writing data to file"
  
  Dim AccumulatorCounter As Long
  Dim CounterWithinPattern As Long
  Dim WriteThisPieceOut As Boolean
  Dim WhiteSpaceCounter As Long
  If (filecounter - UsedPatternCount) > 0 Then ' Has even one been unused?
    f.write "Unused Pattern Piece Graph" & vbCrLf & vbCrLf
    f.write "Name of Pattern                    ABC AB   A   B   C   D" & vbCrLf
    AccumulatorCounter = 0
    Do While Not (AccumulatorCounter = 169) ' total of 168 patterns + 1
      WriteThisPieceOut = False
      AccumulatorCounter = AccumulatorCounter + 1
      CounterWithinPattern = 1000
      If PatternMatrix(CounterWithinPattern + AccumulatorCounter) <> 0 Then WriteThisPieceOut = True
      CounterWithinPattern = 2000
      If PatternMatrix(CounterWithinPattern + AccumulatorCounter) <> 0 Then WriteThisPieceOut = True
      CounterWithinPattern = 3000
      If PatternMatrix(CounterWithinPattern + AccumulatorCounter) <> 0 Then WriteThisPieceOut = True
      CounterWithinPattern = 4000
      If PatternMatrix(CounterWithinPattern + AccumulatorCounter) <> 0 Then WriteThisPieceOut = True
      CounterWithinPattern = 5000
      If PatternMatrix(CounterWithinPattern + AccumulatorCounter) <> 0 Then WriteThisPieceOut = True
      CounterWithinPattern = 6000
      If PatternMatrix(CounterWithinPattern + AccumulatorCounter) <> 0 Then WriteThisPieceOut = True
      If WriteThisPieceOut = True Then
        f.write PatternMatrixName(AccumulatorCounter)
        WhiteSpaceCounter = Len(PatternMatrixName(AccumulatorCounter))
        Do While Not WhiteSpaceCounter = 35
          f.write " "
          WhiteSpaceCounter = WhiteSpaceCounter + 1
        Loop
        If PatternMatrix(AccumulatorCounter + 1000) < 10 Then f.write " "
        f.write PatternMatrix(AccumulatorCounter + 1000) & "  "
        If PatternMatrix(AccumulatorCounter + 2000) < 10 Then f.write " "
        f.write PatternMatrix(AccumulatorCounter + 2000) & "  "
        If PatternMatrix(AccumulatorCounter + 3000) < 10 Then f.write " "
        f.write PatternMatrix(AccumulatorCounter + 3000) & "  "
        If PatternMatrix(AccumulatorCounter + 4000) < 10 Then f.write " "
        f.write PatternMatrix(AccumulatorCounter + 4000) & "  "
        If PatternMatrix(AccumulatorCounter + 5000) < 10 Then f.write " "
        f.write PatternMatrix(AccumulatorCounter + 5000) & "  "
        If PatternMatrix(AccumulatorCounter + 6000) < 10 Then f.write " "
        f.write PatternMatrix(AccumulatorCounter + 6000) & vbCrLf
      End If
    Loop

  End If

  f.Close
  h = "Sorting complete :"
  If TotalSuccesses = 1 Then
    h = h + " 1 match found."
  End If
  If TotalSuccesses <> 1 Then
    h = h + " " & TotalSuccesses & " matches found."
  End If
h = h + "  Results have been written to the file " & """" & "patterns.txt" & """" & " in your Pattern Matcher directory."
MsgBox (h)

DeleteTheTemporaryFiles
labelCurrentStatus.Caption = "Ready"
End Sub



Private Sub cmdExit_Click()
  Dim Frm As Form
  For Each Frm In Forms
    Unload Frm
    Set Frm = Nothing
    Next Frm
  End
End Sub

Public Function SearchInventory(PatternPieces)
    
Dim SearchForABC As Integer
Dim SearchForAB As Integer
Dim SearchForA As Integer
Dim SearchForB As Integer
Dim SearchForC As Integer


Dim LookingForThisABC As Double
Dim LookingForThisAB As Double
Dim LookingForThisA As Double
Dim LookingForThisB As Double
Dim LookingForThisC As Double

Dim AltLookingForThisABC As Double
Dim AltLookingForThisAB As Double
Dim AltLookingForThisA As Double
Dim AltLookingForThisB As Double
Dim AltLookingForThisC As Double

Dim Success As Integer

    
  SearchForABC = 1
  SearchForAB = 1
  SearchForA = 1
  SearchForB = 1
  SearchForC = 1
  Success = 0 ' 0 means no success, the default
  
  Select Case (PatternPieces(10001))
'    Case ""
'      LookingForThisABC = ""
'      AltLookingForThisABC = ""
'      LookingForThisAB = ""
'      AltLookingForThisAB = ""
'      LookingForThisC = ""
'      AltLookingForThisC = ""
'      LookingForThisB = ""
'      AltLookingForThisB = ""
'      LookingForThisA = ""
'      AltLookingForThisA = ""
    Case "Unredeemed Dalja"
      LookingForThisABC = "242524"
      AltLookingForThisABC = "242524"
      LookingForThisAB = "242523"
      AltLookingForThisAB = "242523"
      LookingForThisC = "242521"
      AltLookingForThisC = "242521"
      LookingForThisB = "242520"
      AltLookingForThisB = "242520"
      LookingForThisA = "242664"
      AltLookingForThisA = "242664"
    Case "Unredeemed Lord Mordeth"
      LookingForThisABC = "232624"
      AltLookingForThisABC = "234448"
      LookingForThisAB = "232623"
      AltLookingForThisAB = "234447"
      LookingForThisC = "234445"
      AltLookingForThisC = "234445"
      LookingForThisB = "234444"
      AltLookingForThisB = "234444"
      LookingForThisA = "242670"
      AltLookingForThisA = "242670"
    Case "Unredeemed Roch"
      LookingForThisABC = "242506"
      AltLookingForThisABC = "242506"
      LookingForThisAB = "242505"
      AltLookingForThisAB = "242505"
      LookingForThisC = "242503"
      AltLookingForThisC = "242503"
      LookingForThisB = "242502"
      AltLookingForThisB = "242502"
      LookingForThisA = "242662"
      AltLookingForThisA = "242662"
    Case "Unredeemed Vanya"
      LookingForThisABC = "242551"
      AltLookingForThisABC = "242551"
      LookingForThisAB = "242550"
      AltLookingForThisAB = "242550"
      LookingForThisC = "242548"
      AltLookingForThisC = "242548"
      LookingForThisB = "242547"
      AltLookingForThisB = "242547"
      LookingForThisA = "242668"
      AltLookingForThisA = "242668"
    Case "Upenpet"
      LookingForThisABC = "239712"
      AltLookingForThisABC = "239712"
      LookingForThisAB = "239711"
      AltLookingForThisAB = "239711"
      LookingForThisC = "239709"
      AltLookingForThisC = "239709"
      LookingForThisB = "239708"
      AltLookingForThisB = "239708"
      LookingForThisA = "242820"
      AltLookingForThisA = "242820"
    Case "Ushqa"
      LookingForThisABC = "232327"
      AltLookingForThisABC = "234176"
      LookingForThisAB = "232326"
      AltLookingForThisAB = "234175"
      LookingForThisC = "234173"
      AltLookingForThisC = "234173"
      LookingForThisB = "234172"
      AltLookingForThisB = "234172"
      LookingForThisA = "242772"
      AltLookingForThisA = "242772"
    Case "Viscious Visitant"
      LookingForThisABC = "232139"
      AltLookingForThisABC = "232139"
      LookingForThisAB = "232138"
      AltLookingForThisAB = "232138"
      LookingForThisC = "232136"
      AltLookingForThisC = "232136"
      LookingForThisB = "232135"
      AltLookingForThisB = "232135"
      LookingForThisA = "242754"
      AltLookingForThisA = "242754"
    Case "Wacky Suzerain"
      LookingForThisABC = "229450"
      AltLookingForThisABC = "231698"
      LookingForThisAB = "229449"
      AltLookingForThisAB = "231697"
      LookingForThisC = "231695"
      AltLookingForThisC = "231695"
      LookingForThisB = "231694"
      AltLookingForThisB = "231694"
      LookingForThisA = "242706"
      AltLookingForThisA = "242706"
    Case "Wala"
      LookingForThisABC = "239585"
      AltLookingForThisABC = "239585"
      LookingForThisAB = "239584"
      AltLookingForThisAB = "239584"
      LookingForThisC = "239582"
      AltLookingForThisC = "239582"
      LookingForThisB = "239581"
      AltLookingForThisB = "239581"
      LookingForThisA = "242806"
      AltLookingForThisA = "242806"
    Case "Waqa"
      LookingForThisABC = "234113"
      AltLookingForThisABC = "234113"
      LookingForThisAB = "234112"
      AltLookingForThisAB = "234112"
      LookingForThisC = "234110"
      AltLookingForThisC = "234110"
      LookingForThisB = "234109"
      AltLookingForThisB = "234109"
      LookingForThisA = "242765"
      AltLookingForThisA = "242765"
    Case "Weary Empath Min-Ji Liu"
      LookingForThisABC = "242657"
      AltLookingForThisABC = "242657"
      LookingForThisAB = "242656"
      AltLookingForThisAB = "242656"
      LookingForThisC = "242654"
      AltLookingForThisC = "242654"
      LookingForThisB = "242653"
      AltLookingForThisB = "242653"
      LookingForThisA = "242652"
      AltLookingForThisA = "242652"
    Case "White"
      LookingForThisABC = "229545"
      AltLookingForThisABC = "231788"
      LookingForThisAB = "229544"
      AltLookingForThisAB = "231787"
      LookingForThisC = "229542"
      AltLookingForThisC = "231785"
      LookingForThisB = "229541"
      AltLookingForThisB = "231784"
      LookingForThisA = "242716"
      AltLookingForThisA = "242716"
    Case "Xark the Battletoad"
      LookingForThisABC = "232489"
      AltLookingForThisABC = "234342"
      LookingForThisAB = "232488"
      AltLookingForThisAB = "234341"
      LookingForThisC = "232486"
      AltLookingForThisC = "234339"
      LookingForThisB = "232485"
      AltLookingForThisB = "234338"
      LookingForThisA = "242790"
      AltLookingForThisA = "242790"
    Case "The Penumbral Spirit Hunter"
      LookingForThisABC = "231896"
      AltLookingForThisABC = "231896"
      LookingForThisAB = "231895"
      AltLookingForThisAB = "231895"
      LookingForThisC = "231893"
      AltLookingForThisC = "231893"
      LookingForThisB = "231892"
      AltLookingForThisB = "231892"
      LookingForThisA = "242728"
      AltLookingForThisA = "242728"
    Case "The Peristaltic Abomination"
      LookingForThisABC = "232058"
      AltLookingForThisABC = "232058"
      LookingForThisAB = "232057"
      AltLookingForThisAB = "232057"
      LookingForThisC = "232055"
      AltLookingForThisC = "232055"
      LookingForThisB = "232054"
      AltLookingForThisB = "232054"
      LookingForThisA = "242745"
      AltLookingForThisA = "242745"
    Case "The Peristaltic Aversion"
      LookingForThisABC = "232049"
      AltLookingForThisABC = "232049"
      LookingForThisAB = "232048"
      AltLookingForThisAB = "232048"
      LookingForThisC = "232046"
      AltLookingForThisC = "232046"
      LookingForThisB = "232045"
      AltLookingForThisB = "232045"
      LookingForThisA = "242744"
      AltLookingForThisA = "242744"
    Case "The Proprietrix"
      LookingForThisABC = "232184"
      AltLookingForThisABC = "232184"
      LookingForThisAB = "232183"
      AltLookingForThisAB = "232183"
      LookingForThisC = "232181"
      AltLookingForThisC = "232181"
      LookingForThisB = "232180"
      AltLookingForThisB = "232180"
      LookingForThisA = "242759"
      AltLookingForThisA = "242759"
    Case "The Scheolian Soul Dredge"
      LookingForThisABC = "229518"
      AltLookingForThisABC = "231761"
      LookingForThisAB = "229517"
      AltLookingForThisAB = "231760"
      LookingForThisC = "231758"
      AltLookingForThisC = "231758"
      LookingForThisB = "231757"
      AltLookingForThisB = "231757"
      LookingForThisA = "242713"
      AltLookingForThisA = "242713"
    Case "The Stupendous Breeder"
      LookingForThisABC = "239630"
      AltLookingForThisABC = "239630"
      LookingForThisAB = "239629"
      AltLookingForThisAB = "239629"
      LookingForThisC = "239627"
      AltLookingForThisC = "239627"
      LookingForThisB = "239626"
      AltLookingForThisB = "239626"
      LookingForThisA = "242811"
      AltLookingForThisA = "242811"
    Case "The Talus Suzerain"
      LookingForThisABC = "229221"
      AltLookingForThisABC = "231473"
      LookingForThisAB = "229220"
      AltLookingForThisAB = "231472"
      LookingForThisC = "229218"
      AltLookingForThisC = "231470"
      LookingForThisB = "229217"
      AltLookingForThisB = "231469"
      LookingForThisA = "242681"
      AltLookingForThisA = "242681"
    Case "The Watchdog"
      LookingForThisABC = "232157"
      AltLookingForThisABC = "232157"
      LookingForThisAB = "232156"
      AltLookingForThisAB = "232156"
      LookingForThisC = "232154"
      AltLookingForThisC = "232154"
      LookingForThisB = "232153"
      AltLookingForThisB = "232153"
      LookingForThisA = "242756"
      AltLookingForThisA = "242756"
    Case "The Worm King"
      LookingForThisABC = "231977"
      AltLookingForThisABC = "231977"
      LookingForThisAB = "231976"
      AltLookingForThisAB = "231976"
      LookingForThisC = "231974"
      AltLookingForThisC = "231974"
      LookingForThisB = "231973"
      AltLookingForThisB = "231973"
      LookingForThisA = "242736"
      AltLookingForThisA = "242736"
    Case "Thunderous Chimera"
      LookingForThisABC = "229472"
      AltLookingForThisABC = "231716"
      LookingForThisAB = "229471"
      AltLookingForThisAB = "231715"
      LookingForThisC = "231713"
      AltLookingForThisC = "231713"
      LookingForThisB = "231712"
      AltLookingForThisB = "231712"
      LookingForThisA = "242708"
      AltLookingForThisA = "242708"
    Case "Toss"
      LookingForThisABC = "232112"
      AltLookingForThisABC = "232112"
      LookingForThisAB = "232111"
      AltLookingForThisAB = "232111"
      LookingForThisC = "232109"
      AltLookingForThisC = "232109"
      LookingForThisB = "232108"
      AltLookingForThisB = "232108"
      LookingForThisA = "242751"
      AltLookingForThisA = "242751"
    Case "Skulky"
      LookingForThisABC = "232085"
      AltLookingForThisABC = "232085"
      LookingForThisAB = "232084"
      AltLookingForThisAB = "232084"
      LookingForThisC = "232082"
      AltLookingForThisC = "232082"
      LookingForThisB = "232081"
      AltLookingForThisB = "232081"
      LookingForThisA = "242748"
      AltLookingForThisA = "242748"
    Case "Slinky"
      LookingForThisABC = "232067"
      AltLookingForThisABC = "232067"
      LookingForThisAB = "232066"
      AltLookingForThisAB = "232066"
      LookingForThisC = "232064"
      AltLookingForThisC = "232064"
      LookingForThisB = "232063"
      AltLookingForThisB = "232063"
      LookingForThisA = "242746"
      AltLookingForThisA = "242746"
    Case "Smee"
      LookingForThisABC = "232040"
      AltLookingForThisABC = "232040"
      LookingForThisAB = "232039"
      AltLookingForThisAB = "232039"
      LookingForThisC = "232037"
      AltLookingForThisC = "232037"
      LookingForThisB = "232036"
      AltLookingForThisB = "232036"
      LookingForThisA = "242743"
      AltLookingForThisA = "242743"
    Case "Spiritless Soul Dredge"
      LookingForThisABC = "229257"
      AltLookingForThisABC = "231509"
      LookingForThisAB = "229256"
      AltLookingForThisAB = "231508"
      LookingForThisC = "231506"
      AltLookingForThisC = "231506"
      LookingForThisB = "231505"
      AltLookingForThisB = "231505"
      LookingForThisA = "242685"
      AltLookingForThisA = "242685"
    Case "Taille Frees"
      LookingForThisABC = "232345"
      AltLookingForThisABC = "234194"
      LookingForThisAB = "232344"
      AltLookingForThisAB = "234193"
      LookingForThisC = "232342"
      AltLookingForThisC = "234191"
      LookingForThisB = "232341"
      AltLookingForThisB = "234190"
      LookingForThisA = "242774"
      AltLookingForThisA = "242774"
    Case "Tcheser"
      LookingForThisABC = "239703"
      AltLookingForThisABC = "239703"
      LookingForThisAB = "239702"
      AltLookingForThisAB = "239702"
      LookingForThisC = "239700"
      AltLookingForThisC = "239700"
      LookingForThisB = "239699"
      AltLookingForThisB = "239699"
      LookingForThisA = "242819"
      AltLookingForThisA = "242819"
    Case "The Abysmal Lord"
      LookingForThisABC = "239495"
      AltLookingForThisABC = "239495"
      LookingForThisAB = "239494"
      AltLookingForThisAB = "239494"
      LookingForThisC = "239492"
      AltLookingForThisC = "239492"
      LookingForThisB = "239491"
      AltLookingForThisB = "239491"
      LookingForThisA = "242796"
      AltLookingForThisA = "242796"
    Case "The Abyssal Widow"
      LookingForThisABC = "232193"
      AltLookingForThisABC = "232193"
      LookingForThisAB = "232192"
      AltLookingForThisAB = "232192"
      LookingForThisC = "232190"
      AltLookingForThisC = "232190"
      LookingForThisB = "232189"
      AltLookingForThisB = "232189"
      LookingForThisA = "242760"
      AltLookingForThisA = "242760"
    Case "The Achbile Guardian"
      LookingForThisABC = "239694"
      AltLookingForThisABC = "239694"
      LookingForThisAB = "239693"
      AltLookingForThisAB = "239693"
      LookingForThisC = "239691"
      AltLookingForThisC = "239691"
      LookingForThisB = "239690"
      AltLookingForThisB = "239690"
      LookingForThisA = "242818"
      AltLookingForThisA = "242818"
    Case "The Adonian Soul Dredge"
      LookingForThisABC = "232148"
      AltLookingForThisABC = "232148"
      LookingForThisAB = "232147"
      AltLookingForThisAB = "232147"
      LookingForThisC = "232145"
      AltLookingForThisC = "232145"
      LookingForThisB = "232144"
      AltLookingForThisB = "232144"
      LookingForThisA = "242755"
      AltLookingForThisA = "242755"
    Case "The Adonis Spirit Master"
      LookingForThisABC = "232175"
      AltLookingForThisABC = "232175"
      LookingForThisAB = "232174"
      AltLookingForThisAB = "232174"
      LookingForThisC = "232172"
      AltLookingForThisC = "232172"
      LookingForThisB = "232171"
      AltLookingForThisB = "232171"
      LookingForThisA = "242758"
      AltLookingForThisA = "242758"
    Case "The Archbile Queen"
      LookingForThisABC = "229536"
      AltLookingForThisABC = "231779"
      LookingForThisAB = "229535"
      AltLookingForThisAB = "231778"
      LookingForThisC = "229533"
      AltLookingForThisC = "231776"
      LookingForThisB = "229532"
      AltLookingForThisB = "231775"
      LookingForThisA = "242715"
      AltLookingForThisA = "242715"
    Case "Tough"
      LookingForThisABC = "229432"
      AltLookingForThisABC = "231680"
      LookingForThisAB = "229431"
      AltLookingForThisAB = "231679"
      LookingForThisC = "231677"
      AltLookingForThisC = "231677"
      LookingForThisB = "231676"
      AltLookingForThisB = "231676"
      LookingForThisA = "242704"
      AltLookingForThisA = "242704"
    Case "Ungulera"
      LookingForThisABC = "229230"
      AltLookingForThisABC = "231482"
      LookingForThisAB = "229229"
      AltLookingForThisAB = "231481"
      LookingForThisC = "229227"
      AltLookingForThisC = "231479"
      LookingForThisB = "229226"
      AltLookingForThisB = "231478"
      LookingForThisA = "242682"
      AltLookingForThisA = "242682"
    Case "The Brobdingnagian Mother"
      LookingForThisABC = "239621"
      AltLookingForThisABC = "239621"
      LookingForThisAB = "239620"
      AltLookingForThisAB = "239620"
      LookingForThisC = "239618"
      AltLookingForThisC = "239618"
      LookingForThisB = "239617"
      AltLookingForThisB = "239617"
      LookingForThisA = "242810"
      AltLookingForThisA = "242810"
    Case "The Dredge Driver"
      LookingForThisABC = "232166"
      AltLookingForThisABC = "232166"
      LookingForThisAB = "232165"
      AltLookingForThisAB = "232165"
      LookingForThisC = "232163"
      AltLookingForThisC = "232163"
      LookingForThisB = "232162"
      AltLookingForThisB = "232162"
      LookingForThisA = "242757"
      AltLookingForThisA = "242757"
    Case "The Dryad Demigod"
      LookingForThisABC = "232390"
      AltLookingForThisABC = "234239"
      LookingForThisAB = "232389"
      AltLookingForThisAB = "234238"
      LookingForThisC = "232387"
      AltLookingForThisC = "234236"
      LookingForThisB = "232386"
      AltLookingForThisB = "234235"
      LookingForThisA = "242779"
      AltLookingForThisA = "242779"
    Case "The Dryad Shuffle"
      LookingForThisABC = "231941"
      AltLookingForThisABC = "231941"
      LookingForThisAB = "231940"
      AltLookingForThisAB = "231940"
      LookingForThisC = "231938"
      AltLookingForThisC = "231938"
      LookingForThisB = "231937"
      AltLookingForThisB = "231937"
      LookingForThisA = "242732"
      AltLookingForThisA = "242732"
    Case "The Dune Suzerain"
      LookingForThisABC = "229212"
      AltLookingForThisABC = "231464"
      LookingForThisAB = "231463"
      AltLookingForThisAB = "229211"
      LookingForThisC = "229209"
      AltLookingForThisC = "231461"
      LookingForThisB = "229208"
      AltLookingForThisB = "231460"
      LookingForThisA = "242680"
      AltLookingForThisA = "242680"
    Case "The Elysian Soul Dredge"
      LookingForThisABC = "229239"
      AltLookingForThisABC = "231491"
      LookingForThisAB = "229238"
      AltLookingForThisAB = "231490"
      LookingForThisC = "229236"
      AltLookingForThisC = "231488"
      LookingForThisB = "229235"
      AltLookingForThisB = "231487"
      LookingForThisA = "242683"
      AltLookingForThisA = "242683"
    Case "The Enrapt One"
      LookingForThisABC = "229563"
      AltLookingForThisABC = "231806"
      LookingForThisAB = "229562"
      AltLookingForThisAB = "231805"
      LookingForThisC = "229560"
      AltLookingForThisC = "231803"
      LookingForThisB = "229559"
      AltLookingForThisB = "231802"
      LookingForThisA = "242718"
      AltLookingForThisA = "242718"
    Case "The Indomitable Chimera"
      LookingForThisABC = "232426"
      AltLookingForThisABC = "234277"
      LookingForThisAB = "232425"
      AltLookingForThisAB = "234276"
      LookingForThisC = "232423"
      AltLookingForThisC = "234274"
      LookingForThisB = "232422"
      AltLookingForThisB = "234273"
      LookingForThisA = "242783"
      AltLookingForThisA = "242783"
    Case "The Infernal Soul Dredge"
      LookingForThisABC = "239504"
      AltLookingForThisABC = "242455"
      LookingForThisAB = "239503"
      AltLookingForThisAB = "242454"
      LookingForThisC = "239501"
      AltLookingForThisC = "242452"
      LookingForThisB = "239500"
      AltLookingForThisB = "242451"
      LookingForThisA = "242797"
      AltLookingForThisA = "242797"
    Case "The Maggot Lord"
      LookingForThisABC = "231923"
      AltLookingForThisABC = "231869"
      LookingForThisAB = "231922"
      AltLookingForThisAB = "231868"
      LookingForThisC = "231866"
      AltLookingForThisC = "231866"
      LookingForThisB = "231865"
      AltLookingForThisB = "231865"
      LookingForThisA = "242725"
      AltLookingForThisA = "242725"
    Case "The Mortificator"
      LookingForThisABC = "232291"
      AltLookingForThisABC = "234140"
      LookingForThisAB = "232290"
      AltLookingForThisAB = "234139"
      LookingForThisC = "232288"
      AltLookingForThisC = "234137"
      LookingForThisB = "232287"
      AltLookingForThisB = "234136"
      LookingForThisA = "242768"
      AltLookingForThisA = "242768"
    Case "The Numb One"
      LookingForThisABC = "229554"
      AltLookingForThisABC = "231797"
      LookingForThisAB = "229553"
      AltLookingForThisAB = "231796"
      LookingForThisC = "229551"
      AltLookingForThisC = "231794"
      LookingForThisB = "229550"
      AltLookingForThisB = "231793"
      LookingForThisA = "242717"
      AltLookingForThisA = "242717"
    Case "Marly Suzerain"
      LookingForThisABC = "229423"
      AltLookingForThisABC = "231671"
      LookingForThisAB = "229422"
      AltLookingForThisAB = "231670"
      LookingForThisC = "231668"
      AltLookingForThisC = "231668"
      LookingForThisB = "231667"
      AltLookingForThisB = "231667"
      LookingForThisA = "242703"
      AltLookingForThisA = "242703"
    Case "Mawi"
      LookingForThisABC = "239594"
      AltLookingForThisABC = "239594"
      LookingForThisAB = "239593"
      AltLookingForThisAB = "239593"
      LookingForThisC = "239591"
      AltLookingForThisC = "239591"
      LookingForThisB = "239590"
      AltLookingForThisB = "239590"
      LookingForThisA = "242807"
      AltLookingForThisA = "242807"
    Case "Misery"
      LookingForThisABC = "229365"
      AltLookingForThisABC = "231617"
      LookingForThisAB = "229364"
      AltLookingForThisAB = "231616"
      LookingForThisC = "231614"
      AltLookingForThisC = "231614"
      LookingForThisB = "231613"
      AltLookingForThisB = "231613"
      LookingForThisA = "242697"
      AltLookingForThisA = "242697"
    Case "Moochy"
      LookingForThisABC = "232094"
      AltLookingForThisABC = "232094"
      LookingForThisAB = "232093"
      AltLookingForThisAB = "232093"
      LookingForThisC = "232091"
      AltLookingForThisC = "232091"
      LookingForThisB = "232090"
      AltLookingForThisB = "232090"
      LookingForThisA = "242749"
      AltLookingForThisA = "242749"
    Case "Morrow"
      LookingForThisABC = "231887"
      AltLookingForThisABC = "231887"
      LookingForThisAB = "231886"
      AltLookingForThisAB = "231886"
      LookingForThisC = "231884"
      AltLookingForThisC = "231884"
      LookingForThisB = "231883"
      AltLookingForThisB = "231883"
      LookingForThisA = "242727"
      AltLookingForThisA = "242727"
    Case "Nyame"
      LookingForThisABC = "239486"
      AltLookingForThisABC = "239486"
      LookingForThisAB = "239485"
      AltLookingForThisAB = "239485"
      LookingForThisC = "239483"
      AltLookingForThisC = "239483"
      LookingForThisB = "239482"
      AltLookingForThisB = "239482"
      LookingForThisA = "242795"
      AltLookingForThisA = "242795"
    Case "Odqan"
      LookingForThisABC = "232525"
      AltLookingForThisABC = "234381"
      LookingForThisAB = "232524"
      AltLookingForThisAB = "234379"
      LookingForThisC = "232522"
      AltLookingForThisC = "234375"
      LookingForThisB = "232521"
      AltLookingForThisB = "234374"
      LookingForThisA = "242794"
      AltLookingForThisA = "242794"
    Case "Old Salty"
      LookingForThisABC = "232031"
      AltLookingForThisABC = "232031"
      LookingForThisAB = "232030"
      AltLookingForThisAB = "232030"
      LookingForThisC = "232028"
      AltLookingForThisC = "232028"
      LookingForThisB = "232027"
      AltLookingForThisB = "232027"
      LookingForThisA = "242742"
      AltLookingForThisA = "242742"
    Case "Ooze"
      LookingForThisABC = "229509"
      AltLookingForThisABC = "231752"
      LookingForThisAB = "229508"
      AltLookingForThisAB = "231751"
      LookingForThisC = "231749"
      AltLookingForThisC = "231749"
      LookingForThisB = "231748"
      AltLookingForThisB = "231748"
      LookingForThisA = "242712"
      AltLookingForThisA = "242712"
    Case "Pazuzu"
      LookingForThisABC = "232399"
      AltLookingForThisABC = "234248"
      LookingForThisAB = "232398"
      AltLookingForThisAB = "234247"
      LookingForThisC = "232396"
      AltLookingForThisC = "234245"
      LookingForThisB = "232395"
      AltLookingForThisB = "234244"
      LookingForThisA = "242780"
      AltLookingForThisA = "242780"
    Case "Quake"
      LookingForThisABC = "232121"
      AltLookingForThisABC = "232121"
      LookingForThisAB = "232120"
      AltLookingForThisAB = "232120"
      LookingForThisC = "232118"
      AltLookingForThisC = "232118"
      LookingForThisB = "232117"
      AltLookingForThisB = "232117"
      LookingForThisA = "242752"
      AltLookingForThisA = "242752"
    Case "Quondam"
      LookingForThisABC = "231878"
      AltLookingForThisABC = "231878"
      LookingForThisAB = "231877"
      AltLookingForThisAB = "231877"
      LookingForThisC = "231875"
      AltLookingForThisC = "231875"
      LookingForThisB = "231874"
      AltLookingForThisB = "231874"
      LookingForThisA = "242726"
      AltLookingForThisA = "242726"
    Case "Rallies Fete"
      LookingForThisABC = "232444"
      AltLookingForThisABC = "234297"
      LookingForThisAB = "232443"
      AltLookingForThisAB = "234296"
      LookingForThisC = "232441"
      AltLookingForThisC = "234294"
      LookingForThisB = "232440"
      AltLookingForThisB = "234293"
      LookingForThisA = "242785"
      AltLookingForThisA = "242785"
    Case "Razor the Battletoad"
      LookingForThisABC = "232498"
      AltLookingForThisABC = "234351"
      LookingForThisAB = "232497"
      AltLookingForThisAB = "234350"
      LookingForThisC = "232495"
      AltLookingForThisC = "234348"
      LookingForThisB = "232494"
      AltLookingForThisB = "234347"
      LookingForThisA = "242791"
      AltLookingForThisA = "242791"
    Case "Redeemed Cama"
      LookingForThisABC = "242542"
      AltLookingForThisABC = "242542"
      LookingForThisAB = "242541"
      AltLookingForThisAB = "242541"
      LookingForThisC = "242539"
      AltLookingForThisC = "242539"
      LookingForThisB = "242538"
      AltLookingForThisB = "242538"
      LookingForThisA = "242667"
      AltLookingForThisA = "242667"
    Case "Redeemed Gilthar"
      LookingForThisABC = "232561"
      AltLookingForThisABC = "242515"
      LookingForThisAB = "232560"
      AltLookingForThisAB = "242514"
      LookingForThisC = "232558"
      AltLookingForThisC = "242512"
      LookingForThisB = "232557"
      AltLookingForThisB = "242511"
      LookingForThisA = "242663"
      AltLookingForThisA = "242663"
    Case "Redeemed Lord Galahad"
      LookingForThisABC = "232615"
      AltLookingForThisABC = "234437"
      LookingForThisAB = "232614"
      AltLookingForThisAB = "234436"
      LookingForThisC = "234432"
      AltLookingForThisC = "234432"
      LookingForThisB = "234431"
      AltLookingForThisB = "234431"
      LookingForThisA = "242669"
      AltLookingForThisA = "242669"
    Case "Relief Teals"
      LookingForThisABC = "232273"
      AltLookingForThisABC = "234122"
      LookingForThisAB = "232272"
      AltLookingForThisAB = "234121"
      LookingForThisC = "232270"
      AltLookingForThisC = "234119"
      LookingForThisB = "232269"
      AltLookingForThisB = "234118"
      LookingForThisA = "242766"
      AltLookingForThisA = "242766"
    Case "Sabretooth Slicer"
      LookingForThisABC = "239549"
      AltLookingForThisABC = "239549"
      LookingForThisAB = "239548"
      AltLookingForThisAB = "239548"
      LookingForThisC = "239546"
      AltLookingForThisC = "239546"
      LookingForThisB = "239545"
      AltLookingForThisB = "239545"
      LookingForThisA = "242802"
      AltLookingForThisA = "242802"
    Case "Sampsa"
      LookingForThisABC = "231851"
      AltLookingForThisABC = "231851"
      LookingForThisAB = "231850"
      AltLookingForThisAB = "231850"
      LookingForThisC = "231848"
      AltLookingForThisC = "231848"
      LookingForThisB = "231847"
      AltLookingForThisB = "231847"
      LookingForThisA = "242723"
      AltLookingForThisA = "242723"
    Case "Sasabonsam"
      LookingForThisABC = "232354"
      AltLookingForThisABC = "234203"
      LookingForThisAB = "232353"
      AltLookingForThisAB = "234202"
      LookingForThisC = "232351"
      AltLookingForThisC = "234200"
      LookingForThisB = "232350"
      AltLookingForThisB = "234199"
      LookingForThisA = "242775"
      AltLookingForThisA = "242775"
    Case "Sashu"
      LookingForThisABC = "232220"
      AltLookingForThisABC = "234095"
      LookingForThisAB = "232219"
      AltLookingForThisAB = "234094"
      LookingForThisC = "232217"
      AltLookingForThisC = "234092"
      LookingForThisB = "232216"
      AltLookingForThisB = "234091"
      LookingForThisA = "242763"
      AltLookingForThisA = "242763"
    Case "Satkamear"
      LookingForThisABC = "239657"
      AltLookingForThisABC = "239657"
      LookingForThisAB = "239656"
      AltLookingForThisAB = "239656"
      LookingForThisC = "239654"
      AltLookingForThisC = "239654"
      LookingForThisB = "239653"
      AltLookingForThisB = "239653"
      LookingForThisA = "242814"
      AltLookingForThisA = "242814"
    Case "Sawi"
      LookingForThisABC = "239603"
      AltLookingForThisABC = "239603"
      LookingForThisAB = "239602"
      AltLookingForThisAB = "239602"
      LookingForThisC = "239600"
      AltLookingForThisC = "239600"
      LookingForThisB = "239599"
      AltLookingForThisB = "239599"
      LookingForThisA = "242808"
      AltLookingForThisA = "242808"
    Case "Scratch"
      LookingForThisABC = "229176"
      AltLookingForThisABC = "231428"
      LookingForThisAB = "229175"
      AltLookingForThisAB = "231427"
      LookingForThisC = "229173"
      AltLookingForThisC = "231425"
      LookingForThisB = "231424"
      AltLookingForThisB = "231424"
      LookingForThisA = "242676"
      AltLookingForThisA = "242676"
    Case "Screech"
      LookingForThisABC = "229185"
      AltLookingForThisABC = "231437"
      LookingForThisAB = "229184"
      AltLookingForThisAB = "231436"
      LookingForThisC = "229182"
      AltLookingForThisC = "231434"
      LookingForThisB = "229181"
      AltLookingForThisB = "231433"
      LookingForThisA = "242677"
      AltLookingForThisA = "242677"
    Case "Shake (QL 192)"
      LookingForThisABC = "232211"
      LookingForThisAB = "232210"
      LookingForThisC = "232208"
      LookingForThisB = "232207"
      LookingForThisA = "242762"
      AltLookingForThisABC = "232211"
      AltLookingForThisAB = "232210"
      AltLookingForThisC = "232208"
      AltLookingForThisB = "232207"
      AltLookingForThisA = "242762"
    Case "Shake (QL 195)"
      AltLookingForThisABC = "232103"
      AltLookingForThisAB = "232102"
      AltLookingForThisC = "232100"
      AltLookingForThisB = "232099"
      AltLookingForThisA = "242750"
      LookingForThisABC = "232103"
      LookingForThisAB = "232102"
      LookingForThisC = "232100"
      LookingForThisB = "232099"
      LookingForThisA = "242750"
    Case "Shiver"
      LookingForThisABC = "232202"
      AltLookingForThisABC = "232202"
      LookingForThisAB = "232201"
      AltLookingForThisAB = "232201"
      LookingForThisC = "232199"
      AltLookingForThisC = "232199"
      LookingForThisB = "232198"
      AltLookingForThisB = "232198"
      LookingForThisA = "242761"
      AltLookingForThisA = "242761"
    Case "Shullat"
      LookingForThisABC = "232507"
      AltLookingForThisABC = "234360"
      LookingForThisAB = "232506"
      AltLookingForThisAB = "234359"
      LookingForThisC = "232504"
      AltLookingForThisC = "234357"
      LookingForThisB = "232503"
      AltLookingForThisB = "234356"
      LookingForThisA = "242792"
      AltLookingForThisA = "242792"
    Case "Silver Fang"
      LookingForThisABC = "229149"
      AltLookingForThisABC = "231401"
      LookingForThisAB = "229148"
      AltLookingForThisAB = "231400"
      LookingForThisC = "229146"
      AltLookingForThisC = "231398"
      LookingForThisB = "229145"
      AltLookingForThisB = "231397"
      LookingForThisA = "242673"
      AltLookingForThisA = "242673"
    Case "Defiler of Scheol"
      LookingForThisABC = "229581"
      AltLookingForThisABC = "231824"
      LookingForThisAB = "229580"
      AltLookingForThisAB = "231823"
      LookingForThisC = "231821"
      AltLookingForThisC = "231821"
      LookingForThisB = "231820"
      AltLookingForThisB = "231820"
      LookingForThisA = "242720"
      AltLookingForThisA = "242720"
    Case "Destroyer of Scheol"
      LookingForThisABC = "229599"
      AltLookingForThisABC = "231842"
      LookingForThisAB = "229598"
      AltLookingForThisAB = "231841"
      LookingForThisC = "231839"
      AltLookingForThisC = "231839"
      LookingForThisB = "231838"
      AltLookingForThisB = "231838"
      LookingForThisA = "242722"
      AltLookingForThisA = "242722"
    Case "Devastator of Scheol"
      LookingForThisABC = "229590"
      AltLookingForThisABC = "231833"
      LookingForThisAB = "229589"
      AltLookingForThisAB = "231832"
      LookingForThisC = "231830"
      AltLookingForThisC = "231830"
      LookingForThisB = "231829"
      AltLookingForThisB = "231829"
      LookingForThisA = "242721"
      AltLookingForThisA = "242721"
    Case "Devourer of Scheol"
      LookingForThisABC = "229572"
      AltLookingForThisABC = "231815"
      LookingForThisAB = "229571"
      AltLookingForThisAB = "231814"
      LookingForThisC = "231812"
      AltLookingForThisC = "231812"
      LookingForThisB = "231811"
      AltLookingForThisB = "231811"
      LookingForThisA = "242719"
      AltLookingForThisA = "242719"
    Case "Eidolean Soul Dredge"
      LookingForThisABC = "239639"
      AltLookingForThisABC = "239639"
      LookingForThisAB = "239638"
      AltLookingForThisAB = "239638"
      LookingForThisC = "239636"
      AltLookingForThisC = "239636"
      LookingForThisB = "239635"
      AltLookingForThisB = "239635"
      LookingForThisA = "242812"
      AltLookingForThisA = "242812"
    Case "Exsequiae"
      LookingForThisABC = "239685"
      AltLookingForThisABC = "239685"
      LookingForThisAB = "239684"
      AltLookingForThisAB = "239684"
      LookingForThisC = "239682"
      AltLookingForThisC = "239682"
      LookingForThisB = "239681"
      AltLookingForThisB = "239681"
      LookingForThisA = "242817"
      AltLookingForThisA = "242817"
    Case "Fester Leila"
      LookingForThisABC = "232300"
      AltLookingForThisABC = "234149"
      LookingForThisAB = "232299"
      AltLookingForThisAB = "234148"
      LookingForThisC = "232297"
      AltLookingForThisC = "234146"
      LookingForThisB = "232296"
      AltLookingForThisB = "234145"
      LookingForThisA = "242769"
      AltLookingForThisA = "242769"
    Case "Flinty"
      LookingForThisABC = "229441"
      AltLookingForThisABC = "231689"
      LookingForThisAB = "229440"
      AltLookingForThisAB = "231688"
      LookingForThisC = "231686"
      AltLookingForThisC = "231686"
      LookingForThisB = "231685"
      AltLookingForThisB = "231685"
      LookingForThisA = "242705"
      AltLookingForThisA = "242705"
    Case "Glitter"
      LookingForThisABC = "239648"
      AltLookingForThisABC = "239648"
      LookingForThisAB = "239647"
      AltLookingForThisAB = "239647"
      LookingForThisC = "239645"
      AltLookingForThisC = "239645"
      LookingForThisB = "239644"
      AltLookingForThisB = "239644"
      LookingForThisA = "242813"
      AltLookingForThisA = "242813"
    Case "Gracious Soul Dredge"
      LookingForThisABC = "229248"
      AltLookingForThisABC = "231500"
      LookingForThisAB = "229247"
      AltLookingForThisAB = "231499"
      LookingForThisC = "231497"
      AltLookingForThisC = "231497"
      LookingForThisB = "231496"
      AltLookingForThisB = "231496"
      LookingForThisA = "242684"
      AltLookingForThisA = "242684"
    Case "Gunk"
      LookingForThisABC = "229463"
      AltLookingForThisABC = "231707"
      LookingForThisAB = "229462"
      AltLookingForThisAB = "231706"
      LookingForThisC = "231704"
      AltLookingForThisC = "231704"
      LookingForThisB = "231703"
      AltLookingForThisB = "231703"
      LookingForThisA = "242707"
      AltLookingForThisA = "242707"
    Case "Zoetic Oak"
      LookingForThisABC = "231959"
      AltLookingForThisABC = "231959"
      LookingForThisAB = "231958"
      AltLookingForThisAB = "231958"
      LookingForThisC = "231956"
      AltLookingForThisC = "231956"
      LookingForThisB = "231955"
      AltLookingForThisB = "231955"
      LookingForThisA = "242734"
      AltLookingForThisA = "242734"
    Case "Hadur"
      LookingForThisABC = "231995"
      AltLookingForThisABC = "231995"
      LookingForThisAB = "231994"
      AltLookingForThisAB = "231994"
      LookingForThisC = "231992"
      AltLookingForThisC = "231992"
      LookingForThisB = "231991"
      AltLookingForThisB = "231991"
      LookingForThisA = "242738"
      AltLookingForThisA = "242738"
    Case "Haqa"
      LookingForThisABC = "232336"
      AltLookingForThisABC = "234185"
      LookingForThisAB = "232335"
      AltLookingForThisAB = "234184"
      LookingForThisC = "232333"
      AltLookingForThisC = "234182"
      LookingForThisB = "232332"
      AltLookingForThisB = "234181"
      LookingForThisA = "242773"
      AltLookingForThisA = "242773"
    Case "Hemut"
      LookingForThisABC = "229383"
      AltLookingForThisABC = "231635"
      LookingForThisAB = "229382"
      AltLookingForThisAB = "231634"
      LookingForThisC = "231632"
      AltLookingForThisC = "231632"
      LookingForThisB = "231631"
      AltLookingForThisB = "231631"
      LookingForThisA = "242699"
      AltLookingForThisA = "242699"
    Case "Ho"
      LookingForThisABC = "239675"
      AltLookingForThisABC = "239675"
      LookingForThisAB = "239674"
      AltLookingForThisAB = "239674"
      LookingForThisC = "239672"
      AltLookingForThisC = "239672"
      LookingForThisB = "239671"
      AltLookingForThisB = "239671"
      LookingForThisA = "242816"
      AltLookingForThisA = "242816"
    Case "Ignis Fatui"
      LookingForThisABC = "229266"
      AltLookingForThisABC = "231518"
      LookingForThisAB = "229265"
      AltLookingForThisAB = "231517"
      LookingForThisC = "231515"
      AltLookingForThisC = "231515"
      LookingForThisB = "231514"
      AltLookingForThisB = "231514"
      LookingForThisA = "242686"
      AltLookingForThisA = "242686"
    Case "Ignis Fatuus"
      LookingForThisABC = "229329"
      AltLookingForThisABC = "231581"
      LookingForThisAB = "229328"
      AltLookingForThisAB = "231580"
      LookingForThisC = "231578"
      AltLookingForThisC = "231578"
      LookingForThisB = "231577"
      AltLookingForThisB = "231577"
      LookingForThisA = "242693"
      AltLookingForThisA = "242693"
    Case "Imk'a"
      LookingForThisABC = "232004"
      AltLookingForThisABC = "232004"
      LookingForThisAB = "232003"
      AltLookingForThisAB = "232003"
      LookingForThisC = "232001"
      AltLookingForThisC = "232001"
      LookingForThisB = "232000"
      AltLookingForThisB = "232000"
      LookingForThisA = "242739"
      AltLookingForThisA = "242739"
    Case "Infernal Demon"
      LookingForThisABC = "232579"
      AltLookingForThisABC = "232579"
      LookingForThisAB = "232578"
      AltLookingForThisAB = "232578"
      LookingForThisC = "232576"
      AltLookingForThisC = "232576"
      LookingForThisB = "232575"
      AltLookingForThisB = "232575"
      LookingForThisA = "242665"
      AltLookingForThisA = "242665"
    Case "Iunmin"
      LookingForThisABC = "229284"
      AltLookingForThisABC = "231536"
      LookingForThisAB = "229283"
      AltLookingForThisAB = "231535"
      LookingForThisC = "231533"
      AltLookingForThisC = "231533"
      LookingForThisB = "231532"
      AltLookingForThisB = "231532"
      LookingForThisA = "242688"
      AltLookingForThisA = "242688"
    Case "K'a"
      LookingForThisABC = "232229"
      AltLookingForThisABC = "234104"
      LookingForThisAB = "232228"
      AltLookingForThisAB = "234103"
      LookingForThisC = "232226"
      AltLookingForThisC = "234101"
      LookingForThisB = "232225"
      AltLookingForThisB = "234100"
      LookingForThisA = "242764"
      AltLookingForThisA = "242764"
    Case "Kaleva"
      LookingForThisABC = "232022"
      AltLookingForThisABC = "232022"
      LookingForThisAB = "232021"
      AltLookingForThisAB = "232021"
      LookingForThisC = "232019"
      AltLookingForThisC = "232019"
      LookingForThisB = "232018"
      AltLookingForThisB = "232018"
      LookingForThisA = "242741"
      AltLookingForThisA = "242741"
    Case "Kaoline Suzerain"
      LookingForThisABC = "229527"
      AltLookingForThisABC = "231770"
      LookingForThisAB = "229526"
      AltLookingForThisAB = "231769"
      LookingForThisC = "231767"
      AltLookingForThisC = "231767"
      LookingForThisB = "231766"
      AltLookingForThisB = "231766"
      LookingForThisA = "242714"
      AltLookingForThisA = "242714"
    Case "Khemhet"
      LookingForThisABC = "229392"
      AltLookingForThisABC = "231644"
      LookingForThisAB = "229391"
      AltLookingForThisAB = "231643"
      LookingForThisC = "231641"
      AltLookingForThisC = "231641"
      LookingForThisB = "231640"
      AltLookingForThisB = "231640"
      LookingForThisA = "242700"
      AltLookingForThisA = "242700"
    Case "Lethargic Spirit"
      LookingForThisABC = "229356"
      AltLookingForThisABC = "231608"
      LookingForThisAB = "229355"
      AltLookingForThisAB = "231607"
      LookingForThisC = "231605"
      AltLookingForThisC = "231605"
      LookingForThisB = "231604"
      AltLookingForThisB = "231604"
      LookingForThisA = "242696"
      AltLookingForThisA = "242696"
    Case "Loessial Suzerain"
      LookingForThisABC = "229500"
      AltLookingForThisABC = "231743"
      LookingForThisAB = "229499"
      AltLookingForThisAB = "231742"
      LookingForThisC = "231740"
      AltLookingForThisC = "231740"
      LookingForThisB = "231739"
      AltLookingForThisB = "231739"
      LookingForThisA = "242711"
      AltLookingForThisA = "242711"
    Case "Loltonunon"
      LookingForThisABC = "239666"
      AltLookingForThisABC = "239666"
      LookingForThisAB = "239665"
      AltLookingForThisAB = "239665"
      LookingForThisC = "239663"
      AltLookingForThisC = "239663"
      LookingForThisB = "239662"
      AltLookingForThisB = "239662"
      LookingForThisA = "242815"
      AltLookingForThisA = "242815"
    Case "Lurky"
      LookingForThisABC = "232076"
      AltLookingForThisABC = "232076"
      LookingForThisAB = "232075"
      AltLookingForThisAB = "232075"
      LookingForThisC = "232073"
      AltLookingForThisC = "232073"
      LookingForThisB = "232072"
      AltLookingForThisB = "232072"
      LookingForThisA = "242747"
      AltLookingForThisA = "242747"
    Case "Lya"
      LookingForThisABC = "232372"
      AltLookingForThisABC = "234221"
      LookingForThisAB = "232371"
      AltLookingForThisAB = "234220"
      LookingForThisC = "232369"
      AltLookingForThisC = "234218"
      LookingForThisB = "232368"
      AltLookingForThisB = "234217"
      LookingForThisA = "242777"
      AltLookingForThisA = "242777"
    Case "Malah-Animus"
      LookingForThisABC = "239531"
      AltLookingForThisABC = "239531"
      LookingForThisAB = "239530"
      AltLookingForThisAB = "239530"
      LookingForThisC = "239528"
      AltLookingForThisC = "239528"
      LookingForThisB = "239527"
      AltLookingForThisB = "239527"
      LookingForThisA = "242800"
      AltLookingForThisA = "242800"
    Case "Malah-At"
      LookingForThisABC = "239522"
      AltLookingForThisABC = "239522"
      LookingForThisAB = "239521"
      AltLookingForThisAB = "239521"
      LookingForThisC = "239519"
      AltLookingForThisC = "239519"
      LookingForThisB = "239518"
      AltLookingForThisB = "239518"
      LookingForThisA = "242799"
      AltLookingForThisA = "242799"
    Case "Malah-Auris"
      LookingForThisABC = "239513"
      AltLookingForThisABC = "239513"
      LookingForThisAB = "239512"
      AltLookingForThisAB = "239512"
      LookingForThisC = "239510"
      AltLookingForThisC = "239510"
      LookingForThisB = "239509"
      AltLookingForThisB = "239509"
      LookingForThisA = "242798"
      AltLookingForThisA = "242798"
    Case "Maledicta"
      LookingForThisABC = "239576"
      AltLookingForThisABC = "239576"
      LookingForThisAB = "239575"
      AltLookingForThisAB = "239575"
      LookingForThisC = "239573"
      AltLookingForThisC = "239573"
      LookingForThisB = "239572"
      AltLookingForThisB = "239572"
      LookingForThisA = "242805"
      AltLookingForThisA = "242805"
    Case "Marem"
      LookingForThisABC = "229338"
      AltLookingForThisABC = "231590"
      LookingForThisAB = "229337"
      AltLookingForThisAB = "231589"
      LookingForThisC = "231587"
      AltLookingForThisC = "231587"
      LookingForThisB = "231586"
      AltLookingForThisB = "231586"
      LookingForThisA = "242694"
      AltLookingForThisA = "242694"
    Case "Juma"
      LookingForThisABC = "232013"
      LookingForThisC = "232010"
      LookingForThisAB = "232012"
      LookingForThisB = "232009"
      LookingForThisA = "242740"
      AltLookingForThisABC = "232013"
      AltLookingForThisC = "232010"
      AltLookingForThisAB = "232012"
      AltLookingForThisB = "232009"
      AltLookingForThisA = "242740"
    Case "Blight"
      LookingForThisABC = "229374"
      AltLookingForThisABC = "231626"
      LookingForThisAB = "229373"
      AltLookingForThisAB = "231625"
      LookingForThisC = "231623"
      AltLookingForThisC = "231623"
      LookingForThisB = "231622"
      AltLookingForThisB = "231622"
      LookingForThisA = "242698"
      AltLookingForThisA = "242698"
    Case "Borer"
      LookingForThisABC = "229311"
      AltLookingForThisABC = "231563"
      LookingForThisAB = "229310"
      AltLookingForThisAB = "231562"
      LookingForThisC = "231560"
      AltLookingForThisC = "231560"
      LookingForThisB = "231559"
      AltLookingForThisB = "231559"
      LookingForThisA = "242691"
      AltLookingForThisA = "242691"
    Case "Breaker Teuvo"
      LookingForThisABC = "231932"
      AltLookingForThisABC = "231932"
      LookingForThisAB = "231931"
      AltLookingForThisAB = "231931"
      LookingForThisC = "231929"
      AltLookingForThisC = "231929"
      LookingForThisB = "231928"
      AltLookingForThisB = "231928"
      LookingForThisA = "242731"
      AltLookingForThisA = "242731"
    Case "Brutal Rafter"
      LookingForThisABC = "231905"
      AltLookingForThisABC = "231905"
      LookingForThisAB = "231904"
      AltLookingForThisAB = "231904"
      LookingForThisC = "231902"
      AltLookingForThisC = "231902"
      LookingForThisB = "231901"
      AltLookingForThisB = "231901"
      LookingForThisA = "242729"
      AltLookingForThisA = "242729"
    Case "Brutal Soul Dredge"
      LookingForThisABC = "229203"
      AltLookingForThisABC = "231455"
      LookingForThisAB = "229202"
      AltLookingForThisAB = "231454"
      LookingForThisC = "229200"
      AltLookingForThisC = "231452"
      LookingForThisB = "229199"
      AltLookingForThisB = "231451"
      LookingForThisA = "242679"
      AltLookingForThisA = "242679"
    Case "Canceroid Cupid"
      LookingForThisABC = "239540"
      AltLookingForThisABC = "239540"
      LookingForThisAB = "239539"
      AltLookingForThisAB = "239539"
      LookingForThisC = "239537"
      AltLookingForThisC = "239537"
      LookingForThisB = "239536"
      AltLookingForThisB = "239536"
      LookingForThisA = "242801"
      AltLookingForThisA = "242801"
    Case "Captured Spirit"
      LookingForThisABC = "232480"
      AltLookingForThisABC = "234333"
      LookingForThisAB = "232479"
      AltLookingForThisAB = "234332"
      LookingForThisC = "232477"
      AltLookingForThisC = "234330"
      LookingForThisB = "232476"
      AltLookingForThisB = "234329"
      LookingForThisA = "242789"
      AltLookingForThisA = "242789"
    Case "Careening Blight"
      LookingForThisABC = "239558"
      AltLookingForThisABC = "239558"
      LookingForThisAB = "239557"
      AltLookingForThisAB = "239557"
      LookingForThisC = "239555"
      AltLookingForThisC = "239555"
      LookingForThisB = "239554"
      AltLookingForThisB = "239554"
      LookingForThisA = "242803"
      AltLookingForThisA = "242803"
    Case "Churn"
      LookingForThisABC = "232130"
      AltLookingForThisABC = "232130"
      LookingForThisAB = "232129"
      AltLookingForThisAB = "232129"
      LookingForThisC = "232127"
      AltLookingForThisC = "232127"
      LookingForThisB = "232126"
      AltLookingForThisB = "232126"
      LookingForThisA = "242753"
      AltLookingForThisA = "242753"
    Case "Circumbendibum"
      LookingForThisABC = "229194"
      AltLookingForThisABC = "231446"
      LookingForThisAB = "229193"
      AltLookingForThisAB = "231445"
      LookingForThisC = "229191"
      AltLookingForThisC = "231443"
      LookingForThisB = "229190"
      AltLookingForThisB = "231442"
      LookingForThisA = "242678"
      AltLookingForThisA = "242678"
    Case "Circumbendibus"
      LookingForThisABC = "229158"
      AltLookingForThisABC = "231410"
      LookingForThisAB = "229157"
      AltLookingForThisAB = "231409"
      LookingForThisC = "229155"
      AltLookingForThisC = "231407"
      LookingForThisB = "229154"
      AltLookingForThisB = "231406"
      LookingForThisA = "242674"
      AltLookingForThisA = "242674"
    Case "Contorted Soul Dredge"
      LookingForThisABC = "229405"
      AltLookingForThisABC = "231653"
      LookingForThisAB = "229404"
      AltLookingForThisAB = "231652"
      LookingForThisC = "231650"
      AltLookingForThisC = "231650"
      LookingForThisB = "231649"
      AltLookingForThisB = "231649"
      LookingForThisA = "242701"
      AltLookingForThisA = "242701"
    Case "Careening Death"
      LookingForThisABC = "239567"
      AltLookingForThisABC = "239567"
      LookingForThisAB = "239566"
      AltLookingForThisAB = "239566"
      LookingForThisC = "239564"
      AltLookingForThisC = "239564"
      LookingForThisB = "239563"
      AltLookingForThisB = "239563"
      LookingForThisA = "242804"
      AltLookingForThisA = "242804"
    Case "Srahir" ' Srahir
      LookingForThisABC = "231950"
      LookingForThisC = "231947"
      LookingForThisAB = "231949"
      LookingForThisB = "231946"
      LookingForThisA = "242733"
      AltLookingForThisABC = "231950"
      AltLookingForThisC = "231947"
      AltLookingForThisAB = "231949"
      AltLookingForThisB = "231946"
      AltLookingForThisA = "242733"
    Case "Ats'u"
      LookingForThisABC = "232435"
      LookingForThisC = "234283"
      LookingForThisAB = "234285"
      LookingForThisB = "232431"
      LookingForThisA = "242784"
      AltLookingForThisABC = "234286"
      AltLookingForThisC = "232432"
      AltLookingForThisAB = "232434"
      AltLookingForThisB = "234282"
      AltLookingForThisA = "242784"
    Case "Auger"
      LookingForThisABC = "229293"
      LookingForThisC = "231542"
      LookingForThisAB = "231544"
      LookingForThisB = "231541"
      LookingForThisA = "242689"
      AltLookingForThisABC = "231545"
      AltLookingForThisC = "231542"
      AltLookingForThisAB = "229292"
      AltLookingForThisB = "231541"
      AltLookingForThisA = "242689"
    Case "Bia"
      LookingForThisABC = "234212"
      LookingForThisC = "234209"
      LookingForThisAB = "234211"
      LookingForThisB = "232359"
      LookingForThisA = "242776"
      AltLookingForThisABC = "232363"
      AltLookingForThisC = "234209"
      AltLookingForThisAB = "232362"
      AltLookingForThisB = "234208"
      AltLookingForThisA = "242776"
    Case "Ashmara Ravin"
      LookingForThisABC = "229347"
      LookingForThisC = "231596"
      LookingForThisAB = "231598"
      LookingForThisB = "231595"
      LookingForThisA = "242695"
      AltLookingForThisABC = "231599"
      AltLookingForThisC = "231596"
      AltLookingForThisAB = "229346"
      AltLookingForThisB = "231595"
      AltLookingForThisA = "242695"
    Case "Black Fang"
      LookingForThisABC = "229167"
      LookingForThisC = "231416"
      LookingForThisAB = "231418"
      LookingForThisB = "229163"
      LookingForThisA = "242675"
      AltLookingForThisABC = "231419"
      AltLookingForThisC = "229164"
      AltLookingForThisAB = "229166"
      AltLookingForThisB = "231415"
      AltLookingForThisA = "242675"
    Case "Awl"
      LookingForThisABC = "229302"
      LookingForThisC = "231551"
      LookingForThisAB = "231553"
      LookingForThisB = "231550"
      LookingForThisA = "242690"
      AltLookingForThisABC = "231554"
      AltLookingForThisC = "231551"
      AltLookingForThisAB = "229301"
      AltLookingForThisB = "231550"
      AltLookingForThisA = "242690"
    Case "Bhinaji Navi"
      LookingForThisABC = "229414"
      LookingForThisC = "231659"
      LookingForThisAB = "231661"
      LookingForThisB = "231658"
      LookingForThisA = "242702"
      AltLookingForThisABC = "231662"
      AltLookingForThisC = "231659"
      AltLookingForThisAB = "229413"
      AltLookingForThisB = "231658"
      AltLookingForThisA = "242702"
    Case "Asase Ya"
      LookingForThisABC = "234315"
      LookingForThisC = "234312"
      LookingForThisAB = "234314"
      LookingForThisB = "232458"
      LookingForThisA = "242787"
      AltLookingForThisABC = "232462"
      AltLookingForThisC = "232459"
      AltLookingForThisAB = "232461"
      AltLookingForThisB = "234311"
      AltLookingForThisA = "242787"
    Case "Bellowing Chimera"
      LookingForThisABC = "229482"
      LookingForThisC = "231722"
      LookingForThisAB = "231724"
      LookingForThisB = "231721"
      LookingForThisA = "242709"
      AltLookingForThisABC = "231725"
      AltLookingForThisC = "231722"
      AltLookingForThisAB = "229481"
      AltLookingForThisB = "231721"
      AltLookingForThisA = "242709"
    Case "Bagaspati"
      LookingForThisABC = "231914"
      LookingForThisC = "231911"
      LookingForThisAB = "231913"
      LookingForThisB = "231910"
      LookingForThisA = "242730"
      AltLookingForThisABC = "231914"
      AltLookingForThisC = "231911"
      AltLookingForThisAB = "231913"
      AltLookingForThisB = "231910"
      AltLookingForThisA = "242730"
    Case "Beatific Spirit"
      LookingForThisABC = "231572"
      LookingForThisC = "231569"
      LookingForThisAB = "229319"
      LookingForThisB = "231568"
      LookingForThisA = "242692"
      AltLookingForThisABC = "229320"
      AltLookingForThisC = "231569"
      AltLookingForThisAB = "231571"
      AltLookingForThisB = "231568"
      AltLookingForThisA = "242692"
    Case "Argil Suzerain"
      LookingForThisABC = "231734"
      LookingForThisC = "231731"
      LookingForThisAB = "231733"
      LookingForThisB = "231730"
      LookingForThisA = "242710"
      AltLookingForThisABC = "229491"
      AltLookingForThisC = "231731"
      AltLookingForThisAB = "229490"
      AltLookingForThisB = "231730"
      AltLookingForThisA = "242710"
    Case "Anya"
      LookingForThisABC = "231860"
      LookingForThisC = "231857"
      LookingForThisAB = "231859"
      LookingForThisB = "231856"
      LookingForThisA = "242724"
      AltLookingForThisABC = "231860"
      AltLookingForThisC = "231857"
      AltLookingForThisAB = "231859"
      AltLookingForThisB = "231856"
      AltLookingForThisA = "242724"
    Case "Aray"
      LookingForThisABC = "231986"
      LookingForThisC = "231983"
      LookingForThisAB = "231985"
      LookingForThisB = "231982"
      LookingForThisA = "242737"
      AltLookingForThisABC = "231986"
      AltLookingForThisC = "231983"
      AltLookingForThisAB = "231985"
      AltLookingForThisB = "231982"
      AltLookingForThisA = "242737"
    Case "Anarir"
      LookingForThisABC = "239612"
      LookingForThisC = "239609"
      LookingForThisAB = "239611"
      LookingForThisB = "239608"
      LookingForThisA = "242809"
      AltLookingForThisABC = "239612"
      AltLookingForThisC = "239609"
      AltLookingForThisAB = "239611"
      AltLookingForThisB = "239608"
      AltLookingForThisA = "242809"
    Case "Alatyr"
      LookingForThisABC = "231968"
      LookingForThisC = "231965"
      LookingForThisAB = "231967"
      LookingForThisB = "231964"
      LookingForThisA = "242735"
      AltLookingForThisABC = "231968"
      AltLookingForThisC = "231965"
      AltLookingForThisAB = "231967"
      AltLookingForThisB = "231964"
      AltLookingForThisA = "242735"
    Case "Arch Bigot Lohel"
      LookingForThisABC = "234306"
      LookingForThisC = "234303"
      LookingForThisAB = "234305"
      LookingForThisB = "232449"
      LookingForThisA = "242786"
      AltLookingForThisABC = "232453"
      AltLookingForThisC = "232450"
      AltLookingForThisAB = "232452"
      AltLookingForThisB = "234302"
      AltLookingForThisA = "242786"
    Case "Ahpta"
      LookingForThisABC = "234167"
      LookingForThisC = "232315"
      LookingForThisAB = "234166"
      LookingForThisB = "234163"
      LookingForThisA = "242771"
      AltLookingForThisABC = "232318"
      AltLookingForThisC = "234164"
      AltLookingForThisAB = "232317"
      AltLookingForThisB = "232314"
      AltLookingForThisA = "242771"
    Case "Agent of Putrefaction"
      LookingForThisABC = "232282"
      LookingForThisC = "232279"
      LookingForThisAB = "234130"
      LookingForThisB = "234127"
      LookingForThisA = "242767"
      AltLookingForThisABC = "234131"
      AltLookingForThisC = "234128"
      AltLookingForThisAB = "232281"
      AltLookingForThisB = "232278"
      AltLookingForThisA = "242767"
    Case "Arch Bigot Biap"
      LookingForThisABC = "232417"
      LookingForThisC = "234263"
      LookingForThisAB = "234265"
      LookingForThisB = "232413"
      LookingForThisA = "242782"
      AltLookingForThisABC = "234268"
      AltLookingForThisC = "232414"
      AltLookingForThisAB = "232416"
      AltLookingForThisB = "234262"
      AltLookingForThisA = "242782"
    Case "Agent of Decay"
      LookingForThisABC = "234158"
      LookingForThisC = "232306"
      LookingForThisAB = "232308"
      LookingForThisB = "232305"
      LookingForThisA = "242770"
      AltLookingForThisABC = "232309"
      AltLookingForThisC = "234155"
      AltLookingForThisAB = "234157"
      AltLookingForThisB = "234154"
      AltLookingForThisA = "242770"
    Case "Aesma Daeva"
      LookingForThisABC = "232408"
      LookingForThisC = "232405"
      LookingForThisAB = "232407"
      LookingForThisB = "232404"
      LookingForThisA = "242781"
      AltLookingForThisABC = "234257"
      AltLookingForThisC = "234254"
      AltLookingForThisAB = "234256"
      AltLookingForThisB = "234253"
      AltLookingForThisA = "242781"
    Case "Adobe Suzerain"
      LookingForThisABC = "229275"
      LookingForThisC = "231524"
      LookingForThisAB = "231526"
      LookingForThisB = "231523"
      LookingForThisA = "242687"
      AltLookingForThisABC = "231527"
      AltLookingForThisC = "231524"
      AltLookingForThisAB = "229274"
      AltLookingForThisB = "231523"
      AltLookingForThisA = "242687"
    Case "Arch Bigot Aliel"
      LookingForThisABC = "232471"
      LookingForThisC = "234321"
      LookingForThisAB = "234323"
      LookingForThisB = "232467"
      LookingForThisA = "242788"
      AltLookingForThisABC = "234324"
      AltLookingForThisC = "232468"
      AltLookingForThisAB = "232470"
      AltLookingForThisB = "234320"
      AltLookingForThisA = "242788"
   End Select
    
    Do While Not (SearchForABC = txtNumberOfPatterns.Text)
      If ((PatternPieces(SearchForABC) = LookingForThisABC) Or (PatternPieces(SearchForABC) = AltLookingForThisABC)) Then
        txtPatternLocked2.Text = SearchForABC
        SearchForABC = SearchForABC + 1
        Success = 1 ' Search successful, ABC+D - We can stop.
      Else
        SearchForABC = SearchForABC + 1
       End If
    Loop
      
      If Success = 0 Then ' only search for C if ABC wasn't successful
        Do While Not (SearchForC = txtNumberOfPatterns.Text)
          If ((PatternPieces(SearchForC) = LookingForThisC) Or (PatternPieces(SearchForC) = AltLookingForThisC)) Then
            txtPatternLocked2.Text = SearchForC
            SearchForC = SearchForC + 1
            Success = 2 ' So far, D and C.  Need more.
          Else
            SearchForC = SearchForC + 1
           End If
         Loop
      End If
     
      If Success = 2 Then ' only search for AB if C was successful
        Do While Not (SearchForAB = txtNumberOfPatterns.Text)
          If ((PatternPieces(SearchForAB) = LookingForThisAB) Or (PatternPieces(SearchForAB) = AltLookingForThisAB)) Then
            txtPatternLocked3.Text = SearchForAB
            SearchForAB = SearchForAB + 1
          Success = 3 ' Search successful, AB+C+D - We can stop.
          Else
            SearchForAB = SearchForAB + 1
           End If
         Loop
      End If
     
      If Success = 2 Then ' only search for B if C succeeded
    ' if AB was successful, then it will be a number other than 2 anyhow
        Do While Not (SearchForB = txtNumberOfPatterns.Text)
          If ((PatternPieces(SearchForB) = LookingForThisB) Or (PatternPieces(SearchForB) = AltLookingForThisB)) Then
            txtPatternLocked3.Text = SearchForB
            SearchForB = SearchForB + 1
          Success = 5 ' So far, D + C + B.  Need more.
          Else
            SearchForB = SearchForB + 1
           End If
         Loop
      End If
     
      If Success = 5 Then ' only search for A if B succeeded
        Do While Not (SearchForA = txtNumberOfPatterns.Text)
          If ((PatternPieces(SearchForA) = LookingForThisA) Or (PatternPieces(SearchForA) = AltLookingForThisA)) Then
            txtPatternLocked4.Text = SearchForA
            SearchForA = SearchForA + 1
          Success = 4 ' Search successful, A+B+C+D - We can stop.
          Else
            SearchForA = SearchForA + 1
           End If
         Loop
      End If

' If we've gotten this far, and Success is still 0, then there
' was no match found for this piece.

frmMain!txtResultCode.Text = Success
End Function



'Name:      DELAY FOR AWHILE
'Purpose:      This sub loops for a passed number of seconds.
'Inputs:       sngDelay    The quantity of time to elapse.
'Note:         fractional parts of a second work!
'Assumptions:  The sub assumes that sngDelay is never greater
'              than 86400. I.e. the sub is not designed to delay
'              for longer than a day.
'
'Effects:      The calling routine's processing is
'              effectively delayed
'              by the # of seconds in the argument, sngDelay.
'
'Returns:      Nothing.
'
Public Sub subDelay(sngDelay As Single)
Const cSecondsInDay = 86400        ' # of seconds in a day.

Dim sngStart As Single             ' Start time.
Dim sngStop  As Single             ' Stop time.
Dim sngNow   As Single             ' Current time.

sngStart = Timer                   ' Get current timer.
sngStop = sngStart + sngDelay      ' Set up stop time based on
                                   ' delay.

Do
    sngNow = Timer                 ' Get current timer again.
    If sngNow < sngStart Then      ' Has midnight passed?
        sngStop = sngStart - cSecondsInDay  ' If yes, reset end.
    End If
    DoEvents                       ' Let OS process other events.

Loop While sngNow < sngStop        ' Has time elapsed?

End Sub

