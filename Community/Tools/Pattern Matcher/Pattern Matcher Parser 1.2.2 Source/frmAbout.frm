VERSION 5.00
Begin VB.Form frmAbout 
   Caption         =   "About Pattern Matcher"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4830
   Icon            =   "frmAbout.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3195
   ScaleWidth      =   4830
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      Height          =   2055
      Left            =   188
      Picture         =   "frmAbout.frx":0ECA
      ScaleHeight     =   1995
      ScaleWidth      =   1995
      TabIndex        =   1
      Top             =   240
      Width           =   2055
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Height          =   495
      Left            =   1628
      TabIndex        =   0
      Top             =   2520
      Width           =   1575
   End
   Begin VB.Label labelAboutText 
      Height          =   1935
      Left            =   2588
      TabIndex        =   2
      Top             =   240
      Width           =   2055
   End
End
Attribute VB_Name = "frmAbout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdOk_Click()
  Unload Me
End Sub

Private Sub form_load()
  labelAboutText.Caption = "Pattern Matcher v" & App.Major & "." & App.Minor & "." & App.Revision & vbCrLf & "Coded by Covenant" & vbCrLf & "(petnamer@sbcglobal.net)" & vbCrLf + "Homepage - http://www.halorn.com" & vbCrLf
  labelAboutText.Caption = labelAboutText.Caption & vbCrLf + "Anarchy Online is copyright Funcom GmbH (www.funcom.com)"

End Sub
  
