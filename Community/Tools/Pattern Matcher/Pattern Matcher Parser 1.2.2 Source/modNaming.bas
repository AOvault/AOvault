Attribute VB_Name = "modNaming"
Dim PatternMatrix(7000) As Long
Function PatternNamed(PatternToDetermine As String) As String
      
' PatternMatrix implementation is
' ABC = 1
' AB  = 2
' A   = 3
' B   = 4
' C   = 5
' D   = 6

  Dim PatternToAdd As Long
      
      Select Case PatternToDetermine
        Case 229275
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Adobe Suzerain'"
          PatternToAdd = 1001
        Case 231527
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Adobe Suzerain'"
          PatternToAdd = 1001
        Case 232408
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Aesma Daeva'"
          PatternToAdd = 1002
       Case 234257
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Aesma Daeva'"
          PatternToAdd = 1002
       Case 232309
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Agent of Decay'"
          PatternToAdd = 1003
        Case 234158
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Agent of Decay'"
          PatternToAdd = 1003
        Case 232282
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Agent of Putrefaction'"
          PatternToAdd = 1004
        Case 234131
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Agent of Putrefaction'"
          PatternToAdd = 1004
        Case 232318
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Ahpta'"
          PatternToAdd = 1005
        Case 234167
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Ahpta'"
          PatternToAdd = 1005
        Case 231968
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Alatyr'"
          PatternToAdd = 1006
        Case 232381
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Anansi"
          PatternToAdd = 1007
        Case 234230
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Anansi"
          PatternToAdd = 1007
        Case 232516
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Anansi'"
          PatternToAdd = 1007
        Case 234369
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Anansi'"
          PatternToAdd = 1007
        Case 239612
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Anarir'"
          PatternToAdd = 1008
        Case 231860
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Anya'"
          PatternToAdd = 1009
        Case 231986
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Aray'"
          PatternToAdd = 1010
        Case 232471
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Arch Bigot Aliel'"
          PatternToAdd = 1011
        Case 234324
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Arch Bigot Aliel'"
          PatternToAdd = 1011
        Case 232417
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Arch Bigot Biap'"
          PatternToAdd = 1012
        Case 234268
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Arch Bigot Biap'"
          PatternToAdd = 1012
        Case 232453
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Arch Bigot Lohel'"
          PatternToAdd = 1013
        Case 234306
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Arch Bigot Lohel'"
          PatternToAdd = 1013
        Case 232642
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Arch Demon of Inferno'"
          PatternToAdd = 1014
        Case 229491
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Argil Suzerain'"
          PatternToAdd = 1015
        Case 231734
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Argil Suzerain'"
          PatternToAdd = 1015
        Case 232462
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Asase Ya'"
          PatternToAdd = 1016
        Case 234315
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Asase Ya'"
          PatternToAdd = 1016
        Case 229347
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Ashmara Ravin'"
          PatternToAdd = 1017
        Case 231599
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Ashmara Ravin'"
          PatternToAdd = 1017
        Case 232435
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Ats'u"
          PatternToAdd = 1018
        Case 234286
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Ats'u"
          PatternToAdd = 1018
        Case 229293
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Auger'"
          PatternToAdd = 1019
        Case 231545
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Auger'"
          PatternToAdd = 1019
        Case 229302
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Awl'"
          PatternToAdd = 1020
        Case 231554
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Awl'"
          PatternToAdd = 1020
        Case 231914
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Bagaspati'"
          PatternToAdd = 1021
        Case 229320
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Beatific Spirit'"
          PatternToAdd = 1022
        Case 231572
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Beatific Spirit'"
          PatternToAdd = 1022
        Case 229482
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Bellowing Chimera'"
          PatternToAdd = 1023
        Case 231725
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Bellowing Chimera'"
          PatternToAdd = 1023
        Case 229414
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Bhinaji Navi'"
          PatternToAdd = 1024
        Case 231662
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Bhinaji Navi'"
          PatternToAdd = 1024
        Case 232363
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Bia'"
          PatternToAdd = 1025
        Case 234212
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Bia'"
          PatternToAdd = 1025
        Case 229167
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Black Fang'"
          PatternToAdd = 1026
        Case 231419
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Black Fang'"
          PatternToAdd = 1026
        Case 229374
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Blight'"
          PatternToAdd = 1027
        Case 231626
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Blight'"
          PatternToAdd = 1027
        Case 229311
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Borer'"
          PatternToAdd = 1028
        Case 231563
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Borer'"
          PatternToAdd = 1028
        Case 231932
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Breaker Teuvo'"
          PatternToAdd = 1029
        Case 231905
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Brutal Rafter'"
          PatternToAdd = 1030
        Case 229203
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Brutal Soul Dredge'"
          PatternToAdd = 1031
        Case 231455
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Brutal Soul Dredge'"
          PatternToAdd = 1031
        Case 232597
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Cama'"
          PatternToAdd = 1032
        Case 239540
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Canceroid Cupid'"
          PatternToAdd = 1033
        Case 232480
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Captured Spirit'"
          PatternToAdd = 1034
        Case 234333
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Captured Spirit'"
          PatternToAdd = 1034
        Case 239558
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Careening Blight'"
          PatternToAdd = 1035
        Case 239567
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Careening Death'"
          PatternToAdd = 1036
        Case 232130
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Churn'"
          PatternToAdd = 1037
        Case 229194
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Circumbendibum'"
          PatternToAdd = 1038
        Case 231446
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Circumbendibum'"
          PatternToAdd = 1038
        Case 229158
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Circumbendibus'"
          PatternToAdd = 1039
        Case 231410
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Circumbendibus'"
          PatternToAdd = 1039
        Case 229405
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Contorted Soul Dredge'"
          PatternToAdd = 1040
        Case 231653
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Contorted Soul Dredge'"
          PatternToAdd = 1040
        Case 232570
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Dalja'"
          PatternToAdd = 1041
        Case 229581
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Defiler of Scheol'"
          PatternToAdd = 1042
        Case 231824
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Defiler of Scheol'"
          PatternToAdd = 1042
        Case 229599
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Destroyer of Scheol'"
          PatternToAdd = 1043
        Case 231842
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Destroyer of Scheol'"
          PatternToAdd = 1043
        Case 229590
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Devastator of Scheol'"
          PatternToAdd = 1044
        Case 231833
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Devastator of Scheol'"
          PatternToAdd = 1044
        Case 229572
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Devourer of Scheol'"
          PatternToAdd = 1045
        Case 231815
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Devourer of Scheol'"
          PatternToAdd = 1045
        Case 232588
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Diviner Gil Kald-Thar'"
          PatternToAdd = 1046
        Case 242533
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Diviner Gil Kald-Thar'"
          PatternToAdd = 1046
        Case 239639
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Eidolean Soul Dredge'"
          PatternToAdd = 1047
        Case 232534
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Empath Min-Ji Liu'"
          PatternToAdd = 1048
        Case 239685
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Exsequiae'"
          PatternToAdd = 1049
        Case 232300
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Fester Leila'"
          PatternToAdd = 1050
        Case 234149
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Fester Leila'"
          PatternToAdd = 1050
        Case 229441
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Flinty'"
          PatternToAdd = 1051
       Case 231689
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Flinty'"
          PatternToAdd = 1051
        Case 239648
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Glitter'"
          PatternToAdd = 1052
        Case 229248
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Gracious Soul Dredge'"
          PatternToAdd = 1053
        Case 231500
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Gracious Soul Dredge'"
          PatternToAdd = 1053
        Case 229463
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Gunk'"
          PatternToAdd = 1054
        Case 231707
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Gunk'"
          PatternToAdd = 1054
        Case 231995
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Hadur'"
          PatternToAdd = 1055
        Case 232336
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Haqa'"
          PatternToAdd = 1056
        Case 234185
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Haqa'"
          PatternToAdd = 1056
        Case 229383
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Hemut'"
          PatternToAdd = 1057
        Case 231635
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Hemut'"
          PatternToAdd = 1057
        Case 239675
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Ho'"
          PatternToAdd = 1058
        Case 229266
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Ignis Fatui'"
          PatternToAdd = 1059
        Case 231518
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Ignis Fatui'"
          PatternToAdd = 1059
        Case 229329
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Ignis Fatuus'"
          PatternToAdd = 1060
        Case 231581
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Ignis Fatuus'"
          PatternToAdd = 1060
        Case 232004
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Imk'a"
          PatternToAdd = 1061
        Case 232579
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Infernal Demon'"
          PatternToAdd = 1062
        Case 229284
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Iunmin'"
          PatternToAdd = 1063
        Case 231536
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Iunmin'"
          PatternToAdd = 1063
        Case 232013
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Juma'"
          PatternToAdd = 1064
        Case 232229
          PatternNamed = "Aban-Bhotar-Chi Assembly 'K'a"
          PatternToAdd = 1065
        Case 234104
          PatternNamed = "Aban-Bhotar-Chi Assembly 'K'a"
          PatternToAdd = 1065
        Case 232022
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Kaleva'"
          PatternToAdd = 1066
        Case 229527
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Kaoline Suzerain'"
          PatternToAdd = 1067
        Case 231770
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Kaoline Suzerain'"
          PatternToAdd = 1067
        Case 229392
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Khemhet'"
          PatternToAdd = 1068
        Case 231644
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Khemhet'"
          PatternToAdd = 1068
        Case 232633
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Lady Genevra Di�Venague'"
          PatternToAdd = 1069
        Case 229356
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Lethargic Spirit'"
          PatternToAdd = 1070
        Case 231608
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Lethargic Spirit'"
          PatternToAdd = 1070
        Case 229500
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Loessial Suzerain'"
          PatternToAdd = 1071
        Case 231743
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Loessial Suzerain'"
          PatternToAdd = 1071
        Case 239666
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Loltonunon'"
          PatternToAdd = 1072
        Case 232076
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Lurky'"
          PatternToAdd = 1073
        Case 232372
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Lya'"
          PatternToAdd = 1074
        Case 234221
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Lya'"
          PatternToAdd = 1074
        Case 239531
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Malah-Animus'"
          PatternToAdd = 1075
        Case 239522
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Malah-At'"
          PatternToAdd = 1076
        Case 239513
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Malah-Auris'"
          PatternToAdd = 1077
        Case 239576
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Maledicta'"
          PatternToAdd = 1078
        Case 229338
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Marem'"
          PatternToAdd = 1079
        Case 231590
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Marem'"
          PatternToAdd = 1079
        Case 229423
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Marly Suzerain'"
          PatternToAdd = 1080
        Case 231671
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Marly Suzerain'"
          PatternToAdd = 1080
        Case 239594
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Mawi'"
          PatternToAdd = 1081
        Case 229365
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Misery'"
          PatternToAdd = 1082
        Case 231617
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Misery'"
          PatternToAdd = 1082
        Case 232094
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Moochy'"
          PatternToAdd = 1083
        Case 231887
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Morrow'"
          PatternToAdd = 1084
        Case 239486
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Nyame'"
          PatternToAdd = 1085
        Case 232543
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Ocra'"
          PatternToAdd = 1086
        Case 232525
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Odqan'"
          PatternToAdd = 1087
        Case 234381
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Odqan'"
          PatternToAdd = 1087
        Case 232031
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Old Salty'"
          PatternToAdd = 1088
        Case 229509
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Ooze'"
          PatternToAdd = 1089
        Case 231752
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Ooze'"
          PatternToAdd = 1089
        Case 232399
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Pazuzu'"
          PatternToAdd = 1090
        Case 234248
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Pazuzu'"
          PatternToAdd = 1090
        Case 232121
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Quake'"
          PatternToAdd = 1091
        Case 231878
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Quondam'"
          PatternToAdd = 1092
        Case 232444
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Rallies Fete'"
          PatternToAdd = 1093
        Case 234297
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Rallies Fete'"
          PatternToAdd = 1093
        Case 232498
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Razor the Battletoad'"
          PatternToAdd = 1094
        Case 234351
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Razor the Battletoad'"
          PatternToAdd = 1094
        Case 242542
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Redeemed Cama'"
          PatternToAdd = 1095
        Case 232561
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Redeemed Gilthar'"
          PatternToAdd = 1096
        Case 242515
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Redeemed Gilthar'"
          PatternToAdd = 1096
        Case 232615
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Redeemed Lord Galahad'"
          PatternToAdd = 1097
        Case 234437
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Redeemed Lord Galahad'"
          PatternToAdd = 1097
        Case 242497
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Redeemed Ocra'"
          PatternToAdd = 1098
        Case 232273
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Relief Teals'"
          PatternToAdd = 1099
        Case 234122
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Relief Teals'"
          PatternToAdd = 1099
        Case 232552
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Roch'"
          PatternToAdd = 1100
        Case 239549
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Sabretooth Slicer'"
          PatternToAdd = 1101
        Case 231851
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Sampsa'"
          PatternToAdd = 1102
        Case 232354
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Sasabonsam'"
          PatternToAdd = 1103
        Case 234203
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Sasabonsam'"
          PatternToAdd = 1103
        Case 232220
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Sashu'"
          PatternToAdd = 1104
        Case 234095
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Sashu'"
          PatternToAdd = 1104
        Case 239657
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Satkamear'"
          PatternToAdd = 1105
        Case 239603
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Sawi'"
          PatternToAdd = 1106
        Case 229176
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Scratch'"
          PatternToAdd = 1107
        Case 231428
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Scratch'"
          PatternToAdd = 1107
        Case 229185
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Screech'"
          PatternToAdd = 1108
        Case 231437
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Screech'"
          PatternToAdd = 1108
        Case 232202
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Shiver'"
          PatternToAdd = 1110
        Case 232507
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Shullat'"
          PatternToAdd = 1111
        Case 234360
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Shullat'"
          PatternToAdd = 1111
        Case 229149
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Silver Fang'"
          PatternToAdd = 1112
        Case 231401
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Silver Fang'"
          PatternToAdd = 1112
        Case 232085
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Skulky'"
          PatternToAdd = 1113
        Case 232067
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Slinky'"
          PatternToAdd = 1114
        Case 232040
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Smee'"
          PatternToAdd = 1115
        Case 229257
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Spiritless Soul Dredge'"
          PatternToAdd = 1116
        Case 231509
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Spiritless Soul Dredge'"
          PatternToAdd = 1116
        Case 231950
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Srahir'"
          PatternToAdd = 1117
        Case 232345
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Taille Frees'"
          PatternToAdd = 1118
        Case 234194
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Taille Frees'"
          PatternToAdd = 1118
        Case 239703
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Tcheser'"
          PatternToAdd = 1119
        Case 239495
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Abysmal Lord'"
          PatternToAdd = 1120
        Case 232193
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Abyssal Widow'"
          PatternToAdd = 1121
        Case 239694
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Achbile Guardian'"
          PatternToAdd = 1122
        Case 232148
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Adonian Soul Dredge'"
          PatternToAdd = 1123
        Case 232175
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Adonis Spirit Master'"
          PatternToAdd = 1124
        Case 229536
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Archbile Queen'"
          PatternToAdd = 1125
        Case 231779
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Archbile Queen'"
          PatternToAdd = 1125
        Case 239621
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Brobdingnagian Mother'"
          PatternToAdd = 1126
        Case 232166
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Dredge Driver'"
          PatternToAdd = 1127
        Case 232390
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Dryad Demigod'"
          PatternToAdd = 1128
        Case 234239
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Dryad Demigod'"
          PatternToAdd = 1128
        Case 231941
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Dryad Shuffle'"
          PatternToAdd = 1129
        Case 229212
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Dune Suzerain'"
          PatternToAdd = 1130
        Case 231464
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Dune Suzerain'"
          PatternToAdd = 1130
        Case 229239
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Elysian Soul Dredge'"
          PatternToAdd = 1131
        Case 231491
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Elysian Soul Dredge'"
          PatternToAdd = 1131
        Case 229563
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Enrapt One'"
          PatternToAdd = 1132
        Case 231806
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Enrapt One'"
          PatternToAdd = 1132
        Case 239721
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Great Ice Golem'"
          PatternToAdd = 1133
        Case 232426
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Indomitable Chimera'"
          PatternToAdd = 1134
        Case 234277
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Indomitable Chimera'"
          PatternToAdd = 1134
        Case 239504
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The  Infernal Soul Dredge'"
          PatternToAdd = 1135
        Case 242455
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Infernal Soul Dredge'"
          PatternToAdd = 1136
        Case 231923
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Maggot Lord'"
          PatternToAdd = 1137
        Case 231869
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Maggot Lord'"
          PatternToAdd = 1137
        Case 232291
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Mortificator'"
          PatternToAdd = 1138
        Case 234140
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Mortificator'"
          PatternToAdd = 1138
        Case 229554
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Numb One'"
          PatternToAdd = 1139
        Case 231797
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Numb One'"
          PatternToAdd = 1139
        Case 231896
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Penumbral Spirit Hunter'"
          PatternToAdd = 1140
        Case 232058
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Peristaltic Abomination'"
          PatternToAdd = 1141
        Case 232049
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Peristaltic Aversion'"
          PatternToAdd = 1167
        Case 232184
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Proprietrix'"
          PatternToAdd = 1142
        Case 229518
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Scheolian Soul Dredge'"
          PatternToAdd = 1143
        Case 231761
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Scheolian Soul Dredge'"
          PatternToAdd = 1143
        Case 239630
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Stupendous Breeder'"
          PatternToAdd = 1144
        Case 229221
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Talus Suzerain'"
          PatternToAdd = 1145
        Case 231473
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Talus Suzerain'"
          PatternToAdd = 1145
        Case 232157
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Watchdog'"
          PatternToAdd = 1146
        Case 231977
          PatternNamed = "Aban-Bhotar-Chi Assembly 'The Worm King'"
          PatternToAdd = 1147
        Case 229472
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Thunderous Chimera'"
          PatternToAdd = 1148
        Case 231716
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Thunderous Chimera'"
          PatternToAdd = 1148
        Case 232112
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Toss'"
          PatternToAdd = 1149
        Case 229432
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Tough'"
          PatternToAdd = 1150
        Case 231680
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Tough'"
          PatternToAdd = 1150
        Case 229230
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Ungulera'"
          PatternToAdd = 1151
        Case 231482
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Ungulera'"
          PatternToAdd = 1151
        Case 242524
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Unredeemed Dalja'"
          PatternToAdd = 1152
        Case 232624
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Unredeemed Lord Mordeth'"
          PatternToAdd = 1153
        Case 234448
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Unredeemed Lord Mordeth'"
          PatternToAdd = 1153
        Case 242506
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Unredeemed Roch'"
          PatternToAdd = 1154
        Case 242551
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Unredeemed Vanya'"
          PatternToAdd = 1155
        Case 239712
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Upenpet'"
          PatternToAdd = 1156
        Case 232327
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Ushqa'"
          PatternToAdd = 1157
        Case 234176
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Ushqa'"
          PatternToAdd = 1157
        Case 232606
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Vanya'"
          PatternToAdd = 1158
        Case 232139
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Viscious Visitant'"
          PatternToAdd = 1159
        Case 229450
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Wacky Suzerain'"
          PatternToAdd = 1160
        Case 231698
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Wacky Suzerain'"
          PatternToAdd = 1160
        Case 239585
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Wala'"
          PatternToAdd = 1161
        Case 234113
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Waqa'"
          PatternToAdd = 1162
        Case 242657
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Weary Empath Min-Ji Liu'"
          PatternToAdd = 1163
        Case 229545
          PatternNamed = "Aban-Bhotar-Chi Assembly 'White'"
          PatternToAdd = 1164
        Case 231788
          PatternNamed = "Aban-Bhotar-Chi Assembly 'White'"
          PatternToAdd = 1164
        Case 232489
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Xark the Battletoad'"
          PatternToAdd = 1165
        Case 234342
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Xark the Battletoad'"
          PatternToAdd = 1165
        Case 231959
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Zoetic Oak'"
          PatternToAdd = 1166
        Case 229274
          PatternNamed = "Aban-Bhotar Assembly of 'Adobe Suzerain'"
          PatternToAdd = 2001
        Case 231526
          PatternNamed = "Aban-Bhotar Assembly of 'Adobe Suzerain'"
          PatternToAdd = 2001
        Case 232407
          PatternNamed = "Aban-Bhotar Assembly of 'Aesma Daeva'"
          PatternToAdd = 2002
        Case 234256
          PatternNamed = "Aban-Bhotar Assembly of 'Aesma Daeva'"
          PatternToAdd = 2002
        Case 232308
          PatternNamed = "Aban-Bhotar Assembly of 'Agent of Decay'"
          PatternToAdd = 2003
        Case 234157
          PatternNamed = "Aban-Bhotar Assembly of 'Agent of Decay'"
          PatternToAdd = 2003
        Case 232281
          PatternNamed = "Aban-Bhotar Assembly of 'Agent of Putrefaction'"
          PatternToAdd = 2004
        Case 234130
          PatternNamed = "Aban-Bhotar Assembly of 'Agent of Putrefaction'"
          PatternToAdd = 2004
        Case 232317
          PatternNamed = "Aban-Bhotar Assembly of 'Ahpta'"
          PatternToAdd = 2005
        Case 234166
          PatternNamed = "Aban-Bhotar Assembly of 'Ahpta'"
          PatternToAdd = 2005
        Case 231967
          PatternNamed = "Aban-Bhotar Assembly of 'Alatyr'"
          PatternToAdd = 2006
        Case 232380
          PatternNamed = "Aban-Bhotar Assembly of 'Anansi"
          PatternToAdd = 2007
        Case 234229
          PatternNamed = "Aban-Bhotar Assembly of 'Anansi"
          PatternToAdd = 2007
        Case 232515
          PatternNamed = "Aban-Bhotar Assembly of 'Anansi'"
          PatternToAdd = 2007
        Case 234368
          PatternNamed = "Aban-Bhotar Assembly of 'Anansi'"
          PatternToAdd = 2007
        Case 239611
          PatternNamed = "Aban-Bhotar Assembly of 'Anarir'"
          PatternToAdd = 2008
        Case 231859
          PatternNamed = "Aban-Bhotar Assembly of 'Anya'"
          PatternToAdd = 2009
        Case 231985
          PatternNamed = "Aban-Bhotar Assembly of 'Aray'"
          PatternToAdd = 2010
        Case 232470
          PatternNamed = "Aban-Bhotar Assembly of 'Arch Bigot Aliel'"
          PatternToAdd = 2011
        Case 234323
          PatternNamed = "Aban-Bhotar Assembly of 'Arch Bigot Aliel'"
          PatternToAdd = 2011
        Case 232416
          PatternNamed = "Aban-Bhotar Assembly of 'Arch Bigot Biap'"
          PatternToAdd = 2012
        Case 234265
          PatternNamed = "Aban-Bhotar Assembly of 'Arch Bigot Biap'"
          PatternToAdd = 2012
        Case 232452
          PatternNamed = "Aban-Bhotar Assembly of 'Arch Bigot Lohel'"
          PatternToAdd = 2013
        Case 234305
          PatternNamed = "Aban-Bhotar Assembly of 'Arch Bigot Lohel'"
          PatternToAdd = 2013
        Case 232641
          PatternNamed = "Aban-Bhotar Assembly of 'Arch Demon of Inferno'"
          PatternToAdd = 2014
        Case 229490
          PatternNamed = "Aban-Bhotar Assembly of 'Argil Suzerain'"
          PatternToAdd = 2015
        Case 231733
          PatternNamed = "Aban-Bhotar Assembly of 'Argil Suzerain'"
          PatternToAdd = 2015
        Case 232461
          PatternNamed = "Aban-Bhotar Assembly of 'Asase Ya'"
          PatternToAdd = 2016
        Case 234314
          PatternNamed = "Aban-Bhotar Assembly of 'Asase Ya'"
          PatternToAdd = 2016
        Case 229346
          PatternNamed = "Aban-Bhotar Assembly of 'Ashmara Ravin'"
          PatternToAdd = 2017
        Case 231598
          PatternNamed = "Aban-Bhotar Assembly of 'Ashmara Ravin'"
          PatternToAdd = 2017
        Case 232434
          PatternNamed = "Aban-Bhotar Assembly of 'Ats'u"
          PatternToAdd = 2018
        Case 234285
          PatternNamed = "Aban-Bhotar Assembly of 'Ats'u"
          PatternToAdd = 2018
        Case 229292
          PatternNamed = "Aban-Bhotar Assembly of 'Auger'"
          PatternToAdd = 2019
        Case 231544
          PatternNamed = "Aban-Bhotar Assembly of 'Auger'"
          PatternToAdd = 2019
        Case 229301
          PatternNamed = "Aban-Bhotar Assembly of 'Awl'"
          PatternToAdd = 2020
        Case 231553
          PatternNamed = "Aban-Bhotar Assembly of 'Awl'"
          PatternToAdd = 2020
        Case 231913
          PatternNamed = "Aban-Bhotar Assembly of 'Bagaspati'"
          PatternToAdd = 2021
        Case 229319
          PatternNamed = "Aban-Bhotar Assembly of 'Beatific Spirit'"
          PatternToAdd = 2022
        Case 231571
          PatternNamed = "Aban-Bhotar Assembly of 'Beatific Spirit'"
          PatternToAdd = 2022
        Case 229481
          PatternNamed = "Aban-Bhotar Assembly of 'Bellowing Chimera'"
          PatternToAdd = 2023
        Case 231724
          PatternNamed = "Aban-Bhotar Assembly of 'Bellowing Chimera'"
          PatternToAdd = 2023
        Case 229413
          PatternNamed = "Aban-Bhotar Assembly of 'Bhinaji Navi'"
          PatternToAdd = 2024
        Case 231661
          PatternNamed = "Aban-Bhotar Assembly of 'Bhinaji Navi'"
          PatternToAdd = 2024
        Case 232362
          PatternNamed = "Aban-Bhotar Assembly of 'Bia'"
          PatternToAdd = 2025
        Case 234211
          PatternNamed = "Aban-Bhotar Assembly of 'Bia'"
          PatternToAdd = 2025
        Case 229166
          PatternNamed = "Aban-Bhotar Assembly of 'Black Fang'"
          PatternToAdd = 2026
        Case 231418
          PatternNamed = "Aban-Bhotar Assembly of 'Black Fang'"
          PatternToAdd = 2026
        Case 229373
          PatternNamed = "Aban-Bhotar Assembly of 'Blight'"
          PatternToAdd = 2027
        Case 231625
          PatternNamed = "Aban-Bhotar Assembly of 'Blight'"
          PatternToAdd = 2027
        Case 229310
          PatternNamed = "Aban-Bhotar Assembly of 'Borer'"
          PatternToAdd = 2028
        Case 231562
          PatternNamed = "Aban-Bhotar Assembly of 'Borer'"
          PatternToAdd = 2028
        Case 231931
          PatternNamed = "Aban-Bhotar Assembly of 'Breaker Teuvo'"
          PatternToAdd = 2029
        Case 231904
          PatternNamed = "Aban-Bhotar Assembly of 'Brutal Rafter'"
          PatternToAdd = 2030
        Case 229202
          PatternNamed = "Aban-Bhotar Assembly of 'Brutal Soul Dredge'"
          PatternToAdd = 2031
        Case 231454
          PatternNamed = "Aban-Bhotar Assembly of 'Brutal Soul Dredge'"
          PatternToAdd = 2031
        Case 232596
          PatternNamed = "Aban-Bhotar Assembly of 'Cama'"
          PatternToAdd = 2032
        Case 239539
          PatternNamed = "Aban-Bhotar Assembly of 'Canceroid Cupid'"
          PatternToAdd = 2033
        Case 232479
          PatternNamed = "Aban-Bhotar Assembly of 'Captured Spirit'"
          PatternToAdd = 2034
        Case 234332
          PatternNamed = "Aban-Bhotar Assembly of 'Captured Spirit'"
          PatternToAdd = 2034
        Case 239557
          PatternNamed = "Aban-Bhotar Assembly of 'Careening Blight'"
          PatternToAdd = 2035
        Case 239566
          PatternNamed = "Aban-Bhotar Assembly of 'Careening Death'"
          PatternToAdd = 2036
        Case 232129
          PatternNamed = "Aban-Bhotar Assembly of 'Churn'"
          PatternToAdd = 2037
        Case 229193
          PatternNamed = "Aban-Bhotar Assembly of 'Circumbendibum'"
          PatternToAdd = 2038
        Case 231445
          PatternNamed = "Aban-Bhotar Assembly of 'Circumbendibum'"
          PatternToAdd = 2038
        Case 229157
          PatternNamed = "Aban-Bhotar Assembly of 'Circumbendibus'"
          PatternToAdd = 2039
        Case 231409
          PatternNamed = "Aban-Bhotar Assembly of 'Circumbendibus'"
          PatternToAdd = 2039
        Case 229404
          PatternNamed = "Aban-Bhotar Assembly of 'Contorted Soul Dredge'"
          PatternToAdd = 2040
        Case 231652
          PatternNamed = "Aban-Bhotar Assembly of 'Contorted Soul Dredge'"
          PatternToAdd = 2040
        Case 232569
          PatternNamed = "Aban-Bhotar Assembly of 'Dalja'"
          PatternToAdd = 2041
        Case 229580
          PatternNamed = "Aban-Bhotar Assembly of 'Defiler of Scheol'"
          PatternToAdd = 2042
        Case 231823
          PatternNamed = "Aban-Bhotar Assembly of 'Defiler of Scheol'"
          PatternToAdd = 2042
        Case 229598
          PatternNamed = "Aban-Bhotar Assembly of 'Destroyer of Scheol'"
          PatternToAdd = 2043
        Case 231841
          PatternNamed = "Aban-Bhotar Assembly of 'Destroyer of Scheol'"
          PatternToAdd = 2043
        Case 229589
          PatternNamed = "Aban-Bhotar Assembly of 'Devastator of Scheol'"
          PatternToAdd = 2044
        Case 231832
          PatternNamed = "Aban-Bhotar Assembly of 'Devastator of Scheol'"
          PatternToAdd = 2044
        Case 229571
          PatternNamed = "Aban-Bhotar Assembly of 'Devourer of Scheol'"
          PatternToAdd = 2045
        Case 231814
          PatternNamed = "Aban-Bhotar Assembly of 'Devourer of Scheol'"
          PatternToAdd = 2045
        Case 232587
          PatternNamed = "Aban-Bhotar Assembly of 'Diviner Gil Kald-Thar'"
          PatternToAdd = 2046
        Case 242532
          PatternNamed = "Aban-Bhotar Assembly of 'Diviner Gil Kald-Thar'"
          PatternToAdd = 2046
        Case 239638
          PatternNamed = "Aban-Bhotar Assembly of 'Eidolean Soul Dredge'"
          PatternToAdd = 2047
        Case 232533
          PatternNamed = "Aban-Bhotar Assembly of 'Empath Min-Ji Liu'"
          PatternToAdd = 2048
        Case 239684
          PatternNamed = "Aban-Bhotar Assembly of 'Exsequiae'"
          PatternToAdd = 2049
        Case 232299
          PatternNamed = "Aban-Bhotar Assembly of 'Fester Leila'"
          PatternToAdd = 2050
        Case 234148
          PatternNamed = "Aban-Bhotar Assembly of 'Fester Leila'"
          PatternToAdd = 2050
        Case 229440
          PatternNamed = "Aban-Bhotar Assembly of 'Flinty'"
          PatternToAdd = 2051
        Case 231688
          PatternNamed = "Aban-Bhotar Assembly of 'Flinty'"
          PatternToAdd = 2051
        Case 239647
          PatternNamed = "Aban-Bhotar Assembly of 'Glitter'"
          PatternToAdd = 2052
        Case 229247
          PatternNamed = "Aban-Bhotar Assembly of 'Gracious Soul Dredge'"
          PatternToAdd = 2053
        Case 231499
          PatternNamed = "Aban-Bhotar Assembly of 'Gracious Soul Dredge'"
          PatternToAdd = 2053
        Case 229462
          PatternNamed = "Aban-Bhotar Assembly of 'Gunk'"
          PatternToAdd = 2054
        Case 231706
          PatternNamed = "Aban-Bhotar Assembly of 'Gunk'"
          PatternToAdd = 2054
        Case 231994
          PatternNamed = "Aban-Bhotar Assembly of 'Hadur'"
          PatternToAdd = 2055
        Case 232335
          PatternNamed = "Aban-Bhotar Assembly of 'Haqa'"
          PatternToAdd = 2056
        Case 234184
          PatternNamed = "Aban-Bhotar Assembly of 'Haqa'"
          PatternToAdd = 2056
        Case 229382
          PatternNamed = "Aban-Bhotar Assembly of 'Hemut'"
          PatternToAdd = 2057
        Case 231634
          PatternNamed = "Aban-Bhotar Assembly of 'Hemut'"
          PatternToAdd = 2057
        Case 239674
          PatternNamed = "Aban-Bhotar Assembly of 'Ho'"
          PatternToAdd = 2058
        Case 229265
          PatternNamed = "Aban-Bhotar Assembly of 'Ignis Fatui'"
          PatternToAdd = 2059
        Case 231517
          PatternNamed = "Aban-Bhotar Assembly of 'Ignis Fatui'"
          PatternToAdd = 2059
        Case 229328
          PatternNamed = "Aban-Bhotar Assembly of 'Ignis Fatuus'"
          PatternToAdd = 2060
        Case 231580
          PatternNamed = "Aban-Bhotar Assembly of 'Ignis Fatuus'"
          PatternToAdd = 2060
        Case 232003
          PatternNamed = "Aban-Bhotar Assembly of 'Imk'a"
          PatternToAdd = 2061
        Case 232578
          PatternNamed = "Aban-Bhotar Assembly of 'Infernal Demon'"
          PatternToAdd = 2062
        Case 229283
          PatternNamed = "Aban-Bhotar Assembly of 'Iunmin'"
          PatternToAdd = 2063
        Case 231535
          PatternNamed = "Aban-Bhotar Assembly of 'Iunmin'"
          PatternToAdd = 2063
        Case 232012
          PatternNamed = "Aban-Bhotar Assembly of 'Juma'"
          PatternToAdd = 2064
        Case 232228
          PatternNamed = "Aban-Bhotar Assembly of 'K'a"
          PatternToAdd = 2065
        Case 234103
          PatternNamed = "Aban-Bhotar Assembly of 'K'a"
          PatternToAdd = 2065
        Case 232021
          PatternNamed = "Aban-Bhotar Assembly of 'Kaleva'"
          PatternToAdd = 2066
        Case 229526
          PatternNamed = "Aban-Bhotar Assembly of 'Kaoline Suzerain'"
          PatternToAdd = 2067
        Case 231769
          PatternNamed = "Aban-Bhotar Assembly of 'Kaoline Suzerain'"
          PatternToAdd = 2067
        Case 229391
          PatternNamed = "Aban-Bhotar Assembly of 'Khemhet'"
          PatternToAdd = 2068
        Case 231643
          PatternNamed = "Aban-Bhotar Assembly of 'Khemhet'"
          PatternToAdd = 2068
        Case 232632
          PatternNamed = "Aban-Bhotar Assembly of 'Lady Genevra Di�Venague'"
          PatternToAdd = 2069
        Case 229355
          PatternNamed = "Aban-Bhotar Assembly of 'Lethargic Spirit'"
          PatternToAdd = 2070
        Case 231607
          PatternNamed = "Aban-Bhotar Assembly of 'Lethargic Spirit'"
          PatternToAdd = 2070
        Case 229499
          PatternNamed = "Aban-Bhotar Assembly of 'Loessial Suzerain'"
          PatternToAdd = 2071
        Case 231742
          PatternNamed = "Aban-Bhotar Assembly of 'Loessial Suzerain'"
          PatternToAdd = 2071
        Case 239665
          PatternNamed = "Aban-Bhotar Assembly of 'Loltonunon'"
          PatternToAdd = 2072
        Case 232075
          PatternNamed = "Aban-Bhotar Assembly of 'Lurky'"
          PatternToAdd = 2073
        Case 232371
          PatternNamed = "Aban-Bhotar Assembly of 'Lya'"
          PatternToAdd = 2074
        Case 234220
          PatternNamed = "Aban-Bhotar Assembly of 'Lya'"
          PatternToAdd = 2074
        Case 239530
          PatternNamed = "Aban-Bhotar Assembly of 'Malah-Animus'"
          PatternToAdd = 2075
        Case 239521
          PatternNamed = "Aban-Bhotar Assembly of 'Malah-At'"
          PatternToAdd = 2076
        Case 239512
          PatternNamed = "Aban-Bhotar Assembly of 'Malah-Auris'"
          PatternToAdd = 2077
        Case 239575
          PatternNamed = "Aban-Bhotar Assembly of 'Maledicta'"
          PatternToAdd = 2078
        Case 229337
          PatternNamed = "Aban-Bhotar Assembly of 'Marem'"
          PatternToAdd = 2079
        Case 231589
          PatternNamed = "Aban-Bhotar Assembly of 'Marem'"
          PatternToAdd = 2079
       Case 229422
          PatternNamed = "Aban-Bhotar Assembly of 'Marly Suzerain'"
          PatternToAdd = 2080
        Case 231670
          PatternNamed = "Aban-Bhotar Assembly of 'Marly Suzerain'"
          PatternToAdd = 2080
        Case 239593
          PatternNamed = "Aban-Bhotar Assembly of 'Mawi'"
          PatternToAdd = 2081
        Case 229364
          PatternNamed = "Aban-Bhotar Assembly of 'Misery'"
          PatternToAdd = 2082
        Case 231616
          PatternNamed = "Aban-Bhotar Assembly of 'Misery'"
          PatternToAdd = 2082
        Case 232093
          PatternNamed = "Aban-Bhotar Assembly of 'Moochy'"
          PatternToAdd = 2083
        Case 231886
          PatternNamed = "Aban-Bhotar Assembly of 'Morrow'"
          PatternToAdd = 2084
        Case 239485
          PatternNamed = "Aban-Bhotar Assembly of 'Nyame'"
          PatternToAdd = 2085
        Case 232542
          PatternNamed = "Aban-Bhotar Assembly of 'Ocra'"
          PatternToAdd = 2086
        Case 232524
          PatternNamed = "Aban-Bhotar Assembly of 'Odqan'"
          PatternToAdd = 2087
        Case 234379
          PatternNamed = "Aban-Bhotar Assembly of 'Odqan'"
          PatternToAdd = 2087
        Case 232030
          PatternNamed = "Aban-Bhotar Assembly of 'Old Salty'"
          PatternToAdd = 2088
        Case 229508
          PatternNamed = "Aban-Bhotar Assembly of 'Ooze'"
          PatternToAdd = 2089
        Case 231751
          PatternNamed = "Aban-Bhotar Assembly of 'Ooze'"
          PatternToAdd = 2089
        Case 232398
          PatternNamed = "Aban-Bhotar Assembly of 'Pazuzu'"
          PatternToAdd = 2090
        Case 234247
          PatternNamed = "Aban-Bhotar Assembly of 'Pazuzu'"
          PatternToAdd = 2090
        Case 232120
          PatternNamed = "Aban-Bhotar Assembly of 'Quake'"
          PatternToAdd = 2091
        Case 231877
          PatternNamed = "Aban-Bhotar Assembly of 'Quondam'"
          PatternToAdd = 2092
        Case 232443
          PatternNamed = "Aban-Bhotar Assembly of 'Rallies Fete'"
          PatternToAdd = 2093
        Case 234296
          PatternNamed = "Aban-Bhotar Assembly of 'Rallies Fete'"
          PatternToAdd = 2093
        Case 232497
          PatternNamed = "Aban-Bhotar Assembly of 'Razor the Battletoad'"
          PatternToAdd = 2094
        Case 234350
          PatternNamed = "Aban-Bhotar Assembly of 'Razor the Battletoad'"
          PatternToAdd = 2094
        Case 242541
          PatternNamed = "Aban-Bhotar Assembly of 'Redeemed Cama'"
          PatternToAdd = 2095
        Case 232560
          PatternNamed = "Aban-Bhotar Assembly of 'Redeemed Gilthar'"
          PatternToAdd = 2096
        Case 242514
          PatternNamed = "Aban-Bhotar Assembly of 'Redeemed Gilthar'"
          PatternToAdd = 2096
        Case 232614
          PatternNamed = "Aban-Bhotar Assembly of 'Redeemed Lord Galahad'"
          PatternToAdd = 2097
        Case 234436
          PatternNamed = "Aban-Bhotar Assembly of 'Redeemed Lord Galahad'"
          PatternToAdd = 2097
        Case 242496
          PatternNamed = "Aban-Bhotar Assembly of 'Redeemed Ocra'"
          PatternToAdd = 2098
        Case 232272
          PatternNamed = "Aban-Bhotar Assembly of 'Relief Teals'"
          PatternToAdd = 2099
        Case 234121
          PatternNamed = "Aban-Bhotar Assembly of 'Relief Teals'"
          PatternToAdd = 2099
        Case 232551
          PatternNamed = "Aban-Bhotar Assembly of 'Roch'"
          PatternToAdd = 2100
        Case 239548
          PatternNamed = "Aban-Bhotar Assembly of 'Sabretooth Slicer'"
          PatternToAdd = 2101
        Case 231850
          PatternNamed = "Aban-Bhotar Assembly of 'Sampsa'"
          PatternToAdd = 2102
        Case 232353
          PatternNamed = "Aban-Bhotar Assembly of 'Sasabonsam'"
          PatternToAdd = 2103
        Case 234202
          PatternNamed = "Aban-Bhotar Assembly of 'Sasabonsam'"
          PatternToAdd = 2103
        Case 232219
          PatternNamed = "Aban-Bhotar Assembly of 'Sashu'"
          PatternToAdd = 2104
        Case 234094
          PatternNamed = "Aban-Bhotar Assembly of 'Sashu'"
          PatternToAdd = 2104
        Case 239656
          PatternNamed = "Aban-Bhotar Assembly of 'Satkamear'"
          PatternToAdd = 2105
        Case 239602
          PatternNamed = "Aban-Bhotar Assembly of 'Sawi'"
          PatternToAdd = 2106
        Case 229175
          PatternNamed = "Aban-Bhotar Assembly of 'Scratch'"
          PatternToAdd = 2107
        Case 231427
          PatternNamed = "Aban-Bhotar Assembly of 'Scratch'"
          PatternToAdd = 2107
        Case 229184
          PatternNamed = "Aban-Bhotar Assembly of 'Screech'"
          PatternToAdd = 2108
        Case 231436
          PatternNamed = "Aban-Bhotar Assembly of 'Screech'"
          PatternToAdd = 2108
        Case 232201
          PatternNamed = "Aban-Bhotar Assembly of 'Shiver'"
          PatternToAdd = 2110
        Case 232506
          PatternNamed = "Aban-Bhotar Assembly of 'Shullat'"
          PatternToAdd = 2111
        Case 234359
          PatternNamed = "Aban-Bhotar Assembly of 'Shullat'"
          PatternToAdd = 2111
        Case 229148
          PatternNamed = "Aban-Bhotar Assembly of 'Silver Fang'"
          PatternToAdd = 2112
        Case 231400
          PatternNamed = "Aban-Bhotar Assembly of 'Silver Fang'"
          PatternToAdd = 2112
        Case 232084
          PatternNamed = "Aban-Bhotar Assembly of 'Skulky'"
          PatternToAdd = 2113
        Case 232066
          PatternNamed = "Aban-Bhotar Assembly of 'Slinky'"
          PatternToAdd = 2114
        Case 232039
          PatternNamed = "Aban-Bhotar Assembly of 'Smee'"
          PatternToAdd = 2115
        Case 229256
          PatternNamed = "Aban-Bhotar Assembly of 'Spiritless Soul Dredge'"
          PatternToAdd = 2116
        Case 231508
          PatternNamed = "Aban-Bhotar Assembly of 'Spiritless Soul Dredge'"
          PatternToAdd = 2116
        Case 231949
          PatternNamed = "Aban-Bhotar Assembly of 'Srahir'"
          PatternToAdd = 2117
        Case 232344
          PatternNamed = "Aban-Bhotar Assembly of 'Taille Frees'"
          PatternToAdd = 2118
        Case 234193
          PatternNamed = "Aban-Bhotar Assembly of 'Taille Frees'"
          PatternToAdd = 2118
        Case 239702
          PatternNamed = "Aban-Bhotar Assembly of 'Tcheser'"
          PatternToAdd = 2119
        Case 239494
          PatternNamed = "Aban-Bhotar Assembly of 'The Abysmal Lord'"
          PatternToAdd = 2120
        Case 232192
          PatternNamed = "Aban-Bhotar Assembly of 'The Abyssal Widow'"
          PatternToAdd = 2121
        Case 239693
          PatternNamed = "Aban-Bhotar Assembly of 'The Achbile Guardian'"
          PatternToAdd = 2122
        Case 232147
          PatternNamed = "Aban-Bhotar Assembly of 'The Adonian Soul Dredge'"
          PatternToAdd = 2123
        Case 232174
          PatternNamed = "Aban-Bhotar Assembly of 'The Adonis Spirit Master'"
          PatternToAdd = 2124
        Case 229535
          PatternNamed = "Aban-Bhotar Assembly of 'The Archbile Queen'"
          PatternToAdd = 2125
        Case 231778
          PatternNamed = "Aban-Bhotar Assembly of 'The Archbile Queen'"
          PatternToAdd = 2125
        Case 239620
          PatternNamed = "Aban-Bhotar Assembly of 'The Brobdingnagian Mother'"
          PatternToAdd = 2126
        Case 232165
          PatternNamed = "Aban-Bhotar Assembly of 'The Dredge Driver'"
          PatternToAdd = 2127
        Case 232389
          PatternNamed = "Aban-Bhotar Assembly of 'The Dryad Demigod'"
          PatternToAdd = 2128
        Case 234238
          PatternNamed = "Aban-Bhotar Assembly of 'The Dryad Demigod'"
          PatternToAdd = 2128
        Case 231940
          PatternNamed = "Aban-Bhotar Assembly of 'The Dryad Shuffle'"
          PatternToAdd = 2129
        Case 229211
          PatternNamed = "Aban-Bhotar Assembly of 'The Dune Suzerain'"
          PatternToAdd = 2130
        Case 231463
          PatternNamed = "Aban-Bhotar Assembly of 'The Dune Suzerain'"
          PatternToAdd = 2130
        Case 229238
          PatternNamed = "Aban-Bhotar Assembly of 'The Elysian Soul Dredge'"
          PatternToAdd = 2131
        Case 231490
          PatternNamed = "Aban-Bhotar Assembly of 'The Elysian Soul Dredge'"
          PatternToAdd = 2131
        Case 229562
          PatternNamed = "Aban-Bhotar Assembly of 'The Enrapt One'"
          PatternToAdd = 2132
        Case 231805
          PatternNamed = "Aban-Bhotar Assembly of 'The Enrapt One'"
          PatternToAdd = 2132
        Case 239720
          PatternNamed = "Aban-Bhotar Assembly of 'The Great Ice Golem'"
          PatternToAdd = 2133
        Case 232425
          PatternNamed = "Aban-Bhotar Assembly of 'The Indomitable Chimera'"
          PatternToAdd = 2134
        Case 234276
          PatternNamed = "Aban-Bhotar Assembly of 'The Indomitable Chimera'"
          PatternToAdd = 2134
        Case 239503
          PatternNamed = "Aban-Bhotar Assembly of 'The  Infernal Soul Dredge'"
          PatternToAdd = 2135
        Case 242454
          PatternNamed = "Aban-Bhotar Assembly of 'The Infernal Soul Dredge'"
          PatternToAdd = 2136
        Case 231922
          PatternNamed = "Aban-Bhotar Assembly of 'The Maggot Lord'"
          PatternToAdd = 2137
        Case 231868
          PatternNamed = "Aban-Bhotar Assembly of 'The Maggot Lord'"
          PatternToAdd = 2137
        Case 232290
          PatternNamed = "Aban-Bhotar Assembly of 'The Mortificator'"
          PatternToAdd = 2138
        Case 234139
          PatternNamed = "Aban-Bhotar Assembly of 'The Mortificator'"
          PatternToAdd = 2138
        Case 229553
          PatternNamed = "Aban-Bhotar Assembly of 'The Numb One'"
          PatternToAdd = 2139
        Case 231796
          PatternNamed = "Aban-Bhotar Assembly of 'The Numb One'"
          PatternToAdd = 2139
        Case 231895
          PatternNamed = "Aban-Bhotar Assembly of 'The Penumbral Spirit Hunter'"
          PatternToAdd = 2140
        Case 232057
          PatternNamed = "Aban-Bhotar Assembly of 'The Peristaltic Abomination'"
          PatternToAdd = 2141
        Case 232048
          PatternNamed = "Aban-Bhotar Assembly of 'The Peristaltic Aversion'"
          PatternToAdd = 2167
        Case 232183
          PatternNamed = "Aban-Bhotar Assembly of 'The Proprietrix'"
          PatternToAdd = 2142
        Case 229517
          PatternNamed = "Aban-Bhotar Assembly of 'The Scheolian Soul Dredge'"
          PatternToAdd = 2143
        Case 231760
          PatternNamed = "Aban-Bhotar Assembly of 'The Scheolian Soul Dredge'"
          PatternToAdd = 2143
        Case 239629
          PatternNamed = "Aban-Bhotar Assembly of 'The Stupendous Breeder'"
          PatternToAdd = 2144
        Case 229220
          PatternNamed = "Aban-Bhotar Assembly of 'The Talus Suzerain'"
          PatternToAdd = 2145
        Case 231472
          PatternNamed = "Aban-Bhotar Assembly of 'The Talus Suzerain'"
          PatternToAdd = 2145
        Case 232156
          PatternNamed = "Aban-Bhotar Assembly of 'The Watchdog'"
          PatternToAdd = 2146
        Case 231976
          PatternNamed = "Aban-Bhotar Assembly of 'The Worm King'"
          PatternToAdd = 2147
        Case 229471
          PatternNamed = "Aban-Bhotar Assembly of 'Thunderous Chimera'"
          PatternToAdd = 2148
        Case 231715
          PatternNamed = "Aban-Bhotar Assembly of 'Thunderous Chimera'"
          PatternToAdd = 2148
        Case 232111
          PatternNamed = "Aban-Bhotar Assembly of 'Toss'"
          PatternToAdd = 2149
        Case 229431
          PatternNamed = "Aban-Bhotar Assembly of 'Tough'"
          PatternToAdd = 2150
        Case 231679
          PatternNamed = "Aban-Bhotar Assembly of 'Tough'"
          PatternToAdd = 2150
        Case 229229
          PatternNamed = "Aban-Bhotar Assembly of 'Ungulera'"
          PatternToAdd = 2151
        Case 231481
          PatternNamed = "Aban-Bhotar Assembly of 'Ungulera'"
          PatternToAdd = 2151
        Case 242523
          PatternNamed = "Aban-Bhotar Assembly of 'Unredeemed Dalja'"
          PatternToAdd = 2152
        Case 232623
          PatternNamed = "Aban-Bhotar Assembly of 'Unredeemed Lord Mordeth'"
          PatternToAdd = 2153
        Case 234447
          PatternNamed = "Aban-Bhotar Assembly of 'Unredeemed Lord Mordeth'"
          PatternToAdd = 2153
        Case 242505
          PatternNamed = "Aban-Bhotar Assembly of 'Unredeemed Roch'"
          PatternToAdd = 2154
        Case 242550
          PatternNamed = "Aban-Bhotar Assembly of 'Unredeemed Vanya'"
          PatternToAdd = 2155
        Case 239711
          PatternNamed = "Aban-Bhotar Assembly of 'Upenpet'"
          PatternToAdd = 2156
        Case 232326
          PatternNamed = "Aban-Bhotar Assembly of 'Ushqa'"
          PatternToAdd = 2157
        Case 234175
          PatternNamed = "Aban-Bhotar Assembly of 'Ushqa'"
          PatternToAdd = 2157
        Case 232605
          PatternNamed = "Aban-Bhotar Assembly of 'Vanya'"
          PatternToAdd = 2158
        Case 232138
          PatternNamed = "Aban-Bhotar Assembly of 'Viscious Visitant'"
          PatternToAdd = 2159
        Case 229449
          PatternNamed = "Aban-Bhotar Assembly of 'Wacky Suzerain'"
          PatternToAdd = 2160
        Case 231697
          PatternNamed = "Aban-Bhotar Assembly of 'Wacky Suzerain'"
          PatternToAdd = 2160
        Case 239584
          PatternNamed = "Aban-Bhotar Assembly of 'Wala'"
          PatternToAdd = 2161
        Case 234112
          PatternNamed = "Aban-Bhotar Assembly of 'Waqa'"
          PatternToAdd = 2162
        Case 242656
          PatternNamed = "Aban-Bhotar Assembly of 'Weary Empath Min-Ji Liu'"
          PatternToAdd = 2163
        Case 229544
          PatternNamed = "Aban-Bhotar Assembly of 'White'"
          PatternToAdd = 2164
        Case 231787
          PatternNamed = "Aban-Bhotar Assembly of 'White'"
          PatternToAdd = 2164
        Case 232488
          PatternNamed = "Aban-Bhotar Assembly of 'Xark the Battletoad'"
          PatternToAdd = 2165
        Case 234341
          PatternNamed = "Aban-Bhotar Assembly of 'Xark the Battletoad'"
          PatternToAdd = 2165
        Case 231958
          PatternNamed = "Aban-Bhotar Assembly of 'Zoetic Oak'"
          PatternToAdd = 2166
        Case 242687
          PatternNamed = "Abhan Pattern 'Adobe Suzerain'"
          PatternToAdd = 3001
        Case 242781
          PatternNamed = "Abhan Pattern 'Aesma Daeva'"
          PatternToAdd = 3002
        Case 242770
          PatternNamed = "Abhan Pattern 'Agent of Decay'"
          PatternToAdd = 3003
        Case 242767
          PatternNamed = "Abhan Pattern 'Agent of Putrefaction'"
          PatternToAdd = 3004
        Case 242771
          PatternNamed = "Abhan Pattern 'Ahpta'"
          PatternToAdd = 3005
       Case 242735
          PatternNamed = "Abhan Pattern 'Alatyr'"
          PatternToAdd = 3006
        Case 242778
          PatternNamed = "Abhan Pattern 'Anansi"
          PatternToAdd = 3007
        Case 242793
          PatternNamed = "Abhan Pattern 'Anansi'"
          PatternToAdd = 3007
        Case 242809
          PatternNamed = "Abhan Pattern 'Anarir'"
          PatternToAdd = 3008
        Case 242724
          PatternNamed = "Abhan Pattern 'Anya'"
          PatternToAdd = 3009
        Case 242737
          PatternNamed = "Abhan Pattern 'Aray'"
          PatternToAdd = 3010
        Case 242788
          PatternNamed = "Abhan Pattern 'Arch Bigot Aliel'"
          PatternToAdd = 3011
        Case 242782
          PatternNamed = "Abhan Pattern 'Arch Bigot Biap'"
          PatternToAdd = 3012
        Case 242786
          PatternNamed = "Abhan Pattern 'Arch Bigot Lohel'"
          PatternToAdd = 3013
        Case 242710
          PatternNamed = "Abhan Pattern 'Argil Suzerain'"
          PatternToAdd = 3015
        Case 242787
          PatternNamed = "Abhan Pattern 'Asase Ya'"
          PatternToAdd = 3016
        Case 242695
          PatternNamed = "Abhan Pattern 'Ashmara Ravin'"
          PatternToAdd = 3017
        Case 242784
          PatternNamed = "Abhan Pattern 'Ats'u"
          PatternToAdd = 3018
        Case 242689
          PatternNamed = "Abhan Pattern 'Auger'"
          PatternToAdd = 3019
        Case 242690
          PatternNamed = "Abhan Pattern 'Awl'"
          PatternToAdd = 3020
        Case 242730
          PatternNamed = "Abhan Pattern 'Bagaspati'"
          PatternToAdd = 3021
        Case 242692
          PatternNamed = "Abhan Pattern 'Beatific Spirit'"
          PatternToAdd = 3022
        Case 242709
          PatternNamed = "Abhan Pattern 'Bellowing Chimera'"
          PatternToAdd = 3023
        Case 242702
          PatternNamed = "Abhan Pattern 'Bhinaji Navi'"
          PatternToAdd = 3024
        Case 242776
          PatternNamed = "Abhan Pattern 'Bia'"
          PatternToAdd = 3025
        Case 242675
          PatternNamed = "Abhan Pattern 'Black Fang'"
          PatternToAdd = 3026
        Case 242698
          PatternNamed = "Abhan Pattern 'Blight'"
          PatternToAdd = 3027
        Case 242691
          PatternNamed = "Abhan Pattern 'Borer'"
          PatternToAdd = 3028
        Case 242731
          PatternNamed = "Abhan Pattern 'Breaker Teuvo'"
          PatternToAdd = 3029
       Case 242729
          PatternNamed = "Abhan Pattern 'Brutal Rafter'"
          PatternToAdd = 3030
        Case 242679
          PatternNamed = "Abhan Pattern 'Brutal Soul Dredge'"
          PatternToAdd = 3031
        Case 242801
          PatternNamed = "Abhan Pattern 'Canceroid Cupid'"
          PatternToAdd = 3033
        Case 242789
          PatternNamed = "Abhan Pattern 'Captured Spirit'"
          PatternToAdd = 3034
        Case 242803
          PatternNamed = "Abhan Pattern 'Careening Blight'"
          PatternToAdd = 3035
        Case 242804
          PatternNamed = "Abhan Pattern 'Careening Death'"
          PatternToAdd = 3036
        Case 242753
          PatternNamed = "Abhan Pattern 'Churn'"
          PatternToAdd = 3037
        Case 242678
          PatternNamed = "Abhan Pattern 'Circumbendibum'"
          PatternToAdd = 3038
        Case 242674
          PatternNamed = "Abhan Pattern 'Circumbendibus'"
          PatternToAdd = 3039
        Case 242701
          PatternNamed = "Abhan Pattern 'Contorted Soul Dredge'"
          PatternToAdd = 3040
        Case 242720
          PatternNamed = "Abhan Pattern 'Defiler of Scheol'"
          PatternToAdd = 3042
        Case 242722
          PatternNamed = "Abhan Pattern 'Destroyer of Scheol'"
          PatternToAdd = 3043
        Case 242721
          PatternNamed = "Abhan Pattern 'Devastator of Scheol'"
          PatternToAdd = 3044
        Case 242719
          PatternNamed = "Abhan Pattern 'Devourer of Scheol'"
          PatternToAdd = 3045
        Case 242812
          PatternNamed = "Abhan Pattern 'Eidolean Soul Dredge'"
          PatternToAdd = 3047
        Case 242817
          PatternNamed = "Abhan Pattern 'Exsequiae'"
          PatternToAdd = 3049
        Case 242769
          PatternNamed = "Abhan Pattern 'Fester Leila'"
          PatternToAdd = 3050
        Case 242705
          PatternNamed = "Abhan Pattern 'Flinty'"
          PatternToAdd = 3051
        Case 242813
          PatternNamed = "Abhan Pattern 'Glitter'"
          PatternToAdd = 3052
        Case 242684
          PatternNamed = "Abhan Pattern 'Gracious Soul Dredge'"
          PatternToAdd = 3053
        Case 242707
          PatternNamed = "Abhan Pattern 'Gunk'"
          PatternToAdd = 3054
        Case 242738
          PatternNamed = "Abhan Pattern 'Hadur'"
          PatternToAdd = 3055
        Case 242773
          PatternNamed = "Abhan Pattern 'Haqa'"
          PatternToAdd = 3056
        Case 242699
          PatternNamed = "Abhan Pattern 'Hemut'"
          PatternToAdd = 3057
        Case 242816
          PatternNamed = "Abhan Pattern 'Ho'"
          PatternToAdd = 3058
        Case 242686
          PatternNamed = "Abhan Pattern 'Ignis Fatui'"
          PatternToAdd = 3059
        Case 242693
          PatternNamed = "Abhan Pattern 'Ignis Fatuus'"
          PatternToAdd = 3060
        Case 242739
          PatternNamed = "Abhan Pattern 'Imk'a"
          PatternToAdd = 3061
        Case 242665
          PatternNamed = "Abhan Pattern 'Infernal Demon'"
          PatternToAdd = 3062
        Case 242688
          PatternNamed = "Abhan Pattern 'Iunmin'"
          PatternToAdd = 3063
        Case 242740
          PatternNamed = "Abhan Pattern 'Juma'"
          PatternToAdd = 3064
        Case 242764
          PatternNamed = "Abhan Pattern 'K'a"
          PatternToAdd = 3065
        Case 242741
          PatternNamed = "Abhan Pattern 'Kaleva'"
          PatternToAdd = 3066
        Case 242714
          PatternNamed = "Abhan Pattern 'Kaoline Suzerain'"
          PatternToAdd = 3067
        Case 242700
          PatternNamed = "Abhan Pattern 'Khemhet'"
          PatternToAdd = 3068
        Case 242696
          PatternNamed = "Abhan Pattern 'Lethargic Spirit'"
          PatternToAdd = 3070
        Case 242711
          PatternNamed = "Abhan Pattern 'Loessial Suzerain'"
          PatternToAdd = 3071
        Case 242815
          PatternNamed = "Abhan Pattern 'Loltonunon'"
          PatternToAdd = 3072
        Case 242747
          PatternNamed = "Abhan Pattern 'Lurky'"
          PatternToAdd = 3073
        Case 242777
          PatternNamed = "Abhan Pattern 'Lya'"
          PatternToAdd = 3074
        Case 242800
          PatternNamed = "Abhan Pattern 'Malah-Animus'"
          PatternToAdd = 3075
        Case 242799
          PatternNamed = "Abhan Pattern 'Malah-At'"
          PatternToAdd = 3076
        Case 242798
          PatternNamed = "Abhan Pattern 'Malah-Auris'"
          PatternToAdd = 3077
        Case 242805
          PatternNamed = "Abhan Pattern 'Maledicta'"
          PatternToAdd = 3078
        Case 242694
          PatternNamed = "Abhan Pattern 'Marem'"
          PatternToAdd = 3079
        Case 242703
          PatternNamed = "Abhan Pattern 'Marly Suzerain'"
          PatternToAdd = 3080
        Case 242807
          PatternNamed = "Abhan Pattern 'Mawi'"
          PatternToAdd = 3081
        Case 242697
          PatternNamed = "Abhan Pattern 'Misery'"
          PatternToAdd = 3082
        Case 242749
          PatternNamed = "Abhan Pattern 'Moochy'"
          PatternToAdd = 3083
        Case 242727
          PatternNamed = "Abhan Pattern 'Morrow'"
          PatternToAdd = 3084
        Case 242795
          PatternNamed = "Abhan Pattern 'Nyame'"
          PatternToAdd = 3085
        Case 242794
          PatternNamed = "Abhan Pattern 'Odqan'"
          PatternToAdd = 3087
        Case 242742
          PatternNamed = "Abhan Pattern 'Old Salty'"
          PatternToAdd = 3088
        Case 242712
          PatternNamed = "Abhan Pattern 'Ooze'"
          PatternToAdd = 3089
        Case 242780
          PatternNamed = "Abhan Pattern 'Pazuzu'"
          PatternToAdd = 3090
        Case 242752
          PatternNamed = "Abhan Pattern 'Quake'"
          PatternToAdd = 3091
        Case 242726
          PatternNamed = "Abhan Pattern 'Quondam'"
          PatternToAdd = 3092
        Case 242785
          PatternNamed = "Abhan Pattern 'Rallies Fete'"
          PatternToAdd = 3093
        Case 242791
          PatternNamed = "Abhan Pattern 'Razor the Battletoad'"
          PatternToAdd = 3094
        Case 242667
          PatternNamed = "Abhan Pattern 'Redeemed Cama'"
          PatternToAdd = 3095
        Case 242663
          PatternNamed = "Abhan Pattern 'Redeemed Gilthar'"
          PatternToAdd = 3096
        Case 242669
          PatternNamed = "Abhan Pattern 'Redeemed Lord Galahad'"
          PatternToAdd = 3097
        Case 242766
          PatternNamed = "Abhan Pattern 'Relief Teals'"
          PatternToAdd = 3099
        Case 242802
          PatternNamed = "Abhan Pattern 'Sabretooth Slicer'"
          PatternToAdd = 3101
        Case 242723
          PatternNamed = "Abhan Pattern 'Sampsa'"
          PatternToAdd = 3102
        Case 242775
          PatternNamed = "Abhan Pattern 'Sasabonsam'"
          PatternToAdd = 3103
        Case 242763
          PatternNamed = "Abhan Pattern 'Sashu'"
          PatternToAdd = 3104
        Case 242814
          PatternNamed = "Abhan Pattern 'Satkamear'"
          PatternToAdd = 3105
        Case 242808
          PatternNamed = "Abhan Pattern 'Sawi'"
          PatternToAdd = 3106
        Case 242676
          PatternNamed = "Abhan Pattern 'Scratch'"
          PatternToAdd = 3107
        Case 242677
          PatternNamed = "Abhan Pattern 'Screech'"
          PatternToAdd = 3108
        Case 242761
          PatternNamed = "Abhan Pattern 'Shiver'"
          PatternToAdd = 3110
        Case 242792
          PatternNamed = "Abhan Pattern 'Shullat'"
          PatternToAdd = 3111
        Case 242673
          PatternNamed = "Abhan Pattern 'Silver Fang'"
          PatternToAdd = 3112
        Case 242748
          PatternNamed = "Abhan Pattern 'Skulky'"
          PatternToAdd = 3113
        Case 242746
          PatternNamed = "Abhan Pattern 'Slinky'"
          PatternToAdd = 3114
        Case 242743
          PatternNamed = "Abhan Pattern 'Smee'"
          PatternToAdd = 3115
        Case 242685
          PatternNamed = "Abhan Pattern 'Spiritless Soul Dredge'"
          PatternToAdd = 3116
        Case 242733
          PatternNamed = "Abhan Pattern 'Srahir'"
          PatternToAdd = 3117
       Case 242774
          PatternNamed = "Abhan Pattern 'Taille Frees'"
          PatternToAdd = 3118
        Case 242819
          PatternNamed = "Abhan Pattern 'Tcheser'"
          PatternToAdd = 3119
        Case 242796
          PatternNamed = "Abhan Pattern 'The Abysmal Lord'"
          PatternToAdd = 3120
        Case 242760
          PatternNamed = "Abhan Pattern 'The Abyssal Widow'"
          PatternToAdd = 3121
        Case 242818
          PatternNamed = "Abhan Pattern 'The Achbile Guardian'"
          PatternToAdd = 3122
        Case 242755
          PatternNamed = "Abhan Pattern 'The Adonian Soul Dredge'"
          PatternToAdd = 3123
        Case 242758
          PatternNamed = "Abhan Pattern 'The Adonis Spirit Master'"
          PatternToAdd = 3124
        Case 242715
          PatternNamed = "Abhan Pattern 'The Archbile Queen'"
          PatternToAdd = 3125
        Case 242810
          PatternNamed = "Abhan Pattern 'The Brobdingnagian Mother'"
          PatternToAdd = 3126
        Case 242757
          PatternNamed = "Abhan Pattern 'The Dredge Driver'"
          PatternToAdd = 3127
        Case 242779
          PatternNamed = "Abhan Pattern 'The Dryad Demigod'"
          PatternToAdd = 3128
        Case 242732
          PatternNamed = "Abhan Pattern 'The Dryad Shuffle'"
          PatternToAdd = 3129
        Case 242680
          PatternNamed = "Abhan Pattern 'The Dune Suzerain'"
          PatternToAdd = 3130
        Case 242683
          PatternNamed = "Abhan Pattern 'The Elysian Soul Dredge'"
          PatternToAdd = 3131
        Case 242718
          PatternNamed = "Abhan Pattern 'The Enrapt One'"
          PatternToAdd = 3132
        Case 242783
          PatternNamed = "Abhan Pattern 'The Indomitable Chimera'"
          PatternToAdd = 3134
        Case 242797
          PatternNamed = "Abhan Pattern 'The Infernal Soul Dredge'"
          PatternToAdd = 3136
        Case 242725
          PatternNamed = "Abhan Pattern 'The Maggot Lord'"
          PatternToAdd = 3137
        Case 242768
          PatternNamed = "Abhan Pattern 'The Mortificator'"
          PatternToAdd = 3138
        Case 242717
          PatternNamed = "Abhan Pattern 'The Numb One'"
          PatternToAdd = 3139
        Case 242728
          PatternNamed = "Abhan Pattern 'The Penumbral Spirit Hunter'"
          PatternToAdd = 3140
        Case 242745
          PatternNamed = "Abhan Pattern 'The Peristaltic Abomination'"
          PatternToAdd = 3141
        Case 242744
          PatternNamed = "Abhan Pattern 'The Peristaltic Aversion'"
          PatternToAdd = 3167
        Case 242759
          PatternNamed = "Abhan Pattern 'The Proprietrix'"
          PatternToAdd = 3142
        Case 242713
          PatternNamed = "Abhan Pattern 'The Scheolian Soul Dredge'"
          PatternToAdd = 3143
        Case 242811
          PatternNamed = "Abhan Pattern 'The Stupendous Breeder'"
          PatternToAdd = 3144
        Case 242681
          PatternNamed = "Abhan Pattern 'The Talus Suzerain'"
          PatternToAdd = 3145
        Case 242756
          PatternNamed = "Abhan Pattern 'The Watchdog'"
          PatternToAdd = 3146
        Case 242736
          PatternNamed = "Abhan Pattern 'The Worm King'"
          PatternToAdd = 3147
        Case 242708
          PatternNamed = "Abhan Pattern 'Thunderous Chimera'"
          PatternToAdd = 3148
        Case 242751
          PatternNamed = "Abhan Pattern 'Toss'"
          PatternToAdd = 3149
        Case 242704
          PatternNamed = "Abhan Pattern 'Tough'"
          PatternToAdd = 3150
        Case 242682
          PatternNamed = "Abhan Pattern 'Ungulera'"
          PatternToAdd = 3151
        Case 242664
          PatternNamed = "Abhan Pattern 'Unredeemed Dalja'"
          PatternToAdd = 3152
        Case 242670
          PatternNamed = "Abhan Pattern 'Unredeemed Lord Mordeth'"
          PatternToAdd = 3153
        Case 242662
          PatternNamed = "Abhan Pattern 'Unredeemed Roch'"
          PatternToAdd = 3154
        Case 242668
          PatternNamed = "Abhan Pattern 'Unredeemed Vanya'"
          PatternToAdd = 3155
        Case 242820
          PatternNamed = "Abhan Pattern 'Upenpet'"
          PatternToAdd = 3156
        Case 242772
          PatternNamed = "Abhan Pattern 'Ushqa'"
          PatternToAdd = 3157
        Case 242754
          PatternNamed = "Abhan Pattern 'Viscious Visitant'"
          PatternToAdd = 3159
        Case 242706
          PatternNamed = "Abhan Pattern 'Wacky Suzerain'"
          PatternToAdd = 3160
        Case 242806
          PatternNamed = "Abhan Pattern 'Wala'"
          PatternToAdd = 3161
        Case 242765
          PatternNamed = "Abhan Pattern 'Waqa'"
          PatternToAdd = 3162
        Case 242652
          PatternNamed = "Abhan Pattern 'Weary Empath Min-Ji Liu'"
          PatternToAdd = 3163
        Case 242716
          PatternNamed = "Abhan Pattern 'White'"
          PatternToAdd = 3164
        Case 242790
          PatternNamed = "Abhan Pattern 'Xark the Battletoad'"
          PatternToAdd = 3165
        Case 242734
          PatternNamed = "Abhan Pattern 'Zoetic Oak'"
          PatternToAdd = 3166
        Case 231523
          PatternNamed = "Bhotaar Pattern 'Adobe Suzerain'"
          PatternToAdd = 4001
        Case 232404
          PatternNamed = "Bhotaar Pattern 'Aesma Daeva'"
          PatternToAdd = 4002
        Case 234253
          PatternNamed = "Bhotaar Pattern 'Aesma Daeva'"
          PatternToAdd = 4002
        Case 232305
          PatternNamed = "Bhotaar Pattern 'Agent of Decay'"
          PatternToAdd = 4003
        Case 234154
          PatternNamed = "Bhotaar Pattern 'Agent of Decay'"
          PatternToAdd = 4003
        Case 232278
          PatternNamed = "Bhotaar Pattern 'Agent of Putrefaction'"
          PatternToAdd = 4004
        Case 234127
          PatternNamed = "Bhotaar Pattern 'Agent of Putrefaction'"
          PatternToAdd = 4004
        Case 232314
          PatternNamed = "Bhotaar Pattern 'Ahpta'"
          PatternToAdd = 4005
        Case 234163
          PatternNamed = "Bhotaar Pattern 'Ahpta'"
          PatternToAdd = 4005
        Case 231964
          PatternNamed = "Bhotaar Pattern 'Alatyr'"
          PatternToAdd = 4006
        Case 232377
          PatternNamed = "Bhotaar Pattern 'Anansi"
          PatternToAdd = 4007
        Case 234226
          PatternNamed = "Bhotaar Pattern 'Anansi"
          PatternToAdd = 4007
        Case 234365
          PatternNamed = "Bhotaar Pattern 'Anansi'"
          PatternToAdd = 4007
        Case 239608
          PatternNamed = "Bhotaar Pattern 'Anarir'"
          PatternToAdd = 4008
        Case 231856
          PatternNamed = "Bhotaar Pattern 'Anya'"
          PatternToAdd = 4009
        Case 231982
          PatternNamed = "Bhotaar Pattern 'Aray'"
          PatternToAdd = 4010
        Case 232467
          PatternNamed = "Bhotaar Pattern 'Arch Bigot Aliel'"
          PatternToAdd = 4011
        Case 234320
          PatternNamed = "Bhotaar Pattern 'Arch Bigot Aliel'"
          PatternToAdd = 4011
        Case 232413
          PatternNamed = "Bhotaar Pattern 'Arch Bigot Biap'"
          PatternToAdd = 4012
        Case 234262
          PatternNamed = "Bhotaar Pattern 'Arch Bigot Biap'"
          PatternToAdd = 4012
        Case 232449
          PatternNamed = "Bhotaar Pattern 'Arch Bigot Lohel'"
          PatternToAdd = 4013
        Case 234302
          PatternNamed = "Bhotaar Pattern 'Arch Bigot Lohel'"
          PatternToAdd = 4013
        Case 231730
          PatternNamed = "Bhotaar Pattern 'Argil Suzerain'"
          PatternToAdd = 4015
        Case 232458
          PatternNamed = "Bhotaar Pattern 'Asase Ya'"
          PatternToAdd = 4016
        Case 234311
          PatternNamed = "Bhotaar Pattern 'Asase Ya'"
          PatternToAdd = 4016
        Case 231595
          PatternNamed = "Bhotaar Pattern 'Ashmara Ravin'"
          PatternToAdd = 4017
        Case 232431
          PatternNamed = "Bhotaar Pattern 'Ats'u"
          PatternToAdd = 4018
        Case 234282
          PatternNamed = "Bhotaar Pattern 'Ats'u"
          PatternToAdd = 4018
        Case 231541
          PatternNamed = "Bhotaar Pattern 'Auger'"
          PatternToAdd = 4019
        Case 231550
          PatternNamed = "Bhotaar Pattern 'Awl'"
          PatternToAdd = 4020
        Case 231910
          PatternNamed = "Bhotaar Pattern 'Bagaspati'"
          PatternToAdd = 4021
        Case 231568
          PatternNamed = "Bhotaar Pattern 'Beatific Spirit'"
          PatternToAdd = 4022
        Case 231721
          PatternNamed = "Bhotaar Pattern 'Bellowing Chimera'"
          PatternToAdd = 4023
        Case 231658
          PatternNamed = "Bhotaar Pattern 'Bhinaji Navi'"
          PatternToAdd = 4024
        Case 232359
          PatternNamed = "Bhotaar Pattern 'Bia'"
          PatternToAdd = 4025
        Case 234208
          PatternNamed = "Bhotaar Pattern 'Bia'"
          PatternToAdd = 4025
        Case 229163
          PatternNamed = "Bhotaar Pattern 'Black Fang'"
          PatternToAdd = 4026
        Case 231415
          PatternNamed = "Bhotaar Pattern 'Black Fang'"
          PatternToAdd = 4026
        Case 231622
          PatternNamed = "Bhotaar Pattern 'Blight'"
          PatternToAdd = 4027
        Case 231559
          PatternNamed = "Bhotaar Pattern 'Borer'"
          PatternToAdd = 4028
        Case 231928
          PatternNamed = "Bhotaar Pattern 'Breaker Teuvo'"
          PatternToAdd = 4029
        Case 231901
          PatternNamed = "Bhotaar Pattern 'Brutal Rafter'"
          PatternToAdd = 4030
        Case 229199
          PatternNamed = "Bhotaar Pattern 'Brutal Soul Dredge'"
          PatternToAdd = 4031
        Case 231451
          PatternNamed = "Bhotaar Pattern 'Brutal Soul Dredge'"
          PatternToAdd = 4031
        Case 232593
          PatternNamed = "Bhotaar Pattern 'Cama'"
          PatternToAdd = 4032
        Case 239536
          PatternNamed = "Bhotaar Pattern 'Canceroid Cupid'"
          PatternToAdd = 4033
        Case 232476
          PatternNamed = "Bhotaar Pattern 'Captured Spirit'"
          PatternToAdd = 4034
        Case 234329
          PatternNamed = "Bhotaar Pattern 'Captured Spirit'"
          PatternToAdd = 4034
        Case 239554
          PatternNamed = "Bhotaar Pattern 'Careening Blight'"
          PatternToAdd = 4035
        Case 239563
          PatternNamed = "Bhotaar Pattern 'Careening Death'"
          PatternToAdd = 4036
        Case 232126
          PatternNamed = "Bhotaar Pattern 'Churn'"
          PatternToAdd = 4037
        Case 229190
          PatternNamed = "Bhotaar Pattern 'Circumbendibum'"
          PatternToAdd = 4038
        Case 231442
          PatternNamed = "Bhotaar Pattern 'Circumbendibum'"
          PatternToAdd = 4038
        Case 229154
          PatternNamed = "Bhotaar Pattern 'Circumbendibus'"
          PatternToAdd = 4039
        Case 231406
          PatternNamed = "Bhotaar Pattern 'Circumbendibus'"
          PatternToAdd = 4039
        Case 231649
          PatternNamed = "Bhotaar Pattern 'Contorted Soul Dredge'"
          PatternToAdd = 4040
        Case 232566
          PatternNamed = "Bhotaar Pattern 'Dalja'"
          PatternToAdd = 4041
        Case 231820
          PatternNamed = "Bhotaar Pattern 'Defiler of Scheol'"
          PatternToAdd = 4042
        Case 231838
          PatternNamed = "Bhotaar Pattern 'Destroyer of Scheol'"
          PatternToAdd = 4043
        Case 231829
          PatternNamed = "Bhotaar Pattern 'Devastator of Scheol'"
          PatternToAdd = 4044
        Case 231811
          PatternNamed = "Bhotaar Pattern 'Devourer of Scheol'"
          PatternToAdd = 4045
        Case 239635
          PatternNamed = "Bhotaar Pattern 'Eidolean Soul Dredge'"
          PatternToAdd = 4047
        Case 232530
          PatternNamed = "Bhotaar Pattern 'Empath Min-Ji Liu'"
          PatternToAdd = 4048
        Case 239681
          PatternNamed = "Bhotaar Pattern 'Exsequiae'"
          PatternToAdd = 4049
        Case 232296
          PatternNamed = "Bhotaar Pattern 'Fester Leila'"
          PatternToAdd = 4050
        Case 234145
          PatternNamed = "Bhotaar Pattern 'Fester Leila'"
          PatternToAdd = 4050
        Case 231685
          PatternNamed = "Bhotaar Pattern 'Flinty'"
          PatternToAdd = 4051
        Case 239644
          PatternNamed = "Bhotaar Pattern 'Glitter'"
          PatternToAdd = 4052
        Case 231496
          PatternNamed = "Bhotaar Pattern 'Gracious Soul Dredge'"
          PatternToAdd = 4053
        Case 231703
          PatternNamed = "Bhotaar Pattern 'Gunk'"
          PatternToAdd = 4054
        Case 231991
          PatternNamed = "Bhotaar Pattern 'Hadur'"
          PatternToAdd = 4055
        Case 232332
          PatternNamed = "Bhotaar Pattern 'Haqa'"
          PatternToAdd = 4056
        Case 234181
          PatternNamed = "Bhotaar Pattern 'Haqa'"
          PatternToAdd = 4056
        Case 231631
          PatternNamed = "Bhotaar Pattern 'Hemut'"
          PatternToAdd = 4057
        Case 239671
          PatternNamed = "Bhotaar Pattern 'Ho'"
          PatternToAdd = 4058
        Case 231514
          PatternNamed = "Bhotaar Pattern 'Ignis Fatui'"
          PatternToAdd = 4059
        Case 231577
          PatternNamed = "Bhotaar Pattern 'Ignis Fatuus'"
          PatternToAdd = 4060
        Case 232000
          PatternNamed = "Bhotaar Pattern 'Imk'a"
          PatternToAdd = 4061
        Case 232575
          PatternNamed = "Bhotaar Pattern 'Infernal Demon'"
          PatternToAdd = 4062
        Case 231532
          PatternNamed = "Bhotaar Pattern 'Iunmin'"
          PatternToAdd = 4063
        Case 232009
          PatternNamed = "Bhotaar Pattern 'Juma'"
          PatternToAdd = 4064
        Case 232225
          PatternNamed = "Bhotaar Pattern 'K'a"
          PatternToAdd = 4065
        Case 234100
          PatternNamed = "Bhotaar Pattern 'K'a"
          PatternToAdd = 4065
        Case 232018
          PatternNamed = "Bhotaar Pattern 'Kaleva'"
          PatternToAdd = 4066
        Case 231766
          PatternNamed = "Bhotaar Pattern 'Kaoline Suzerain'"
          PatternToAdd = 4067
        Case 231640
          PatternNamed = "Bhotaar Pattern 'Khemhet'"
          PatternToAdd = 4068
        Case 231604
          PatternNamed = "Bhotaar Pattern 'Lethargic Spirit'"
          PatternToAdd = 4070
        Case 231739
          PatternNamed = "Bhotaar Pattern 'Loessial Suzerain'"
          PatternToAdd = 4071
        Case 239662
          PatternNamed = "Bhotaar Pattern 'Loltonunon'"
          PatternToAdd = 4072
        Case 232072
          PatternNamed = "Bhotaar Pattern 'Lurky'"
          PatternToAdd = 4073
        Case 232368
          PatternNamed = "Bhotaar Pattern 'Lya'"
          PatternToAdd = 4074
        Case 234217
          PatternNamed = "Bhotaar Pattern 'Lya'"
          PatternToAdd = 4074
        Case 239527
          PatternNamed = "Bhotaar Pattern 'Malah-Animus'"
          PatternToAdd = 4075
        Case 239518
          PatternNamed = "Bhotaar Pattern 'Malah-At'"
          PatternToAdd = 4076
        Case 239509
          PatternNamed = "Bhotaar Pattern 'Malah-Auris'"
          PatternToAdd = 4077
        Case 239572
          PatternNamed = "Bhotaar Pattern 'Maledicta'"
          PatternToAdd = 4078
        Case 231586
          PatternNamed = "Bhotaar Pattern 'Marem'"
          PatternToAdd = 4079
        Case 231667
          PatternNamed = "Bhotaar Pattern 'Marly Suzerain'"
          PatternToAdd = 4080
        Case 239590
          PatternNamed = "Bhotaar Pattern 'Mawi'"
          PatternToAdd = 4081
        Case 231613
          PatternNamed = "Bhotaar Pattern 'Misery'"
          PatternToAdd = 4082
        Case 232090
          PatternNamed = "Bhotaar Pattern 'Moochy'"
          PatternToAdd = 4083
        Case 231883
          PatternNamed = "Bhotaar Pattern 'Morrow'"
          PatternToAdd = 4084
        Case 239482
          PatternNamed = "Bhotaar Pattern 'Nyame'"
          PatternToAdd = 4085
        Case 232521
          PatternNamed = "Bhotaar Pattern 'Odqan'"
          PatternToAdd = 4087
        Case 234374
          PatternNamed = "Bhotaar Pattern 'Odqan'"
          PatternToAdd = 4087
        Case 232027
          PatternNamed = "Bhotaar Pattern 'Old Salty'"
          PatternToAdd = 4088
        Case 231748
          PatternNamed = "Bhotaar Pattern 'Ooze'"
          PatternToAdd = 4089
        Case 232395
          PatternNamed = "Bhotaar Pattern 'Pazuzu'"
          PatternToAdd = 4090
        Case 234244
          PatternNamed = "Bhotaar Pattern 'Pazuzu'"
          PatternToAdd = 4090
        Case 232117
          PatternNamed = "Bhotaar Pattern 'Quake'"
          PatternToAdd = 4091
        Case 231874
          PatternNamed = "Bhotaar Pattern 'Quondam'"
          PatternToAdd = 4092
        Case 232440
          PatternNamed = "Bhotaar Pattern 'Rallies Fete'"
          PatternToAdd = 4093
        Case 234293
          PatternNamed = "Bhotaar Pattern 'Rallies Fete'"
          PatternToAdd = 4093
        Case 232494
          PatternNamed = "Bhotaar Pattern 'Razor the Battletoad'"
          PatternToAdd = 4094
        Case 234347
          PatternNamed = "Bhotaar Pattern 'Razor the Battletoad'"
          PatternToAdd = 4094
        Case 242538
          PatternNamed = "Bhotaar Pattern 'Redeemed Cama'"
          PatternToAdd = 4095
        Case 232557
          PatternNamed = "Bhotaar Pattern 'Redeemed Gilthar'"
          PatternToAdd = 4096
        Case 242511
          PatternNamed = "Bhotaar Pattern 'Redeemed Gilthar'"
          PatternToAdd = 4096
        Case 234431
          PatternNamed = "Bhotaar Pattern 'Redeemed Lord Galahad'"
          PatternToAdd = 4097
        Case 232269
          PatternNamed = "Bhotaar Pattern 'Relief Teals'"
          PatternToAdd = 4099
        Case 234118
          PatternNamed = "Bhotaar Pattern 'Relief Teals'"
          PatternToAdd = 4099
        Case 232548
          PatternNamed = "Bhotaar Pattern 'Roch'"
          PatternToAdd = 4100
        Case 239545
          PatternNamed = "Bhotaar Pattern 'Sabretooth Slicer'"
          PatternToAdd = 4101
        Case 231847
          PatternNamed = "Bhotaar Pattern 'Sampsa'"
          PatternToAdd = 4102
        Case 232350
          PatternNamed = "Bhotaar Pattern 'Sasabonsam'"
          PatternToAdd = 4103
        Case 234199
          PatternNamed = "Bhotaar Pattern 'Sasabonsam'"
          PatternToAdd = 4103
        Case 232216
          PatternNamed = "Bhotaar Pattern 'Sashu'"
          PatternToAdd = 4104
        Case 234091
          PatternNamed = "Bhotaar Pattern 'Sashu'"
          PatternToAdd = 4104
        Case 239653
          PatternNamed = "Bhotaar Pattern 'Satkamear'"
          PatternToAdd = 4105
        Case 239599
          PatternNamed = "Bhotaar Pattern 'Sawi'"
          PatternToAdd = 4106
        Case 231424
          PatternNamed = "Bhotaar Pattern 'Scratch'"
          PatternToAdd = 4107
        Case 229181
          PatternNamed = "Bhotaar Pattern 'Screech'"
          PatternToAdd = 4108
        Case 231433
          PatternNamed = "Bhotaar Pattern 'Screech'"
          PatternToAdd = 4108
        Case 232198
          PatternNamed = "Bhotaar Pattern 'Shiver'"
          PatternToAdd = 4110
        Case 232503
          PatternNamed = "Bhotaar Pattern 'Shullat'"
          PatternToAdd = 4111
        Case 234356
          PatternNamed = "Bhotaar Pattern 'Shullat'"
          PatternToAdd = 4111
        Case 229145
          PatternNamed = "Bhotaar Pattern 'Silver Fang'"
          PatternToAdd = 4112
        Case 231397
          PatternNamed = "Bhotaar Pattern 'Silver Fang'"
          PatternToAdd = 4112
        Case 232081
          PatternNamed = "Bhotaar Pattern 'Skulky'"
          PatternToAdd = 4113
        Case 232063
          PatternNamed = "Bhotaar Pattern 'Slinky'"
          PatternToAdd = 4114
        Case 232036
          PatternNamed = "Bhotaar Pattern 'Smee'"
          PatternToAdd = 4115
        Case 231505
          PatternNamed = "Bhotaar Pattern 'Spiritless Soul Dredge'"
          PatternToAdd = 4116
        Case 231946
          PatternNamed = "Bhotaar Pattern 'Srahir'"
          PatternToAdd = 4117
        Case 232341
          PatternNamed = "Bhotaar Pattern 'Taille Frees'"
          PatternToAdd = 4118
        Case 234190
          PatternNamed = "Bhotaar Pattern 'Taille Frees'"
          PatternToAdd = 4118
        Case 239699
          PatternNamed = "Bhotaar Pattern 'Tcheser'"
          PatternToAdd = 4119
        Case 239491
          PatternNamed = "Bhotaar Pattern 'The Abysmal Lord'"
          PatternToAdd = 4120
        Case 232189
          PatternNamed = "Bhotaar Pattern 'The Abyssal Widow'"
          PatternToAdd = 4121
        Case 239690
          PatternNamed = "Bhotaar Pattern 'The Achbile Guardian'"
          PatternToAdd = 4122
        Case 232144
          PatternNamed = "Bhotaar Pattern 'The Adonian Soul Dredge'"
          PatternToAdd = 4123
        Case 232171
          PatternNamed = "Bhotaar Pattern 'The Adonis Spirit Master'"
          PatternToAdd = 4124
        Case 229532
          PatternNamed = "Bhotaar Pattern 'The Archbile Queen'"
          PatternToAdd = 4125
        Case 231775
          PatternNamed = "Bhotaar Pattern 'The Archbile Queen'"
          PatternToAdd = 4125
        Case 239617
          PatternNamed = "Bhotaar Pattern 'The Brobdingnagian Mother'"
          PatternToAdd = 4126
        Case 232162
          PatternNamed = "Bhotaar Pattern 'The Dredge Driver'"
          PatternToAdd = 4127
        Case 232386
          PatternNamed = "Bhotaar Pattern 'The Dryad Demigod'"
          PatternToAdd = 4128
        Case 234235
          PatternNamed = "Bhotaar Pattern 'The Dryad Demigod'"
          PatternToAdd = 4128
        Case 231937
          PatternNamed = "Bhotaar Pattern 'The Dryad Shuffle'"
          PatternToAdd = 4129
        Case 229208
          PatternNamed = "Bhotaar Pattern 'The Dune Suzerain'"
          PatternToAdd = 4130
        Case 231460
          PatternNamed = "Bhotaar Pattern 'The Dune Suzerain'"
          PatternToAdd = 4130
        Case 229235
          PatternNamed = "Bhotaar Pattern 'The Elysian Soul Dredge'"
          PatternToAdd = 4131
        Case 231487
          PatternNamed = "Bhotaar Pattern 'The Elysian Soul Dredge'"
          PatternToAdd = 4131
        Case 229559
          PatternNamed = "Bhotaar Pattern 'The Enrapt One'"
          PatternToAdd = 4132
        Case 231802
          PatternNamed = "Bhotaar Pattern 'The Enrapt One'"
          PatternToAdd = 4132
        Case 232422
          PatternNamed = "Bhotaar Pattern 'The Indomitable Chimera'"
          PatternToAdd = 4134
        Case 234273
          PatternNamed = "Bhotaar Pattern 'The Indomitable Chimera'"
          PatternToAdd = 4134
        Case 239500
          PatternNamed = "Bhotaar Pattern 'The  Infernal Soul Dredge'"
          PatternToAdd = 4135
        Case 242451
          PatternNamed = "Bhotaar Pattern 'The Infernal Soul Dredge'"
          PatternToAdd = 4136
        Case 231865
          PatternNamed = "Bhotaar Pattern 'The Maggot Lord'"
          PatternToAdd = 4137
        Case 232287
          PatternNamed = "Bhotaar Pattern 'The Mortificator'"
          PatternToAdd = 4138
        Case 234136
          PatternNamed = "Bhotaar Pattern 'The Mortificator'"
          PatternToAdd = 4138
        Case 229550
          PatternNamed = "Bhotaar Pattern 'The Numb One'"
          PatternToAdd = 4139
        Case 231793
          PatternNamed = "Bhotaar Pattern 'The Numb One'"
          PatternToAdd = 4139
        Case 231892
          PatternNamed = "Bhotaar Pattern 'The Penumbral Spirit Hunter'"
          PatternToAdd = 4140
        Case 232054
          PatternNamed = "Bhotaar Pattern 'The Peristaltic Abomination'"
          PatternToAdd = 4141
        Case 232045
          PatternNamed = "Bhotaar Pattern 'The Peristaltic Aversion'"
          PatternToAdd = 4167
       Case 232180
          PatternNamed = "Bhotaar Pattern 'The Proprietrix'"
          PatternToAdd = 4142
        Case 231757
          PatternNamed = "Bhotaar Pattern 'The Scheolian Soul Dredge'"
          PatternToAdd = 4143
        Case 239626
          PatternNamed = "Bhotaar Pattern 'The Stupendous Breeder'"
          PatternToAdd = 4144
        Case 229217
          PatternNamed = "Bhotaar Pattern 'The Talus Suzerain'"
          PatternToAdd = 4145
        Case 231469
          PatternNamed = "Bhotaar Pattern 'The Talus Suzerain'"
          PatternToAdd = 4145
        Case 232153
          PatternNamed = "Bhotaar Pattern 'The Watchdog'"
          PatternToAdd = 4146
        Case 231973
          PatternNamed = "Bhotaar Pattern 'The Worm King'"
          PatternToAdd = 4147
        Case 231712
          PatternNamed = "Bhotaar Pattern 'Thunderous Chimera'"
          PatternToAdd = 4148
        Case 232108
          PatternNamed = "Bhotaar Pattern 'Toss'"
          PatternToAdd = 4149
        Case 231676
          PatternNamed = "Bhotaar Pattern 'Tough'"
          PatternToAdd = 4150
        Case 229226
          PatternNamed = "Bhotaar Pattern 'Ungulera'"
          PatternToAdd = 4151
        Case 231478
          PatternNamed = "Bhotaar Pattern 'Ungulera'"
          PatternToAdd = 4151
        Case 242520
          PatternNamed = "Bhotaar Pattern 'Unredeemed Dalja'"
          PatternToAdd = 4152
        Case 234444
          PatternNamed = "Bhotaar Pattern 'Unredeemed Lord Mordeth'"
          PatternToAdd = 4153
        Case 242502
          PatternNamed = "Bhotaar Pattern 'Unredeemed Roch'"
          PatternToAdd = 4154
        Case 242547
          PatternNamed = "Bhotaar Pattern 'Unredeemed Vanya'"
          PatternToAdd = 4155
        Case 239708
          PatternNamed = "Bhotaar Pattern 'Upenpet'"
          PatternToAdd = 4156
        Case 234172
          PatternNamed = "Bhotaar Pattern 'Ushqa'"
          PatternToAdd = 4157
        Case 232602
          PatternNamed = "Bhotaar Pattern 'Vanya'"
          PatternToAdd = 4158
        Case 232135
          PatternNamed = "Bhotaar Pattern 'Viscious Visitant'"
          PatternToAdd = 4159
        Case 231694
          PatternNamed = "Bhotaar Pattern 'Wacky Suzerain'"
          PatternToAdd = 4160
        Case 239581
          PatternNamed = "Bhotaar Pattern 'Wala'"
          PatternToAdd = 4161
        Case 234109
          PatternNamed = "Bhotaar Pattern 'Waqa'"
          PatternToAdd = 4162
        Case 242653
          PatternNamed = "Bhotaar Pattern 'Weary Empath Min-Ji Liu'"
          PatternToAdd = 4163
        Case 229541
          PatternNamed = "Bhotaar Pattern 'White'"
          PatternToAdd = 4164
        Case 231784
          PatternNamed = "Bhotaar Pattern 'White'"
          PatternToAdd = 4164
        Case 232485
          PatternNamed = "Bhotaar Pattern 'Xark the Battletoad'"
          PatternToAdd = 4165
        Case 234338
          PatternNamed = "Bhotaar Pattern 'Xark the Battletoad'"
          PatternToAdd = 4165
        Case 231955
          PatternNamed = "Bhotaar Pattern 'Zoetic Oak'"
          PatternToAdd = 4166
        Case 231524
          PatternNamed = "Chi Pattern of  'Adobe Suzerain'"
          PatternToAdd = 5001
        Case 232405
          PatternNamed = "Chi Pattern of  'Aesma Daeva'"
          PatternToAdd = 5002
        Case 234254
          PatternNamed = "Chi Pattern of  'Aesma Daeva'"
          PatternToAdd = 5002
        Case 232306
          PatternNamed = "Chi Pattern of  'Agent of Decay'"
          PatternToAdd = 5003
        Case 234155
          PatternNamed = "Chi Pattern of  'Agent of Decay'"
          PatternToAdd = 5003
        Case 232279
          PatternNamed = "Chi Pattern of  'Agent of Putrefaction'"
          PatternToAdd = 5004
        Case 234128
          PatternNamed = "Chi Pattern of  'Agent of Putrefaction'"
          PatternToAdd = 5004
        Case 232315
          PatternNamed = "Chi Pattern of  'Ahpta'"
          PatternToAdd = 5005
        Case 234164
          PatternNamed = "Chi Pattern of  'Ahpta'"
          PatternToAdd = 5005
        Case 231965
          PatternNamed = "Chi Pattern of  'Alatyr'"
          PatternToAdd = 5006
        Case 232378
          PatternNamed = "Chi Pattern of  'Anansi"
          PatternToAdd = 5007
        Case 234227
          PatternNamed = "Chi Pattern of  'Anansi"
          PatternToAdd = 5007
        Case 232513
          PatternNamed = "Chi Pattern of  'Anansi'"
          PatternToAdd = 5007
        Case 234366
          PatternNamed = "Chi Pattern of  'Anansi'"
          PatternToAdd = 5007
        Case 239609
          PatternNamed = "Chi Pattern of  'Anarir'"
          PatternToAdd = 5008
        Case 231857
          PatternNamed = "Chi Pattern of  'Anya'"
          PatternToAdd = 5009
        Case 231983
          PatternNamed = "Chi Pattern of  'Aray'"
          PatternToAdd = 5010
        Case 232468
          PatternNamed = "Chi Pattern of  'Arch Bigot Aliel'"
          PatternToAdd = 5011
        Case 234321
          PatternNamed = "Chi Pattern of  'Arch Bigot Aliel'"
          PatternToAdd = 5011
        Case 232414
          PatternNamed = "Chi Pattern of  'Arch Bigot Biap'"
          PatternToAdd = 5012
        Case 234263
          PatternNamed = "Chi Pattern of  'Arch Bigot Biap'"
          PatternToAdd = 5012
        Case 232450
          PatternNamed = "Chi Pattern of  'Arch Bigot Lohel'"
          PatternToAdd = 5013
        Case 234303
          PatternNamed = "Chi Pattern of  'Arch Bigot Lohel'"
          PatternToAdd = 5013
        Case 231731
          PatternNamed = "Chi Pattern of  'Argil Suzerain'"
          PatternToAdd = 5015
        Case 232459
          PatternNamed = "Chi Pattern of  'Asase Ya'"
          PatternToAdd = 5016
        Case 234312
          PatternNamed = "Chi Pattern of  'Asase Ya'"
          PatternToAdd = 5016
        Case 231596
          PatternNamed = "Chi Pattern of  'Ashmara Ravin'"
          PatternToAdd = 5017
        Case 232432
          PatternNamed = "Chi Pattern of  'Ats'u"
          PatternToAdd = 5018
        Case 234283
          PatternNamed = "Chi Pattern of  'Ats'u"
          PatternToAdd = 5018
        Case 231542
          PatternNamed = "Chi Pattern of  'Auger'"
          PatternToAdd = 5019
        Case 231551
          PatternNamed = "Chi Pattern of  'Awl'"
          PatternToAdd = 5020
        Case 231911
          PatternNamed = "Chi Pattern of  'Bagaspati'"
          PatternToAdd = 5021
        Case 231569
          PatternNamed = "Chi Pattern of  'Beatific Spirit'"
          PatternToAdd = 5022
        Case 231722
          PatternNamed = "Chi Pattern of  'Bellowing Chimera'"
          PatternToAdd = 5023
        Case 231659
          PatternNamed = "Chi Pattern of  'Bhinaji Navi'"
          PatternToAdd = 5024
        Case 234209
          PatternNamed = "Chi Pattern of  'Bia'"
          PatternToAdd = 5025
        Case 229164
          PatternNamed = "Chi Pattern of  'Black Fang'"
          PatternToAdd = 5026
        Case 231416
          PatternNamed = "Chi Pattern of  'Black Fang'"
          PatternToAdd = 5026
        Case 231623
          PatternNamed = "Chi Pattern of  'Blight'"
          PatternToAdd = 5027
        Case 231560
          PatternNamed = "Chi Pattern of  'Borer'"
          PatternToAdd = 5028
        Case 231929
          PatternNamed = "Chi Pattern of  'Breaker Teuvo'"
          PatternToAdd = 5029
        Case 231902
          PatternNamed = "Chi Pattern of  'Brutal Rafter'"
          PatternToAdd = 5030
        Case 229200
          PatternNamed = "Chi Pattern of  'Brutal Soul Dredge'"
          PatternToAdd = 5031
        Case 231452
          PatternNamed = "Chi Pattern of  'Brutal Soul Dredge'"
          PatternToAdd = 5031
        Case 232594
          PatternNamed = "Chi Pattern of  'Cama'"
          PatternToAdd = 5032
        Case 239537
          PatternNamed = "Chi Pattern of  'Canceroid Cupid'"
          PatternToAdd = 5033
        Case 232477
          PatternNamed = "Chi Pattern of  'Captured Spirit'"
          PatternToAdd = 5034
        Case 234330
          PatternNamed = "Chi Pattern of  'Captured Spirit'"
          PatternToAdd = 5034
        Case 239555
          PatternNamed = "Chi Pattern of  'Careening Blight'"
          PatternToAdd = 5035
        Case 239564
          PatternNamed = "Chi Pattern of  'Careening Death'"
          PatternToAdd = 5036
        Case 232127
          PatternNamed = "Chi Pattern of  'Churn'"
          PatternToAdd = 5037
        Case 229191
          PatternNamed = "Chi Pattern of  'Circumbendibum'"
          PatternToAdd = 5038
        Case 231443
          PatternNamed = "Chi Pattern of  'Circumbendibum'"
          PatternToAdd = 5038
        Case 229155
          PatternNamed = "Chi Pattern of  'Circumbendibus'"
          PatternToAdd = 5039
        Case 231407
          PatternNamed = "Chi Pattern of  'Circumbendibus'"
          PatternToAdd = 5039
        Case 231650
          PatternNamed = "Chi Pattern of  'Contorted Soul Dredge'"
          PatternToAdd = 5040
        Case 232567
          PatternNamed = "Chi Pattern of  'Dalja'"
          PatternToAdd = 5041
        Case 231821
          PatternNamed = "Chi Pattern of  'Defiler of Scheol'"
          PatternToAdd = 5042
        Case 231839
          PatternNamed = "Chi Pattern of  'Destroyer of Scheol'"
          PatternToAdd = 5043
        Case 231830
          PatternNamed = "Chi Pattern of  'Devastator of Scheol'"
          PatternToAdd = 5044
        Case 231812
          PatternNamed = "Chi Pattern of  'Devourer of Scheol'"
          PatternToAdd = 5045
        Case 239636
          PatternNamed = "Chi Pattern of  'Eidolean Soul Dredge'"
          PatternToAdd = 5047
        Case 232531
          PatternNamed = "Chi Pattern of  'Empath Min-Ji Liu'"
          PatternToAdd = 5048
        Case 239682
          PatternNamed = "Chi Pattern of  'Exsequiae'"
          PatternToAdd = 5049
        Case 232297
          PatternNamed = "Chi Pattern of  'Fester Leila'"
          PatternToAdd = 5050
        Case 234146
          PatternNamed = "Chi Pattern of  'Fester Leila'"
          PatternToAdd = 5050
        Case 231686
          PatternNamed = "Chi Pattern of  'Flinty'"
          PatternToAdd = 5051
        Case 239645
          PatternNamed = "Chi Pattern of  'Glitter'"
          PatternToAdd = 5052
        Case 231497
          PatternNamed = "Chi Pattern of  'Gracious Soul Dredge'"
          PatternToAdd = 5053
        Case 231704
          PatternNamed = "Chi Pattern of  'Gunk'"
          PatternToAdd = 5054
        Case 231992
          PatternNamed = "Chi Pattern of  'Hadur'"
          PatternToAdd = 5055
        Case 232333
          PatternNamed = "Chi Pattern of  'Haqa'"
          PatternToAdd = 5056
        Case 234182
          PatternNamed = "Chi Pattern of  'Haqa'"
          PatternToAdd = 5056
        Case 231632
          PatternNamed = "Chi Pattern of  'Hemut'"
          PatternToAdd = 5057
        Case 239672
          PatternNamed = "Chi Pattern of  'Ho'"
          PatternToAdd = 5058
        Case 231515
          PatternNamed = "Chi Pattern of  'Ignis Fatui'"
          PatternToAdd = 5059
        Case 231578
          PatternNamed = "Chi Pattern of  'Ignis Fatuus'"
          PatternToAdd = 5060
        Case 232001
          PatternNamed = "Chi Pattern of  'Imk'a"
          PatternToAdd = 5061
        Case 232576
          PatternNamed = "Chi Pattern of  'Infernal Demon'"
          PatternToAdd = 5062
        Case 231533
          PatternNamed = "Chi Pattern of  'Iunmin'"
          PatternToAdd = 5063
        Case 232010
          PatternNamed = "Chi Pattern of  'Juma'"
          PatternToAdd = 5064
        Case 232226
          PatternNamed = "Chi Pattern of  'K'a"
          PatternToAdd = 5065
        Case 234101
          PatternNamed = "Chi Pattern of  'K'a"
          PatternToAdd = 5065
       Case 232019
          PatternNamed = "Chi Pattern of  'Kaleva'"
          PatternToAdd = 5066
        Case 231767
          PatternNamed = "Chi Pattern of  'Kaoline Suzerain'"
          PatternToAdd = 5067
        Case 231641
          PatternNamed = "Chi Pattern of  'Khemhet'"
          PatternToAdd = 5068
        Case 231605
          PatternNamed = "Chi Pattern of  'Lethargic Spirit'"
          PatternToAdd = 5070
        Case 231740
          PatternNamed = "Chi Pattern of  'Loessial Suzerain'"
          PatternToAdd = 5071
        Case 239663
          PatternNamed = "Chi Pattern of  'Loltonunon'"
          PatternToAdd = 5072
       Case 232073
          PatternNamed = "Chi Pattern of  'Lurky'"
          PatternToAdd = 5073
        Case 232369
          PatternNamed = "Chi Pattern of  'Lya'"
          PatternToAdd = 5074
        Case 234218
          PatternNamed = "Chi Pattern of  'Lya'"
          PatternToAdd = 5074
        Case 239528
          PatternNamed = "Chi Pattern of  'Malah-Animus'"
          PatternToAdd = 5075
        Case 239519
          PatternNamed = "Chi Pattern of  'Malah-At'"
          PatternToAdd = 5076
        Case 239510
          PatternNamed = "Chi Pattern of  'Malah-Auris'"
          PatternToAdd = 5077
        Case 239573
          PatternNamed = "Chi Pattern of  'Maledicta'"
          PatternToAdd = 5078
        Case 231587
          PatternNamed = "Chi Pattern of  'Marem'"
          PatternToAdd = 5079
        Case 231668
          PatternNamed = "Chi Pattern of  'Marly Suzerain'"
          PatternToAdd = 5080
        Case 239591
          PatternNamed = "Chi Pattern of  'Mawi'"
          PatternToAdd = 5081
        Case 231614
          PatternNamed = "Chi Pattern of  'Misery'"
          PatternToAdd = 5082
        Case 232091
          PatternNamed = "Chi Pattern of  'Moochy'"
          PatternToAdd = 5083
        Case 231884
          PatternNamed = "Chi Pattern of  'Morrow'"
          PatternToAdd = 5084
        Case 239483
          PatternNamed = "Chi Pattern of  'Nyame'"
          PatternToAdd = 5085
        Case 232522
          PatternNamed = "Chi Pattern of  'Odqan'"
          PatternToAdd = 5087
        Case 234375
          PatternNamed = "Chi Pattern of  'Odqan'"
          PatternToAdd = 5087
        Case 232028
          PatternNamed = "Chi Pattern of  'Old Salty'"
          PatternToAdd = 5088
        Case 231749
          PatternNamed = "Chi Pattern of  'Ooze'"
          PatternToAdd = 5089
        Case 232396
          PatternNamed = "Chi Pattern of  'Pazuzu'"
          PatternToAdd = 5090
        Case 234245
          PatternNamed = "Chi Pattern of  'Pazuzu'"
          PatternToAdd = 5090
        Case 232118
          PatternNamed = "Chi Pattern of  'Quake'"
          PatternToAdd = 5091
        Case 231875
          PatternNamed = "Chi Pattern of  'Quondam'"
          PatternToAdd = 5092
        Case 232441
          PatternNamed = "Chi Pattern of  'Rallies Fete'"
          PatternToAdd = 5093
        Case 234294
          PatternNamed = "Chi Pattern of  'Rallies Fete'"
          PatternToAdd = 5093
        Case 232495
          PatternNamed = "Chi Pattern of  'Razor the Battletoad'"
          PatternToAdd = 5094
        Case 234348
          PatternNamed = "Chi Pattern of  'Razor the Battletoad'"
          PatternToAdd = 5094
        Case 242539
          PatternNamed = "Chi Pattern of  'Redeemed Cama'"
          PatternToAdd = 5095
        Case 232558
          PatternNamed = "Chi Pattern of  'Redeemed Gilthar'"
          PatternToAdd = 5096
        Case 242512
          PatternNamed = "Chi Pattern of  'Redeemed Gilthar'"
          PatternToAdd = 5096
        Case 234432
          PatternNamed = "Chi Pattern of  'Redeemed Lord Galahad'"
          PatternToAdd = 5097
        Case 232270
          PatternNamed = "Chi Pattern of  'Relief Teals'"
          PatternToAdd = 5099
        Case 234119
          PatternNamed = "Chi Pattern of  'Relief Teals'"
          PatternToAdd = 5099
        Case 232549
          PatternNamed = "Chi Pattern of  'Roch'"
          PatternToAdd = 5100
        Case 239546
          PatternNamed = "Chi Pattern of  'Sabretooth Slicer'"
          PatternToAdd = 5101
        Case 231848
          PatternNamed = "Chi Pattern of  'Sampsa'"
          PatternToAdd = 5102
        Case 232351
          PatternNamed = "Chi Pattern of  'Sasabonsam'"
          PatternToAdd = 5103
        Case 234200
          PatternNamed = "Chi Pattern of  'Sasabonsam'"
          PatternToAdd = 5103
        Case 232217
          PatternNamed = "Chi Pattern of  'Sashu'"
          PatternToAdd = 5104
        Case 234092
          PatternNamed = "Chi Pattern of  'Sashu'"
          PatternToAdd = 5104
        Case 239654
          PatternNamed = "Chi Pattern of  'Satkamear'"
          PatternToAdd = 5105
        Case 239600
          PatternNamed = "Chi Pattern of  'Sawi'"
          PatternToAdd = 5106
        Case 229173
          PatternNamed = "Chi Pattern of  'Scratch'"
          PatternToAdd = 5107
        Case 231425
          PatternNamed = "Chi Pattern of  'Scratch'"
          PatternToAdd = 5107
        Case 229182
          PatternNamed = "Chi Pattern of  'Screech'"
          PatternToAdd = 5108
        Case 231434
          PatternNamed = "Chi Pattern of  'Screech'"
          PatternToAdd = 5108
        Case 232199
          PatternNamed = "Chi Pattern of  'Shiver'"
          PatternToAdd = 5110
        Case 232504
          PatternNamed = "Chi Pattern of  'Shullat'"
          PatternToAdd = 5111
        Case 234357
          PatternNamed = "Chi Pattern of  'Shullat'"
          PatternToAdd = 5111
        Case 229146
          PatternNamed = "Chi Pattern of  'Silver Fang'"
          PatternToAdd = 5112
        Case 231398
          PatternNamed = "Chi Pattern of  'Silver Fang'"
          PatternToAdd = 5112
        Case 232082
          PatternNamed = "Chi Pattern of  'Skulky'"
          PatternToAdd = 5113
        Case 232064
          PatternNamed = "Chi Pattern of  'Slinky'"
          PatternToAdd = 5114
        Case 232037
          PatternNamed = "Chi Pattern of  'Smee'"
          PatternToAdd = 5115
        Case 231506
          PatternNamed = "Chi Pattern of  'Spiritless Soul Dredge'"
          PatternToAdd = 5116
        Case 231947
          PatternNamed = "Chi Pattern of  'Srahir'"
          PatternToAdd = 5117
        Case 232342
          PatternNamed = "Chi Pattern of  'Taille Frees'"
          PatternToAdd = 5118
        Case 234191
          PatternNamed = "Chi Pattern of  'Taille Frees'"
          PatternToAdd = 5118
        Case 239700
          PatternNamed = "Chi Pattern of  'Tcheser'"
          PatternToAdd = 5119
        Case 239492
          PatternNamed = "Chi Pattern of  'The Abysmal Lord'"
          PatternToAdd = 5120
        Case 232190
          PatternNamed = "Chi Pattern of  'The Abyssal Widow'"
          PatternToAdd = 5121
        Case 239691
          PatternNamed = "Chi Pattern of  'The Achbile Guardian'"
          PatternToAdd = 5122
        Case 232145
          PatternNamed = "Chi Pattern of  'The Adonian Soul Dredge'"
          PatternToAdd = 5123
        Case 232172
          PatternNamed = "Chi Pattern of  'The Adonis Spirit Master'"
          PatternToAdd = 5124
        Case 229533
          PatternNamed = "Chi Pattern of  'The Archbile Queen'"
          PatternToAdd = 5125
        Case 231776
          PatternNamed = "Chi Pattern of  'The Archbile Queen'"
          PatternToAdd = 5125
        Case 239618
          PatternNamed = "Chi Pattern of  'The Brobdingnagian Mother'"
          PatternToAdd = 5126
        Case 232163
          PatternNamed = "Chi Pattern of  'The Dredge Driver'"
          PatternToAdd = 5127
        Case 232387
          PatternNamed = "Chi Pattern of  'The Dryad Demigod'"
          PatternToAdd = 5128
        Case 234236
          PatternNamed = "Chi Pattern of  'The Dryad Demigod'"
          PatternToAdd = 5128
        Case 231938
          PatternNamed = "Chi Pattern of  'The Dryad Shuffle'"
          PatternToAdd = 5129
        Case 229209
          PatternNamed = "Chi Pattern of  'The Dune Suzerain'"
          PatternToAdd = 5130
        Case 231461
          PatternNamed = "Chi Pattern of  'The Dune Suzerain'"
          PatternToAdd = 5130
        Case 229236
          PatternNamed = "Chi Pattern of  'The Elysian Soul Dredge'"
          PatternToAdd = 5131
        Case 231488
          PatternNamed = "Chi Pattern of  'The Elysian Soul Dredge'"
          PatternToAdd = 5131
        Case 229560
          PatternNamed = "Chi Pattern of  'The Enrapt One'"
          PatternToAdd = 5132
        Case 231803
          PatternNamed = "Chi Pattern of  'The Enrapt One'"
          PatternToAdd = 5132
        Case 232423
          PatternNamed = "Chi Pattern of  'The Indomitable Chimera'"
          PatternToAdd = 5134
        Case 234274
          PatternNamed = "Chi Pattern of  'The Indomitable Chimera'"
          PatternToAdd = 5134
        Case 239501
          PatternNamed = "Chi Pattern of  'The  Infernal Soul Dredge'"
          PatternToAdd = 5135
        Case 242452
          PatternNamed = "Chi Pattern of  'The Infernal Soul Dredge'"
          PatternToAdd = 5136
        Case 231866
          PatternNamed = "Chi Pattern of  'The Maggot Lord'"
          PatternToAdd = 5137
        Case 232288
          PatternNamed = "Chi Pattern of  'The Mortificator'"
          PatternToAdd = 5138
        Case 234137
          PatternNamed = "Chi Pattern of  'The Mortificator'"
          PatternToAdd = 5138
        Case 229551
          PatternNamed = "Chi Pattern of  'The Numb One'"
          PatternToAdd = 5139
        Case 231794
          PatternNamed = "Chi Pattern of  'The Numb One'"
          PatternToAdd = 5139
        Case 231893
          PatternNamed = "Chi Pattern of  'The Penumbral Spirit Hunter'"
          PatternToAdd = 5140
        Case 232055
          PatternNamed = "Chi Pattern of  'The Peristaltic Abomination'"
          PatternToAdd = 5141
        Case 232046
          PatternNamed = "Chi Pattern of  'The Peristaltic Aversion'"
          PatternToAdd = 5167
        Case 232181
          PatternNamed = "Chi Pattern of  'The Proprietrix'"
          PatternToAdd = 5142
        Case 231758
          PatternNamed = "Chi Pattern of  'The Scheolian Soul Dredge'"
          PatternToAdd = 5143
        Case 239627
          PatternNamed = "Chi Pattern of  'The Stupendous Breeder'"
          PatternToAdd = 5144
        Case 229218
          PatternNamed = "Chi Pattern of  'The Talus Suzerain'"
          PatternToAdd = 5145
        Case 231470
          PatternNamed = "Chi Pattern of  'The Talus Suzerain'"
          PatternToAdd = 5145
        Case 232154
          PatternNamed = "Chi Pattern of  'The Watchdog'"
          PatternToAdd = 5146
        Case 231974
          PatternNamed = "Chi Pattern of  'The Worm King'"
          PatternToAdd = 5147
        Case 231713
          PatternNamed = "Chi Pattern of  'Thunderous Chimera'"
          PatternToAdd = 5148
        Case 232109
          PatternNamed = "Chi Pattern of  'Toss'"
          PatternToAdd = 5149
        Case 231677
          PatternNamed = "Chi Pattern of  'Tough'"
          PatternToAdd = 5150
        Case 229227
          PatternNamed = "Chi Pattern of  'Ungulera'"
          PatternToAdd = 5151
        Case 231479
          PatternNamed = "Chi Pattern of  'Ungulera'"
          PatternToAdd = 5151
        Case 242521
          PatternNamed = "Chi Pattern of  'Unredeemed Dalja'"
          PatternToAdd = 5152
        Case 234445
          PatternNamed = "Chi Pattern of  'Unredeemed Lord Mordeth'"
          PatternToAdd = 5153
        Case 242503
          PatternNamed = "Chi Pattern of  'Unredeemed Roch'"
          PatternToAdd = 5154
        Case 242548
          PatternNamed = "Chi Pattern of  'Unredeemed Vanya'"
          PatternToAdd = 5155
        Case 239709
          PatternNamed = "Chi Pattern of  'Upenpet'"
          PatternToAdd = 5156
        Case 234173
          PatternNamed = "Chi Pattern of  'Ushqa'"
          PatternToAdd = 5157
        Case 232603
          PatternNamed = "Chi Pattern of  'Vanya'"
          PatternToAdd = 5158
        Case 232136
          PatternNamed = "Chi Pattern of  'Viscious Visitant'"
          PatternToAdd = 5159
        Case 231695
          PatternNamed = "Chi Pattern of  'Wacky Suzerain'"
          PatternToAdd = 5160
        Case 239582
          PatternNamed = "Chi Pattern of  'Wala'"
          PatternToAdd = 5161
        Case 234110
          PatternNamed = "Chi Pattern of  'Waqa'"
          PatternToAdd = 5162
        Case 242654
          PatternNamed = "Chi Pattern of  'Weary Empath Min-Ji Liu'"
          PatternToAdd = 5163
        Case 229542
          PatternNamed = "Chi Pattern of  'White'"
          PatternToAdd = 5164
        Case 231785
          PatternNamed = "Chi Pattern of  'White'"
          PatternToAdd = 5164
        Case 232486
          PatternNamed = "Chi Pattern of  'Xark the Battletoad'"
          PatternToAdd = 5165
        Case 234339
          PatternNamed = "Chi Pattern of  'Xark the Battletoad'"
          PatternToAdd = 5165
        Case 231956
          PatternNamed = "Chi Pattern of  'Zoetic Oak'"
          PatternToAdd = 5166
        Case 231525
          PatternNamed = "Dom Pattern of 'Adobe Suzerain'"
          PatternToAdd = 6001
        Case 232406
          PatternNamed = "Dom Pattern of 'Aesma Daeva'"
          PatternToAdd = 6002
        Case 234255
          PatternNamed = "Dom Pattern of 'Aesma Daeva'"
          PatternToAdd = 6002
        Case 232307
          PatternNamed = "Dom Pattern of 'Agent of Decay'"
          PatternToAdd = 6003
        Case 234156
          PatternNamed = "Dom Pattern of 'Agent of Decay'"
          PatternToAdd = 6003
        Case 232280
          PatternNamed = "Dom Pattern of 'Agent of Putrefaction'"
          PatternToAdd = 6004
        Case 234129
          PatternNamed = "Dom Pattern of 'Agent of Putrefaction'"
          PatternToAdd = 6004
        Case 232316
          PatternNamed = "Dom Pattern of 'Ahpta'"
          PatternToAdd = 6005
       Case 234165
          PatternNamed = "Dom Pattern of 'Ahpta'"
          PatternToAdd = 6005
       Case 231966
          PatternNamed = "Dom Pattern of 'Alatyr'"
          PatternToAdd = 6006
        Case 232379
          PatternNamed = "Dom Pattern of 'Anansi"
          PatternToAdd = 6007
        Case 234228
          PatternNamed = "Dom Pattern of 'Anansi"
          PatternToAdd = 6007
        Case 232514
          PatternNamed = "Dom Pattern of 'Anansi'"
          PatternToAdd = 6007
        Case 234367
          PatternNamed = "Dom Pattern of 'Anansi'"
          PatternToAdd = 6007
        Case 239610
          PatternNamed = "Dom Pattern of 'Anarir'"
          PatternToAdd = 6008
        Case 231858
          PatternNamed = "Dom Pattern of 'Anya'"
          PatternToAdd = 6009
        Case 231984
          PatternNamed = "Dom Pattern of 'Aray'"
          PatternToAdd = 6010
        Case 232469
          PatternNamed = "Dom Pattern of 'Arch Bigot Aliel'"
          PatternToAdd = 6011
        Case 234322
          PatternNamed = "Dom Pattern of 'Arch Bigot Aliel'"
          PatternToAdd = 6011
        Case 232415
          PatternNamed = "Dom Pattern of 'Arch Bigot Biap'"
          PatternToAdd = 6012
        Case 234264
          PatternNamed = "Dom Pattern of 'Arch Bigot Biap'"
          PatternToAdd = 6012
        Case 232451
          PatternNamed = "Dom Pattern of 'Arch Bigot Lohel'"
          PatternToAdd = 6013
        Case 234304
          PatternNamed = "Dom Pattern of 'Arch Bigot Lohel'"
          PatternToAdd = 6013
        Case 231732
          PatternNamed = "Dom Pattern of 'Argil Suzerain'"
          PatternToAdd = 6015
        Case 232460
          PatternNamed = "Dom Pattern of 'Asase Ya'"
          PatternToAdd = 6016
        Case 234313
          PatternNamed = "Dom Pattern of 'Asase Ya'"
          PatternToAdd = 6016
        Case 231597
          PatternNamed = "Dom Pattern of 'Ashmara Ravin'"
          PatternToAdd = 6017
        Case 232433
          PatternNamed = "Dom Pattern of 'Ats'u"
          PatternToAdd = 6018
        Case 234284
          PatternNamed = "Dom Pattern of 'Ats'u"
          PatternToAdd = 6018
        Case 231543
          PatternNamed = "Dom Pattern of 'Auger'"
          PatternToAdd = 6019
        Case 231552
          PatternNamed = "Dom Pattern of 'Awl'"
          PatternToAdd = 6020
        Case 231912
          PatternNamed = "Dom Pattern of 'Bagaspati'"
          PatternToAdd = 6021
        Case 231570
          PatternNamed = "Dom Pattern of 'Beatific Spirit'"
          PatternToAdd = 6022
        Case 231723
          PatternNamed = "Dom Pattern of 'Bellowing Chimera'"
          PatternToAdd = 6023
        Case 231660
          PatternNamed = "Dom Pattern of 'Bhinaji Navi'"
          PatternToAdd = 6024
        Case 234210
          PatternNamed = "Dom Pattern of 'Bia'"
          PatternToAdd = 6025
        Case 229165
          PatternNamed = "Dom Pattern of 'Black Fang'"
          PatternToAdd = 6026
        Case 231417
          PatternNamed = "Dom Pattern of 'Black Fang'"
          PatternToAdd = 6026
        Case 231624
          PatternNamed = "Dom Pattern of 'Blight'"
          PatternToAdd = 6027
        Case 231561
          PatternNamed = "Dom Pattern of 'Borer'"
          PatternToAdd = 6028
        Case 231930
          PatternNamed = "Dom Pattern of 'Breaker Teuvo'"
          PatternToAdd = 6029
        Case 231903
          PatternNamed = "Dom Pattern of 'Brutal Rafter'"
          PatternToAdd = 6030
        Case 229201
          PatternNamed = "Dom Pattern of 'Brutal Soul Dredge'"
          PatternToAdd = 6031
        Case 231453
          PatternNamed = "Dom Pattern of 'Brutal Soul Dredge'"
          PatternToAdd = 6031
        Case 232595
          PatternNamed = "Dom Pattern of 'Cama'"
          PatternToAdd = 6032
        Case 239538
          PatternNamed = "Dom Pattern of 'Canceroid Cupid'"
          PatternToAdd = 6033
        Case 232478
          PatternNamed = "Dom Pattern of 'Captured Spirit'"
          PatternToAdd = 6034
        Case 234331
          PatternNamed = "Dom Pattern of 'Captured Spirit'"
          PatternToAdd = 6034
        Case 239556
          PatternNamed = "Dom Pattern of 'Careening Blight'"
          PatternToAdd = 6035
        Case 239565
          PatternNamed = "Dom Pattern of 'Careening Death'"
          PatternToAdd = 6036
        Case 232128
          PatternNamed = "Dom Pattern of 'Churn'"
          PatternToAdd = 6037
        Case 229192
          PatternNamed = "Dom Pattern of 'Circumbendibum'"
          PatternToAdd = 6038
        Case 231444
          PatternNamed = "Dom Pattern of 'Circumbendibum'"
          PatternToAdd = 6038
        Case 229156
          PatternNamed = "Dom Pattern of 'Circumbendibus'"
          PatternToAdd = 6039
        Case 231408
          PatternNamed = "Dom Pattern of 'Circumbendibus'"
          PatternToAdd = 6039
        Case 231651
          PatternNamed = "Dom Pattern of 'Contorted Soul Dredge'"
          PatternToAdd = 6040
        Case 232568
          PatternNamed = "Dom Pattern of 'Dalja'"
          PatternToAdd = 6041
        Case 231822
          PatternNamed = "Dom Pattern of 'Defiler of Scheol'"
          PatternToAdd = 6042
        Case 231840
          PatternNamed = "Dom Pattern of 'Destroyer of Scheol'"
          PatternToAdd = 6043
        Case 231831
          PatternNamed = "Dom Pattern of 'Devastator of Scheol'"
          PatternToAdd = 6044
        Case 231813
          PatternNamed = "Dom Pattern of 'Devourer of Scheol'"
          PatternToAdd = 6045
        Case 239637
          PatternNamed = "Dom Pattern of 'Eidolean Soul Dredge'"
          PatternToAdd = 6047
        Case 232532
          PatternNamed = "Dom Pattern of 'Empath Min-Ji Liu'"
          PatternToAdd = 6048
        Case 239683
          PatternNamed = "Dom Pattern of 'Exsequiae'"
          PatternToAdd = 6049
        Case 232298
          PatternNamed = "Dom Pattern of 'Fester Leila'"
          PatternToAdd = 6050
        Case 234147
          PatternNamed = "Dom Pattern of 'Fester Leila'"
          PatternToAdd = 6050
        Case 231687
          PatternNamed = "Dom Pattern of 'Flinty'"
          PatternToAdd = 6051
        Case 239646
          PatternNamed = "Dom Pattern of 'Glitter'"
          PatternToAdd = 6052
        Case 231498
          PatternNamed = "Dom Pattern of 'Gracious Soul Dredge'"
          PatternToAdd = 6053
        Case 231705
          PatternNamed = "Dom Pattern of 'Gunk'"
          PatternToAdd = 6054
        Case 231993
          PatternNamed = "Dom Pattern of 'Hadur'"
          PatternToAdd = 6055
        Case 232334
          PatternNamed = "Dom Pattern of 'Haqa'"
          PatternToAdd = 6056
        Case 234183
          PatternNamed = "Dom Pattern of 'Haqa'"
          PatternToAdd = 6056
        Case 231633
          PatternNamed = "Dom Pattern of 'Hemut'"
          PatternToAdd = 6057
        Case 239673
          PatternNamed = "Dom Pattern of 'Ho'"
          PatternToAdd = 6058
        Case 231516
          PatternNamed = "Dom Pattern of 'Ignis Fatui'"
          PatternToAdd = 6059
        Case 231579
          PatternNamed = "Dom Pattern of 'Ignis Fatuus'"
          PatternToAdd = 6060
        Case 232002
          PatternNamed = "Dom Pattern of 'Imk'a"
          PatternToAdd = 6061
        Case 232577
          PatternNamed = "Dom Pattern of 'Infernal Demon'"
          PatternToAdd = 6062
        Case 231534
          PatternNamed = "Dom Pattern of 'Iunmin'"
          PatternToAdd = 6063
        Case 232011
          PatternNamed = "Dom Pattern of 'Juma'"
          PatternToAdd = 6064
        Case 232227
          PatternNamed = "Dom Pattern of 'K'a"
          PatternToAdd = 6065
        Case 234102
          PatternNamed = "Dom Pattern of 'K'a"
          PatternToAdd = 6065
        Case 232020
          PatternNamed = "Dom Pattern of 'Kaleva'"
          PatternToAdd = 6066
        Case 231768
          PatternNamed = "Dom Pattern of 'Kaoline Suzerain'"
          PatternToAdd = 6067
        Case 231642
          PatternNamed = "Dom Pattern of 'Khemhet'"
          PatternToAdd = 6068
        Case 231606
          PatternNamed = "Dom Pattern of 'Lethargic Spirit'"
          PatternToAdd = 6070
        Case 231741
          PatternNamed = "Dom Pattern of 'Loessial Suzerain'"
          PatternToAdd = 6071
        Case 239664
          PatternNamed = "Dom Pattern of 'Loltonunon'"
          PatternToAdd = 6072
        Case 232074
          PatternNamed = "Dom Pattern of 'Lurky'"
          PatternToAdd = 6073
        Case 232370
          PatternNamed = "Dom Pattern of 'Lya'"
          PatternToAdd = 6074
        Case 234219
          PatternNamed = "Dom Pattern of 'Lya'"
          PatternToAdd = 6074
        Case 239529
          PatternNamed = "Dom Pattern of 'Malah-Animus'"
          PatternToAdd = 6075
        Case 239520
          PatternNamed = "Dom Pattern of 'Malah-At'"
          PatternToAdd = 6076
        Case 239511
          PatternNamed = "Dom Pattern of 'Malah-Auris'"
          PatternToAdd = 6077
        Case 239574
          PatternNamed = "Dom Pattern of 'Maledicta'"
          PatternToAdd = 6078
        Case 231588
          PatternNamed = "Dom Pattern of 'Marem'"
          PatternToAdd = 6079
        Case 231669
          PatternNamed = "Dom Pattern of 'Marly Suzerain'"
          PatternToAdd = 6080
        Case 239592
          PatternNamed = "Dom Pattern of 'Mawi'"
          PatternToAdd = 6081
        Case 231615
          PatternNamed = "Dom Pattern of 'Misery'"
          PatternToAdd = 6082
        Case 232092
          PatternNamed = "Dom Pattern of 'Moochy'"
          PatternToAdd = 6083
        Case 231885
          PatternNamed = "Dom Pattern of 'Morrow'"
          PatternToAdd = 6084
        Case 239484
          PatternNamed = "Dom Pattern of 'Nyame'"
          PatternToAdd = 6085
        Case 232523
          PatternNamed = "Dom Pattern of 'Odqan'"
          PatternToAdd = 6087
        Case 234376
          PatternNamed = "Dom Pattern of 'Odqan'"
          PatternToAdd = 6087
        Case 232029
          PatternNamed = "Dom Pattern of 'Old Salty'"
          PatternToAdd = 6088
        Case 231750
          PatternNamed = "Dom Pattern of 'Ooze'"
          PatternToAdd = 6089
        Case 232397
          PatternNamed = "Dom Pattern of 'Pazuzu'"
          PatternToAdd = 6090
        Case 234246
          PatternNamed = "Dom Pattern of 'Pazuzu'"
          PatternToAdd = 6090
        Case 232119
          PatternNamed = "Dom Pattern of 'Quake'"
          PatternToAdd = 6091
        Case 231876
          PatternNamed = "Dom Pattern of 'Quondam'"
          PatternToAdd = 6092
        Case 232442
          PatternNamed = "Dom Pattern of 'Rallies Fete'"
          PatternToAdd = 6093
        Case 234295
          PatternNamed = "Dom Pattern of 'Rallies Fete'"
          PatternToAdd = 6093
        Case 232496
          PatternNamed = "Dom Pattern of 'Razor the Battletoad'"
          PatternToAdd = 6094
        Case 234349
          PatternNamed = "Dom Pattern of 'Razor the Battletoad'"
          PatternToAdd = 6094
        Case 242540
          PatternNamed = "Dom Pattern of 'Redeemed Cama'"
          PatternToAdd = 6095
        Case 232559
          PatternNamed = "Dom Pattern of 'Redeemed Gilthar'"
          PatternToAdd = 6096
        Case 242513
          PatternNamed = "Dom Pattern of 'Redeemed Gilthar'"
          PatternToAdd = 6096
        Case 234434
          PatternNamed = "Dom Pattern of 'Redeemed Lord Galahad'"
          PatternToAdd = 6097
        Case 232271
          PatternNamed = "Dom Pattern of 'Relief Teals'"
          PatternToAdd = 6099
        Case 234120
          PatternNamed = "Dom Pattern of 'Relief Teals'"
          PatternToAdd = 6099
        Case 232550
          PatternNamed = "Dom Pattern of 'Roch'"
          PatternToAdd = 6100
        Case 239547
          PatternNamed = "Dom Pattern of 'Sabretooth Slicer'"
          PatternToAdd = 6101
        Case 231849
          PatternNamed = "Dom Pattern of 'Sampsa'"
          PatternToAdd = 6102
        Case 232352
          PatternNamed = "Dom Pattern of 'Sasabonsam'"
          PatternToAdd = 6103
        Case 234201
          PatternNamed = "Dom Pattern of 'Sasabonsam'"
          PatternToAdd = 6103
        Case 232218
          PatternNamed = "Dom Pattern of 'Sashu'"
          PatternToAdd = 6104
        Case 234093
          PatternNamed = "Dom Pattern of 'Sashu'"
          PatternToAdd = 6104
        Case 239655
          PatternNamed = "Dom Pattern of 'Satkamear'"
          PatternToAdd = 6105
        Case 239601
          PatternNamed = "Dom Pattern of 'Sawi'"
          PatternToAdd = 6106
        Case 229174
          PatternNamed = "Dom Pattern of 'Scratch'"
          PatternToAdd = 6107
        Case 231426
          PatternNamed = "Dom Pattern of 'Scratch'"
          PatternToAdd = 6107
        Case 229183
          PatternNamed = "Dom Pattern of 'Screech'"
          PatternToAdd = 6108
        Case 231435
          PatternNamed = "Dom Pattern of 'Screech'"
          PatternToAdd = 6108
        Case 232200
          PatternNamed = "Dom Pattern of 'Shiver'"
          PatternToAdd = 6110
        Case 232505
          PatternNamed = "Dom Pattern of 'Shullat'"
          PatternToAdd = 6111
        Case 234358
          PatternNamed = "Dom Pattern of 'Shullat'"
          PatternToAdd = 6111
        Case 229147
          PatternNamed = "Dom Pattern of 'Silver Fang'"
          PatternToAdd = 6112
        Case 231399
          PatternNamed = "Dom Pattern of 'Silver Fang'"
          PatternToAdd = 6112
        Case 232083
          PatternNamed = "Dom Pattern of 'Skulky'"
          PatternToAdd = 6113
        Case 232065
          PatternNamed = "Dom Pattern of 'Slinky'"
          PatternToAdd = 6114
        Case 232038
          PatternNamed = "Dom Pattern of 'Smee'"
          PatternToAdd = 6115
        Case 231507
          PatternNamed = "Dom Pattern of 'Spiritless Soul Dredge'"
          PatternToAdd = 6116
        Case 231948
          PatternNamed = "Dom Pattern of 'Srahir'"
          PatternToAdd = 6117
        Case 232343
          PatternNamed = "Dom Pattern of 'Taille Frees'"
          PatternToAdd = 6118
        Case 234192
          PatternNamed = "Dom Pattern of 'Taille Frees'"
          PatternToAdd = 6118
        Case 239701
          PatternNamed = "Dom Pattern of 'Tcheser'"
          PatternToAdd = 6119
        Case 239493
          PatternNamed = "Dom Pattern of 'The Abysmal Lord'"
          PatternToAdd = 6120
        Case 232191
          PatternNamed = "Dom Pattern of 'The Abyssal Widow'"
          PatternToAdd = 6121
        Case 239692
          PatternNamed = "Dom Pattern of 'The Achbile Guardian'"
          PatternToAdd = 6122
        Case 232146
          PatternNamed = "Dom Pattern of 'The Adonian Soul Dredge'"
          PatternToAdd = 6123
        Case 232173
          PatternNamed = "Dom Pattern of 'The Adonis Spirit Master'"
          PatternToAdd = 6124
        Case 229534
          PatternNamed = "Dom Pattern of 'The Archbile Queen'"
          PatternToAdd = 6125
        Case 231777
          PatternNamed = "Dom Pattern of 'The Archbile Queen'"
          PatternToAdd = 6125
        Case 239619
          PatternNamed = "Dom Pattern of 'The Brobdingnagian Mother'"
          PatternToAdd = 6126
        Case 232164
          PatternNamed = "Dom Pattern of 'The Dredge Driver'"
          PatternToAdd = 6127
        Case 232388
          PatternNamed = "Dom Pattern of 'The Dryad Demigod'"
          PatternToAdd = 6128
        Case 234237
          PatternNamed = "Dom Pattern of 'The Dryad Demigod'"
          PatternToAdd = 6128
        Case 231939
          PatternNamed = "Dom Pattern of 'The Dryad Shuffle'"
          PatternToAdd = 6129
        Case 229210
          PatternNamed = "Dom Pattern of 'The Dune Suzerain'"
          PatternToAdd = 6130
        Case 231462
          PatternNamed = "Dom Pattern of 'The Dune Suzerain'"
          PatternToAdd = 6130
        Case 229237
          PatternNamed = "Dom Pattern of 'The Elysian Soul Dredge'"
          PatternToAdd = 6131
        Case 231489
          PatternNamed = "Dom Pattern of 'The Elysian Soul Dredge'"
          PatternToAdd = 6131
        Case 229561
          PatternNamed = "Dom Pattern of 'The Enrapt One'"
          PatternToAdd = 6132
        Case 231804
          PatternNamed = "Dom Pattern of 'The Enrapt One'"
          PatternToAdd = 6132
        Case 232424
          PatternNamed = "Dom Pattern of 'The Indomitable Chimera'"
          PatternToAdd = 6134
        Case 234275
          PatternNamed = "Dom Pattern of 'The Indomitable Chimera'"
          PatternToAdd = 6134
        Case 239502
          PatternNamed = "Dom Pattern of 'The  Infernal Soul Dredge'"
          PatternToAdd = 6135
        Case 242453
          PatternNamed = "Dom Pattern of 'The Infernal Soul Dredge'"
          PatternToAdd = 6136
        Case 231867
          PatternNamed = "Dom Pattern of 'The Maggot Lord'"
          PatternToAdd = 6137
        Case 232289
          PatternNamed = "Dom Pattern of 'The Mortificator'"
          PatternToAdd = 6138
        Case 234138
          PatternNamed = "Dom Pattern of 'The Mortificator'"
          PatternToAdd = 6138
        Case 229552
          PatternNamed = "Dom Pattern of 'The Numb One'"
          PatternToAdd = 6139
        Case 231795
          PatternNamed = "Dom Pattern of 'The Numb One'"
          PatternToAdd = 6139
        Case 231894
          PatternNamed = "Dom Pattern of 'The Penumbral Spirit Hunter'"
          PatternToAdd = 6140
        Case 232056
          PatternNamed = "Dom Pattern of 'The Peristaltic Abomination'"
          PatternToAdd = 6141
        Case 232047
          PatternNamed = "Dom Pattern of 'The Peristaltic Aversion'"
          PatternToAdd = 6167
        Case 232182
          PatternNamed = "Dom Pattern of 'The Proprietrix'"
          PatternToAdd = 6142
        Case 231759
          PatternNamed = "Dom Pattern of 'The Scheolian Soul Dredge'"
          PatternToAdd = 6143
        Case 239628
          PatternNamed = "Dom Pattern of 'The Stupendous Breeder'"
          PatternToAdd = 6144
        Case 229219
          PatternNamed = "Dom Pattern of 'The Talus Suzerain'"
          PatternToAdd = 6145
        Case 231471
          PatternNamed = "Dom Pattern of 'The Talus Suzerain'"
          PatternToAdd = 6145
        Case 232155
          PatternNamed = "Dom Pattern of 'The Watchdog'"
          PatternToAdd = 6146
        Case 231975
          PatternNamed = "Dom Pattern of 'The Worm King'"
          PatternToAdd = 6147
        Case 231714
          PatternNamed = "Dom Pattern of 'Thunderous Chimera'"
          PatternToAdd = 6148
        Case 232110
          PatternNamed = "Dom Pattern of 'Toss'"
          PatternToAdd = 6149
        Case 231678
          PatternNamed = "Dom Pattern of 'Tough'"
          PatternToAdd = 6150
        Case 229228
          PatternNamed = "Dom Pattern of 'Ungulera'"
          PatternToAdd = 6151
        Case 231480
          PatternNamed = "Dom Pattern of 'Ungulera'"
          PatternToAdd = 6151
        Case 242522
          PatternNamed = "Dom Pattern of 'Unredeemed Dalja'"
          PatternToAdd = 6152
        Case 234446
          PatternNamed = "Dom Pattern of 'Unredeemed Lord Mordeth'"
          PatternToAdd = 6153
        Case 242504
          PatternNamed = "Dom Pattern of 'Unredeemed Roch'"
          PatternToAdd = 6154
        Case 242549
          PatternNamed = "Dom Pattern of 'Unredeemed Vanya'"
          PatternToAdd = 6155
        Case 239710
          PatternNamed = "Dom Pattern of 'Upenpet'"
          PatternToAdd = 6156
        Case 234174
          PatternNamed = "Dom Pattern of 'Ushqa'"
          PatternToAdd = 6157
        Case 232604
          PatternNamed = "Dom Pattern of 'Vanya'"
          PatternToAdd = 6158
        Case 232137
          PatternNamed = "Dom Pattern of 'Viscious Visitant'"
          PatternToAdd = 6159
        Case 231696
          PatternNamed = "Dom Pattern of 'Wacky Suzerain'"
          PatternToAdd = 6160
        Case 239583
          PatternNamed = "Dom Pattern of 'Wala'"
          PatternToAdd = 6161
        Case 234111
          PatternNamed = "Dom Pattern of 'Waqa'"
          PatternToAdd = 6162
        Case 242655
          PatternNamed = "Dom Pattern of 'Weary Empath Min-Ji Liu'"
          PatternToAdd = 6163
        Case 229543
          PatternNamed = "Dom Pattern of 'White'"
          PatternToAdd = 6164
        Case 231786
          PatternNamed = "Dom Pattern of 'White'"
          PatternToAdd = 6164
        Case 232487
          PatternNamed = "Dom Pattern of 'Xark the Battletoad'"
          PatternToAdd = 6165
        Case 234340
          PatternNamed = "Dom Pattern of 'Xark the Battletoad'"
          PatternToAdd = 6165
        Case 231957
          PatternNamed = "Dom Pattern of 'Zoetic Oak'"
          PatternToAdd = 6166
      
' Shake 192 and 195 needed to be seperated out

        Case 232211
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Shake' (QL 192)"
          PatternToAdd = 1109
        Case 232210
          PatternNamed = "Aban-Bhotar Assembly of 'Shake' (QL 192)"
          PatternToAdd = 2109
        Case 242762
          PatternNamed = "Abhan Pattern 'Shake' (QL 192)"
          PatternToAdd = 3109
        Case 232207
          PatternNamed = "Bhotaar Pattern 'Shake' (QL 192)"
          PatternToAdd = 4109
        Case 232208
          PatternNamed = "Chi Pattern of  'Shake' (QL 192)"
          PatternToAdd = 5109
        Case 232209
          PatternNamed = "Dom Pattern of 'Shake' (QL 192)"
          PatternToAdd = 6109

        Case 232103
          PatternNamed = "Aban-Bhotar-Chi Assembly 'Shake' (QL 195)"
          PatternToAdd = 1168
        Case 232102
          PatternNamed = "Aban-Bhotar Assembly of 'Shake' (QL 195)"
          PatternToAdd = 2168
        Case 242750
          PatternNamed = "Abhan Pattern 'Shake' (QL 195)"
          PatternToAdd = 3168
        Case 232099
          PatternNamed = "Bhotaar Pattern 'Shake' (QL 195)"
          PatternToAdd = 4168
        Case 232100
          PatternNamed = "Chi Pattern of  'Shake' (QL 195)"
          PatternToAdd = 5168
        Case 232101
          PatternNamed = "Dom Pattern of 'Shake' (QL 195)"
          PatternToAdd = 6168

      
      
      End Select
      
      PatternMatrix(PatternToAdd) = PatternMatrix(PatternToAdd) + 1
      frmMain!txtAccumulatedPatternPiece.Text = PatternToAdd
      
End Function
      
