Pattern Matcher 1.2.2 Source Notes

*** Introduction ***

(You know, I'm sure there are folks that just want to jump into things.  Go right ahead, but you'll miss out on some important things by skipping this section, things that you'll probably feel bad for not knowing earlier.)

Hello, Programmer!

I'm writing this as a quick overview of how the programs for Pattern Matcher work.  Before we get started, though, I want to make one thing perfectly clear - I am not a programmer.  I'm a person who likes programming, but it's been over a decade since I did any sort of "professional" programming.  

This means that the code can be a bit difficult to follow, since I literally had to teach myself every "new" thing that was put in.  Since I wrote this program, I've learned plenty of "newer", "neater", "more efficient" and "more stylish" ways to do things, but if the code did what it was supposed to do, I generally left it alone.  That said, there have been some "internal" improvement made as revisions were made, so I'd like to think I've learned from mistakes and learned how to do complex things in a simpler way.

So in the end, if you look at a piece of code and say "Gosh, why was it done that way?  Process ______ would have been much better" then please understand that it's likely I didn't even KNOW about Process ______ when I made it.  

Okay, that's out of the way.  I'll try to explain how things work here.

First off, Pattern Matcher is really made of two programs, the Item Loader and the Pattern Matcher.  Since it can be confusing thinking of Pattern Matcher the project and Pattern Matcher the program, I'll call the programs the Loader and the Matcher.

*** How the Loader Works ***

The Loader is a Visual C++ program that "hooks" into an AO process.  The way Anarchy Online works, roughly speaking, is via libraries that constantly communicate with the main program.  When a particular routine is needed, the main AO program finds the appropriate library, communicates with it, the library processes it, the library sends data back to the main AO program, and the main AO program does something with it.

If this sounds familiar to you, then it's probably because you're thinking of ClickSaver - and as it turns out, the programs have a lot in common when it comes to the code surrounding hook injection.  

Item Loader "hooks" into the datastream as data is sent to one particular library - the DataBlockToMessage library in nr.dll.  When data is sent by the server and is processed by AO, it goes to DataBlockToMessage.  

Item Loader is a passive program; it observes the data, never changing it.  When a block of data is received, it stores the block in memory and parses it out, by the following -

Item # in pack | Item Low QL | Item High QL | Item ID

These values are written to disk, in a file called "c:\results.inv".  This is the file that the next part, the Matcher, will use.

Now for some bad news.  Pattern Matcher uses an older version (1.0.4) of Item Loader, the source of which I've unfortunately lost due to a hard drive crash in late 2004.  The good news is, I still have source for Item Loader, but it's in a newer version than the one that Pattern Matcher uses.  If you look at the source for Item Loader 1.0.7 (the last version I made) I commented out the "old style" of doing things, so it'd be simple to remove the current writing scheme and replace it with the older method.  Sorry about that.

*** How the Matcher Works ***

For good or ill, I ended up doing things the "long way" with the Matcher; it reads data from "c:\results.inv" and simply matches it up in a lookup table.  It starts by finding a "D" pattern, and then working its way up like this -

If a matching "ABC" pattern is found, stop. 
  otherwise if a matching "C" pattern is found, then
    if a matching "AB" pattern is found, stop.
      otherwise if a matching "B" pattern is found, then
        if a matching "A" pattern is found, stop.
          
- and if it "falls out" of the if-then-else, for example if a "D" pattern is found with no "C" pattern, then it moves on.  

When a match is found, the pattern pieces involved are marked so as not to flag another match.  