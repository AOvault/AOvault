Pattern Matcher v1.3.4 (December 11, 2005)
by Covenant (petnamer@sbcglobal.net)
http://www.halorn.com

Quick start
-----------

Place your pattern pieces into backpacks.  Either shut down AO and then restart it or go through a zone boundary such as from outside of a shop into a shop (an important, non-optional step).  Run the Item Loader program, and with Item Loader running, open each backpack.  Close Item Loader and then run Pattern Matcher.  Set your Anarchy Online directory in the Options screen.  Press the "Sort" button and the results are in "patterns.txt" in your Pattern Matcher directory.

What is Pattern Matcher?
------------------------

Pattern Matcher is a program that will scan through selected packs for "Pocket Boss" patterns.  It will then figure out which patterns can be assembled into "complete" patterns, and give you a list of the left-over patterns as well.  It does this by reading in the contents of a backpack as it is opened, and then sorting the accumulated pieces.

Why did you make this?
----------------------

After accumulating over a dozen packs of assorted pocket boss pattern pieces, I spent far too much time searching through packs, relying on memory and the occasional scribbled note on which ones I could actually assemble.  Plus, being a tradeskiller, I was eternally receiving odd pieces from friends with the occasional "Got anything that would complete what I have?"

So, rather than lose my sanity doing this over and over, I figured I'd lose it only once by writing a program to sort it.

How do I install it?
--------------------

No installation is required.  However, since the program creates an output file (called "patterns.txt"), it is recommend to place Pattern Matcher in its own directory.

How do I use it?
----------------

1. Take your pocket boss patterns and place them into backpacks.  

2. Restart AO or go through a zone (from outside of a shop into a shop, for instance).  THIS IS NOT AN OPTIONAL STEP!  Windowed mode is strongly recommended for this.

3. Start the Item Loader program (again, windowed mode is recommended while using these programs).  

4. Open each pack.  You do not need to close the backpacks, but you can if it will help you.  The program will catch "duplicate" packs, so if you accidently open a pack twice it will not affect the results.

5. Close the Item Loader program.  

6. Run Pattern Matcher, and press the button labelled "Sort".

7. Go to the directory you placed Pattern Matcher in, and locate the file "patterns.txt" - this file contains the program's report.  

How can I accumulate patterns for more than one player?
----------------------------------------------------------

Simply log off one character, start AO with another character, and re-run Item Loader.  The backpack data will accumulate until you run the Pattern Matcher.

Known Issues
------------

1. Some pocket boss pieces are not included, because it is impossible to assemble a complete set - one or more pieces needed to assemble it (such as an Abhan piece) simply don't exist in-game.  Since these can't be assembled, they're not inventoried.

2. The Item Loader program uses the MadCodeHook library, similar to Clicksaver.  If you have trouble running Clicksaver you might also have trouble running the Item Loader program. 

Revision History
----------------

1.3.4 - Updated to work with the 16.1 patch.  Please note that as a result, Pattern Matcher will need to know where your Anarchy Online directory is located (this can be set in options, and should only need to be done once).

1.3.3 - The program now recognizes the pocket bosses "Anansi" and "Anansi's Adherent".

1.3.2 - Added better error handling in the event that the program encounters corrupt data, duplicate backpacks, or missing files.  

      - Added a "manual purge" option to delete data files between sessions.  Hopefully this won't be needed, but it exists in the event that duplicates occur again.

1.3.1 - Added listing for partial matches for A + B, A + B + C, or AB + C pieces (in other words, pieces that could actually be combined into something).

1.3.0 - Updated to work with the 15.9 patch.

      - The program will now keep a running tally of patterns and matches as it runs, giving the user a better notion of how the program is progressing.

1.2.2 - The two versions of Shake (QL 192 and QL 195) are now seperate entries on the unused pattern graph.

1.2.1 - Updated to work with the new AI expansion.

1.2.0 - Added the unused pattern graph for listing out numerically the patterns not used in a match.

1.1.2 - The program now properly deletes temporary files after use, rather than keeping them around (and potentially mixing results with previous runs of the program).

1.1.1 - Fixed a formatting error (similar to 1.1.0's fix) with K'a.

1.1.0 - The pocket bosses Imk'a and Ats'u weren't being displayed correctly because of apostrophes, and are now fixed.  

1.0.0 - First actual release.

      - Replaced the Bank Tracker Demo program with the Item Loader program (for greater accuracy and ease of use).

0.5.3 - Updated URL to reflect the new server.

0.5.2 - Seperated the QL 192 and QL 195 patterns of 'Shake' into their own seperate match entries, since both types are found in-game.

      - Added the FAQ.

0.5.1 - Deleted the temporary file used by the program during sorting.

0.5.0 - First beta version.

Thanks to
---------

I'd like to thank :

Mig for creating the Bank Tracker Demo program.  Even though Pattern Matcher no longer uses this program, I owe Mig a debt of gratitude for showing that backpack datastream reading was possible.

The fine folks at Auno.org (www.auno.org) for making the database easier to go through.  I also used the database from Anarchy Mainframe (www.aomainframe.net) for doublechecking, but any database errors that crop up are my fault, not theirs.