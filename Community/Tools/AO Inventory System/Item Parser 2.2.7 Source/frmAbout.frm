VERSION 5.00
Begin VB.Form frmAbout 
   Caption         =   "About Item Parser"
   ClientHeight    =   6315
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5535
   Icon            =   "frmAbout.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6315
   ScaleWidth      =   5535
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      AutoSize        =   -1  'True
      Height          =   2775
      Left            =   480
      Picture         =   "frmAbout.frx":0ECA
      ScaleHeight     =   2715
      ScaleWidth      =   4515
      TabIndex        =   1
      Top             =   360
      Width           =   4575
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Height          =   495
      Left            =   1980
      TabIndex        =   0
      Top             =   5490
      Width           =   1575
   End
   Begin VB.Label labelAboutText 
      Alignment       =   2  'Center
      Height          =   1815
      Left            =   720
      TabIndex        =   2
      Top             =   3450
      Width           =   4095
   End
End
Attribute VB_Name = "frmAbout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdOk_Click()
  Unload Me
End Sub

Private Sub form_load()
  labelAboutText.Caption = "Item Parser v" & App.Major & "." & App.Minor & "." & App.Revision & vbCrLf & "Coded by Covenant" & vbCrLf & "(petnamer@sbcglobal.net)" & vbCrLf + "Homepage - http://www.halorn.com" & vbCrLf
  labelAboutText.Caption = labelAboutText.Caption & vbCrLf + "Anarchy Online is copyright Funcom GmbH (www.funcom.com)"

End Sub
  


