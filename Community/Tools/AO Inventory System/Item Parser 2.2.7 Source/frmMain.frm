VERSION 5.00
Begin VB.Form frmMain 
   Caption         =   "Item Parser"
   ClientHeight    =   4308
   ClientLeft      =   60
   ClientTop       =   348
   ClientWidth     =   7812
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   4308
   ScaleWidth      =   7812
   StartUpPosition =   3  'Windows Default
   Begin VB.OptionButton radioUseAOMinfo 
      Caption         =   "aomainframe.info"
      Height          =   255
      Left            =   5400
      TabIndex        =   30
      Top             =   2160
      Width           =   1695
   End
   Begin VB.OptionButton radioUseAOMcom 
      Caption         =   "aomainframe.com"
      Height          =   255
      Left            =   5400
      TabIndex        =   29
      Top             =   1800
      Width           =   1695
   End
   Begin VB.OptionButton radioUseAOMnet 
      Caption         =   "aomainframe.net"
      Height          =   255
      Left            =   5400
      TabIndex        =   28
      Top             =   1440
      Width           =   1695
   End
   Begin VB.OptionButton radioUseAUNO 
      Caption         =   "auno.org"
      Height          =   255
      Left            =   5400
      TabIndex        =   27
      Top             =   1080
      Width           =   1695
   End
   Begin VB.CheckBox chkFlagUNIQUE 
      Caption         =   "Check2"
      Height          =   195
      Left            =   4920
      TabIndex        =   26
      Top             =   3840
      Width           =   255
   End
   Begin VB.CheckBox chkFlagNODROP 
      Caption         =   "Check1"
      Height          =   195
      Left            =   4920
      TabIndex        =   25
      Top             =   3000
      Width           =   195
   End
   Begin VB.CheckBox chkIncludeUNIQUE 
      Caption         =   "Check2"
      Height          =   195
      Left            =   4920
      TabIndex        =   21
      Top             =   3480
      Value           =   1  'Checked
      Width           =   255
   End
   Begin VB.CheckBox chkIncludeNODROP 
      Height          =   255
      Left            =   4920
      TabIndex        =   20
      Top             =   2640
      Value           =   1  'Checked
      Width           =   255
   End
   Begin VB.CommandButton cmdOptions 
      Caption         =   "&Options"
      Height          =   495
      Left            =   2520
      TabIndex        =   17
      Top             =   2760
      Width           =   1815
   End
   Begin VB.PictureBox Picture10 
      Height          =   255
      Left            =   187
      Picture         =   "frmMain.frx":0ECA
      ScaleHeight     =   204
      ScaleWidth      =   204
      TabIndex        =   15
      Top             =   120
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox Picture9 
      Height          =   255
      Left            =   187
      Picture         =   "frmMain.frx":875C
      ScaleHeight     =   204
      ScaleWidth      =   204
      TabIndex        =   14
      Top             =   120
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox Picture8 
      Height          =   255
      Left            =   187
      Picture         =   "frmMain.frx":FFEE
      ScaleHeight     =   204
      ScaleWidth      =   204
      TabIndex        =   13
      Top             =   120
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox Picture7 
      Height          =   255
      Left            =   187
      Picture         =   "frmMain.frx":17880
      ScaleHeight     =   204
      ScaleWidth      =   204
      TabIndex        =   12
      Top             =   120
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox Picture6 
      Height          =   255
      Left            =   187
      Picture         =   "frmMain.frx":1F112
      ScaleHeight     =   204
      ScaleWidth      =   204
      TabIndex        =   11
      Top             =   120
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox Picture5 
      Height          =   255
      Left            =   187
      Picture         =   "frmMain.frx":269A4
      ScaleHeight     =   204
      ScaleWidth      =   204
      TabIndex        =   10
      Top             =   120
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox Picture4 
      Height          =   255
      Left            =   187
      Picture         =   "frmMain.frx":2E236
      ScaleHeight     =   204
      ScaleWidth      =   204
      TabIndex        =   9
      Top             =   120
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox Picture3 
      Height          =   255
      Left            =   187
      Picture         =   "frmMain.frx":35AC8
      ScaleHeight     =   204
      ScaleWidth      =   204
      TabIndex        =   7
      Top             =   120
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox Picture2 
      Height          =   255
      Left            =   187
      Picture         =   "frmMain.frx":3D35A
      ScaleHeight     =   204
      ScaleWidth      =   204
      TabIndex        =   6
      Top             =   120
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox Picture1 
      AutoSize        =   -1  'True
      Height          =   972
      Left            =   1507
      Picture         =   "frmMain.frx":44BEC
      ScaleHeight     =   924
      ScaleWidth      =   1596
      TabIndex        =   4
      Top             =   720
      Width           =   1644
   End
   Begin VB.TextBox txtOutputDirectoryName 
      Height          =   285
      Left            =   2302
      TabIndex        =   0
      Top             =   960
      Width           =   2295
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "E&xit"
      Height          =   495
      Left            =   2520
      TabIndex        =   8
      Top             =   3600
      Width           =   1815
   End
   Begin VB.CommandButton cmdAbout 
      Caption         =   "&About"
      Height          =   495
      Left            =   360
      TabIndex        =   5
      Top             =   3600
      Width           =   1815
   End
   Begin VB.CommandButton cmdPasteAndSort 
      Caption         =   "&Create Report"
      Height          =   495
      Left            =   360
      TabIndex        =   2
      Top             =   2760
      Width           =   1815
   End
   Begin VB.Label Label6 
      Caption         =   "Label UNIQUE items in results"
      Height          =   255
      Left            =   5160
      TabIndex        =   24
      Top             =   3840
      Width           =   2655
   End
   Begin VB.Label Label5 
      Caption         =   "Label NODROP items in results"
      Height          =   255
      Left            =   5160
      TabIndex        =   23
      Top             =   3000
      Width           =   2535
   End
   Begin VB.Label Label4 
      Caption         =   "Include UNIQUE items in results"
      Height          =   255
      Left            =   5160
      TabIndex        =   22
      Top             =   3480
      Width           =   2535
   End
   Begin VB.Label Label3 
      Caption         =   "Include NODROP items in results"
      Height          =   255
      Left            =   5160
      TabIndex        =   19
      Top             =   2640
      Width           =   2535
   End
   Begin VB.Label Label2 
      Caption         =   "Server to use for item information"
      Height          =   255
      Left            =   5040
      TabIndex        =   18
      Top             =   720
      Width           =   2655
   End
   Begin VB.Label labelIfLeetSwimsSlowly 
      Alignment       =   2  'Center
      Caption         =   "If Leetie is swimming slowly, minimize or close other programs (including AO) to improve performance"
      Height          =   615
      Left            =   382
      TabIndex        =   16
      Top             =   2160
      Width           =   4335
   End
   Begin VB.Label Label1 
      Caption         =   "Name of Output Directory"
      Height          =   255
      Left            =   262
      TabIndex        =   3
      Top             =   960
      Width           =   1935
   End
   Begin VB.Line Line1 
      Visible         =   0   'False
      X1              =   427
      X2              =   4627
      Y1              =   120
      Y2              =   120
   End
   Begin VB.Label labelPleaseWait 
      Alignment       =   2  'Center
      Height          =   255
      Left            =   300
      TabIndex        =   1
      Top             =   240
      Width           =   7335
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim FoundThisFileOnSearch As String
Dim fso2 As New FileSystemObject
Dim fld As Folder

Dim BaseFileNameForPacks As String
Dim DataFileForBackpackInput As String

Option Explicit

Private Sub chkIncludeNODROP_Click()
If chkIncludeNODROP.Value = 0 Then
  chkFlagNODROP.Visible = False
  Label5.Visible = False
End If

If chkIncludeNODROP.Value = 1 Then
  chkFlagNODROP.Visible = True
  Label5.Visible = True
End If

End Sub

Private Sub chkIncludeUNIQUE_Click()
If chkIncludeUNIQUE.Value = 0 Then
  chkFlagUNIQUE.Visible = False
  Label6.Visible = False
End If

If chkIncludeUNIQUE.Value = 1 Then
  chkFlagUNIQUE.Visible = True
  Label6.Visible = True
End If

End Sub

Private Sub cmdAbout_Click()
  frmAbout.Show
End Sub

Private Sub cmdOptions_Click()
  frmOptions.Show
End Sub

Private Sub form_load()
  
  
  radioUseAUNO.Value = True
  radioUseAOMnet.Value = False
  radioUseAOMcom.Value = False
  radioUseAOMinfo.Value = False
  
'  chkIncludeNODROP.Value = 1
'  chkIncludeUNIQUE.Value = 1
  cmdPasteAndSort.Visible = True
  cmdOptions.Visible = True
  labelPleaseWait.Caption = "Ready"
  labelIfLeetSwimsSlowly.Visible = False
  Picture1.Visible = False
'  Line1.Visible = True
  Label1.Visible = True
  txtOutputDirectoryName.Visible = True
'  frmOptions!chkDeleteDataFiles.Visible = True
'  frmOptions!chkPlacePackNamesAfterItemNames.Visible = True


End Sub

Private Sub cmdExit_Click()
  Dim Frm As Form
  For Each Frm In Forms
    Unload Frm
    Set Frm = Nothing
  Next Frm
  End
End Sub
Private Sub DeleteTheTemporaryFiles()
  On Error Resume Next
  
  Kill (DataFileForBackpackInput)
  
'  Kill ("c:\working.tmp")

Errortrap:
  If Err.Number = 53 Then
'       Who cares... do nothing at all
  End If
End Sub

Private Sub CheckToSeeIfDatabaseFileExists()
  On Error GoTo Errortrap
  Const ForReading = 1, ForWriting = 2, ForAppending = 8
  Dim fso, f
  Set fso = CreateObject("Scripting.FileSystemObject")
  Set f = fso.OpenTextFile("itemdb.db", ForReading)
  f.Close

Errortrap:
  If Err.Number = 53 Then
    MsgBox "The required database extract was not found." & vbCrLf & "Run the DumpAOItems program first to create a database extract." & vbCrLf & "Be sure that Anarchy Online is not running when you do this.", vbExclamation, "A required file is missing!"
    cmdExit_Click 'force exit
  End If
End Sub

Private Sub CheckToSeeIfUserDataFileExists()
  On Error GoTo Errortrap
  Const ForReading = 1, ForWriting = 2, ForAppending = 8
  Dim fso, f
  
  DataFileForBackpackInput = BaseFileNameForPacks & "aois_temp1.inv"
  
  Set fso = CreateObject("Scripting.FileSystemObject")
  Set f = fso.OpenTextFile(DataFileForBackpackInput, ForReading)
  f.Close

Errortrap:
  If Err.Number = 53 Then
    CheckToSeeIfUserDataFileExistsInRootDirectory
  End If
End Sub

Private Sub CheckToSeeIfUserDataFileExistsInRootDirectory()
  On Error GoTo Errortrap
  Const ForReading = 1, ForWriting = 2, ForAppending = 8
  Dim fso, f
  
  DataFileForBackpackInput = "c:\aois_temp1.inv"
  
  Set fso = CreateObject("Scripting.FileSystemObject")
  Set f = fso.OpenTextFile(DataFileForBackpackInput, ForReading)
  f.Close

Errortrap:
  If Err.Number = 53 Then
    MsgBox "No user data was found.  Please run the Item Loader program to read in backpack data, and then run this program again.", vbExclamation, "A required file is missing!"
    cmdExit_Click 'force exit
  End If
End Sub




Public Function FileExists(ByVal sDirName As String) As Boolean
  On Error GoTo Errortrap
  
  Const ForReading = 1, ForWriting = 2, ForAppending = 8
  Dim fso, f
  
  Set fso = CreateObject("Scripting.FileSystemObject")
  Set f = fso.OpenTextFile(sDirName, ForReading)
  f.Close

  FileExists = True

Errortrap:
  If Err.Number = 53 Then
    FileExists = False
'MsgBox ("file not found..." & sDirName)
  End If

End Function

Public Sub cmdPasteAndSort_Click()
  
  Dim LineToIgnore As String
  Const ForReading = 1, ForWriting = 2, ForAppending = 8
  Dim fso, f
  Set fso = CreateObject("Scripting.FileSystemObject")
  Set f = fso.OpenTextFile("aois.cfg", ForReading)
  LineToIgnore = f.ReadLine
  LineToIgnore = f.ReadLine
  BaseFileNameForPacks = f.ReadLine
  f.Close

  If BaseFileNameForPacks = "" Then
    MsgBox ("You will need to set your Anarchy Online directory location before proceeding.")
    cmdOptions_Click
Else
  
'  cmdPasteAndSort.Visible = False
'  cmdOptions.Visible = False
  RunThroughPacks
End If


End Sub

Public Sub RunThroughPacks()
  
' This is the main routine of the program.  This
' routine will do the following -
'
' Verify that files to work on actually do exist.
' Set up the directory for writing (informing the user
'
'   that a default directory name will be used if none
'   was entered by the user.
'
  Dim LineToIgnore As String

  Const ForReading = 1, ForWriting = 2, ForAppending = 8
  Dim fso, f
  Set fso = CreateObject("Scripting.FileSystemObject")
  Set f = fso.OpenTextFile("aois.cfg", ForReading)
  LineToIgnore = f.ReadLine
  LineToIgnore = f.ReadLine
  BaseFileNameForPacks = f.ReadLine
  f.Close




  CheckToSeeIfDatabaseFileExists ' stop if this file isn't there.
  CheckToSeeIfUserDataFileExists ' is data stored in the AO directory or in the root directory?


' In the event that no directory name was selected,
' use the default of My Inventory Results.
  If txtOutputDirectoryName.Text = "" Then
    MsgBox ("Default path name of " & """" & "My Inventory Results" & """" & " will be used.")
    txtOutputDirectoryName.Text = "My Inventory Results"
  End If
  If txtOutputDirectoryName.Text = Null Then
    MsgBox ("Default path name of " & """" & "My Inventory Results" & """" & " will be used.")
    txtOutputDirectoryName.Text = "My Inventory Results"
  End If
  
  labelPleaseWait.Caption = "Loading inventory contents"
  
  
' There are a lot of variables to declare.  This is
' the main area for variable declaration for all
' variables used through this routine.
  
' Variables for file management.  The lack of fso2 is
' not a mistype.  f and the fso system has alreadt been defined.
  Dim fso2, f2
  Dim fso3, f3
  Dim fso4, f4
  Set fso = CreateObject("Scripting.FileSystemObject")
  Set fso3 = CreateObject("Scripting.FileSystemObject")
  Set fso4 = CreateObject("Scripting.FileSystemObject")
  
' Variables for deciding between aodb and auno
  Const AUNO = 1, AODB = 2
  Dim AunoOrAodb As Long
  Dim ExcludeDueToNoDropOrUnique As Boolean

' Variables for handling custom backpack names
  Dim CounterForEndInfo As Long
  Dim ItemsReadSinceLastEndInfo As Long
  Dim NumberOfDirectoriesSearchedThrough As Long
  Dim NumberOfFilesSearchedThrough As Long
  Dim TargetFileNameForThisPackSize As Currency
  Dim TargetFileNameForThisPack As String
  Dim ThisItemIsInCustomPack(400010) As String
  Dim PackTempLong As Long
  Dim PackViewingTargetString As String
  Dim TempPackViewWorkingString As String
  Dim PackFileToView As String
  Dim TempPackSpecificCounter As Long
  Dim MostRecentPackTypeFound As String
  Dim PackSpecificCounter As Long
  Dim PackSpecificType(40010) As String
  Dim PackCustomName(40010) As String
  
' Variables for configuration file management
  Dim UserNameFromConfigFile As String
  
' Variables for reading in item information (temporary
' and intermediate files
  Dim BackPackTypeData As String
  Dim filecounter As Long
  Dim tempstring As String
  Dim TotalSuccesses As Long
  Dim SuccessPackItemFound As Long
  Dim TotalPacksFound As Long

' Variables for preserving the actual item information
  Dim ItemIsInCategory(400010) As Long
  Dim ItemInBackpack(400010) As String
  Dim ItemInBackpackID(400010) As String
  Dim ItemInBackpackQL(400010) As String
  Dim ItemInBackpackStack(400010) As String
  Dim PackID(400010) As String
  Dim BackPackType(400010) As String
  Dim ItemIsInPackNumber(400010) As Long
  Dim BackPackInventoried(400010) As String
  Dim TheUserID As String
  Dim ItemInBackpackNodropStatus(400010) As String
  Dim ItemInBackpackUniqueStatus(400010) As String
  
' Variables for handling the database.
  Dim LineReadFromDatabase As String
  Dim DatabaseItemName(400010) As String
  Dim DatabaseItemNumber(400010) As Long
  Dim NumberOfDatabaseItems As Long
  Dim LookingAtThisItem As Long
  Dim ItemNumberAsAStringForTheProgessUpdate As String
  Dim CurrentItemReadFromDatabase As Long
  Dim DatabaseItemIsNodrop(400010) As String
  Dim DatabaseItemIsUnique(400010) As String

' Variables for the contents - database matching.
  Dim CurrentNumberOfBackpacksCounted As Long
  Dim CurrentWorkingItem As Long

' Variables for dealing with the Who Am I routine.
  Dim IsThisAWhoAmIReport As Boolean
  Dim AunoID As String
  Dim WhoAmICounter As Long
  Dim ItemOnTheCharacterQL(100) As String
  Dim AnotherCounter As Long
  Dim ItemOnTheCharacter(100) As String
  Dim ItemOnTheCharacterLocation(100) As Long
  Dim ItemOnTheCharacterHeader(100) As String
  Dim ItemOnTheCharacterID(100) As String
  Dim ItemOnTheCharacterNumber As Long
  
' Variables for writing reports.
  Dim LinesWritten As Long

' Temporary variables for counters and sorting.
  Dim i As Long
  Dim j As Long
  Dim k As Long
  Dim h As String

' Set up the FSO for reading in the config file
  Set fso = CreateObject("Scripting.FileSystemObject")
  Set f = fso.OpenTextFile(DataFileForBackpackInput, ForReading)
  
' Set counters to default values.
  SuccessPackItemFound = 0
  filecounter = 1
  TotalPacksFound = 0
  TotalSuccesses = 0
  PackSpecificCounter = 0
  
' Default setting is that this is not a Who Am I report.
  IsThisAWhoAmIReport = False
  
' Set mouse to the "hourglass" to show program is busy.
  MousePointer = vbHourglass
 
' Begin reading in information, until the end of the
' data stream has been reached.
  Do While Not f.AtEndOfStream

' subDelay is required to make the system "catch up" to
' itself, otherwise it looks like the program has
' locked up during operation.
    subDelay (0.001)
    
' Read in one line of text, and set up a series of
' if-then responses to place information the data fields.
    tempstring = f.ReadLine
    
' Check for backpack type, and assign a value accordingly.
    If Mid(tempstring, 1, 3) = "BP-" Then
      BackPackTypeData = Mid(tempstring, 4, 6)
  ' Set the original name to Unknown Pack Type.  That way
  ' it will be overwritten by an actual pack type.  If
  ' a pack type was found but could not be defined, then
  ' at least it will be known.
      BackPackType(TotalSuccesses + 1) = "Unknown Pack Type[" + BackPackTypeData + "]"
      Select Case BackPackTypeData
        Case "099241"
          BackPackType(TotalSuccesses + 1) = "Medium Backpack"
        Case "099228"
          BackPackType(TotalSuccesses + 1) = "Small Backpack"
        Case "099302"
          BackPackType(TotalSuccesses + 1) = "Book of Knowledge"
        Case "157685"
          BackPackType(TotalSuccesses + 1) = "Large Backpack - Sniper"
        Case "157695"
          BackPackType(TotalSuccesses + 1) = "Large Backpack - Vehicles"
        Case "157682"
          BackPackType(TotalSuccesses + 1) = "Large Backpack - Sharp Star"
        Case "157683"
          BackPackType(TotalSuccesses + 1) = "Large Backpack - Mountain Top"
        Case "157684"
          BackPackType(TotalSuccesses + 1) = "Large Backpack - Black Button"
        Case "157692"
          BackPackType(TotalSuccesses + 1) = "Large Backpack - Medical"
        Case "157693"
          BackPackType(TotalSuccesses + 1) = "Large Backpack - Nano Crystals"
        Case "157691"
          BackPackType(TotalSuccesses + 1) = "Large Backpack - Implants"
        Case "157687"
          BackPackType(TotalSuccesses + 1) = "Large Backpack - Swimmer"
        Case "157689"
          BackPackType(TotalSuccesses + 1) = "Large Backpack - Clothing"
        Case "157688"
          BackPackType(TotalSuccesses + 1) = "Large Backpack - Speeder"
        Case "157690"
          BackPackType(TotalSuccesses + 1) = "Large Backpack - Weapons"
        Case "157694"
          BackPackType(TotalSuccesses + 1) = "Large Backpack - Trash"
        Case "157686"
          BackPackType(TotalSuccesses + 1) = "Large Backpack - Cold Front"
        Case "143832"
          BackPackType(TotalSuccesses + 1) = "Large Backpack"
        Case "152039"
          BackPackType(TotalSuccesses + 1) = "The Pioneer Backpack"
        Case "156831"
          BackPackType(TotalSuccesses + 1) = "The Pioneer Backpack (Modified)"
        Case "164564"
          BackPackType(TotalSuccesses + 1) = "Notum Threaded Backpack"
        Case "223770"
          BackPackType(TotalSuccesses + 1) = "Smart Backpack"
        Case "165201"
          BackPackType(TotalSuccesses + 1) = "Beeping Backpack"
        Case "207254"
          BackPackType(TotalSuccesses + 1) = "Personal Backpack"
        Case "156599"
          IsThisAWhoAmIReport = True
        Case "245657"
          BackPackType(TotalSuccesses + 1) = "Blackpack"
        Case "158790"
          BackPackType(TotalSuccesses + 1) = "Robust Backpack"
        Case "245595"
          BackPackType(TotalSuccesses + 1) = "Doctor's Pill Pack"
      End Select
      MostRecentPackTypeFound = BackPackType(TotalSuccesses + 1)
    End If

' Identify the pack ID.  This is a unique number for each
' backpack item in the game - no two are ever the same.
    If Mid(tempstring, 1, 7) = "PackID=" Then
      PackSpecificCounter = PackSpecificCounter + 1
      PackSpecificType(PackSpecificCounter) = MostRecentPackTypeFound
      PackID(PackSpecificCounter) = Mid(tempstring, 8, (Len(tempstring) - 7))
    End If
    
' Identify the user ID.  This really only needs to be
' determined one time, since it never changes.
    If Mid(tempstring, 1, 7) = "UserID=" Then
      TheUserID = Mid(tempstring, 8, (Len(tempstring) - 7))
    End If

' Identify the item Low ID.  This is the ID used to
' positively identify the item in the database matching
' routine.
    If Mid(tempstring, 1, 6) = "LowID=" Then
      ItemInBackpack(TotalSuccesses + 1) = Mid(tempstring, 7, (Len(tempstring) - 6))
      ItemInBackpackID(TotalSuccesses + 1) = Trim(ItemInBackpack(TotalSuccesses + 1))
    End If

' Identify the item QL.
    If Mid(tempstring, 1, 7) = "ItemQL=" Then
      ItemInBackpackQL(TotalSuccesses + 1) = Mid(tempstring, 8, (Len(tempstring) - 7))
    End If

' Identify the number of items in a stack.  If the item
' is a singular item (i.e. it is not a stack), then this
' value is 1.  Otherwise, it is the number of items in
' the stack
    If Mid(tempstring, 1, 6) = "Stack=" Then
      ItemInBackpackStack(TotalSuccesses + 1) = Mid(tempstring, 7, (Len(tempstring) - 6))
      SuccessPackItemFound = 1
    End If
    
' If we find the word "Backpack" then we're at a backpack
' header, which means we need to update how many packs
' we have found, and denote that the item currently
' looked at is in fact a backpack header.  Backpack header
' items are handled by the item name being "BPHOLDER",
' and the item QL is a sequential number (starting with 1)
' denoting how many packs have been found so far.
    If (Len(tempstring) > 8) Then ' Only check if line length could have the word backpack in it
      If Left(tempstring, 8) = "Backpack" Then
        TotalPacksFound = TotalPacksFound + 1
        TotalSuccesses = TotalSuccesses + 1
        ItemInBackpack(TotalSuccesses) = "BPHOLDER"
        ItemInBackpackQL(TotalSuccesses) = TotalPacksFound
      End If
    End If
    
' If the item found is an actual item (and not just
' information about the backpack), then increment
' TotalSuccesses by one, add another number to the
' number of items since the last pack has been read, and
' reset the successful pack item found value back to zero.
    If SuccessPackItemFound = 1 Then
      TotalSuccesses = TotalSuccesses + 1
      ItemsReadSinceLastEndInfo = ItemsReadSinceLastEndInfo + 1
      SuccessPackItemFound = 0
    End If
    
' EndInfo means we are at the end of a backpack.  Now
' we cycle through pack IDs that have already been
' inventoried, and if a duplicate pack ID is found, then
' this backpack has already been inventoried... and
' therefore, we have to throw out all the contents of
' this duplicate pack.
    If Left(tempstring, 7) = "EndInfo" Then
      CounterForEndInfo = 1
      Do While Not CounterForEndInfo = PackSpecificCounter
        If PackID(PackSpecificCounter) = PackID(CounterForEndInfo) Then
' A duplicate has been found.  We will reset
' TotalSuccesses back by the number of items just
' read in.

' Take away whatever the number of contents in the
' last pack just read in.
          TotalSuccesses = TotalSuccesses - ItemsReadSinceLastEndInfo
          
' Take away 1 for the pack those items were in since
' the pack itself is considered an item too for the
' purposes of backpack item type
          TotalSuccesses = TotalSuccesses - 1
' Forget this pack ever existed.
          PackID(CounterForEndInfo) = "IGNORETHISPACK"
        End If
      CounterForEndInfo = CounterForEndInfo + 1
      Loop
' Reset ItemsReadSinceLastEndInfo back to zero, since
' this backpack has been checked through and this
' counter needs to build up again with the number of
' items read in the next pack.
    ItemsReadSinceLastEndInfo = 0
    End If

' The variable filecounter holds how many lines of
' text have been read in total.  This is a debugging
' variable more than anything else, and is not used
' by the program itself aside from file count tracking.
    filecounter = filecounter + 1
  
  
' Constantly updating the screen with this caption
' helps to avoid a "locked up" appearance.
    labelPleaseWait.Caption = "Loading inventory contents"
' And now loop back and get more information.  This loop
' ends when the end of the filestream is reached.
  Loop
  
' The raw data file has been read in completely, so it is
' time to close it.
  f.Close
  
' In the event that no items were found whatsoever, we
' need to notify the user and exit the program.
  If TotalSuccesses = 0 Then
    MsgBox ("No recognized items were found in the inventory list!")
    Dim Frm As Form
    For Each Frm In Forms
      Unload Frm
      Set Frm = Nothing
    Next Frm
    End
  End If
  
' Next step will be to check for custom pack names,
' locate the files with the custom pack name information,
' and assign those values to the proper packs.
  
' Read in the configuration file, so we can find the
' AO folder name.  This folder name is the base
' directory in searching for custom pack names.  We
' will ignore a few lines because they're not important.
  Set f = fso.OpenTextFile("aois.cfg", ForReading)
  LineToIgnore = f.ReadLine
  LineToIgnore = f.ReadLine
  BaseFileNameForPacks = f.ReadLine
  f.Close
  
' Build the actual text that we will be looking for inside
' the Container XML files.
  PackViewingTargetString = "    <String name="
  PackViewingTargetString = PackViewingTargetString & """"
  PackViewingTargetString = PackViewingTargetString & "container_name"
  PackViewingTargetString = PackViewingTargetString & """"
  PackViewingTargetString = PackViewingTargetString & " value='&quot;"
  
  TempPackSpecificCounter = 0
  Do While Not TempPackSpecificCounter = (PackSpecificCounter) 'so it will do it at least once
    TempPackSpecificCounter = TempPackSpecificCounter + 1
    PackFileToView = ""
    TargetFileNameForThisPack = "Container_51017x" & PackID(TempPackSpecificCounter) & ".xml"
    TargetFileNameForThisPackSize = FindFile(BaseFileNameForPacks, TargetFileNameForThisPack, NumberOfDirectoriesSearchedThrough, NumberOfFilesSearchedThrough)
    PackFileToView = FoundThisFileOnSearch
' This If-Then is needed because otherwise a previously
' located pack name will be used as a match.  This was
' a problem with v1.8.2 and fixed with v1.9.0.
    If Right(PackFileToView, Len(TargetFileNameForThisPack)) <> TargetFileNameForThisPack Then
      PackFileToView = ""
    End If

' So in the end, PackFileToView is the filename to
' evaluate.  If the file does in fact exist, see if
' the target text is present - and if it is, extract
' out the custom pack name.
    If FileExists(PackFileToView) Then
      Set f = fso.OpenTextFile(PackFileToView, ForReading)
      Do While Not (f.AtEndOfStream)
        TempPackViewWorkingString = f.ReadLine
        If Mid(TempPackViewWorkingString, 1, Len(PackViewingTargetString)) = PackViewingTargetString Then
          PackTempLong = Len(TempPackViewWorkingString)
          PackTempLong = PackTempLong - 10
          PackTempLong = PackTempLong - Len(PackViewingTargetString)
          PackCustomName(TempPackSpecificCounter) = Mid(TempPackViewWorkingString, (Len(PackViewingTargetString) + 1), PackTempLong)
        End If
      Loop
      f.Close
    End If
  Loop


' Next, we will read the whole darn database extract
' into memory.  This whole routine is liberally sprinkled
' with subDelay calls because otherwise it gets ahead
' of itself and it looks like it locks up.
  subDelay (0.01)
  
' Update the user on what we're doing, and let them know
' what to do is our dear little leet appears to be
' swimming slowly.
  labelPleaseWait.Caption = "Reading in database extract"
  labelIfLeetSwimsSlowly.Visible = True
  subDelay (0.01)

  Set f = fso.OpenTextFile("itemdb.db", ForReading)
  
' Reset counters used to read in the database, and
' turn on the leet picture.
  NumberOfDatabaseItems = 0
  Picture1.Visible = True
  Label1.Visible = False
  txtOutputDirectoryName.Visible = False

' This last one might seem like a silly move, but I
' tend to be paranoid in this.
  DatabaseItemName(0) = ""
  
  
  
' Here goes... open the database filestream, and read in
' the entire contents.
  Do Until (f.AtEndOfStream)
    subDelay (0.00001)
    ItemNumberAsAStringForTheProgessUpdate = Str(NumberOfDatabaseItems + 1000)
' Update our progress with dear Leetie swimming away.
' The way this works is, when a certain number is reached,
' the leet picture updates itself.  There's a leet
' animation every 500 database items.
      Select Case Right(ItemNumberAsAStringForTheProgessUpdate, 4)
        Case "0011", "4511"
          Picture1.Picture = Picture2.Picture
        Case "0511", "5011"
          Picture1.Picture = Picture3.Picture
        Case "1011", "5511"
          Picture1.Picture = Picture4.Picture
        Case "1511", "6011"
          Picture1.Picture = Picture5.Picture
        Case "2011", "6511"
          Picture1.Picture = Picture6.Picture
        Case "2511", "7011"
          Picture1.Picture = Picture7.Picture
        Case "3011", "7511"
          Picture1.Picture = Picture8.Picture
        Case "3511", "8011"
          Picture1.Picture = Picture9.Picture
        Case "4011", "8511"
          Picture1.Picture = Picture10.Picture
      End Select
    
' Increment the number of database items, read in the
' data, and parse it out into the appropriate variables.
    NumberOfDatabaseItems = NumberOfDatabaseItems + 1
    LineReadFromDatabase = f.ReadLine
    CurrentItemReadFromDatabase = Val(LineReadFromDatabase)
    LookingAtThisItem = CurrentItemReadFromDatabase
    DatabaseItemNumber(LookingAtThisItem) = LookingAtThisItem
    DatabaseItemName(LookingAtThisItem) = f.ReadLine
    LineReadFromDatabase = f.ReadLine
    ItemIsInCategory(LookingAtThisItem) = Val(LineReadFromDatabase)
    DatabaseItemIsNodrop(LookingAtThisItem) = f.ReadLine
    DatabaseItemIsUnique(LookingAtThisItem) = f.ReadLine
  Loop

' Update the user on our progress and shut off the leet
' picture.
  labelIfLeetSwimsSlowly.Visible = False
  labelPleaseWait.Caption = "Assigning database information to items"

' With the database now in memory, we can start looking
' for matches.
  
' Reset variables to zero.
  CurrentNumberOfBackpacksCounted = 0
  j = 0
  
  Do While Not (j = TotalSuccesses)
    j = j + 1
    CurrentWorkingItem = Val(ItemInBackpack(j))
    If DatabaseItemName(CurrentWorkingItem) = "" Then
      ItemInBackpack(j) = "[Unknown Item] " & ItemInBackpack(j)
        Else
      ItemInBackpack(j) = DatabaseItemName(CurrentWorkingItem)
      ItemInBackpackNodropStatus(j) = DatabaseItemIsNodrop(CurrentWorkingItem)
      ItemInBackpackUniqueStatus(j) = DatabaseItemIsUnique(CurrentWorkingItem)
    End If
    If ItemInBackpack(j) = "[Unknown Item] BPHOLDER" Then
      CurrentNumberOfBackpacksCounted = CurrentNumberOfBackpacksCounted + 1
      BackPackInventoried(CurrentNumberOfBackpacksCounted) = BackPackType(j)
        Else
      ItemIsInPackNumber(j) = CurrentNumberOfBackpacksCounted
    End If
  Loop
  f.Close
  
' Now, each item should have a name as well as an ID.
' We need to create the reports.
  subDelay (0.01)
' Update the user on our progress and shut off the leet
' picture.
  labelPleaseWait.Caption = "Parsing done, now creating reports"
  
  subDelay (0.0001)
  
' Create the output directory.  If it already exists,
' then we don't have to create it.
  If DirExists(txtOutputDirectoryName.Text) Then
' do nothing
      Else
    MkDir (txtOutputDirectoryName.Text)
  End If

' For Who Am I reports, this will be a bit different.
' So we need to determine which it is.

If IsThisAWhoAmIReport = False Then

' Reset counters for report writing.
  LinesWritten = 1
  PackSpecificCounter = 0
  
' Since the pipe-seperated format is simply arranged
' raw data, that is the simplest one to create first.
  Set f = fso.OpenTextFile(txtOutputDirectoryName.Text & "\parsed_pipe.txt", ForWriting, True)
  Do While Not (LinesWritten = TotalSuccesses + 1)
    If ItemInBackpack(LinesWritten) = "[Unknown Item] BPHOLDER" Then
      PackSpecificCounter = PackSpecificCounter + 1
    Else
' Format for pipe seperated file as of v1.8.2
' Name of Item | QL | Stack | | Category of Item | Backpack # | Type of Backpack | Directory
'
      j = Val(ItemInBackpackID(LinesWritten)) ' Category of Item
'  Since we are going through each item in each pack,
'  this is a great time to assign the # of each item
'  to each pack.
      ThisItemIsInCustomPack(LinesWritten) = PackCustomName(PackSpecificCounter)
      
      ExcludeDueToNoDropOrUnique = False
      If (chkIncludeNODROP.Value = 1) Then
        ' 1 means include=yes
        ExcludeDueToNoDropOrUnique = False
      End If
      If (chkIncludeUNIQUE.Value = 1) Then
        ' 1 means include=yes
        ExcludeDueToNoDropOrUnique = False
      End If
      If (chkIncludeNODROP.Value = 0) Then
        ' 0 means include=no
        If ItemInBackpackNodropStatus(LinesWritten) = "nd" Then
          ExcludeDueToNoDropOrUnique = True
        End If
      End If
      If (chkIncludeUNIQUE.Value = 0) Then
        ' 0 means include=no
        If ItemInBackpackUniqueStatus(LinesWritten) = "u" Then
          ExcludeDueToNoDropOrUnique = True
        End If
      End If
      If (ExcludeDueToNoDropOrUnique = False) Then
        f.Write ItemInBackpack(LinesWritten) ' Name of Item
        f.Write "|"
        f.Write ItemInBackpackQL(LinesWritten) ' QL
        f.Write "|"
        f.Write ItemInBackpackStack(LinesWritten) ' Stack
        f.Write "|"
        f.Write ItemIsInCategory(j) ' Category of Item
        f.Write "|"
        f.Write PackSpecificCounter ' Backpack #
        f.Write "|"
        f.Write PackSpecificType(PackSpecificCounter) ' Backpack Type
        f.Write "|"
        f.Write txtOutputDirectoryName.Text ' Directory
        f.Write vbCrLf
      End If
    End If
    LinesWritten = LinesWritten + 1
  Loop
  f.Close

' Add [NODROP] and [UNIQUE] tags to the name itself,
' if indicated by the user having selected these controls.

' Reset counters for report writing.
  LinesWritten = 1
  PackSpecificCounter = 0
  
  
  Do While Not (LinesWritten = TotalSuccesses + 1)
    If ItemInBackpack(LinesWritten) <> "[Unknown Item] BPHOLDER" Then
      If chkFlagNODROP.Value = 1 Then
        If ItemInBackpackNodropStatus(LinesWritten) = "nd" Then
          ItemInBackpack(LinesWritten) = ItemInBackpack(LinesWritten) & " (NODROP)"
        End If
      End If
      If chkFlagUNIQUE.Value = 1 Then
        If ItemInBackpackUniqueStatus(LinesWritten) = "u" Then
          ItemInBackpack(LinesWritten) = ItemInBackpack(LinesWritten) & " (UNIQUE)"
        End If
      End If
    End If
    LinesWritten = LinesWritten + 1
  Loop

' Reset counters for report writing.
  LinesWritten = 1
  PackSpecificCounter = 0

' Output as plain text
  Set f = fso.OpenTextFile(txtOutputDirectoryName.Text & "\text_pack.txt", ForWriting, True)
  f.Write "The following output was generated by the AO Inventory System (www.halorn.com)." & vbCrLf
  f.Write "Report created " & Date & " at " & Time & vbCrLf
  Do While Not (LinesWritten = TotalSuccesses + 1)
    If ItemInBackpack(LinesWritten) = "[Unknown Item] BPHOLDER" Then
      f.Write vbCrLf
      PackSpecificCounter = PackSpecificCounter + 1
      f.Write "Backpack # " & PackSpecificCounter & " "
        If PackCustomName(PackSpecificCounter) <> "" Then
          f.Write PackCustomName(PackSpecificCounter) & " "
        End If
      If PackSpecificType(PackSpecificCounter) <> "" Then
        f.Write "(" & PackSpecificType(PackSpecificCounter) & ")"
      End If
      f.Write vbCrLf
    Else
'!!!!!unique/nodrop coding stuff!!!!!!!!!!!!!!!!!!!!!
      ExcludeDueToNoDropOrUnique = False
      If (chkIncludeNODROP.Value = 1) Then
        ' 1 means include=yes
        ExcludeDueToNoDropOrUnique = False
      End If
      If (chkIncludeUNIQUE.Value = 1) Then
        ' 1 means include=yes
        ExcludeDueToNoDropOrUnique = False
      End If
      If (chkIncludeNODROP.Value = 0) Then
        ' 0 means include=no
        If ItemInBackpackNodropStatus(LinesWritten) = "nd" Then
          ExcludeDueToNoDropOrUnique = True
        End If
      End If
      If (chkIncludeUNIQUE.Value = 0) Then
        ' 0 means include=no
        If ItemInBackpackUniqueStatus(LinesWritten) = "u" Then
          ExcludeDueToNoDropOrUnique = True
        End If
      End If
      If (ExcludeDueToNoDropOrUnique = False) Then
        f.Write ItemInBackpack(LinesWritten) & " QL:" & ItemInBackpackQL(LinesWritten)
        If ItemInBackpackStack(LinesWritten) > 1 Then
          f.Write " Stack of " & ItemInBackpackStack(LinesWritten)
        End If
        f.Write vbCrLf
      End If
    End If
    LinesWritten = LinesWritten + 1
  Loop
  f.Close

' Reset counters for report writing.
  PackSpecificCounter = 0
  LinesWritten = 1

' Output as UBB (forum) code linked to database
  Set f = fso.OpenTextFile(txtOutputDirectoryName.Text & "\ubb_pack.txt", ForWriting, True)
  If radioUseAUNO.Value = True Then
    f.Write "[size=-2][color=#009090]Inventory list created by the [url=http://www.halorn.com]Anarchy Online Inventory System[/url] - database reference provided by [url=http://www.auno.org]Auno.org[/url].[/color][/size]"
  End If
  If radioUseAOMnet.Value = True Then
    f.Write "[size=-2][color=#009090]Inventory list created by the [url=http://www.halorn.com]Anarchy Online Inventory System[/url] - database reference provided by [url=http://www.aomainframe.net]aomainframe.net[/url].[/color][/size]"
  End If
  If radioUseAOMcom.Value = True Then
    f.Write "[size=-2][color=#009090]Inventory list created by the [url=http://www.halorn.com]Anarchy Online Inventory System[/url] - database reference provided by [url=http://www.aomainframe.com]aomainframe.com[/url].[/color][/size]"
  End If
  If radioUseAOMinfo.Value = True Then
    f.Write "[size=-2][color=#009090]Inventory list created by the [url=http://www.halorn.com]Anarchy Online Inventory System[/url] - database reference provided by [url=http://www.aomainframe.info]aomainframe.info[/url].[/color][/size]"
  End If
  Do While Not (LinesWritten = TotalSuccesses + 1)
    If ItemInBackpack(LinesWritten) = "[Unknown Item] BPHOLDER" Then
      f.Write vbCrLf
      PackSpecificCounter = PackSpecificCounter + 1
      f.Write "Backpack # " & PackSpecificCounter & " "
        If PackCustomName(PackSpecificCounter) <> "" Then
          f.Write PackCustomName(PackSpecificCounter) & " "
        End If
      If PackSpecificType(PackSpecificCounter) <> "" Then
        f.Write "(" & PackSpecificType(PackSpecificCounter) & ")"
      End If
      f.Write vbCrLf
    Else
      
      ExcludeDueToNoDropOrUnique = False
      If (chkIncludeNODROP.Value = 1) Then
        ' 1 means include=yes
        ExcludeDueToNoDropOrUnique = False
      End If
      If (chkIncludeUNIQUE.Value = 1) Then
        ' 1 means include=yes
        ExcludeDueToNoDropOrUnique = False
      End If
      If (chkIncludeNODROP.Value = 0) Then
        ' 0 means include=no
        If ItemInBackpackNodropStatus(LinesWritten) = "nd" Then
          ExcludeDueToNoDropOrUnique = True
        End If
      End If
      If (chkIncludeUNIQUE.Value = 0) Then
        ' 0 means include=no
        If ItemInBackpackUniqueStatus(LinesWritten) = "u" Then
          ExcludeDueToNoDropOrUnique = True
        End If
      End If
      If (ExcludeDueToNoDropOrUnique = False) Then
        If radioUseAUNO.Value = True Then
          f.Write "[url=http://auno.org/ao/db.php?cmd=view&id=" & ItemInBackpackID(LinesWritten) & "&ql=" & ItemInBackpackQL(LinesWritten) & "]QL " & ItemInBackpackQL(LinesWritten) & " " & ItemInBackpack(LinesWritten) & "[/url]"
        End If
        If radioUseAOMnet.Value = True Then
          f.Write "[url=http://www.aomainframe.net/showitem.asp?LowID=" & ItemInBackpackID(LinesWritten)
          f.Write "&HiID=" & ItemInBackpackID(LinesWritten)
          f.Write "&QL=" & ItemInBackpackQL(LinesWritten) & "]QL " & ItemInBackpackQL(LinesWritten) & " " & ItemInBackpack(LinesWritten) & "[/url]"
        End If
        If radioUseAOMcom.Value = True Then
          f.Write "[url=http://www.aomainframe.com/showitem.asp?LowID=" & ItemInBackpackID(LinesWritten)
          f.Write "&HiID=" & ItemInBackpackID(LinesWritten)
          f.Write "&QL=" & ItemInBackpackQL(LinesWritten) & "]QL " & ItemInBackpackQL(LinesWritten) & " " & ItemInBackpack(LinesWritten) & "[/url]"
        End If
        If radioUseAOMinfo.Value = True Then
          f.Write "[url=http://www.aomainframe.info/showitem.asp?LowID=" & ItemInBackpackID(LinesWritten)
          f.Write "&HiID=" & ItemInBackpackID(LinesWritten)
          f.Write "&QL=" & ItemInBackpackQL(LinesWritten) & "]QL " & ItemInBackpackQL(LinesWritten) & " " & ItemInBackpack(LinesWritten) & "[/url]"
        End If



'  End If
        If ItemInBackpackStack(LinesWritten) > 1 Then
          f.Write " Stack of " & ItemInBackpackStack(LinesWritten)
        End If
        j = Val(ItemInBackpackID(LinesWritten))
        Select Case ItemIsInCategory(j)
          Case 51, 52, 53, 54, 55
          f.Write DetermineSymbiantShortName(ItemInBackpack(LinesWritten))
        End Select
        f.Write vbCrLf
      End If
    
    End If
    LinesWritten = LinesWritten + 1
  Loop
  f.Close

' Reset counters for report writing.
  PackSpecificCounter = 0
  LinesWritten = 1

' Output as HTML code linked to auno.org or aodb.info
  Set f = fso.OpenTextFile(txtOutputDirectoryName.Text & "\html_pack.html", ForWriting, True)
  If radioUseAUNO.Value = True Then
    f.Write "The following output was generated by the <a href=" & """" & "http://www.halorn.com" & """" & ">AO Inventory System</a>.<br>" & vbCrLf
    f.Write "Linked database information courtesy of <a href=http://auno.org>Auno.org</a>.<br>"
  End If
  If radioUseAOMnet.Value = True Then
    f.Write "The following output was generated by the <a href=" & """" & "http://www.halorn.com" & """" & ">AO Inventory System</a>.<br>" & vbCrLf
    f.Write "Linked database information courtesy of <a href=http://www.aomainframe.net>www.aomainframe.net</a>.<br>"
  End If
  If radioUseAOMcom.Value = True Then
    f.Write "The following output was generated by the <a href=" & """" & "http://www.halorn.com" & """" & ">AO Inventory System</a>.<br>" & vbCrLf
    f.Write "Linked database information courtesy of <a href=http://www.aomainframe.com>www.aomainframe.com</a>.<br>"
  End If
  If radioUseAOMinfo.Value = True Then
    f.Write "The following output was generated by the <a href=" & """" & "http://www.halorn.com" & """" & ">AO Inventory System</a>.<br>" & vbCrLf
    f.Write "Linked database information courtesy of <a href=http://www.aomainframe.info>www.aomainframe.info</a>.<br>"
  End If
  
  f.Write "Report created " & Date & " at " & Time & "<br>" & vbCrLf
  Do While Not (LinesWritten = TotalSuccesses + 1)
    If ItemInBackpack(LinesWritten) = "[Unknown Item] BPHOLDER" Then
    
      f.Write "<br>" & vbCrLf
      PackSpecificCounter = PackSpecificCounter + 1
      f.Write "Backpack # " & PackSpecificCounter & " "
'      If (frmOptions!chkPlacePackNamesAfterItemNames.Value = True) Then
        If PackCustomName(PackSpecificCounter) <> "" Then
          f.Write PackCustomName(PackSpecificCounter) & " "
        End If
'      End If
      If PackSpecificType(PackSpecificCounter) <> "" Then
        f.Write "(" & PackSpecificType(PackSpecificCounter) & ")"
      End If
      f.Write "<br>" & vbCrLf
    
    Else
      ExcludeDueToNoDropOrUnique = False
      If (chkIncludeNODROP.Value = 1) Then
        ' 1 means include=yes
        ExcludeDueToNoDropOrUnique = False
      End If
      If (chkIncludeUNIQUE.Value = 1) Then
        ' 1 means include=yes
        ExcludeDueToNoDropOrUnique = False
      End If
      If (chkIncludeNODROP.Value = 0) Then
        ' 0 means include=no
        If ItemInBackpackNodropStatus(LinesWritten) = "nd" Then
          ExcludeDueToNoDropOrUnique = True
        End If
      End If
      If (chkIncludeUNIQUE.Value = 0) Then
        ' 0 means include=no
        If ItemInBackpackUniqueStatus(LinesWritten) = "u" Then
          ExcludeDueToNoDropOrUnique = True
        End If
      End If
      If (ExcludeDueToNoDropOrUnique = False) Then
        If radioUseAUNO.Value = True Then
          f.Write "<a href=" & """" & "http://auno.org/ao/db.php?cmd=view&id=" & ItemInBackpackID(LinesWritten) & "&ql=" & ItemInBackpackQL(LinesWritten) & """" & ">QL " & ItemInBackpackQL(LinesWritten) & " " & ItemInBackpack(LinesWritten) & "</a>"
        End If
        If radioUseAOMnet.Value = True Then
          f.Write "<a href=" & """" & "http://www.aomainframe.net/showitem.asp?LowID=" & ItemInBackpackID(LinesWritten)
          f.Write "&HiID=" & ItemInBackpackID(LinesWritten)
          f.Write "&QL=" & ItemInBackpackQL(LinesWritten) & """" & ">QL " & ItemInBackpackQL(LinesWritten) & " " & ItemInBackpack(LinesWritten) & "</a>"
        End If
        If radioUseAOMcom.Value = True Then
          f.Write "<a href=" & """" & "http://www.aomainframe.com/showitem.asp?LowID=" & ItemInBackpackID(LinesWritten)
          f.Write "&HiID=" & ItemInBackpackID(LinesWritten)
          f.Write "&QL=" & ItemInBackpackQL(LinesWritten) & """" & ">QL " & ItemInBackpackQL(LinesWritten) & " " & ItemInBackpack(LinesWritten) & "</a>"
        End If
        If radioUseAOMinfo.Value = True Then
          f.Write "<a href=" & """" & "http://www.aomainframe.info/showitem.asp?LowID=" & ItemInBackpackID(LinesWritten)
          f.Write "&HiID=" & ItemInBackpackID(LinesWritten)
          f.Write "&QL=" & ItemInBackpackQL(LinesWritten) & """" & ">QL " & ItemInBackpackQL(LinesWritten) & " " & ItemInBackpack(LinesWritten) & "</a>"
        End If
        If ItemInBackpackStack(LinesWritten) > 1 Then
          f.Write " Stack of " & ItemInBackpackStack(LinesWritten)
        End If
        f.Write "<br>" & vbCrLf
      End If
    End If
    LinesWritten = LinesWritten + 1
  Loop
  f.Close

  subDelay (0.0001)

' Now that the sort-by-pack is done, we can alphabetize
' everything for placement in the categorization.
  labelPleaseWait.Caption = "Preparing to sort by categories"
  subDelay (0.0001)
  
  Dim jz As Long
  Dim kz As Long
  Dim hzstr As String
  Dim v As Long
  Dim z As Long
  Dim LocatedOne As Boolean
  Dim LookingForThisCategoryName(100) As String
  
'  LookingForThisCategoryName(0) = " Uncategorized Items"
  LookingForThisCategoryName(2) = " Ammunition"
  LookingForThisCategoryName(3) = " Armor"
  LookingForThisCategoryName(4) = " Health/Nano Supplies"
  LookingForThisCategoryName(5) = " Jewelry"
  LookingForThisCategoryName(6) = " Clothing"
  LookingForThisCategoryName(7) = " Utility/Rings"
  LookingForThisCategoryName(8) = " Belt/NCU Items"
  LookingForThisCategoryName(9) = " Quest Items"
  LookingForThisCategoryName(10) = " Weapons"
  LookingForThisCategoryName(11) = " Tradeskill Items"
  LookingForThisCategoryName(12) = " Maps/Map Upgrades"
  LookingForThisCategoryName(13) = " Grafts/Pills/Fingers"
  LookingForThisCategoryName(14) = " Pocket Boss Items"
  LookingForThisCategoryName(15) = " Clusters"
  
  LookingForThisCategoryName(41) = " General"
  LookingForThisCategoryName(42) = " Implants"
  LookingForThisCategoryName(44) = " Arul Saba Items"
  LookingForThisCategoryName(45) = " Land Control"
  LookingForThisCategoryName(46) = " Tier Armor Components"
  
  LookingForThisCategoryName(51) = " Artillery Unit"
  LookingForThisCategoryName(52) = " Control Unit"
  LookingForThisCategoryName(53) = " Extermination Unit"
  LookingForThisCategoryName(54) = " Infantry Unit"
  LookingForThisCategoryName(55) = " Support Unit"
  
  LookingForThisCategoryName(56) = " Adventurer"
  LookingForThisCategoryName(57) = " Agent"
  LookingForThisCategoryName(58) = " Bureaucrat"
  LookingForThisCategoryName(59) = " Doctor"
  LookingForThisCategoryName(60) = " Enforcer"
  LookingForThisCategoryName(61) = " Engineer"
  LookingForThisCategoryName(62) = " Fixer"
  LookingForThisCategoryName(63) = " Keeper"
  LookingForThisCategoryName(64) = " Martial Artist"
  LookingForThisCategoryName(65) = " Meta-Physicist"
  LookingForThisCategoryName(66) = " Nano-Technician"
  LookingForThisCategoryName(67) = " Shade"
  LookingForThisCategoryName(68) = " Soldier"
  LookingForThisCategoryName(69) = " Trader"
  LookingForThisCategoryName(70) = " Generic Nano Crystals"

  
' Re-sort the list in ALPABETICAL ORDER
  For kz = 1 To (LinesWritten)
    For jz = 1 To (LinesWritten - kz)
      If ItemInBackpack(jz + 1) < ItemInBackpack(jz) Then
'swap if the two items are out of order
        hzstr = ItemInBackpack(jz)
        ItemInBackpack(jz) = ItemInBackpack(jz + 1)
        ItemInBackpack(jz + 1) = hzstr
        
        hzstr = ItemInBackpackID(jz)
        ItemInBackpackID(jz) = ItemInBackpackID(jz + 1)
        ItemInBackpackID(jz + 1) = hzstr

        hzstr = ItemInBackpackStack(jz)
        ItemInBackpackStack(jz) = ItemInBackpackStack(jz + 1)
        ItemInBackpackStack(jz + 1) = hzstr

        hzstr = ItemInBackpackNodropStatus(jz)
        ItemInBackpackNodropStatus(jz) = ItemInBackpackNodropStatus(jz + 1)
        ItemInBackpackNodropStatus(jz + 1) = hzstr

        hzstr = ItemInBackpackUniqueStatus(jz)
        ItemInBackpackUniqueStatus(jz) = ItemInBackpackUniqueStatus(jz + 1)
        ItemInBackpackUniqueStatus(jz + 1) = hzstr

        hzstr = ThisItemIsInCustomPack(jz)
        ThisItemIsInCustomPack(jz) = ThisItemIsInCustomPack(jz + 1)
        ThisItemIsInCustomPack(jz + 1) = hzstr
        
        hzstr = PackID(jz)
        PackID(jz) = PackID(jz + 1)
        PackID(jz + 1) = hzstr
        
        hzstr = ItemInBackpackQL(jz)
        ItemInBackpackQL(jz) = ItemInBackpackQL(jz + 1)
        ItemInBackpackQL(jz + 1) = hzstr
      End If
    Next jz
  Next kz

' Plain text version
  Set f2 = fso.OpenTextFile(txtOutputDirectoryName.Text & "\text_alpha.txt", ForWriting, True)
  f2.Write "The following output was generated by the AO Inventory System (www.halorn.com)." & vbCrLf
  f2.Write "Report created " & Date & " at " & Time & vbCrLf

' HTML version
  Set f3 = fso.OpenTextFile(txtOutputDirectoryName.Text & "\html_alpha.html", ForWriting, True)
  f3.Write "The following output was generated by the <a href=" & """" & "http://www.halorn.com" & """" & ">AO Inventory System</a>.<br>" & vbCrLf
  If radioUseAUNO.Value = True Then
    f3.Write "Linked database information courtesy of <a href=http://auno.org>Auno.org</a>.<br>"
  End If
  If radioUseAOMnet.Value = True Then
    f3.Write "Linked database information courtesy of <a href=http://www.aomainframe.net>www.aomainframe.net</a>.<br>"
  End If
  If radioUseAOMcom.Value = True Then
    f3.Write "Linked database information courtesy of <a href=http://www.aomainframe.com>www.aomainframe.com</a>.<br>"
  End If
  If radioUseAOMinfo.Value = True Then
    f3.Write "Linked database information courtesy of <a href=http://www.aomainframe.info>www.aomainframe.info</a>.<br>"
  End If
  f3.Write "Report created " & Date & " at " & Time & "<br>" & vbCrLf

' UBB code text version
  Set f4 = fso.OpenTextFile(txtOutputDirectoryName.Text & "\ubb_alpha.txt", ForWriting, True)
  If radioUseAUNO.Value = True Then
    f4.Write "[size=-2][color=#009090]Inventory list created by the [url=http://www.halorn.com]Anarchy Online Inventory System[/url] - database reference provided by [url=http://www.auno.org]Auno.org[/url].[/color][/size]"
  End If
  If radioUseAOMnet.Value = True Then
    f4.Write "[size=-2][color=#009090]Inventory list created by the [url=http://www.halorn.com]Anarchy Online Inventory System[/url] - database reference provided by [url=http://www.aomainframe.net]www.aomainframe.net[/url].[/color][/size]"
  End If
  If radioUseAOMcom.Value = True Then
    f4.Write "[size=-2][color=#009090]Inventory list created by the [url=http://www.halorn.com]Anarchy Online Inventory System[/url] - database reference provided by [url=http://www.aomainframe.com]www.aomainframe.com[/url].[/color][/size]"
  End If
  If radioUseAOMinfo.Value = True Then
    f4.Write "[size=-2][color=#009090]Inventory list created by the [url=http://www.halorn.com]Anarchy Online Inventory System[/url] - database reference provided by [url=http://www.aomainframe.info]www.aomainframe.info[/url].[/color][/size]"
  End If

For z = 0 To 100
If LookingForThisCategoryName(z) <> "" Then
  
subDelay (0.0001)
labelPleaseWait.Caption = "Sorting by category - " & LookingForThisCategoryName(z)
  
  subDelay (0.0001)
  LocatedOne = False
  For v = 1 To LinesWritten
    subDelay (0.0001)
    j = Val(ItemInBackpackID(v))
    If ItemIsInCategory(j) = z Then
      
'beginmarker
      ExcludeDueToNoDropOrUnique = False
      If (chkIncludeNODROP.Value = 1) Then
        ' 1 means include=yes
        ExcludeDueToNoDropOrUnique = False
      End If
      If (chkIncludeUNIQUE.Value = 1) Then
        ' 1 means include=yes
        ExcludeDueToNoDropOrUnique = False
      End If
      If (chkIncludeNODROP.Value = 0) Then
        ' 0 means include=no
        If ItemInBackpackNodropStatus(v) = "nd" Then
          ExcludeDueToNoDropOrUnique = True
        End If
      End If
      If (chkIncludeUNIQUE.Value = 0) Then
        ' 0 means include=no
        If ItemInBackpackUniqueStatus(v) = "u" Then
          ExcludeDueToNoDropOrUnique = True
        End If
      End If
      If (ExcludeDueToNoDropOrUnique = False) Then

        If LocatedOne = False Then
          f2.Write vbCrLf & LookingForThisCategoryName(z)
          f2.Write vbCrLf
          f3.Write "<p>" & vbCrLf & LookingForThisCategoryName(z) & "<br>"
          f3.Write vbCrLf
          f4.Write vbCrLf & LookingForThisCategoryName(z)
          f4.Write vbCrLf
          LocatedOne = True
        End If
    
    
    f2.Write ItemInBackpack(v) & " QL:" & ItemInBackpackQL(v)
      
    If radioUseAUNO.Value = True Then
      f3.Write "<a href=" & """" & "http://auno.org/ao/db.php?cmd=view&id=" & ItemInBackpackID(v) & "&ql=" & ItemInBackpackQL(v) & """" & ">QL " & ItemInBackpackQL(v) & " " & ItemInBackpack(v) & "</a>"
    End If
    If radioUseAOMnet.Value = True Then
      f3.Write "<a href=" & """" & "http://www.aomainframe.net/showitem.asp?LowID=" & ItemInBackpackID(v)
      f3.Write "&HiID=" & ItemInBackpackID(v)
      f3.Write "&QL=" & ItemInBackpackQL(v) & """" & ">QL " & ItemInBackpackQL(v) & " " & ItemInBackpack(v) & "</a>"
    End If
    If radioUseAOMcom.Value = True Then
      f3.Write "<a href=" & """" & "http://www.aomainframe.com/showitem.asp?LowID=" & ItemInBackpackID(v)
      f3.Write "&HiID=" & ItemInBackpackID(v)
      f3.Write "&QL=" & ItemInBackpackQL(v) & """" & ">QL " & ItemInBackpackQL(v) & " " & ItemInBackpack(v) & "</a>"
    End If
    If radioUseAOMinfo.Value = True Then
      f3.Write "<a href=" & """" & "http://www.aomainframe.info/showitem.asp?LowID=" & ItemInBackpackID(v)
      f3.Write "&HiID=" & ItemInBackpackID(v)
      f3.Write "&QL=" & ItemInBackpackQL(v) & """" & ">QL " & ItemInBackpackQL(v) & " " & ItemInBackpack(v) & "</a>"
    End If
  
    If radioUseAUNO.Value = True Then
      f4.Write "[url=http://auno.org/ao/db.php?cmd=view&id=" & ItemInBackpackID(v) & "&ql=" & ItemInBackpackQL(v) & "]QL " & ItemInBackpackQL(v) & " " & ItemInBackpack(v) & "[/url]"
    End If
    If radioUseAOMnet.Value = True Then
      f4.Write "[url=http://www.aomainframe.net/showitem.asp?LowID=" & ItemInBackpackID(v)
      f4.Write "&HiID=" & ItemInBackpackID(v)
      f4.Write "&QL=" & ItemInBackpackQL(v) & "]QL " & ItemInBackpackQL(v) & " " & ItemInBackpack(v) & "[/url]"
    End If
    If radioUseAOMcom.Value = True Then
      f4.Write "[url=http://www.aomainframe.com/showitem.asp?LowID=" & ItemInBackpackID(v)
      f4.Write "&HiID=" & ItemInBackpackID(v)
      f4.Write "&QL=" & ItemInBackpackQL(v) & "]QL " & ItemInBackpackQL(v) & " " & ItemInBackpack(v) & "[/url]"
    End If
    If radioUseAOMinfo.Value = True Then
      f4.Write "[url=http://www.aomainframe.info/showitem.asp?LowID=" & ItemInBackpackID(v)
      f4.Write "&HiID=" & ItemInBackpackID(v)
      f4.Write "&QL=" & ItemInBackpackQL(v) & "]QL " & ItemInBackpackQL(v) & " " & ItemInBackpack(v) & "[/url]"
    End If
      
'      f3.Write "<a href=" & """" & "http://auno.org/ao/db.php?cmd=view&id=" & ItemInBackpackID(v) & "&ql=" & ItemInBackpackQL(v) & """" & ">QL " & ItemInBackpackQL(v) & " " & ItemInBackpack(v) & "</a>"
'      f4.Write "[url=http://auno.org/ao/db.php?cmd=view&id=" & ItemInBackpackID(v) & "&ql=" & ItemInBackpackQL(v) & "]QL " & ItemInBackpackQL(v) & " " & ItemInBackpack(v) & "[/url]"
        
      If ItemInBackpackStack(v) > 1 Then
        f2.Write " Stack of " & ItemInBackpackStack(v)
        f3.Write " Stack of " & ItemInBackpackStack(v)
        f4.Write " Stack of " & ItemInBackpackStack(v)
      End If
    
    
    
    
    
    
' Symbiant short name
  Select Case z
    Case 51, 52, 53, 54, 55
    f4.Write DetermineSymbiantShortName(ItemInBackpack(v))
  End Select

'      If (frmOptions!chkPlacePackNamesAfterItemNames.Value = True) Then
        If ThisItemIsInCustomPack(v) <> "" Then
          f2.Write " (" & ThisItemIsInCustomPack(v) & ")"
          f3.Write " (" & ThisItemIsInCustomPack(v) & ")"
          f4.Write " (" & ThisItemIsInCustomPack(v) & ")"
        End If
'      End If
      
      
' By this point, the name has been placed.
' Complete it with a line break.
      f2.Write vbCrLf
      f3.Write "<br>" & vbCrLf
      f4.Write vbCrLf
    End If
  End If 'of the unique or nodrop flag issue
  
  
  
  
  Next v
End If
Next z

  f2.Close
  f3.Close
  f4.Close

' Re-sort the list in QL ORDER
  For kz = 1 To (LinesWritten)
    For jz = 1 To (LinesWritten - kz)
      If Val(ItemInBackpackQL(jz + 1)) < Val(ItemInBackpackQL(jz)) Then
'swap if the two items are out of order
        hzstr = ItemInBackpack(jz)
        ItemInBackpack(jz) = ItemInBackpack(jz + 1)
        ItemInBackpack(jz + 1) = hzstr
        
        hzstr = ItemInBackpackID(jz)
        ItemInBackpackID(jz) = ItemInBackpackID(jz + 1)
        ItemInBackpackID(jz + 1) = hzstr
        
        hzstr = ItemInBackpackStack(jz)
        ItemInBackpackStack(jz) = ItemInBackpackStack(jz + 1)
        ItemInBackpackStack(jz + 1) = hzstr

        hzstr = ThisItemIsInCustomPack(jz)
        ThisItemIsInCustomPack(jz) = ThisItemIsInCustomPack(jz + 1)
        ThisItemIsInCustomPack(jz + 1) = hzstr
        
        hzstr = ItemInBackpackNodropStatus(jz)
        ItemInBackpackNodropStatus(jz) = ItemInBackpackNodropStatus(jz + 1)
        ItemInBackpackNodropStatus(jz + 1) = hzstr

        hzstr = ItemInBackpackUniqueStatus(jz)
        ItemInBackpackUniqueStatus(jz) = ItemInBackpackUniqueStatus(jz + 1)
        ItemInBackpackUniqueStatus(jz + 1) = hzstr
        
        hzstr = PackID(jz)
        PackID(jz) = PackID(jz + 1)
        PackID(jz + 1) = hzstr
        
        hzstr = ItemInBackpackQL(jz)
        ItemInBackpackQL(jz) = ItemInBackpackQL(jz + 1)
        ItemInBackpackQL(jz + 1) = hzstr
      End If
    Next jz
  Next kz
  
  subDelay (0.0001)

' Plain text version
  Set f2 = fso.OpenTextFile(txtOutputDirectoryName.Text & "\text_ql.txt", ForWriting, True)
  f2.Write "The following output was generated by the AO Inventory System (www.halorn.com)." & vbCrLf
  f2.Write "Report created " & Date & " at " & Time & vbCrLf

' HTML version
  Set f3 = fso.OpenTextFile(txtOutputDirectoryName.Text & "\html_ql.html", ForWriting, True)
  f3.Write "The following output was generated by the <a href=" & """" & "http://www.halorn.com" & """" & ">AO Inventory System</a>.<br>" & vbCrLf
  If radioUseAUNO.Value = True Then
    f3.Write "Linked database information courtesy of <a href=http://auno.org>Auno.org</a>.<br>"
  End If
  If radioUseAOMnet.Value = True Then
    f3.Write "Linked database information courtesy of <a href=http://www.aomainframe.net>www.aomainframe.net</a>.<br>"
  End If
  If radioUseAOMcom.Value = True Then
    f3.Write "Linked database information courtesy of <a href=http://www.aomainframe.net>www.aomainframe.com</a>.<br>"
  End If
  If radioUseAOMinfo.Value = True Then
    f3.Write "Linked database information courtesy of <a href=http://www.aomainframe.net>www.aomainframe.info</a>.<br>"
  End If
  f3.Write "Report created " & Date & " at " & Time & "<br>" & vbCrLf

' UBB code text version
  Set f4 = fso.OpenTextFile(txtOutputDirectoryName.Text & "\ubb_ql.txt", ForWriting, True)
  If radioUseAUNO.Value = True Then
    f4.Write "[size=-2][color=#009090]Inventory list created by the [url=http://www.halorn.com]Anarchy Online Inventory System[/url] - database reference provided by [url=http://www.auno.org]Auno.org[/url].[/color][/size]"
  End If
  If radioUseAOMnet.Value = True Then
    f4.Write "[size=-2][color=#009090]Inventory list created by the [url=http://www.halorn.com]Anarchy Online Inventory System[/url] - database reference provided by [url=http://www.aomainframe.net]www.aomainframe.net[/url].[/color][/size]"
  End If
  If radioUseAOMcom.Value = True Then
    f4.Write "[size=-2][color=#009090]Inventory list created by the [url=http://www.halorn.com]Anarchy Online Inventory System[/url] - database reference provided by [url=http://www.aomainframe.com]www.aomainframe.com[/url].[/color][/size]"
  End If
  If radioUseAOMinfo.Value = True Then
    f4.Write "[size=-2][color=#009090]Inventory list created by the [url=http://www.halorn.com]Anarchy Online Inventory System[/url] - database reference provided by [url=http://www.aomainframe.info]www.aomainframe.info[/url].[/color][/size]"
  End If


  For z = 0 To 100
  If LookingForThisCategoryName(z) <> "" Then
    subDelay (0.0001)
    labelPleaseWait.Caption = "Sorting by category - " & LookingForThisCategoryName(z)
    subDelay (0.0001)
    LocatedOne = False
    For v = 1 To LinesWritten
      subDelay (0.0001)
      j = Val(ItemInBackpackID(v))
      If ItemIsInCategory(j) = z Then
      
'''''
      ExcludeDueToNoDropOrUnique = False
      If (chkIncludeNODROP.Value = 1) Then
        ' 1 means include=yes
        ExcludeDueToNoDropOrUnique = False
      End If
      If (chkIncludeUNIQUE.Value = 1) Then
        ' 1 means include=yes
        ExcludeDueToNoDropOrUnique = False
      End If
      If (chkIncludeNODROP.Value = 0) Then
        ' 0 means include=no
        If ItemInBackpackNodropStatus(v) = "nd" Then
          ExcludeDueToNoDropOrUnique = True
        End If
      End If
      If (chkIncludeUNIQUE.Value = 0) Then
        ' 0 means include=no
        If ItemInBackpackUniqueStatus(v) = "u" Then
          ExcludeDueToNoDropOrUnique = True
        End If
      End If
      If (ExcludeDueToNoDropOrUnique = False) Then

    
        If LocatedOne = False Then
          f2.Write vbCrLf & LookingForThisCategoryName(z)
          f2.Write vbCrLf
          f3.Write "<p>" & vbCrLf & LookingForThisCategoryName(z) & "<br>"
          f3.Write vbCrLf
          f4.Write vbCrLf & LookingForThisCategoryName(z)
          f4.Write vbCrLf
          LocatedOne = True
        End If
    
    f2.Write ItemInBackpack(v) & " QL:" & ItemInBackpackQL(v)
      
    If radioUseAUNO.Value = True Then
      f3.Write "<a href=" & """" & "http://auno.org/ao/db.php?cmd=view&id=" & ItemInBackpackID(v) & "&ql=" & ItemInBackpackQL(v) & """" & ">QL " & ItemInBackpackQL(v) & " " & ItemInBackpack(v) & "</a>"
    End If
    If radioUseAOMnet.Value = True Then
      f3.Write "<a href=" & """" & "http://www.aomainframe.net/showitem.asp?LowID=" & ItemInBackpackID(v)
      f3.Write "&HiID=" & ItemInBackpackID(v)
      f3.Write "&QL=" & ItemInBackpackQL(v) & """" & ">QL " & ItemInBackpackQL(v) & " " & ItemInBackpack(v) & "</a>"
    End If
    If radioUseAOMcom.Value = True Then
      f3.Write "<a href=" & """" & "http://www.aomainframe.com/showitem.asp?LowID=" & ItemInBackpackID(v)
      f3.Write "&HiID=" & ItemInBackpackID(v)
      f3.Write "&QL=" & ItemInBackpackQL(v) & """" & ">QL " & ItemInBackpackQL(v) & " " & ItemInBackpack(v) & "</a>"
    End If
    If radioUseAOMinfo.Value = True Then
      f3.Write "<a href=" & """" & "http://www.aomainframe.info/showitem.asp?LowID=" & ItemInBackpackID(v)
      f3.Write "&HiID=" & ItemInBackpackID(v)
      f3.Write "&QL=" & ItemInBackpackQL(v) & """" & ">QL " & ItemInBackpackQL(v) & " " & ItemInBackpack(v) & "</a>"
    End If
  
    If radioUseAUNO.Value = True Then
      f4.Write "[url=http://auno.org/ao/db.php?cmd=view&id=" & ItemInBackpackID(v) & "&ql=" & ItemInBackpackQL(v) & "]QL " & ItemInBackpackQL(v) & " " & ItemInBackpack(v) & "[/url]"
    End If
    If radioUseAOMnet.Value = True Then
      f4.Write "[url=http://www.aomainframe.net/showitem.asp?LowID=" & ItemInBackpackID(v)
      f4.Write "&HiID=" & ItemInBackpackID(v)
      f4.Write "&QL=" & ItemInBackpackQL(v) & "]QL " & ItemInBackpackQL(v) & " " & ItemInBackpack(v) & "[/url]"
    End If
    If radioUseAOMcom.Value = True Then
      f4.Write "[url=http://www.aomainframe.com/showitem.asp?LowID=" & ItemInBackpackID(v)
      f4.Write "&HiID=" & ItemInBackpackID(v)
      f4.Write "&QL=" & ItemInBackpackQL(v) & "]QL " & ItemInBackpackQL(v) & " " & ItemInBackpack(v) & "[/url]"
    End If
    If radioUseAOMinfo.Value = True Then
      f4.Write "[url=http://www.aomainframe.info/showitem.asp?LowID=" & ItemInBackpackID(v)
      f4.Write "&HiID=" & ItemInBackpackID(v)
      f4.Write "&QL=" & ItemInBackpackQL(v) & "]QL " & ItemInBackpackQL(v) & " " & ItemInBackpack(v) & "[/url]"
    End If
      
'      f3.Write "<a href=" & """" & "http://auno.org/ao/db.php?cmd=view&id=" & ItemInBackpackID(v) & "&ql=" & ItemInBackpackQL(v) & """" & ">QL " & ItemInBackpackQL(v) & " " & ItemInBackpack(v) & "</a>"
'      f4.Write "[url=http://auno.org/ao/db.php?cmd=view&id=" & ItemInBackpackID(v) & "&ql=" & ItemInBackpackQL(v) & "]QL " & ItemInBackpackQL(v) & " " & ItemInBackpack(v) & "[/url]"
        
      If ItemInBackpackStack(v) > 1 Then
        f2.Write " Stack of " & ItemInBackpackStack(v)
        f3.Write " Stack of " & ItemInBackpackStack(v)
        f4.Write " Stack of " & ItemInBackpackStack(v)
      End If
' Symbiant short name
  Select Case z
    Case 51, 52, 53, 54, 55
    f4.Write DetermineSymbiantShortName(ItemInBackpack(v))
  End Select

'      If (frmOptions!chkPlacePackNamesAfterItemNames.Value = True) Then
        If ThisItemIsInCustomPack(v) <> "" Then
          f2.Write " (" & ThisItemIsInCustomPack(v) & ")"
          f3.Write " (" & ThisItemIsInCustomPack(v) & ")"
          f4.Write " (" & ThisItemIsInCustomPack(v) & ")"
        End If
'      End If
      
      
' By this point, the name has been placed.
' Complete it with a line break.
      f2.Write vbCrLf
      f3.Write "<br>" & vbCrLf
      f4.Write vbCrLf
    End If
  End If 'of the unique or nodrop flag issue
  
  
  
  
  Next v

  End If
  Next z
  subDelay (0.0001)
  DoEvents
End If ' This is the end of "if this was a Who Am I report
       ' of the "if it is not a Who Am I report" section.



  If IsThisAWhoAmIReport = True Then
  
  ' Output as plain text
  
  
  
  Set f = fso.OpenTextFile(txtOutputDirectoryName.Text & "\profile.html", ForWriting, True)
  f.Write "<html>" & vbCrLf
  f.Write "The following character profile was generated by the <a href=" & """" & "http://www.halorn.com" & """" & ">AO Inventory System</a>.<br>" & vbCrLf
  f.Write "Report created " & Date & " at " & Time & "<br>" & vbCrLf
  
   For WhoAmICounter = 1 To 45
     For AnotherCounter = 1 To 45
       If Val(ItemInBackpackStack(AnotherCounter)) = WhoAmICounter Then ' this item matches what we want
         ItemOnTheCharacter(WhoAmICounter) = ItemInBackpack(AnotherCounter)
         ItemOnTheCharacterQL(WhoAmICounter) = ItemInBackpackQL(AnotherCounter)
         ItemOnTheCharacterLocation(WhoAmICounter) = ItemInBackpackStack(AnotherCounter)
         ItemOnTheCharacterID(WhoAmICounter) = ItemInBackpackID(AnotherCounter)
       End If
     Next AnotherCounter
   Next WhoAmICounter
    
  For WhoAmICounter = 1 To 45
    Select Case ItemOnTheCharacterLocation(WhoAmICounter)
      Case "1"
        ItemOnTheCharacterHeader(WhoAmICounter) = "HUD1 :                "
      Case "2"
        ItemOnTheCharacterHeader(WhoAmICounter) = "HUD3 :                "
      Case "3"
        ItemOnTheCharacterHeader(WhoAmICounter) = "UTIL1 :               "
      Case "4"
        ItemOnTheCharacterHeader(WhoAmICounter) = "UTIL2 :               "
      Case "5"
        ItemOnTheCharacterHeader(WhoAmICounter) = "UTIL3 :               "
      Case "6"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Weapon (Right Hand) : "
      Case "7"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Belt :                "
      Case "8"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Weapon (Left Hand) :  "
      Case "9"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Deck #1 :             "
      Case "10"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Deck #2 :             "
      Case "11"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Deck #3 :             "
      Case "12"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Deck #4 :             "
      Case "13"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Deck #5 :             "
      Case "14"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Deck #6 :             "
      Case "15"
        ItemOnTheCharacterHeader(WhoAmICounter) = "HUD2 :                "
      Case "17"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Neck :                "
      Case "18"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Head :                "
      Case "19"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Back :                "
      Case "20"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Right Shoulder :      "
      Case "21"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Chest :               "
      Case "22"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Left Shoulder :       "
      Case "23"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Right Sleeve :        "
      Case "25"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Left Sleeve :         "
      Case "24"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Hands :               "
      Case "26"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Right Wrist :         "
      Case "27"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Pants :               "
      Case "28"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Left Wrist :          "
      Case "29"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Right Finger :        "
      Case "30"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Boots :               "
      Case "31"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Left Finger :         "
      Case "33"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Eye :                 "
      Case "34"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Head :                "
      Case "35"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Ear :                 "
      Case "36"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Right Arm :           "
      Case "37"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Chest :               "
      Case "38"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Left Arm :            "
      Case "39"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Right Wrist :         "
      Case "40"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Waist :               "
      Case "41"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Left Wrist :          "
      Case "42"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Right Hand :          "
      Case "43"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Leg :                 "
      Case "44"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Left Hand :           "
      Case "45"
        ItemOnTheCharacterHeader(WhoAmICounter) = "Feet :                "
      End Select
  Next WhoAmICounter
  
  For WhoAmICounter = 1 To 45
    If ItemOnTheCharacterQL(WhoAmICounter) = "" Then
      ItemOnTheCharacter(WhoAmICounter) = "(Empty)" & vbCrLf
        Else
      ItemOnTheCharacter(WhoAmICounter) = ItemOnTheCharacter(WhoAmICounter)
    End If
  Next WhoAmICounter
  
  f.Write "<br>"
  f.Write "<b>UTILITY / WEAPONS / DECK</b><br>" & vbCrLf

  If ItemOnTheCharacterQL(1) <> "" Then
    WhoAmICounter = 1
    If ItemOnTheCharacterQL(WhoAmICounter) <> "" Then
    f.Write ItemOnTheCharacterHeader(WhoAmICounter)
    f.Write "<a href=" & """" & "http://auno.org/ao/db.php?cmd=view&id="
    f.Write ItemOnTheCharacterID(WhoAmICounter)
    f.Write "&ql="
    f.Write ItemOnTheCharacterQL(WhoAmICounter)
    f.Write """" & ">"
    f.Write ItemOnTheCharacter(WhoAmICounter)
    f.Write "</a>"
    f.Write " QL: " & ItemOnTheCharacterQL(WhoAmICounter) & vbCrLf & "<br>" & vbCrLf
    End If
  End If

  If ItemOnTheCharacterQL(15) <> "" Then
    WhoAmICounter = 15
    If ItemOnTheCharacterQL(WhoAmICounter) <> "" Then
    f.Write ItemOnTheCharacterHeader(WhoAmICounter)
    f.Write "<a href=" & """" & "http://auno.org/ao/db.php?cmd=view&id="
    f.Write ItemOnTheCharacterID(WhoAmICounter)
    f.Write "&ql="
    f.Write ItemOnTheCharacterQL(WhoAmICounter)
    f.Write """" & ">"
    f.Write ItemOnTheCharacter(WhoAmICounter)
    f.Write "</a>"
    f.Write " QL: " & ItemOnTheCharacterQL(WhoAmICounter) & vbCrLf & "<br>" & vbCrLf
    End If
  End If
  
  For WhoAmICounter = 2 To 14
  If ItemOnTheCharacterQL(WhoAmICounter) <> "" Then
    f.Write ItemOnTheCharacterHeader(WhoAmICounter)
    f.Write "<a href=" & """" & "http://auno.org/ao/db.php?cmd=view&id="
    f.Write ItemOnTheCharacterID(WhoAmICounter)
    f.Write "&ql="
    f.Write ItemOnTheCharacterQL(WhoAmICounter)
    f.Write """" & ">"
    f.Write ItemOnTheCharacter(WhoAmICounter)
    f.Write "</a>"
    f.Write " QL: " & ItemOnTheCharacterQL(WhoAmICounter) & vbCrLf & "<br>" & vbCrLf
  End If
  Next WhoAmICounter
  
  f.Write "<br>"
  f.Write "<b>ARMOR</b><br>" & vbCrLf
  For WhoAmICounter = 16 To 30
  If ItemOnTheCharacterQL(WhoAmICounter) <> "" Then
    f.Write ItemOnTheCharacterHeader(WhoAmICounter)
    f.Write "<a href=" & """" & "http://auno.org/ao/db.php?cmd=view&id="
    f.Write ItemOnTheCharacterID(WhoAmICounter)
    f.Write "&ql="
    f.Write ItemOnTheCharacterQL(WhoAmICounter)
    f.Write """" & ">"
    f.Write ItemOnTheCharacter(WhoAmICounter)
    f.Write "</a>"
    f.Write " QL: " & ItemOnTheCharacterQL(WhoAmICounter) & vbCrLf & "<br>" & vbCrLf
  End If
  Next WhoAmICounter
  
  f.Write "<br>"
  f.Write "<b>IMPLANTS</b><br>" & vbCrLf
  For WhoAmICounter = 32 To 45
  If ItemOnTheCharacterQL(WhoAmICounter) <> "" Then
    f.Write ItemOnTheCharacterHeader(WhoAmICounter)
    f.Write "<a href=" & """" & "http://auno.org/ao/db.php?cmd=view&id="
    f.Write ItemOnTheCharacterID(WhoAmICounter)
    f.Write "&ql="
    f.Write ItemOnTheCharacterQL(WhoAmICounter)
    f.Write """" & ">"
    f.Write ItemOnTheCharacter(WhoAmICounter)
    f.Write "</a>"
    f.Write " QL: " & ItemOnTheCharacterQL(WhoAmICounter) & vbCrLf & "<br>" & vbCrLf
  End If
  Next WhoAmICounter
  
  
' Now for the auno stuff.

  If ItemOnTheCharacterQL(1) <> "" Then
   AunoID = AunoID & "&id1-2=" & ItemOnTheCharacterID(1)
   AunoID = AunoID & "&ql1-2=" & ItemOnTheCharacterQL(1)
  End If
  If ItemOnTheCharacterQL(15) <> "" Then
   AunoID = AunoID & "&id1-32768=" & ItemOnTheCharacterID(15)
   AunoID = AunoID & "&ql1-32768=" & ItemOnTheCharacterQL(15)
  End If
  If ItemOnTheCharacterQL(2) <> "" Then
   AunoID = AunoID & "&id1-4=" & ItemOnTheCharacterID(2)
   AunoID = AunoID & "&ql1-4=" & ItemOnTheCharacterQL(2)
  End If
  If ItemOnTheCharacterQL(3) <> "" Then
   AunoID = AunoID & "&id1-8=" & ItemOnTheCharacterID(3)
   AunoID = AunoID & "&ql1-8=" & ItemOnTheCharacterQL(3)
  End If
  If ItemOnTheCharacterQL(4) <> "" Then
   AunoID = AunoID & "&id1-16=" & ItemOnTheCharacterID(4)
   AunoID = AunoID & "&ql1-16=" & ItemOnTheCharacterQL(4)
  End If
  If ItemOnTheCharacterQL(5) <> "" Then
   AunoID = AunoID & "&id1-32=" & ItemOnTheCharacterID(5)
   AunoID = AunoID & "&ql1-32=" & ItemOnTheCharacterQL(5)
  End If
  If ItemOnTheCharacterQL(6) <> "" Then
   AunoID = AunoID & "&id1-64=" & ItemOnTheCharacterID(6)
   AunoID = AunoID & "&ql1-64=" & ItemOnTheCharacterQL(6)
  End If
  If ItemOnTheCharacterQL(7) <> "" Then
   AunoID = AunoID & "&id1-128=" & ItemOnTheCharacterID(7)
   AunoID = AunoID & "&ql1-128=" & ItemOnTheCharacterQL(7)
  End If
  If ItemOnTheCharacterQL(8) <> "" Then
   AunoID = AunoID & "&id1-256=" & ItemOnTheCharacterID(8)
   AunoID = AunoID & "&ql1-256=" & ItemOnTheCharacterQL(8)
  End If
  If ItemOnTheCharacterQL(9) <> "" Then
   AunoID = AunoID & "&id1-512=" & ItemOnTheCharacterID(9)
   AunoID = AunoID & "&ql1-512=" & ItemOnTheCharacterQL(9)
  End If
  If ItemOnTheCharacterQL(10) <> "" Then
   AunoID = AunoID & "&id1-1024=" & ItemOnTheCharacterID(10)
   AunoID = AunoID & "&ql1-1024=" & ItemOnTheCharacterQL(10)
  End If
  If ItemOnTheCharacterQL(11) <> "" Then
   AunoID = AunoID & "&id1-2048=" & ItemOnTheCharacterID(11)
   AunoID = AunoID & "&ql1-2048=" & ItemOnTheCharacterQL(11)
  End If
  If ItemOnTheCharacterQL(12) <> "" Then
   AunoID = AunoID & "&id1-4096=" & ItemOnTheCharacterID(12)
   AunoID = AunoID & "&ql1-4096=" & ItemOnTheCharacterQL(12)
  End If
  If ItemOnTheCharacterQL(13) <> "" Then
   AunoID = AunoID & "&id1-8192=" & ItemOnTheCharacterID(13)
   AunoID = AunoID & "&ql1-8192=" & ItemOnTheCharacterQL(13)
  End If
  If ItemOnTheCharacterQL(14) <> "" Then
   AunoID = AunoID & "&id1-16384=" & ItemOnTheCharacterID(14)
   AunoID = AunoID & "&ql1-16384=" & ItemOnTheCharacterQL(14)
  End If
  If ItemOnTheCharacterQL(17) <> "" Then
   AunoID = AunoID & "&id2-2=" & ItemOnTheCharacterID(17)
   AunoID = AunoID & "&ql2-2=" & ItemOnTheCharacterQL(17)
  End If
  If ItemOnTheCharacterQL(18) <> "" Then
   AunoID = AunoID & "&id2-4=" & ItemOnTheCharacterID(18)
   AunoID = AunoID & "&ql2-4=" & ItemOnTheCharacterQL(18)
  End If
  If ItemOnTheCharacterQL(19) <> "" Then
   AunoID = AunoID & "&id2-8=" & ItemOnTheCharacterID(19)
   AunoID = AunoID & "&ql2-8=" & ItemOnTheCharacterQL(19)
  End If
  If ItemOnTheCharacterQL(20) <> "" Then
   AunoID = AunoID & "&id2-16=" & ItemOnTheCharacterID(20)
   AunoID = AunoID & "&ql2-16=" & ItemOnTheCharacterQL(20)
  End If
  If ItemOnTheCharacterQL(21) <> "" Then
   AunoID = AunoID & "&id2-32=" & ItemOnTheCharacterID(21)
   AunoID = AunoID & "&ql2-32=" & ItemOnTheCharacterQL(21)
  End If
  If ItemOnTheCharacterQL(22) <> "" Then
   AunoID = AunoID & "&id2-64=" & ItemOnTheCharacterID(22)
   AunoID = AunoID & "&ql2-64=" & ItemOnTheCharacterQL(22)
  End If
  If ItemOnTheCharacterQL(23) <> "" Then
   AunoID = AunoID & "&id2-128=" & ItemOnTheCharacterID(23)
   AunoID = AunoID & "&ql2-128=" & ItemOnTheCharacterQL(23)
  End If
  If ItemOnTheCharacterQL(24) <> "" Then
   AunoID = AunoID & "&id2-256=" & ItemOnTheCharacterID(24)
   AunoID = AunoID & "&ql2-256=" & ItemOnTheCharacterQL(24)
  End If
  If ItemOnTheCharacterQL(25) <> "" Then
   AunoID = AunoID & "&id2-512=" & ItemOnTheCharacterID(25)
   AunoID = AunoID & "&ql2-512=" & ItemOnTheCharacterQL(25)
  End If
  If ItemOnTheCharacterQL(26) <> "" Then
   AunoID = AunoID & "&id2-1024=" & ItemOnTheCharacterID(26)
   AunoID = AunoID & "&ql2-1024=" & ItemOnTheCharacterQL(26)
  End If
  If ItemOnTheCharacterQL(27) <> "" Then
   AunoID = AunoID & "&id2-2048=" & ItemOnTheCharacterID(27)
   AunoID = AunoID & "&ql2-2048=" & ItemOnTheCharacterQL(27)
  End If
  If ItemOnTheCharacterQL(28) <> "" Then
   AunoID = AunoID & "&id2-4096=" & ItemOnTheCharacterID(28)
   AunoID = AunoID & "&ql2-4096=" & ItemOnTheCharacterQL(28)
  End If
  If ItemOnTheCharacterQL(29) <> "" Then
   AunoID = AunoID & "&id2-8192=" & ItemOnTheCharacterID(29)
   AunoID = AunoID & "&ql2-8192=" & ItemOnTheCharacterQL(29)
  End If
  If ItemOnTheCharacterQL(30) <> "" Then
   AunoID = AunoID & "&id2-16384=" & ItemOnTheCharacterID(30)
   AunoID = AunoID & "&ql2-16384=" & ItemOnTheCharacterQL(30)
  End If
  If ItemOnTheCharacterQL(31) <> "" Then
   AunoID = AunoID & "&id2-32768=" & ItemOnTheCharacterID(31)
   AunoID = AunoID & "&ql2-32768=" & ItemOnTheCharacterQL(31)
  End If
  If ItemOnTheCharacterQL(33) <> "" Then
   AunoID = AunoID & "&id3-2=" & ItemOnTheCharacterID(33)
   AunoID = AunoID & "&ql3-2=" & ItemOnTheCharacterQL(33)
  End If
  If ItemOnTheCharacterQL(34) <> "" Then
   AunoID = AunoID & "&id3-4=" & ItemOnTheCharacterID(34)
   AunoID = AunoID & "&ql3-4=" & ItemOnTheCharacterQL(34)
  End If
  If ItemOnTheCharacterQL(35) <> "" Then
   AunoID = AunoID & "&id3-8=" & ItemOnTheCharacterID(35)
   AunoID = AunoID & "&ql3-8=" & ItemOnTheCharacterQL(35)
  End If
  If ItemOnTheCharacterQL(36) <> "" Then
   AunoID = AunoID & "&id3-16=" & ItemOnTheCharacterID(36)
   AunoID = AunoID & "&ql3-16=" & ItemOnTheCharacterQL(36)
  End If
  If ItemOnTheCharacterQL(37) <> "" Then
   AunoID = AunoID & "&id3-32=" & ItemOnTheCharacterID(37)
   AunoID = AunoID & "&ql3-32=" & ItemOnTheCharacterQL(37)
  End If
  If ItemOnTheCharacterQL(38) <> "" Then
   AunoID = AunoID & "&id3-64=" & ItemOnTheCharacterID(38)
   AunoID = AunoID & "&ql3-64=" & ItemOnTheCharacterQL(38)
  End If
  If ItemOnTheCharacterQL(39) <> "" Then
   AunoID = AunoID & "&id3-128=" & ItemOnTheCharacterID(39)
   AunoID = AunoID & "&ql3-128=" & ItemOnTheCharacterQL(39)
  End If
  If ItemOnTheCharacterQL(40) <> "" Then
   AunoID = AunoID & "&id3-256=" & ItemOnTheCharacterID(40)
   AunoID = AunoID & "&ql3-256=" & ItemOnTheCharacterQL(40)
  End If
  If ItemOnTheCharacterQL(41) <> "" Then
   AunoID = AunoID & "&id3-512=" & ItemOnTheCharacterID(41)
   AunoID = AunoID & "&ql3-512=" & ItemOnTheCharacterQL(41)
  End If
  If ItemOnTheCharacterQL(42) <> "" Then
   AunoID = AunoID & "&id3-1024=" & ItemOnTheCharacterID(42)
   AunoID = AunoID & "&ql3-1024=" & ItemOnTheCharacterQL(42)
  End If
  If ItemOnTheCharacterQL(43) <> "" Then
   AunoID = AunoID & "&id3-2048=" & ItemOnTheCharacterID(43)
   AunoID = AunoID & "&ql3-2048=" & ItemOnTheCharacterQL(43)
  End If
  If ItemOnTheCharacterQL(44) <> "" Then
   AunoID = AunoID & "&id3-4096=" & ItemOnTheCharacterID(44)
   AunoID = AunoID & "&ql3-4096=" & ItemOnTheCharacterQL(44)
  End If
  If ItemOnTheCharacterQL(45) <> "" Then
   AunoID = AunoID & "&id3-8192=" & ItemOnTheCharacterID(45)
   AunoID = AunoID & "&ql3-8192=" & ItemOnTheCharacterQL(45)
  End If
  
  f.Write "<br>" & vbCrLf
  f.Write "Click <a href=http://auno.org/ao/equip.php?noedit=1" & AunoID
  f.Write ">here</a> to see your character profile, courtesy of auno.org."
  f.Write vbCrLf
  f.Write "<br>" & vbCrLf
  f.Write "<br>" & vbCrLf
  f.Write "For a link suitable for posting in a forum such as the official AO forum, cut and paste the following :"
  f.Write vbCrLf
  f.Write "<br>" & vbCrLf
  f.Write "<br>" & vbCrLf
  f.Write "[url=http://auno.org/ao/equip.php?noedit=1"
  f.Write AunoID
  f.Write "]My Equipment[/url]"
  
  f.Write "</html>" & vbCrLf
  
  f.Close










End If ' This is the end of "if this was a Who Am I report
       ' of the "if it IS a Who Am I report" section.

  
  
  
  
  
  
  
  
  
  
  
  
  
' Clean up
  labelPleaseWait.Caption = "Cleaning up"
  subDelay (0.001)
  
  If frmOptions!chkDeleteDataFiles.Value = 1 Then 'checked
    DeleteTheTemporaryFiles
  End If
  MousePointer = vbNormal
  If IsThisAWhoAmIReport = False Then
    f2.Close
    f3.Close
    f4.Close
  End If
  subDelay (0.001)
' Put the boxes back the way they were when we started
  cmdPasteAndSort.Visible = True
  cmdOptions.Visible = True
  Picture1.Visible = False
  Label1.Visible = True
  subDelay (0.001)
'  subDelay (0.0001)
'  DoEvents
'  frmOptions!chkDeleteDataFiles.Visible = True
'  frmOptions!chkPlacePackNamesAfterItemNames.Visible = True
  If IsThisAWhoAmIReport = False Then
    labelPleaseWait.Caption = "Parsing completed - " & (TotalSuccesses + 1) & " items listed"
  End If
  If IsThisAWhoAmIReport = True Then
    labelPleaseWait.Caption = "Character information report written"
  End If
  
  txtOutputDirectoryName.Visible = True
End Sub

Public Function DirExists(ByVal sDirName As String) As Boolean

    Dim sDir As String

    On Error Resume Next

    DirExists = False

    sDir = Dir$(sDirName, vbDirectory)
    If (Len(sDir) > 0) And (Err = 0) Then
        DirExists = True
    End If

End Function

'Name:      DELAY FOR AWHILE
'Purpose:      This sub loops for a passed number of seconds.
'Inputs:       sngDelay    The quantity of time to elapse.
'Note:         fractional parts of a second work!
'Assumptions:  The sub assumes that sngDelay is never greater
'              than 86400. I.e. the sub is not designed to delay
'              for longer than a day.
'
'Effects:      The calling routine's processing is
'              effectively delayed
'              by the # of seconds in the argument, sngDelay.
'
'Returns:      Nothing.
'
Public Sub subDelay(sngDelay As Single)
Const cSecondsInDay = 86400        ' # of seconds in a day.

Dim sngStart As Single             ' Start time.
Dim sngStop  As Single             ' Stop time.
Dim sngNow   As Single             ' Current time.

sngStart = Timer                   ' Get current timer.
sngStop = sngStart + sngDelay      ' Set up stop time based on
                                   ' delay.

Do
    sngNow = Timer                 ' Get current timer again.
    If sngNow < sngStart Then      ' Has midnight passed?
        sngStop = sngStart - cSecondsInDay  ' If yes, reset end.
    End If
    DoEvents                       ' Let OS process other events.

Loop While sngNow < sngStop        ' Has time elapsed?

End Sub


Public Function DetermineSymbiantShortName(ByVal SymbiantLineToParse As String) As String
  Dim SymbiantTypeOnTable As String
  Dim EventualResult1 As String
  Dim Counter1 As Long
  Dim NextCounter1 As Long
  Counter1 = 1
  Do Until Mid(SymbiantLineToParse, Counter1, 1) = " "
    Counter1 = Counter1 + 1
  Loop
  EventualResult1 = Left(SymbiantLineToParse, Counter1 - 1)
  NextCounter1 = Counter1
  Do Until Mid(SymbiantLineToParse, NextCounter1, 1) = ","
    NextCounter1 = NextCounter1 + 1
  Loop
  SymbiantTypeOnTable = Mid(SymbiantLineToParse, (Counter1), (NextCounter1 - (Counter1 + 9)))
  Select Case SymbiantTypeOnTable
    Case " Ocular"
      EventualResult1 = EventualResult1 & "O"
    Case " Brain"
      EventualResult1 = EventualResult1 & "B"
    Case " Ear"
      EventualResult1 = EventualResult1 & "E"
    Case " Chest"
      EventualResult1 = EventualResult1 & "C"
    Case " Feet"
      EventualResult1 = EventualResult1 & "F"
    Case " Thigh"
      EventualResult1 = EventualResult1 & "T"
    Case " Waist"
      EventualResult1 = EventualResult1 & "W"
    Case " Left Hand"
      EventualResult1 = EventualResult1 & "LH"
    Case " Left Wrist"
      EventualResult1 = EventualResult1 & "LW"
    Case " Left Arm"
      EventualResult1 = EventualResult1 & "LA"
    Case " Right Hand"
      EventualResult1 = EventualResult1 & "RH"
    Case " Right Wrist"
      EventualResult1 = EventualResult1 & "RW"
    Case " Right Arm"
      EventualResult1 = EventualResult1 & "RA"
  End Select
  EventualResult1 = EventualResult1 & Mid(SymbiantLineToParse, NextCounter1 + 2, 1)
  DetermineSymbiantShortName = " ( " & EventualResult1 & " )"
End Function




Private Function FindFile(ByVal sFol As String, sFile As String, _
   nDirs As Long, nFiles As Long) As Currency
   
  Dim tFld As Folder, tFil As File, FileName As String
  Dim FoundSuccess As Long
  Dim FoundThisFile As String

    
 
  FoundSuccess = 0
  On Error GoTo Catch
  
  Set fld = fso2.GetFolder(sFol)
  FileName = Dir(fso2.BuildPath(fld.Path, sFile), vbNormal Or _
    vbHidden Or vbSystem Or vbReadOnly)
  While Len(FileName) <> 0
'  While FoundSuccess = 0
    FindFile = FindFile + FileLen(fso2.BuildPath(fld.Path, _
      FileName))
    nFiles = nFiles + 1
    FoundSuccess = 1
'    List1.AddItem fso2.BuildPath(fld.Path, FileName) ' Load ListBox
    FoundThisFile = fso2.BuildPath(fld.Path, FileName)
'    MsgBox (FoundThisFile)
    FileName = Dir()
    DoEvents
  Wend
'  Label1 = "Searching" & vbCrLf & fld.Path & "..."
  nDirs = nDirs + 1
  If fld.SubFolders.Count > 0 Then
    For Each tFld In fld.SubFolders
      DoEvents
      FindFile = FindFile + FindFile(tFld.Path, sFile, nDirs, nFiles)
    Next
  End If
  
  If FoundSuccess = 1 Then
'    MsgBox (FoundThisFile)
  FoundThisFileOnSearch = FoundThisFile
  End If
  
  Exit Function
Catch:   FileName = ""
         Resume Next
        
  
   End Function





