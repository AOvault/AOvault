VERSION 5.00
Begin VB.Form frmOptions 
   Caption         =   "Item Parser Options"
   ClientHeight    =   3255
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7500
   Icon            =   "frmOptions.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3255
   ScaleWidth      =   7500
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command2 
      Caption         =   "&Browse"
      Height          =   495
      Left            =   5760
      TabIndex        =   6
      Top             =   1320
      Width           =   1455
   End
   Begin VB.TextBox txtAOFilePathBox 
      Height          =   285
      Left            =   240
      TabIndex        =   3
      Top             =   840
      Width           =   4935
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Return to main menu"
      Height          =   615
      Left            =   2160
      TabIndex        =   2
      Top             =   2280
      Width           =   3255
   End
   Begin VB.CheckBox chkPlacePackNamesAfterItemNames 
      Caption         =   "Place names of packs with item names"
      Height          =   255
      Left            =   360
      TabIndex        =   1
      Top             =   120
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.CheckBox chkDeleteDataFiles 
      Caption         =   "Delete data files when finished"
      Height          =   255
      Left            =   2438
      TabIndex        =   0
      Top             =   240
      Value           =   1  'Checked
      Width           =   2655
   End
   Begin VB.Line Line2 
      X1              =   7320
      X2              =   120
      Y1              =   2040
      Y2              =   2040
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   7320
      Y1              =   600
      Y2              =   600
   End
   Begin VB.Label Label3 
      Caption         =   "Your Anarchy Online folder location"
      Height          =   375
      Left            =   5520
      TabIndex        =   5
      Top             =   720
      Width           =   1575
   End
   Begin VB.Label Label2 
      Caption         =   "To change your Anarchy Online installation folder location, type in the new location above or press the Browse button."
      Height          =   495
      Left            =   240
      TabIndex        =   4
      Top             =   1320
      Width           =   5295
   End
End
Attribute VB_Name = "frmOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
  Const ConfigFileName = "aois.cfg"
  Dim ConfigHeaderName As String
  Dim ConfigVersionName As String


Private Sub Command1_Click()
  Const ForReading = 1, ForWriting = 2, ForAppending = 8
  Dim fso, f
  Set fso = CreateObject("Scripting.FileSystemObject")
  Set f = fso.OpenTextFile(ConfigFileName, ForWriting, True)
  f.Write ConfigHeaderName
  f.Write vbCrLf
  f.Write ConfigVersionName
  f.Write vbCrLf
  f.Write txtAOFilePathBox.Text
  f.Write vbCrLf
  f.Close
  
  Unload Me

End Sub

Public Function FileExists(ByVal sDirName As String) As Boolean
  On Error GoTo Errortrap
  Const ForReading = 1, ForWriting = 2, ForAppending = 8
  Dim fso, f
  Set fso = CreateObject("Scripting.FileSystemObject")
  Set f = fso.OpenTextFile(sDirName, ForReading)
  f.Close
  FileExists = True

Errortrap:
  If Err.Number = 53 Then
    FileExists = False
MsgBox ("config file not found...")
  End If

End Function




Private Sub Command2_Click()
  frmFindLocation.Show
End Sub

Private Sub form_load()

  Const ForReading = 1, ForWriting = 2, ForAppending = 8
  Dim fso, f
  Dim OverallUserName As String
  Dim AOFilePath As String
  
  Set fso = CreateObject("Scripting.FileSystemObject")
  
  If FileExists(ConfigFileName) Then
    Set f = fso.OpenTextFile(ConfigFileName, ForReading)
    ConfigHeaderName = f.ReadLine
    ConfigVersionName = f.ReadLine
    AOFilePath = f.ReadLine
    f.Close
    txtAOFilePathBox.Text = AOFilePath
  End If
  


End Sub

