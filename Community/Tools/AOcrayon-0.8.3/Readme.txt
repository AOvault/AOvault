﻿========
AOcrayon
========

AOcrayon is a third-party tool that was made with the intent to help players of
Funcom MMOs (like Anarchy Online and Age of Conan) change their chat text colors
easily, without mucking about with 'complex' XML file editing.

Note: This changes only what you see. It has no impact on anyone else.


Features:
=========

 - Change your chat colors to whatever you want using a color picker.
 - Supports custom GUIs (version 0.6.0 and above)
 - Advanced mode for changing any color listed in the TextColors.xml file.
 - Doesn't do anything to your output, so don't worry about someone else being
   annoyed with your changes.


Usage:
======

To reload changes without restarting your game client:
 - Make your changes and click "Save".
 - Anarchy Online:
    - Open the "debug window" by hitting  CTRL  +  SHIFT  +  F7 .
    - In the "Actions" tab, click "Reload menus".
      (You may have to repeat this step a few times before anything happens)
 - Age of Conan / The Secret World:
    - Simply type "/reloadui" in-game, and press ENTER.

Installation:
=============

Installer:
 - Download the file "AOcrayon-0.8.1.exe" to wherever you store your stuff.
 - Run the installer and follow the instructions given.

Manual installation:
 - Download the file "AOcrayon-0.8.1.zip" to wherever you store your stuff.
 - Extract the content to a desired location (e.g. "C:\Program Files\AOcrayon").
 - (Optional) Create a shortcut to the file "AOcrayon.exe" on your desktop.


Links:
======

 http://www.wrongplace.net/            News site
 http://aodevs.com/                    Release site for FunCom 3rd party tools
 http://www.anarchy-online.com/        Official Anarchy Online site
 http://www.ageofconan.com/            Official Age of Conan site
 http://www.thesecretworld.com/        Official The Secret World site

Credits:
========

Author: Mawerick (mawerick@wrongplace.net)
(Please write in english only)

Special thanks to Oofbig for coming up with the cool name!
