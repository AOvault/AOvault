﻿Imports System.Resources

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("XRDB4")> 
<Assembly: AssemblyDescription("Xyphos Resource DataBase tools")> 
<Assembly: AssemblyCompany("Xyphos Productions")> 
<Assembly: AssemblyProduct("XRDB4")> 
<Assembly: AssemblyCopyright("Copyright © Xyphos Productions 2009-2013")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("56f72511-0174-406d-bcd0-dadda6bf68d6")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("4.8.2.3")> 
<Assembly: AssemblyFileVersion("4.8.2.3")> 

<Assembly: NeutralResourcesLanguageAttribute("en-US")> 