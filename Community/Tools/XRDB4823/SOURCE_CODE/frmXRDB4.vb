﻿' frmXRDB4.vb - Main form code
' Programmer: William "Xyphos" Scott <TheGreatXyphos@gmail.com>
' Date: Aug 21, 2009
' Updated: Apr 10, 2009
'
'This file is part of XRDB4.
'
'    XRDB4 is free software: you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation, either version 3 of the License, or
'    (at your option) any later version.
'
'    XRDB4 is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'
'    You should have received a copy of the GNU General Public License
'    along with XRDB4.  If not, see <http://www.gnu.org/licenses/>.


Imports System.Web 'This is for the updater, it tends to freeze without it
Public Class frmXRDB4

    Private cParse As Parser
    Private Parsing As Boolean

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Show()
        Me.Refresh()

        cmbPri.SelectedIndex = System.Threading.ThreadPriority.Normal

        'MsgBox("Hook Wait")

        Startup()

        lblAOPath.Text = AOPath
        lblExtractPath.Text = ExtractPath
        lblOutputPath.Text = ParsePath
        lblAOVersion.Text = PatchVersion()
        lblLegal.Text = lblLegal.Text.Replace("{VER}", Process.GetCurrentProcess.MainModule.FileVersionInfo.ProductVersion)

        LoadPlugins()
        FindDataFiles()

        'hokay.. let's run command line tasks...
        If JobExtract Then btnExtract_Click(Nothing, Nothing)

        If JobParse Then
            OldData = IIf(ODF <> "", ODF, OldData)
            If OldData.ToUpper = "SKIP" Then OldData = ""

            'OldData = IIf((ODF.ToUpper = "SKIP") Or (ODF = ""), OldData, ODF)
            NewData = IIf(NDF = "", NewData, NDF)

            FindDF(lbParseOld, OldData)
            FindDF(lbParseNew, NewData)

            btnParse_Click(Nothing, Nothing)
        End If

        If JobQuit Then Shutdown(ExitCodes.NoError)

    End Sub

    Private Sub frmXRDB4_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        Shutdown(ExitCodes.NoError)
    End Sub

    Private Sub btnFindPlugins_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFindPlugins.Click
        LoadPlugins()
    End Sub







    Public Function LoadPlugins() As Boolean
        Trace.WriteLine("Looking for Plugins...")

        Dim PluginDir As String = AppPath() & "Plugins"

        PluginDLLs.Clear()
        'lbPluginsOLD.Items.Clear()
        lvPlugins.Items.Clear()
        'LastPlugin = ""

        If Not My.Computer.FileSystem.DirectoryExists(PluginDir) Then
            Trace.WriteLine("Created plugin dir")
            My.Computer.FileSystem.CreateDirectory(PluginDir)
        End If

        Dim SI As Integer = -1
        Dim DI As New System.IO.DirectoryInfo(AppPath() & "Plugins")
        For Each FI As System.IO.FileInfo In DI.GetFiles("*.dll")
            Try
                Trace.WriteLine(String.Format( _
                    "Loading {0}{1}{0}...", Chr(34), FI.FullName))

                Dim ASM As System.Reflection.Assembly = System.Reflection.Assembly.LoadFrom(FI.FullName)

                For Each Type As System.Type In ASM.GetTypes
                    If Type.IsPublic AndAlso Type.IsClass Then
                        If GetType(XRDB4_Extras.Plugin).IsAssignableFrom(Type) Then
                            If Not PluginDLLs.ContainsKey(Type.FullName) Then
                                PluginDLLs.Add(Type.FullName, FI.FullName)

                                Dim SQ As String() = Type.FullName.Split(".") ' FI.FullName.Split(".")
                                Dim LVI As New ListViewItem
                                LVI.Text = SQ(0)
                                'LVI.SubItems.Add(SQ(0))
                                LVI.SubItems.Add(SQ(1))
                                LVI.SubItems.Add(ASM.GetName.Version.ToString)
                                lvPlugins.Items.Add(LVI)
                                Dim I As Integer = lvPlugins.Items.Count - 1

                                'Dim I As Integer = lbPluginsOLD.Items.Add(Type.FullName)
                                'Dim I As Integer = lvPlugins.Items.Add(
                                If LCase(Type.FullName) = LCase(LastPlugin) Then
                                    SI = I
                                End If
                            Else
                                Trace.WriteLine(String.Format( _
                                    "Dupe Plugin Ignored: {0}{1}{0} in dll {0}{2}{0}.", _
                                    Chr(34), Type.FullName, FI.FullName))
                            End If
                        Else
                            Trace.WriteLine(String.Format( _
                                "Plugin unassignable: {0}{1}{0} in dll {0}{2}{0}.", _
                                Chr(34), Type.FullName, FI.FullName))
                        End If
                    Else

                    End If
                Next

            Catch ex As Exception
                Trace.WriteLine("Load Failed, Exception was thrown")
                Trace.WriteLine(ex.ToString)
            End Try
        Next

        If SI = -1 Then
            'If lbPluginsOLD.Items.Count > 0 Then lbPluginsOLD.SelectedIndex = 0
            If lvPlugins.Items.Count > 0 Then
                lvPlugins.SelectedIndices.Clear()
                lvPlugins.SelectedIndices.Add(0)
            End If
        Else
            'If lbPluginsOLD.Items.Count > 0 Then lbPluginsOLD.SelectedIndex = SI
            If lvPlugins.Items.Count > 0 Then
                lvPlugins.SelectedIndices.Clear()
                lvPlugins.SelectedIndices.Add(SI)
            End If
        End If

        Return (SI > -1)

    End Function

    Private Sub btnAOPath_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAOPath.Click
        Dim FB As New FolderBrowserDialog
        With FB
            .Description = "Please locate your AO Install Folder."
            .RootFolder = Environment.SpecialFolder.Desktop
            .ShowNewFolderButton = False
            If .ShowDialog(Me) = Windows.Forms.DialogResult.Cancel Then Exit Sub
            AOPath = .SelectedPath
            lblAOPath.Text = AOPath
        End With

        lblAOVersion.Text = PatchVersion()
    End Sub



    ' fix for preventing no selection
    Private AllowLVPC As Boolean = True
    Private LVPC As Integer = 0
    Private Sub lvPlugins_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lvPlugins.MouseDown
        AllowLVPC = (lvPlugins.GetItemAt(e.X, e.Y) Is Nothing)
    End Sub

    Private Sub lvPlugins_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvPlugins.SelectedIndexChanged

        If lvPlugins.SelectedIndices.Count = 0 Then
            If AllowLVPC Then lvPlugins.Items(LVPC).Selected = True
            Exit Sub
        End If

        Dim LVI As ListViewItem = lvPlugins.SelectedItems(0)
        LVPC = LVI.Index
        LastPlugin = LVI.Text & "." & LVI.SubItems(1).Text

        'now to initiate the plugin and fetch it's extract schema
        Dim ASM As Type = System.Reflection.Assembly.LoadFrom(PluginDLLs(LastPlugin)).GetType(LastPlugin)
        Plugin = DirectCast(Activator.CreateInstance(ASM), XRDB4_Extras.Plugin)

        lvExtractTypes.Items.Clear()

        Try
            ExtractRecords = Plugin.ExtractInfo()
        Catch ex As Exception
            ExtractRecords = Nothing
        End Try

        If ExtractRecords Is Nothing Then Exit Sub
        If Not IsArray(ExtractRecords) Then Exit Sub

        btnExtract.Enabled = EnableExtract()

        For I As Integer = 0 To ExtractRecords.Length - 1
            LVI = lvExtractTypes.Items.Add("") '(ExtractRecords(I).TypeName)
            LVI.Checked = ExtractRecords(I).Checked
            LVI.SubItems.Add(ExtractRecords(I).TypeName)
            LVI.SubItems.Add(String.Format("0x{0:X}", ExtractRecords(I).RecordType))
            LVI.SubItems.Add("")
            LVI.ImageIndex = -1 ' 0
        Next

        FindDataFiles()

    End Sub

    Private Sub btnExtractPath_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExtractPath.Click
        Dim FB As New FolderBrowserDialog
        With FB
            .Description = "Please locate where you want to extract data."
            .RootFolder = Environment.SpecialFolder.Desktop
            .ShowNewFolderButton = True
            If .ShowDialog(Me) = Windows.Forms.DialogResult.Cancel Then Exit Sub
            ExtractPath = .SelectedPath
            lblExtractPath.Text = ExtractPath
            FindDataFiles()
        End With
    End Sub

    Private Sub btnCheckAOVersion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckAOVersion.Click
        lblAOVersion.Text = PatchVersion()
    End Sub

    Public Sub btnExtract_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExtract.Click
        TabControl1.SelectedIndex = 1
        If Not CheckAOPath() Then
            If JobExtract Then Shutdown(ExitCodes.InvalidAOFolder) Else Exit Sub
        End If

        If Plugin Is Nothing Then
            MsgBox("Plugin required for ExtractInfo", MsgBoxStyle.Critical)
            If JobExtract Then Shutdown(ExitCodes.InvalidPlugin) Else Exit Sub
        End If

        Try
            If Not My.Computer.FileSystem.DirectoryExists(ExtractPath) Then Throw New Exception("")
        Catch ex As Exception
            MsgBox("Extract path is invalid or missing", MsgBoxStyle.Critical)
            If JobExtract Then Shutdown(ExitCodes.InvalidExtractPath) Else Exit Sub
        End Try

        Dim DTS As Date = Now

        CurrentPatch = PatchVersion()
        lblAOVersion.Text = CurrentPatch

        ' Clear picures
        For I As Integer = 0 To lvExtractTypes.Items.Count - 1
            lvExtractTypes.Items(I).ImageIndex = ExtractImageBlank
            lvExtractTypes.Items(I).SubItems(3).Text = ""
        Next

        lvExtractTypes.Enabled = False
        btnExtract.Enabled = False

        Dim XT As XTrees = Nothing

        Try
            XT = New XTrees(pbExtractMarqee)
        Catch ex As Exception
            Dim CE As New XRDB4_Extras.CustomException(ex.Message, "Error creating XTrees object")
            If JobExtract Then Shutdown(ExitCodes.Unexpected) Else Exit Sub
        End Try

        Dim TR As Integer = 0
        Dim CV As Integer = pvInt(CurrentPatch)

        For I As Integer = 0 To ExtractRecords.Length - 1
            lvExtractTypes.Items(I).ImageIndex = ExtractImageCurrent

            If ExtractRecords(I).Checked Then
                Dim RT As Integer = ExtractRecords(I).RecordType

                'Decoder switch
                Dim DC As Boolean = ((RT = RDB_ITEM) OrElse (RT = RDB_NANO)) AndAlso (CV < 14020100)

                Dim X As Integer = XT.Export(RT, DC)
                lvExtractTypes.Items(I).ImageIndex = ExtractImageGreenCheck
                lvExtractTypes.Items(I).SubItems(3).Text = FormatNumber(X, 0)
                TR += X
            Else
                lvExtractTypes.Items(I).ImageIndex = ExtractImageRedX
            End If
        Next

        XT.Dispose()
        lvExtractTypes.Enabled = True
        btnExtract.Enabled = True

        FindDataFiles()

        Dim DTX As Date = DateTime.Now
        Dim Span As String = DTX.Subtract(DTS).ToString
        Span = Span.Substring(0, Span.IndexOf("."))


        If JobExtract Then Exit Sub 'bypass the msgbox
        MsgBox(String.Format( _
            "Time Elapsed {0}{1}{2} Records Extracted.", _
            Span, vbNewLine, FormatNumber(TR, 0)), MsgBoxStyle.Information)

    End Sub

    Private Sub lvExtractTypes_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles lvExtractTypes.ItemCheck
        ExtractRecords(e.Index).Checked = CBool(e.NewValue = CheckState.Checked)
        btnExtract.Enabled = EnableExtract()
    End Sub

    Private Function EnableExtract() As Boolean
        If (ExtractRecords Is Nothing) OrElse (Not IsArray(ExtractRecords)) Then Return False
        If ExtractRecords.Length = 0 Then Return False
        Dim Allow As Boolean = False
        For I As Integer = 0 To ExtractRecords.Length - 1
            If ExtractRecords(I).Checked Then Allow = True
        Next
        Return Allow
    End Function

    Private Sub btnOutputPath_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOutputPath.Click
        Dim FB As New FolderBrowserDialog
        With FB
            .Description = "Please locate where output should save."
            .RootFolder = Environment.SpecialFolder.Desktop
            .ShowNewFolderButton = True
            If .ShowDialog(Me) = Windows.Forms.DialogResult.Cancel Then Exit Sub
            ParsePath = .SelectedPath
            lblOutputPath.Text = ParsePath
        End With
    End Sub

    Private Sub btnParseFind_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnParseFind.Click
        FindDataFiles()
    End Sub

    Private Sub FindDataFiles()
        'Exit Sub
        lbParseNew.Items.Clear()
        lbParseOld.Items.Clear()
        DataFiles.Clear()

        lbParseNew.Items.Add("NONE" & Strings.Space(20) & vbTab & "<NO DATA FILE>" & vbTab)
        lbParseOld.Items.Add("SKIP" & Strings.Space(20) & vbTab & "<SKIP COMPARE>" & vbTab)

        Try
            If Not My.Computer.FileSystem.DirectoryExists(ExtractPath) Then Exit Sub
        Catch ex As Exception
            Exit Sub
        End Try

        Dim DI As New System.IO.DirectoryInfo(ExtractPath)
        Dim RX As New System.Text.RegularExpressions.Regex(LastPlugin & ".+?\.xrdb4", System.Text.RegularExpressions.RegexOptions.Singleline Or System.Text.RegularExpressions.RegexOptions.IgnoreCase Or System.Text.RegularExpressions.RegexOptions.Multiline)

        For Each FI As System.IO.FileInfo In DI.GetFiles("*.xrdb4")
            Dim GZB As New gzbFile(FI.FullName, IO.FileMode.Open, IO.Compression.CompressionMode.Decompress)
            Dim GZH As gzbFile.FileXRDB4Header = GZB.ReadHeader()
            GZB.Close()
            If Not CheckDataFile(GZH) Then Continue For

            If chkParsePluginOnly.Checked AndAlso Not RX.IsMatch(FI.Name) Then Continue For

            Dim DF As String = pvStr(GZH.FileVer) & Strings.Space(10) & vbTab & FI.Name & vbTab & FI.FullName
            lbParseNew.Items.Add(DF)
            lbParseOld.Items.Add(DF)
            If Not DataFiles.ContainsKey(DF) Then DataFiles.Add(DF, FI.FullName)
        Next

        'Crop the unwanted ones.
        If lbParseOld.Items.Count > 1 Then lbParseOld.Items.RemoveAt(lbParseOld.Items.Count - 2)
        If lbParseNew.Items.Count > 1 Then lbParseNew.Items.RemoveAt(lbParseNew.Items.Count - 1)

        lbParseNew.SelectedIndex = lbParseNew.Items.Count - 1

        If lbParseOld.Items.Count > 1 Then
            lbParseOld.SelectedIndex = lbParseOld.Items.Count - 1 'scroll fix
            DoEvents()
            lbParseOld.SelectedIndex = lbParseOld.Items.Count - 2
        Else
            lbParseOld.SelectedIndex = 0
        End If

        btnParse.Enabled = CBool(lbParseNew.Items(lbParseNew.SelectedIndex) <> "NONE")

    End Sub

    Public Sub btnParse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnParse.Click
        TabControl1.SelectedIndex = 2

        If Parsing Then
            cParse.ForceAbort("Aborted by user")
            If JobParse Then Shutdown(ExitCodes.ParseAborted) Else Exit Sub
        End If

        If Plugin Is Nothing Then
            MsgBox("Plugin Required for parsing", MsgBoxStyle.Critical)
            If JobParse Then Shutdown(ExitCodes.InvalidPlugin) Else Exit Sub
        End If

        Try
            If Not My.Computer.FileSystem.DirectoryExists(ParsePath) Then Throw New Exception("")
        Catch ex As Exception
            MsgBox("Invalid Output Path", MsgBoxStyle.Critical)
            If JobParse Then Shutdown(ExitCodes.InvalidOutputPath) Else Exit Sub
        End Try

        Try
            If Not My.Computer.FileSystem.FileExists(NewData) Then Throw New Exception("")
        Catch ex As Exception
            MsgBox("New Datafile is invalid or missing", MsgBoxStyle.Critical)
            If JobParse Then Shutdown(ExitCodes.InvalidNewData) Else Exit Sub
        End Try


        'btnParse.Enabled = False
        btnParse.Text = "Abort"

        Dim DTS As Date = DateTime.Now
        cParse = New Parser(lblParseStatus, pbParse, _
                        cmbPri, chkDebugTrace.Checked)

        cParse.ForcePriority(Threading.ThreadPriority.Highest - cmbPri.SelectedIndex)
        Parsing = True

        Select Case cParse.Parse
            Case Parser.ParserReturns.Parsing
                MsgBox("WTF? Still Parsing!")
                'MsgBox("I made a boo-boo.", MsgBoxStyle.Critical)
                If JobParse Then Shutdown(ExitCodes.Unexpected)
            Case Parser.ParserReturns.Success
                lblParseStatus.Text = "Parse Success!"

                Dim DTX As Date = DateTime.Now
                Dim Span As String = DTX.Subtract(DTS).ToString
                Span = Span.Substring(0, Span.IndexOf("."))
                If Not JobParse Then
                    MsgBox(String.Format( _
                           "Time Elapsed {0}{1}{2} Records Parsed.", _
                           Span, vbNewLine, FormatNumber(TotalParsed, 0)), _
                           MsgBoxStyle.Information)
                End If
            Case Parser.ParserReturns.Aborted
                lblParseStatus.Text = "Parse Aborted!"
                cParse.Close("Parse Aborted by User") 'make sure it closes
                MsgBox("Parse Aborted!" & vbNewLine & cParse.AbortMsg, MsgBoxStyle.Exclamation)
                If JobParse Then Shutdown(ExitCodes.ParseAborted)
            Case Parser.ParserReturns.Crashed
                lblParseStatus.Text = "Plugin Crashed!"
                cParse.Close("Plugin Crashed!") 'make sure it closes
                MsgBox("Plugin Crashed!", MsgBoxStyle.Exclamation)
                If JobParse Then Shutdown(ExitCodes.PluginCrashed)
            Case Parser.ParserReturns.Failed
                'gotta debug.
                lblParseStatus.Text = "Parse Failed!"
                wbDebug.Navigate("file://" & AppPath() & "XRDB4-DebugTrace.htm")
                cParse.Close("Parse Failed!") 'make sure it closes
                If JobParse Then Shutdown(ExitCodes.Unexpected)
                MsgBox("Parse Failed! Debug Information was dumped to the 'Debugger' tab.", _
                       MsgBoxStyle.Exclamation)
        End Select

        cParse = Nothing
        'btnParse.Enabled = True
        btnParse.Text = "Parse"
        Parsing = False
        pbParse.Value = 0
    End Sub

    Private Sub FindDF(ByVal lb As ListBox, ByVal DF As String)
        For I As Integer = 0 To lb.Items.Count - 1
            Dim D As String = lb.Items(I)
            Dim S As String() = Split(D, vbTab)
            If S(2).ToLower = DF.ToLower Then
                lb.SelectedIndex = I
                Exit Sub
            End If
        Next
    End Sub

    Private Sub lbParseOld_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbParseOld.SelectedIndexChanged
        If lbParseOld.SelectedIndex < 0 Then Exit Sub
        Dim DF As String = lbParseOld.Items(lbParseOld.SelectedIndex)
        Dim S As String() = Split(DF, vbTab)
        OldData = S(2)
        lblOldData.Text = S(1) 'Strings.Left(S(1), InStrRev(UCase(S(1)), ".XRDB4") - 1)
        'OldData = lbParseOld.SelectedIndex
    End Sub

    Private Sub lbParseNew_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbParseNew.SelectedIndexChanged
        If lbParseNew.SelectedIndex < 0 Then Exit Sub
        Dim DF As String = lbParseNew.Items(lbParseNew.SelectedIndex)
        Dim S As String() = Split(DF, vbTab)
        NewData = S(2)
        lblNewData.Text = S(1) 'Strings.Left(S(1), InStrRev(UCase(S(1)), ".XRDB4") - 1)
        'NewData = lbParseNew.SelectedIndex
    End Sub

    Private Sub rtfGPL_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkClickedEventArgs) Handles rtfGPL.LinkClicked
        System.Diagnostics.Process.Start(e.LinkText)
    End Sub

    Private Sub cmbPri_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbPri.SelectedIndexChanged
        Try
            cParse.ForcePriority(Threading.ThreadPriority.Highest - cmbPri.SelectedIndex)
            DoEvents()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub chkParsePluginOnly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkParsePluginOnly.CheckedChanged
        FindDataFiles()
    End Sub

    Private Sub btnDebuggerReload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDebuggerReload.Click
        wbDebug.Navigate("file://" & AppPath() & "XRDB4-DebugTrace.htm")
    End Sub

    Private Sub btnUpdates_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdates.Click
        RunUpdater()
    End Sub

    Private Sub rtfGPL_TextChanged(sender As Object, e As EventArgs) Handles rtfGPL.TextChanged

    End Sub
End Class
