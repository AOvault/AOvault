﻿' Main.vb - Various project-global stuff
' Programmer: William "Xyphos" Scott <TheGreatXyphos@gmail.com>
' Date: Aug 21, 2009
' Updated: Apr 10, 2010
'
'This file is part of XRDB4.
'
'    XRDB4 is free software: you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation, either version 3 of the License, or
'    (at your option) any later version.
'
'    XRDB4 is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'
'    You should have received a copy of the GNU General Public License
'    along with XRDB4.  If not, see <http://www.gnu.org/licenses/>.

Imports System.IO

Module Main
#Region "Public Const"
    Public Const RDBPATH As String = "\cd_image\data\db\ResourceDatabase"
    Public Const DEADC0DE As Int32 = &HDEADC0DE
    Public Const XRDB As Int32 = &H58524442
    Public Const VerNum As Int32 = 40500 'datafile format number

    Public Const ExtractImageBlank = -1
    Public Const ExtractImageGreenCheck = 0
    Public Const ExtractImageRedX = 1
    Public Const ExtractImageCurrent = 2

    Public Const RDB_ITEM = &HF4254
    Public Const RDB_NANO = &HFDE85
#End Region
#Region "Public Vars"
    Public AOPath As String = ""
    Public ExtractPath As String = ""
    Public ParsePath As String = ""
    Public LastPlugin As String = ""
    Public CurrentPatch As String = ""
    Public UpdaterSite As String = ""

    Public Settings As New XRDB4_Extras.cfgFile

    Public Plugin As XRDB4_Extras.Plugin
    Public ExtractRecords() As XRDB4_Extras.ExtractRecordDictionary.ExtractRecord

    Public PluginDLLs As New Hashtable

    Public DataFiles As New Hashtable
    Public OldData As String
    Public NewData As String

    Public TotalParsed As Integer

    Public CLine As Boolean


    Public JobExtract As Boolean = False
    Public JobParse As Boolean = False
    Public JobQuit As Boolean = False

    Public ODF As String = ""
    Public NDF As String = ""

    Public PCMD As String = ""

#End Region
#Region "Exit Codes"
    Public Enum ExitCodes
        Unexpected = 0
        NoError
        InvalidAOFolder
        InvalidPlugin
        InvalidExtractPath
        InvalidOutputPath
        InvalidOldData
        InvalidNewData
        ParsingError
        ParseAborted
        PluginCrashed
        InvalidCommandLine
        UpdatingSoftware
    End Enum
#End Region

    Public Sub Startup()

        Dim cfgFile As String = AppPath() & "Config\XRDB4.cfg"
        Dim Sect As XRDB4_Extras.cfgFile.cfgSection

        Settings.Load(cfgFile)
        Sect = Settings.AddSection("Settings") 'Returns existing section if exist
        Sect.AddKey("AOPath")
        Sect.AddKey("LastPlugin")
        Sect.AddKey("ExtractPath")
        Sect.AddKey("ParsePath")
        Sect.AddKey("UpdateSite")

        AOPath = Sect("AOPath")
        LastPlugin = Sect("LastPlugin")
        ExtractPath = Sect("ExtractPath")
        ParsePath = Sect("ParsePath")
        UpdaterSite = Sect("UpdateSite")
        If UpdaterSite = "" Then UpdaterSite = "http://www.xyphos.com"

        RunCommands()
    End Sub

    Public Sub RunCommands()

        'Dim JobUpdate As Boolean = False


        'grab command list...
        Dim CMD As New List(Of String)
        For Each C As String In My.Application.CommandLineArgs
            CMD.Add(C)
            CLine = True 'set CommandLine mode
        Next

        'and parse the commands
        Try
            While CMD.Count > 0
                Select Case CMD(0).Trim.ToUpper
                    Case "-A"   'AO Install path; shift 2
                        AOPath = CMD(1)
                        If Not CheckAOPath(AOPath) Then Shutdown(ExitCodes.InvalidAOFolder)
                        CMD.RemoveAt(1)

                    Case "-L" 'plugin name
                        LastPlugin = CMD(1)
                        If Not frmXRDB4.LoadPlugins Then
                            MsgBox("Invalid Plugin Specified: " & vbNewLine & CMD(1), MsgBoxStyle.Critical)
                            Shutdown(ExitCodes.InvalidPlugin)
                        End If
                        CMD.RemoveAt(1)

                    Case "-E" 'extract path
                        ExtractPath = CMD(1)
                        If Not My.Computer.FileSystem.DirectoryExists(ExtractPath) Then
                            MsgBox("Invalid Extract Path Specified: " & vbNewLine & CMD(1), MsgBoxStyle.Critical)
                            Shutdown(ExitCodes.InvalidExtractPath)
                        End If
                        CMD.RemoveAt(1)

                    Case "-X" 'Extract mode (performed later down)
                        JobExtract = True

                    Case "-T" 'output path
                        ParsePath = CMD(1)
                        If Not My.Computer.FileSystem.DirectoryExists(ParsePath) Then
                            MsgBox("Invalid Output Path Specified: " & vbNewLine & CMD(1), MsgBoxStyle.Critical)
                            Shutdown(ExitCodes.InvalidOutputPath)
                        End If
                        CMD.RemoveAt(1)

                    Case "-O" 'use old datafile
                        ODF = CMD(1).Trim
                        'If ODF.ToUpper = "SKIP" Then ODF = ""
                        If ODF <> "SKIP" Then
                            If Not CheckDataFile(ODF) Then
                                MsgBox("Invalid Old Datafile Specified: " & vbNewLine & CMD(1), MsgBoxStyle.Critical)
                                Shutdown(ExitCodes.InvalidOldData)
                            End If
                        End If
                        CMD.RemoveAt(1)

                    Case "-N" 'use old datafile
                        NDF = CMD(1).Trim
                        If Not CheckDataFile(NDF) Then
                            MsgBox("Invalid New Datafile Specified: " & vbNewLine & CMD(1), MsgBoxStyle.Critical)
                            Shutdown(ExitCodes.InvalidNewData)
                        End If
                        CMD.RemoveAt(1)

                    Case "-H" 'thread priority
                        Dim TP As Integer = Math.Max(1, Math.Min(5, Val(CMD(1)))) - 1
                        frmXRDB4.cmbPri.SelectedIndex = TP

                    Case "-P" ' Run parse
                        JobParse = True

                    Case "-B" ' Run Parse with debugging enabled
                        JobParse = True
                        frmXRDB4.chkDebugTrace.Checked = True

                    Case "-Q" ' Quits after tasks
                        JobQuit = True

                    Case "-U" ' Run updater
                        RunUpdater()

                    Case "-Z"
                        CMD.RemoveAt(0)
                        PCMD = Join(CMD.ToArray(), " ")
                        CMD.Clear()
                        CMD.Add("") 'dummy to remove below
                    Case Else
                        'Throw New Exception("Invalid Command Line")
                End Select
                CMD.RemoveAt(0)
            End While

        Catch ex As Exception
            MsgBox("Invalid Command Line Arguments Specified: " & vbNewLine & Command(), MsgBoxStyle.Critical)
            Shutdown(ExitCodes.InvalidCommandLine)
        End Try


    End Sub


    Public Sub Shutdown(ByVal ExitCode As ExitCodes)

        If Not CLine Then
            Dim cfgFile As String = AppPath() & "Config\XRDB4.cfg"
            Dim Sect As XRDB4_Extras.cfgFile.cfgSection

            Sect = Settings("Settings")
            Sect("AOPath") = AOPath
            Sect("LastPlugin") = LastPlugin
            Sect("ExtractPath") = ExtractPath
            Sect("ParsePath") = ParsePath
            Sect("UpdateSite") = UpdaterSite
            Settings.Save(cfgFile)
        End If

        'attempt to exit with status code
        System.Environment.Exit(ExitCode)


        End 'Hardcore Shutdown if above fails for any odd reason
    End Sub

    Public Function AppPath() As String
        Return My.Application.Info.DirectoryPath & "\"
    End Function

    Public Sub DoEvents()
        System.Windows.Forms.Application.DoEvents()
    End Sub

    Public Function CheckAOPath(Optional ByVal strPath As String = vbNullString) As Boolean
        Dim sPath As String = IIf(strPath = vbNullString, AOPath, strPath)

        Try
            If Not My.Computer.FileSystem.FileExists(sPath & "\anarchy.exe") Then Throw New Exception("")
            If Not My.Computer.FileSystem.FileExists(sPath & "\version.id") Then Throw New Exception("")
            If Not My.Computer.FileSystem.FileExists(sPath & RDBPATH & ".idx") Then Throw New Exception("")
            If Not My.Computer.FileSystem.FileExists(sPath & RDBPATH & ".dat") Then Throw New Exception("")
        Catch ex As Exception
            MsgBox("Invalid AO Install Path. Please locate your AO Folder.", MsgBoxStyle.Critical)
            Return False
        End Try

        Return True
    End Function

    Public Function PatchVersion(Optional ByVal Glue As String = ".") As String
        If Not CheckAOPath() Then Return "XX.XX.XX.XX"
        Dim strVersion As String = My.Computer.FileSystem.ReadAllText(AOPath & "\version.id")
        Return pvStr(pvInt(strVersion))
    End Function

    Public Function pvStr(ByVal pvi As Integer, Optional ByVal Glue As String = ".") As String
        Dim Q As String = String.Format("{0:D8}", pvi)
        Dim Major As Int32 = Val(Q.Substring(0, 2))
        Dim Minor As Int32 = Val(Q.Substring(2, 2))
        Dim Revision As Int32 = Val(Q.Substring(4, 2))
        Dim Build As Int32 = Val(Q.Substring(6, 2))

        Return String.Format("{1:D2}{0}{2:D2}{0}{3:D2}{0}{4:D2}", _
                     Glue, Major, Minor, Revision, Build)
    End Function

    Public Function pvInt(ByVal pvs As String, Optional ByVal Delimiter As Char = ".") As Integer
        pvs = pvs.Trim(" ", vbTab, vbCr, vbLf).Replace("_EP1", "")
        Dim G() As String = pvs.Split(Delimiter)

        'decimal fix for older AO versions, allow testlive version support
        For I As Int32 = G.Length To 3
            pvs &= (Delimiter & "0")
        Next
        G = pvs.Split(Delimiter)    'split again after fix

        Dim Ret As Integer = Val(String.Format("{0:D2}{1:D2}{2:D2}{3:D2}", CInt(G(0)), CInt(G(1)), CInt(G(2)), CInt(G(3))))
        Return Ret
    End Function

    Public Function ProgressValue(ByVal Min As Int32, ByVal Max As Int32) As Int32
        Return CInt((Min / Max) * 100)
    End Function

    Public Function CheckDataFile(ByVal H As gzbFile.FileXRDB4Header) As Boolean
        Return CBool((H.Marker = DEADC0DE) And (H.IDTag = XRDB) And (H.Version = VerNum))
    End Function

    Public Function CheckDataFile(ByVal FileName As String) As Boolean
        If Not My.Computer.FileSystem.FileExists(FileName) Then Return False
        Dim GZB As New gzbFile(FileName, IO.FileMode.Open, IO.Compression.CompressionMode.Decompress)
        Dim GZH As gzbFile.FileXRDB4Header = GZB.ReadHeader()
        GZB.Close()
        If Not CheckDataFile(GZH) Then Return False
        Return True
    End Function


    Public Sub RunUpdater()
        Dim XU As New XyphosUpdaterLib.Updater
        Dim FVI As FileVersionInfo = Process.GetCurrentProcess.MainModule.FileVersionInfo
        If XU.Update(UpdaterSite, FVI) Then
            Shutdown(ExitCodes.UpdatingSoftware)
        End If
    End Sub


End Module
