﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmXRDB4
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmXRDB4))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lvPlugins = New System.Windows.Forms.ListView()
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnFindPlugins = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnAOPath = New System.Windows.Forms.Button()
        Me.lblAOPath = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.btnCheckAOVersion = New System.Windows.Forms.Button()
        Me.pbExtractMarqee = New System.Windows.Forms.ProgressBar()
        Me.lblAOVersion = New System.Windows.Forms.Label()
        Me.btnExtract = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.lvExtractTypes = New System.Windows.Forms.ListView()
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ilExtract = New System.Windows.Forms.ImageList(Me.components)
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnExtractPath = New System.Windows.Forms.Button()
        Me.lblExtractPath = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbPri = New System.Windows.Forms.ComboBox()
        Me.chkDebugTrace = New System.Windows.Forms.CheckBox()
        Me.lblParseStatus = New System.Windows.Forms.Label()
        Me.btnParse = New System.Windows.Forms.Button()
        Me.pbParse = New System.Windows.Forms.ProgressBar()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.chkParsePluginOnly = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox11 = New System.Windows.Forms.GroupBox()
        Me.lblNewData = New System.Windows.Forms.Label()
        Me.GroupBox10 = New System.Windows.Forms.GroupBox()
        Me.lblOldData = New System.Windows.Forms.Label()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.lbParseNew = New System.Windows.Forms.ListBox()
        Me.btnParseFind = New System.Windows.Forms.Button()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.lbParseOld = New System.Windows.Forms.ListBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.btnOutputPath = New System.Windows.Forms.Button()
        Me.lblOutputPath = New System.Windows.Forms.Label()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.btnDebuggerReload = New System.Windows.Forms.Button()
        Me.wbDebug = New System.Windows.Forms.WebBrowser()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.btnUpdates = New System.Windows.Forms.Button()
        Me.rtfGPL = New System.Windows.Forms.RichTextBox()
        Me.lblLegal = New System.Windows.Forms.Label()
        Me.picGPL = New System.Windows.Forms.PictureBox()
        Me.picLogo = New System.Windows.Forms.PictureBox()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox11.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        CType(Me.picGPL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(472, 357)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox2)
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(464, 331)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Settings"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lvPlugins)
        Me.GroupBox2.Controls.Add(Me.btnFindPlugins)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 59)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(448, 264)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Plugins"
        '
        'lvPlugins
        '
        Me.lvPlugins.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader5, Me.ColumnHeader6, Me.ColumnHeader7})
        Me.lvPlugins.FullRowSelect = True
        Me.lvPlugins.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvPlugins.HideSelection = False
        Me.lvPlugins.Location = New System.Drawing.Point(6, 19)
        Me.lvPlugins.MultiSelect = False
        Me.lvPlugins.Name = "lvPlugins"
        Me.lvPlugins.Size = New System.Drawing.Size(436, 209)
        Me.lvPlugins.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lvPlugins.TabIndex = 2
        Me.lvPlugins.UseCompatibleStateImageBehavior = False
        Me.lvPlugins.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Plugin Base"
        Me.ColumnHeader5.Width = 175
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Plugin Name"
        Me.ColumnHeader6.Width = 175
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Plugin Version"
        Me.ColumnHeader7.Width = 80
        '
        'btnFindPlugins
        '
        Me.btnFindPlugins.Location = New System.Drawing.Point(342, 234)
        Me.btnFindPlugins.Name = "btnFindPlugins"
        Me.btnFindPlugins.Size = New System.Drawing.Size(100, 24)
        Me.btnFindPlugins.TabIndex = 1
        Me.btnFindPlugins.Text = "Find Plugins"
        Me.btnFindPlugins.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnAOPath)
        Me.GroupBox1.Controls.Add(Me.lblAOPath)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(448, 47)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "AO Install Path"
        '
        'btnAOPath
        '
        Me.btnAOPath.Image = Global.XRDB4.My.Resources.Resources.OPEN
        Me.btnAOPath.Location = New System.Drawing.Point(422, 16)
        Me.btnAOPath.Name = "btnAOPath"
        Me.btnAOPath.Size = New System.Drawing.Size(20, 20)
        Me.btnAOPath.TabIndex = 1
        Me.btnAOPath.UseVisualStyleBackColor = True
        '
        'lblAOPath
        '
        Me.lblAOPath.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblAOPath.Location = New System.Drawing.Point(6, 16)
        Me.lblAOPath.Name = "lblAOPath"
        Me.lblAOPath.Size = New System.Drawing.Size(410, 20)
        Me.lblAOPath.TabIndex = 0
        Me.lblAOPath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.GroupBox5)
        Me.TabPage2.Controls.Add(Me.GroupBox4)
        Me.TabPage2.Controls.Add(Me.GroupBox3)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(464, 331)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Extract"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.btnCheckAOVersion)
        Me.GroupBox5.Controls.Add(Me.pbExtractMarqee)
        Me.GroupBox5.Controls.Add(Me.lblAOVersion)
        Me.GroupBox5.Controls.Add(Me.btnExtract)
        Me.GroupBox5.Location = New System.Drawing.Point(8, 275)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(448, 48)
        Me.GroupBox5.TabIndex = 5
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "AO Version"
        '
        'btnCheckAOVersion
        '
        Me.btnCheckAOVersion.Location = New System.Drawing.Point(122, 14)
        Me.btnCheckAOVersion.Name = "btnCheckAOVersion"
        Me.btnCheckAOVersion.Size = New System.Drawing.Size(60, 24)
        Me.btnCheckAOVersion.TabIndex = 1
        Me.btnCheckAOVersion.Text = "Check"
        Me.btnCheckAOVersion.UseVisualStyleBackColor = True
        '
        'pbExtractMarqee
        '
        Me.pbExtractMarqee.Location = New System.Drawing.Point(188, 14)
        Me.pbExtractMarqee.Name = "pbExtractMarqee"
        Me.pbExtractMarqee.Size = New System.Drawing.Size(188, 24)
        Me.pbExtractMarqee.TabIndex = 4
        '
        'lblAOVersion
        '
        Me.lblAOVersion.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblAOVersion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAOVersion.Location = New System.Drawing.Point(6, 16)
        Me.lblAOVersion.Name = "lblAOVersion"
        Me.lblAOVersion.Size = New System.Drawing.Size(110, 22)
        Me.lblAOVersion.TabIndex = 0
        Me.lblAOVersion.Text = "XX.XX.XX"
        Me.lblAOVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnExtract
        '
        Me.btnExtract.Enabled = False
        Me.btnExtract.Location = New System.Drawing.Point(382, 15)
        Me.btnExtract.Name = "btnExtract"
        Me.btnExtract.Size = New System.Drawing.Size(60, 24)
        Me.btnExtract.TabIndex = 3
        Me.btnExtract.Text = "Extract"
        Me.btnExtract.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.lvExtractTypes)
        Me.GroupBox4.Location = New System.Drawing.Point(8, 59)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(448, 210)
        Me.GroupBox4.TabIndex = 2
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Record Types"
        '
        'lvExtractTypes
        '
        Me.lvExtractTypes.CheckBoxes = True
        Me.lvExtractTypes.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader3, Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader4})
        Me.lvExtractTypes.FullRowSelect = True
        Me.lvExtractTypes.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvExtractTypes.HideSelection = False
        Me.lvExtractTypes.Location = New System.Drawing.Point(6, 19)
        Me.lvExtractTypes.Name = "lvExtractTypes"
        Me.lvExtractTypes.Size = New System.Drawing.Size(436, 185)
        Me.lvExtractTypes.SmallImageList = Me.ilExtract
        Me.lvExtractTypes.TabIndex = 0
        Me.lvExtractTypes.UseCompatibleStateImageBehavior = False
        Me.lvExtractTypes.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Enabled"
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "TypeName"
        Me.ColumnHeader1.Width = 150
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "RecordType"
        Me.ColumnHeader2.Width = 120
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "# Extracted"
        Me.ColumnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ColumnHeader4.Width = 100
        '
        'ilExtract
        '
        Me.ilExtract.ImageStream = CType(resources.GetObject("ilExtract.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ilExtract.TransparentColor = System.Drawing.Color.Transparent
        Me.ilExtract.Images.SetKeyName(0, "GreenCheck.gif")
        Me.ilExtract.Images.SetKeyName(1, "RedX.gif")
        Me.ilExtract.Images.SetKeyName(2, "ARW05RT.ICO")
        Me.ilExtract.Images.SetKeyName(3, "ARW03RT.ICO")
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnExtractPath)
        Me.GroupBox3.Controls.Add(Me.lblExtractPath)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 6)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(448, 47)
        Me.GroupBox3.TabIndex = 1
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Extract Path"
        '
        'btnExtractPath
        '
        Me.btnExtractPath.Image = Global.XRDB4.My.Resources.Resources.OPEN
        Me.btnExtractPath.Location = New System.Drawing.Point(422, 16)
        Me.btnExtractPath.Name = "btnExtractPath"
        Me.btnExtractPath.Size = New System.Drawing.Size(20, 20)
        Me.btnExtractPath.TabIndex = 1
        Me.btnExtractPath.UseVisualStyleBackColor = True
        '
        'lblExtractPath
        '
        Me.lblExtractPath.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblExtractPath.Location = New System.Drawing.Point(6, 16)
        Me.lblExtractPath.Name = "lblExtractPath"
        Me.lblExtractPath.Size = New System.Drawing.Size(410, 20)
        Me.lblExtractPath.TabIndex = 0
        Me.lblExtractPath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.Label1)
        Me.TabPage3.Controls.Add(Me.cmbPri)
        Me.TabPage3.Controls.Add(Me.chkDebugTrace)
        Me.TabPage3.Controls.Add(Me.lblParseStatus)
        Me.TabPage3.Controls.Add(Me.btnParse)
        Me.TabPage3.Controls.Add(Me.pbParse)
        Me.TabPage3.Controls.Add(Me.GroupBox7)
        Me.TabPage3.Controls.Add(Me.GroupBox6)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(464, 331)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Parse"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(282, 280)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(157, 13)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Thread Priority (USE CAUTION)"
        '
        'cmbPri
        '
        Me.cmbPri.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPri.FormattingEnabled = True
        Me.cmbPri.Items.AddRange(New Object() {"Highest", "AboveNormal", "Normal", "BelowNormal", "Lowest"})
        Me.cmbPri.Location = New System.Drawing.Point(285, 302)
        Me.cmbPri.Name = "cmbPri"
        Me.cmbPri.Size = New System.Drawing.Size(105, 21)
        Me.cmbPri.TabIndex = 8
        '
        'chkDebugTrace
        '
        Me.chkDebugTrace.AutoSize = True
        Me.chkDebugTrace.Location = New System.Drawing.Point(8, 279)
        Me.chkDebugTrace.Name = "chkDebugTrace"
        Me.chkDebugTrace.Size = New System.Drawing.Size(233, 17)
        Me.chkDebugTrace.TabIndex = 7
        Me.chkDebugTrace.Text = "Enable Debug Trace (VERY VERY SLOW!)"
        Me.chkDebugTrace.UseVisualStyleBackColor = True
        '
        'lblParseStatus
        '
        Me.lblParseStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblParseStatus.Location = New System.Drawing.Point(5, 299)
        Me.lblParseStatus.Name = "lblParseStatus"
        Me.lblParseStatus.Size = New System.Drawing.Size(168, 25)
        Me.lblParseStatus.TabIndex = 6
        Me.lblParseStatus.Text = "Ready"
        Me.lblParseStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnParse
        '
        Me.btnParse.Location = New System.Drawing.Point(396, 299)
        Me.btnParse.Name = "btnParse"
        Me.btnParse.Size = New System.Drawing.Size(60, 24)
        Me.btnParse.TabIndex = 5
        Me.btnParse.Text = "Parse"
        Me.btnParse.UseVisualStyleBackColor = True
        '
        'pbParse
        '
        Me.pbParse.Location = New System.Drawing.Point(179, 300)
        Me.pbParse.Name = "pbParse"
        Me.pbParse.Size = New System.Drawing.Size(100, 24)
        Me.pbParse.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.pbParse.TabIndex = 4
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.chkParsePluginOnly)
        Me.GroupBox7.Controls.Add(Me.Label3)
        Me.GroupBox7.Controls.Add(Me.GroupBox11)
        Me.GroupBox7.Controls.Add(Me.GroupBox10)
        Me.GroupBox7.Controls.Add(Me.GroupBox9)
        Me.GroupBox7.Controls.Add(Me.btnParseFind)
        Me.GroupBox7.Controls.Add(Me.GroupBox8)
        Me.GroupBox7.Location = New System.Drawing.Point(8, 59)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(448, 218)
        Me.GroupBox7.TabIndex = 3
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Data Files"
        '
        'chkParsePluginOnly
        '
        Me.chkParsePluginOnly.AutoSize = True
        Me.chkParsePluginOnly.Checked = True
        Me.chkParsePluginOnly.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkParsePluginOnly.Location = New System.Drawing.Point(218, 71)
        Me.chkParsePluginOnly.Name = "chkParsePluginOnly"
        Me.chkParsePluginOnly.Size = New System.Drawing.Size(191, 17)
        Me.chkParsePluginOnly.TabIndex = 7
        Me.chkParsePluginOnly.Text = "Show selected plugin datafiles only"
        Me.chkParsePluginOnly.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label3.Location = New System.Drawing.Point(218, 19)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(224, 46)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "HINT: The current 'Extract Path' on the 'Extract' tab is the folder that this too" & _
    "l will search for datafiles."
        '
        'GroupBox11
        '
        Me.GroupBox11.Controls.Add(Me.lblNewData)
        Me.GroupBox11.Location = New System.Drawing.Point(6, 171)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(436, 41)
        Me.GroupBox11.TabIndex = 5
        Me.GroupBox11.TabStop = False
        Me.GroupBox11.Text = "Newer"
        '
        'lblNewData
        '
        Me.lblNewData.AutoSize = True
        Me.lblNewData.Location = New System.Drawing.Point(6, 16)
        Me.lblNewData.Name = "lblNewData"
        Me.lblNewData.Size = New System.Drawing.Size(60, 13)
        Me.lblNewData.TabIndex = 0
        Me.lblNewData.Text = "<New File>"
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.lblOldData)
        Me.GroupBox10.Location = New System.Drawing.Point(6, 124)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(436, 41)
        Me.GroupBox10.TabIndex = 4
        Me.GroupBox10.TabStop = False
        Me.GroupBox10.Text = "Older"
        '
        'lblOldData
        '
        Me.lblOldData.AutoSize = True
        Me.lblOldData.Location = New System.Drawing.Point(6, 16)
        Me.lblOldData.Name = "lblOldData"
        Me.lblOldData.Size = New System.Drawing.Size(54, 13)
        Me.lblOldData.TabIndex = 0
        Me.lblOldData.Text = "<Old File>"
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.lbParseNew)
        Me.GroupBox9.Location = New System.Drawing.Point(112, 19)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(100, 99)
        Me.GroupBox9.TabIndex = 3
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "Newer"
        '
        'lbParseNew
        '
        Me.lbParseNew.FormattingEnabled = True
        Me.lbParseNew.Items.AddRange(New Object() {"NONE"})
        Me.lbParseNew.Location = New System.Drawing.Point(6, 19)
        Me.lbParseNew.Name = "lbParseNew"
        Me.lbParseNew.Size = New System.Drawing.Size(88, 69)
        Me.lbParseNew.Sorted = True
        Me.lbParseNew.TabIndex = 0
        '
        'btnParseFind
        '
        Me.btnParseFind.Location = New System.Drawing.Point(218, 94)
        Me.btnParseFind.Name = "btnParseFind"
        Me.btnParseFind.Size = New System.Drawing.Size(224, 24)
        Me.btnParseFind.TabIndex = 2
        Me.btnParseFind.Text = "Find Data Files"
        Me.btnParseFind.UseVisualStyleBackColor = True
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.lbParseOld)
        Me.GroupBox8.Location = New System.Drawing.Point(6, 19)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(100, 99)
        Me.GroupBox8.TabIndex = 1
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Older"
        '
        'lbParseOld
        '
        Me.lbParseOld.FormattingEnabled = True
        Me.lbParseOld.Items.AddRange(New Object() {"SKIP"})
        Me.lbParseOld.Location = New System.Drawing.Point(6, 19)
        Me.lbParseOld.Name = "lbParseOld"
        Me.lbParseOld.Size = New System.Drawing.Size(88, 69)
        Me.lbParseOld.Sorted = True
        Me.lbParseOld.TabIndex = 0
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.btnOutputPath)
        Me.GroupBox6.Controls.Add(Me.lblOutputPath)
        Me.GroupBox6.Location = New System.Drawing.Point(8, 6)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(448, 47)
        Me.GroupBox6.TabIndex = 2
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Output Path"
        '
        'btnOutputPath
        '
        Me.btnOutputPath.Image = Global.XRDB4.My.Resources.Resources.OPEN
        Me.btnOutputPath.Location = New System.Drawing.Point(422, 16)
        Me.btnOutputPath.Name = "btnOutputPath"
        Me.btnOutputPath.Size = New System.Drawing.Size(20, 20)
        Me.btnOutputPath.TabIndex = 1
        Me.btnOutputPath.UseVisualStyleBackColor = True
        '
        'lblOutputPath
        '
        Me.lblOutputPath.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblOutputPath.Location = New System.Drawing.Point(6, 16)
        Me.lblOutputPath.Name = "lblOutputPath"
        Me.lblOutputPath.Size = New System.Drawing.Size(410, 20)
        Me.lblOutputPath.TabIndex = 0
        Me.lblOutputPath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.btnDebuggerReload)
        Me.TabPage4.Controls.Add(Me.wbDebug)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(464, 331)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Debug"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'btnDebuggerReload
        '
        Me.btnDebuggerReload.Location = New System.Drawing.Point(0, 0)
        Me.btnDebuggerReload.Name = "btnDebuggerReload"
        Me.btnDebuggerReload.Size = New System.Drawing.Size(12, 12)
        Me.btnDebuggerReload.TabIndex = 1
        Me.btnDebuggerReload.UseVisualStyleBackColor = True
        '
        'wbDebug
        '
        Me.wbDebug.Dock = System.Windows.Forms.DockStyle.Fill
        Me.wbDebug.Location = New System.Drawing.Point(0, 0)
        Me.wbDebug.MinimumSize = New System.Drawing.Size(20, 20)
        Me.wbDebug.Name = "wbDebug"
        Me.wbDebug.ScrollBarsEnabled = False
        Me.wbDebug.Size = New System.Drawing.Size(464, 331)
        Me.wbDebug.TabIndex = 0
        Me.wbDebug.Url = New System.Uri("", System.UriKind.Relative)
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.btnUpdates)
        Me.TabPage5.Controls.Add(Me.rtfGPL)
        Me.TabPage5.Controls.Add(Me.lblLegal)
        Me.TabPage5.Controls.Add(Me.picGPL)
        Me.TabPage5.Controls.Add(Me.picLogo)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(464, 331)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "About"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'btnUpdates
        '
        Me.btnUpdates.Location = New System.Drawing.Point(346, 299)
        Me.btnUpdates.Name = "btnUpdates"
        Me.btnUpdates.Size = New System.Drawing.Size(110, 24)
        Me.btnUpdates.TabIndex = 5
        Me.btnUpdates.Text = "Check for Updates"
        Me.btnUpdates.UseVisualStyleBackColor = True
        '
        'rtfGPL
        '
        Me.rtfGPL.Location = New System.Drawing.Point(8, 73)
        Me.rtfGPL.Name = "rtfGPL"
        Me.rtfGPL.ReadOnly = True
        Me.rtfGPL.Size = New System.Drawing.Size(448, 220)
        Me.rtfGPL.TabIndex = 4
        Me.rtfGPL.Text = resources.GetString("rtfGPL.Text")
        '
        'lblLegal
        '
        Me.lblLegal.AutoSize = True
        Me.lblLegal.Location = New System.Drawing.Point(78, 3)
        Me.lblLegal.Name = "lblLegal"
        Me.lblLegal.Size = New System.Drawing.Size(244, 52)
        Me.lblLegal.TabIndex = 1
        Me.lblLegal.Text = "XRDB4 v{VER}" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Copyright (C) 2009-2013 Xyphos Productions" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "<TheGreatXyphos@gmail.c" & _
    "om>" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Public GPLv3 FREEWARE; Not for Sale or Trade." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'picGPL
        '
        Me.picGPL.Image = Global.XRDB4.My.Resources.Resources.gplv3_88x31
        Me.picGPL.Location = New System.Drawing.Point(368, 3)
        Me.picGPL.Name = "picGPL"
        Me.picGPL.Size = New System.Drawing.Size(88, 31)
        Me.picGPL.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picGPL.TabIndex = 3
        Me.picGPL.TabStop = False
        '
        'picLogo
        '
        Me.picLogo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picLogo.Image = Global.XRDB4.My.Resources.Resources.favicon
        Me.picLogo.Location = New System.Drawing.Point(8, 3)
        Me.picLogo.Name = "picLogo"
        Me.picLogo.Size = New System.Drawing.Size(64, 64)
        Me.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picLogo.TabIndex = 0
        Me.picLogo.TabStop = False
        '
        'frmXRDB4
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(472, 357)
        Me.Controls.Add(Me.TabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmXRDB4"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "XRDB4"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox11.ResumeLayout(False)
        Me.GroupBox11.PerformLayout()
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox10.PerformLayout()
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        CType(Me.picGPL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnAOPath As System.Windows.Forms.Button
    Friend WithEvents lblAOPath As System.Windows.Forms.Label
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents picLogo As System.Windows.Forms.PictureBox
    Friend WithEvents lblLegal As System.Windows.Forms.Label
    Friend WithEvents picGPL As System.Windows.Forms.PictureBox
    Friend WithEvents rtfGPL As System.Windows.Forms.RichTextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnFindPlugins As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btnExtractPath As System.Windows.Forms.Button
    Friend WithEvents lblExtractPath As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents lvExtractTypes As System.Windows.Forms.ListView
    Friend WithEvents ilExtract As System.Windows.Forms.ImageList
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents lblAOVersion As System.Windows.Forms.Label
    Friend WithEvents pbExtractMarqee As System.Windows.Forms.ProgressBar
    Friend WithEvents btnExtract As System.Windows.Forms.Button
    Friend WithEvents btnCheckAOVersion As System.Windows.Forms.Button
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents btnOutputPath As System.Windows.Forms.Button
    Friend WithEvents lblOutputPath As System.Windows.Forms.Label
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents btnParse As System.Windows.Forms.Button
    Friend WithEvents pbParse As System.Windows.Forms.ProgressBar
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents lbParseNew As System.Windows.Forms.ListBox
    Friend WithEvents btnParseFind As System.Windows.Forms.Button
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents lbParseOld As System.Windows.Forms.ListBox
    Friend WithEvents lblParseStatus As System.Windows.Forms.Label
    Friend WithEvents wbDebug As System.Windows.Forms.WebBrowser
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox11 As System.Windows.Forms.GroupBox
    Friend WithEvents lblNewData As System.Windows.Forms.Label
    Friend WithEvents GroupBox10 As System.Windows.Forms.GroupBox
    Friend WithEvents lblOldData As System.Windows.Forms.Label
    Friend WithEvents chkDebugTrace As System.Windows.Forms.CheckBox
    Friend WithEvents cmbPri As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkParsePluginOnly As System.Windows.Forms.CheckBox
    Friend WithEvents btnDebuggerReload As System.Windows.Forms.Button
    Friend WithEvents lvPlugins As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnUpdates As System.Windows.Forms.Button

End Class
