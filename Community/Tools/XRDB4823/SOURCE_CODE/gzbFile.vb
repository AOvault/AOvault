﻿' gzbFile.vb - A generic class library to add some BinaryReader and 
'              BinaryWriter functionality to GZipStreams
' Programmer: William "Xyphos" Scott <TheGreatXyphos@gmail.com>
' Date: Aug 21, 2009
'
'This file is part of XRDB4.
'
'    XRDB4 is free software: you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation, either version 3 of the License, or
'    (at your option) any later version.
'
'    XRDB4 is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'
'    You should have received a copy of the GNU General Public License
'    along with XRDB4.  If not, see <http://www.gnu.org/licenses/>.

Imports System.IO
Imports System.IO.Compression
Imports System.Text.Encoding

Public NotInheritable Class gzbFile

    Private FS As FileStream
    Private BR As BinaryReader
    Private BW As BinaryWriter
    Private GZ As GZipStream

    Public Structure FileXRDB4Header
        Public Marker As Int32
        Public IDTag As Int32
        Public Version As Int32
        Public FileVer As Int32
    End Structure

    Public Structure FileXRDB4Record
        Public MarkerTag As Int32
        Public RecordType As Int32
        Public RecordNum As Int32
        Public RecordLen As Int32
        Public RecordCRC As UInt64
        Public RecordData As Byte()
    End Structure

    Public Sub New(ByVal FileName As String, ByVal FileMode As FileMode, ByVal CompressionMode As CompressionMode)
        Me.FS = New FileStream(FileName, FileMode, FileAccess.ReadWrite, FileShare.Read)
        Me.BR = New BinaryReader(FS)
        Me.BW = New BinaryWriter(FS)
        Me.GZ = New GZipStream(FS, CompressionMode)
    End Sub

    Public Sub Close()
        Me.GZ.Close()
        Me.GZ.Dispose()
        Me.BR.Close()
        Me.BW.Close()
        Me.FS.Close()
        Me.FS.Dispose()
        MyBase.Finalize()
    End Sub

#Region "Write"
    Public Overloads Sub Write(ByVal Buffer As Byte(), Optional ByVal Offset As Integer = 0, Optional ByVal Count As Integer = -1)
        GZ.Write(Buffer, Offset, IIf(Count = -1, Buffer.Length - Offset, Count))
    End Sub
    Public Overloads Sub Write(ByVal Value As Int32)
        Dim Q As Byte() = BitConverter.GetBytes(Value)
        GZ.Write(Q, 0, Q.Length)
    End Sub
    Public Overloads Sub Write(ByVal Value As UInt64)
        Dim Q As Byte() = BitConverter.GetBytes(Value)
        GZ.Write(Q, 0, Q.Length)
    End Sub

    Public Sub WriteHeader()
        Dim fXRDB4 As New FileXRDB4Header
        With fXRDB4
            .Marker = DEADC0DE
            .IDTag = XRDB
            .Version = VerNum
            .FileVer = pvInt(CurrentPatch)
            Me.Write(.Marker)
            Me.Write(.IDTag)
            Me.Write(.Version)
            Me.Write(.FileVer)
        End With
    End Sub

    Public Sub WriteRecord(ByVal fXRDB4 As FileXRDB4Record)
        With fXRDB4
            .MarkerTag = DEADC0DE
            Me.Write(.MarkerTag)
            Me.Write(.RecordType)
            Me.Write(.RecordNum)
            Me.Write(.RecordLen)
            Me.Write(.RecordCRC)
            Me.Write(.RecordData)
        End With
    End Sub
#End Region

#Region "Read"
    Public Function Read(ByVal Count As Integer) As Byte()
        Dim Q(Count) As Byte
        GZ.Read(Q, 0, Count)
        Return Q
    End Function
    Public Function ReadInt32() As Int32
        Dim Q(3) As Byte
        GZ.Read(Q, 0, 4)
        Return BitConverter.ToInt32(Q, 0)
    End Function
    Public Function ReadUInt64() As UInt64
        Dim Q(7) As Byte
        GZ.Read(Q, 0, 8)
        Return BitConverter.ToUInt64(Q, 0)
    End Function

    Public Function ReadHeader() As FileXRDB4Header
        Dim fXRDB4 As New FileXRDB4Header
        With fXRDB4
            .Marker = Me.ReadInt32
            .IDTag = Me.ReadInt32
            .Version = Me.ReadInt32
            .FileVer = Me.ReadInt32
        End With
        Return fXRDB4
    End Function

    Public Function ReadRecord() As FileXRDB4Record
        Dim fXRDB4 As New FileXRDB4Record
        With fXRDB4
            .MarkerTag = Me.ReadInt32
            .RecordType = Me.ReadInt32
            .RecordNum = Me.ReadInt32
            .RecordLen = Me.ReadInt32
            .RecordCRC = Me.ReadUInt64
            .RecordData = Me.Read(.RecordLen)
        End With
        Return fXRDB4
    End Function
#End Region

#Region "Underlays"
    Public ReadOnly Property FileStream() As FileStream
        Get
            Return Me.FS
        End Get
    End Property
    Public ReadOnly Property BinaryReader() As BinaryReader
        Get
            Return Me.BR
        End Get
    End Property
    Public ReadOnly Property BinaryWriter() As BinaryWriter
        Get
            Return Me.BW
        End Get
    End Property
    Public ReadOnly Property GZipStream() As GZipStream
        Get
            Return Me.GZ
        End Get
    End Property
    Public ReadOnly Property Position() As Integer
        Get
            Return Me.FS.Position
        End Get
    End Property
    Public ReadOnly Property Length() As Integer
        Get
            Return Me.FS.Length
        End Get
    End Property
#End Region
End Class
