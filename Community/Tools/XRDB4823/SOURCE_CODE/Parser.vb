﻿' Parser.vb - Custom Class Library to parse XRDB4's exported data files
' Programmer: William "Xyphos" Scott <TheGreatXyphos@gmail.com>
' Date: Aug 21, 2009
'
'This file is part of XRDB4.
'
'    XRDB4 is free software: you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation, either version 3 of the License, or
'    (at your option) any later version.
'
'    XRDB4 is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'
'    You should have received a copy of the GNU General Public License
'    along with XRDB4.  If not, see <http://www.gnu.org/licenses/>.

Imports System.Reflection
Imports System.Threading
Imports System.IO
Imports System.IO.Compression

Public NotInheritable Class Parser

    Private Const BlankOperator As Byte = 255 'As defined in lookups
    Private Const DefaultTarget As Byte = 255 'As defined in lookups

    Public Enum ParserReturns As Integer
        Parsing = -1
        Success = 0
        Aborted = 1
        Failed = 2  'debug should be enabled
        Crashed = 3
    End Enum

    Private Structure ReqsStruc
        Public AttrNum As Integer
        Public AttrVal As Integer
        Public AttrOp As Integer
    End Structure

    Private lblStat As Label
    Private pbProg As ProgressBar
    Private Shared cmbPrior As ComboBox

    Private Shared WithEvents Output As XRDB4_Extras.Plugin

    Private OldDF As String
    Private NewDF As String

    Private Checksums As New Hashtable

    Private Shared ReturnCode As ParserReturns
    'Private Shared RecordType As Integer
    'Private Shared RecordNum As Integer
    'Private Shared Buffer As Byte()
    Private Shared pThread As System.Threading.Thread = Nothing
    Private Shared cThread As System.Threading.Thread = Nothing

    Private Shared GZB As gzbFile

    Private BR As BufferedReader
    Private FunctionSets As New Hashtable
    Private Blacklist As New Hashtable
    Private AOID As Integer

    Private Tracing As Boolean

    Private Shared myAbortMsg As String
    Private Shared SkipCompare As Boolean
    Private Shared XH As XRDB4.gzbFile.FileXRDB4Header

    Private Shared Pri As System.Threading.ThreadPriority

    Public ReadOnly Property AbortMsg() As String
        Get
            Return myAbortMsg
        End Get
    End Property

    Public Sub Close(ByVal Msg As String)
        If Not GZB Is Nothing Then GZB.Close()
        Abort(Msg)
    End Sub

    Private Shared Sub cPriority(ByVal value As System.Threading.ThreadPriority) Handles Output.ChangePriority
        value = Math.Min(value, System.Threading.ThreadPriority.Highest)
        value = Math.Max(value, System.Threading.ThreadPriority.Lowest)

        Pri = value
        'MsgBox(value)

        'twin try's depending on which thread is active
        Try
            pThread.Priority = value
        Catch ex As Exception
        End Try

        Try
            cThread.Priority = value
        Catch ex As Exception
        End Try
    End Sub

    Public Sub ForcePriority(ByVal P As System.Threading.ThreadPriority)
        cPriority(P)
    End Sub


    Public Sub New(ByVal lblStatus As Label, _
                   ByVal pbProgress As ProgressBar, _
                   ByVal cmpPri As ComboBox, _
                   ByVal EnableTracing As Boolean)

        Output = Plugin

        OldDF = OldData  ' DataFiles(OldData)
        NewDF = NewData ' DataFiles(NewData)
        lblStat = lblStatus
        pbProg = pbProgress
        cmbPrior = cmpPri
        Tracing = EnableTracing
    End Sub

    Public Sub ForceAbort(ByVal Msg As String)
        myAbortMsg = Msg
        ReturnCode = ParserReturns.Aborted
        If Not GZB Is Nothing Then GZB.Close()
        'If Not (pThread Is Nothing) Then pThread.Abort()
    End Sub

    Private Shared Sub Abort(ByVal Msg As String) Handles Output.Abort ', Optional ByVal RC As ParserReturns = ParserReturns.Aborted) Handles Output.Abort
        Try
            myAbortMsg = Msg
            Output.Parse_End(True)
        Catch e As SqlTypes.SqlTypeException
            ReturnCode = ParserReturns.Crashed
        Catch ex As Exception
            Dim CE As New XRDB4_Extras.CustomException(ex.ToString, "Plugin Error")
            ReturnCode = ParserReturns.Crashed
        End Try
        ReturnCode = ParserReturns.Aborted
        If Not (pThread Is Nothing) Then pThread.Abort()
    End Sub

    Private Sub PreParse() 'ByVal SkipCompare As Boolean, ByVal XH As gzbFile.FileXRDB4Header)
        Try
            Output.Parse_Begin(ParsePath, pvStr(XH.FileVer), SkipCompare, PCMD)
        Catch e As SQLite.SQLiteException
            ReturnCode = ParserReturns.Crashed
        Catch ex As Exception
            Dim CE As New XRDB4_Extras.CustomException(ex.ToString, "Plugin Error")
            ReturnCode = ParserReturns.Crashed
        End Try
    End Sub

    Private Sub PostParse()
        Try
            Output.Parse_End(False)
            'ReturnCode = ParserReturns.Success
        Catch e As SQLite.SQLiteException
            ReturnCode = ParserReturns.Crashed
        Catch ex As Exception
            Dim CE As New XRDB4_Extras.CustomException(ex.ToString, "Plugin Error")
            ReturnCode = ParserReturns.Crashed
        End Try
    End Sub

    Private Function CleanReturn() As Integer
        Try
            If (Not GZB Is Nothing) Then GZB.Close()
            GZB = Nothing
        Catch e As SQLite.SQLiteException
            ReturnCode = ParserReturns.Crashed
        Catch ex As Exception
        End Try
        Return ReturnCode
    End Function

    Public Function Parse() As ParserReturns
        SkipCompare = CBool(OldDF = "") 'Is Nothing)
        ReturnCode = ParserReturns.Parsing
        TotalParsed = 0

        If SkipCompare And (Not JobParse) Then
            lblStat.Text = "Full Parse?"

            If MsgBox(String.Format("You're about to perform a full parse.{0}" & _
                                    "This operation could take a long time.{0}" & _
                                    "Are you sure you want to do this?", vbNewLine), _
                                MsgBoxStyle.Exclamation Or MsgBoxStyle.YesNo) _
                                = MsgBoxResult.No Then Return ParserReturns.Aborted
        Else
            If Not SkipCompare Then GetCRCs()
            If ReturnCode <> ParserReturns.Parsing Then Return CleanReturn()
        End If

        GZB = New gzbFile(NewDF, FileMode.Open, CompressionMode.Decompress)
        XH = GZB.ReadHeader()

        lblStat.Text = "Initializing Plugin..."
        DoEvents()

        'Init routine is threaded
        cThread = New Thread(AddressOf PreParse)
        cThread.IsBackground = False
        cThread.Priority = Pri
        cThread.Start()

        Dim oPri As Integer = Pri
        While cThread.IsAlive
            If Pri <> oPri Then
                oPri = Pri
                cmbPrior.SelectedIndex = System.Threading.ThreadPriority.Highest - Pri
            End If
            DoEvents()
        End While
        If ReturnCode <> ParserReturns.Parsing Then Return CleanReturn()


        'load it up
        LoadFunctionSets(XH.FileVer)

        lblStat.Text = "Parsing..."
        DoEvents()

        'lets do it
        pThread = New Thread(AddressOf DoParse)
        pThread.IsBackground = False
        pThread.Priority = Pri ' ThreadPriority.Normal
        pThread.Start() 'pbProg)

        'let it run threaded so we can poll for results and progress
        While (ReturnCode = ParserReturns.Parsing) ' AndAlso (pThread.IsAlive)
            pbProg.Value = CInt((GZB.FileStream.Position / GZB.FileStream.Length) * 100)
            pbProg.Refresh()
            If Pri <> oPri Then
                oPri = Pri
                cmbPrior.SelectedIndex = System.Threading.ThreadPriority.Highest - Pri
            End If
            DoEvents()
        End While

        Dim CR As Integer = CleanReturn()
        If (ReturnCode <> ParserReturns.Success) Then Return CR

        lblStat.Text = "Finalizing..."

        'run this threaded too!
        cThread = New Thread(AddressOf PostParse)
        cThread.IsBackground = False
        cThread.Priority = Pri
        cThread.Start() 'pbProg)

        While cThread.IsAlive
            If Pri <> oPri Then
                oPri = Pri
                cmbPrior.SelectedIndex = System.Threading.ThreadPriority.Highest - Pri
            End If
            DoEvents()
        End While

        Return CleanReturn()
    End Function


    Private Sub LoadFunctionSets(ByVal FileVer As Integer)
        Dim sFV As String = pvStr(FileVer)
        Dim cfg As New XRDB4_Extras.cfgFile(AppPath() & "Config\FunctionSets.cfg")

        'sort them out first and filter invalid sections
        Dim SL As New SortedList
        Dim RX As New System.Text.RegularExpressions.Regex("^\d{2}\.\d{2}\.\d{2}\.\d{2}$")
        For Each Sect As XRDB4_Extras.cfgFile.cfgSection In cfg.Sections
            If RX.Match(Sect.Name).Success Then SL.Add(Sect.Name, Sect)
        Next

        'now handle it as needed
        For Each DE As DictionaryEntry In SL
            If sFV < DE.Key Then Continue For
            Dim Sect As XRDB4_Extras.cfgFile.cfgSection = DE.Value
            For Each Key As XRDB4_Extras.cfgFile.cfgKey In Sect.Keys
                If FunctionSets.ContainsKey(Key.Name) Then
                    FunctionSets(Key.Name) = Key.Value
                Else
                    FunctionSets.Add(Key.Name, Key.Value)
                End If
            Next
        Next

        'done.
    End Sub

    Private Sub GetCRCs()

        lblStat.Text = "Reading Checksums..."
        GZB = New gzbFile(OldDF, FileMode.Open, CompressionMode.Decompress)
        GZB.ReadHeader()


        While ReturnCode = ParserReturns.Parsing
            Dim XR As gzbFile.FileXRDB4Record = GZB.ReadRecord
            If XR.MarkerTag <> DEADC0DE Then
                Dim CE As New XRDB4_Extras.CustomException(String.Format("{0}{1}{0}{2}is corrupt!", Chr(34), GZB.FileStream.Name, vbNewLine), "Corrupted data file detected")
                ReturnCode = ParserReturns.Failed
                Exit Sub
            End If
            If XR.RecordType = 0 Then Exit While
            Dim Key As String = CStr(XR.RecordType) & ":" & CStr(XR.RecordNum)
            Checksums.Add(Key, XR.RecordCRC)
            pbProg.Value = CInt((GZB.FileStream.Position / GZB.FileStream.Length) * 100)
            DoEvents()
        End While
        GZB.Close()
        GZB = Nothing

    End Sub

    Private Sub DoParse()

        Do While ReturnCode = ParserReturns.Parsing
            Dim XR As gzbFile.FileXRDB4Record = GZB.ReadRecord
            If XR.MarkerTag <> DEADC0DE Then
                Dim CE As New XRDB4_Extras.CustomException(String.Format("{0}{1}{0}{2}is corrupt!", Chr(34), GZB.FileStream.Name, vbNewLine), "Corrupted data file detected")
                Abort("Corrupt Datafile")
                ReturnCode = ParserReturns.Failed
                Exit Sub
            End If


            Dim Key As String = XR.RecordType & ":" & XR.RecordNum

            'blacklist
            If Blacklist.ContainsKey(Key) Then
                'TotalParsed += 1
                Continue Do
            End If

            Dim CS As XRDB4_Extras.Plugin.ChangeStates = _
                XRDB4_Extras.Plugin.ChangeStates.NewRecord

            If Checksums.ContainsKey(Key) Then
                If Checksums(Key) <> XR.RecordCRC Then
                    CS = XRDB4_Extras.Plugin.ChangeStates.ModifiedRecord
                Else
                    CS = XRDB4_Extras.Plugin.ChangeStates.NoChange
                End If
            End If

            System.Windows.Forms.Application.DoEvents()
            AOID = XR.RecordNum

            Select Case XR.RecordType
                Case 0  'Terminating Block
                    ReturnCode = ParserReturns.Success
                    Exit Do
                Case RDB_ITEM, RDB_NANO
                    ParseItemNano(XR, CS)
                Case Else
                    Try
                        If Not Output.OtherData_Begin(XR.RecordNum, XR.RecordType, CS) Then Continue Do
                        Output.OtherData(XR.RecordData)
                        Output.OtherData_End()
                    Catch e As SQLite.SQLiteException
                        ReturnCode = ParserReturns.Crashed
                    Catch ex As Exception
                        Dim CE As New XRDB4_Extras.CustomException(ex.ToString, "Plugin Error")
                        ReturnCode = ParserReturns.Crashed
                    End Try
            End Select
            TotalParsed += 1

        Loop

    End Sub

    Private Function CritError(ByVal Msg As String) As Boolean
        If MsgBox(String.Format("{0}{1}" & _
                                 "[Retry = Skip record and continue]{1}" & _
                                 "[Cancel = Abort and debug]", _
                                 Msg, vbNewLine), _
                                  MsgBoxStyle.RetryCancel Or MsgBoxStyle.Critical) _
                                  = MsgBoxResult.Cancel Then
            BR.DebugDump(Msg, ParserReturns.Failed)
        End If
        Return False
    End Function


    Private Sub ParseItemNano(ByVal XR As gzbFile.FileXRDB4Record, ByVal CS As XRDB4_Extras.Plugin.ChangeStates)

        BR = New BufferedReader(XR, Tracing)

        Try
            If Not Output.ItemNano_Begin(XR.RecordNum, _
                                         (XR.RecordType = RDB_NANO), _
                                         CS) Then Exit Sub
        Catch e As SQLite.SQLiteException
            ReturnCode = ParserReturns.Crashed
        Catch ex As Exception
            Dim CE As New XRDB4_Extras.CustomException(ex.ToString, "Plugin Error")
            ReturnCode = ParserReturns.Crashed
        End Try

        'Attrs
        BR.SkipBytes(16, "Pre-Attr")
        Dim Count As Integer = BR.ReadCNum("AttrCount")

        Dim iInfo As New XRDB4_Extras.Plugin.ItemNanoInfo

        Dim iAttrs As New List(Of XRDB4_Extras.Plugin.ItemNanoKeyVal)
        Dim isTower As Boolean = False

        For I As Integer = 0 To Count - 1
            Dim KV As New XRDB4_Extras.Plugin.ItemNanoKeyVal
            KV.AttrKey = BR.ReadInt32("AttrKey" & CStr(I))
            KV.AttrVal = BR.ReadInt32("AttrVal" & CStr(I))

            iAttrs.Add(KV)

            Select Case KV.AttrKey
                Case &H36
                    iInfo.QL = KV.AttrVal
                Case &H4C
                    iInfo.EquipPage = KV.AttrVal
                Case &H58
                    iInfo.DefaultSlot = KV.AttrVal
                Case &H12A
                    iInfo.EquipSlots = KV.AttrVal
                Case &H184
                    isTower = True
            End Select

        Next
        BR.SkipBytes(8, "Post-Attr")

        'ItemBody
        Dim NameLen As Int16 = BR.ReadInt16("NameLen")
        Dim DescLen As Int16 = BR.ReadInt16("DescLen")
        If (NameLen < 0) OrElse (DescLen < 0) OrElse _
            (NameLen > &HFFF&) OrElse (DescLen > &HFFF&) Then
            BR.DebugDump("NameLen or DescLen is invalid", ParserReturns.Failed)
        End If

        'Dim iName As String = ""
        If NameLen > 0 Then iInfo.Name = BR.ReadString(NameLen, "Name") Else iInfo.Name = ""

        'Dim iDesc As String = ""
        If DescLen > 0 Then iInfo.Description = BR.ReadString(DescLen, "Description") Else iInfo.Description = ""


        '[76]	; Item Type
        '0 = "Misc"
        '1 = "Weapon"
        '2 = "Armor"
        '3 = "Implant"
        '4 = "NPC"
        '5 = "Spirit"

        '6 = "Utility"
        '7 = "Tower"

        Dim BM As New XRDB4_Extras.BitManipulation
        With iInfo
            .Type = .EquipPage 'defaulting

            'then override
            If (InStr(.Name, "_") <> 0) OrElse _
                    (InStr(UCase(.Name), "BOSS") <> 0) Then
                .Type = 4 'NPC
            ElseIf isTower Then
                .Type = 7 'tower
            ElseIf .EquipPage = 1 Then
                If BM.CheckBit(.EquipSlots, 6) OrElse _
                    BM.CheckBit(.EquipSlots, 8) Then
                    .Type = 1 'Weapon
                Else
                    .Type = 6 'Utility
                End If
            End If
        End With

        Try
            Output.ItemNano(iInfo, iAttrs.ToArray)
        Catch e As SQLite.SQLiteException
            ReturnCode = ParserReturns.Crashed
        Catch ex As Exception
            Dim CE As New XRDB4_Extras.CustomException(ex.ToString, "Plugin Error")
            ReturnCode = ParserReturns.Crashed
        End Try

        'parse sets
        Dim R As Boolean = True
        While (BR.Ptr < (BR.Buffer.Length - &H8)) AndAlso (R = True)
            Dim KeyNum As Integer = BR.ReadInt32("ParseSetsKeyNum")
            Select Case KeyNum
                Case 0
                    'BR.SkipBytes(1, "StringTerminator")
                    Exit While
                   
                Case 2
                    ParseFunctionSet(R)
                Case 4
                    ParseAtkDefSet(R)
                Case 6 ' skipped crap?
                    BR.SkipBytes(4, "Pre-SkipSet")
                    Dim I As Integer = BR.ReadCNum("SkipSet") * 8 ' CNum x (2xInt)
                    BR.SkipBytes(I, "Post-SkipSet")
                Case 14 'animset
                    ParseAnimSoundSet(1, R)
                Case 20 ' soundset
                    ParseAnimSoundSet(2, R)
                Case &H16 'actionset
                    ParseActionSet(R)
                Case &H17 'shophash
                    ParseShopHash(R)
                Case Else
                    R = CritError("Invalid KeyNum " & KeyNum)
            End Select
        End While

        Try
            Output.ItemNano_End()
        Catch e As SQLite.SQLiteException
            ReturnCode = ParserReturns.Crashed
        Catch ex As Exception
            Dim CE As New XRDB4_Extras.CustomException(ex.ToString, "Plugin Error")
            ReturnCode = ParserReturns.Crashed
        End Try

    End Sub

    Private Sub ParseShopHash(ByRef R As Boolean)

        Dim SH As New List(Of XRDB4_Extras.Plugin.ItemNanoFunction)
        Dim EventNum As Integer = BR.ReadInt32("EventNum")
        Dim NumFuncs As Integer = BR.ReadCNum("NumFuncs")

        For I As Integer = 1 To NumFuncs
            Dim strValue As String = BR.ReadString(4, "StrArg")
            Dim numA As Integer = BR.ReadByte("numA")
            Dim numB As Integer = BR.ReadByte("numB")
            If (numA = 0) AndAlso (numB = 0) Then
                numA = BR.ReadInt16("numA2")
                numB = BR.ReadInt16("numB2")
            End If
            'v4.8.0  dynamic skip length
            Dim J As Integer = Math.Min(11, (BR.Buffer.Length) - BR.Ptr)
            BR.SkipBytes(J, "ShopHashSkip")
            Dim FN As New XRDB4_Extras.Plugin.ItemNanoFunction
            FN.FunctionArgs = New String() {strValue, numA, numB}
            FN.FunctionReqs = New XRDB4_Extras.Plugin.ItemNanoRequirement() {}
            FN.Target = DefaultTarget
            FN.TickCount = 1
            FN.TickInterval = 0
            SH.Add(FN)
        Next

        Try
            Output.ItemNanoEventAndFunctions(EventNum, SH.ToArray)
        Catch e As SQLite.SQLiteException
            ReturnCode = ParserReturns.Crashed
        Catch ex As Exception
            Dim CE As New XRDB4_Extras.CustomException(ex.ToString, "Plugin Error")
            ReturnCode = ParserReturns.Crashed
            R = False
        End Try
    End Sub


    Private Sub ParseActionSet(ByRef R As Boolean)

        If BR.ReadInt32("&H24 Check") <> &H24 Then
            Throw New Exception("Why am I here?")
        End If

        For I As Integer = 1 To BR.ReadCNum("MaxSets")
            Dim actionNum As Integer = BR.ReadInt32("actionNum")
            Dim NumReqs As Integer = BR.ReadCNum("NumReqs")
            Dim SF() As XRDB4_Extras.Plugin.ItemNanoRequirement = {}

            If NumReqs > 0 Then
                Dim RawReqs As New List(Of ReqsStruc)
                For J As Integer = 0 To NumReqs - 1
                    Dim RR As New ReqsStruc
                    RR.AttrNum = BR.ReadInt32("RawReqsNum")
                    RR.AttrVal = BR.ReadInt32("RawReqsVal")
                    RR.AttrOp = BR.ReadInt32("RawReqsOp")
                    RawReqs.Add(RR)
                Next
                SF = ParseReqs(RawReqs.ToArray)
            End If

            Try
                Output.ItemNanoAction(actionNum, SF)
            Catch e As SQLite.SQLiteException
                ReturnCode = ParserReturns.Crashed
            Catch ex As Exception
                Dim CE As New XRDB4_Extras.CustomException(ex.ToString, "Plugin Error")
                ReturnCode = ParserReturns.Crashed
                R = False
            End Try
        Next
    End Sub

    Private Sub ParseAnimSoundSet(ByVal TypeN As Integer, ByRef R As Boolean)
        Dim SetType As Integer = BR.ReadInt32("SetType")
        Dim NumFunc As Integer = BR.ReadCNum("NumFunc")

        For I As Integer = 1 To NumFunc
            Dim animList As New List(Of Integer)
            Dim actionNum As Integer = BR.ReadInt32("actionNum")
            Dim maxSets As Integer = BR.ReadCNum("maxSets")
            For J As Integer = 1 To maxSets
                Dim animNum As Integer = BR.ReadInt32("animNum" & J)
                animList.Add(animNum)
            Next

            Try
                Select Case TypeN
                    Case 1
                        Output.ItemNanoAnimSets(actionNum, animList.ToArray)
                    Case 2
                        Output.ItemNanoSoundSets(actionNum, animList.ToArray)
                    Case Else
                        Throw New Exception("Xyphos, you're an idiot!")
                End Select
            Catch e As SQLite.SQLiteException
                ReturnCode = ParserReturns.Crashed
            Catch ex As Exception
                Dim CE As New XRDB4_Extras.CustomException(ex.ToString, "Plugin Error")
                ReturnCode = ParserReturns.Crashed
                R = False
            End Try
        Next
    End Sub


    Private Sub ParseFunctionSet(ByRef R As Boolean)
        Dim EventNum As Integer = BR.ReadInt32("EventNum")
        Dim NumFuncs As Integer = BR.ReadCNum("NumFuncs")
        Dim iFunc As New List(Of XRDB4_Extras.Plugin.ItemNanoFunction)
        For I As Integer = 0 To NumFuncs - 1
            Dim SF As New XRDB4_Extras.Plugin.ItemNanoFunction
            SF.FunctionReqs = New XRDB4_Extras.Plugin.ItemNanoRequirement() {}
            SF.FunctionArgs = New String() {}

            SF.FunctionNum = BR.ReadInt32("FuncNum")
            BR.SkipBytes(8, "FuncHeaderPreSkip")
            Dim NumReqs As Integer = BR.ReadInt32("NumReqs")
            If NumReqs > 0 Then
                Dim RawReqs As New List(Of ReqsStruc)
                For J As Integer = 0 To NumReqs - 1
                    Dim RR As New ReqsStruc
                    RR.AttrNum = BR.ReadInt32("RawReqsNum")
                    RR.AttrVal = BR.ReadInt32("RawReqsVal")
                    RR.AttrOp = BR.ReadInt32("RawReqsOp")
                    RawReqs.Add(RR)
                Next
                SF.FunctionReqs = ParseReqs(RawReqs.ToArray)
            End If
            SF.TickCount = BR.ReadInt32("TickCount")
            SF.TickInterval = BR.ReadInt32("TickInterval")
            SF.Target = BR.ReadInt32("Target")
            BR.SkipBytes(4, "FuncHeaderPostSkip")
            SF.FunctionArgs = ParseArgs(SF.FunctionNum, R)
            If R = False Then Exit Sub
            iFunc.Add(SF)
        Next
        Try
            Output.ItemNanoEventAndFunctions(EventNum, iFunc.ToArray)
        Catch e As SQLite.SQLiteException
            ReturnCode = ParserReturns.Crashed
        Catch ex As Exception
            Dim CE As New XRDB4_Extras.CustomException(ex.ToString, "Plugin Error")
            ReturnCode = ParserReturns.Crashed
            R = False
        End Try
    End Sub

    'here, we "multiplex?" 3 req values into 5 values that are more useable
    Private Function ParseReqs(ByVal RawReqs() As ReqsStruc) As XRDB4_Extras.Plugin.ItemNanoRequirement()
        Dim Reqs As Integer = UBound(RawReqs)
        Dim RetReqs As New List(Of XRDB4_Extras.Plugin.ItemNanoRequirement)

        For I As Integer = 0 To Reqs
            Dim RR As ReqsStruc = RawReqs(I)
            Dim IR As New XRDB4_Extras.Plugin.ItemNanoRequirement

            IR.Target = &H13
            IR.AttrNum = RR.AttrNum
            IR.MainOp = RR.AttrOp
            IR.AttrValue = RR.AttrVal
            IR.ChildOp = BlankOperator

            If ((I < Reqs) AndAlso ((((IR.MainOp = &H12) OrElse _
                                      (IR.MainOp = &H13)) OrElse _
                                      ((IR.MainOp = &H1A) OrElse _
                                       (IR.MainOp = &H1B))) OrElse _
                                       ((((IR.MainOp = &H1C) OrElse _
                                          (IR.MainOp = &H1D)) OrElse _
                                          ((IR.MainOp = 30) OrElse _
                                           (IR.MainOp = &H25))) OrElse _
                                           ((IR.MainOp = 100) OrElse _
                                            (IR.MainOp = 110))))) Then
                IR.Target = IR.MainOp
                I += 1
                RR = RawReqs(I)
                IR.AttrNum = RR.AttrNum
                IR.MainOp = RR.AttrOp
                IR.AttrValue = RR.AttrVal
            End If

            If Not ((I >= Reqs) OrElse (Reqs = 1)) Then
                Dim aNum As Integer = RawReqs(I + 1).AttrNum
                Dim aVal As Integer = RawReqs(I + 1).AttrVal
                Dim aOp As Integer = RawReqs(I + 1).AttrOp
                If ((((aOp = 3) OrElse (aOp = 4)) OrElse (aOp = &H2A)) AndAlso (aNum = 0)) Then
                    IR.ChildOp = aOp
                    I += 1
                End If
            End If

            RetReqs.Add(IR)
        Next

        'ok, now to fix the childop order; need to bump each value down 1 element
        Dim A As XRDB4_Extras.Plugin.ItemNanoRequirement
        Dim B As XRDB4_Extras.Plugin.ItemNanoRequirement
        Dim C As Integer = RetReqs.Count - 1
        Dim D As Integer = RetReqs.Count - 2

        For I As Integer = 0 To D
            A = RetReqs(I)
            B = RetReqs(I + 1)
            A.ChildOp = B.ChildOp
            RetReqs(I) = A
        Next
        A = RetReqs(C)
        A.ChildOp = BlankOperator
        RetReqs(C) = A


        Return RetReqs.ToArray  'and return our results
    End Function

    Private Function ParseArgs(ByVal FuncNum As String, ByRef R As Boolean) As String()
        If Not FunctionSets.ContainsKey(FuncNum) Then
            BR.DebugDump("Unhandled Function")

        End If
        Dim Args() As String = Split(FunctionSets(FuncNum), ",")
        Dim ArgList As New List(Of String)
        For Each Arg As String In Args
            Dim QTY As Integer = Val(Trim(Arg))
            Dim Proc As String = LCase(Strings.Right(Trim(Arg), 1))
            Select Case Proc
                Case "n"
                    For I As Integer = 1 To QTY
                        Dim V As Integer = BR.ReadInt32("n:V")
                        ArgList.Add(V)
                    Next
                Case "h"
                    For I As Integer = 1 To QTY
                        Dim V As String = BR.ReadHash("h:V")
                        ArgList.Add(V)
                    Next
                Case "s"
                    For I As Integer = 1 To QTY
                        Dim V As String = ""
                        Dim StrLen As Integer = BR.ReadInt32("StrLen") - 1
                        If StrLen > 0 Then V = BR.ReadString("s:V")
                        BR.SkipBytes(1, "StringTerminator")
                        ArgList.Add(V)
                    Next
                Case "x"
                    BR.SkipBytes(QTY, "x:Skip")
                Case Else
                    R = CritError("Function Misconfiguration: " & Proc)
                    'BR.DebugDump("Function Misconfiguration: " & Proc)
            End Select
        Next

        Return ArgList.ToArray
    End Function

    Private Sub ParseAtkDefSet(ByRef R As Boolean)
        Dim Atks As New List(Of XRDB4_Extras.Plugin.ItemNanoKeyVal)
        Dim Defs As New List(Of XRDB4_Extras.Plugin.ItemNanoKeyVal)

        BR.SkipBytes(4, "AtkDefSkip")
        Dim MaxSet As Integer = BR.ReadCNum("MaxSet")
        For I As Integer = 1 To MaxSet
            Dim Key As Integer = BR.ReadInt32("Key")
            Dim Sets As Integer = BR.ReadCNum("Sets")
            For J As Integer = 1 To Sets
                'Dim attr As New XRDB4_Extras.Plugin.ItemNanoAttribute
                Dim KV As New XRDB4_Extras.Plugin.ItemNanoKeyVal
                KV.AttrKey = BR.ReadInt32("AttrKey")
                KV.AttrVal = BR.ReadInt32("AttrVal")
GhettoFix:
                'Item:213413 in the first SL patch has invalid key,
                ' this is a ghettofix to handle it.
                Select Case Key
                    Case 3
                        If (XH.FileVer = 15000100) AndAlso _
                            (AOID = 213413) Then
                            Key = 13
                            GoTo GhettoFix
                        End If

                    Case 12
                        Atks.Add(KV)
                    Case 13
                        Defs.Add(KV)
                    Case Else
                        R = CritError("Unhandled AtkDef Set: " & Key)
                End Select
            Next
        Next

        Try
            Output.ItemNanoAttackAndDefense(Atks.ToArray, Defs.ToArray)
        Catch e As SQLite.SQLiteException
            ReturnCode = ParserReturns.Crashed
        Catch ex As Exception
            Dim CE As New XRDB4_Extras.CustomException(ex.ToString, "Plugin Error")
            ReturnCode = ParserReturns.Crashed
            R = False
        End Try
    End Sub



    Protected Friend NotInheritable Class BufferedReader
        Private Structure xTraceStruc
            Public Type As String
            Public Length As Integer
            Public Label As String
            Public Caller As String
            Public Value As String
        End Structure

        Private xTraceInfo As New ArrayList
        Public Ptr As Integer
        Public Buffer As Byte()
        Public RecordType As Integer
        Public RecordNum As Integer
        Public Tracing As Boolean

        'Public Sub New(ByVal bArray As Byte())
        Public Sub New(ByVal XR As gzbFile.FileXRDB4Record, ByVal EnableTracing As Boolean)
            RecordType = XR.RecordType
            RecordNum = XR.RecordNum
            Buffer = XR.RecordData 'xrbArray
            Tracing = EnableTracing
        End Sub

        Private Function AddTrace(ByVal Type As String, _
                             ByVal Length As Integer, _
                             ByVal Label As String) As Boolean

            If Not Tracing Then Return CBool((Ptr + Length) >= Buffer.Length)

            Dim XT As New xTraceStruc
            Dim ST As New StackTrace
            With XT
                .Type = Type
                .Length = Length
                .Label = Label
                .Caller = ST.GetFrame(2).GetMethod.Name
                .Value = "[NULL]"
                'Trace.WriteLine(.Caller)
            End With
            xTraceInfo.Add(XT)
            If (Ptr + Length) > Buffer.Length Then
                'over read
                DebugDump("Read past end")
                Return True
            End If
            Return False
        End Function


        Private Sub AddValue(ByVal Value As String)
            If Not Tracing Then Exit Sub
            Dim XT As xTraceStruc = xTraceInfo(xTraceInfo.Count - 1)
            XT.Value = Value
            xTraceInfo(xTraceInfo.Count - 1) = XT
        End Sub


        Public Sub DebugDump(ByVal Message As String, Optional ByVal RC As ParserReturns = ParserReturns.Failed)
            Dim Contents As String = My.Computer.FileSystem.ReadAllText(AppPath() & "XRDB4-DebugTrace.tpl")
            Contents = Replace(Contents, "{RECORDTYPE}", "0x" & String.Format("{0:X}", RecordType))
            Contents = Replace(Contents, "{RECORDNUM}", "0x" & String.Format("{0:X}", RecordNum))
            Contents = Replace(Contents, "{RECORDLEN}", "0x" & String.Format("{0:X}", Buffer.Length))
            Contents = Replace(Contents, "{MESSAGE}", Message)

            Dim JS As New System.Text.StringBuilder
            Dim HX As New System.Text.StringBuilder

            Dim xCol As Integer = 0
            Dim xRow As Integer = 0
            Dim xPtr As Integer = 0
            Dim GreenBlue As Boolean = False

            HX.Append(String.Format("<span class={0}hexrow{0}>000000</span>", Chr(34)))
            For I As Integer = 0 To xTraceInfo.Count - 1
                Dim XT As xTraceStruc = xTraceInfo(I)

                JS.Append(String.Format( _
                           "[{0}{1}{0},{0}{2}{0},{0}{3}{0},{0}{4}{0}],{5}", _
                           Chr(34), _
                           XT.Caller.Replace(Chr(34), "&quot;"), _
                           XT.Type.Replace(Chr(34), "&quot;"), _
                           XT.Label.Replace(Chr(34), "&quot;"), _
                           XT.Value.Replace(Chr(34), "&quot;").Replace(vbCr, "").Replace(vbLf, "<br />"), _
                           vbNewLine))

                HX.Append(String.Format( _
                           "<span class={0}{1}{0} onmouseover={0}msg({2});{0}>", _
                           Chr(34), IIf(GreenBlue, "green", "blue"), I))

                For J As Integer = 0 To XT.Length - 1
                    If xPtr < Buffer.Length Then
                        Dim xChar As String = Chr(Buffer(xPtr))
                        Select Case xChar
                            Case "!" To "~"
                            Case Else : xChar = "&nbsp;"
                        End Select

                        HX.Append(String.Format( _
                                   "<span class={0}col{0}>{1:X2}<br />{2}</span>", _
                                   Chr(34), Buffer(xPtr), xChar))
                    Else
                        HX.Append(String.Format( _
                            "<span class={0}hexheadcell{0}>XX<br />&oplus;</span>", _
                                   Chr(34)))
                        Exit For
                    End If
                    xPtr += 1
                    xCol += 1
                    If xCol = 16 Then
                        xRow += 16
                        HX.Append(String.Format( _
                                  "<br /><span class={0}hexrow{0}>{1:X6}</span>", _
                                  Chr(34), xRow))
                        xCol = 0
                    End If
                Next
                HX.Append("</span>")
                GreenBlue = Not GreenBlue
                If xPtr >= Buffer.Length Then Exit For
            Next

            'spit out the rest if needed.
            If xPtr < Buffer.Length Then

                HX.Append(String.Format( _
                          "<span class={0}red{0}>", _
                          Chr(34)))

                For I = xPtr To Buffer.Length - 1

                    Dim xChar As String = Chr(Buffer(xPtr))
                    Select Case xChar
                        Case "!" To "~"
                        Case Else : xChar = "&nbsp;"
                    End Select

                    HX.Append(String.Format( _
                               "<span class={0}col{0}>{1:X2}<br />{2}</span>", _
                               Chr(34), Buffer(xPtr), xChar))
                    xPtr += 1
                    xCol += 1
                    If xCol = 16 Then
                        xRow += 16

                        HX.Append(String.Format( _
                                  "<br /><span class={0}hexrow{0}>{1:X6}</span>", _
                                  Chr(34), xRow))
                        xCol = 0
                    End If
                Next
                HX.Append("</span>")
            End If

            Contents = Replace(Contents, "{JAVASCRIPT}", JS.ToString)
            Contents = Replace(Contents, "{HEXDUMP}", HX.ToString)

            My.Computer.FileSystem.WriteAllText(AppPath() & "XRDB4-DebugTrace.htm", Contents, False)
            'Abort(ParserReturns.Failed)
            ReturnCode = ParserReturns.Failed
        End Sub

        Public Function ReadInt32(ByVal Label As String) As Integer
            If AddTrace("integer", 4, Label) Then Return 0
            Dim Value As Integer = BitConverter.ToInt32(Buffer, Ptr)
            AddValue(Value)
            Ptr += 4
            Return Value
        End Function
        Public Function ReadCNum(ByVal Label As String) As Integer
            If AddTrace("CNum", 4, Label) Then Return 0
            Dim Value As Integer = BitConverter.ToInt32(Buffer, Ptr)
            Value = CLng(Math.Round(CDbl(((CDbl(Value) / 1009) - 1))))
            AddValue(Value)
            Ptr += 4
            Return Value
        End Function

        Public Function ReadInt16(ByVal Label As String) As Int16
            If AddTrace("Int16", 2, Label) Then Return 0
            Dim Value As Int16 = BitConverter.ToInt16(Buffer, Ptr)
            AddValue(Value)
            Ptr += 2
            Return Value
        End Function

        Public Function ReadByte(ByVal Label As String) As Byte
            If AddTrace("Byte", 1, Label) Then Return 0
            Dim Value As Byte = Buffer(Ptr)
            AddValue(Value)
            Ptr += 1
            Return Value
        End Function

        Public Function ReadHash(ByVal Label As String) As String
            If AddTrace("Hash", 4, Label) Then Return 0
            Dim Value(3) As Byte
            Array.Copy(Buffer, Ptr, Value, 0, 4)
            Array.Reverse(Value)
            Ptr += 4
            Return System.Text.Encoding.ASCII.GetString(Value)
        End Function
        Public Function ReadString(ByVal Label As String) As String
            'scan ahead and build string while checking for zbyte
            Dim SB As New System.Text.StringBuilder
            Dim I As Integer
            For I = Ptr To Buffer.Length - 1
                Dim B As Byte = Buffer(I)
                If B = 0 Then Exit For
                SB.Append(Chr(B))
            Next
            Dim Value As String = SB.ToString
            Dim Length As Integer = Value.Length
            If AddTrace(String.Format("String({0})", Length), Length, Label) Then Return ""
            Ptr += Length
            Return Value
        End Function
        Public Function ReadString(ByVal Length As Integer, ByVal Label As String) As String
            If AddTrace(String.Format("String({0})", Length), Length, Label) Then Return 0
            Dim Value As String = System.Text.Encoding.UTF8.GetString(Buffer, Ptr, Length)
            AddValue(Value)
            Ptr += Length
            Return Value
        End Function

        Public Sub SkipBytes(ByVal Count As Integer, ByVal Label As String)
            If AddTrace(String.Format("Skipped({0})", Count), Count, Label) Then Exit Sub
            Ptr += Count
        End Sub
    End Class

End Class
