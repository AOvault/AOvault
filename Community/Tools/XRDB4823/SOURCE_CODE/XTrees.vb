﻿' XTrees.vb - a new class library to replace the old CTrees.vb
' Programmer: William "Xyphos" Scott <TheGreatXyphos@gmail.com>
' Date: Oct 21, 2009
' Last Modified: Apr 9, 2010
'
' Special thanks goes to "Suicidal" for helping decode the IDX format
'
'This file is part of XRDB4.
'
'    XRDB4 is free software: you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation, either version 3 of the License, or
'    (at your option) any later version.
'
'    XRDB4 is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'
'    You should have received a copy of the GNU General Public License
'    along with XRDB4.  If not, see <http://www.gnu.org/licenses/>.

Imports Microsoft.VisualBasic.CompilerServices
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices
Imports System.IO
Imports System.IO.Compression
Imports System.Threading
Imports System.Security.Cryptography

Public Class XTrees

    Private GZB As gzbFile
    Private PB As ProgressBar

    'Private IDX As New Hashtable
    Private IDX As New Dictionary(Of String, List(Of Int32))
    Private DAT As New List(Of bFile)

    Private BlockOffset As Integer
    Private DataFileSize As Integer
    Private SizeFix As Integer = &HC


    Public Sub New(ByVal pbMarqee As ProgressBar)
        PB = pbMarqee
        PB.Style = ProgressBarStyle.Marquee

        'find and open datafiles
        Dim J As String = RDBPATH
        Dim RX As New System.Text.RegularExpressions.Regex("ResourceDatabase.dat")
        Dim DI As New DirectoryInfo(AOPath & "\cd_image\data\db")
        For Each FI As FileInfo In DI.GetFiles("ResourceDatabase.*")
            If RX.IsMatch(FI.FullName) Then DAT.Add(New bFile(FI.FullName))
        Next

        'open index file
        Dim BR As New bFile(AOPath & RDBPATH & ".idx")

        'get block offsets
        BR.Stream.BaseStream.Seek(&HC, SeekOrigin.Begin)
        BlockOffset = BR.Stream.ReadInt32

        'get datafile size
        BR.Stream.BaseStream.Seek(&HB8, SeekOrigin.Begin)
        DataFileSize = BR.Stream.ReadInt32

        'in the case of old format where this is -1  (0xFFFFFFFF)
        If DataFileSize = -1 Then
            DataFileSize = DAT(0).Stream.BaseStream.Length
        Else
            'SizeFix += 8
        End If

        'get position of first block
        BR.Stream.BaseStream.Seek(&H48, SeekOrigin.Begin)
        Dim FirstBlock As Integer = BR.Stream.ReadInt32
        'Dim fullist As New List(Of Integer) ' delete
        'goto first block
        BR.Stream.BaseStream.Seek(FirstBlock, SeekOrigin.Begin)
        Do
            'PB.Value = ProgressValue(FS.Position, FS.Length)
            DoEvents()
            Dim FowardLink As Integer = BR.Stream.ReadInt32 'address of next block
            If FowardLink = 0 Then Exit Do

            BR.Stream.ReadInt32() 'skip back link
            Dim ActiveEntry As Short = BR.Stream.ReadInt16 'num of entries in block
            BR.Stream.ReadBytes(8) 'skip unknown irrelavant crap

            For I As Integer = ActiveEntry To 1 Step -1

                Dim rOff As Integer = BR.Stream.ReadInt32


                Dim rType As String = SwapEndian(BR.Stream.ReadInt32)

                If rOff > 0 Then
                    BR.Stream.ReadInt32() 'skip this next one; not really needed

                    If IDX.ContainsKey(rType) Then
                        IDX(rType).Add(rOff)
                    Else
                        Dim oList As New List(Of Integer)
                        oList.Add(rOff)
                        IDX.Add(rType, oList)
                    End If
                End If
            Next

            BR.Stream.BaseStream.Seek(FowardLink, SeekOrigin.Begin) 'seek to next block
        Loop
        BR.Stream.Close()


        'create datafile
        Dim FName As String = String.Format("{0}\{1}_{2}.xrdb4", _
            ExtractPath, _
            LastPlugin.Replace(".", "-"), _
            CurrentPatch.Replace(".", "-"))


        GZB = New gzbFile(FName, FileMode.Create, CompressionMode.Compress)
        GZB.WriteHeader()

    End Sub

    Public Function Export(ByVal RecType As Integer, ByVal Decode As Boolean) As Integer
        Dim DM5 As New MD5CryptoServiceProvider

        'Dim num As Integer = 0
        Dim NumExported = 0
        Dim rType As String = RecType
        If Not IDX.ContainsKey(rType) Then Return 0 'no records of specified type, return zero
        Dim oList As List(Of Integer) = IDX(rType)
        ' Dim x As Integer = 0
        For Each rOff As Integer In oList
            DoEvents()

            'If (rOff + 22) Then

            Dim OffConv As ULong = Math.Abs(rOff)

            'read the record header
            'Dim Header() As Byte = SegRead(&H22, CUInt(rOff), True) 'DAT(DF).Stream.ReadBytes(&H22)

            Dim Header() As Byte = SegRead(&H22, OffConv, True) 'DAT(DF).Stream.ReadBytes(&H22)
            Dim RT As Integer = BitConverter.ToInt32(Header, &HA)
            Dim RN As Integer = BitConverter.ToInt32(Header, &HE)
            Dim RL As Integer = BitConverter.ToInt32(Header, &H12) - SizeFix  '&HC '16
            
            'If RL < 0 Then
            'MessageBox.Show(RL & rOff.ToString & " " & x.ToString & " " & RecType)
            'End If

            ' x = x + 1
            'read the record
            'Dim rData() As Byte = SegRead(Math.Abs(RL), False)
            Dim rData() As Byte = SegRead(RL, False)
            If Decode Then Decode_Item_Data(Header, rData)

            'calc the crc
            Dim MD As Byte() = DM5.ComputeHash(rData, 0, RL)
            Dim CRCa As UInt64 = BitConverter.ToUInt64(MD, 0)
            Dim CRCb As UInt64 = BitConverter.ToUInt64(MD, 8)
            Dim CRC As UInt64 = CRCa Xor CRCb

            GZB.Write(DEADC0DE)      'deadc0de marker
            GZB.Write(RT)            'Record Type
            GZB.Write(RN)            'Record Number
            GZB.Write(RL)            'Record Length
            GZB.Write(CRC)           'Checksum
            GZB.Write(rData, 0, RL) 'Record Data

            NumExported += 1

        Next

        Return NumExported
    End Function

    Public Sub Dispose()

        'final null record
        Dim Term As Int32 = 0
        Me.GZB.Write(DEADC0DE) 'deadcode marker
        Me.GZB.Write(Term) 'type
        Me.GZB.Write(Term) 'number
        Me.GZB.Write(Term) 'length
        Me.GZB.Write(Term) 'Checksum part 1 of int64
        Me.GZB.Write(Term) 'Checksum pert 2 of int64
        GZB.Close()

        For Each BF As bFile In DAT
            BF.Stream.Close()
        Next
        PB.Style = ProgressBarStyle.Blocks
    End Sub

    Private Function SwapEndian(ByVal lngIn As Integer) As Integer
        Dim bytes As Byte() = BitConverter.GetBytes(lngIn)
        If BitConverter.IsLittleEndian Then
            Array.Reverse(bytes)
        End If
        Return BitConverter.ToInt32(bytes, 0)
    End Function
    Private Function SwapEndian(ByVal lngIn As UInteger) As UInteger
        Dim bytes As Byte() = BitConverter.GetBytes(lngIn)
        If BitConverter.IsLittleEndian Then
            Array.Reverse(bytes)
        End If
        Return BitConverter.ToUInt32(bytes, 0)
    End Function

    Private Function SegRead(ByVal Count As Integer, Optional ByVal Offset As ULong = 0, Optional ByVal CalcDF As Boolean = False) As Byte()
        Static DF As Integer

        'If Offset <> -1 Then
        If CalcDF = True Then

            'calc which datafile the record is in
            DF = Math.Floor(Offset / DataFileSize)
            If DF > 0 Then Offset = (Offset - (DataFileSize * DF)) + BlockOffset


            DAT(DF).Stream.BaseStream.Seek(Offset, SeekOrigin.Begin)
        End If

        'calc how many bytes we can read from the file if not all
        'Dim Size As Integer = DAT(DF).Stream.BaseStream.Length
        'Dim Here As Integer = DAT(DF).Stream.BaseStream.Position
        Dim Size As ULong = DAT(DF).Stream.BaseStream.Length
        Dim Here As ULong = DAT(DF).Stream.BaseStream.Position
        If (Here + Count) <= Size Then Return DAT(DF).Stream.ReadBytes(Count)

        'else, we calc how many bytes to read then jump to the rest in the next DF
        Dim aHalf As Integer = Size - Here
        Dim Buffer1 As Byte() = DAT(DF).Stream.ReadBytes(aHalf)

        DAT(DF + 1).Stream.BaseStream.Seek(BlockOffset, SeekOrigin.Begin)

        Dim bHalf As Integer = Count - aHalf
        Dim Buffer2 As Byte() = DAT(DF + 1).Stream.ReadBytes(bHalf)

        Return ArrayMerge(Buffer1, Buffer2) 'Buffer3
    End Function

    Private Function ArrayMerge(ByVal Buffer1() As Byte, ByVal Buffer2() As Byte) As Byte()
        Dim Buffer3((Buffer1.Length + Buffer2.Length) - 1) As Byte
        Array.Copy(Buffer1, 0, Buffer3, 0, Buffer1.Length)
        Array.Copy(Buffer2, 0, Buffer3, Buffer1.Length, Buffer2.Length)
        Return Buffer3
    End Function


    'Decode routine from Auno's adbd tool; aodb.cc
    'Special thanks to Vhab and Neo-Vortex for translating 
    'some of the code into basic English for me to understand

    '// Decode data, required for items/nanos from version <= 13.91
    'static void
    'decode_item_data(unsigned char *datap, uint32_t len)
    '{
    '  uint32_t i;
    '  uint64_t seed;
    '
    '  seed   = *( (int*)(datap + 4) );
    '  datap += 0x18;
    '
    '  for (i=0x18; i<len; i++)
    '    {
    '      seed *= 0x1012003;
    '      seed %= 0x4e512dc8fULL;
    '      *datap++ ^= (uint8_t)(seed);
    '    }
    '}

    'There's a minor difference in the routine versions,
    'but works the same way
    Private Sub Decode_Item_Data(ByVal Header() As Byte, ByRef rData() As Byte)
        Dim N As Int32 = BitConverter.ToInt32(Header, &HE)
        Dim Seed As UInt64 = N

        For I As Int32 = 0 To rData.Length - 1
            Seed *= &H1012003
            Seed = Seed Mod &H4E512DC8FUL
            rData(I) = rData(I) Xor (Seed And 255)
        Next
        Dim Test As String = System.Text.Encoding.UTF8.GetString(rData)
    End Sub
End Class

Public Class bFile
    Private FS As FileStream
    Private BR As BinaryReader

    Public Sub New(ByVal FileName As String)
        FS = New FileStream(FileName, FileMode.Open, FileAccess.Read, FileShare.Read)
        BR = New BinaryReader(FS)
    End Sub

    Public ReadOnly Property Stream() As BinaryReader
        Get
            Return BR
        End Get
    End Property
End Class

