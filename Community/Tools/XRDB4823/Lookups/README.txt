XRDB4 Definition Files:

These .cfg files are in .ini format, are used and required 
by the XRDB4-Extras.Lookup tool.

Try to not edit these files unless you know what you are doing
but then again, nothing really serious will happen if you do.

They're here so they can be used and edited as needed 
without having to recompile a new version of XRDB4.
