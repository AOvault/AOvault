﻿// $Id: ParserInfo.cs 17 2011-05-28 02:09:29Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;

namespace Twp.Build
{
	public class ParserInfo
	{
		private string product;
		public string Product
		{
			get { return this.product; }
			set { this.product = value; }
		}

		private string description;
		public string Description
		{
			get { return this.description; }
			set { this.description = value; }
		}

		private string company;
		public string Company
		{
			get { return this.company; }
			set { this.company = value; }
		}

		private string copyright;
		public string Copyright
		{
			get { return this.copyright; }
			set { this.copyright = value; }
		}

		private string license;
		public string License
		{
			get { return this.license; }
			set { this.license = value; }
		}

		private Version version;
		public Version Version
		{
			get { return this.version; }
			set { this.version = value; }
		}
	}
}
