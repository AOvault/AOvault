﻿// $Id: Program.cs 19 2011-06-01 05:57:15Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.IO;

namespace Twp.Build
{
	class Program
	{
		static void Main( string[] args )
		{
			try
			{
				Arguments arguments = new Arguments( args );

				if( arguments["help"] != null || arguments["h"] != null || args.Length == 0 )
				{
					Console.WriteLine( "Twp.Build is a utility for pre- and post-build events." );
					Console.WriteLine();
					Console.WriteLine( "Available arguments:" );
					Console.WriteLine( "  -help (-h)" );
					Console.WriteLine( "    Optional. Displays this output" );
					Console.WriteLine();
					Console.WriteLine( "  -quiet (-q)" );
					Console.WriteLine( "    Optional. Suppress non-critical output" );
					Console.WriteLine();
					Console.WriteLine( "  -workingDirectory=\"...\"" );
					Console.WriteLine( "    Optional. Sets the working directory to start the installer script in" );
					Console.WriteLine();
					Console.WriteLine( "  -inScript=\"...\"" );
					Console.WriteLine( "    Mandatory for pre-build. The .in script that should be run" );
					Console.WriteLine();
					Console.WriteLine( "  -nsisScript=\"...\"" );
					Console.WriteLine( "    Mandatory for post-build. The .nsi script that should be run" );
					Console.WriteLine();
					Console.WriteLine( "  -nsisExecutable=\"...\"" );
					Console.WriteLine( "    Optional. Defines the NSIS executable that needs to be run (only works if nsisPath is also defined)" );
					Console.WriteLine();
					Console.WriteLine( "  -nsisPath=\"...\"" );
					Console.WriteLine( "    Optional. Defines the path NSIS executable resides in" );
					Environment.Exit( 0 );
				}

				// - workingDirectory
				string workingDirectory = Environment.CurrentDirectory;
				if( arguments["workingDirectory"] != null )
					workingDirectory = arguments["workingDirectory"];

				// IN parser
				if( arguments["inScript"] != null )
				{
					string inScript = arguments["inScript"];
					Parser parser = new Parser();
					if( arguments["quiet"] != null || arguments["q"] != null )
					{
						parser.Quiet = true;
					}
					bool success = parser.Run( workingDirectory, inScript );
					if( !success )
					{
						WriteLine( parser.ErrorMessage );
						Environment.Exit( 4 );
					}
				}
	
				// NSIS compiler
				if( arguments["nsisScript"] != null )
				{
#if DEBUG
					WriteLine( "NSIS is not available in Debug mode." );
					Environment.Exit( 0 );
#else
					string nsisScript = arguments["nsisScript"];
					NSIS installer = null;
					if( arguments["nsisExecutable"] != null && arguments["nsisPath"] != null )
					{
						installer = new NSIS( arguments["nsisPath"], arguments["nsisExecutable"] );
					}
					else
					{
						installer = new NSIS();
					}
					if( arguments["quiet"] != null || arguments["q"] != null )
					{
						installer.Quiet = true;
					}

					// Check if NSIS is installed
					if( !installer.IsInstalled )
					{
						WriteLine( "NSIS is not installed, will not create an installer!" );
						Environment.Exit( 0 );
					}

					// Check if the script exists
					if( !File.Exists( nsisScript ) )
					{
						WriteLine( "NSIS script not found at {0}.", nsisScript );
						Environment.Exit( 4 );
					}

					// Run installer script
					bool success = installer.Run( workingDirectory, nsisScript );
					if( !success )
					{
						WriteLine( installer.ErrorMessage );
						Environment.Exit( 2 );
					}
#endif
				}
			}
			catch( Exception ex )
			{
				WriteLine( "Unexpected exception: {0}", ex );
				Environment.Exit( 1 );
			}
		}

		private static void WriteLine( string format, params object[] args )
		{
			Console.WriteLine( "Twp.Build: " + format, args );
		}
	}
}
