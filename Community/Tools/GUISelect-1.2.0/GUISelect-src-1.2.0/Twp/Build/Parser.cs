﻿// $Id: Parser.cs 25 2011-06-01 12:26:11Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Twp.Build
{
	public class Parser
	{
		private FileInfo fileInfo;
		private string workingDirectory;
		private ParserInfo info = new ParserInfo();
		private List<FileInfo> files;
		private List<FileInfo> output;
		private Dictionary<string, string> shortCuts;

		private bool quiet = false;
		public bool Quiet
		{
			get { return this.quiet; }
			set { this.quiet = value; }
		}

		private string errorMessage = null;
		public string ErrorMessage
		{
			get { return this.errorMessage; }
		}

		public bool Run( string workingDirectory, string inScript )
		{
			this.fileInfo = new FileInfo( Path.Combine( workingDirectory, inScript ) );
			this.workingDirectory = workingDirectory;
			this.WriteLine( "InScript: {0}", this.fileInfo.FullName );
			if( !this.fileInfo.Exists )
			{
				this.errorMessage = String.Format( "InScript File not found: {0}", this.fileInfo );
				return false;
			}

			using( StreamReader sr = this.fileInfo.OpenText() )
			{
				while( !sr.EndOfStream )
				{
					string line = this.ReadLine( sr );

					if( String.IsNullOrEmpty( line ) || line.StartsWith( "#" ) )
						continue;

					string[] parts = line.Split( new char[] { '=' } );
					if( parts.Length != 2 )
						this.WriteLine( "Failed to parse line: {0}", line );
					else
						this.ParseToken( parts[0], parts[1].Trim() );
				}
				sr.Close();
			}

			foreach( FileInfo outFile in this.output )
			{
				if( !this.ProcessFile( outFile ) )
					return false;
			}

			return true;
		}

		private string ReadLine( StreamReader sr )
		{
			string line = sr.ReadLine();
			if( String.IsNullOrEmpty( line ) )
				return null;

			if( line.EndsWith( "\\" ) )
			{
				line = line.TrimEnd( new char[] { '\\' } );
				line = line.TrimEnd();
				line += " " + this.ReadLine( sr );
			}
			return line.Trim();
		}

		private void ParseToken( string token, string value )
		{
			string trimmed = value.Trim( new char[] { '"', '\'' } );
			switch( token )
			{
				case "PRODUCT_NAME":
					this.info.Product = trimmed;
					this.WriteLine( "Product Name: {0}", this.info.Product );
					break;

				case "PRODUCT_VERSION":
					// Quick-fix SVN revision number...
					if( trimmed.Contains( "$WCREV$" ) )
					{
						trimmed = trimmed.Replace( "$WCREV$", "0" );
					}
					this.info.Version = new Version( trimmed );
					this.WriteLine( "Version: {0}", this.info.Version );
					break;

				case "PRODUCT_DESCRIPTION":
					this.info.Description = trimmed;
					this.WriteLine( "Description: {0}", this.info.Description );
					break;

				case "PRODUCT_COMPANY":
					this.info.Company = trimmed;
					this.WriteLine( "Company: {0}", this.info.Company );
					break;

				case "PRODUCT_COPYRIGHT":
					this.info.Copyright = trimmed;
					this.WriteLine( "Copyright: {0}", this.info.Copyright );
					break;

				case "PRODUCT_LICENSE":
					this.info.License = trimmed;
					this.WriteLine( "License: {0}", this.info.License );
					break;

				case "FILES":
					this.WriteLine( "Files:" );
					this.files = this.ParseFiles( value );
					break;

				case "SHORTCUTS":
					this.WriteLine( "Shortcuts:" );
					this.ParseShortCuts( value );
					break;

				case "OUTPUT":
					this.WriteLine( "Output Files:" );
					this.output = this.ParseFiles( value );
					break;

				default:
					this.WriteLine( "Unknown token: {0} (with value {1})", token, value );
					break;
			}
		}

		private List<FileInfo> ParseFiles( string input )
		{
			List<FileInfo> files = new List<FileInfo>();
			string[] fileNames = input.Split( new char[] { ' ' } );
			foreach( string fileName in fileNames )
			{
				string trimmed = fileName.Trim( new char[] { '"', '\'' } );
				FileInfo file = new FileInfo( Path.Combine( this.workingDirectory, trimmed ) );
				this.WriteLine( "FileName: {0}", file.FullName );
				files.Add( file );
			}
			return files;
		}

		private void ParseShortCuts( string input )
		{
			this.shortCuts = new Dictionary<string, string>();
			string[] files = input.Split( new char[] { ' ' } );
			foreach( string file in files )
			{
				string[] parts = file.Split( new char[] { '|' }, 2 );
				if( parts.Length != 2 )
				{
					this.WriteLine( "Failed to parse shortcut: {0}", file );
				}
				else
				{
					this.WriteLine( "Shortcut: {0} => {1}", parts[1], parts[0] );
					this.shortCuts.Add( parts[0], parts[1] );
				}
			}
		}

		private bool ProcessFile( FileInfo file )
		{
			FileInfo input = new FileInfo( file.FullName + ".in" );
			if( !input.Exists )
			{
				this.errorMessage = String.Format( "OUTPUT ERROR! File not found: {0}", input.FullName );
				return false;
			}

			if( file.Exists )
			{
				file.Delete();
			}

			this.WriteLine( "Processing {0}...", input.FullName );
			using( StreamWriter sw = file.CreateText() )
			{
				using( StreamReader sr = input.OpenText() )
				{
					while( !sr.EndOfStream )
					{
						string line = sr.ReadLine();
						line = this.ProcessLine( line );
						if( line.Contains( "\n" ) )
						{
							string[] lines = line.Split( new char[] { '\n' } );
							foreach( string subLine in lines )
							{
								sw.WriteLine( subLine );
							}
						}
						else
						{
							sw.WriteLine( line );
						}
					}
					sr.Close();
				}
				sw.Close();
			}

			return true;
		}

		private string ProcessLine( string line )
		{
			return Regex.Replace( line, @"@([A-Z_]\w+)?@", new MatchEvaluator( ProcessToken ) );
		}

		private string ProcessToken( Match match )
		{
			string value = "";
			switch( match.Value )
			{
				case "@PRODUCT@":
					value = this.info.Product;
					break;

				case "@VERSION@":
					value = this.info.Version.ToString();
					break;

				case "@MAJOR@":
					value = this.info.Version.Major.ToString();
					break;

				case "@MINOR@":
					value = this.info.Version.Minor.ToString();
					break;

				case "@BUILD@":
					value = this.info.Version.Build.ToString();
					break;

				case "@REVISION@":
					value = this.info.Version.Revision.ToString();
					break;

				case "@DESCRIPTION@":
					value = this.info.Description;
					break;

				case "@COMPANY@":
					value = this.info.Company;
					break;

				case "@COPYRIGHT@":
					value = this.info.Copyright;
					break;

				case "@LICENSE@":
					if( !String.IsNullOrEmpty( this.info.License ) )
					{
						FileInfo file = new FileInfo( Path.Combine( this.workingDirectory, this.info.License ) );
						value = "!insertmacro MUI_PAGE_LICENSE \"" + file.FullName + "\"";
					}
					break;

				case "@FILES@":
					value = ProcessFiles( match.Index );
					break;
		
				case "@SHORTCUTS@":
					value = ProcessShortCuts( match.Index );
					break;
			}
			return value;
		}

		private string ProcessFiles( int pad )
		{
			string value = null;
			foreach( FileInfo file in this.files )
			{
				if( value != null )
					value += "\n" + new String( ' ', pad );
				value += "File \"" + file.FullName + "\"";
			}
			return value;
		}

		private string ProcessShortCuts( int pad )
		{
			string value = null;
			foreach( KeyValuePair<string, string> kvp in this.shortCuts )
			{
				if( value != null )
					value += "\n" + new String( ' ', pad );
				value += "CreateShortCut \"$SMPROGRAMS\\$StartMenuFolder\\";
				value += kvp.Value + ".lnk\" \"$INSTDIR\\" + kvp.Key + "\"";
			}
			return value;
		}

		private void WriteLine( string format, params object[] args )
		{
			if( this.quiet )
				return;

			Console.WriteLine( "Twp.Build: " + format, args );
		}
	}
}
