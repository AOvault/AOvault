﻿/*
* Vha.Build
* Copyright (C) 2010 Remco van Oosterhout
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Twp.Build
{
	public class NSIS
	{
		private string path;
		private string executable;
		private Dictionary<string, string> definitions;

		public NSIS()
		{
			this.executable = Path.Combine( "NSIS", "makensis.exe" );
			this.path = Environment.GetFolderPath( Environment.SpecialFolder.ProgramFiles ) + Path.DirectorySeparatorChar;
			if( !this.IsInstalled )
				this.path = "C:/Program Files/";
			if( !this.IsInstalled )
				this.path = "C:/Program Files (x86)/";
			this.definitions = new Dictionary<string, string>();
		}

		public NSIS( string path, string executable )
		{
			this.path = Path.GetFullPath( path );
			this.executable = executable;
			this.definitions = new Dictionary<string, string>();
		}

		public bool IsInstalled
		{
			get { return File.Exists( this.FullPath ); }
		}

		public string FullPath
		{
			get { return Path.Combine( this.path, this.executable ); }
		}

		private string errorMessage = null;
		public string ErrorMessage
		{
			get { return this.errorMessage; }
		}

		private bool quiet = false;
		public bool Quiet
		{
			get { return this.quiet; }
			set { this.quiet = value; }
		}

		public void Define( string key, string value )
		{
			if( this.definitions.ContainsKey( key ) )
				this.definitions[key] = value;
			else
				this.definitions.Add( key, value );
			this.WriteLine( "Defined '{0}' as '{1}'", key, value );
		}

		public bool Run( string script )
		{
			return this.Run( Environment.CurrentDirectory, script );
		}

		public bool Run( string workingDirectory, string script )
		{
			// Check if NSIS is installed
			if( !this.IsInstalled )
			{
				this.errorMessage = "Unable to locate Nullsoft Scriptable Install System";
				return false;
			}
			// Some preperations
			script = Path.GetFullPath( script );
			workingDirectory = Path.GetFullPath( workingDirectory );
			this.WriteLine( "Script: " + script );
			this.WriteLine( "Working directory: " + workingDirectory );
			StringBuilder args = new StringBuilder();
			foreach( KeyValuePair<string, string> d in this.definitions )
			{
				args.Append( string.Format(
					"\"/D{0}={1}\" ",
					d.Key,
					d.Value.Replace( "\\", "\\\\" ).Replace( "\"", "\\\"" )
					) );
			}
			if( this.quiet )
				args.Append( "/V2 " );
			else
				args.Append( "/V3 " );
			args.Append( "\"" + script + "\"" );
			this.WriteLine( "Arguments: " + args.ToString() );

			// Prepare process
			ProcessStartInfo info = new ProcessStartInfo( this.FullPath, args.ToString() );
			info.WorkingDirectory = workingDirectory;
			info.UseShellExecute = false;
			info.RedirectStandardOutput = true;

			// Start process
			Process process = Process.Start( info );
			Console.WriteLine( process.StandardOutput.ReadToEnd() );
			process.WaitForExit();
			int exitCode = process.ExitCode;
			process.Dispose();
			if( exitCode == 0 )
				return true;

			// Report error
			this.errorMessage = String.Format( "NSIS terminated with error code {0}", exitCode );
			return false;
		}

		private void WriteLine( string format, params object[] args )
		{
			if( this.quiet )
				return;

			Console.WriteLine( "Twp.Build: " + format, args );
		}
	}
}
