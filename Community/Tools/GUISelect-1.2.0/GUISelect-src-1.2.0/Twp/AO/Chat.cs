﻿// $Id: Chat.cs 23 2011-06-01 07:55:24Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.IO;

namespace Twp.AO
{
	public class Chat
	{
		public Chat()
		{
		}
		
		public void Read( string path )
		{
			DirectoryInfo dirInfo = new DirectoryInfo( Path.Combine( path, "Windows" ) );
			DirectoryInfo[] winDirs = dirInfo.GetDirectories( "Window*" );
			foreach( DirectoryInfo winDir in winDirs )
			{
				ChatWindow window = new ChatWindow( winDir );
				this.windows.Add( window );
			}
		}

		private List<ChatWindow> windows = new List<ChatWindow>();
		public List<ChatWindow> Windows
		{
			get { return this.windows; }
		}
	}

	public class ChatWindow
	{
		public ChatWindow( DirectoryInfo dirInfo )
		{
			this.dirInfo = dirInfo;
			this.config.Read( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "Config.xml" ) );
			this.inputHistory.Read( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "InputHistory.xml" ) );
		}

		private DirectoryInfo dirInfo;

		private Xml.XmlFile config = new Xml.XmlFile();
		public Xml.XmlFile Config
		{
			get { return this.config; }
		}

		private Xml.XmlFile inputHistory = new Xml.XmlFile();
		public Xml.XmlFile InputHistory
		{
			get { return this.inputHistory; }
		}
	}
}
