﻿// $Id: BinFile.cs 6 2011-05-19 09:41:20Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.IO;
using System.Text;

namespace Twp.AO
{
	public class BinFile
	{
		public BinFile()
		{
			this.fileName = String.Empty;
			this.stream = null;
		}

		private string fileName;
		public string FileName
		{
			get { return this.fileName; }
		}

		private FileStream stream;

		public bool Open( string fileName )
		{
			if( File.Exists( fileName ) )
			{
				this.stream = File.OpenRead( fileName );
				return true;
			}
			else
				return false;
		}

		public byte[] GetBytes( int length )
		{
			byte[] buffer = new byte[length];
			this.stream.Read( buffer, 0, length );
			return buffer;
		}

		public byte GetByte()
		{
			byte[] buffer = this.GetBytes( 1 );
			return buffer[0];
		}

		public short GetShort()
		{
			byte[] buffer = this.GetBytes( 2 );
			if( BitConverter.IsLittleEndian )
				Array.Reverse( buffer );
			short value = BitConverter.ToInt16( buffer, 0 );
			return value;
		}

		public ushort GetUShort()
		{
			byte[] buffer = this.GetBytes( 2 );
			if( BitConverter.IsLittleEndian )
				Array.Reverse( buffer );
			ushort value = BitConverter.ToUInt16( buffer, 0 );
			return value;
		}

		public int GetInt()
		{
			byte[] buffer = this.GetBytes( 4 );
			if( BitConverter.IsLittleEndian )
				Array.Reverse( buffer );
			int value = BitConverter.ToInt32( buffer, 0 );
			return value;
		}

		public uint GetUInt()
		{
			byte[] buffer = this.GetBytes( 4 );
			if( BitConverter.IsLittleEndian )
				Array.Reverse( buffer );
			uint value = BitConverter.ToUInt32( buffer, 0 );
			return value;
		}

		public string GetString()
		{
			int length = this.GetInt();
			byte[] buffer = this.GetBytes( length );
			string value = Encoding.ASCII.GetString( buffer );
			return value;
		}
	}
}
