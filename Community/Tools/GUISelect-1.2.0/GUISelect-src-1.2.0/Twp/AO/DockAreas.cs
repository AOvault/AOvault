﻿// $Id: DockAreas.cs 23 2011-06-01 07:55:24Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.IO;
using Twp.Utilities;

namespace Twp.AO
{
	public class DockAreas
	{
		public DockAreas()
		{
		}

		public void Read( string path )
		{
			this.items.Clear();
			this.oldAreas = 0;

			DirectoryInfo dirInfo = new DirectoryInfo( path );
			this.rollupArea = new DockArea( Twp.Utilities.Path.Combine( dirInfo.FullName, "RollupArea.xml" ) );
			this.planetMap = new DockArea( Twp.Utilities.Path.Combine( dirInfo.FullName, "PlanetMapViewConfig.xml" ) );
			FileInfo[] dockFiles = dirInfo.GetFiles( "DockArea*" );
			foreach( FileInfo dockFile in dockFiles )
			{
				DockArea dockArea = new DockArea( dockFile.FullName );
				this.items.Add( dockArea.Name, dockArea );
				if( dockArea.IsOld )
					this.oldAreas++;
			}
		}

		private DockArea rollupArea;
		public DockArea RollupArea
		{
			get { return this.rollupArea; }
		}

		private DockArea planetMap;
		public DockArea PlanetMap
		{
			get { return this.planetMap; }
		}

		private Dictionary<string, DockArea> items = new Dictionary<string, DockArea>();
		public Dictionary<string, DockArea> Items
		{
			get { return this.items; }
		}

		private int oldAreas = 0;
		public int OldAreas
		{
			get { return this.oldAreas; }
		}
	}

	public class DockArea : ContainerBase
	{
		public DockArea( string fileName ) : base( fileName )
		{
			this.Parse();
			// check age.
			TimeSpan span = DateTime.UtcNow - this.fileInfo.LastWriteTimeUtc;
			this.isOld = span.Days > Container.OldAge;
			this.isStale = span.Days > Container.StaleAge;
		}

		private void Parse()
		{
			Xml.ArchiveElement root = this.xmlFile.Root as Xml.ArchiveElement;
			if( root == null )
				return;

			foreach( Xml.IAOElement element in root.Items )
			{
				Xml.ArchiveElement archive = element as Xml.ArchiveElement;
				if( archive != null && archive.Name == "dock_config" )
				{
					this.ParseConfig( archive );
				}
				Xml.ArchiveItem item = element as Xml.ArchiveItem;
				if( item != null )
				{
					switch( item.Name )
					{
						case "dock_name":
							this.name = Xml.XmlFile.StripQuotes( item.Value );
							break;

						case "dock_type":
							this.type = Xml.XmlFile.StripQuotes( item.Value );
							break;

						default:
							Log.Debug( "[DockArea] Unknown element: {0}", item.Name );
							break;
					}
				}
			}
		}

		private void ParseConfig( Xml.ArchiveElement archive )
		{
			foreach( Xml.IAOElement element in archive.Items )
			{
				Xml.ArchiveElement array = element as Xml.ArchiveElement;
				if( array != null && array.Type == "Array" )
				{
					switch( array.Name )
					{
						case "dock_node_configs":
							// used in rollup area
							break;

						case "docked_view_identities":
							// used if there are multiple windows connected to this dockarea
							this.identities.Clear();
							foreach( Xml.ArchiveItem arrayItem in array.Items )
							{
								this.identities.Add( Xml.XmlFile.StripQuotes( arrayItem.Value ) );
							}
							break;

						default:
							break;
					}
				}
				Xml.ArchiveItem item = element as Xml.ArchiveItem;
				if( item != null )
				{
					switch( item.Name )
					{
						case "docked_view_identities":
							// used if there's only one window connected to this dockarea
							this.identities.Clear();
							this.identities.Add( Xml.XmlFile.StripQuotes( item.Value ) );
							break;

						default:
							break;
					}
				}
			}
		}

		private string name;
		public string Name
		{
			get { return this.name; }
		}

		private string type;
		public string Type
		{
			get { return this.type; }
		}

		private List<string> identities = new List<string>();
		public List<string> Identities
		{
			get { return this.identities; }
		}

		private bool isOld;
		public bool IsOld
		{
			get { return this.isOld; }
		}

		private bool isStale;
		public bool IsStale
		{
			get { return this.isStale; }
		}
	}
}
