﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Twp.AO
{
	public class Gui
	{
		internal Gui()
		{
			this.xmlFile = null;
			this.dirInfo = null;
		}

		/// <summary>
		/// Creates a new instance of the <see cref="Twp.AO.Gui">Gui</c> class.
		/// </summary>
		/// <param name="xmlFile">The XmlFile containg the user's Prefs.xml data.</param>
		public Gui( Xml.XmlFile xmlFile )
		{
			this.xmlFile = xmlFile;
			string guiName = this.Name;
			if( String.IsNullOrEmpty( guiName ) )
				guiName = "Default";
			this.dirInfo = new DirectoryInfo( GetPath( this.xmlFile.FileInfo.Directory.Parent.FullName, guiName ) );
		}

		/// <summary>
		/// Creates a new instance of the <see cref="Twp.AO.Gui">Gui</c> class.
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <param name="gui">A string representing a GUI folder.</param>
		public Gui( string path, string gui )
		{
			if( Gui.IsValid( path, gui ) )
			{
				this.xmlFile = Prefs.Read( path );
				this.dirInfo = new DirectoryInfo( GetPath( path, gui ) );
			}
		}

		public static Gui Default( string path )
		{
			Gui gui = new Gui();
			gui.dirInfo = new DirectoryInfo( GetPath( path, "Default" ) );
			return gui;
		}

		private Xml.XmlFile xmlFile;
		private Xml.IAOElement nameElement;
		private DirectoryInfo dirInfo;
		public DirectoryInfo DirInfo
		{
			get { return this.dirInfo; }
		}

		/// <summary>
		/// Checks if the path of <paramref name="gui"/> exists in relation to <paramref name="path"/>.
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <param name="gui">A string representing a GUI folder.</param>
		/// <returns>true if the path is valid; otherwise, false.</returns>
		public static bool IsValid( string path, string gui )
		{
			return Directory.Exists( GetPath( path, gui ) );
		}

		/// <summary>
		/// Checks if the path of <paramref name="gui"/> exists.
		/// </summary>
		/// <param name="gui">A <see cref="Gui"/> containing information about a GUI folder.</param>
		/// <returns>true if the path is valid; otherwise, false.</returns>
		public static bool IsValid( Gui gui )
		{
			return gui.dirInfo.Exists;
		}

		/// <summary>
		/// Returns a string representing the file [path]\\cd_image\\gui\\[gui]\\[file].
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <param name="gui">A string representing a GUI folder.</param>
		/// <param name="file">A string representing a file name.</param>
		/// <returns>a string representing the file [path]\\cd_image\\gui\\[gui]\\[file].</returns>
		public static string GetFile( string path, string gui, string file )
		{
			return Twp.Utilities.Path.Combine( GetPath( path, gui ), file );
		}

		/// <summary>
		/// Returns a string representing the folder [path]\\cd_image\\gui\\[gui].
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <param name="gui">A string representing a GUI folder.</param>
		/// <returns>a string representing the folder [path]\\cd_image\\gui\\[gui].</returns>
		public static string GetPath( string path, string gui )
		{
			return Twp.Utilities.Path.Combine( path, "cd_image", "gui", gui );
		}

		/// <summary>
		/// Gets a list of GUI's that exist in the GUI base dir in relation to <paramref name="path"/>.
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <returns>An array holding the GUI's, or null if no GUI's were found.</returns>
		public static string[] List( string path )
		{
			DirectoryInfo di = new DirectoryInfo( Twp.Utilities.Path.Combine( path, "cd_image", "gui" ) );
			if( !di.Exists )
				return null;

			DirectoryInfo[] subs = di.GetDirectories();
			if( subs.Length > 0 )
			{
				string[] guiDirs = new string[subs.Length];
				for( int i = 0; i < subs.Length; ++i )
				{
					guiDirs[i] = subs[i].Name;
				}
				return guiDirs;
			}
			else
				return null;
		}

		private Xml.XmlFile mainPrefs = null;
		public Xml.XmlFile MainPrefs
		{
			get
			{
				if( this.mainPrefs == null )
				{
					this.mainPrefs = new Xml.XmlFile();
					this.mainPrefs.Read( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "MainPrefs.xml" ) );
				}
				return this.mainPrefs;
			}
		}

		private Xml.XmlFile loginPrefs = null;
		public Xml.XmlFile LoginPrefs
		{
			get
			{
				if( this.loginPrefs == null )
				{
					this.loginPrefs = new Xml.XmlFile();
					this.loginPrefs.Read( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "LoginPrefs.xml" ) );
				}
				return this.loginPrefs;
			}
		}

		private Xml.XmlFile charPrefs = null;
		public Xml.XmlFile CharPrefs
		{
			get
			{
				if( this.charPrefs == null )
				{
					this.charPrefs = new Xml.XmlFile();
					this.charPrefs.Read( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "CharPrefs.xml" ) );
				}
				return this.charPrefs;
			}
		}

		private Xml.XmlFile textColors = null;
		public Xml.XmlFile TextColors
		{
			get
			{
				if( this.textColors == null )
				{
					this.textColors = new Xml.XmlFile();
					this.textColors.Read( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "TextColors.xml" ) );
				}
				return this.textColors;
			}
		}

		/// <summary>
		/// Gets or sets the selected GUI Name in a user's Prefs.xml file in an
		/// Anarchy Online installation.
		/// </summary>
		public string Name
		{
			get
			{
				if( this.nameElement == null )
				{
					if( this.xmlFile == null || this.xmlFile.Root == null )
						return null;

					this.nameElement = this.xmlFile.Root.GetElement( "GUIName" );
					if( this.nameElement == null )
						return null;
				}
				return Xml.XmlFile.StripQuotes( this.nameElement.Value );
			}
			set
			{
				if( this.nameElement == null )
				{
					if( this.xmlFile == null || this.xmlFile.Root == null )
						return;

					this.nameElement = xmlFile.Root.GetElement( "GUIName" );
					if( this.nameElement == null )
						return;
				}
				this.nameElement.Value = Xml.XmlFile.AddQuotes( value );
			}
		}

		/// <summary>
		/// Gets or sets the selected GUI SkinFile in a user's Prefs.xml file in an
		/// Anarchy Online installation.
		/// </summary>
		public string SkinFile
		{
			get
			{
				if( this.nameElement == null )
				{
					if( this.xmlFile == null || this.xmlFile.Root == null )
						return null;

					this.nameElement = this.xmlFile.Root.GetElement( "GUISkin" );
					if( this.nameElement == null )
						return null;
				}
				return this.nameElement.Value.Trim( new char[] { '"' } );
			}
			set
			{
				if( this.nameElement == null )
				{
					if( this.xmlFile == null || this.xmlFile.Root == null )
						return;

					this.nameElement = xmlFile.Root.GetElement( "GUISkin" );
					if( this.nameElement == null )
						return;
				}
				this.nameElement.Value = '"' + value + '"';
			}
		}
	}
}
