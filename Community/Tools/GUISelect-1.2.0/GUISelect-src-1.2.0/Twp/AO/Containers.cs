﻿// $Id: Containers.cs 23 2011-06-01 07:55:24Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.IO;
using Twp.Utilities;

namespace Twp.AO
{
	public class Containers
	{
		public Containers()
		{
		}

		public void Read( string path )
		{
			DirectoryInfo dirInfo = new DirectoryInfo( path );
			this.bank = new Container( Twp.Utilities.Path.Combine( dirInfo.FullName, "Bank.xml" ) );
			this.inventory = new Container( Twp.Utilities.Path.Combine( dirInfo.FullName, "Inventory.xml" ) );
			this.specialActions = new Container( Twp.Utilities.Path.Combine( dirInfo.FullName, "SpecialActions.xml" ) );

			this.bags.Clear();
			this.oldBags = 0;
			FileInfo[] bagFiles = dirInfo.GetFiles( "Container_51017x*" );
			foreach( FileInfo bagFile in bagFiles )
			{
				Container bag = new Container( bagFile.FullName );
				this.bags.Add( bag );
				if( bag.IsOld )
					this.oldBags++;
			}

			this.lootBags.Clear();
			FileInfo[] lootBagFiles = dirInfo.GetFiles( "TempContainer_*" );
			foreach( FileInfo lootBagFile in lootBagFiles )
			{
				Container lootBag = new Container( lootBagFile.FullName );
				this.bags.Add( lootBag );
			}

			this.shortcutBars.Clear();
			FileInfo[] shortcutBarFiles = dirInfo.GetFiles( "ShortcutBar_*" );
			foreach( FileInfo shortcutBarFile in shortcutBarFiles )
			{
				ShortcutBar shortcutBar = new ShortcutBar( shortcutBarFile.FullName );
				this.shortcutBars.Add( shortcutBar );
			}
		}

		private Container bank;
		public Container Bank
		{
			get { return this.bank; }
		}

		private Container inventory;
		public Container Inventory
		{
			get { return this.inventory; }
		}

		private List<Container> bags = new List<Container>();
		public List<Container> Bags
		{
			get { return this.bags; }
		}

		private int oldBags = 0;
		public int OldBags
		{
			get { return this.oldBags; }
		}

		private List<Container> lootBags = new List<Container>();
		public List<Container> LootBags
		{
			get { return this.lootBags; }
		}

		private Container specialActions;
		public Container SpecialActions
		{
			get { return this.specialActions; }
		}

		private List<ShortcutBar> shortcutBars = new List<ShortcutBar>();
		public List<ShortcutBar> ShortcutBars
		{
			get { return this.shortcutBars; }
		}
	}

	public class ContainerBase
	{
		public ContainerBase( string fileName )
		{
			this.fileInfo = new FileInfo( fileName );
			this.xmlFile.Read( this.fileInfo.FullName );
		}

		protected FileInfo fileInfo;
		protected Xml.XmlFile xmlFile = new Xml.XmlFile();
	}

	public class Container : ContainerBase
	{
		public Container( string fileName ) : base( fileName )
		{
			this.Parse();
		}

		public static int OldAge = 30;
		public static int StaleAge = 90;

		protected virtual void Parse()
		{
			// check age.
			TimeSpan span = DateTime.UtcNow - this.fileInfo.LastWriteTimeUtc;
			this.isOld = span.Days > OldAge;
			this.isStale = span.Days > StaleAge;

			Xml.ArchiveElement root = this.xmlFile.Root as Xml.ArchiveElement;
			if( root == null )
				return;

			foreach( Xml.IAOElement element in root.Items )
			{
				Xml.ArchiveItem item = element as Xml.ArchiveItem;
				if( item != null )
				{
					switch( item.Name )
					{
						case "container_name":
							this.name = Xml.XmlFile.StripQuotes( item.Value );
							break;

						case "DockableViewDockName":
							this.dockArea = Xml.XmlFile.StripQuotes( item.Value );
							break;

						default:
							break;
					}
				}
			}
		}

		public string Id
		{
			get { return this.fileInfo.Name; }
		}

		private string name;
		public string Name
		{
			get { return this.name; }
		}

		private string dockArea;
		public string DockArea
		{
			get { return this.dockArea; }
		}

		private bool isOld;
		public bool IsOld
		{
			get { return this.isOld; }
		}

		private bool isStale;
		public bool IsStale
		{
			get { return this.isStale; }
		}
	}

	public class ShortcutBar : ContainerBase
	{
		public ShortcutBar( string fileName ) : base( fileName ) { }

		protected void Parse()
		{
			Xml.ArchiveElement root = this.xmlFile.Root as Xml.ArchiveElement;
			if( root == null )
			    return;

			foreach( Xml.IAOElement element in root.Items )
			{
				//Xml.ArchiveElement archive = element as Xml.ArchiveElement;
				//if( archive != null && archive.Name == "dock_config" )
				//{
				//    this.ParseConfig( archive );
				//}
				Xml.ArchiveItem item = element as Xml.ArchiveItem;
				if( item != null )
				{
					switch( item.Name )
					{
						default:
							break;
					}
				}
			}
		}
	}
}
