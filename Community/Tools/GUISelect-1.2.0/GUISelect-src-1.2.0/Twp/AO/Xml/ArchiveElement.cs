﻿// $Id: ArchiveElement.cs 7 2011-05-20 15:56:53Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Xml;

namespace Twp.AO.Xml
{
	public class ArchiveElement : IAOElement
	{
		public ArchiveElement()
		{
			this.type = "Archive";
			this.name = String.Empty;
			this.code = "0";
		}

		public ArchiveElement( string name, string code )
		{
			this.type = "Archive";
			this.name = name;
			this.code = code;
		}

		public ArchiveElement( string type, string name, string code )
		{
			this.type = type;
			this.name = name;
			this.code = code;
		}

		private string type;
		public string Type
		{
			get { return this.type; }
			set { this.type = value; }
		}

		private string name;
		public string Name
		{
			get { return this.name; }
			set { this.name = value; }
		}

		private string code;
		public string Value
		{
			get { return this.code; }
			set { this.code = value; }
		}

		public bool HasChildren
		{
			get { return this.items.Count > 0; }
		}

		private List<IAOElement> items = new List<IAOElement>();
		public List<IAOElement> Items
		{
			get { return this.items; }
		}

		public void ReadXml( XmlReader reader )
		{
			this.type = reader.Name;
			if( reader.HasAttributes )
			{
				this.name = reader.GetAttribute( "name" );
				this.code = reader.GetAttribute( "code" );
			}

			this.items.Clear();
			if( reader.IsEmptyElement )
				return;

			while( reader.Read() )
			{
				if( reader.IsStartElement() )
				{
					IAOElement element = ArchiveElement.CreateElement( reader.Name );
					element.ReadXml( reader );
					this.items.Add( element );
				}
				else if( reader.NodeType == XmlNodeType.EndElement )
					break;
			}
		}

		public void WriteXml( XmlWriter writer )
		{
			writer.WriteStartElement( this.type );
			writer.WriteAttributeString( "name", this.name );
			if( !String.IsNullOrEmpty( this.code ) )
			{
				writer.WriteAttributeString( "code", this.code );
			}
			foreach( IAOElement element in this.items )
			{
				element.WriteXml( writer );
			}
			writer.WriteEndElement();
		}

		public IAOElement GetElement( string name )
		{
			if( this.name == name )
				return this;

			foreach( IAOElement element in this.items )
			{
				IAOElement child = element.GetElement( name );
				if( child != null )
					return child;
			}
			return null;
		}

		public override string ToString()
		{
			if( String.IsNullOrEmpty( this.code ) )
				return String.Format( "[{0}] {1}", this.type, this.name );
			else
				return String.Format( "[{0}] {1} (Code={2})", this.type, this.name, this.code );
		}

		public static IAOElement CreateElement( string name )
		{
			IAOElement element;
			switch( name )
			{
				case "Archive":
				case "Array":
					element = new ArchiveElement();
					break;

				default:
					element = new ArchiveItem();
					break;
			}
			return element;
		}
	}
}
