﻿// $Id: IAOElement.cs 7 2011-05-20 15:56:53Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Xml;

namespace Twp.AO.Xml
{
	public interface IAOElement
	{
		string Name
		{
			get;
			set;
		}

		string Value
		{
			get;
			set;
		}

		bool HasChildren
		{
			get;
		}

		void ReadXml( XmlReader reader );

		void WriteXml( XmlWriter writer );

		IAOElement GetElement( string name );
	}
}
