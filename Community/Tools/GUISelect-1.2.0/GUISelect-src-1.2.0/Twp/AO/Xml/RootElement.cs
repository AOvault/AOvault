﻿// $Id: RootElement.cs 11 2011-05-27 19:24:27Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Xml;

namespace Twp.AO.Xml
{
	public class RootElement : IAOElement
	{
		public RootElement()
		{
		}

		private List<IAOElement> items = new List<IAOElement>();
		public List<IAOElement> Items
		{
			get { return this.items; }
		}

		public string Name
		{
			get { return "Root"; }
			set { throw new NotImplementedException(); }
		}

		public string Value
		{
			get { throw new NotImplementedException(); }
			set { throw new NotImplementedException(); }
		}

		public bool HasChildren
		{
			get { return this.items.Count > 0; }
		}

		public void ReadXml( XmlReader reader )
		{
			if( reader.IsEmptyElement )
				return;

			while( reader.Read() )
			{
				if( reader.IsStartElement() )
				{
					IAOElement node = XmlFile.CreateElement( reader.Name );
					node.ReadXml( reader );
					this.items.Add( node );
				}
				else if( reader.NodeType == XmlNodeType.EndElement )
					break;
			}
		}

		public void WriteXml( XmlWriter writer )
		{
			writer.WriteStartElement( "Root" );
			foreach( IAOElement node in this.items )
			{
				node.WriteXml( writer );
			}
			writer.WriteEndElement();
		}

		public IAOElement GetElement( string name )
		{
			foreach( IAOElement element in this.items )
			{
				IAOElement child = element.GetElement( name );
				if( child != null )
					return child;
			}
			return null;
		}

		public override string ToString()
		{
			return "[Root]";
		}
	}
}
