﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace Twp.AO.Xml
{
	public class HtmlColorElement : IAOElement
	{
		public HtmlColorElement()
		{
		}

		private string name;
		public string Name
		{
			get { return this.name; }
			set { this.name = value; }
		}

		private string value;
		public string Value
		{
			get { return this.value; }
			set { this.value = value; }
		}

		public bool HasChildren
		{
			get { return false; }
		}

		public void ReadXml( XmlReader reader )
		{
			if( !reader.HasAttributes )
				return;

			this.name = reader.GetAttribute( "name" );
			this.value = reader.GetAttribute( "color" );
		}

		public void WriteXml( XmlWriter writer )
		{
			writer.WriteStartElement( "HTMLColor" );
			writer.WriteAttributeString( "name", this.name );
			writer.WriteAttributeString( "color", this.value );
			writer.WriteEndElement();
		}

		public IAOElement GetElement( string name )
		{
			if( this.name == name )
				return this;
			return null;
		}

		public override string ToString()
		{
			return String.Format( "[HtmlColor] {0}={1}", this.name, this.value );
		}
	}
}
