﻿using System;
using System.Xml;

namespace Twp.AO.Xml
{
	public class UnknownElement : IAOElement
	{
		public UnknownElement()
		{
			this.name = String.Empty;
			this.value = String.Empty;
		}

		public UnknownElement( string name, string value )
		{
			this.name = name;
			this.value = value;
		}

		private string name;
		public string Name
		{
			get { return this.name; }
			set { this.name = value; }
		}

		private string value;
		public string Value
		{
			get { return this.value; }
			set { this.value = value; }
		}

		public bool HasChildren
		{
			get { return false; }
		}

		public void ReadXml( XmlReader reader )
		{
			if( reader.HasAttributes )
			{
				this.name = reader.GetAttribute( "name" );
			}
			else
			{
				this.name = "Unknown";
			}
			this.value = reader.ReadOuterXml();
		}

		public void WriteXml( XmlWriter writer )
		{
			writer.WriteRaw( this.value );
		}

		public IAOElement GetElement( string name )
		{
			if( this.name == name )
				return this;
			return null;
		}

		public override string ToString()
		{
			return "[Unknown] " + this.value;
		}
	}
}
