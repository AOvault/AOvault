﻿// $Id: XmlFile.cs 27 2011-06-05 22:35:02Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.IO;
using System.Xml;
using Twp.Utilities;

namespace Twp.AO.Xml
{
	public class XmlFile
	{
		public XmlFile()
		{
		}

		private IAOElement root;
		public IAOElement Root
		{
			get { return this.root; }
		}

		private FileInfo fileInfo;
		public FileInfo FileInfo
		{
			get { return this.fileInfo; }
			set { this.fileInfo = value; }
		}

		public bool Read( string fileName )
		{
			if( !File.Exists( fileName ) )
				return false;

			this.fileInfo = new FileInfo( fileName );
			XmlReader reader = null;
			try
			{
				XmlReaderSettings settings = new XmlReaderSettings();
				settings.IgnoreWhitespace = true;
				settings.IgnoreComments = true;
				reader = XmlReader.Create( this.fileInfo.FullName, settings );
				while( reader.Read() )
				{
					if( reader.IsStartElement() )
					{
						IAOElement element = XmlFile.CreateElement( reader.Name );
						element.ReadXml( reader );
						if( this.root == null )
							this.root = element;
						else
							Log.Error( "Multiple Root Elements: {0} (Root is {1})", element, this.root );
					}
				}
			}
			catch( XmlException ex )
			{
				Log.Debug( "[XmlFile.Read] XmlException: " + ex.ToString() );
			}
			finally
			{
				if( reader != null )
					reader.Close();
			}

			return true;
		}

		public bool Write( string fileName = null )
		{
			if( String.IsNullOrEmpty( fileName ) )
				fileName = this.fileInfo.FullName;

			if( String.IsNullOrEmpty( fileName ) )
			{
				Log.Error( "[XmlFile.Write] No filename specified!" );
				return false;
			}

			XmlWriter writer = null;
			try
			{
				XmlWriterSettings settings = new XmlWriterSettings();
				settings.Indent = true;
				settings.IndentChars = "    ";
				settings.OmitXmlDeclaration = true;
				writer = XmlWriter.Create( fileName, settings );
				this.root.WriteXml( writer );
			}
			catch( XmlException ex )
			{
				Log.Debug( "[XmlFile.Write] XmlException: " + ex.ToString() );
			}
			finally
			{
				if( writer != null )
					writer.Close();
			}
			return true;
		}

		public static string StripQuotes( string text )
		{
			if( String.IsNullOrEmpty( text ) )
				return String.Empty;
			return text.Trim( new char[] { '"', '\'' } );
		}

		public static string AddQuotes( string text )
		{
			return '"' + StripQuotes( text ) + '"';
		}

		public static IAOElement CreateElement( string name )
		{
			IAOElement element = null;
			switch( name )
			{
				case "Root":
					element = new RootElement();
					break;

				case "Archive":
					element = new ArchiveElement();
					break;

				case "Value":
					element = new ValueElement();
					break;

				case "TextLine":
					element = new TextLineElement();
					break;

				case "HTMLColor":
					element = new HtmlColorElement();
					break;

				default:
					element = new UnknownElement();
					break;
			}
			return element;
		}
	}
}