﻿using System;
using System.Xml;

namespace Twp.AO.Xml
{
	public class ValueElement : IAOElement
	{
		public ValueElement()
		{
			this.name = String.Empty;
			this.value = String.Empty;
		}

		public ValueElement( string name, string value )
		{
			this.name = name;
			this.value = value;
		}

		private string name;
		public string Name
		{
			get { return this.name; }
			set { this.name = value; }
		}

		private string value;
		public string Value
		{
			get { return this.value; }
			set { this.value = value; }
		}

		public bool HasChildren
		{
			get { return false; }
		}

		public void ReadXml( XmlReader reader )
		{
			if( !reader.HasAttributes )
				return;

			this.name = reader.GetAttribute( "name" );
			this.value = reader.GetAttribute( "value" );
		}

		public void WriteXml( XmlWriter writer )
		{
			writer.WriteStartElement( "Value" );
			writer.WriteAttributeString( "name", this.name );
			writer.WriteAttributeString( "value", this.value );
			writer.WriteEndElement();
		}

		public IAOElement GetElement( string name )
		{
			if( this.name == name )
				return this;
			return null;
		}

		public override string ToString()
		{
			return String.Format( "[Value] {0}={1}", this.name, this.value );
		}
	}
}
