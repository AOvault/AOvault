﻿// $Id: IgnoreList.cs 23 2011-06-01 07:55:24Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using Twp.Utilities;

namespace Twp.AO
{
	public class IgnoreList
	{
		public IgnoreList()
		{
		}

		private BinFile binFile = new BinFile();

		private List<uint> ids = new List<uint>();
		public List<uint> Ids
		{
			get { return this.ids; }
		}

		public void Read( string path )
		{
			this.ids.Clear();
			if( !this.binFile.Open( Twp.Utilities.Path.Combine( path, "IgnoreList.bin" ) ) )
				return;

			int count = this.binFile.GetInt();
			for( int n = 0; n < count; n++ )
			{
				this.ids.Add( this.binFile.GetUInt() );
			}
		}
	}
}
