﻿// $Id: Prefs.cs 23 2011-06-01 07:55:24Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.IO;

namespace Twp.AO
{
	public class Prefs
	{
		/// <summary>
		/// Checks the path for the existance of the "Prefs" folder.
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <returns>true if the path is valid, otherwise, false.</returns>
		public static bool IsValid( string path )
		{
			return File.Exists( Twp.Utilities.Path.Combine( path, "Prefs", "Prefs.xml" ) );
		}

		public static Xml.XmlFile Read( string path )
		{
			Xml.XmlFile xmlFile = new Xml.XmlFile();
			string filePath = Twp.Utilities.Path.Combine( path, "Prefs", "Prefs.xml" );
			if( !xmlFile.Read( filePath ) )
			{
				xmlFile = Gui.Default( path ).MainPrefs;
				xmlFile.FileInfo = new FileInfo( filePath );
			}
			return xmlFile;
		}

		/// <summary>
		/// Returns a string representing the folder [path]\\Prefs.
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <returns>a string representing the folder [path]\\Prefs.</returns>
		public static string GetPath( string path )
		{
			return Twp.Utilities.Path.Combine( path, "Prefs" );
		}
		
		public static List<Account> GetAccounts( string path )
		{
			if( String.IsNullOrEmpty( path ) )
				return null;

			DirectoryInfo di = new DirectoryInfo( GetPath( path ) );
			if( !di.Exists )
				return null;

			List<Account> accounts = new List<Account>();
			foreach( DirectoryInfo actDir in di.GetDirectories() )
			{
				Account account = new Account( actDir );
				accounts.Add( account );
			}
			return accounts;
		}
	}
}
