﻿// $Id: GamePath.cs 23 2011-06-01 07:55:24Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.IO;

namespace Twp.AO
{
	/// <summary>
	/// Provides static functions for AO path related tasks.
	/// </summary>
	public static class GamePath
	{
		/// <summary>
		/// Attempts to confirm the validity of an Anarchy Online installation.
		/// A Folder Browser dialog is presented repeatedly until the user either
		/// selects a valid path or clicks the "Cancel" button.
		/// </summary>
		/// <param name="path">A string representing the installation path. If this is not null orn an empty string,
		/// it is used to provide the dialog with a default selection.</param>
		/// <returns>true if a valid path is found, otherwise, false.</returns>
		public static bool Confirm( ref string path )
		{
			while( !IsValid( path ) )
			{
				if( Browse( ref path ) == false )
					return false;
			}
			return true;
		}

		/// <summary>
		/// Displays a <see cref="System.Windows.Forms.FolderBrowserDialog"/> to the user.
		/// No path validation is performed.
		/// </summary>
		/// <param name="path">A string representing the installation path. If this is not null orn an empty string,
		/// it is used to provide the dialog with a default selection.</param>
		/// <returns>true if a path was selected, otherwise, false.</returns>
		public static bool Browse( ref string path )
		{
			System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
			dialog.Description = "Select the folder where Anarchy Online is installed:";
			dialog.ShowNewFolderButton = false;
			if( !String.IsNullOrEmpty( path ) )
				dialog.SelectedPath = path;
			if( dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK )
			{
				path = dialog.SelectedPath;
				dialog.Dispose();
				return true;
			}
			dialog.Dispose();
			return false;
		}

		/// <summary>
		/// Checks the path for the existance of the "version.id" file.
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <returns>true if the path is valid, otherwise, false.</returns>
		public static bool IsValid( string path )
		{
			if( String.IsNullOrEmpty( path ) )
				return false;
			return File.Exists( Path.Combine( path, "version.id" ) );
		}
	}
}
