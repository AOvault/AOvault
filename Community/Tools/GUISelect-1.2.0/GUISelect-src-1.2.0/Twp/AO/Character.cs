﻿// $Id: Character.cs 23 2011-06-01 07:55:24Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.IO;

namespace Twp.AO
{
	public class Character
	{
		public Character( DirectoryInfo dirInfo )
		{
			this.dirInfo = dirInfo;
			string idStr = this.dirInfo.Name.Substring( 4 );
			this.id = Convert.ToUInt32( idStr );
		}

		private DirectoryInfo dirInfo;

		private uint id = 0;
		public uint ID
		{
			get { return this.id; }
		}

		private Chat chat;
		public Chat Chat
		{
			get
			{
				if( this.chat == null )
				{
					this.chat = new Chat();
					this.chat.Read( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "Chat" ) );
				}
				return this.chat;
			}
		}

		private Containers containers;
		public Containers Containers
		{
			get
			{
				if( this.containers == null )
				{
					this.containers = new Containers();
					this.containers.Read( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "Containers" ) );
				}
				return this.containers;
			}
		}

		private DockAreas dockAreas;
		public DockAreas DockAreas
		{
			get
			{
				if( this.dockAreas == null )
				{
					this.dockAreas = new DockAreas();
					this.dockAreas.Read( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "DockAreas" ) );
				}
				return this.dockAreas;
			}
		}

		private Xml.XmlFile prefs;
		public Xml.XmlFile Prefs
		{
			get
			{
				if( this.prefs == null )
				{
					this.prefs = new Xml.XmlFile();
					this.prefs.Read( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "Prefs.xml" ) );
				}
				return this.prefs;
			}
		}

		private CfgFile config;
		public CfgFile Config
		{
			get
			{
				if( this.config == null )
				{
					this.config = new CfgFile();
					this.config.Read( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "Char.cfg" ) );
				}
				return this.config;
			}
		}

		private IgnoreList ignoreList;
		public IgnoreList IgnoreList
		{
			get
			{
				if( this.ignoreList == null )
				{
					this.ignoreList = new IgnoreList();
					this.ignoreList.Read( this.dirInfo.FullName );
				}
				return this.ignoreList;
			}
		}

		private TextMacros textMacros;
		public TextMacros TextMacros
		{
			get
			{
				if( this.textMacros == null )
				{
					this.textMacros = new TextMacros();
					this.textMacros.Read( this.dirInfo.FullName );
				}
				return this.textMacros;
			}
		}
	}
}
