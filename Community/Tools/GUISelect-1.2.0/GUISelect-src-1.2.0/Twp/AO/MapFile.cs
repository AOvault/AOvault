﻿using System;
using System.Collections.Generic;
using System.IO;
using Twp.Utilities;

namespace Twp.AO
{
	public class MapFile : IEquatable<MapFile>
	{
		public MapFile()
		{
		}

		private FileInfo fileInfo;
		public FileInfo FileInfo
		{
			get { return this.fileInfo; }
		}

		private string name;
		public string Name
		{
			get { return this.name; }
		}

		private string type;
		public string Type
		{
			get { return this.type; }
		}

		private CfgFile config;
		public CfgFile Config
		{
			get { return this.config; }
		}

		private Xml.XmlFile coordsFile;
		public Xml.XmlFile CoordFile
		{
			get { return this.coordsFile; }
		}

		public bool IsValid
		{
			get
			{
				if( String.IsNullOrEmpty( this.name ) || String.IsNullOrEmpty( this.type ) )
					return false;
				else
					return true;
			}
		}

		public static MapFile Read( FileInfo fileInfo )
		{
			if( !fileInfo.Exists )
				return null;

			Log.Debug( "[MapFile.Read]", "File: {0}", fileInfo.FullName );

			MapFile mapFile = new MapFile();
			mapFile.fileInfo = fileInfo;
			mapFile.config = new CfgFile();
			if( mapFile.config.Read( fileInfo.FullName ) )
			{
				mapFile.name = mapFile.config.Nodes["Name"];
				mapFile.type = mapFile.config.Nodes["Type"];

				if( !String.IsNullOrEmpty( mapFile.config.Nodes["CoordsFile"] ) )
				{
					mapFile.coordsFile = new Xml.XmlFile();
					mapFile.coordsFile.Read( Twp.Utilities.Path.Combine( fileInfo.Directory.Parent.FullName,
														   mapFile.config.Nodes["CoordsFile"] ) );
				}
			}
			return mapFile;
		}

		public override string ToString()
		{
			return this.name;
		}

		#region IEquatable

		public bool Equals( MapFile other )
		{
			if( other == null )
				return false;
			return ( this.name == other.name );
		}

		public override int GetHashCode()
		{
			return this.name.GetHashCode();
		}

		public override bool Equals( object obj )
		{
			MapFile md = obj as MapFile;
			if( md != null )
			{
				return this.Equals( md );
			}
			string st = obj as string;
			if( st != null )
			{
				return this.name.Equals( st ) || String.Equals( this.fileInfo.FullName, st, StringComparison.OrdinalIgnoreCase );
			}
			return base.Equals( obj );
		}

		public static bool operator ==( MapFile md1, MapFile md2 )
		{
			if( object.ReferenceEquals( md1, md2 ) )
				return true;
			if( object.ReferenceEquals( md1, null ) )
				return false;
			if( object.ReferenceEquals( md2, null ) )
				return false;
			return md1.Equals( md2 );
		}

		public static bool operator !=( MapFile md1, MapFile md2 )
		{
			if( object.ReferenceEquals( md1, md2 ) )
				return false;
			if( object.ReferenceEquals( md1, null ) )
				return true;
			if( object.ReferenceEquals( md2, null ) )
				return true;
			return !md1.Equals( md2 );
		}

		#endregion
	}
}
