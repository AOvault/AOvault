﻿// $Id: Map.cs 23 2011-06-01 07:55:24Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Text;
using Twp.Utilities;
using System.IO;

namespace Twp.AO
{
	public class Map
	{
		private Xml.XmlFile xmlFile;
		private DirectoryInfo dirInfo;
		private Xml.IAOElement rkElement;
		private Xml.IAOElement slElement;

		internal Map()
		{
			this.xmlFile = null;
			this.dirInfo = null;
		}

		/// <summary>
		/// Creates a new instance of the <c ref="Twp.AO.Map">Map</c> class.
		/// </summary>
		/// <param name="xmlFile">The XmlFile containg the user's Prefs.xml data.</param>
		public Map( Xml.XmlFile xmlFile )
		{
			this.xmlFile = xmlFile;
			this.dirInfo = new DirectoryInfo( GetPath( this.xmlFile.FileInfo.Directory.Parent.FullName, "Default" ) );
		}

		/// <summary>
		/// Creates a new instance of the <c ref="Twp.AO.Map">Map</c> class.
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <param name="map">A string representing a Map folder.</param>
		public Map( string path, string map )
		{
			if( Gui.IsValid( path, map ) )
			{
				this.xmlFile = Prefs.Read( path );
				this.dirInfo = new DirectoryInfo( GetPath( path, map ) );
			}
		}

		/// <summary>
		/// Returns a string representing the folder [path]\\cd_image\\textures\\PlanetMap\\[map].
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <param name="map">A string representing a map folder.</param>
		/// <returns>a string representing the folder [path]\\cd_image\\textures\\PlanetMap\\[map].</returns>
		public static string GetPath( string path, string map )
		{
			return Twp.Utilities.Path.Combine( path, "cd_image", "textures", "PlanetMap", map );
		}

		/// <summary>
		/// Returns a string representing the file [path]\\cd_image\\textures\\PlanetMap\\[map]\\[file].
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <param name="map">A string representing a map folder.</param>
		/// <param name="file">A string representing a file name.</param>
		/// <returns>a string representing the file [path]\\cd_image\\textures\\PlanetMap\\[map]\\[file].</returns>
		public static string GetFile( string path, string map, string file )
		{
			return Twp.Utilities.Path.Combine( GetPath( path, map ), file );
		}

		/// <summary>
		/// Gets a list of maps that exist in the map base dir in relation to <paramref name="path"/>.
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <returns>An array holding the maps, or null if no maps were found.</returns>
		public static string[] List( string path )
		{
			DirectoryInfo di = new DirectoryInfo( Twp.Utilities.Path.Combine( path, "cd_image", "textures", "PlanetMap" ) );
			if( !di.Exists )
				return null;

			DirectoryInfo[] subs = di.GetDirectories();
			if( subs.Length > 0 )
			{
				string[] mapDirs = new string[subs.Length];
				for( int i = 0; i < subs.Length; ++i )
				{
					mapDirs[i] = subs[i].Name;
				}
				return mapDirs;
			}
			else
				return null;
		}

		/// <summary>
		/// Gets a list of map definition files that exist in the map dir in relation to <paramref name="path"/>.
		/// </summary>
		/// <param name="path">A string representing an Anarchy Online installation path.</param>
		/// <param name="map">A string representing a map folder.</param>
		/// <returns>An array holding the map files, or null if no map files were found.</returns>
		public static string[] GetFiles( DirectoryInfo dirInfo )
		{
			if( !dirInfo.Exists )
				return null;

			FileInfo[] files = dirInfo.GetFiles( "*.txt" );
			if( files.Length > 0 )
			{
				string[] mapFiles = new string[files.Length];
				for( int i = 0; i < files.Length; ++i )
				{
					mapFiles[i] = files[i].Name;
				}
				return mapFiles;
			}
			else
				return null;
		}

		private List<MapFile> files = null;
		public List<MapFile> Files
		{
			get
			{
				if( this.files == null )
				{
					this.files = new List<MapFile>();
					foreach( FileInfo fi in this.dirInfo.GetFiles( "*.txt" ) )
					{
						MapFile mapFile = MapFile.Read( fi );
						if( mapFile.IsValid )
							this.files.Add( mapFile );
					}
				}
				return this.files;
			}
		}

		/// <summary>
		/// Gets or sets the selected Rubi-Ka map in an account's Prefs.xml file in an
		/// Anarchy Online installation.
		/// </summary>
		public string RubiKa
		{
			get
			{
				if( this.rkElement == null )
				{
					if( this.xmlFile == null || this.xmlFile.Root == null )
						return null;

					this.rkElement = this.xmlFile.Root.GetElement( "PlanetMapIndexFile" );
					if( this.rkElement == null )
						return null;
				}
				return Xml.XmlFile.StripQuotes( this.rkElement.Value );
			}
			set
			{
				if( this.rkElement == null )
				{
					if( this.xmlFile == null || this.xmlFile.Root == null )
						return;

					this.rkElement = xmlFile.Root.GetElement( "PlanetMapIndexFile" );
					if( this.rkElement == null )
						return;
				}
				this.rkElement.Value = Xml.XmlFile.AddQuotes( value );
			}
		}

		/// <summary>
		/// Gets or sets the selected Rubi-Ka map in an account's Prefs.xml file in an
		/// Anarchy Online installation.
		/// </summary>
		public string Shadowlands
		{
			get
			{
				if( this.slElement == null )
				{
					if( this.xmlFile == null || this.xmlFile.Root == null )
						return null;

					this.slElement = this.xmlFile.Root.GetElement( "ShadowlandMapIndexFile" );
					if( this.slElement == null )
						return null;
				}
				return Xml.XmlFile.StripQuotes( this.slElement.Value );
			}
			set
			{
				if( this.slElement == null )
				{
					if( this.xmlFile == null || this.xmlFile.Root == null )
						return;

					this.slElement = xmlFile.Root.GetElement( "ShadowlandMapIndexFile" );
					if( this.slElement == null )
						return;
				}
				this.slElement.Value = Xml.XmlFile.AddQuotes( value );
			}
		}
	}
}
