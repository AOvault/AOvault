﻿// $Id: TextMacros.cs 23 2011-06-01 07:55:24Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using Twp.Utilities;

namespace Twp.AO
{
	public class TextMacros
	{
		public TextMacros()
		{
		}

		private BinFile binFile = new BinFile();

		private List<TextMacro> macros = new List<TextMacro>();
		public List<TextMacro> Macros
		{
			get { return this.macros; }
		}

		public void Read( string path )
		{
			this.macros.Clear();
			if( !this.binFile.Open( Twp.Utilities.Path.Combine( path, "TextMacro.bin" ) ) )
				return;

			int count = this.binFile.GetInt();
			for( int n = 0; n < count; n++ )
			{
				TextMacro macro = new TextMacro();
				macro.Id = this.binFile.GetInt();
				macro.Name = this.binFile.GetString();
				macro.Command = this.binFile.GetString();
				this.macros.Add( macro );
			}
		}
	}

	public class TextMacro
	{
		private int id;
		public int Id
		{
			get { return this.id; }
			set { this.id = value; }
		}

		private string name;
		public string Name
		{
			get { return this.name; }
			set { this.name = value; }
		}

		private string command;
		public string Command
		{
			get { return this.command; }
			set { this.command = value; }
		}

		public override string ToString()
		{
			return String.Format( "[ID::{0}] {1}: {2}", this.id, this.name, this.command );
		}
	}
}
