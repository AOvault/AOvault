﻿// $Id: Account.cs 23 2011-06-01 07:55:24Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.IO;

namespace Twp.AO
{
	public class Account
	{
		public Account( DirectoryInfo dirInfo )
		{
			this.dirInfo = dirInfo;
		}

		private DirectoryInfo dirInfo;

		public string Name
		{
			get { return this.dirInfo.Name; }
		}

		private Xml.XmlFile prefs = null;
		public Xml.XmlFile Prefs
		{
			get
			{
				if( this.prefs == null )
				{
					this.prefs = new Xml.XmlFile();
					this.prefs.Read( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "Prefs.xml" ) );
				}
				return this.prefs;
			}
		}

		private CfgFile config = null;
		public CfgFile Config
		{
			get
			{
				if( this.config == null )
				{
					this.config = new CfgFile();
					this.config.Read( Twp.Utilities.Path.Combine( this.dirInfo.FullName, "Login.cfg" ) );
				}
				return this.config;
			}
		}

		private List<Character> characters = null;
		public List<Character> Characters
		{
			get
			{
				if( this.characters == null )
				{
					this.characters = new List<Character>();
					DirectoryInfo[] charDirs = this.dirInfo.GetDirectories( "Char*" );
					foreach( DirectoryInfo charDir in charDirs )
					{
						Character character = new Character( charDir );
						this.characters.Add( character );
					}
				}
				return this.characters;
			}
		}
	}
}
