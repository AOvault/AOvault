﻿// $Id: Path.cs 31 2011-06-12 22:24:59Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.IO;
using System.Reflection;

namespace Twp.Utilities
{
	/// <summary>
	/// Various custom IO functions.
	/// </summary>
	public static class Path
	{
		/// <summary>
		/// Combines several path strings into one.
		/// </summary>
		/// <param name="paths">A string array containing path elements.</param>
		/// <returns>A string representing the combined path.</returns>
		/// <exception cref="ArgumentException"/>
		/// <exception cref="ArgumentNullException"/>
		public static string Combine( params string[] paths )
		{
			return String.Join( System.IO.Path.DirectorySeparatorChar.ToString(), paths );
		}

		/// <summary>
		/// Gets the application data directory for the company and product as defined the the assembly information of the entry assembly. 
		/// If the entry assembly is <c>null</c>, this method will fall back to the calling assembly to retrieve the information.
		/// If the folder does not exist, the folder is automatically created by this method. 
		/// 
		/// This method returns a value like [application data]\[company]\[product name].
		/// </summary>
		/// <returns>Directory for the application data.</returns>
		public static string GetAppDataPath()
		{
			Assembly assembly = Assembly.GetEntryAssembly();
			if( assembly == null )
				assembly = Assembly.GetCallingAssembly();
			string company = GetAssemblyAttributeValue( assembly, typeof( AssemblyCompanyAttribute ), "Company" );
			string product = GetAssemblyAttributeValue( assembly, typeof( AssemblyProductAttribute ), "Product" );
			return GetAppDataPath( company, product );
		}

		/// <summary>
		/// Gets the application data directory for a specific product. If the folder does not exist, the folder is automatically created by this method.
		/// 
		/// This method returns a value like [application data]\[product name].
		/// </summary>
		/// <param name="productName">Name of the product.</param>
		/// <returns>Directory for the application data.</returns>
		public static string GetAppDataPath( string productName )
		{
			return GetAppDataPath( string.Empty, productName );
		}

		/// <summary>
		/// Gets the application data directory for a specific product of a specific company. If the folder does not exist, the
		/// folder is automatically created by this method.
		/// 
		/// This method returns a value like [application data]\[company]\[product name].
		/// </summary>
		/// <param name="company">Name of the company.</param>
		/// <param name="product">Name of the product.</param>
		/// <returns>Directory for the application data.</returns>
		public static string GetAppDataPath( string company, string product )
		{
			string path = Combine( Environment.GetFolderPath( Environment.SpecialFolder.ApplicationData ), company, product );
			if( !Directory.Exists( path ) )
			{
				Directory.CreateDirectory( path );
			}
			return path;
		}

		/// <summary>
		/// Gets the specific <see cref="Attribute"/> value of the attribute type in the specified assembly.
		/// </summary>
		/// <param name="assembly">Assembly to read the information from.</param>
		/// <param name="attribute">Attribute to read.</param>
		/// <param name="property">Property to read from the attribute.</param>
		/// <returns>Value of the attribute or empty if the attribute is not found.</returns>
		public static string GetAssemblyAttributeValue( Assembly assembly, Type attribute, string property )
		{
			object[] attributes = assembly.GetCustomAttributes( attribute, false );

			if( attributes == null || attributes.Length == 0 )
				return String.Empty;

			object attributeValue = attributes[0];
			if( attributeValue == null )
				return String.Empty;

			Type attributeType = attributeValue.GetType();
			PropertyInfo propertyInfo = attributeType.GetProperty( property );
			if( propertyInfo == null )
				return String.Empty;

			object propertyValue = propertyInfo.GetValue( attributeValue, null );
			if( propertyValue == null )
				return String.Empty;

			return propertyValue.ToString();
		}

		/// <summary>
		/// Copies the content of a directory to another location, including subdirectories.
		/// </summary>
		/// <param name="source">A string representing the path of the source directory.</param>
		/// <param name="target">A string representing the path of the destination.</param>
		public static void Copy( string source, string target ) { Copy( source, target, true ); }

		/// <summary>
		/// Copies the content of a directory to another location, optionally including subdirectories.
		/// </summary>
		/// <param name="source">A string representing the path of the source directory.</param>
		/// <param name="target">A string representing the path of the destination.</param>
		/// <param name="recursive">If true, subdirectories are included; otherwise not.</param>
		public static void Copy( string source, string target, bool recursive )
		{
			DirectoryInfo dir = new DirectoryInfo( source );
			if( !dir.Exists )
			{
				throw new DirectoryNotFoundException( "Source directory not found: " + source );
			}

			if( !Directory.Exists( target ) )
			{
				if( OnIOEvent( String.Format( "Creating folder {0}.", target ) ) )
					return;
				Directory.CreateDirectory( target );
			}

			FileInfo[] files = dir.GetFiles();
			foreach( FileInfo file in files )
			{
				string newFile = Path.Combine( target, file.Name );
				if( OnIOEvent( String.Format( "Copying file {0} to {1}.", file.Name, newFile ) ) )
					return;
				file.CopyTo( newFile );
			}

			if( recursive )
			{
				DirectoryInfo[] subDirs = dir.GetDirectories();
				foreach( DirectoryInfo subDir in subDirs )
				{
					string newPath = Path.Combine( target, subDir.Name );
					Copy( subDir.FullName, newPath, recursive );
				}
			}
		}

		private static bool OnIOEvent( string message )
		{
			IOEventArgs e = new IOEventArgs( message );
			if( IOEvent != null )
				IOEvent( e );
			return e.Cancel;
		}

		public static event IOEventHandler IOEvent;

		public delegate void IOEventHandler( IOEventArgs e );
		public class IOEventArgs : EventArgs
		{
			public IOEventArgs( string message )
			{
				this.message = message;
			}

			private string message;
			public string Message
			{
				get { return this.message; }
			}

			private bool cancel;
			public bool Cancel
			{
				get { return this.cancel; }
				set { this.cancel = value; }
			}
		}
	}
}
