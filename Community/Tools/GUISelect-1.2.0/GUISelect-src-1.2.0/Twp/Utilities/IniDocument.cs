﻿// $Id: IniDocument.cs 21 2011-06-01 05:59:44Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;

namespace Twp.Utilities
{
	/// <summary>
	/// Represents an Ini-style settings file.
	/// </summary>
	public class IniDocument
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="IniDocument"/> class.
		/// </summary>
		public IniDocument() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="IniDocument"/> class with a specified file name and location.
		/// </summary>
		/// <param name="fileName">The name of the settings file.</param>
		/// <param name="path">The location of the settings file.</param>
		public IniDocument( string fileName, string path )
		{
			this.fileName = fileName;
			this.path = path;
			this.SetFullFileName();
		}

		/// <summary>
		/// Gets the <see cref="IniGroupTable"/> that holds the settings groups of this ini document.
		/// </summary>
		public IniGroupTable Groups
		{
			get { return this.groups; }
		}
		private IniGroupTable groups = new IniGroupTable();

		/// <summary>
		/// Gets or sets the IniGroup associated with the key <paramref name="name"/>.
		/// This is equivalent of using Groups[name] and is provided for convenience.
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public IniGroup this[string name]
		{
			get
			{
				if( !this.groups.ContainsKey( name ) )
					this.groups.Add( name, new IniGroup() );
				return this.groups[name];
			}
			set
			{
				if( this.groups[name] != value )
					this.groups[name] = value;
			}
		}

		/// <summary>
		/// Gets or sets the name of the settings file.
		/// </summary>
		public string FileName
		{
			get { return this.fileName; }
			set
			{
				this.fileName = value;
				this.SetFullFileName();
			}
		}
		private string fileName;

		/// <summary>
		/// Gets or sets the name of the path where the settings file is located.
		/// </summary>
		public string Path
		{
			get { return this.path; }
			set
			{
				this.path = value;
				this.SetFullFileName();
			}
		}
		private string path;

		private string fullFileName;

		private void SetFullFileName()
		{
			if( !String.IsNullOrEmpty( this.path ) && !String.IsNullOrEmpty( this.fileName ) )
				this.fullFileName = System.IO.Path.Combine( this.path, this.fileName );
			else
				this.fullFileName = this.fileName;
		}

		/// <summary>
		/// Sets the value of a group setting to a default value if no value is set.
		/// </summary>
		/// <param name="group">The name of the group.</param>
		/// <param name="key">The name of the setting.</param>
		/// <param name="value">The default value to set.</param>
		public void SetDefault( string group, string key, string value )
		{
			if( this.groups[group][key] == null )
				this.groups[group][key] = value;
		}

		/// <summary>
		/// Reads an ini-style settings file.
		/// </summary>
		/// <param name="fileName">The name of the settings file.</param>
		/// <exception cref="ArgumentException">fileName is an empty string ("").</exception>
		/// <exception cref="ArgumentNullException">fileName is null.</exception>
		/// <exception cref="FileNotFoundException">The file cannot be found.</exception>
		/// <exception cref="DirectoryNotFoundException">The specified path is invalid, such as being on an unmapped drive.</exception>
		/// <exception cref="IOException">An I/O error occurs.</exception>
		/// <exception cref="OutOfMemoryException">There is insufficient memory to allocate a buffer for the content.</exception>
		/// <exception cref="IniParseException">A Parsing error occured.</exception>
		public void Read( string fileName )
		{
			this.FileName = fileName;
			this.Read();
		}

		/// <summary>
		/// Reads an ini-style settings file.
		/// </summary>
		/// <param name="fileName">The name of the settings file.</param>
		/// <param name="path">The location of the settings file.</param>
		/// <exception cref="ArgumentException">fileName is an empty string ("").</exception>
		/// <exception cref="ArgumentNullException">fileName is null.</exception>
		/// <exception cref="FileNotFoundException">The file cannot be found.</exception>
		/// <exception cref="DirectoryNotFoundException">The specified path is invalid, such as being on an unmapped drive.</exception>
		/// <exception cref="IOException">An I/O error occurs.</exception>
		/// <exception cref="OutOfMemoryException">There is insufficient memory to allocate a buffer for the content.</exception>
		/// <exception cref="IniParseException">A Parsing error occured.</exception>
		public void Read( string fileName, string path )
		{
			this.FileName = fileName;
			this.Path = path;
			this.Read();
		}

		/// <summary>
		/// Reads an ini-style settings file.
		/// </summary>
		/// <exception cref="System.ArgumentException">FileName is an empty string ("").</exception>
		/// <exception cref="System.ArgumentNullException">FileName is null.</exception>
		/// <exception cref="System.IO.FileNotFoundException">The file cannot be found.</exception>
		/// <exception cref="System.IO.DirectoryNotFoundException">The specified path is invalid, such as being on an unmapped drive.</exception>
		/// <exception cref="System.IO.IOException">An I/O error occurs.</exception>
		/// <exception cref="System.OutOfMemoryException">There is insufficient memory to allocate a buffer for the content.</exception>
		/// <exception cref="Twp.Utilities.IniParseException">A Parsing error occured.</exception>
		public void Read()
		{
			if( String.IsNullOrEmpty( this.fullFileName ) )
				this.SetFullFileName();

			if( !File.Exists( this.fullFileName ) )
				return;

			using( StreamReader sr = new StreamReader( this.fullFileName ) )
			{
				this.groups.Clear();
				string line;
				string groupName = null;
				IniGroup group = null;
				int lineNumber = 0;
				while( ( line = sr.ReadLine() ) != null )
				{
					++lineNumber;

					if( line.StartsWith( "#", StringComparison.Ordinal ) || line.Trim() == String.Empty )
						continue;

					if( line.StartsWith( "[", StringComparison.Ordinal ) )
					{
						if( groupName != null && group != null )
							this.groups.Add( groupName, group );

						int end = line.LastIndexOf( ']' );
						if( end == -1 )
							throw new IniParseException( String.Format( "A Group start tag was found," +
																		" but the end tag is missing" +
																		" on line {0}.", lineNumber ),
														 line );

						groupName = line.Substring( 1, end - 1 ).Trim();
						if( groupName == string.Empty )
							groupName = null;
						else
							group = new IniGroup();
					}
					else if( line.Contains( "=" ) )
					{
						if( groupName == null || group == null )
							throw new IniParseException( String.Format( "A Settings line was found" +
																		" outside any groups on line {0}.",
																		lineNumber ),
														 line );

						string[] parts = line.Split( new char[] { '=' } );
						group.Add( parts[0].Trim(), parts[1].Trim() );
					}
					else
						throw new IniParseException( String.Format( "Unrecognized format in line {0}.",
																	lineNumber ),
													 line );
				}
				if( groupName != null && group != null )
					this.groups.Add( groupName, group );
			}
		}

		/// <summary>
		/// Writes an ini-style settings file.
		/// </summary>
		/// <exception cref="UnauthorizedAccessException">Access is denied.</exception>
		/// <exception cref="ArgumentException">FileName is en empty string ("").</exception>
		/// <exception cref="ArgumentNullException">FileName is null.</exception>
		/// <exception cref="FileNotFoundException">The file cannot be found.</exception>
		/// <exception cref="DirectoryNotFoundException">The specified path is invalid, such as being on an unmapped drive.</exception>
		/// <exception cref="IOException">An I/O error occurs.</exception>
		/// <exception cref="OutOfMemoryException">There is insufficient memory to allocate a buffer for the content.</exception>
		public void Write()
		{
			if( !String.IsNullOrEmpty( this.path ) && !Directory.Exists( this.path ) )
				Directory.CreateDirectory( this.path );

			if( String.IsNullOrEmpty( this.fullFileName ) )
				this.SetFullFileName();

			using( StreamWriter sw = new StreamWriter( this.fullFileName ) )
			{
				foreach( KeyValuePair<string, IniGroup> group in groups )
				{
					sw.WriteLine( "[{0}]", group.Key );
					foreach( KeyValuePair<string, string> item in group.Value )
					{
						sw.WriteLine( "{0}={1}", item.Key, item.Value );
					}
					sw.WriteLine();
				}
			}
		}
	}
}
