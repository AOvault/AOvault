﻿// $Id: IniGroupTable.cs 6 2011-05-19 09:41:20Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;

namespace Twp.Utilities
{
	/// <summary>
	/// Represents a table of Ini-style groups.
	/// </summary>
	public class IniGroupTable : Dictionary<string, IniGroup>
	{
		/// <summary>
		/// Gets or sets the group assöciated with the specified name.
		/// </summary>
		/// <param name="name">The name of the group.</param>
		/// <value>
		/// An <see cref="IniGroup"/> containing the settings for the named group.
		/// If no group of that name exists, a get operation will create a new empty <see cref="IniGroup"/>.
		/// </value>
		public new IniGroup this[string name]
		{
			get
			{
				if( !base.ContainsKey( name ) )
					base.Add( name, new IniGroup() );
				return base[name];
			}
			set
			{
				if( base[name] != value )
					base[name] = value;
			}
		}
	}
}
