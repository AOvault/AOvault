﻿// $Id: IniParseException.cs 6 2011-05-19 09:41:20Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Runtime.Serialization;

namespace Twp.Utilities
{
	/// <summary>
	/// Represents errors that occurs during Ini file parsing.
	/// </summary>
	[Serializable]
	public class IniParseException : Exception
	{
		/// <summary>
		/// Gets the content of the line in which the error occured.
		/// </summary>
		public string Line
		{
			get;
			set;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="IniParseException"/> class.
		/// </summary>
		public IniParseException() : base() { }

		/// <summary>
		/// Initializes a new instance of the <see cref="IniParseException"/> class with a specified error message.
		/// </summary>
		/// <param name="message">The message that descrives the error.</param>
		public IniParseException( string message ) : base( message ) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="IniParseException"/> class with a specified error message and source line.
		/// </summary>
		/// <param name="message">The message that descrives the error.</param>
		/// <param name="line">The line in which the error occured.</param>
		public IniParseException( string message, string line ) : base( message )
		{
			Line = line;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="IniParseException"/> class with a specified error message
		/// and a reference to the inner exception that is the cause of this error.
		/// </summary>
		/// <param name="message">The message that descrives the error.</param>
		/// <param name="innerException">The exception that is the cause of the current exception,
		///  or a null reference if no inner exception is specivied</param>
		public IniParseException( string message, Exception innerException ) : base( message, innerException ) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="IniParseException"/> class with serialized data.
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> that holds the serialized object data about the exception being thrown</param>
		/// <param name="context">The <see cref="StreamingContext"/> that contains contextual information about the source ordestination.</param>
		protected IniParseException( SerializationInfo info, StreamingContext context ) : base( info, context ) { }
	}
}
