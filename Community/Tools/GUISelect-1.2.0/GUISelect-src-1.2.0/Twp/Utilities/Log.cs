﻿// $Id: Log.cs 6 2011-05-19 09:41:20Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace Twp.Utilities
{
	/// <summary>
	/// Provides methods for writing information and event messages
	/// to a log file, as well as displaying them to the user.
	/// </summary>
	/// <example>
	/// <code>
	/// private static void Main(string[] args)
	/// {
	/// 	Log.Start("log.txt");
	/// 	Log.Warning("This is a warning message!");
	/// }
	/// </code>
	/// </example>
	public static class Log
	{
		private static string LogFile;

		/// <summary>
		/// Gets a value indicating whether the Log system has been started.
		/// </summary>
		public static bool Started
		{
			get { return started; }
		}
		private static bool started;

		/// <summary>
		/// Gets or sets a value indicating how debug info will be displayed.
		/// </summary>
		public static OutputType Output
		{
			get { return output; }
			set { output = value; }
		}
		private static OutputType output = OutputType.Trace;

		[Flags]
		public enum OutputType
		{
			None = 0,
			Trace = 1,
			Console = 2,
			Dialog = 4,
		}

		public enum LogLevel
		{
			Debug,
			Information,
			Warning,
			Error,
			Fatal,
		}

		public static event LoggedEventHandler Logged;

		/// <summary>
		/// Sets the file name to which the Log system should write to, and writes a start messsage to it.
		/// </summary>
		/// <param name="fileName">The name of the file used for writing.</param>
		/// <param name="path">The location of the file.</param>
		public static void Start( string fileName, string path )
		{
			Start( fileName, path, true );
		}

		/// <summary>
		/// Sets the file name to which the Log system should write to, and writes a start messsage to it.
		/// </summary>
		/// <param name="fileName">The name of the file used for writing.</param>
		/// <param name="path">The location of the file.</param>
		/// <param name="truncate">true if the file should be truncated, otherwise, false.</param>
		public static void Start( string fileName, string path, bool truncate )
		{
			if( !String.IsNullOrEmpty( path ) )
			{
				if( !Directory.Exists( path ) )
					Directory.CreateDirectory( path );
				LogFile = Path.Combine( path, fileName );
			}
			else
			{
				LogFile = fileName;
			}
			if( truncate )
			{
				if( File.Exists( LogFile ) )
					File.Delete( LogFile );
			}
			started = true;
			Add( LogLevel.Information, "Logger started." );
		}

		private static void Add( LogLevel level, string message )
		{
			if( !started )
				return;

			using( StreamWriter log = File.AppendText( LogFile ) )
			{
				log.Write( "[{0} {1}] ",
				          DateTime.Now.ToShortDateString(),
				          DateTime.Now.ToShortTimeString() );
				if( level != LogLevel.Information )
					log.Write( "[{0}] ", level );
				log.WriteLine( message );
				log.Flush();
				log.Close();
			}
		}

		private static void Show( LogLevel level, string message )
		{
			if( !started )
				return;

			if( Logged != null )
			{
				LoggedEventArgs e = new LoggedEventArgs( level, message );
				Logged( e );
				if( e.Cancel == true )
					return;
			}

			MessageBoxIcon icon = MessageBoxIcon.None;
			switch( level )
			{
				case LogLevel.Debug:
					icon = MessageBoxIcon.None;
					break;
				case LogLevel.Information:
					icon = MessageBoxIcon.Information;
					break;
				case LogLevel.Warning:
					icon = MessageBoxIcon.Warning;
					break;
				case LogLevel.Error:
					icon = MessageBoxIcon.Exclamation;
					break;
				case LogLevel.Fatal:
					icon = MessageBoxIcon.Error;
					break;
			}
			MessageBox.Show( message, level.ToString(), MessageBoxButtons.OK, icon );
		}

		/// <summary>
		/// Writes the text representation of the object to the Log file,
		/// as well as to the standard output stream.
		/// </summary>
		/// <param name="value">An <see cref="Object"/> whose name is written to the output.</param>
		[Conditional("DEBUG")]
		public static void Debug( object value )
		{
			Debug( value.ToString() );
		}

		/// <summary>
		/// Writes the text representation of the specified array of objects to the Log file,
		/// as well as the standard output stream, using the specified format information.
		/// </summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="args">An array of objects to write using format.</param>
		[Conditional("DEBUG")]
		public static void Debug( string format, params object[] args )
		{
			Debug( String.Format( format, args ) );
		}

		/// <summary>
		/// Writes a text message to the Log file, as well as the standard output stream,
		/// using the specified format information.
		/// </summary>
		/// <param name="message">The message to write.</param>
		[Conditional("DEBUG")]
		public static void Debug( string message )
		{
			Add( LogLevel.Debug, message );
			if( (output & OutputType.Trace) != 0 )
				System.Diagnostics.Trace.WriteLine( message );
			
			if( (output & OutputType.Console) != 0 )
				Console.WriteLine( message );

			if( Logged != null )
			{
				LoggedEventArgs e = new LoggedEventArgs( LogLevel.Debug, message );
				Logged( e );
				if( e.Cancel == true )
					return;
			}

			if( (output & OutputType.Dialog) != 0 )
				Show( LogLevel.Debug, message );
		}

		/// <summary>
		/// Writes the text representation of the object to the Log file.
		/// </summary>
		/// <param name="value">An <see cref="Object"/> whose name is written to the output.</param>
		public static void Information( object value )
		{
			Information( value.ToString() );
		}

		/// <summary>
		/// Writes the text representation of the specified array of objects
		/// to the Log file, as well as the standard output stream, using the specified format information.
		/// </summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="args">An array of objects to write using format.</param>
		public static void Information( string format, params object[] args )
		{
			Information( String.Format( format, args ) );
		}

		/// <summary>
		/// Writes a message to the Log file.
		/// </summary>
		/// <param name="message">The message to write.</param>
		public static void Information( string message )
		{
			Add( LogLevel.Information, message );
			Show( LogLevel.Information, message );
		}

		/// <summary>
		/// Writes the text representation of the object to the Log file.
		/// </summary>
		/// <param name="value">An <see cref="Object"/> whose name is written to the output.</param>
		public static void Warning( object value )
		{
			Warning( value.ToString() );
		}

		/// <summary>
		/// Writes the text representation of the specified array of objects
		/// to the Log file, as well as the standard output stream, using the specified format information.
		/// </summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="args">An array of objects to write using format.</param>
		public static void Warning( string format, params object[] args )
		{
			Warning( String.Format( format, args ) );
		}

		/// <summary>
		/// Writes a message to the Log file.
		/// </summary>
		/// <param name="message">The message to write.</param>
		public static void Warning( string message )
		{
			Add( LogLevel.Warning, message );
			Show( LogLevel.Warning, message );
		}

		/// <summary>
		/// Writes the text representation of the object to the Log file.
		/// </summary>
		/// <param name="value">An <see cref="Object"/> whose name is written to the output.</param>
		public static void Error( object value )
		{
			Error( value.ToString() );
		}

		/// <summary>
		/// Writes the text representation of the specified array of objects
		/// to the Log file, as well as the standard output stream, using the specified format information.
		/// </summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="args">An array of objects to write using format.</param>
		public static void Error( string format, params object[] args )
		{
			Error( String.Format( format, args ) );
		}

		/// <summary>
		/// Writes a message to the Log file.
		/// </summary>
		/// <param name="message">The message to write.</param>
		public static void Error( string message )
		{
			Add( LogLevel.Error, message );
			Show( LogLevel.Error, message );
		}

		/// <summary>
		/// Writes the text representation of the object to the Log file.
		/// The Application is then terminated.
		/// </summary>
		/// <param name="value">An <see cref="Object"/> whose name is written to the output.</param>
		public static void Fatal( object value )
		{
			Fatal( value.ToString() );
		}

		/// <summary>
		/// Writes the text representation of the specified array of objects
		/// to the Log file, as well as the standard output stream,
		/// using the specified format information.
		/// The Application is then terminated.
		/// </summary>
		/// <param name="format">A composite format string.</param>
		/// <param name="args">An array of objects to write using format.</param>
		public static void Fatal( string format, params object[] args )
		{
			Fatal( String.Format( format, args ) );
		}

		/// <summary>
		/// Writes a message to the Log file. The Application is then terminated.
		/// </summary>
		/// <param name="message">The message to write.</param>
		public static void Fatal( string message )
		{
			Add( LogLevel.Fatal, message );
			Show( LogLevel.Fatal, message );
			Process.GetCurrentProcess().Kill();
		}

		public delegate void LoggedEventHandler( LoggedEventArgs e );
		public class LoggedEventArgs : EventArgs
		{
			private readonly string message;
			private readonly LogLevel level;
			private bool cancel;

			public LoggedEventArgs( LogLevel level, string message )
			{
				this.level = level;
				this.message = message;
				this.cancel = false;
			}

			public LoggedEventArgs( LogLevel level, string message, bool cancel )
			{
				this.level = level;
				this.message = message;
				this.cancel = cancel;
			}

			public LogLevel Level
			{
				get { return this.level; }
			}

			public string Message
			{
				get { return this.message; }
			}

			public bool Cancel
			{
				get { return this.cancel; }
				set { this.cancel = true; }
			}
		}
	}
}
