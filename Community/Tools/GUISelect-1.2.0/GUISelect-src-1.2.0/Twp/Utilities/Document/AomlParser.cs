// $Id: AomlParser.cs 9 2011-05-20 16:19:41Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2010
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;

namespace Twp.Utilities
{
	public class AomlParser
	{
		private string text;
		
		public ISegment Parse( string text )
		{
			if( text == null )
				return null;
			this.text = text;
			
			

			return null;
		}
		
		private string GetNextTag()
		{
			return String.Empty;
		}

		private Color ParseColor( string xhtmlColor )
		{
			if( xhtmlColor.StartsWith( "#" ) )
				return ColorTranslator.FromHtml( xhtmlColor );
			else
				return Color.FromName( xhtmlColor );
		}

		public AomlParser()
		{
		}
	}
}
