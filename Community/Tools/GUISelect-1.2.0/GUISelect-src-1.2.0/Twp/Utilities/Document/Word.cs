﻿// $Id: Word.cs 9 2011-05-20 16:19:41Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2010
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Diagnostics;
using System.Drawing;

namespace Twp.Utilities
{
	/// <summary>
	/// Description of Word.
	/// </summary>
	public class Word
	{
		IDocument document;
		TextSegment segment;

		private int offset;
		public int Offset
		{
			get { return this.offset; }
		}

		private int length;
		public int Length
		{
			get { return this.length; }
		}
		
		private Color color;
		public Color Color
		{
			get { return this.color; }
		}
		
		private bool bold;
		public bool Bold
		{
			get { return this.bold; }
		}

		private bool italic;
		public bool Italic
		{
			get { return this.italic; }
		}

		private bool underline;
		public bool Underline
		{
			get { return this.underline; }
		}
		
		public string Text
		{
			get
			{
				if( document == null )
				{
					return String.Empty;
				}
				return document.GetText( this.segment.Offset + this.offset, this.length );
			}
		}

		public Word( IDocument document, TextSegment segment, int offset, int length,
		           	 Color color, bool bold, bool italic, bool underline )
		{
			Debug.Assert( document != null );
			Debug.Assert( segment != null );

			this.document = document;
			this.segment = segment;
			this.offset = offset;
			this.length = length;
			this.color = color;
			this.bold = bold;
			this.italic = italic;
			this.underline = underline;
		}
	}
}
