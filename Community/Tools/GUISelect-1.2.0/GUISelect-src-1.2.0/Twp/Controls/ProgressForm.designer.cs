﻿namespace Twp.Controls
{
	partial class ProgressForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing && ( components != null ) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.progressBar = new System.Windows.Forms.ProgressBar();
			this.progressLabel = new System.Windows.Forms.Label();
			this.totalProgressBar = new System.Windows.Forms.ProgressBar();
			this.totalProgressLabel = new System.Windows.Forms.Label();
			this.cancelButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// progressBar
			// 
			this.progressBar.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
						| System.Windows.Forms.AnchorStyles.Right ) ) );
			this.progressBar.Location = new System.Drawing.Point( 12, 67 );
			this.progressBar.Name = "progressBar";
			this.progressBar.Size = new System.Drawing.Size( 383, 23 );
			this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
			this.progressBar.TabIndex = 3;
			// 
			// progressLabel
			// 
			this.progressLabel.AutoSize = true;
			this.progressLabel.Location = new System.Drawing.Point( 12, 51 );
			this.progressLabel.Name = "progressLabel";
			this.progressLabel.Size = new System.Drawing.Size( 0, 13 );
			this.progressLabel.TabIndex = 2;
			// 
			// totalProgressBar
			// 
			this.totalProgressBar.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
						| System.Windows.Forms.AnchorStyles.Right ) ) );
			this.totalProgressBar.Location = new System.Drawing.Point( 12, 25 );
			this.totalProgressBar.Maximum = 10;
			this.totalProgressBar.Name = "totalProgressBar";
			this.totalProgressBar.Size = new System.Drawing.Size( 383, 23 );
			this.totalProgressBar.Step = 1;
			this.totalProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
			this.totalProgressBar.TabIndex = 1;
			// 
			// totalProgressLabel
			// 
			this.totalProgressLabel.AutoSize = true;
			this.totalProgressLabel.Location = new System.Drawing.Point( 12, 9 );
			this.totalProgressLabel.Name = "totalProgressLabel";
			this.totalProgressLabel.Size = new System.Drawing.Size( 0, 13 );
			this.totalProgressLabel.TabIndex = 0;
			// 
			// cancelButton
			// 
			this.cancelButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.cancelButton.Location = new System.Drawing.Point( 166, 98 );
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size( 75, 23 );
			this.cancelButton.TabIndex = 4;
			this.cancelButton.Text = "&Cancel";
			this.cancelButton.UseVisualStyleBackColor = true;
			this.cancelButton.Click += new System.EventHandler( this.cancelButton_Click );
			// 
			// ProgressForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size( 407, 130 );
			this.Controls.Add( this.cancelButton );
			this.Controls.Add( this.totalProgressLabel );
			this.Controls.Add( this.totalProgressBar );
			this.Controls.Add( this.progressLabel );
			this.Controls.Add( this.progressBar );
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "ProgressForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Progress...";
			this.ResumeLayout( false );
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ProgressBar progressBar;
		private System.Windows.Forms.Label progressLabel;
		private System.Windows.Forms.ProgressBar totalProgressBar;
		private System.Windows.Forms.Label totalProgressLabel;
		private System.Windows.Forms.Button cancelButton;
	}
}