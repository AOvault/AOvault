﻿// $Id: IToolBoxRenderer.cs 31 2011-06-12 22:24:59Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;

namespace Twp.Controls
{
	public interface IToolBoxRenderer : INotifyPropertyChanged
	{
		Rectangle GetBounds( Rectangle bounds );
		int CaptionHeight { get; set; }

		void DrawCaption( Graphics g, Rectangle bounds, string text, bool collapsed );
		void DrawCollapseButton( Graphics g, Rectangle bounds, bool collapsed, bool pressed, bool hover );
		void DrawBackground( Graphics g, Rectangle bounds );
	}
}
