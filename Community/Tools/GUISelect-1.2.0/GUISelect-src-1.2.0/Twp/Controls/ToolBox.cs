﻿// $Id: ToolBox.cs 31 2011-06-12 22:24:59Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace Twp.Controls
{
	/// <summary>
	/// The ToolBox class.
	/// </summary>
	[DefaultProperty( "Text" )]
	[Designer( typeof( Design.ToolBoxDesigner ), typeof( IDesigner ) )]
	public class ToolBox : Control
	{
		/// <summary>
		/// Creates a new instance of the ToolBox class.
		/// </summary>
		public ToolBox()
		{
			this.Size = new Size( 100, 100 );
			this.SetStyle( ControlStyles.ResizeRedraw, true );
			this.SetStyle( ControlStyles.ContainerControl, true );
			this.SetStyle( ControlStyles.SupportsTransparentBackColor, true );
			this.DoubleBuffered = true;
		}

		[Category( "Appearance" )]
		[Description( "The background color of the component." )]
		[DefaultValue( typeof( Color ), "Transparent" )]
		public override Color BackColor
		{
			get { return base.BackColor; }
			set { base.BackColor = value; }
		}

		/// <summary>
		/// Gets or sets the <see cref="IToolBoxRenderer"/> used by this ToolBox.
		/// </summary>
		[Category( "Appearance" )]
		[Description( "The renderer used to render the titlebar and background." )]
		public IToolBoxRenderer Renderer
		{
			get { return this.renderer; }
			set
			{
				if( this.renderer != value )
				{
					if( this.renderer != null )
						this.renderer.PropertyChanged -= new PropertyChangedEventHandler( this.OnPropertyChanged );
					this.renderer = value;
					if( this.renderer != null )
						this.renderer.PropertyChanged += new PropertyChangedEventHandler( this.OnPropertyChanged );
					this.Invalidate();
				}
			}
		}
		private IToolBoxRenderer renderer;

		void OnPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			this.PerformLayout();
			this.Refresh();
		}

		/// <summary>
		/// Gets the rectangle that represents the display area of the control.
		/// </summary>
		public override Rectangle DisplayRectangle
		{
			get
			{
				if( this.renderer == null )
					return base.DisplayRectangle;
				return this.renderer.GetBounds( base.DisplayRectangle );
			}
		}

		/// <summary>
		/// Gets or sets the title text of the ToolBox.
		/// </summary>
		[Browsable( true )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		[Description( "The title to display in the title bar of the ToolBox" )]
		public override string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				if( base.Text != value )
				{
					base.Text = value;
					this.Invalidate();
				}
			}
		}

		[Category( "Appearance" )]
		[Description( "Indicates whether the panel is in the collapsed state." )]
		[DefaultValue( false )]
		public bool Collapsed
		{
			get { return this.collapsed; }
			set
			{
				if( this.collapsed != value )
				{
					this.collapsed = value;
					this.OnCollapsedChanged();
				}
			}
		}
		private bool collapsed = false;

		public event EventHandler CollapsedChanged;

		protected virtual void OnCollapsedChanged()
		{
			if( this.CollapsedChanged != null )
				this.CollapsedChanged( this, EventArgs.Empty );

			this.CollapseExpand();
		}

		[Category( "Appearance" )]
		[Description( "Indicates whether the toolbox is allowed to collapse." )]
		[DefaultValue( false )]
		public bool AllowCollapse
		{
			get { return this.allowCollapse; }
			set
			{
				if( this.allowCollapse != value )
				{
					this.allowCollapse = value;
					this.OnAllowCollapseChanged();
				}
			}
		}
		private bool allowCollapse = false;

		public event EventHandler AllowCollapseChanged;

		protected virtual void OnAllowCollapseChanged()
		{
			if( this.AllowCollapseChanged != null )
				this.AllowCollapseChanged( this, EventArgs.Empty );

			this.CollapseExpand();
		}

		private void CollapseExpand()
		{
			bool collapsed = this.collapsed;
			if( !this.allowCollapse )
				collapsed = false;

			if( collapsed )
				this.Height = this.CollapsedHeight;
			else
				this.Height = this.ExpandedHeight;

			this.Invalidate();
		}

		private int CollapsedHeight
		{
			get
			{
				if( this.renderer != null )
					return this.renderer.CaptionHeight;
				else
					return 0;
			}
		}

		private int ExpandedHeight
		{
			get
			{
				int height = this.CollapsedHeight + 3;
				if( this.Controls.Count > 0 )
				{
					height = 0;
					int top = Int32.MaxValue;
					foreach( Control c in this.Controls )
					{
						if( height < c.Bottom )
							height = c.Bottom;
						if( top > c.Top )
							top = c.Top;
					}
					height += top - this.CollapsedHeight;
				}
				return height;
			}
		}

		private Rectangle CollapseBox
		{
			get
			{
				Rectangle rect = new Rectangle();
				int height = this.CollapsedHeight;

				if( height <= 17 )
				{
					rect.Size = new Size( height, height );
					rect.Location = new Point( this.Width - height, 0 );
				}
				else
				{
					int y = (height - 17) / 2;
					rect.Size = new Size( 17, 17 );
					rect.Location = new Point( this.Width - 17 - y, y );
				}
				return rect;
			}
		}

		protected override void OnMouseMove( MouseEventArgs e )
		{
			base.OnMouseMove( e );
			if( this.allowCollapse )
			{
				if( this.CollapseBox.Contains( e.Location ) )
					this.Hover = true;
				else
					this.Hover = false;
			}
		}

		protected override void OnMouseLeave( EventArgs e )
		{
			base.OnMouseLeave( e );
			if( this.allowCollapse )
			{
				if( this.hover )
					this.Hover = false;
			}
		}

		protected override void OnMouseDown( MouseEventArgs e )
		{
			base.OnMouseDown( e );
			if( this.allowCollapse )
			{
				if( this.CollapseBox.Contains( e.Location ) )
					this.Pressed = true;
			}
		}

		protected override void OnMouseUp( MouseEventArgs e )
		{
			base.OnMouseUp( e );
			if( this.allowCollapse )
			{
				if( this.CollapseBox.Contains( e.Location ) )
				{
					this.Pressed = false;
					this.Collapsed = !this.Collapsed;
				}
			}
		}

		protected override void OnMouseDoubleClick( MouseEventArgs e )
		{
			base.OnMouseDoubleClick( e );
			if( this.allowCollapse )
			{
				Rectangle rect = new Rectangle( 0, 0, this.Width, this.CollapsedHeight );
				if( rect.Contains( e.Location ) && !this.CollapseBox.Contains( e.Location ) )
				{
					this.Collapsed = !this.Collapsed;
				}
			}
		}

		private bool Hover
		{
			get { return this.hover; }
			set
			{
				if( this.hover != value )
				{
					this.hover = value;
					this.SetCursor();
					this.Refresh();
				}
			}
		}
		private bool hover = false;

		private bool Pressed
		{
			get { return this.pressed; }
			set
			{
				this.pressed = value;
				this.Refresh();
			}
		}
		private bool pressed = false;

		private void SetCursor()
		{
			if( this.hover )
				this.Cursor = Cursors.Hand;
			else
				this.Cursor = Cursors.Default;
		}

		protected override void OnPaint( PaintEventArgs e )
		{
			if( this.renderer == null )
			{
				base.OnPaint( e );
			}
			else
			{
				this.renderer.DrawCaption( e.Graphics, base.DisplayRectangle, this.Text, this.collapsed );
				if( this.allowCollapse )
					this.renderer.DrawCollapseButton( e.Graphics, this.CollapseBox, this.collapsed, this.pressed, this.hover );
				if( !this.allowCollapse || !this.collapsed )
					this.renderer.DrawBackground( e.Graphics, base.DisplayRectangle );
			}
		}
	}
}
