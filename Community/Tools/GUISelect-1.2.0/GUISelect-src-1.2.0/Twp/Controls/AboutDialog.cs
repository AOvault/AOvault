﻿// $Id: AboutDialog.cs 12 2011-05-27 19:27:20Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Windows.Forms;

namespace Twp.Controls
{
	/// <summary>
	/// Version string format used by the AboutDialog
	/// </summary>
	public enum VersionFormat
	{
		/// <summary>Shows full version string, i.e. 1.0.0.0</summary>
		Full,
		/// <summary>Shows simple version string, i.e. 1.0.0</summary>
		Simple,
		/// <summary>Shows detailed version string, i.e. 1.0.0 (rev 1)</summary>
		Detailed
	}

	/// <summary>
	/// Represents a Dialog that shows information about a program.
	/// </summary>
	//[ToolboxItem( true )]
	//[DesignTimeVisible( true )]
	[DefaultProperty( "VersionFormat" )]
	public partial class AboutDialog : Form
	{
		/// <summary>
		/// Gets or sets a value indicating the website url for the program.
		/// </summary>
		[Category( "Appearance" )]
		[Description( "The website URL for the program." )]
		public string Url
		{
			get { return this.urlBox.Text; }
			set { this.urlBox.Text = value; }
		}

		/// <summary>
		/// Gets or sets a value indicating the contact e-mail for the program.
		/// </summary>
		[Category( "Appearance" )]
		[Description( "The main contact e-mail for the program." )]
		public string Email
		{
			get { return this.emailBox.Text; }
			set { this.emailBox.Text = value; }
		}

		/// <summary>
		/// Gets or sets an image representing the program icon.
		/// This should be a 64x64 PNG file.
		/// </summary>
		[Category( "Appearance" )]
		[Description( "The program's icon. This should be a 64x64 PNG file." )]
		public System.Drawing.Image Image
		{
			get { return this.iconBox.Image; }
			set { this.iconBox.Image = value; }
		}

		/// <summary>
		/// Gets or sets an image representing the license for the program.
		/// This should be a 88x33 PNG file.
		/// </summary>
		[Category( "Apperance" )]
		[Description( "An image representing the license for the program. This should be a 88x33 PNG file." )]
		public System.Drawing.Image LicenseImage
		{
			get { return this.licenseIconBox.Image; }
			set { this.licenseIconBox.Image = value; }
		}
		
		/// <summary>
		/// Gets or sets a value representing the format of the version string.
		/// The default is <see cref="Twp.Controls.VersionFormat.Full"/>.
		/// </summary>
		[Category( "Behavior" )]
		[Description( "A value representing the format of the version string." )]
		[DefaultValue( VersionFormat.Full )]
		public VersionFormat VersionFormat
		{
			get { return this.versionFormat; }
			set
			{
				this.versionFormat = value;
				this.OnVersionFormatChanged();
			}
		}
		private VersionFormat versionFormat = VersionFormat.Full;

		[Browsable( false )]
		public override string Text
		{
			get { return base.Text; }
			set { base.Text = value; }
		}
		
		private Assembly assembly = null;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="AboutDialog"/> class.
		/// </summary>
		public AboutDialog()
		{
			InitializeComponent();
		}

		protected override void OnLoad( EventArgs e )
		{
			base.OnLoad( e );

			if( this.DesignMode )
				return;

			this.assembly = Assembly.GetEntryAssembly();
			this.Text = "About " + ( this.assembly.GetCustomAttributes( typeof( AssemblyTitleAttribute ), false )[0] as AssemblyTitleAttribute ).Title;
			this.productLabel.Text = ( this.assembly.GetCustomAttributes( typeof( AssemblyTitleAttribute ), false )[0] as AssemblyTitleAttribute ).Title;

			this.OnVersionFormatChanged();

			this.descriptionLabel.Text = ( this.assembly.GetCustomAttributes( typeof( AssemblyDescriptionAttribute ), false )[0] as AssemblyDescriptionAttribute ).Description;
			this.copyrightLabel.Text = ( this.assembly.GetCustomAttributes( typeof( AssemblyCopyrightAttribute ), false )[0] as AssemblyCopyrightAttribute ).Copyright;
		}
		
		private void OnVersionFormatChanged()
		{
			if( this.DesignMode )
				return;

			if( this.assembly == null )
				return;

			Version version = this.assembly.GetName().Version;
			switch( this.versionFormat )
			{
				case VersionFormat.Full:
					this.versionLabel.Text = "v" + version.ToString();
					break;
				case VersionFormat.Simple:
					this.versionLabel.Text = String.Format( "v{0}.{1}.{2}",
					                                       version.Major,
					                                       version.Minor,
					                                       version.Build );
					break;
				case VersionFormat.Detailed:
					this.versionLabel.Text = String.Format( "v{0}.{1}.{2} (rev {3})",
					                                       version.Major,
					                                       version.Minor,
					                                       version.Build,
					                                       version.Revision );
					break;
			}
		}

		private void OnLinkClicked( object sender, LinkLabelLinkClickedEventArgs e )
		{
			Process.Start( e.Link.LinkData as string );
		}
	}
}
