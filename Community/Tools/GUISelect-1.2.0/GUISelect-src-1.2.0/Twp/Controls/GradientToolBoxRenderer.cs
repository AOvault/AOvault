﻿// $Id: GradientToolBoxRenderer.cs 31 2011-06-12 22:24:59Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace Twp.Controls
{
	[ToolboxItem( true )]
	[DefaultProperty( "Font" )]
	public class GradientToolBoxRenderer : SimpleToolBoxRenderer
	{
		[Category( "Colors" )]
		[Description( "The background gradient color of the title bar." )]
		[DefaultValue( typeof( Color ), "GradientInactiveCaption" )]
		public Color GradientColor
		{
			get { return this.gradientColor; }
			set
			{
				if( this.gradientColor != value )
				{
					this.gradientColor = value;
					this.OnPropertyChanged( new PropertyChangedEventArgs( "GradientColor" ) );
				}
			}
		}
		private Color gradientColor = SystemColors.GradientInactiveCaption;

		public override void DrawCaption( Graphics g, Rectangle bounds, string text, bool collapsed )
		{
			Rectangle rect = bounds;
			rect.Height = this.CaptionHeight;

			g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;

			Point end = new Point( rect.Right, rect.Bottom );
			using( LinearGradientBrush brush = new LinearGradientBrush( rect.Location, end, this.BackColor, this.gradientColor ) )
			{
				g.FillRectangle( brush, rect );
			}

			rect.Height -= 1;
			rect.Width -= 1;
			if( this.TitleBorder )
			{
				using( Pen pen = new Pen( this.BorderColor ) )
				{
					g.DrawRectangle( pen, rect );
				}
			}

			if( !String.IsNullOrEmpty( text ) )
			{
				TextFormatFlags tff = TextFormatFlags.Left | TextFormatFlags.VerticalCenter | TextFormatFlags.LeftAndRightPadding;
				TextRenderer.DrawText( g, text, this.Font, rect, this.ForeColor, tff );
			}
		}
	}
}
