﻿// $Id: Line.cs 28 2011-06-05 22:36:54Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Twp.Controls
{
	/// <summary>
	/// Specifies the style used to paint a line.
	/// </summary>
	public enum LineStyle
	{
		/// <summary>The line has a sunken look.</summary>
		Sunken,
		/// <summary>The line has a raised look.</summary>
		Raised,
		/// <summary>The line has a flat look.</summary>
		Flat,
	}

	/// <summary>
	/// Description of Line.
	/// </summary>
	[DefaultProperty( "LineStyle" )]
	[DefaultEvent( "OrientationChanged" )]
	[Designer( typeof( Design.LineDesigner ), typeof( System.ComponentModel.Design.IDesigner ) )]
	public class Line : Control, INotifyPropertyChanged
	{
		public Line()
		{
			this.SetStyle( ControlStyles.ResizeRedraw, true );
			this.SetStyle( ControlStyles.SupportsTransparentBackColor, true );
			this.DoubleBuffered = true;
			this.BackColor = Color.Transparent;
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged( PropertyChangedEventArgs e )
		{
			if( this.PropertyChanged != null )
				this.PropertyChanged( this, e );
		}

		/// <summary>
		/// Gets or sets a value representing the orientation of the line.
		/// The default is Horizontal.
		/// </summary>
		[Category( "Appearance" )]
		[Description( "The orientation of the line." )]
		[DefaultValue( Orientation.Horizontal )]
		public Orientation Orientation
		{
			get { return this.orientation; }
			set
			{
				if( this.orientation != value )
				{
					this.orientation = value;

					switch( this.orientation )
					{
						case Orientation.Horizontal:
							this.Width = this.Height;
							break;
						case Orientation.Vertical:
							this.Height = this.Width;
							break;
					}

					this.OnResize( EventArgs.Empty );
					this.OnPropertyChanged( new PropertyChangedEventArgs( "Orientation" ) );
				}
			}
		}
		private Orientation orientation = Orientation.Horizontal;

		/// <summary>
		/// Gets or sets a value representing the style of the line.
		/// </summary>
		[Category( "Appearance" )]
		[Description( "The style of the line." )]
		[DefaultValue( LineStyle.Sunken )]
		public LineStyle LineStyle
		{
			get { return this.lineStyle; }
			set
			{
				if( this.lineStyle != value )
				{
					this.lineStyle = value;
					this.Invalidate();
					this.OnPropertyChanged( new PropertyChangedEventArgs( "LineStyle" ) );
				}
			}
		}
		private LineStyle lineStyle = LineStyle.Sunken;

		/// <summary>
		/// Gets or sets a value representing the padding on either side of the line.
		/// </summary>
		[Category( "Appearance" )]
		[Description( "The padding in either side of the line" )]
		[DefaultValue( 4 )]
		public int LinePadding
		{
			get { return this.linePadding; }
			set
			{
				if( this.linePadding != value )
				{
					this.linePadding = value;
					this.OnResize( EventArgs.Empty );
					this.OnPropertyChanged( new PropertyChangedEventArgs( "LinePadding" ) );
				}
			}
		}
		private int linePadding = 4;

		/// <summary>
		/// Raises the <see cref="System.Windows.Forms.Control.Paint"/> event.
		/// </summary>
		/// <param name="e"></param>
		protected override void OnPaint( PaintEventArgs e )
		{
			Point start = Point.Empty;
			Point end = Point.Empty;
			Color light = Color.Empty;
			Color dark = Color.Empty;
			switch( this.lineStyle )
			{
				case LineStyle.Sunken:
					light = Color.FromKnownColor( KnownColor.ButtonHighlight );
					dark = Color.FromKnownColor( KnownColor.ButtonShadow );
					break;
				case LineStyle.Raised:
					light = Color.FromKnownColor( KnownColor.ButtonShadow );
					dark = Color.FromKnownColor( KnownColor.ButtonHighlight );
					break;
				case LineStyle.Flat:
					dark = Color.FromKnownColor( KnownColor.ButtonShadow );
					break;
				default:
					return;
			}
			Rectangle rect = this.ClientRectangle;
			switch( this.orientation )
			{
				case Orientation.Horizontal:
					start = new Point( rect.X, rect.Y + this.linePadding );
					end = new Point( rect.X + rect.Width, rect.Y + this.linePadding );
					e.Graphics.DrawLine( new Pen( dark ), start, end );
					if( light != Color.Empty )
					{
						start.Offset( 0, 1 );
						end.Offset( 0, 1 );
						e.Graphics.DrawLine( new Pen( light ), start, end );
					}
					break;
				case Orientation.Vertical:
					start = new Point( rect.X + this.linePadding, rect.Y );
					end = new Point( rect.X + this.linePadding, rect.Y + rect.Height );
					e.Graphics.DrawLine( new Pen( dark ), start, end );
					if( light != Color.Empty )
					{
						start.Offset( 1, 0 );
						end.Offset( 1, 0 );
						e.Graphics.DrawLine( new Pen( light ), start, end );
					}
					break;
			}
		}

		protected override void OnResize( EventArgs e )
		{
			if( this.orientation == Orientation.Horizontal )
				this.Height = ( this.linePadding * 2 ) + 2;
			else
				this.Width = ( this.linePadding * 2 ) + 2;
			this.Invalidate();
		}

		[Browsable( false )]
		public override string Text
		{
			get { return base.Text; }
			set { base.Text = value; }
		}

		[Browsable( false )]
		public override Color BackColor
		{
			get { return base.BackColor; }
			set { base.BackColor = value; }
		}

		[Browsable( false )]
		public override Color ForeColor
		{
			get { return base.ForeColor; }
			set { base.ForeColor = value; }
		}

		[Browsable( false )]
		public override Font Font
		{
			get { return base.Font; }
			set { base.Font = value; }
		}

		[Browsable( false )]
		public override Image BackgroundImage
		{
			get { return base.BackgroundImage; }
			set { base.BackgroundImage = value; }
		}
	}
}
