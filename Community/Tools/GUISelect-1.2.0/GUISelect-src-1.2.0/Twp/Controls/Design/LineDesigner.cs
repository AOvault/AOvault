﻿// $Id: LineDesigner.cs 26 2011-06-05 18:50:18Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace Twp.Controls.Design
{
	internal class LineDesigner : ControlDesigner
	{
		private Line line;

		public LineDesigner()
		{
		}

		public override void Initialize( IComponent component )
		{
			base.Initialize( component );
			this.line = (Line) component;
		}

		public override SelectionRules SelectionRules
		{
			get
			{
				if( this.line.Orientation == Orientation.Horizontal )
				{
					return SelectionRules.Moveable | SelectionRules.Visible
						| SelectionRules.LeftSizeable | SelectionRules.RightSizeable;
				}
				else
				{
					return SelectionRules.Moveable | SelectionRules.Visible
						| SelectionRules.TopSizeable | SelectionRules.BottomSizeable;
				}
			}
		}
	}
}
