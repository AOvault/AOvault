﻿// $Id: ToolBoxDesigner.cs 31 2011-06-12 22:24:59Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace Twp.Controls.Design
{
	public class ToolBoxDesigner : ParentControlDesigner
	{
		ToolBox toolBox;
		DesignerActionListCollection actionLists;

		public ToolBoxDesigner()
		{
		}

		public override void Initialize( IComponent component )
		{
			base.Initialize( component );
			this.toolBox = (ToolBox) component;
		}

		private bool IsContained
		{
			get { return this.toolBox.Parent is ToolBoxContainer; }
		}

		protected override void PostFilterProperties( System.Collections.IDictionary properties )
		{
			base.PostFilterProperties( properties );
			if( this.IsContained )
			{
				properties.Remove( "Renderer" );
			}
		}

		public override SelectionRules SelectionRules
		{
			get
			{
				if( this.IsContained )
					return SelectionRules.TopSizeable | SelectionRules.BottomSizeable | SelectionRules.Visible;
				else
					return SelectionRules.AllSizeable | SelectionRules.Moveable | SelectionRules.Visible;
			}
		}

		public override DesignerActionListCollection ActionLists
		{
		    get
		    {
				if( this.IsContained )
					return base.ActionLists;

				if( this.actionLists == null )
		        {
		            this.actionLists = new DesignerActionListCollection();
		            this.actionLists.Add( new ToolBoxActionList( this.toolBox ) );
		        }
		        return this.actionLists;
		    }
		}

		internal class ToolBoxActionList : DesignerActionList
		{
			public ToolBoxActionList( IComponent component )
				: base( component )
			{
			}

			private void SetProperty( string name, object value )
			{
				PropertyDescriptor property = TypeDescriptor.GetProperties( this.Component )[name];
				property.SetValue( base.Component, value );
			}

			private ToolBox ToolBox
			{
				get { return (ToolBox) base.Component; }
			}

			public void ToggleDockStyle()
			{
				if( this.ToolBox.Dock != DockStyle.Fill )
					SetProperty( "Dock", DockStyle.Fill );
				else
					SetProperty( "Dock", DockStyle.None );
			}

			private string GetDockStyleText()
			{
				if( this.ToolBox.Dock == DockStyle.Fill )
					return "Undock in parent container";
				else
					return "Dock in parent container";
			}

			public override DesignerActionItemCollection GetSortedActionItems()
			{
				DesignerActionItemCollection items = new DesignerActionItemCollection();
				items.Add( new DesignerActionMethodItem( this, "ToggleDockStyle", this.GetDockStyleText() ) );
				return items;
			}
		}
	}
}
