﻿// $Id: ToolBoxContainerDesigner.cs 31 2011-06-12 22:24:59Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace Twp.Controls.Design
{
	public class ToolBoxContainerDesigner : ParentControlDesigner
	{
		ToolBoxContainer container;
		DesignerActionListCollection actionLists;

		public ToolBoxContainerDesigner()
		{
		}

		public override void Initialize( IComponent component )
		{
			base.Initialize( component );
			this.container = (ToolBoxContainer) component;
		}

		public override bool CanParent( Control control )
		{
			return control is ToolBox;
		}

		public override DesignerActionListCollection ActionLists
		{
			get
			{
				if( this.actionLists == null )
				{
					this.actionLists = new DesignerActionListCollection();
					this.actionLists.Add( new ToolBoxContainerActionList( this.container ) );
				}
				return this.actionLists;
			}
		}

		internal class ToolBoxContainerActionList : DesignerActionList
		{
			public ToolBoxContainerActionList( IComponent component )
				: base( component )
			{
			}

			private void SetProperty( string name, object value )
			{
				PropertyDescriptor property = TypeDescriptor.GetProperties( this.Component )[name];
				property.SetValue( base.Component, value );
			}

			private ToolBoxContainer Container
			{
				get { return (ToolBoxContainer) base.Component; }
			}

			public void ToggleDockStyle()
			{
				if( this.Container.Dock != DockStyle.Fill )
					SetProperty( "Dock", DockStyle.Fill );
				else
					SetProperty( "Dock", DockStyle.None );
			}

			private string GetDockStyleText()
			{
				if( this.Container.Dock == DockStyle.Fill )
					return "Undock in parent container";
				else
					return "Dock in parent container";
			}

			public override DesignerActionItemCollection GetSortedActionItems()
			{
				DesignerActionItemCollection items = new DesignerActionItemCollection();
				items.Add( new DesignerActionMethodItem( this, "ToggleDockStyle", this.GetDockStyleText() ) );
				return items;
			}
		}
	}
}
