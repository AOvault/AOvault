﻿// $Id: SimpleToolBoxRenderer.cs 31 2011-06-12 22:24:59Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Windows.Forms.VisualStyles;

namespace Twp.Controls
{
	[ToolboxItem( true )]
	[DefaultProperty( "Font" )]
	public class SimpleToolBoxRenderer : Component, IToolBoxRenderer
	{
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Gets the default font used by the <see cref="SimpleToolBoxRenderer"/>.
		/// </summary>
		private static Font DefaultFont = SystemFonts.SmallCaptionFont;

		/// <summary>
		/// Gets the default caption height used by the <see cref="SimpleToolBoxRenderer"/>
		/// </summary>
		private static int DefaultCaptionHeight = SystemInformation.ToolWindowCaptionHeight;

		[Category( "Appearance" )]
		[Description( "The font used to draw the title text." )]
		public Font Font
		{
			get { return this.font; }
			set
			{
				if( this.font != value )
				{
					this.font = value;
					this.OnPropertyChanged( new PropertyChangedEventArgs( "Font" ) );
				}
			}
		}
		private Font font = DefaultFont;

		private void ResetFont()
		{
			this.font = DefaultFont;
		}

		private bool ShouldSerializeFont()
		{
			return this.font != DefaultFont;
		}

		[Category( "Appearance" )]
		[Description( "The height of the caption bar." )]
		public int CaptionHeight
		{
			get { return this.captionHeight; }
			set
			{
				if( this.captionHeight != value )
				{
					this.captionHeight = value;
					this.OnPropertyChanged( new PropertyChangedEventArgs( "CaptionHeight" ) );
				}
			}
		}
		private int captionHeight = DefaultCaptionHeight;

		private void ResetCaptionHeight()
		{
			this.captionHeight = DefaultCaptionHeight;
		}

		private bool ShouldSerializeCaptionHeight()
		{
			return this.captionHeight != DefaultCaptionHeight;
		}


		/// <summary>
		/// Gets or sets the internal border style of the ToolBox.
		/// </summary>
		[Category( "Appearance" )]
		[Description( "The internal border style" )]
		[DefaultValue( BorderStyle.FixedSingle )]
		public BorderStyle BorderStyle
		{
			get { return this.borderStyle; }
			set
			{
				if( this.borderStyle != value )
				{
					this.borderStyle = value;
					this.OnPropertyChanged( new PropertyChangedEventArgs( "BorderStyle" ) );
				}
			}
		}
		private BorderStyle borderStyle = BorderStyle.FixedSingle;

		/// <summary>
		/// Gets or sets whether to show a border around the title bar.
		/// </summary>
		[Category( "Appearance" )]
		[Description( "Sets whether to show a border around the title bar." )]
		[DefaultValue( true )]
		public bool TitleBorder
		{
			get { return this.titleBorder; }
			set
			{
				if( this.titleBorder != value )
				{
					this.titleBorder = value;
					this.OnPropertyChanged( new PropertyChangedEventArgs( "TitleBorder" ) );
				}
			}
		}
		private bool titleBorder = true;

		[Category( "Colors" )]
		[Description( "The color of the title bar text." )]
		[DefaultValue( typeof( Color ), "InactiveCaptionText" )]
		public Color ForeColor
		{
			get { return this.foreColor; }
			set
			{
				if( this.foreColor != value )
				{
					this.foreColor = value;
					this.OnPropertyChanged( new PropertyChangedEventArgs( "ForeColor" ) );
				}
			}
		}
		private Color foreColor = SystemColors.InactiveCaptionText;

		[Category( "Colors" )]
		[Description( "The background color of the title bar." )]
		[DefaultValue( typeof( Color ), "InactiveCaption" )]
		public Color BackColor
		{
			get { return this.backColor; }
			set
			{
				if( this.backColor != value )
				{
					this.backColor = value;
					this.OnPropertyChanged( new PropertyChangedEventArgs( "BackColor" ) );
				}
			}
		}
		private Color backColor = SystemColors.InactiveCaption;

		[Category( "Colors" )]
		[Description( "The border color of the title bar, as well as the content in FixedSingle mode." )]
		[DefaultValue( typeof( Color ), "Black" )]
		public Color BorderColor
		{
			get { return this.borderColor; }
			set
			{
				if( this.borderColor != value )
				{
					this.borderColor = value;
					this.OnPropertyChanged( new PropertyChangedEventArgs( "BorderColor" ) );
				}
			}
		}
		private Color borderColor = Color.Black;

		protected virtual void OnPropertyChanged( PropertyChangedEventArgs e )
		{
			if( this.PropertyChanged != null )
				this.PropertyChanged( this, e );
		}

		public Rectangle GetBounds( Rectangle bounds )
		{
			Rectangle rect = bounds;
			rect.Height -= this.captionHeight;
			rect.Offset( 0, this.captionHeight );
			switch( this.borderStyle )
			{
				case BorderStyle.None:
					break;
				case BorderStyle.FixedSingle:
					rect.Inflate( -2, -2 );
					rect.Height -= 1;
					rect.Offset( 0, 1 );
					break;
				case BorderStyle.Fixed3D:
					rect.Inflate( -2, -2 );
					break;
			}
			return rect;
		}

		public virtual void DrawCaption( Graphics g, Rectangle bounds, string text, bool collapsed )
		{
			Rectangle rect = bounds;
			rect.Height = this.captionHeight;

			g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;

			using( SolidBrush brush = new SolidBrush( this.backColor ) )
			{
				g.FillRectangle( brush, rect );
			}
			rect.Height -= 1;
			rect.Width -= 1;
			if( this.titleBorder )
			{
				using( Pen pen = new Pen( this.borderColor ) )
				{
					g.DrawRectangle( pen, rect );
				}
			}

			if( !String.IsNullOrEmpty( text ) )
			{
				TextFormatFlags tff = TextFormatFlags.Left | TextFormatFlags.VerticalCenter | TextFormatFlags.LeftAndRightPadding;
				TextRenderer.DrawText( g, text, this.font, rect, this.foreColor, tff );
			}
		}

		public void DrawCollapseButton( Graphics g, Rectangle bounds, bool collapsed, bool pressed, bool hover )
		{
			g.SmoothingMode = SmoothingMode.HighQuality;
			Rectangle rect = bounds;
			rect.Inflate( -1, -1 );

			if( Application.RenderWithVisualStyles )
			{
				VisualStyleRenderer renderer;
				if( pressed )
					renderer = new VisualStyleRenderer( VisualStyleElement.Button.PushButton.Pressed );
				else if( hover )
					renderer = new VisualStyleRenderer( VisualStyleElement.Button.PushButton.Hot );
				else
					renderer = new VisualStyleRenderer( VisualStyleElement.Button.PushButton.Normal );
				renderer.DrawBackground( g, rect );
			}
			else
			{
				rect.Inflate( -1, -1 );
				if( pressed )
					ControlPaint.DrawButton( g, rect, ButtonState.Pushed );
				else if( hover )
					ControlPaint.DrawButton( g, rect, ButtonState.Normal );
				else
					ControlPaint.DrawButton( g, rect, ButtonState.Flat );
			}

			using( Pen pen = new Pen( this.foreColor, 1.25F ) )
			{
				rect.Inflate( -4, -4 );
				rect.Height -= 1;
				rect.Width -= 1;
				if( collapsed )
				{
					int hMiddle = rect.Left + rect.Width / 2;
					g.DrawLine( pen, hMiddle, rect.Top, hMiddle, rect.Bottom );
				}
				int vMiddle = rect.Top + rect.Height / 2;
				g.DrawLine( pen, rect.Left, vMiddle, rect.Right, vMiddle );
			}
		}

		public virtual void DrawBackground( Graphics g, Rectangle bounds )
		{
			Rectangle rect = this.GetBounds( bounds );
			switch( this.borderStyle )
			{
				case BorderStyle.None:
					if( this.DesignMode )
					{
						rect.Height -= 1;
						rect.Width -= 1;
						using( Pen pen = new Pen( SystemColors.ButtonShadow ) )
						{
							pen.DashStyle = DashStyle.Dash;
							g.DrawRectangle( pen, rect );
						}
					}
					break;
				case BorderStyle.FixedSingle:
					rect.Height -= 1;
					rect.Width -= 1;
					rect.Inflate( 2, 2 );
					using( Pen pen = new Pen( this.borderColor ) )
					{
						g.DrawRectangle( pen, rect );
					}
					break;
				case BorderStyle.Fixed3D:
					rect.Inflate( 2, 2 );
					ControlPaint.DrawBorder3D( g, rect, Border3DStyle.Sunken );
					break;
			}
		}
	}
}
