// $Id: InputDialog.cs 6 2011-05-19 09:41:20Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace Twp.Controls
{
	public class InputDialog : Form
	{
		public static string GetText( string title, string label )
		{
			return GetText( title, label, null, false, Size.Empty );
		}

		public static string GetText( string title, string label, bool hidden )
		{
			return GetText( title, label, null, hidden, Size.Empty );
		}

		public static string GetText( string title, string label, string text )
		{
			return GetText( title, label, text, false, Size.Empty );
		}

		public static string GetText( string title, string label, string text, bool hidden )
		{
			return GetText( title, label, text, hidden, Size.Empty );
		}

		public static string GetText( string title, string label, string text, bool hidden, Size size )
		{
			using( TextBox input = new TextBox() )
			{
				input.Text = text;
				input.UseSystemPasswordChar = hidden;
				using( InputDialog dialog = new InputDialog( input, title, label ) )
				{
					if( !size.IsEmpty )
						dialog.ClientSize = size;
					if( dialog.ShowDialog() == DialogResult.OK )
						return input.Text;
				}
			}
			return null;
		}

		public static decimal GetNumber( string title, string label )
		{
			return GetNumber( title, label, 0, 0, 100, 1, 0, Size.Empty );
		}

		public static decimal GetNumber( string title, string label, int decimals )
		{
			return GetNumber( title, label, 0, 0, 100, 1, decimals, Size.Empty );
		}

		public static decimal GetNumber( string title, string label, decimal value,
		                                 int minimum, int maximum, int increment )
		{
			return GetNumber( title, label, value, minimum, maximum, increment,
			                  0, Size.Empty );
		}

		public static decimal GetNumber( string title, string label, decimal value,
		                                 int minimum, int maximum, int increment,
		                                 int decimals )
		{
			return GetNumber( title, label, value, minimum, maximum, increment,
			                  decimals, Size.Empty );
		}

		public static decimal GetNumber( string title, string label, decimal value,
		                                 int minimum, int maximum, int increment,
		                                 int decimals, Size size )
		{
			using( NumericUpDown input = new NumericUpDown() )
			{
				input.Value = value;
				input.Minimum = minimum;
				input.Maximum = maximum;
				input.Increment = increment;
				input.DecimalPlaces = decimals;
				using( InputDialog dialog = new InputDialog( input, title, label ) )
				{
					if( !size.IsEmpty )
						dialog.ClientSize = size;
					if( dialog.ShowDialog() == DialogResult.OK )
						return input.Value;
				}
			}
			return -1;
		}

		public static string GetItem( string title, string label, string[] items )
		{
			return GetItem( title, label, items, 0, false, Size.Empty );
		}

		public static string GetItem( string title, string label, string[] items, int current )
		{
			return GetItem( title, label, items, current, false, Size.Empty );
		}

		public static string GetItem( string title, string label, string[] items, int current, bool editable )
		{
			return GetItem( title, label, items, current, editable, Size.Empty );
		}

		public static string GetItem( string title, string label, string[] items, int current, bool editable, Size size )
		{
			using( ComboBox input = new ComboBox() )
			{
				if( editable )
					input.DropDownStyle = ComboBoxStyle.DropDown;
				else
					input.DropDownStyle = ComboBoxStyle.DropDownList;
				input.Items.AddRange( items );
				input.SelectedIndex = current;
				using( InputDialog dialog = new InputDialog( input, title, label ) )
				{
					if( !size.IsEmpty )
						dialog.ClientSize = size;
					if( dialog.ShowDialog() == DialogResult.OK )
						return input.Text;
				}
			}
			return null;
		}

		private System.ComponentModel.IContainer components = null;

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if( components != null )
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}
		
		public InputDialog( Control inputControl, string title, string label )
		{
			Debug.Assert( inputControl != null );

			this.label = new Label();
			this.inputControl = inputControl;
			this.okButton = new Button();
			this.cancelButton = new Button();
			this.line = new Line();
			this.SuspendLayout();

			this.label.Anchor = AnchorStyles.Top | AnchorStyles.Bottom
				| AnchorStyles.Left | AnchorStyles.Right;
			this.label.Location = new Point( 8, 8 );
			this.label.Name = "label";
			this.label.Size = new Size( 235, 16 );
			this.label.TabIndex = 0;
			this.label.Text = label;

			this.inputControl.Anchor = AnchorStyles.Bottom | AnchorStyles.Left
				| AnchorStyles.Right;
			this.inputControl.Location = new Point( 8, 28 );
			this.inputControl.Name = "textBox";
			this.inputControl.Size = new Size( 235, 20 );
			this.inputControl.TabIndex = 1;

			this.line.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
			this.line.Location = new Point( 0, 58 );
			this.line.Name = "line";
			this.line.Size = new Size( 250, 2 );
			this.line.TabIndex = 2;

			this.okButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
			this.okButton.DialogResult = DialogResult.OK;
			this.okButton.Location = new Point( 88, 67 );
			this.okButton.Name = "okButton";
			this.okButton.Size = new Size( 75, 25 );
			this.okButton.TabIndex = 3;
			this.okButton.Text = "&OK";

			this.cancelButton.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
			this.cancelButton.DialogResult = DialogResult.Cancel;
			this.cancelButton.Location = new Point( 168, 67 );
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new Size( 75, 25 );
			this.cancelButton.TabIndex = 4;
			this.cancelButton.Text = "&Cancel";

			this.AcceptButton = this.okButton;
			this.CancelButton = this.cancelButton;
			this.ClientSize = new Size( 250, 100 );
			this.Controls.Add( this.cancelButton );
			this.Controls.Add( this.okButton );
			this.Controls.Add( this.line );
			this.Controls.Add( this.inputControl );
			this.Controls.Add( this.label );
			this.FormBorderStyle = FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "InputDialog";
			this.StartPosition = FormStartPosition.CenterParent;
			this.Text = title;
			this.ResumeLayout( false );
			this.PerformLayout();
		}

		private Label label;
		private Control inputControl;
		private Line line;
		private Button okButton;
		private Button cancelButton;
	}
}
