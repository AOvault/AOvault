﻿// $Id: LogWindow.cs 15 2011-05-27 19:54:14Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;

namespace Twp.Controls
{
	[ToolboxItem( true )]
	[DesignTimeVisible( true )]
	[DefaultProperty( "MaximumLines" )]
	public class LogWindow : Component, INotifyPropertyChanged
	{
		public LogWindow()
		{
			InitializeComponent();
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.listBox = new ListBox();
			this.form = new Form();
			this.form.SuspendLayout();
			// 
			// listBox
			// 
			this.listBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listBox.FormattingEnabled = true;
			this.listBox.HorizontalScrollbar = true;
			this.listBox.Location = new System.Drawing.Point( 0, 0 );
			this.listBox.Name = "listBox";
			this.listBox.Size = new System.Drawing.Size( 435, 89 );
			this.listBox.TabIndex = 0;
			// 
			// form
			// 
			this.form.ControlBox = false;
			this.form.Controls.Add( this.listBox );
			this.form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.form.Name = "LogWindow";
			this.form.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.form.Text = "Log";
			this.form.ResumeLayout( false );
		}

		private ListBox listBox;
		private Form form;

		#endregion

		[Category( "Behavior" )]
		[Description( "The maximum number of lines to display in the log window." )]
		[DefaultValue( 100 )]
		public int MaximumLines
		{
			get { return this.maximumLines; }
			set
			{
				if( this.maximumLines != value )
				{
					this.maximumLines = value;
					this.OnPropertyChanged( new PropertyChangedEventArgs( "MaximumLines" ) );
				}
			}
		}
		private int maximumLines = 100;

		[Browsable( false )]
		public Size Size
		{
			get { return this.form.Size; }
			set { this.form.Size = value; }
		}

		[Browsable( false )]
		public int Height
		{
			get { return this.form.Height; }
			set { this.form.Height = value; }
		}

		[Browsable( false )]
		public int Width
		{
			get { return this.form.Width; }
			set { this.form.Width = value; }
		}

		[Browsable( false )]
		public Point Location
		{
			get { return this.form.Location; }
			set { this.form.Location = value; }
		}

		public event PropertyChangedEventHandler PropertyChanged;
		protected virtual void OnPropertyChanged( PropertyChangedEventArgs e )
		{
			if( this.PropertyChanged != null )
				this.PropertyChanged( this, e );
		}

		public void AddMessage( string message )
		{
			this.listBox.Items.Add( message );
			if( listBox.Items.Count > 100 )
			{
				listBox.Items.RemoveAt( 0 );
			}
			listBox.SelectedIndex = listBox.Items.Count - 1;
			listBox.ClearSelected();
		}

		public void Show()
		{
			if( !this.DesignMode )
				this.form.Show();
		}
	}
}
