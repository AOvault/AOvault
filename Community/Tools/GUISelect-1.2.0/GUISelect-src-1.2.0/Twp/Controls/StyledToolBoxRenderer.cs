﻿// $Id: StyledToolBoxRenderer.cs 31 2011-06-12 22:24:59Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace Twp.Controls
{
	public enum GradientMode
	{
		None,
		Simple,
		Diagonal,
		Horizontal,
		Vertical,
	};

	[ToolboxItem( true )]
	[DefaultProperty( "Font" )]
	public class StyledToolBoxRenderer : Component, IToolBoxRenderer
	{
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Gets the default font used by the <see cref="StyledToolBoxRenderer"/>.
		/// </summary>
		private static Font DefaultFont = SystemFonts.SmallCaptionFont;

		/// <summary>
		/// Gets the default caption height used by the <see cref="SimpleToolBoxRenderer"/>
		/// </summary>
		private static int DefaultCaptionHeight = SystemInformation.ToolWindowCaptionHeight;

		[Category( "Appearance" )]
		[Description( "The font used to draw the caption text." )]
		public Font Font
		{
			get { return this.font; }
			set
			{
				if( this.font != value )
				{
					this.font = value;
					this.OnPropertyChanged( new PropertyChangedEventArgs( "Font" ) );
				}
			}
		}
		private Font font = DefaultFont;

		private void ResetFont()
		{
			this.font = DefaultFont;
		}

		private bool ShouldSerializeFont()
		{
			return this.font != DefaultFont;
		}

		[Category( "Appearance" )]
		[Description( "The height of the caption bar." )]
		public int CaptionHeight
		{
			get { return this.captionHeight; }
			set
			{
				if( this.captionHeight != value )
				{
					this.captionHeight = value;
					this.OnPropertyChanged( new PropertyChangedEventArgs( "TitleBarHeight" ) );
				}
			}
		}
		private int captionHeight = 17;

		private void ResetCaptionHeight()
		{
			this.captionHeight = DefaultCaptionHeight;
		}

		private bool ShouldSerializeCaptionHeight()
		{
			return this.captionHeight != DefaultCaptionHeight;
		}

		[Category( "Colors" )]
		[Description( "The color of the title bar text." )]
		[DefaultValue( typeof( Color ), "Control" )]
		public Color ForeColor
		{
			get { return this.foreColor; }
			set
			{
				if( this.foreColor != value )
				{
					this.foreColor = value;
					this.OnPropertyChanged( new PropertyChangedEventArgs( "ForeColor" ) );
				}
			}
		}
		private Color foreColor = SystemColors.Control;

		[Category( "Colors" )]
		[Description( "The background color of the title bar." )]
		[DefaultValue( typeof( Color ), "ControlDark" )]
		public Color TitleBackColor
		{
			get { return this.titleBackColor; }
			set
			{
				if( this.titleBackColor != value )
				{
					this.titleBackColor = value;
					this.OnPropertyChanged( new PropertyChangedEventArgs( "TitleBackColor" ) );
				}
			}
		}
		private Color titleBackColor = SystemColors.ControlDark;

		[Category( "Colors" )]
		[Description( "The background gradient color of the title bar." )]
		[DefaultValue( typeof( Color ), "ControlDarkDark" )]
		public Color TitleGradientColor
		{
			get { return this.titleGradientColor; }
			set
			{
				if( this.titleGradientColor != value )
				{
					this.titleGradientColor = value;
					this.OnPropertyChanged( new PropertyChangedEventArgs( "TitleGradientColor" ) );
				}
			}
		}
		private Color titleGradientColor = SystemColors.ControlDarkDark;

		[Category( "Colors" )]
		[Description( "The border color of the title bar." )]
		[DefaultValue( typeof( Color ), "ControlDarkDark" )]
		public Color TitleBorderColor
		{
			get { return this.titleBorderColor; }
			set
			{
				if( this.titleBorderColor != value )
				{
					this.titleBorderColor = value;
					this.OnPropertyChanged( new PropertyChangedEventArgs( "TitleBorderColor" ) );
				}
			}
		}
		private Color titleBorderColor = SystemColors.ControlDarkDark;

		[Category( "Colors" )]
		[Description( "The background color of the title bar." )]
		[DefaultValue( typeof( Color ), "ControlLightLight" )]
		public Color BackColor
		{
			get { return this.backColor; }
			set
			{
				if( this.backColor != value )
				{
					this.backColor = value;
					this.OnPropertyChanged( new PropertyChangedEventArgs( "BackColor" ) );
				}
			}
		}
		private Color backColor = SystemColors.ControlLightLight;

		[Category( "Colors" )]
		[Description( "The background gradient color of the title bar." )]
		[DefaultValue( typeof( Color ), "ControlLight" )]
		public Color GradientColor
		{
			get { return this.gradientColor; }
			set
			{
				if( this.gradientColor != value )
				{
					this.gradientColor = value;
					this.OnPropertyChanged( new PropertyChangedEventArgs( "GradientColor" ) );
				}
			}
		}
		private Color gradientColor = SystemColors.ControlLight;

		[Category( "Appearance" )]
		[Description( "The smoothing mode." )]
		[DefaultValue( SmoothingMode.HighQuality )]
		public SmoothingMode SmoothingMode
		{
			get { return this.smoothingMode; }
			set
			{
				if( this.smoothingMode != value )
				{
					this.smoothingMode = value;
					this.OnPropertyChanged( new PropertyChangedEventArgs( "SmoothingMode" ) );
				}
			}
		}
		private SmoothingMode smoothingMode = SmoothingMode.HighQuality;

		[Category( "Appearance" )]
		[Description( "Indicates how the title bar and background should be rendered." )]
		[DefaultValue( GradientMode.Vertical )]
		public GradientMode GradientMode
		{
			get { return this.gradientMode; }
			set
			{
				if( this.gradientMode != value )
				{
					this.gradientMode = value;
					this.OnPropertyChanged( new PropertyChangedEventArgs( "GradientMode" ) );
				}
			}
		}
		private GradientMode gradientMode = GradientMode.Vertical;

		[Category( "Appearance" )]
		[Description( "The padding of the client area." )]
		[DefaultValue( typeof( Padding ), "0, 0, 0, 0" )]
		public Padding Padding
		{
			get { return this.padding; }
			set
			{
				if( this.padding != value )
				{
					this.padding = value;
					this.OnPropertyChanged( new PropertyChangedEventArgs( "Padding" ) );
				}
			}
		}
		private Padding padding = new Padding( 0, 0, 0, 0 );

		protected virtual void OnPropertyChanged( PropertyChangedEventArgs e )
		{
			if( this.PropertyChanged != null )
				this.PropertyChanged( this, e );
		}

		public Rectangle GetBounds( Rectangle bounds )
		{
			Rectangle rect = bounds;
			rect.Height -= this.captionHeight;
			rect.Offset( 0, this.captionHeight );
			rect.Inflate( -1, -1 );
			rect.Size -= this.padding.Size;
			rect.Offset( this.padding.Left, this.padding.Top );
			return rect;
		}

		public virtual void DrawCaption( Graphics g, Rectangle bounds, string text, bool collapsed )
		{
			g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
			g.SmoothingMode = this.smoothingMode;

			Rectangle rect = bounds;
			rect.Height = this.captionHeight;
			if( (rect.Height & 1 ) == 1 )
				rect.Height -= 1;
			if( ( rect.Width & 1 ) == 1 )
				rect.Width -= 1;
			//rect.Offset( 1, 1 );
			Point end;
			switch( this.gradientMode )
			{
				case GradientMode.Diagonal:
					end = new Point( rect.Right, rect.Bottom );
					break;
				case GradientMode.Horizontal:
					end = new Point( rect.Right, rect.Top );
					break;
				case GradientMode.Vertical:
					end = new Point( rect.Left, rect.Bottom );
					break;
				default:
					end = Point.Empty;
					break;
			}
			RectangleCorners corners = RectangleCorners.Top;
			if( collapsed )
				corners = RectangleCorners.All;
			using( GraphicsPath path = RoundedRectangle.Create( rect, 3, corners ) )
			{
				if( end == Point.Empty )
				{
					using( SolidBrush brush = new SolidBrush( this.titleBackColor ) )
					{
						g.FillPath( brush, path );
					}
					if( this.gradientMode == Controls.GradientMode.Simple )
					{
						Rectangle backRect = rect;
						int halfHeight = backRect.Height / 2;
						backRect.Height -= halfHeight;
						backRect.Y += halfHeight;
						using( SolidBrush brush = new SolidBrush( this.titleGradientColor ) )
						{
							g.FillRectangle( brush, backRect );
						}
					}
				}
				else
				{
					using( LinearGradientBrush brush = new LinearGradientBrush( rect.Location, end, this.titleBackColor, this.titleGradientColor ) )
					{
						g.FillPath( brush, path );
					}
				}
				using( Pen pen = new Pen( this.titleBorderColor ) )
				{
					g.DrawPath( pen, path );
				}
			}

			if( !String.IsNullOrEmpty( text ) )
			{
				TextFormatFlags tff = TextFormatFlags.Left | TextFormatFlags.VerticalCenter | TextFormatFlags.LeftAndRightPadding;
				TextRenderer.DrawText( g, text, this.font, rect, this.foreColor, tff );
			}
		}

		public void DrawCollapseButton( Graphics g, Rectangle bounds, bool collapsed, bool pressed, bool hover )
		{
			g.SmoothingMode = SmoothingMode.HighQuality;
			Rectangle rect = bounds;
			rect.Inflate( -1, -1 );
			rect.Height -= 1;
			rect.Width -= 1;

			Color color = this.titleBackColor;
			if( pressed )
			{
				color = this.titleGradientColor;
			}

			if( hover )
			{
				using( GraphicsPath path = RoundedRectangle.Create( rect, 3 ) )
				{
					using( SolidBrush brush = new SolidBrush( color ) )
					{
						g.FillPath( brush, path );
					}
					using( Pen pen = new Pen( this.titleBorderColor ) )
					{
						g.DrawPath( SystemPens.ButtonShadow, path );
					}
				}
			}

			using( Pen pen = new Pen( this.foreColor, 1.6F ) )
			{
				rect.Inflate( -3, -3 );
				int middle = rect.Top + rect.Height / 2;
				Point[] top, bottom;
				if( collapsed )
				{
					top = new Point[] {
						new Point( rect.Left, rect.Top ),
						new Point( rect.Left + rect.Width / 2, middle ),
						new Point( rect.Right, rect.Top )
					};
					bottom = new Point[] {
						new Point( rect.Left, middle ),
						new Point( rect.Left + rect.Width / 2, rect.Bottom ),
						new Point( rect.Right, middle )
					};
				}
				else
				{
					top = new Point[] {
						new Point( rect.Left, rect.Bottom ),
						new Point( rect.Left + rect.Width / 2, middle ),
						new Point( rect.Right, rect.Bottom )
					};
					bottom = new Point[] {
						new Point( rect.Left, middle ),
						new Point( rect.Left + rect.Width / 2, rect.Top ),
						new Point( rect.Right, middle )
					};
				}
				g.DrawLines( pen, top );
				g.DrawLines( pen, bottom );
			}
		}

		public virtual void DrawBackground( Graphics g, Rectangle bounds )
		{
			g.SmoothingMode = this.smoothingMode;

			Rectangle rect = this.GetBounds( bounds );
			rect.Inflate( 1, 1 );
			rect.Size += this.padding.Size;
			rect.Offset( -this.padding.Left, -this.padding.Top );
			rect.Width -= 1;
			rect.Height -= 1;

			Point end;
			switch( this.gradientMode )
			{
				case GradientMode.Horizontal:
					end = new Point( rect.Right, rect.Top );
					break;
				case GradientMode.Vertical:
					end = new Point( rect.Left, rect.Bottom );
					break;
				case GradientMode.Diagonal:
					end = new Point( rect.Right, rect.Bottom );
					break;
				default:
					end = Point.Empty;
					break;
			}
			using( GraphicsPath path = RoundedRectangle.Create( rect, 2, RectangleCorners.Bottom ) )
			{
				if( end == Point.Empty )
				{
					using( SolidBrush brush = new SolidBrush( this.backColor ) )
					{
						g.FillPath( brush, path );
					}
				}
				else
				{
					using( LinearGradientBrush brush = new LinearGradientBrush( rect.Location, end, this.backColor, this.gradientColor ) )
					{
						g.FillPath( brush, path );
					}
				}
				using( Pen pen = new Pen( this.gradientColor ) )
				{
					g.DrawPath( pen, path );
				}
			}
		}
	}
}
