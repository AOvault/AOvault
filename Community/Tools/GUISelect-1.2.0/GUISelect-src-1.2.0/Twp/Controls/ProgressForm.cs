﻿// $Id: ProgressForm.cs 6 2011-05-19 09:41:20Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Windows.Forms;

namespace Twp.Controls
{
	public partial class ProgressForm : Form
	{
		public ProgressForm()
		{
			InitializeComponent();
		}

		public int Steps
		{
			get { return this.totalProgressBar.Maximum; }
			set { this.totalProgressBar.Maximum = value; }
		}

		public int Maximum
		{
			get { return this.progressBar.Maximum; }
			set
			{
				this.progressBar.Maximum = value;
				this.progressLabel.Text = String.Format( "0 of {0}", value );
				this.progressBar.Value = 0;
				this.count = 0;
			}
		}

		private bool canceled = false;
		public bool Canceled
		{
			get { return this.canceled; }
		}

		internal int count;

		public void SetupStep( string text, int size )
		{
			this.totalProgressBar.PerformStep();
			this.totalProgressLabel.Text = String.Format( "Step {0}: {1}", this.totalProgressBar.Value, text );
			this.progressBar.Step = size;
			this.progressBar.Value = 0;
			this.count = 0;
		}

		public void PerformStep()
		{
			this.count++;
			if( this.count >= this.progressBar.Step )
			{
				this.count = 0;
				int remains = this.progressBar.Maximum - this.progressBar.Value;
				if( this.progressBar.Step > remains )
					this.progressBar.Step = remains;
				this.progressBar.PerformStep();
				this.progressLabel.Text = String.Format( "{0} of {1}", this.progressBar.Value, this.progressBar.Maximum );
			}
		}

		private void cancelButton_Click( object sender, EventArgs e )
		{
			this.canceled = true;
		}
	}
}
