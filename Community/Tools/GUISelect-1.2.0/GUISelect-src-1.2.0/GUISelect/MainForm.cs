﻿// $Id: MainForm.cs 16 2011-06-01 07:56:49Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Windows.Forms;
using Twp.AO;
using Twp.AO.Xml;
using Twp.Controls;
using Twp.Utilities;

namespace GUISelect
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();

#if DEBUG
			this.Move += new EventHandler( this.OnMoved );
			this.Shown += new EventHandler( this.OnShown );
			Log.Logged += new Log.LoggedEventHandler( this.OnLogged );
#endif
		}

#if DEBUG
		LogWindow logWindow = new LogWindow();

		void OnLogged( Log.LoggedEventArgs e )
		{
			this.logWindow.AddMessage( e.Message );
		}

		private void OnMoved( object sender, EventArgs e )
		{
			System.Drawing.Point location = this.Location;
			location.X += this.Width;
			logWindow.Location = location;
			logWindow.Height = this.Height;
		}

		private void OnShown( object sender, EventArgs e )
		{
			logWindow.Show();
		}
#endif

		private XmlFile prefsFile;
		private Gui gui;

		internal void OnLoad( object sender, EventArgs e )
		{
			Program.Settings.Load();

			string path = Program.Settings.AOPath;
			if( GamePath.Confirm( ref path ) )
				Program.Settings.AOPath = path;
			else
				Application.Exit();

			this.UpdatePath( Program.Settings.AOPath );
		}

		internal void OnClosing( object sender, FormClosingEventArgs e )
		{
			Program.Settings.Save();
		}

		internal void OnAbout( object sender, EventArgs e )
		{
			using( AboutDialog about = new AboutDialog() )
			{
				about.Url = "http://guiselect.wrongplace.net/";
				about.Email = "mawerick@wrongplace.net";
				about.Image = Properties.Resources.Icon_64_2;
				about.LicenseImage = Properties.Resources.Icon_GPL;
#if DEBUG
				about.VersionFormat = VersionFormat.Detailed;
#else
				about.VersionFormat = VersionFormat.Simple;
#endif
				about.ShowDialog();
			}
		}

		internal void OnBrowsePath( object sender, EventArgs e )
		{
			string path = Program.Settings.AOPath;
			if( GamePath.Browse( ref path ) )
				this.UpdatePath( path );
		}

		private void OnGuiChanged( object sender, EventArgs e )
		{
			this.gui.Name = this.GuiList.SelectedItem.ToString();
			Log.Debug( "[Main.OnGuiChanged] Gui set to {0}", this.gui.Name );
			this.SaveButton.Enabled = true;
		}

		internal void OnSetSkinChanged( object sender, EventArgs e )
		{
			this.SaveButton.Enabled = true;
		}

		internal void OnSave( object sender, EventArgs e )
		{
			System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
			sw.Start();
			this.prefsFile.Write( /*"Test.xml"*/ );
			sw.Stop();
			Log.Debug( "[Main.OnSave] Prefs.xml written in {0}", sw.Elapsed );
			this.SaveButton.Enabled = false;
		}

		internal void UpdatePath( string path )
		{
			if( !GamePath.IsValid( path ) )
			{
				Log.Warning( "The selected folder is not a valid Anarchy Online installation." );
				this.AOPathEntry.Text = "**Error**";
				this.SetInput( false );
				return;
			}

			Program.Settings.AOPath = path;
			this.AOPathEntry.Text = path;
			this.prefsFile = Prefs.Read( path );
			this.gui = new Gui( this.prefsFile );

			this.GuiList.Items.Clear();
			string[] guiDirs = Gui.List( Program.Settings.AOPath );
			if( guiDirs != null )
			{
				this.GuiList.Items.AddRange( guiDirs );
				if( guiDirs.Length > 1 )
				{
					this.SetInput( true );
				}
				else
				{
					this.SetInput( false );
				}
				this.UpdateGui();
			}
			else
			{
				Log.Warning( "No GUI's found in the selected AO folder!" );
				this.SetInput( false );
			}
			this.SaveButton.Enabled = false;
		}

		internal void UpdateGui()
		{
			Log.Debug( "[Main.UpdateGui] GUI: {0}", this.gui.Name );
			if( !String.IsNullOrEmpty( this.gui.Name ) && this.GuiList.Items.Contains( this.gui.Name ) )
				this.GuiList.SelectedItem = this.gui.Name;
			else
				this.GuiList.SelectedItem = "Default";
		}

		internal void SetInput( bool enabled )
		{
			this.GuiLabel.Enabled = enabled;
			this.GuiList.Enabled = enabled;
		}

		static private void DebugXml( IAOElement element, string indent = "" )
		{
			Log.Debug( "{0}{1}", indent, element );
			if( element.HasChildren )
			{
				string subindent = indent + "  ";
				if( element is RootElement )
				{
					foreach( IAOElement node in ( element as RootElement ).Items )
					{
						DebugXml( node, subindent );
					}
				}
				else if( element is ArchiveElement )
				{
					foreach( IAOElement item in ( element as ArchiveElement ).Items )
					{
						DebugXml( item, subindent );
					}
				}
			}
		}
	}
}
