﻿// $Id: Program.cs 16 2011-06-01 07:56:49Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Threading;
using System.Windows.Forms;
using Twp.Controls;
using Twp.Utilities;

namespace GUISelect
{
	/// <summary>
	/// Class with program entry point.
	/// </summary>
	internal sealed class Program
	{
		/// <summary>
		/// Program entry point.
		/// </summary>
		[STAThread]
		private static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault( true );
			Application.ThreadException += new ThreadExceptionEventHandler( OnThreadException );
			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler( OnUnhandledException );
			Log.Start( "log.txt", Path.GetAppDataPath() );
			Application.Run( new MainForm() );
		}
		
		internal static Settings Settings = new Settings();

		private static void OnThreadException( object sender, ThreadExceptionEventArgs e )
		{
			ExceptionDialog.Show( e.Exception );
		}

		private static void OnUnhandledException( object sender, UnhandledExceptionEventArgs e )
		{
			ExceptionDialog.Show( e.ExceptionObject as Exception );
		}
	}
}
