=========
GUISelect
=========

GUISelect is a third-party tool that was made with the intent to help players
of Anarchy Online change their GUI quickly and easily.


Usage:
======

 - Install custom GUI's along side the default one.
   ** DO NOT make any changes to the default GUI folder!
 - In GUISelect, click the browse button to select your AO install.
   If a valid AO folder is selected, the GUI list is automatically populated.
 - Select your preferred GUI from the drop-down list.
 - Optionally, check the "Set skin" box, to make GUISelect also set the GUI
   graphics file to the one specified by the GUI.
   If you don't do this, you can manually select a GUI graphics file from the
   AO launcher.
 - Start AO and enjoy.

Installation:
=============

Installer:
 - Download the file "GUISelect-1.0.0.exe" to wherever you store your stuff.
 - Run the installer and follow the instructions given.

Manual installation:
 - Download the file "GUISelect-1.0.0.zip" to wherever you store your stuff.
 - Extract the content to a desired location
   (e.g. "C:\Program Files\GUISelect").
 - (Optional) Create a shortcut to the file "GUISelect.exe" on your desktop.


Links:
======

 http://www.wrongplace.net/            News site
 http://guiselect.wrongplace.net/      Project site
 http://aodevs.com/                    Release site for AO 3rd party tools


Credits:
========

Author: Mawerick (mawerick@wrongplace.net)
(Please write in English only)
