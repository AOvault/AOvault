﻿// MainForm.Designer.cs
//
//  Copyright (C) 2009 Mawerick
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; version 2 of the License.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
//
namespace GUISelect
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.AOPathEntry = new System.Windows.Forms.TextBox();
			this.AOPathButton = new System.Windows.Forms.Button();
			this.InfoLabel = new System.Windows.Forms.Label();
			this.SaveButton = new System.Windows.Forms.Button();
			this.SetSkinBox = new System.Windows.Forms.CheckBox();
			this.GuiList = new System.Windows.Forms.ComboBox();
			this.AOPathBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.AboutButton = new System.Windows.Forms.Button();
			this.ToolTip = new System.Windows.Forms.ToolTip( this.components );
			this.line1 = new Twp.Controls.Line();
			this.AOPathLabel = new System.Windows.Forms.Label();
			this.line2 = new Twp.Controls.Line();
			this.GuiLabel = new System.Windows.Forms.Label();
			this.line3 = new Twp.Controls.Line();
			this.SuspendLayout();
			// 
			// AOPathEntry
			// 
			this.AOPathEntry.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
						| System.Windows.Forms.AnchorStyles.Right ) ) );
			this.AOPathEntry.Location = new System.Drawing.Point( 12, 54 );
			this.AOPathEntry.Name = "AOPathEntry";
			this.AOPathEntry.ReadOnly = true;
			this.AOPathEntry.Size = new System.Drawing.Size( 206, 20 );
			this.AOPathEntry.TabIndex = 4;
			// 
			// AOPathButton
			// 
			this.AOPathButton.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right ) ) );
			this.AOPathButton.Image = global::GUISelect.Properties.Resources.Icon_Browse;
			this.AOPathButton.Location = new System.Drawing.Point( 224, 52 );
			this.AOPathButton.Name = "AOPathButton";
			this.AOPathButton.Size = new System.Drawing.Size( 24, 24 );
			this.AOPathButton.TabIndex = 5;
			this.AOPathButton.UseVisualStyleBackColor = true;
			this.AOPathButton.Click += new System.EventHandler( this.OnBrowsePath );
			// 
			// InfoLabel
			// 
			this.InfoLabel.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
						| System.Windows.Forms.AnchorStyles.Right ) ) );
			this.InfoLabel.Location = new System.Drawing.Point( 12, 9 );
			this.InfoLabel.Name = "InfoLabel";
			this.InfoLabel.Size = new System.Drawing.Size( 236, 16 );
			this.InfoLabel.TabIndex = 1;
			this.InfoLabel.Text = "Select a GUI for your AO installation:";
			this.InfoLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// SaveButton
			// 
			this.SaveButton.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left )
						| System.Windows.Forms.AnchorStyles.Right ) ) );
			this.SaveButton.Location = new System.Drawing.Point( 69, 176 );
			this.SaveButton.Name = "SaveButton";
			this.SaveButton.Size = new System.Drawing.Size( 122, 24 );
			this.SaveButton.TabIndex = 0;
			this.SaveButton.Text = "&Save";
			this.ToolTip.SetToolTip( this.SaveButton, "Saves the selected GUI name to your Prefs.xml file.\r\n(If no Prefs.xml file is fou" +
					"nd, the default GUI\'s MainPrefs.xml file is read instead, and saved as Prefs/Pre" +
					"fs.xml)" );
			this.SaveButton.UseVisualStyleBackColor = true;
			this.SaveButton.Click += new System.EventHandler( this.OnSave );
			// 
			// SetSkinBox
			// 
			this.SetSkinBox.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
						| System.Windows.Forms.AnchorStyles.Right ) ) );
			this.SetSkinBox.Enabled = false;
			this.SetSkinBox.Location = new System.Drawing.Point( 12, 136 );
			this.SetSkinBox.Name = "SetSkinBox";
			this.SetSkinBox.Size = new System.Drawing.Size( 236, 20 );
			this.SetSkinBox.TabIndex = 9;
			this.SetSkinBox.Text = "Also set GUI Skin file";
			this.ToolTip.SetToolTip( this.SetSkinBox, "This is only done if the selected GUI contains a valid GUISkin definition,\r\nand t" +
					"he defined skin file exists relative to the [AOPath]\\cd_image folder." );
			this.SetSkinBox.UseVisualStyleBackColor = true;
			this.SetSkinBox.CheckedChanged += new System.EventHandler( this.OnSetSkinChanged );
			// 
			// GuiList
			// 
			this.GuiList.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
						| System.Windows.Forms.AnchorStyles.Right ) ) );
			this.GuiList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.GuiList.Location = new System.Drawing.Point( 12, 109 );
			this.GuiList.Name = "GuiList";
			this.GuiList.Size = new System.Drawing.Size( 236, 21 );
			this.GuiList.Sorted = true;
			this.GuiList.TabIndex = 8;
			this.GuiList.SelectedIndexChanged += new System.EventHandler( this.OnGuiChanged );
			// 
			// AOPathBrowserDialog
			// 
			this.AOPathBrowserDialog.Description = "Please locate your AO install folder:";
			this.AOPathBrowserDialog.ShowNewFolderButton = false;
			// 
			// AboutButton
			// 
			this.AboutButton.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left ) ) );
			this.AboutButton.Location = new System.Drawing.Point( 12, 176 );
			this.AboutButton.Name = "AboutButton";
			this.AboutButton.Size = new System.Drawing.Size( 24, 24 );
			this.AboutButton.TabIndex = 11;
			this.AboutButton.Text = "?";
			this.AboutButton.UseVisualStyleBackColor = true;
			this.AboutButton.Click += new System.EventHandler( this.OnAbout );
			// 
			// line1
			// 
			this.line1.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
						| System.Windows.Forms.AnchorStyles.Right ) ) );
			this.line1.Location = new System.Drawing.Point( 0, 25 );
			this.line1.Name = "line1";
			this.line1.Size = new System.Drawing.Size( 260, 10 );
			this.line1.TabIndex = 2;
			// 
			// AOPathLabel
			// 
			this.AOPathLabel.Location = new System.Drawing.Point( 12, 35 );
			this.AOPathLabel.Name = "AOPathLabel";
			this.AOPathLabel.Size = new System.Drawing.Size( 100, 16 );
			this.AOPathLabel.TabIndex = 3;
			this.AOPathLabel.Text = "AO Install path:";
			this.AOPathLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
			// 
			// line2
			// 
			this.line2.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
						| System.Windows.Forms.AnchorStyles.Right ) ) );
			this.line2.Location = new System.Drawing.Point( 0, 80 );
			this.line2.Name = "line2";
			this.line2.Size = new System.Drawing.Size( 260, 10 );
			this.line2.TabIndex = 6;
			// 
			// GuiLabel
			// 
			this.GuiLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.GuiLabel.Location = new System.Drawing.Point( 12, 90 );
			this.GuiLabel.Name = "GuiLabel";
			this.GuiLabel.Size = new System.Drawing.Size( 100, 16 );
			this.GuiLabel.TabIndex = 7;
			this.GuiLabel.Text = "Selected GUI:";
			this.GuiLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
			// 
			// line3
			// 
			this.line3.Anchor = ( (System.Windows.Forms.AnchorStyles) ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left )
						| System.Windows.Forms.AnchorStyles.Right ) ) );
			this.line3.Location = new System.Drawing.Point( 0, 159 );
			this.line3.Name = "line3";
			this.line3.Size = new System.Drawing.Size( 260, 10 );
			this.line3.TabIndex = 10;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size( 260, 212 );
			this.Controls.Add( this.line3 );
			this.Controls.Add( this.SetSkinBox );
			this.Controls.Add( this.GuiLabel );
			this.Controls.Add( this.GuiList );
			this.Controls.Add( this.line2 );
			this.Controls.Add( this.AOPathButton );
			this.Controls.Add( this.AOPathEntry );
			this.Controls.Add( this.AOPathLabel );
			this.Controls.Add( this.line1 );
			this.Controls.Add( this.AboutButton );
			this.Controls.Add( this.SaveButton );
			this.Controls.Add( this.InfoLabel );
			this.DoubleBuffered = true;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = global::GUISelect.Properties.Resources.GUISelect2;
			this.Location = new System.Drawing.Point( 3, 0 );
			this.MaximizeBox = false;
			this.Name = "MainForm";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "GUISelect";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.OnClosing );
			this.Load += new System.EventHandler( this.OnLoad );
			this.ResumeLayout( false );
			this.PerformLayout();

		}
		private System.Windows.Forms.ComboBox GuiList;
		private System.Windows.Forms.Label GuiLabel;
		private Twp.Controls.Line line3;
		private Twp.Controls.Line line2;
		private System.Windows.Forms.Label AOPathLabel;
		private Twp.Controls.Line line1;
		private System.Windows.Forms.ToolTip ToolTip;
		private System.Windows.Forms.CheckBox SetSkinBox;
		private System.Windows.Forms.Button AboutButton;
		private System.Windows.Forms.Button SaveButton;
		private System.Windows.Forms.Label InfoLabel;
		private System.Windows.Forms.FolderBrowserDialog AOPathBrowserDialog;
		private System.Windows.Forms.Button AOPathButton;
		private System.Windows.Forms.TextBox AOPathEntry;
	}
}
