// $Id: Settings.cs 16 2011-06-01 07:56:49Z mawerick $
//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Windows.Forms;
using Twp.Controls;
using Twp.Utilities;

namespace GUISelect
{
	public class Settings
	{
		public Settings()
		{
			this.doc = new IniDocument( "GUISelect.ini", Path.GetAppDataPath() );
		}

		private IniDocument doc;

		public string AOPath
		{
			get { return this.doc.Groups["General"]["AOPath"]; }
			set { this.doc.Groups["General"]["AOPath"] = value; }
		}

		//public bool SetSkin
		//{
		//    get { return Convert.ToBoolean( this.doc.Groups["General"]["SetSkin"] ); }
		//    set { this.doc.Groups["General"]["SetSkin"] = value.ToString(); }
		//}

		public void Load()
		{
			try
			{
				this.doc.Read();
			}
			catch( IniParseException ex )
			{
				Log.Warning( ex.Message );
				Log.Debug( "[Settings] [Load]", ex.Line );
			}
			this.doc.SetDefault( "General", "AOPath", String.Empty );
			//this.doc.SetDefault( "General", "SetSkin", "true" );
		}

		public void Save()
		{
			try
			{
				this.doc.Write();
			}
			catch( Exception ex )
			{
				ExceptionDialog.Show( ex );
			}
		}
	}
}
