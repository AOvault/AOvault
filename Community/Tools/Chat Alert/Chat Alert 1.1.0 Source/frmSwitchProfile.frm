VERSION 5.00
Begin VB.Form frmSwitchProfile 
   Caption         =   "Chat Alert (Profiles)"
   ClientHeight    =   6615
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8835
   Icon            =   "frmSwitchProfile.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6615
   ScaleWidth      =   8835
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdExitWithoutSaving 
      Caption         =   "E&xit program without saving"
      Height          =   495
      Left            =   4710
      TabIndex        =   26
      Top             =   6000
      Width           =   2655
   End
   Begin VB.CommandButton cmdBrowse2 
      Caption         =   "Browse"
      Height          =   255
      Left            =   7920
      TabIndex        =   7
      Top             =   1920
      Width           =   855
   End
   Begin VB.CommandButton cmdBrowse3 
      Caption         =   "Browse"
      Height          =   255
      Left            =   7920
      TabIndex        =   11
      Top             =   2760
      Width           =   855
   End
   Begin VB.CommandButton cmdBrowse4 
      Caption         =   "Browse"
      Height          =   255
      Left            =   7920
      TabIndex        =   15
      Top             =   3600
      Width           =   855
   End
   Begin VB.CommandButton cmdBrowse5 
      Caption         =   "Browse"
      Height          =   255
      Left            =   7920
      TabIndex        =   19
      Top             =   4440
      Width           =   855
   End
   Begin VB.CommandButton cmdBrowse6 
      Caption         =   "Browse"
      Height          =   255
      Left            =   7920
      TabIndex        =   23
      Top             =   5400
      Width           =   855
   End
   Begin VB.CommandButton cmdBrowse1 
      Caption         =   "Browse"
      Height          =   255
      Left            =   7920
      TabIndex        =   3
      Top             =   1080
      Width           =   855
   End
   Begin VB.TextBox txtPathInUse 
      Height          =   285
      Left            =   240
      TabIndex        =   27
      Top             =   240
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.TextBox txtOptName5 
      Height          =   285
      Left            =   1800
      TabIndex        =   17
      Top             =   4080
      Width           =   3135
   End
   Begin VB.TextBox txtOptName6 
      Height          =   285
      Left            =   1800
      TabIndex        =   21
      Top             =   4920
      Width           =   3135
   End
   Begin VB.TextBox txtPath6 
      Height          =   285
      Left            =   120
      TabIndex        =   22
      Top             =   5400
      Width           =   7695
   End
   Begin VB.TextBox txtPath5 
      Height          =   285
      Left            =   120
      TabIndex        =   18
      Top             =   4440
      Width           =   7695
   End
   Begin VB.TextBox txtPath4 
      Height          =   285
      Left            =   120
      TabIndex        =   14
      Top             =   3600
      Width           =   7695
   End
   Begin VB.OptionButton Option6 
      Caption         =   "Profile #&6 A.K.A."
      Height          =   375
      Left            =   120
      TabIndex        =   20
      Top             =   4920
      Width           =   1575
   End
   Begin VB.TextBox txtPath3 
      Height          =   285
      Left            =   120
      TabIndex        =   10
      Top             =   2760
      Width           =   7695
   End
   Begin VB.TextBox txtPath2 
      Height          =   285
      Left            =   120
      TabIndex        =   6
      Top             =   1920
      Width           =   7695
   End
   Begin VB.TextBox txtOptName3 
      Height          =   285
      Left            =   1800
      TabIndex        =   9
      Top             =   2400
      Width           =   3135
   End
   Begin VB.TextBox txtOptName4 
      Height          =   285
      Left            =   1800
      TabIndex        =   13
      Top             =   3240
      Width           =   3135
   End
   Begin VB.TextBox txtOptName2 
      Height          =   285
      Left            =   1800
      TabIndex        =   5
      Top             =   1560
      Width           =   3135
   End
   Begin VB.OptionButton Option2 
      Caption         =   "Profile #&2 A.K.A."
      Height          =   375
      Left            =   120
      TabIndex        =   4
      Top             =   1560
      Width           =   1575
   End
   Begin VB.TextBox txtPath1 
      Height          =   285
      Left            =   120
      TabIndex        =   2
      Top             =   1080
      Width           =   7695
   End
   Begin VB.TextBox txtOptName1 
      Height          =   285
      Left            =   1800
      TabIndex        =   1
      Top             =   720
      Width           =   3135
   End
   Begin VB.OptionButton Option5 
      Caption         =   "Profile #&5 A.K.A."
      Height          =   375
      Left            =   120
      TabIndex        =   16
      Top             =   4080
      Width           =   1575
   End
   Begin VB.OptionButton Option4 
      Caption         =   "Profile #&4 A.K.A."
      Height          =   375
      Left            =   120
      TabIndex        =   12
      Top             =   3240
      Width           =   1575
   End
   Begin VB.OptionButton Option3 
      Caption         =   "Profile #&3 A.K.A."
      Height          =   375
      Left            =   120
      TabIndex        =   8
      Top             =   2400
      Width           =   1575
   End
   Begin VB.OptionButton Option1 
      Caption         =   "Profile #&1 A.K.A."
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   720
      Width           =   1575
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Height          =   495
      Left            =   1470
      TabIndex        =   25
      Top             =   6000
      Width           =   2655
   End
   Begin VB.Line Line6 
      X1              =   120
      X2              =   8760
      Y1              =   5760
      Y2              =   5760
   End
   Begin VB.Line Line5 
      X1              =   120
      X2              =   8760
      Y1              =   3960
      Y2              =   3960
   End
   Begin VB.Line Line4 
      X1              =   120
      X2              =   8760
      Y1              =   1440
      Y2              =   1440
   End
   Begin VB.Line Line3 
      X1              =   120
      X2              =   8760
      Y1              =   2280
      Y2              =   2280
   End
   Begin VB.Line Line2 
      X1              =   120
      X2              =   7800
      Y1              =   3120
      Y2              =   3120
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   8760
      Y1              =   4800
      Y2              =   4800
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Choose the profile you would like to use. "
      Height          =   375
      Left            =   683
      TabIndex        =   24
      Top             =   240
      Width           =   7215
   End
End
Attribute VB_Name = "frmSwitchProfile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdExitWithoutSaving_Click()

' Completely and totally exit.

  Dim Frm As Form
  For Each Frm In Forms
    Unload Frm
    Set Frm = Nothing
    Next Frm
  End

End Sub

Private Sub cmdOk_Click()
  
  On Error GoTo Errortrap
  
  If Option1.Value = True Then txtPathInUse.Text = txtPath1.Text
  If Option2.Value = True Then txtPathInUse.Text = txtPath2.Text
  If Option3.Value = True Then txtPathInUse.Text = txtPath3.Text
  If Option4.Value = True Then txtPathInUse.Text = txtPath4.Text
  If Option5.Value = True Then txtPathInUse.Text = txtPath5.Text
  If Option6.Value = True Then txtPathInUse.Text = txtPath6.Text
    
  txtPathInUse.Text = ReverseSlashes(txtPathInUse.Text)
    
  Const ForReading = 1, ForWriting = 2, ForAppending = 8
  Dim fso4, f4
  Set fso4 = CreateObject("Scripting.FileSystemObject")
  Set f4 = fso4.OpenTextFile("chatprofiles.txt", ForWriting, True)
  If Option1.Value = True Then f4.write "1" & vbCrLf
  If Option2.Value = True Then f4.write "2" & vbCrLf
  If Option3.Value = True Then f4.write "3" & vbCrLf
  If Option4.Value = True Then f4.write "4" & vbCrLf
  If Option5.Value = True Then f4.write "5" & vbCrLf
  If Option6.Value = True Then f4.write "6" & vbCrLf
  f4.write txtOptName1.Text & vbCrLf
  f4.write txtPath1.Text & vbCrLf
  f4.write txtOptName2.Text & vbCrLf
  f4.write txtPath2.Text & vbCrLf
  f4.write txtOptName3.Text & vbCrLf
  f4.write txtPath3.Text & vbCrLf
  f4.write txtOptName4.Text & vbCrLf
  f4.write txtPath4.Text & vbCrLf
  f4.write txtOptName5.Text & vbCrLf
  f4.write txtPath5.Text & vbCrLf
  f4.write txtOptName6.Text & vbCrLf
  f4.write txtPath6.Text & vbCrLf
  f4.Close
  
  Dim ValidPathFound As Boolean
  ValidPathFound = True
  If txtPathInUse.Text = "(Location not defined)" Then
    ValidPathFound = False
    MsgBox ("The log path in use is either invalid or was not found.  " & vbCrLf & vbCrLf & "If you need help locating a valid chat log path, please see the help file called " & """" & "How to Find a Chat Log Path" & """" & ".")
  End If
  
  Dim fso7, f7
  Set fso7 = CreateObject("Scripting.FileSystemObject")
  Set f7 = fso7.OpenTextFile(txtPathInUse.Text, ForReading)
  f7.Close

'MsgBox (ValidPathFound & " / " & txtPathInUse.Text)
If ValidPathFound = True Then
    frmMain.Show
    Unload Me
  End If

Errortrap:
'MsgBox (Err.Number)
  If Err.Number <> 0 Then
    If ValidPathFound <> False Then
      ValidPathFound = False
'  If Err.Number = 53 Then MsgBox ("The log path in use doesn't have a " & """" & "log.txt" & """" & " file in it.  This is usually because chat logging has not been enabled for this chat window." & vbCrLf & vbCrLf & "If you need help locating a valid chat log path, please see the help file called " & """" & "How to Find a Chat Log Path" & """" & ".")
'  If Err.Number = 76 Then MsgBox ("The log path in use is either invalid or was not found.  " & vbCrLf & vbCrLf & "If you need help locating a valid chat log path, please see the help file called " & """" & "How to Find a Chat Log Path" & """" & ".")
      MsgBox ("The log path in use is either invalid or was not found.  " & vbCrLf & vbCrLf & "If you need help locating a valid chat log path, please see the help file called " & """" & "How to Find a Chat Log Path" & """" & ".")
    End If
  End If
End Sub

Private Sub cmdBrowse1_Click()
frmBrowseForLocation!txtCurrentlyModifying.Text = "1"
frmBrowseForLocation.Enabled = True
frmBrowseForLocation.Visible = True
End Sub
Private Sub cmdBrowse2_Click()
frmBrowseForLocation!txtCurrentlyModifying.Text = "2"
frmBrowseForLocation.Enabled = True
frmBrowseForLocation.Visible = True
End Sub
Private Sub cmdBrowse3_Click()
frmBrowseForLocation!txtCurrentlyModifying.Text = "3"
frmBrowseForLocation.Enabled = True
frmBrowseForLocation.Visible = True
End Sub
Private Sub cmdBrowse4_Click()
frmBrowseForLocation!txtCurrentlyModifying.Text = "4"
frmBrowseForLocation.Enabled = True
frmBrowseForLocation.Visible = True
End Sub
Private Sub cmdBrowse5_Click()
frmBrowseForLocation!txtCurrentlyModifying.Text = "5"
frmBrowseForLocation.Enabled = True
frmBrowseForLocation.Visible = True
End Sub
Private Sub cmdBrowse6_Click()
frmBrowseForLocation!txtCurrentlyModifying.Text = "6"
frmBrowseForLocation.Enabled = True
frmBrowseForLocation.Visible = True
End Sub

Function ReverseSlashes(LineRead As String) As String

  Dim EventualString As String
  Dim CounterInString As Long

  CounterInString = 0
  EventualString = ""

  Do Until CounterInString = Len(LineRead)
    CounterInString = CounterInString + 1
    If Mid(LineRead, CounterInString, 1) <> "/" Then
      EventualString = EventualString & Mid(LineRead, CounterInString, 1)
    End If
    If Mid(LineRead, CounterInString, 1) = "/" Then
      EventualString = EventualString & "\"
    End If
  Loop
  ReverseSlashes = EventualString
End Function


Public Sub form_load()

  Dim WhichProfileToUse As String

  Option1.Value = False
  Option2.Value = False
  Option3.Value = False
  Option4.Value = False
  Option5.Value = False
  Option6.Value = False
  Const ForReading = 1, ForWriting = 2, ForAppending = 8
  Dim fso3, f3
  Set fso3 = CreateObject("Scripting.FileSystemObject")
  Set f3 = fso3.OpenTextFile("chatprofiles.txt", ForReading)
  WhichProfileToUse = f3.readline
  txtOptName1.Text = f3.readline
  txtPath1.Text = ReverseSlashes(f3.readline)
  txtOptName2.Text = f3.readline
  txtPath2.Text = ReverseSlashes(f3.readline)
  txtOptName3.Text = f3.readline
  txtPath3.Text = ReverseSlashes(f3.readline)
  txtOptName4.Text = f3.readline
  txtPath4.Text = ReverseSlashes(f3.readline)
  txtOptName5.Text = f3.readline
  txtPath5.Text = ReverseSlashes(f3.readline)
  txtOptName6.Text = f3.readline
  txtPath6.Text = ReverseSlashes(f3.readline)
  f3.Close
  Select Case WhichProfileToUse
    Case "1"
      txtPathInUse.Text = txtPath1.Text
      Option1.Value = True
    Case "2"
      txtPathInUse.Text = txtPath2.Text
      Option2.Value = True
    Case "3"
      txtPathInUse.Text = txtPath3.Text
      Option3.Value = True
    Case "4"
      txtPathInUse.Text = txtPath4.Text
      Option4.Value = True
    Case "5"
      txtPathInUse.Text = txtPath5.Text
      Option5.Value = True
    Case "6"
      txtPathInUse.Text = txtPath6.Text
      Option6.Value = True
  End Select
End Sub

Private Sub Option1_Click()
  txtPathInUse.Text = txtPath1.Text
End Sub
Private Sub Option2_Click()
  txtPathInUse.Text = txtPath2.Text
End Sub
Private Sub Option3_Click()
  txtPathInUse.Text = txtPath3.Text
End Sub
Private Sub Option4_Click()
  txtPathInUse.Text = txtPath4.Text
End Sub
Private Sub Option5_Click()
  txtPathInUse.Text = txtPath5.Text
End Sub
Private Sub Option6_Click()
  txtPathInUse.Text = txtPath6.Text
End Sub

