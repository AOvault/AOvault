VERSION 5.00
Begin VB.Form frmMain 
   Caption         =   "Chat Alert"
   ClientHeight    =   4935
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7125
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4935
   ScaleWidth      =   7125
   StartUpPosition =   2  'CenterScreen
   Begin VB.OptionButton optPlaySoundsOff 
      Caption         =   "Off"
      Height          =   255
      Left            =   5640
      TabIndex        =   20
      Top             =   2760
      Width           =   735
   End
   Begin VB.OptionButton optPlaySoundsOn 
      Caption         =   "On"
      Height          =   255
      Left            =   5640
      TabIndex        =   19
      Top             =   2400
      Width           =   735
   End
   Begin VB.CommandButton cmdXPDisplayReset 
      Caption         =   "Reset Statistics"
      Height          =   495
      Left            =   5280
      TabIndex        =   11
      Top             =   3480
      Width           =   1455
   End
   Begin VB.CommandButton cmdPauseorResume 
      Caption         =   "&Pause"
      Height          =   495
      Left            =   2000
      TabIndex        =   5
      Top             =   4320
      Width           =   1455
   End
   Begin VB.CommandButton cmdSwitchProfile 
      Caption         =   "&Switch Profile"
      Height          =   495
      Left            =   360
      TabIndex        =   4
      Top             =   4320
      Width           =   1455
   End
   Begin VB.CommandButton cmdAbout 
      Caption         =   "&About"
      Height          =   495
      Left            =   3640
      TabIndex        =   3
      Top             =   4320
      Width           =   1455
   End
   Begin VB.TextBox txtStatusText 
      Height          =   375
      Left            =   840
      TabIndex        =   2
      Top             =   960
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.Timer Timer1 
      Interval        =   100
      Left            =   6000
      Top             =   120
   End
   Begin VB.TextBox txtTextLineRead 
      Height          =   375
      Left            =   840
      TabIndex        =   1
      Top             =   360
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "E&xit"
      Height          =   495
      Left            =   5280
      TabIndex        =   0
      Top             =   4320
      Width           =   1455
   End
   Begin VB.Label lblPlaySounds 
      Alignment       =   2  'Center
      Caption         =   "Playing Sounds is Currently"
      Height          =   615
      Left            =   5400
      TabIndex        =   21
      Top             =   1920
      Width           =   1215
   End
   Begin VB.Line Line4 
      X1              =   5280
      X2              =   6720
      Y1              =   3120
      Y2              =   3120
   End
   Begin VB.Line Line3 
      X1              =   6720
      X2              =   6720
      Y1              =   1800
      Y2              =   3120
   End
   Begin VB.Line Line1 
      X1              =   5280
      X2              =   5280
      Y1              =   1800
      Y2              =   3120
   End
   Begin VB.Line Line2 
      X1              =   5280
      X2              =   6720
      Y1              =   1800
      Y2              =   1800
   End
   Begin VB.Label lblXPAveragePerHour 
      Alignment       =   2  'Center
      Caption         =   "0"
      Height          =   255
      Left            =   3760
      TabIndex        =   18
      Top             =   3720
      Width           =   1215
   End
   Begin VB.Label lblXPAveragePerMinute 
      Alignment       =   2  'Center
      Caption         =   "0"
      Height          =   255
      Left            =   3760
      TabIndex        =   17
      Top             =   3240
      Width           =   1215
   End
   Begin VB.Label lblXPorSK5 
      Caption         =   "Projected XP/SK Per Hour"
      Height          =   255
      Left            =   600
      TabIndex        =   16
      Top             =   3720
      Width           =   3135
   End
   Begin VB.Label lblXPorSK4 
      Caption         =   "Average XP/SK Per Minute"
      Height          =   255
      Left            =   600
      TabIndex        =   15
      Top             =   3240
      Width           =   3135
   End
   Begin VB.Label lblXPAverage 
      Alignment       =   2  'Center
      Caption         =   "0"
      Height          =   255
      Left            =   3760
      TabIndex        =   14
      Top             =   2760
      Width           =   1215
   End
   Begin VB.Label lblNumberOfXPGains 
      Alignment       =   2  'Center
      Caption         =   "0"
      Height          =   255
      Left            =   3760
      TabIndex        =   13
      Top             =   2280
      Width           =   1215
   End
   Begin VB.Label lblTotalXPGained 
      Alignment       =   2  'Center
      Caption         =   "0"
      Height          =   255
      Left            =   3760
      TabIndex        =   12
      Top             =   1800
      Width           =   1215
   End
   Begin VB.Label lblXPorSK3 
      Caption         =   "Average XP/SK per Gain"
      Height          =   255
      Left            =   600
      TabIndex        =   10
      Top             =   2760
      Width           =   3135
   End
   Begin VB.Label lblXPorSK2 
      Caption         =   "Number of times XP/SK gained"
      Height          =   255
      Left            =   600
      TabIndex        =   9
      Top             =   2280
      Width           =   3135
   End
   Begin VB.Label lblXPorSK1 
      Caption         =   "XP/SK gained during this session"
      Height          =   255
      Left            =   600
      TabIndex        =   8
      Top             =   1800
      Width           =   3135
   End
   Begin VB.Label labelAreWePausedOrResumed 
      Alignment       =   2  'Center
      Height          =   495
      Left            =   1095
      TabIndex        =   7
      Top             =   960
      Width           =   4935
   End
   Begin VB.Label labelReadingFromThisLog 
      Alignment       =   2  'Center
      Height          =   495
      Left            =   1155
      TabIndex        =   6
      Top             =   240
      Width           =   4815
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
  
'   Set up variables for use.
  Dim TimeCheckCalculationDone As Boolean
  Dim HowManyTextStringsAreThere As Long
  Dim StringToLookFor(90) As String
  Dim StrWav_To_Play(90) As String
  Dim SizeOfFile As Long
  Dim CurrentSizeOfFile As Long
  Dim Counter As Long
  Dim TextStringRead As String
  Dim XPorSKGainedOverall As Long
  Dim XPorSKGainedOverallVersusTime As Long
  Dim HowManyMinutesHavePassed As Long
  
  Const FoundXP = 1, FoundSK = 2, FoundNothing = 3
  Const ForReading = 1, ForWriting = 2, ForAppending = 8
  Dim fso, f

Private Sub cmdModifySounds_Click()
  frmModifySounds.Show
  Unload Me
End Sub

Private Sub cmdPauseorResume_Click()
  If Timer1.Enabled = False Then
    Timer1.Enabled = True
    cmdPauseorResume.Caption = "&Pause"
    labelAreWePausedOrResumed = "Log file reading is currently on." & vbCrLf & "Press the Pause key below to halt reading from the log file."
      Else
    Timer1.Enabled = False
    cmdPauseorResume.Caption = "&Resume"
    labelAreWePausedOrResumed = "Log file reading is currently paused." & vbCrLf & "Press the Resume key below to turn log file reading back on."
  End If
End Sub

Public Function CommaValue(ByVal StringToComma As String) As String

  Dim CommaCounter As Long
  
  CommaCounter = Len(StringToComma)

  Select Case CommaCounter

  Case 0, 1, 2, 3
    CommaValue = StringToComma
  Case 4
    CommaValue = Mid(StringToComma, 1, 1) & "," & Mid(StringToComma, 2, 3)
  Case 5
    CommaValue = Mid(StringToComma, 1, 2) & "," & Mid(StringToComma, 3, 3)
  Case 6
    CommaValue = Mid(StringToComma, 1, 3) & "," & Mid(StringToComma, 4, 3)
  Case 7
    CommaValue = Mid(StringToComma, 1, 1) & "," & Mid(StringToComma, 2, 3) & "," & Mid(StringToComma, 5, 3)
  Case 8
    CommaValue = Mid(StringToComma, 1, 2) & "," & Mid(StringToComma, 3, 3) & "," & Mid(StringToComma, 6, 3)
  Case 9
    CommaValue = Mid(StringToComma, 1, 3) & "," & Mid(StringToComma, 4, 3) & "," & Mid(StringToComma, 7, 3)
  Case 10
    CommaValue = Mid(StringToComma, 1, 1) & "," & Mid(StringToComma, 2, 3) & "," & Mid(StringToComma, 5, 3) & "," & Mid(StringToComma, 8, 3)
  Case 11
    CommaValue = Mid(StringToComma, 1, 2) & "," & Mid(StringToComma, 3, 3) & "," & Mid(StringToComma, 6, 3) & "," & Mid(StringToComma, 9, 3)
  Case 12
    CommaValue = Mid(StringToComma, 1, 3) & "," & Mid(StringToComma, 4, 3) & "," & Mid(StringToComma, 7, 3) & "," & Mid(StringToComma, 10, 3)
  End Select



End Function


Public Sub FindTheAlertFiles()
  
'   Open up the alertfile.txt, which tells us what path
'   (and file name) to monitor.  This value is stored
'   as AlertPathName.
  
  Dim WhichProfileToUse As String

  Dim fso3, f3
  Set fso3 = CreateObject("Scripting.FileSystemObject")
  Set f3 = fso3.OpenTextFile("chatprofiles.txt", ForReading)
  WhichProfileToUse = f3.readline
  frmSwitchProfile!txtOptName1.Text = f3.readline
  frmSwitchProfile!txtPath1.Text = f3.readline
  frmSwitchProfile!txtOptName2.Text = f3.readline
  frmSwitchProfile!txtPath2.Text = f3.readline
  frmSwitchProfile!txtOptName3.Text = f3.readline
  frmSwitchProfile!txtPath3.Text = f3.readline
  frmSwitchProfile!txtOptName4.Text = f3.readline
  frmSwitchProfile!txtPath4.Text = f3.readline
  frmSwitchProfile!txtOptName5.Text = f3.readline
  frmSwitchProfile!txtPath5.Text = f3.readline
  frmSwitchProfile!txtOptName6.Text = f3.readline
  frmSwitchProfile!txtPath6.Text = f3.readline
  f3.Close
  Select Case WhichProfileToUse
    Case "1"
      labelReadingFromThisLog.Caption = "Profile currently selected : #1"
      If frmSwitchProfile!txtOptName1.Text <> "" Then labelReadingFromThisLog.Caption = labelReadingFromThisLog.Caption & " A.K.A. " & frmSwitchProfile!txtOptName1.Text
      frmSwitchProfile!txtPathInUse.Text = frmSwitchProfile!txtPath1.Text
    Case "2"
      labelReadingFromThisLog.Caption = "Profile currently selected : #2"
      If frmSwitchProfile!txtOptName2.Text <> "" Then labelReadingFromThisLog.Caption = labelReadingFromThisLog.Caption & " A.K.A. " & frmSwitchProfile!txtOptName2.Text
      frmSwitchProfile!txtPathInUse.Text = frmSwitchProfile!txtPath2.Text
    Case "3"
      labelReadingFromThisLog.Caption = "Profile currently selected : #3"
      If frmSwitchProfile!txtOptName3.Text <> "" Then labelReadingFromThisLog.Caption = labelReadingFromThisLog.Caption & " A.K.A. " & frmSwitchProfile!txtOptName3.Text
      frmSwitchProfile!txtPathInUse.Text = frmSwitchProfile!txtPath3.Text
    Case "4"
      labelReadingFromThisLog.Caption = "Profile currently selected : #4"
      If frmSwitchProfile!txtOptName4.Text <> "" Then labelReadingFromThisLog.Caption = labelReadingFromThisLog.Caption & " A.K.A. " & frmSwitchProfile!txtOptName4.Text
      frmSwitchProfile!txtPathInUse.Text = frmSwitchProfile!txtPath4.Text
    Case "5"
      labelReadingFromThisLog.Caption = "Profile currently selected : #5"
      If frmSwitchProfile!txtOptName5.Text <> "" Then labelReadingFromThisLog.Caption = labelReadingFromThisLog.Caption & " A.K.A. " & frmSwitchProfile!txtOptName5.Text
      frmSwitchProfile!txtPathInUse.Text = frmSwitchProfile!txtPath5.Text
    Case "6"
      labelReadingFromThisLog.Caption = "Profile currently selected : #6"
      If frmSwitchProfile!txtOptName6.Text <> "" Then labelReadingFromThisLog.Caption = labelReadingFromThisLog.Caption & " A.K.A. " & frmSwitchProfile!txtOptName6.Text
      frmSwitchProfile!txtPathInUse.Text = frmSwitchProfile!txtPath6.Text
  End Select


'   If the currently selected location is "(Location not defined)",
'   or if the file doesn't exist, then force the user to input one.
'   (Location not defined)

  If frmSwitchProfile!txtPathInUse.Text = "(Location not defined)" Then
    frmSwitchProfile.Show
    Unload Me
  End If

'   Open up the alertsounds.txt file, which tells us the
'   strings to look for and the corresponding sound file to
'   play when that string is found.  The number of strings
'   located is stored as HowManyTextStringsAreThere and each
'   individual sound file is stored as StringToLookFor(x) and
'   StrWav_To_Play(x) corresponding to the target string and
'   corresponding sound file.
  HowManyTextStringsAreThere = 0
  
  Dim fso2, f2
  Set fso2 = CreateObject("Scripting.FileSystemObject")
  Set f2 = fso2.OpenTextFile("alertsounds.txt", ForReading)
  Do While Not f2.atendofstream
    HowManyTextStringsAreThere = HowManyTextStringsAreThere + 1
    StringToLookFor(HowManyTextStringsAreThere) = f2.readline
    StrWav_To_Play(HowManyTextStringsAreThere) = f2.readline
  Loop
  f2.Close
End Sub
Private Sub cmdSwitchProfile_Click()
  Timer1.Enabled = False ' Turn off timer while in set-up
  frmSwitchProfile.Show
  Unload Me
End Sub
Private Sub cmdAbout_Click()
  frmAbout.Show
End Sub

Private Sub cmdXPDisplayReset_Click()
  HowManyMinutesHavePassed = 0
  XPorSKGainedOverallVersusTime = 0
  XPorSKGainedOverall = 0

  lblTotalXPGained.Caption = "0"
  lblNumberOfXPGains.Caption = "0"
  lblXPAverage.Caption = "0"
  lblXPAveragePerMinute.Caption = "0"
  lblXPAveragePerHour.Caption = "0"
End Sub

Public Sub form_load()
  
  optPlaySoundsOn.Value = True
  optPlaySoundsOff.Value = False
  TimeCheckCalculationDone = False
'    Timer starts disabled during set-up
  Timer1.Enabled = False
'    This loads in the file to watch and the text files
  FindTheAlertFiles
'    Open the file to monitor.
  Set fso = CreateObject("Scripting.FileSystemObject")
  Set f = fso.OpenTextFile(frmSwitchProfile!txtPathInUse.Text, ForReading)
'   Get to the end of the file
  Do While Not f.atendofstream
    TextStringRead = f.readline
  Loop
'    Clear it so it won't trip a sound if the last thing found
'    happens to be a sound.
  TextStringRead = ""
'   Find the inital file size.
  SizeOfFile = FileLen(frmSwitchProfile!txtPathInUse.Text)

txtTextLineRead.Text = SizeOfFile

'   Start the timer.  1000 is one second.
  Timer1.Interval = 10
  Timer1.Enabled = True
  
End Sub

Public Sub cmdExit_Click()
'  Close the datastream and unload the program.
  f.Close
  Dim Frm As Form
  For Each Frm In Forms
    Unload Frm
    Set Frm = Nothing
    Next Frm
  End
End Sub

Public Sub Timer1_Timer()
  
' Dimension the variables used for XPorSK gain meter
  
  Dim FoundAMatch As Integer
  Dim SpaceHasBeenFound As Boolean
  Dim XPGainedLineHeader As String
  Dim SKGainedLineHeader As String
  Dim XPGainedLine As String
  Dim SKGainedLine As String
  Dim XPorSKGainedstr As String
  Dim XPorSKGained As Long
  Dim XPSKCounter As Long
  Dim XPSKGainedNumber As Long
  Dim XPSKtempstr As String
  Dim XPSKCastingNumber As Long
  Dim TimeCheck As Long
  Dim TimeCheckAverageCalculation As Long
  Dim ControlCharCounter As Long
  Dim ControlCharString As String
  
  
  TimeCheck = Second(Time)


  If (TimeCheck = 18) Then
    If TimeCheckCalculationDone = False Then
      HowManyMinutesHavePassed = HowManyMinutesHavePassed + 1
      XPorSKGainedOverallVersusTime = (XPorSKGainedOverall / HowManyMinutesHavePassed)
      lblXPAveragePerMinute.Caption = CommaValue(Round(XPorSKGainedOverallVersusTime))
      lblXPAveragePerHour.Caption = CommaValue(Round((XPorSKGainedOverallVersusTime * 60)))
      TimeCheckCalculationDone = True
    End If
  End If

  If TimeCheck = 17 Then TimeCheckCalculationDone = False
  If TimeCheck = 19 Then TimeCheckCalculationDone = False
  
' This is the every-15-seconds code, not sure if I really
' like the way this works out.  Leaving it in while I
' ponder it.
'  If (TimeCheck = 11) Or (TimeCheck = 41) Or (TimeCheck = 26) Or (TimeCheck = 56) Then
'    If TimeCheckCalculationDone = False Then
'      HowManyMinutesHavePassed = HowManyMinutesHavePassed + 1
'      XPorSKGainedOverallVersusTime = (XPorSKGainedOverall * 4 / HowManyMinutesHavePassed)
'      lblXPAveragePerMinute.Caption = Round(XPorSKGainedOverallVersusTime, 2)
'      lblXPAveragePerHour.Caption = Round((XPorSKGainedOverallVersusTime * 60), 2)
'      TimeCheckCalculationDone = True
'    End If
'  End If
 
'  If TimeCheck = 10 Then TimeCheckCalculationDone = False
'  If TimeCheck = 40 Then TimeCheckCalculationDone = False
'  If TimeCheck = 12 Then TimeCheckCalculationDone = False
'  If TimeCheck = 42 Then TimeCheckCalculationDone = False
'  If TimeCheck = 25 Then TimeCheckCalculationDone = False
'  If TimeCheck = 27 Then TimeCheckCalculationDone = False
'  If TimeCheck = 55 Then TimeCheckCalculationDone = False
'  If TimeCheck = 57 Then TimeCheckCalculationDone = False
  
  On Error GoTo Errortrap
'       Tell user that log file reading is on.
    labelAreWePausedOrResumed = "Log file reading is currently on." & vbCrLf & "Press the Pause key below to halt reading from the log file."
'       Determine the current size of the monitored file.
    CurrentSizeOfFile = FileLen(frmSwitchProfile!txtPathInUse.Text)
'       Has this file size changed from the initial
'       (or most recently determined) monitored file size?
    If CurrentSizeOfFile <> SizeOfFile Then
'       Since a change has occurred, go to the end of the file.
       Do Until f.atendofstream
txtStatusText.Text = "Was " & SizeOfFile & " and is now " & CurrentSizeOfFile
         SizeOfFile = CurrentSizeOfFile
         TextStringRead = f.readline
'MsgBox ("file size change!")
       Loop

'       Strip out 0A characters
       
'       ControlCharCounter = 1
'       Do While Not ControlCharCounter = Len(TextStringRead)
'         If Mid(TextStringRead, ControlCharCounter, 1) <> Chr(10) Then
'           ControlCharString = ControlCharString & Mid(TextStringRead, ControlCharCounter, 1)
'         End If
'       ControlCharCounter = ControlCharCounter + 1
'       Loop
'       TextStringRead = ControlCharString
       
       
       
       
       
       
       txtTextLineRead.Text = TextStringRead
'MsgBox (txtTextLineRead.Text & " " & TextStringRead)



'        XP and SK gets checked before anything else, in case
'        the user has something in addition that will check
'        and handle XP or SK changes.
   
    
    XPGainedLineHeader = "You received "
    XPGainedLine = " xp."
    SKGainedLineHeader = "You gained "
    SKGainedLine = " points of Shadowknowledge."

' Therefore, we're going to try the second part of the phrase
' as the words-to-catch for identification.  Once that criteria
' has been met, then the next "word" between the spaces is the
' amount - convert that to a number - then as a last
' verification use the first part of the phrase.

   FoundAMatch = FoundNothing
   
   If (Right(TextStringRead, Len(XPGainedLine)) = XPGainedLine) Then
'MsgBox ("found xp")
     FoundAMatch = FoundXP
     lblXPorSK1.Caption = "XP gained during this session"
     lblXPorSK2.Caption = "Number of times XP gained"
     lblXPorSK3.Caption = "Average XP per Gain"
     lblXPorSK4.Caption = "Average XP Per Minute"
     lblXPorSK5.Caption = "Projected XP Per Hour"
   End If
      
   If (Right(TextStringRead, Len(SKGainedLine)) = SKGainedLine) Then
     FoundAMatch = FoundSK
     lblXPorSK1.Caption = "SK gained during this session"
     lblXPorSK2.Caption = "Number of times SK gained"
     lblXPorSK3.Caption = "Average SK per Gain"
     lblXPorSK4.Caption = "Average SK Per Minute"
     lblXPorSK5.Caption = "Projected SK Per Hour"
   End If
   
   If (FoundAMatch = FoundXP) Or (FoundAMatch = FoundSK) Then
      SpaceHasBeenFound = False
      XPorSKGainedstr = ""
' Place XPSKCounter at the very end of the actual XP/SK gained
      If FoundAMatch = FoundXP Then
        XPSKCounter = Len(TextStringRead) - Len(XPGainedLine)
      End If
      If FoundAMatch = FoundSK Then
'MsgBox ("found sk")
        XPSKCounter = Len(TextStringRead) - Len(SKGainedLine)
      End If
'MsgBox (FoundAMatch)
      Do Until (SpaceHasBeenFound = True)
        XPSKtempstr = Mid(TextStringRead, XPSKCounter, 1)
        XPSKCounter = XPSKCounter - 1
          If XPSKtempstr = " " Then
' Now we need to check that we are GAINING XP/SK here.
' This is done by checking the words just before the
' found XP/SK value, to be sure that they match the
' quote You Got XP/SK unquote text.

'If XPSKGainedNumber = XPSKGainedNumber Then


'MsgBox (Mid(TextStringRead, (XPSKCounter + 2 - Len(SKGainedLineHeader)), Len(SKGainedLineHeader)))

If (Mid(TextStringRead, (XPSKCounter + 2 - Len(SKGainedLineHeader)), Len(SKGainedLineHeader)) = SKGainedLineHeader) Or (Mid(TextStringRead, (XPSKCounter + 2 - Len(XPGainedLineHeader)), Len(XPGainedLineHeader)) = XPGainedLineHeader) Then
'MsgBox ("1... found xp, I think " & XPorSKGainedstr & " ][ " & XPSKtempstr)
              XPSKGainedNumber = lblNumberOfXPGains.Caption
              XPSKGainedNumber = XPSKGainedNumber + 1
              lblNumberOfXPGains.Caption = CommaValue(XPSKGainedNumber)
              XPSKCastingNumber = lblTotalXPGained.Caption
              XPSKCastingNumber = XPSKCastingNumber + Val(XPorSKGainedstr)
              XPorSKGainedOverall = XPorSKGainedOverall + Val(XPorSKGainedstr)
              lblTotalXPGained.Caption = CommaValue(XPSKCastingNumber)
              lblXPAverage.Caption = CommaValue(Round((XPorSKGainedOverall / XPSKGainedNumber)))
            End If
            SpaceHasBeenFound = True
           Else
            XPorSKGainedstr = XPSKtempstr + XPorSKGainedstr
          End If
      Loop
    End If
      
'        Since we're at the end of the file, and we have the
'        contents of that last line stored as TextStringRead,
'        let's cycle through target files and see if there's a
'        match.  If there is, play the appropriate sound and
'        exit out of the checking cycle.  We only check
'        to see if a matching string is found to play
'        a sound if and only if the option to play
'        sounds is enabled.
      If optPlaySoundsOn.Value = True Then
        Counter = 1
        Do While Counter <> (HowManyTextStringsAreThere + 1) ' 1 more than the maximum
          If Right(TextStringRead, Len(StringToLookFor(Counter))) = StringToLookFor(Counter) Then
'            gPlaying = PlaySound(StrWav_To_Play(Counter), 0, ASYNCH)    ' older unused way to play file
'MsgBox ("found")
            sndPlaySound StrWav_To_Play(Counter), 1
            TextStringRead = ""
            Counter = HowManyTextStringsAreThere ' exactly at the maximum, so it'll stop looking
          End If
          Counter = Counter + 1
        Loop
      End If
    End If

Errortrap:
  If Err.Number = 53 Then
    MsgBox ("A valid chat log path has not been selected.  If you need help locating a valid chat log path, please see the help file called " & """" & "How to Find a Chat Log Path" & """" & ".")
    frmSwitchProfile.Enabled = True
    frmSwitchProfile.Visible = True
     cmdSwitchProfile_Click
  End If
End Sub

