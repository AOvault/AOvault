VERSION 5.00
Begin VB.Form frmAbout 
   Caption         =   "About Chat Alert"
   ClientHeight    =   6120
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6645
   Icon            =   "frmAbout.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   ScaleHeight     =   6120
   ScaleWidth      =   6645
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox Picture1 
      Height          =   3615
      Left            =   255
      Picture         =   "frmAbout.frx":0ECA
      ScaleHeight     =   3555
      ScaleWidth      =   6075
      TabIndex        =   2
      Top             =   240
      Width           =   6135
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Height          =   495
      Left            =   2415
      TabIndex        =   0
      Top             =   5280
      Width           =   1815
   End
   Begin VB.Label labelAboutText 
      Alignment       =   2  'Center
      Height          =   855
      Left            =   1875
      TabIndex        =   1
      Top             =   4080
      Width           =   2895
   End
End
Attribute VB_Name = "frmAbout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdOk_Click()
  Unload Me
End Sub

Private Sub form_load()

  labelAboutText.Caption = "Chat Alert v" & App.Major & "." & App.Minor & "." & App.Revision & vbCrLf & "Coded by Covenant" & vbCrLf & "(petnamer@sbcglobal.net)" & vbCrLf + "Homepage - http://www.halorn.com" & vbCrLf
  labelAboutText.Caption = labelAboutText.Caption & vbCrLf + "Anarchy Online is copyright Funcom GmbH (www.funcom.com)"

End Sub
