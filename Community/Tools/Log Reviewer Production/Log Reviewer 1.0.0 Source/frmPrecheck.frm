VERSION 5.00
Begin VB.Form frmPrecheck 
   Caption         =   "Precheck"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   Icon            =   "frmPrecheck.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
End
Attribute VB_Name = "frmPrecheck"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
  
'   Set up variables for use.
  
Sub form_load()
  
  On Error GoTo Errortrap
  
  Const ForReading = 1, ForWriting = 2, ForAppending = 8
  Dim fso3, f3
  Set fso3 = CreateObject("Scripting.FileSystemObject")
  Set f3 = fso3.OpenTextFile("chatprofiles.txt", ForReading)
  WhichProfileToUse = f3.readline
  frmSwitchProfile!txtOptName1.Text = f3.readline
  frmSwitchProfile!txtPath1.Text = f3.readline
  frmSwitchProfile!txtOptName2.Text = f3.readline
  frmSwitchProfile!txtPath2.Text = f3.readline
  frmSwitchProfile!txtOptName3.Text = f3.readline
  frmSwitchProfile!txtPath3.Text = f3.readline
  frmSwitchProfile!txtOptName4.Text = f3.readline
  frmSwitchProfile!txtPath4.Text = f3.readline
  frmSwitchProfile!txtOptName5.Text = f3.readline
  frmSwitchProfile!txtPath5.Text = f3.readline
  frmSwitchProfile!txtOptName6.Text = f3.readline
  frmSwitchProfile!txtPath6.Text = f3.readline
  f3.Close
  Select Case WhichProfileToUse
    Case "1"
      frmSwitchProfile!txtPathInUse.Text = frmSwitchProfile!txtPath1.Text
    Case "2"
      frmSwitchProfile!txtPathInUse.Text = frmSwitchProfile!txtPath2.Text
    Case "3"
      frmSwitchProfile!txtPathInUse.Text = frmSwitchProfile!txtPath3.Text
    Case "4"
      frmSwitchProfile!txtPathInUse.Text = frmSwitchProfile!txtPath4.Text
    Case "5"
      frmSwitchProfile!txtPathInUse.Text = frmSwitchProfile!txtPath5.Text
    Case "6"
      frmSwitchProfile!txtPathInUse.Text = frmSwitchProfile!txtPath6.Text
  End Select






' Open frmSwitchProfile!txtPathInUse.Text and see if it fails into the Errortrap.
  Set f3 = fso3.OpenTextFile(frmSwitchProfile!txtPathInUse.Text, ForReading)
  f3.Close






  If frmSwitchProfile!txtPathInUse.Text = "(Location not defined)" Then
    MsgBox ("A valid chat log path has not been selected." & vbCrLf & vbCrLf & "This message is normal if this is your first time using the program, and you have not yet set up a chat log path." & vbCrLf & vbCrLf & "If you need help locating a valid chat log path, please see the help file called " & """" & "How to Find a Chat Log Path" & """" & ".")
    frmSwitchProfile.Show
    Unload Me
  End If

If ((Err.Number = 0) And (frmSwitchProfile!txtPathInUse.Text <> "(Location not defined)")) Then
  frmMain.Show
  Unload Me
End If

Errortrap:
  If ((Err.Number = 76) Or (Err.Number = 53)) Then
    MsgBox ("A valid chat log path has not been selected." & vbCrLf & vbCrLf & "This message is normal if this is your first time using the program, and you have not yet set up a chat log path." & vbCrLf & vbCrLf & "If you need help locating a valid chat log path, please see the help file called " & """" & "How to Find a Chat Log Path" & """" & ".")
    frmSwitchProfile.Enabled = True
    frmSwitchProfile.Visible = True
    Unload Me
  End If

End Sub






















