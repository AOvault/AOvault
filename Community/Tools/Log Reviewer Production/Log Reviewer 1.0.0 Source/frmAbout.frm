VERSION 5.00
Begin VB.Form frmAbout 
   Caption         =   "About Log Reviewer"
   ClientHeight    =   5880
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6000
   Icon            =   "frmAbout.frx":0000
   LinkTopic       =   "Form2"
   ScaleHeight     =   5880
   ScaleWidth      =   6000
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox Picture1 
      Height          =   3015
      Left            =   353
      Picture         =   "frmAbout.frx":0CCA
      ScaleHeight     =   2955
      ScaleWidth      =   5235
      TabIndex        =   2
      Top             =   240
      Width           =   5295
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Height          =   495
      Left            =   2273
      TabIndex        =   0
      Top             =   5280
      Width           =   1455
   End
   Begin VB.Label labelAboutText 
      Caption         =   "Label1"
      Height          =   1335
      Left            =   1200
      TabIndex        =   1
      Top             =   3600
      Width           =   3615
   End
End
Attribute VB_Name = "frmAbout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdOk_Click()
  Unload Me
End Sub

Private Sub form_load()

  labelAboutText.Caption = "Log Reviewer v" & App.Major & "." & App.Minor & "." & App.Revision & vbCrLf & "Coded by Covenant" & vbCrLf & "(petnamer@sbcglobal.net)" & vbCrLf + "Homepage - http://www.halorn.com" & vbCrLf
  labelAboutText.Caption = labelAboutText.Caption & vbCrLf + "Anarchy Online is copyright Funcom GmbH (www.funcom.com)"

End Sub

