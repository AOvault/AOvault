VERSION 5.00
Begin VB.Form frmMain 
   Caption         =   "Log Reviewer"
   ClientHeight    =   4065
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6765
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4065
   ScaleWidth      =   6765
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdSwitchProfile 
      Caption         =   "Find Chat Log File"
      Height          =   495
      Left            =   1800
      TabIndex        =   5
      Top             =   3360
      Width           =   1455
   End
   Begin VB.ComboBox cmbWhichChannelToParse 
      Height          =   315
      Left            =   1800
      TabIndex        =   3
      Text            =   "Choose your chat channel here"
      Top             =   960
      Width           =   3135
   End
   Begin VB.CommandButton cmdAbout 
      Caption         =   "&About"
      Height          =   495
      Left            =   3480
      TabIndex        =   2
      Top             =   3360
      Width           =   1455
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "E&xit"
      Height          =   495
      Left            =   5160
      TabIndex        =   1
      Top             =   3360
      Width           =   1455
   End
   Begin VB.CommandButton cmdParse 
      Caption         =   "&Parse"
      Height          =   495
      Left            =   120
      TabIndex        =   0
      Top             =   3360
      Width           =   1455
   End
   Begin VB.Label labelReadingFromThisLog 
      Alignment       =   2  'Center
      Caption         =   "Label2"
      Height          =   735
      Left            =   1035
      TabIndex        =   6
      Top             =   240
      Width           =   4695
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Ready"
      Height          =   735
      Left            =   1755
      TabIndex        =   4
      Top             =   2040
      Width           =   3255
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim TargetChannel As String

Private Sub cmbWhichChannelToParse_click()
  TargetChannel = cmbWhichChannelToParse.Text
End Sub

Private Sub form_load()
  FindTheAlertFiles
  Dim FoundAMarker As Boolean
  Dim LengthOfLineRead As Long
  Dim TargetString As String
  Dim LineReadFromFile As String
  Dim BaseDirectory As String
  Dim Counter As Long
  Const ForReading = 1, ForWriting = 2, ForAppending = 8
  Dim fso, f
'  BaseDirectory = "C:\Program Files\funcom\anarchy online\prefs\cov186\char512449212\chat\windows\window3\"
  BaseDirectory = frmSwitchProfile!txtPathInUse.Text
  Set fso = CreateObject("Scripting.FileSystemObject")
  Set f = fso.OpenTextFile(BaseDirectory & "Config.xml", ForReading)
  LineReadFromFile = ""
  TargetString = "    <Array name="
  TargetString = TargetString & """"
  TargetString = TargetString & "selected_group_names"
  TargetString = TargetString & """"
  TargetString = TargetString & ">"
  Do While Not LineReadFromFile = TargetString
    LineReadFromFile = f.readline
  Loop
  Do While Not LineReadFromFile = "    </Array>"
    LineReadFromFile = f.readline
    LengthOfLineRead = Len(LineReadFromFile)
    If LengthOfLineRead > 30 Then
      LineReadFromFile = Mid(LineReadFromFile, 30, (LengthOfLineRead - 39))
      cmbWhichChannelToParse.AddItem LineReadFromFile
    End If
  Loop
  f.Close
End Sub
Private Sub cmdParse_Click()
  Dim LinesChecked As Long
  Dim NameOfChatSubject As String
  Dim Marker1, Marker2, Marker3 As Long
  Const ForReading = 1, ForWriting = 2, ForAppending = 8
  Dim fso, f
  Dim fso2, f2
  Dim TotalInstancesFound As Long
'  BaseDirectory = "C:\Program Files\funcom\anarchy online\prefs\cov186\char512449212\chat\windows\window3\"
  BaseDirectory = frmSwitchProfile!txtPathInUse.Text
  Set fso = CreateObject("Scripting.FileSystemObject")
  Set fso2 = CreateObject("Scripting.FileSystemObject")
  Set f = fso.OpenTextFile(BaseDirectory & "Log.txt", ForReading)
  Set f2 = fso.OpenTextFile("output.txt", ForWriting, True)
  f2.write "Log Viewer File for chat channel " & TargetChannel & " created on " & Date & " at " & Time & "." & vbCrLf & vbCrLf
  
  TotalInstancesFound = 0
  Do While Not f.atendofstream
    subDelay (0.0001)
    Label1.Caption = "Reading file, please wait"
    Counter = 1
    LineReadFromFile = f.readline
    LengthOfLineRead = Len(LineReadFromFile)
    FoundAMarker = False
    Do While Not FoundAMarker = True
      If (Mid(LineReadFromFile, Counter, 1) = ",") Then
        FoundAMarker = True
       Else
        Counter = Counter + 1
      End If
    Loop
    Counter = Counter + 2
    Marker1 = Counter
    FoundAMarker = False
    Do While Not FoundAMarker = True
      If (Mid(LineReadFromFile, Counter, 1) = ",") Then
        FoundAMarker = True
       Else
        Counter = Counter + 1
      End If
    Loop
    Marker2 = Counter - 1
    TargetString = (Mid(LineReadFromFile, Marker1, (Marker2 - Marker1)))
    If TargetChannel = TargetString Then
      TotalInstancesFound = TotalInstancesFound + 1
      Counter = Counter + 2
      FoundAMarker = False
      Do While Not FoundAMarker = True
        If (Mid(LineReadFromFile, Counter, 1) = ",") Then
          FoundAMarker = True
         Else
          Counter = Counter + 1
        End If
      Loop
      Marker3 = Counter - 4
      NameOfChatSubject = Mid(LineReadFromFile, Marker2 + 3, (Marker3 - Marker2))
'      NameOfChatSubject = NameOfChatSubject & " " & Marker2 & " " & Counter
      FoundAMarker = False
      Do While Not FoundAMarker = True
        If (Mid(LineReadFromFile, Counter, 1) = "]") Then
          FoundAMarker = True
         Else
          Counter = Counter + 1
        End If
      Loop
      f2.write NameOfChatSubject & ": " & Right(LineReadFromFile, Len(LineReadFromFile) - Counter) & vbCrLf
    End If
  Loop
  If TotalInstancesFound = 1 Then
    f2.write vbCrLf & "1 instance was found."
    Label1.Caption = "1 instance was found" & vbCrLf & "for " & TargetChannel & "."
   Else
    f2.write vbCrLf & TotalInstancesFound & " instances were found."
    Label1.Caption = TotalInstancesFound & " instances were found" & vbCrLf & "for " & TargetChannel & "."
  End If
  f2.Close
  f.Close
End Sub

Private Sub cmdAbout_Click()
  frmAbout.Show
End Sub


Public Sub cmdExit_Click()
  Dim Frm As Form
  For Each Frm In Forms
    Unload Frm
    Set Frm = Nothing
    Next Frm
  End
End Sub

Public Sub FindTheAlertFiles()
  
'   Open up the alertfile.txt, which tells us what path
'   (and file name) to monitor.  This value is stored
'   as AlertPathName.
  
  Dim WhichProfileToUse As String
  Const ForReading = 1, ForWriting = 2, ForAppending = 8

  Dim fso3, f3
  Set fso3 = CreateObject("Scripting.FileSystemObject")
  Set f3 = fso3.OpenTextFile("chatprofiles.txt", ForReading)
  WhichProfileToUse = f3.readline
  frmSwitchProfile!txtOptName1.Text = f3.readline
  frmSwitchProfile!txtPath1.Text = f3.readline
  frmSwitchProfile!txtOptName2.Text = f3.readline
  frmSwitchProfile!txtPath2.Text = f3.readline
  frmSwitchProfile!txtOptName3.Text = f3.readline
  frmSwitchProfile!txtPath3.Text = f3.readline
  frmSwitchProfile!txtOptName4.Text = f3.readline
  frmSwitchProfile!txtPath4.Text = f3.readline
  frmSwitchProfile!txtOptName5.Text = f3.readline
  frmSwitchProfile!txtPath5.Text = f3.readline
  frmSwitchProfile!txtOptName6.Text = f3.readline
  frmSwitchProfile!txtPath6.Text = f3.readline
  f3.Close
  Select Case WhichProfileToUse
    Case "1"
      labelReadingFromThisLog.Caption = "Chat window currently selected : #1"
      If frmSwitchProfile!txtOptName1.Text <> "" Then labelReadingFromThisLog.Caption = labelReadingFromThisLog.Caption & " A.K.A. " & frmSwitchProfile!txtOptName1.Text
      frmSwitchProfile!txtPathInUse.Text = frmSwitchProfile!txtPath1.Text
    Case "2"
      labelReadingFromThisLog.Caption = "Chat window currently selected : #2"
      If frmSwitchProfile!txtOptName2.Text <> "" Then labelReadingFromThisLog.Caption = labelReadingFromThisLog.Caption & " A.K.A. " & frmSwitchProfile!txtOptName2.Text
      frmSwitchProfile!txtPathInUse.Text = frmSwitchProfile!txtPath2.Text
    Case "3"
      labelReadingFromThisLog.Caption = "Chat window currently selected : #3"
      If frmSwitchProfile!txtOptName3.Text <> "" Then labelReadingFromThisLog.Caption = labelReadingFromThisLog.Caption & " A.K.A. " & frmSwitchProfile!txtOptName3.Text
      frmSwitchProfile!txtPathInUse.Text = frmSwitchProfile!txtPath3.Text
    Case "4"
      labelReadingFromThisLog.Caption = "Chat window currently selected : #4"
      If frmSwitchProfile!txtOptName4.Text <> "" Then labelReadingFromThisLog.Caption = labelReadingFromThisLog.Caption & " A.K.A. " & frmSwitchProfile!txtOptName4.Text
      frmSwitchProfile!txtPathInUse.Text = frmSwitchProfile!txtPath4.Text
    Case "5"
      labelReadingFromThisLog.Caption = "Chat window currently selected : #5"
      If frmSwitchProfile!txtOptName5.Text <> "" Then labelReadingFromThisLog.Caption = labelReadingFromThisLog.Caption & " A.K.A. " & frmSwitchProfile!txtOptName5.Text
      frmSwitchProfile!txtPathInUse.Text = frmSwitchProfile!txtPath5.Text
    Case "6"
      labelReadingFromThisLog.Caption = "Chat window currently selected : #6"
      If frmSwitchProfile!txtOptName6.Text <> "" Then labelReadingFromThisLog.Caption = labelReadingFromThisLog.Caption & " A.K.A. " & frmSwitchProfile!txtOptName6.Text
      frmSwitchProfile!txtPathInUse.Text = frmSwitchProfile!txtPath6.Text
  End Select


'   If the currently selected location is "(Location not defined)",
'   or if the file doesn't exist, then force the user to input one.
'   (Location not defined)

  If frmSwitchProfile!txtPathInUse.Text = "(Location not defined)" Then
    frmSwitchProfile.Show
    Unload Me
  End If

End Sub

Private Sub cmdSwitchProfile_Click()
  frmSwitchProfile.Show
  Unload Me
End Sub


'Name:      DELAY FOR AWHILE
'Purpose:      This sub loops for a passed number of seconds.
'Inputs:       sngDelay    The quantity of time to elapse.
'Note:         fractional parts of a second work!
'Assumptions:  The sub assumes that sngDelay is never greater
'              than 86400. I.e. the sub is not designed to delay
'              for longer than a day.
'
'Effects:      The calling routine's processing is
'              effectively delayed
'              by the # of seconds in the argument, sngDelay.
'
'Returns:      Nothing.
'
Public Sub subDelay(sngDelay As Single)
Const cSecondsInDay = 86400        ' # of seconds in a day.

Dim sngStart As Single             ' Start time.
Dim sngStop  As Single             ' Stop time.
Dim sngNow   As Single             ' Current time.

sngStart = Timer                   ' Get current timer.
sngStop = sngStart + sngDelay      ' Set up stop time based on
                                   ' delay.

Do
    sngNow = Timer                 ' Get current timer again.
    If sngNow < sngStart Then      ' Has midnight passed?
        sngStop = sngStart - cSecondsInDay  ' If yes, reset end.
    End If
    DoEvents                       ' Let OS process other events.

Loop While sngNow < sngStop        ' Has time elapsed?

End Sub


