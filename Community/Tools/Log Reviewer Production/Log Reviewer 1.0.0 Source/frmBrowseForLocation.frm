VERSION 5.00
Begin VB.Form frmBrowseForLocation 
   Caption         =   "Browse for Location"
   ClientHeight    =   4560
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5640
   Icon            =   "frmBrowseForLocation.frx":0000
   LinkTopic       =   "Form3"
   ScaleHeight     =   4560
   ScaleWidth      =   5640
   StartUpPosition =   3  'Windows Default
   Begin VB.FileListBox File1 
      Height          =   2430
      Left            =   360
      Pattern         =   "log.txt"
      TabIndex        =   5
      Top             =   1080
      Width           =   2295
   End
   Begin VB.DriveListBox Drive1 
      Height          =   315
      Left            =   360
      TabIndex        =   4
      Top             =   720
      Width           =   2295
   End
   Begin VB.DirListBox Dir1 
      Height          =   2790
      Left            =   2760
      TabIndex        =   3
      Top             =   720
      Width           =   2295
   End
   Begin VB.TextBox txtCurrentlyModifying 
      Height          =   375
      Left            =   4800
      TabIndex        =   2
      Text            =   "Text1"
      Top             =   240
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Ok"
      Height          =   375
      Left            =   1095
      TabIndex        =   1
      Top             =   3720
      Width           =   1095
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      Height          =   375
      Left            =   3255
      TabIndex        =   0
      Top             =   3720
      Width           =   1095
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Browse to the file location of the chat log.  See the enclosed help file for details on how to find this file."
      Height          =   375
      Left            =   360
      TabIndex        =   6
      Top             =   240
      Width           =   4335
   End
End
Attribute VB_Name = "frmBrowseForLocation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdOk_Click()
File1_DblClick
End Sub

Private Sub Dir1_Change()
File1.Path = Dir1.Path
End Sub

Private Sub Drive1_Change()
Dir1.Path = Drive1.Drive
End Sub

Private Sub File1_DblClick()
' Note - This was taken from Chat Alert, but the
' is a major difference - log.txt is not appended to the
' end in this version.
  Select Case txtCurrentlyModifying.Text
    Case "1"
      frmSwitchProfile!txtPath1.Text = File1.Path & "\"
    Case "2"
      frmSwitchProfile!txtPath2.Text = File1.Path & "\"
    Case "3"
      frmSwitchProfile!txtPath3.Text = File1.Path & "\"
    Case "4"
      frmSwitchProfile!txtPath4.Text = File1.Path & "\"
    Case "5"
      frmSwitchProfile!txtPath5.Text = File1.Path & "\"
    Case "6"
      frmSwitchProfile!txtPath6.Text = File1.Path & "\"
  End Select
  frmSwitchProfile!txtPathInUse.Text = File1.Path & "\"
  frmBrowseForLocation.Visible = False
  frmBrowseForLocation.Enabled = False
  Unload Me
End Sub
Private Sub cmdCancel_Click()
  Unload Me
End Sub


