Symbskin 0.D v.2.0.2 BETA

24.09.2004
----------

OK, here is my edited version of the Symbskin (named:"0ut.Damage") with bits and pieces
from Dovves GUI. I made this because I found myself wanting to combine the best of
the current skins out there and this is in no way ment to take the achivement away from the
highly talanted people behind the original GUI's. A big thanks to Arctic for his revolutionary
design and and Dovves for his loveley graphics work.

Keep im mind this is work in progress and may contain all sort of bugs.

*****************************************************************************************
v 2.0.2 - BETA
--------------
-Added some missing graphics files.
-Removed the "fadeout X" due to some unexplaied change done to the GUI. Will investigate this further.
-Changed the mission waypoint indicator to original one
-Changed the Mini bars possition

v 2.0.1 - BETA
--------------
-Completely rewritten .XML files for full AO: Alien Invation support.
-All execessive graphics removed resulting in a 100%+ file size reduction.
-Managed to reduce the mouse lag, resault may vary.
-Opening certain windows no longer close the menu.
-New "action unavailable" icon.
-Leet's pet menu (by demand)

v 2.0 - BETA
-------------
-Updated to support AO: Alien Invasion's new GUI.
-New target graphics
-New NCU background
-New Quickbar background
-I have choosen to leave the target markers in the left and right lower corners as they
 function as an indicator you are in combat and some people might want them. You can turn 
 them off in options (see instructions above).

v 1.1
-----
-New Compass
-New Left/right stat grapchis
-New agg/def bar graphics
-New menu threads
-New macro icons
-Switched the possition on NCU and Perk buttons.

v 1.0
-----
First release

Note:
-----

This is work in progress and I take no responsebility for any kind of errors that might 
occure due to the usage of this GUI.

Have fun,

Driddle

------------------------------------
Email: asteroid@rocketmail.com
Home: http://hem.passagen.se/driddle/
------------------------------------
