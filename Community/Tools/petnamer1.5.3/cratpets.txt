Bureaucrat Droids - Name QL Skill
Beginning of Bureaucrat Pets
-2
-2
Basic Worker-Droid
1
17
Limited Worker-Droid
1
22
Faithful Worker-Droid
4
35
Advanced Worker
7
44
Supervisor-Grade Worker-Droid
7
53
Executive-Grade Worker-Droid
10
61
Director-Grade Worker-Droid
14
70
Basic Helper-Droid
14
79
Limited Helper-Bot
17
87
Faithful Helper-Droid
17
96
Advanced Helper
20
104
Supervisor-Grade Helper-Droid
20
112
Executive-Grade Helper-Droid
24
121
Director-Grade Helper-Droid
27
130
Basic Attendant-Droid
27
139
Limited Attendant-Droid
30
151
Faithful Attendant-Droid
33
163
Advanced Attendant-Droid
37
175
Supervisor-Grade Attendant-Droid
37
188
Executive-Grade Attendant-Droid
40
200
Director-Grade Attendant-Droid
43
212
Basic Assistant-Droid
47
226
Limited Assistant-Droid
50
241
Faithful Assistant-Droid
53
257
Advanced Assistant-Droid
57
273
Supervisor-Grade Assistant-Droid
60
289
Executive-Grade Assistant-Droid
63
305
Director-Grade Assistant-Droid
66
321
Basic Aide-Droid
70
336
Limited Aide-Droid
73
350
Faithful Aide-Droid
76
365
Advanced Aide-Droid
80
379
Supervisor-Grade Aide-Droid
83
394
Executive-Grade Aide-Droid
86
408
Director-Grade Aide-Droid
90
428
Basic Secretary-Droid
96
450
Limited Secretary-Droid
99
473
Faithful Secretary-Droid
106
493
Advanced Secretary-Droid
109
513
Supervisor-Grade Secretary-Droid
113
533
Executive-Grade Secretary-Droid
119
554                       
Director-Grade Secretary-Droid
123
574
Basic Administrator
129
594
Limited Administrator-Droid
132
610
Faithful Administrator-Droid
132
620
Advanced Administrator-Droid
136
628
Supervisor-Grade Administrator-Droid
136
637
Executive-Grade Administrator-Droid
139
646
Director-Grade Administrator-Droid
142
654
Basic Minion
142
662
Limited Minion
146
670
Faithful Minion
146
678
Advanced Minion
149
689
Supervisor-Grade Minion
152
700
Executive-Grade Minion
152
711
Director-Grade Minion
156
723
Basic Bodyguard
159
737
Limited Bodyguard
162
747
Faithful Bodyguard
162
756
Advanced Bodyguard
165
766
Supervisor-Grade Bodyguard
165
772
Executive-Grade Bodyguard
169
779
Director-Grade Bodyguard
169
787
End of Bureaucrat Pets
-1
5000
end
0
0
0
0
0
0
0
0
0
