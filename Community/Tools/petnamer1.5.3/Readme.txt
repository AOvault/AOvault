PetNamer v1.5.3 (July 2, 2004)
by Covenant (petnamer@sbcglobal.net)
http://diaom.tripod.com

Quick start
-----------

Put it into a folder and run it.  Select your Anarchy Online (AO) folder, name your pet(s), create the files and exit the program.  Load up AO.  Run the scripts made by the program (names selected in the Options section of PetNamer).  Or type in /petnamer and move the macros to your action bar.  If you want to see the nanocrystal name of the best pet you can cast with your skills, use the Calculate feature.  

What is PetNamer?
-----------------

PetNamer is a program that will help you name your pets and set up scripts for using them.  It works by creating scripts and help files in your Anarchy Online (AO) folder.

It also takes some of the difficulty out of shopping and setting up for pets by helping you figure out the best pet you can cast with your skills, the skill level required to keep the pet from becoming "over-equipped", and the skills needed for the "next best" pet.  

Why did you make this?
----------------------

Naming and managing pets can be a big hassle in AO.  This is especially true when you try to use colors in the pet names, since it involves a lot of cutting and pasting, and using pets can be intimidating to new AO players - all those commands, all that work with text files and macros.  

PetNamer streamlines the process.  Simply put in the name (or names) you want for your pet(s) and PetNamer will not only put in the correct color text codes, but it will also create the script files needed to control your pets with those "colorized" names included.

Additionally, figuring out the pet you can use without running into the "over-equipping" rule with a particular skill can be a chore.  PetNamer's calculation feature will help you figure out what pet you can use with a particular skill, and also what the "next best" pet possible is - so you'll know your next goal for your next pet.

How do I install it?
--------------------

Just put it into a folder and let it run.  The first time it runs, it will ask you to find your AO folder.  It will also create a script directory for you if you haven't done this already... AO doesn't make a script directory when it is first installed, unfortunately.

How do I use it?
----------------

At the top of the screen are empty boxes for you to enter names for your pets.  Underneath that are profession boxes.  If you're creating pet scripts for a Meta-Physicist, then you'll see three pet name boxes.  If you're creating them for an Engineer there will be two, one for the robot and one for the mechdog.  Bureaucrats only have one naming box, for their 'droid.

Next to the pet name box(es) is a checkbox.  If this box is checked, then when PetNamer is run again, it will recall the name that you've put into this box.  If it is unchecked, then PetNamer will start up with that box cleared.  

Underneath the profession boxes are buttons you can click on to place a selected color into a pet name box.  There is also a button that will toggle the buttons between text describing that color and the color itself (giving you a colored button).

How can I modify the program settings?
--------------------------------------

The "Options" button will take you to a screen with four option tabs.  
The first is "Script Options" and from there you can select the names of the output scripts, and also set the delay used in the pet heal and pet heal me scripts.  

If the delay is set to a number other than zero, a "/pet behind" followed by a /delay of the requested time will be issued.  This delay time is in milliseconds, so a value of 1000 would delay the healpet for one second.  Typically a value of 400 works to help combat lag.  

At the bottom is a checkbox where you can select "follow" or "behind" as the command issued to pause a pet's action.

The second tab - "Pet Announcements" - will let you type in a line of text for your pet to say before it attacks or heals.  If you do not want your pet to say anything, leave the announcement box empty for that pet.

The third and fourth tabs - "MP Pet Selections" and "Engy/Crat Pet Selections" will let you specify which pets you would like to have named.  All pets are selected as a default, and choices are remembered between sessions.

What other features are included?
---------------------------------

The "Calculate" button will bring you to the pet calculation window.  Here, you can enter a value for your pet creating skills (Time & Space plus Matter Creation/Biological Metamorphosis/Matter Metamorphosis, depending on the type of pet) to see the nanocrystal name of the best pet that can be cast with those values, and the name of the next highest pet.  You'll also be notified of the minimum skill needed to avoid the "over-equipping" issue and retain control of your pet.  This can be useful when shopping for the next pet nanocrystal, or to see what pet you could cast with temporary nanoskill enhancers (such as Mocham's or a wrangle).

Beyond that, you have an "About" button, a "Create" button, and an "Exit" button.  All of these will do... well, one tells you about the program, one create the script files, and the other exits the program.  Not much there.

What scripts does it make?
--------------------------

PetNamer will make an in-game script called, surprisingly enough, "/petnamer" ... type in /petnamer while in AO and it will bring up a box with your macro(s), ready to go.  Clicking on the text in that box will create a shortcut macro that you can place on your action bar.

PetNamer will also make the following scripts, based on your currently selected profession.

/petnaming - names your pet(s).  Note that this works by trying to name your pet against every known type of pet for your profession, so AO will return a lot of "invalid command" lines when you do this.  These messages are harmless and can be ignored.  

Meta-Physicists will also get -

/petattack - tells your attack pet to attack the currently selected target.  This does not make the mezz pet or heal pet attack.

/petmezz - tells your mezz pet to attack the currently selected target.

/petheal - tells your heal pet to heal your currently selected target.

/pethealme - tells your healpet to heal yourself.

/petbehind - tells your attack pet to go into "behind" mode.  This will keep the attack pet from selecting a target on its own and attack it.

/mezzpetbehind - tells your mezz pet to go into "behind" mode.  This will keep the mezz pet from selecting a target on its own and attack it.

NOTE : Unless a pet announcement is included, the Engineer and Bureaucrat /pet attack macros do not contain the pet name.  This is intentional, as Engineers and Bureaucrats rarely need to have one pet do one job while another pet(s) do another.  Pet announcements will only be made by the Engineer robot and the Bureaucrat 'droid - the other pets do not speak.

The Meta-Physicist /pet attack macro, on the other hand, DOES name the attack pet specifically, so as not to issue commands to the heal or mezz pets.  

Revision History
----------------

1.5.3 - Corrected the healpet "HealYou" macro created via the help files (a stray space was causing the macro to format incorrectly).

1.5.2 - Fixed a pretty obscure bug involving color boxes and the mechdog naming box.  

      - Minor adjustments made to windows and dialog boxes to (hopefully) make their purposes clearer.

1.5.1 - Fixed two errors with the Option screen - the script directory box would not update itself when the program was first run, and the Engineer/Bureaucrat pet selection box is now attached to the correct top bar.

      - Added the option to choose between "follow" and "behind" as the command to pause a pet.  These commands are used in the Meta-Physicist pet scripts to pause the healpet in the heal self and heal other scripts, and also as seperate commands for the attack and mezz pets.  

      - Redid some behind the scenes work with configuration files.  From this point on, you will not need to delete your current configuration file to use a newer version (yay!).

1.5.0 - Added support for the new Meta-Physicist demon and Engineer mechdog pets.  As of this writing not all of these pets have been located in-game on the live servers, so I used names from the databases.

      - Added options to select exactly which pets you wanted to use.  Now you will no longer send names for pets you'll never use again or pets you'll not use for a long time yet, cutting down on the "invalid command" announcements given by trying to name pets you're not using.

1.4.2 - Placed the mezzpet behind script in the in-game help file.

      - If the healpet behind delay (in the options screen) is at 0, then the /pet behind and /delay lines will not be placed into the healpet "heal other" and "heal me" scripts.  This is also the default made by PetNamer when the program is first run and creates the configuration file.

1.4.1 - The Praetorian Legionnaire is now correctly named.

1.4.0 - Added the pet announcement option.  

      - Added a seperate behind script for the mezz pet.  

      - Cleaned up a few miscellaneous borders and buttons that were a bit off center.

1.3.5 - Added the names for the Meta-Physicist Revenant pets.  I've also removed the Revenant pet from the skill calculator, since there are only three of them, and they rely less on skill and more on level to be able to use.

      - Finally nailed down what was causing the buttons to be different from the main screen on some computers, and fixed the problem.

1.3.4 - Fixed a "fill color" mistake - this caused the main screen to have a white background with grey buttons when run under WinXP - this was quite ugly.  

1.3.3 - Placed a button on the naming screen that will toggle the color buttons between a button with the color on it, and a button with the name for that color.

1.3.2 - Added Dark Blue and Pink as selectable colors.  Be warned, that Dark Blue is -really- dark.

1.3.1 - Changed the "over-equip" calcuation to include the next-best-pet that can be used.

      - Added the FAQ.

      - Added the "/pet behind" script as a created script.

1.3.0 - Added a feature to the Calculate screen to determine the minimum skill required to avoid having an "over-equipped" pet.

      - The "Bureaucrat Attendant" was mistakenly left out of the bureaucrat pet naming routine, and is now included.

1.2.0 - If the pet naming field is empty (for the Engineer and Bureaucrat) or if all three pet naming fields are empty (for the Meta-Physicist), it will now refuse to make scripts and let you know why.

      - Changed the profession selection buttons from "radio" buttons to regular buttons, for ease in selecting.   
 
      - Fixed a memory leak under Windows 98

      - Added name-length checking due to AO's new name length filter.

      - Made the program unable to be Maximized, since it looks silly when that is done.  It can still be Minimized off your desktop.

      - Added support for the new Meta-Physicist Revenant pet.  This is still incomplete pending the release of the higher-level Revenants.

1.1.0 - Added the pet calculation feature.

1.0.0 - Code trimming, behind-the-scenes work to make it a bit cleaner.  First actual release.

0.8.1 - Switched from colored text boxes to clickable buttons, and trimmed down the code size.

0.7.0 - Focused the program on naming and MP-only pet commands.  Previously the program was trying to make every script known, which wasn't really necessary.

    - Added the options screen, to allow the user to rename the script files and set the heal pet delay time.

    - Added the in-game macro creation screen.

0.6.3 - First beta version.  Previous versions were in-house tests.

Thanks to
---------

I'd like to thank :

Alysanda (Atlantean) for providing me with the names of the high level Bureaucrat pets.

Ionicdude (Atlantean) for providing me with the names of the Engineer robot pets.

Zane0 (Atlantean) for providing me with the names of the Engineer Mech Dog pets.

Beaker (http://kuren.org/ao) for the lowdown on how colors are made in Anarchy Online.

The fine folks at Auno.org (www.auno.org) for the nanocrystal database.  This is the database used for the Calculate option.  I also used the database from Anarchy Mainframe (www.aodb.info) for doublechecking.

And an Enforcer I hunted with a long time ago, whose name I've sadly forgotten.  He gave me the idea of making this program when he asked if I could color my pet, so he'll know not to try and get aggro back when the mob was hitting the pet.  It was a great idea and it worked well, but it was a huge hassle to switch my scripts around "on the fly" ... and thus the idea for PetNamer was born.