<?php
$Your_Dimension = 1;
$Your_Org_ID = 9332739;
?>

<HTML>
<HEAD>
</HEAD>

<!--
    PHP to display Anarchy Online Organization Statistics
    Copyright (C) 2011  Llie@RK1

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<BODY>
<link rel="stylesheet" type="text/css" media="screen" href="stats.css" />

<?php
$xml = simplexml_load_file("http://people.anarchy-online.com/org/stats/d/" . $Your_Dimension . "/name/" . $Your_Org_ID . "/basicstats.xml");

// variables

$OrgName = "";
$LastUpdated = "";

$Adventurers = 0;
$Agents = 0;
$Bureaucrats = 0;
$Doctors = 0;
$Enforcers = 0;
$Engineers = 0;
$Fixers = 0;
$Keepers = 0;
$MartialArtists = 0;
$MetaPhysicists = 0;
$NanoTechnicians = 0;
$Shades = 0;
$Soldiers = 0;
$Traders = 0;
$MaxProf = 0;

$Solitus = 0;
$Opifex = 0;
$NanoMage = 0;
$Atrox = 0;
$MaxBreed = 0;

$Male = 0;
$Neuter = 0;
$Female = 0;
$MaxGender = 0;

$MinLevel = 999;
$SumLevel = 0;
$MaxLevel = 0;

$NMembers = 0;

// parse XML / accumulate statistics

foreach($xml->children() as $Organization)
{

    if ( $Organization->getName() == "name" )
    {
	$OrgName = $Organization;
    }
    if ( $Organization->getName() == "last_updated" )
    {
	$LastUpdated = $Organization;
    }

foreach($Organization->children() as $Member)
{

    if ( $Member->getName() == "member" )
    {

	foreach($Member->children() as $Stat)
	{

	    if ( $Stat->getName() == "level" )
	    {
	        if ( intval( $Stat ) < $MinLevel )
		{
		    $MinLevel = intval( $Stat );
		}
	        if ( intval( $Stat ) > $MaxLevel )
		{
		    $MaxLevel = intval( $Stat );
		}
		$SumLevel += intval( $Stat );
		$NMembers ++;
	    }

	    if ( $Stat->getName() == "gender" )
	    {
		switch ( $Stat )
		{
		case "Male":
		     $Male++;
		     break;
		case "Female":
		     $Female++;
		     break;
		case "Neuter":
		     $Neuter++;
		     break;
		}
		if ( $Male > $MaxGender )
		   $MaxGender = $Male;
		if ( $Female > $MaxGender )
		   $MaxGender = $Female;
		if ( $Neuter > $MaxGender )
		   $MaxGender = $Neuter;
	    }

	    if ( $Stat->getName() == "breed" )
	    {
		switch ( $Stat )
		{
		case "Solitus":
		     $Solitus++;
		     break;
		case "Opifex":
		     $Opifex++;
		     break;
		case "Nano":
		     $NanoMage++;
		     break;
		case "Atrox":
		     $Atrox++;
		     break;
		}
		if ( $Solitus > $MaxBreed )
		   $MaxBreed = $Solitus;
		if ( $Opifex > $MaxBreed )
		   $MaxBreed = $Opifex;
		if ( $NanoMage > $MaxBreed )
		   $MaxBreed = $NanoMage;
		if ( $Atrox > $MaxBreed )
		   $MaxBreed = $Atrox;
	    }

	    if ( $Stat->getName() == "profession" )
	    {
		switch ( $Stat )
		{
		case "Adventurer":
		     $Adventurers++;
		     break;
		case "Agent":
		     $Agents++;
		     break;
		case "Bureaucrat":
		     $Bureaucrats++;
		     break;
		case "Doctor":
		     $Doctors++;
		     break;
		case "Enforcer":
		     $Enforcers++;
		     break;
		case "Engineer":
		     $Engineers++;
		     break;
		case "Fixer":
		     $Fixers++;
		     break;
		case "Keeper":
		     $Keepers++;
		     break;
		case "Martial Artist":
		     $MartialArtists++;
		     break;
		case "Meta-Physicist":
		     $MetaPhysicists++;
		     break;
		case "Nano-Technician":
		     $NanoTechnicians++;
		     break;
		case "Shade":
		     $Shades++;
		     break;
		case "Soldier":
		     $Soldiers++;
		     break;
		case "Trader":
		     $Traders++;
		     break;
		}
		if ( $Adventurers > $MaxProf )
		   $MaxProf = $Adventurers;
 		if ( $Agents > $MaxProf )
		   $MaxProf = $Agents;
 		if ( $Bureaucrats > $MaxProf )
		   $MaxProf = $Bureaucrats;
 		if ( $Doctors > $MaxProf )
		   $MaxProf = $Doctors;
 		if ( $Enforcers > $MaxProf )
		   $MaxProf = $Enforcers;
 		if ( $Engineers > $MaxProf )
		   $MaxProf = $Engineers;
 		if ( $Fixers > $MaxProf )
		   $MaxProf = $Fixers;
 		if ( $Keepers > $MaxProf )
		   $MaxProf = $Keepers;
		if ( $MartialArtists > $MaxProf )
		   $MaxProf = $MartialArtists;
		if ( $MetaPhysicists > $MaxProf )
		   $MaxProf = $MetaPhysicists;
		if ( $NanoTechnicians > $MaxProf )
		   $MaxProf = $NanoTechnicians;
		if ( $Shades > $MaxProf )
		   $MaxProf = $Shades;
		if ( $Soldiers > $MaxProf )
		   $MaxProf = $Soldiers;
		if ( $Traders > $MaxProf )
		   $MaxProf = $Traders;
	    }

	}
    }	
}
}

// functions to write output

function table_head( $col1 )
{
      echo "<div class=OrgStatsTable>";
      echo "<div class=OrgStatsRow>";
      echo "<div class=OrgStatsTitleCell>" . $col1 . "</div>";
      echo "<div class=OrgStatsTitleCell>#</div>";
      echo "<div class=OrgStatsLastTitleCell>% Of Total</div>";
      echo "</div>";
}

function table_row( $name, $number, $total, $max )
{

     echo "<div class=OrgStatsRow>";
     echo "<div class=OrgStatsCell>" . $name . "</div>";
     echo "<div class=OrgStatsCell>" . intval(($number/$total)*1000)/10.0 . "</div>";
     echo "<div class=OrgStatsCell><div class=OrgStatsBar style=\"width: " . intval(($number/$max)*100) . "\">&nbsp;</div></div>";
     echo "</div>";

}

function table_close(  )
{
    echo "</div>";
}

// output statistics:
?> 

<div id=ExamplePrevSection style="text-align: center">

<!-- Previous Section -->

</div>

<div id=OrgStats>

  <div id=OrgStatsCenteredTitle>
    <?php
       echo $OrgName;
       ?> Organization Statistics
  </div>

  <div id=OrgStatsLeftColumn>

    <div class=OrgStatsBox>

      <div class=OrgStatsTable>
	<div class=OrgStatsRow>
	  <div class=OrgStatsCell>
	    Number of Members: <?php 
				  echo $NMembers;
				  ?>
	  </div>
	</div>
      </div>

      <div class=OrgStatsTable>
	<div class=OrgStatsRow>
	  <div class=OrgStatsCell>
	    Min. level: <?php 
			   echo $MinLevel;
			   ?>
	  </div>

	  <div class=OrgStatsCell>
	    Avg. level: <?php 
			   echo intval( $SumLevel / $NMembers);
			   ?>
	  </div>

	  <div class=OrgStatsCell>
	    Max. level: <?php 
			   echo $MaxLevel;
			   ?>
	  </div>

	</div>

      </div>

    </div>

    <div class=OrgStatsBox>

      <?php
	 table_head( "Breed" );
	 table_row( "Solitus", $Solitus, $NMembers, $MaxBreed );
	 table_row( "Opifex", $Opifex, $NMembers, $MaxBreed );
	 table_row( "Nano", $NanoMage, $NMembers, $MaxBreed );
	 table_row( "Atrox", $Atrox, $NMembers, $MaxBreed );
	 table_close( );
	 ?>

    </div>

    <div class=OrgStatsBox>

      <?php
	 table_head( "Gender" );
	 table_row( "Male", $Male, $NMembers, $MaxGender );
	 table_row( "Neuter", $Neuter, $NMembers, $MaxGender );
	 table_row( "Female", $Female, $NMembers, $MaxGender );
	 table_close( );
	 ?>

    </div>

    <div class=OrgStatsBox>

      <div class=OrgStatsTable>
	<div class=OrgStatsRow>
	  <div class=OrgStatsCell>
	    Last updated: <?php
			     echo $LastUpdated;
			     ?>
	  </div>
	</div>
      </div>
    </div>

  </div>

  <div id=OrgStatsRightColumn>
    <div class=OrgStatsBox>
      <?php
	 table_head( "Profession" );
	 table_row( "Adventurers", $Adventurers, $NMembers, $MaxProf );
	 table_row( "Agents", $Agents, $NMembers, $MaxProf );
	 table_row( "Bureaucrats", $Bureaucrats, $NMembers, $MaxProf );
	 table_row( "Doctors", $Doctors, $NMembers, $MaxProf );
	 table_row( "Enforcers", $Enforcers, $NMembers, $MaxProf );
	 table_row( "Engineers", $Engineers, $NMembers, $MaxProf );
	 table_row( "Fixers", $Fixers, $NMembers, $MaxProf );
	 table_row( "Keepers", $Keepers, $NMembers, $MaxProf );
	 table_row( "Martial Artists", $MartialArtists, $NMembers, $MaxProf );
	 table_row( "Meta-Physicists", $MetaPhysicists, $NMembers, $MaxProf );
	 table_row( "Nano-Technicians", $NanoTechnicians, $NMembers, $MaxProf );
	 table_row( "Shades", $Shades, $NMembers, $MaxProf );
	 table_row( "Soldiers", $Soldiers, $NMembers, $MaxProf );
	 table_row( "Traders", $Traders, $NMembers, $MaxProf );
	 table_close( );
	 ?>
    </div>
  </div>

</div>

<div id=ExampleNextSection style="text-align: center">
<!-- Next Section -->
</div>

</body>
