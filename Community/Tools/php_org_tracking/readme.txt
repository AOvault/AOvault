LICENSE

    PHP to display Anarchy Online Organization Statistics
    Copyright (C) 2011  Llie@RK1

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

FILES

	readme.txt -- This file
	stats.php -- The HTML file that displays org statistics
	stats.css -- the CSS stylesheet that affects how the
                     statistics look

PREREQUISITES

    A web server/host with PHP 5 installed and with PHP built with
    libxml extension enabled.  (Should be default for PHP.)

USE

    1) Enter your org's dimension and ID
    =================================================================

    Begin by modifying the stats.php file.  You will find two
    variables near the top of the file:

    $Your_Dimension = 1;
    $Your_Org_ID = 9332739;
    
    Change these two numbers for the dimension of your org and your
    org's ID number.

    If you don't know what your org's ID number is you can get it by
    going to the following web page:

    "People of Rubi-Ka"
    http://www.anarchy-online.com/wsp/anarchy/frontend.cgi?func=frontend.show&table=PUBLISH&template=iframe&func_id=1020&navID=1003,1007,1020

    Enter in the name of a member of your organization in the search
    box and submit, and you will get a page with that character's
    statistics.

    Click on the link labelled "Permanent link to the bio of <name>".

    Now click on the link labelled "XML Version".

    Scroll down to find the section marked
    "<organization_membership>".  If your web browser window does not
    provide you with a scroll bar, then click on the "-" (minus signs)
    of some of the upper sections (such as "<name>", "<basic_stats>"
    to close them, and eventually you will be able to see the bottom
    part.

    You will find your org's ID number in the
    "<organization_membership>" secton within the "<organization_id>"
    tags.

    2) Upload files to your web server
    =================================================================

    Upload stats.php and stats.css to your web server.

    3) Options for how to display the org statistics
    =================================================================

    You can display your organization's statistics on a web site in
    one of two ways:

    If you want your org statistics to appear on its own web page,
    then you're done.  Point a web browser at the stats.php web page
    and you should see your org's statistics.

    Alternatively, if you would like to add your org's statistics to
    an existing web page add the following line to your web page at
    the point where you would like the org statistics to appear:
    <?php include("menu.php"); ?>

    4) Inserting content on the org statistics web page
    =================================================================

    If you would like to put additional content above the org
    statistics, then add it to the section marked:
    <!-- Previous Section -->

    if you would like to put additional content below the org
    statistics, then add it to the section marked:
    <!-- Next Section -->

    5) Modifying the appearance of the org statistics
    =================================================================

    You can control just about every aspect of the org statistics by
    modifying the stats.css CSS stylesheet.  Most importantly, if your
    web page is already using a style sheet you will want to delete
    the first style definition for "body".

    All other styles start with "OrgStats..." which should prevent
    adverse interactions with your web site's other stylesheets (if
    any).

CLOSING REMARKS

    This little project was initiated by Windgaerd (RK1) of Mercury
    Dragons and AO-Universe.

    If you have questions or comments, you can send an in-game PM to
    Llie (RK1) or send a forum PM to Llie at the AO-Universe.com
    forums.

