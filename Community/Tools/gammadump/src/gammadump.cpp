/*
    GammaDump - a damage dumper for Anarchy Online.

	Gammadump is distributed under the terms of the GNU GPL Version 3.
	Created by Alanceil, 2007.
	Mailto: mail@tom-f.org
	Homepage: http://tom-f.org/code/gammadump/gammadump.php

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program, in the file called LICENSE.
    If not, see http://www.gnu.org/licenses/ .

	Thanks to:
		Shye
		Iwha
		ChaosSystem
		Aeneas
		Cashmaid

*/

#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <csignal>
#include <sstream>

//here comes the portability stuff, first unix
#ifndef WIN
	#define SECSLEEP sleep(1);
	//needed for unbuffered terminal
	#include <termios.h>
#endif
//windows
#ifdef WIN
	#include <conio.h>
	#include <windows.h>
	#define SECSLEEP Sleep(1000);
#endif

//maximum amount of players in database
#define MAXPLAYERS 4096

using namespace std;
//let's begin, rock n' roll!

//--- begin global variables ---
//if you ask: many of them are there for convenience; to avoid overly long function calls and return() mayhem

const string version = "1.4";
const string url = "http://tom-f.org/code/gammadump/gammadump.php";

//vars needed for activecheck
time_t beginsec = time(NULL);
time_t nowsec = time(NULL);
int activesecs[MAXPLAYERS];
int dmg_activemin[MAXPLAYERS];
double leechfactor[MAXPLAYERS];
int secs_between_hits;
const int actimeout = 7; //max idle seconds
bool time_first = true;

//vars for writeout if cfg'd
int writeinterval = 0;
int teaminterval = 0;
time_t lastwrite = time(NULL);
time_t nowwrite = time(NULL);
time_t teamlastwrite = time(NULL);
time_t teamnowwrite = time(NULL);

//petcheck vars
int petdb[MAXPLAYERS];
int petcounter[MAXPLAYERS];

//team vars
int teamdb[MAXPLAYERS];
int maxteamdb = 0;

//vars for playerstats
string iddb[MAXPLAYERS];
int dmg[MAXPLAYERS];
double dmg_min[MAXPLAYERS];
int dmg_hit[MAXPLAYERS];
int crits[MAXPLAYERS];
int hits[MAXPLAYERS];
double critperc[MAXPLAYERS];
int lasthit[MAXPLAYERS];
int nowhit[MAXPLAYERS];
int highestid = 0;
int highestplace = 1;
int dmg_poison[MAXPLAYERS];
int dmg_cold[MAXPLAYERS];
int dmg_fire[MAXPLAYERS];
int dmg_projectile[MAXPLAYERS];
int dmg_melee[MAXPLAYERS];
int dmg_radiation[MAXPLAYERS];
int dmg_unknown[MAXPLAYERS];
int dmg_energy[MAXPLAYERS];
int dmg_dimach[MAXPLAYERS];
int dmg_brawling[MAXPLAYERS];
int dmg_sneakattack[MAXPLAYERS];
int dmg_fastattack[MAXPLAYERS];
int dmg_flingshot[MAXPLAYERS];
int dmg_fullauto[MAXPLAYERS];
int dmg_burst[MAXPLAYERS];
int dmg_aimedshot[MAXPLAYERS];
int dmg_other[MAXPLAYERS];

//vars set by configfile
string chatscript = "dmg.txt";
string teamscript = "teamdmg.txt";
string aologfile;
string you = "You";
bool colour = true;
bool debug = false;

//vars for place calculation
//we write id and name again to allow easy finding of player names and id's without burning cpu cycles to resolve them again
//change to maxplayers bc. of arbitrary place calc
string dmglist_name[MAXPLAYERS];
int dmglist_id[MAXPLAYERS];
int dmglist_dmg[MAXPLAYERS];

//pipe stuff
#ifdef WIN
	int pipeval[2];
	char opcode;
#endif

//misc
bool bail = false;
bool isdotted = false;
int maxint = 0;

// no, we can't use a performant hashmap here. the implementation is buggy.
// have to iterate over everything here.
// i'll leave the declaration here nevertheless, for any (maybe) coming ideas about hashmaps
// __gnu_cxx::hash_map<const char*, int> playerdb;
// __gnu_cxx::hash_map<const char*, int>::iterator itplayerdb;

//--- end global variables ---

//--- begin functions ---


//even more portability stuff
#ifdef WIN
	void setcolour(unsigned short col){
		if(colour){
			/*
			0 = black
			1 = dblue
			2 = dgreen
			3 = dcyan
			4 = dred
			5 = dviolet
			6 = dyellow
			7 = dwhite
			8 = grey
			9 = blue
			10 = green
			11 = cyan
			12 = red
			13 = violet
			14 = yellow
			15 = white

			Colouring scheme: 	11 - highlighted messages for the user
						12 - (fatal) errors
						7 - default
						15 - dialog / normal messages for the user

			Note: Do not use colours in the parsing-output (the ...:.**ptt:::... thing) - unneeded slowdown.
			*/
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),col);
		}
	}
#endif

//unix needs some work to read chars w/o enter, there is no kbhit, or getch. colour setting differs, maybe ncurses later on
// - had a look at ncurses. using ncurses would work, but keeping windows compat. while using it is horrible.
#ifndef WIN
	void setcolour(unsigned short col){
		if(colour){
			switch(col){
				case 0: cout <<  "\033[3;30m"; break;
				case 1: cout <<  "\033[0;34m"; break;
				case 2: cout <<  "\033[0;32m"; break;
				case 3: cout <<  "\033[0;36m"; break;
				case 4: cout <<  "\033[0;31m"; break;
				case 5: cout <<  "\033[0;35m"; break;
				case 6: cout <<  "\033[0;33m"; break;
				case 7: cout <<  "\033[0;37m"; break;
				case 8: cout <<  "\033[1;30m"; break;
				case 9: cout <<  "\033[1;34m"; break;
				case 10: cout << "\033[1;32m"; break;
				case 11: cout << "\033[1;36m"; break;
				case 12: cout << "\033[1;31m"; break;
				case 13: cout << "\033[1;35m"; break;
				case 14: cout << "\033[1;33m"; break;
				case 15: cout << "\033[1;37m"; break;
				case 99: cout << "\033[0m"; break; //'magic' colour for linux users, this resets console colours to default
			}
		}
	}

	//old user input code, testing new one below
	/*
	int kbhit(){
		struct termios term, oterm;
		int fd = 0;
		int c = 0;
		tcgetattr(fd, &oterm);
		memcpy(&term, &oterm, sizeof(term));
		term.c_lflag = term.c_lflag & (!ICANON);
		term.c_cc[VMIN] = 0;
		term.c_cc[VTIME] = 1;
		tcsetattr(fd, TCSANOW, &term);
		c = getchar();
		tcsetattr(fd, TCSANOW, &oterm);
		if (c != -1) ungetc(c, stdin);
		return ((c != -1) ? 1 : 0);
	}

	int getch(){
		static int ch = -1, fd = 0;
		struct termios tnew, told;
		fd = fileno(stdin);
		tcgetattr(fd, &told);
		tnew = told;
		tnew.c_lflag &= ~(ICANON|ECHO);
		tcsetattr(fd, TCSANOW, &tnew);
		ch = getchar();
		tcsetattr(fd, TCSANOW, &told);
		return ch;
	}
	*/

	//turns print-characters-to-screen mode on and off
	void doecho(bool enable){
		struct termios ttystate;
		tcgetattr(STDIN_FILENO, &ttystate);
		if(enable){
			ttystate.c_lflag |= ECHO;
		}
		else{
	        ttystate.c_lflag &= ~ECHO;
		}
		tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);
	}

	//code from http://cc.byexamples.com/20070408/non-blocking-user-input-in-loop-without-ncurses/
	void nonblock(bool state){
	    struct termios ttystate;

	    //get the terminal state
	    tcgetattr(STDIN_FILENO, &ttystate);

	    if (state==true){
	        //turn off canonical mode
	        ttystate.c_lflag &= ~ICANON;

	        //minimum of number input read.
	        ttystate.c_cc[VMIN] = 1;
	    }
	    else if (state==false){
	        //turn on canonical mode
	        ttystate.c_lflag |= ICANON;
	    }
	    //set the terminal attributes.
	    tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);
	}

	int kbhit(){
	    struct timeval tv;
	    fd_set fds;
	    tv.tv_sec = 0;
	    tv.tv_usec = 0;
	    FD_ZERO(&fds);
	    FD_SET(STDIN_FILENO, &fds); //STDIN_FILENO is 0
	    select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
	    return FD_ISSET(STDIN_FILENO, &fds);
	}
	//end code from
#endif

//integer to string conversion
string itos(int num){
	ostringstream bufstream;
	bufstream << num;
	string buf = bufstream.str();
	return buf;
}

//double to string conversion
string dtos(double num){
	ostringstream bufstream;
	bufstream << num;
	string buf = bufstream.str();
	return buf;
}

/*
//scales a number, scale(123.4567,3) returns 123.45
//but at this time, we only need a scale to 3, so see below.
double scale(double num, int scale){
	double scale_factor=1.0;
	for(int i=0; i<scale; i++){
		scale_factor = scale_factor * 10.0;
	}
	num = ((int)(num*scale_factor))/scale_factor
	return num;
}
*/
//scales to 2
double scale2(double num){
	return ((int)(num*100.0))/100.0;
}


//maximum int value
int getmaxint(){
	//mathematical way
	/*unsigned int res = 0;
	for(int i=0;i<sizeof(int);i++){
		res = res * 256;
	}
	//now we have maximum unsigned int, make that signed
	res = ((res / 2) - 1);
	return (int) res;*/

	//cool way ;)
	unsigned int res = 0;
	res = (res - 1)/2; //here we forcefully flip to maximum value, and half of that gives us maximum signed int
	return res;
}

//inserts thousand-separator commas
string insertpts(int num){
	string buf=itos(num);
	int len = buf.length();
	int cnt = 1;
	while(cnt < len){
		if( (cnt%3) == 0 ){ buf.insert(len-cnt,"'"); }
		cnt++;
	}
	return buf;
}
string insertpts(double num){
	string buf=dtos(num);
	unsigned int len = buf.find_first_of(".");
	if(len == string::npos){ len=buf.length(); }

	unsigned int cnt = 1;
	while(cnt < len){
		if( (cnt%3) == 0 ){ buf.insert(len-cnt,"'"); }
		cnt++;
	}
	return buf;
}

//resolves names to ids, it creates new ids if not found and create=true. returns dummy-id 0 on error.
int getid(string name,bool create){
	if(debug){ cout << "getid: Parameters: " << name << "," << create << "," << highestid << endl; }
	int id = 0;
	int playerid = 0;
	bool found = false;
	while(id <= highestid){
 		if(name == iddb[id]){
			found = true;
			playerid=id;
 		}
 		id++;
 	}

	//we didn't find the player ?
	if (found != true){
		if(create == true){
			// we need to add an id
		   	highestid++;
		   	if(highestid > MAXPLAYERS){
		   		// don't do (debug) here, the user should know this
		   		setcolour(12);
		   		cout << "#> getid: Warning. The maximum amount of loggable players has been reached. The new player's damage will be added to a dummy player (ID 0). Please report this." << endl;
		   		setcolour(7);
		   		highestid--;
   		   		return 0;
		   	}
		   	iddb[highestid] = name;
		   	if(debug){
				cout << "#> getid: Added playerid " << highestid << " for name " << name << "." << endl;
		   		cout << "#> getid: Max ID is " << highestid << ", id pool is " << (double)(100*highestid) / (double)MAXPLAYERS << "% full." << endl;
			}
		   return highestid;
		}
		//not found and no create ? must have been a bad search
		else{
			return 0;
		}
	}
	else{
		if(debug){
			cout << "#> getid: Returning playerid " << playerid << " for name " << name << "." << endl;
			cout << "#> getid: Max ID is " << highestid << ", id pool is " << (double)(100*highestid) / (double)MAXPLAYERS << "% full." << endl;
		}
		return playerid;
	}

	//failsafe ;)
	return 0;
}


//returns if id is in the player's team, on remove=true delete teammember. we do this here because then
//we don't have to re-iterate over the whole array, saving cpu cycles
bool isteammember(int id,bool remove){
	int cnt = 0;
	while(cnt <= maxteamdb){
		if(teamdb[cnt] == id){
			if(!remove){
				if(debug){ cout << "#> Found teammember with ID: " << id << " at teamdb[" << cnt << "]." << endl; }
				return true;
			}
			if(remove){
				if(debug){ cout << "#> Removing teammember with ID: " << id << " at teamdb[" << cnt << "]." << endl; }
				teamdb[cnt] = 0;
				return true;
			}
		}
		cnt++;
	}
	if(debug){ cout << "#> Could not find teammember with ID: " << id << endl; }
	return false;
}

//return val=true here means "go on", false="next one please"
//we need this to avoid doubling the sort functions, one for with team ids, one without
bool calcplaces_team_helper(bool teamonly,int id){
	if(!teamonly){
		return true;
	}
	else{
		//returns true if teammember, false if not
		return isteammember(id,false);
	}
}

//process the dmg arrays and calculate the highscore list
//on teamonly = true, choose only teammembers from iddb, show=true prints the highscore to stdout
void calcplaces(bool show,bool teamonly,int maxplaces){

	//check that the max places nr isn't ridiculously high, input=MAXPLAYERS => overflow. baaaaad.
	if(maxplaces > MAXPLAYERS-1){
        maxplaces = MAXPLAYERS-1;
        setcolour(12); cout << "#> Maxplaces was too high and was set to the maximum amount of players." << endl; setcolour(7);
    }

    //shall we search for the lowest damagedealers ?
	int dir_factor=1;
	if(maxplaces < 0){
		dir_factor=-1;
		maxplaces = maxplaces * -1;
	}

	//clear the array
	int cnt=0;
	while(cnt <= highestplace){
		if (debug){ cout << "#> Clearing dmglist #" << cnt << "." << endl; }
		//for reverse search we also have to "invert" the default dmg
		if(dir_factor==-1){
			dmglist_dmg[cnt]=maxint; // max length of int here, sizeof(int)=4, (256^4)/2, perhaps later I'll add a function to calc' this dynamically
		}
		else{ dmglist_dmg[cnt]=0; }
		dmglist_name[cnt]="";
		dmglist_id[cnt]=0;
		cnt++;
	}

	//who is #1 ?
	int id=1;
	//dmglist_dmg[1]=0;

	while(id <= highestid){
		if (
		    (dmg[id] != 0) &&
			( (dmglist_dmg[1]*dir_factor) <= (dmg[id]*dir_factor) ) &&
			(calcplaces_team_helper(teamonly, id))
		){
			dmglist_dmg[1] = dmg[id];
			dmglist_name[1] = iddb[id];
			dmglist_id[1] = id;
		}
		id++;
	}


	//who is the rest ?
	int mark;
	int markid;
	int place=2; //the #place to check
	string markname;

	bool foundplayer; //if we didn't find a player for a place, we can stop the search.
	while(place <= maxplaces){
		id=1;

		if(dir_factor==-1){
			mark=maxint;
		}
		else{ mark=0; }//the damage to get under

		markname=""; //the name of the candidate
		markid=0; //id of the cand. player
		if (debug){ cout << "#> Searching for place number " << place << "." << endl; }

		foundplayer=false;
		while(id <= highestid){
			//when 2 players have the same damage, we need separate those two, that's what id != dmglist_id[place-1] is there for.
			if (
				(dmg[id] != 0) &&
				( (dmg[id]*dir_factor) <= (dmglist_dmg[place-1]*dir_factor) ) &&
				(id != dmglist_id[place-1]) &&
				(calcplaces_team_helper(teamonly, id))
			){
				if ( (dmg[id]*dir_factor) > (mark*dir_factor) ){
					mark = dmg[id];
					markname = iddb[id];
					markid = id;
					highestplace = place;
					foundplayer = true;
				}
			}
			id++;
		}

		dmglist_name[place] = markname;
		dmglist_dmg[place] = mark;
		dmglist_id[place] = markid;

		place++;
		if(foundplayer == false){ break; }
	}

	//all's sorted. shall we show the dmg ?
	if(show){
		if (debug){ cout << "#> Highestplace is " << highestplace << "." << endl; }
		place=1;
		cout << endl;
		while( (place <= highestplace) && (place <= maxplaces) ){
			if (debug){ cout << "#> place=" << place << "." << endl; }

			setcolour(15); cout << "#> "; setcolour(10); cout << "#" << place << ": " << dmglist_name[place];
			if (petcounter[dmglist_id[place]] > 0){
				cout << " (Pets: " << petcounter[dmglist_id[place]] << ")";
			}

			setcolour(15);
			cout << endl << "#> "; setcolour(14); cout << insertpts(dmglist_dmg[place]) << " dmg "; setcolour(15); cout << "(" << insertpts(scale2(dmg_min[dmglist_id[place]])) << " dpm / " << insertpts(dmg_activemin[dmglist_id[place]]) << " dpam / leechfactor: " << leechfactor[dmglist_id[place]] << ")" << endl;
			place++;
		}
		setcolour(7);
	}
}

//here we write the highscore list to a file
//make sure it was generated with calcplaces before when calling this function
void writeout(bool show, bool teamlist){
	ofstream odmgfd;

	//team damage goes somewhere else, we do it this way so we can easily give the user a path feedback
	string outfile;
	if(teamlist){ outfile = teamscript; }
	else{ outfile = chatscript; }
	odmgfd.open (outfile.c_str());
	if(odmgfd.is_open()){
		int place = 1;
		string teamstring = ""; if(teamlist){ teamstring = "- Team damage -<br>"; }

		odmgfd << "<a href=\"text://" << teamstring << "Format: dmg ( dmg/min / dmg/activemin / leechfactor )<br><br>";
		//no more than 6 places bc. of 1024 byte limit; that's also why no maxplaces is there
		while( (place <= highestplace) && (place <= 6) ){
			odmgfd << "<font color='#00FF33'>#" << place << ": " << dmglist_name[place];

			if (petcounter[dmglist_id[place]] > 0){
				odmgfd << " (Pets: " << petcounter[dmglist_id[place]] << ")";
			}

			odmgfd << "</font><br><font color='#FFFF77'>" << insertpts(dmglist_dmg[place]) << " dmg</font> (" << insertpts(scale2(dmg_min[dmglist_id[place]])) << " dpm / " << insertpts(dmg_activemin[dmglist_id[place]]) << " dpam / " << leechfactor[dmglist_id[place]] << " )<br>";
			place++;
		}
		odmgfd << "<br><a href='chatcmd:///start " << url << "'>Gammadump v" << version << "</a>";
		odmgfd << "\">Gammadump stats</a>";
		odmgfd.close();
	}
	else{
		setcolour(12); cout << "#> Error opening file '" << outfile << "' for writing. Please check your path." << endl; setcolour(7);
		odmgfd.close();
		return;
	}

	ifstream idmgfd;
	idmgfd.open (outfile.c_str());
	idmgfd.seekg(0, ios::end);
	int end = idmgfd.tellg();
	idmgfd.close();
	if(show){
		setcolour(15); cout << "#> "; setcolour(11); cout << "Log written to '" << outfile << "'. Size: " << end << " bytes." << endl; setcolour(7);
	}
	if(end > 1024){
		setcolour(15); cout << "#> "; setcolour(12); cout << "Warning! The logfile is greater than 1024 bytes and it is very likely that your AO Client will crash upon displaying it." << endl; setcolour(7);
	}
}

//is the id a pet or not ? and if yes, return the owner, so he gets the credit
int petcheck(int id){
	if (petdb[id] != 0){
		//let the player get the credit
		int oldid = id;
		id=petdb[oldid];
		if(debug){cout << "#> Pet detected. Pet-ID " << oldid << " replaced with Player-ID " << id << "." << endl;}
	}
	return id;
}

//dmg/activemin code
void activecheck(int id){
	//don't add <actimeout> secs penalty to newcomers
	if(lasthit[id] == 0){
		lasthit[id] = nowsec;
	}
	else{
		lasthit[id] = nowhit[id];
	}

	nowhit[id] = nowsec;
	secs_between_hits = nowhit[id] - lasthit[id];

	//let's give the player a timeout/penalty of <timeout> secs max, so making afk pauses doesn't count
	if (secs_between_hits > actimeout){ secs_between_hits = actimeout; }
	activesecs[id] = activesecs[id] + secs_between_hits;
	//avoid div/0
	if (activesecs[id] == 0){ activesecs[id] = 1; }
	dmg_activemin[id] = (int) ((double)(dmg[id] * 60) / (double)activesecs[id]);
}

//print stats for a player
void showstats(int id){
	if(debug){
		cout << "#> Stats for " << iddb[id] << ", ID " << id << ":" << endl;
		cout << "#>    lasthit=" << lasthit[id] << " nowhit=" << nowhit[id] << " secs_between_hits=" << secs_between_hits << endl;
	}
	setcolour(15);
	cout << "#> Stats for " << iddb[id] << ":" << endl;
	cout << "#>    Damage=" << dmg[id] << ", Damage/Min=" << scale2(dmg_min[id]) << ", Hits=" << hits[id] << ", Crits=" << crits[id] << ", Pets=" << petcounter[id] << endl;
	cout << "#>    %Crits=" << critperc[id] << ", Damage/Hit=" << dmg_hit[id] << endl;
	cout << "#>    Active seconds=" << activesecs[id] << ", Dmg/Activemin=" << dmg_activemin[id] << ", Leechfactor=" << leechfactor[id] << endl;
	cout << "#>" << endl;
	cout << "#>    Damage types: " << endl;

    setcolour(15); cout << "#>      ";
	setcolour(15); cout << "Projectile: " << dmg_projectile[id] << ",";
	setcolour(15); cout << " Melee: " << dmg_melee[id] << ",";
	setcolour(7); cout << " Unknown: " << dmg_unknown[id] << endl;

	setcolour(15); cout << "#>      ";
	setcolour(10); cout << "Poison: " << dmg_poison[id] << ",";
	setcolour(11); cout << " Cold: " << dmg_cold[id] << ",";
	setcolour(12); cout << " Fire: " << dmg_fire[id] << ",";
	setcolour(14); cout << " Radiation: " << dmg_radiation[id] << ",";
	setcolour(15); cout << " Energy: " << dmg_energy[id] << endl;

	setcolour(15); cout << "#>      ";
	setcolour(11); cout << "Fast Attack: " << dmg_fastattack[id] << ",";
	setcolour(10); cout << " Brawling: " << dmg_brawling[id] << ",";
	setcolour(12); cout << " Dimach: " << dmg_dimach[id] << ",";
	setcolour(14); cout << " Sneak Attack: " << dmg_sneakattack[id] << endl;


	setcolour(15); cout << "#>      ";
	setcolour(11); cout << "Fling Shot: " << dmg_flingshot[id] << ",";
	setcolour(10); cout << " Burst: " << dmg_burst[id] << ",";
	setcolour(12); cout << " Full Auto: " << dmg_fullauto[id] << ",";
	setcolour(14); cout << " Aimed Shot: " << dmg_aimedshot[id] << endl;

	setcolour(15); cout << "#>      ";
	setcolour(7); cout << "Other: " << dmg_other[id] << endl;
}

//search player in db and return stats
void searchstats(){
	string search;
	setcolour(15);
	cout << "#> Enter player name (case sensitive): ";

	#ifndef WIN
		doecho(true);
	#endif
	getline (cin,search);
	#ifndef WIN
		doecho(false);
	#endif

		int id;
	int tempid;
	id=getid(search,false);
	tempid=petcheck(id);
	if(tempid != id){ id = tempid; cout << "#> "; setcolour(11); cout << "The player you searched for was a pet. Displaying stats for owner." << endl; setcolour(15); }

	if(id != 0){
		showstats(id);
	}
	else{
		setcolour(15); cout << "#> "; setcolour(11); cout << "Sorry, the player '" << search << "' was not found in the database." << endl;
		setcolour(7);
	}
}

//here we update our dmg arrays
void calcstats(int id, string name, int idmg, int crit, int hit, string dtype, bool do_activecheck){

	double deltamin;
	//avoid div/0 on load
	if((nowsec - beginsec) == 0){
		deltamin = 0.01;
	}
	else{
		deltamin = (double)(nowsec - beginsec) / 60.0;
	}
	if (debug){cout << "#> Deltamin: '" << deltamin << "'" << endl;}

	//int-overflow protection
	if( ((maxint - idmg) < dmg[id]) ){
		dmg[id] = maxint;
		setcolour(12);
		cout << "#> " << name << " has reached the maximum damage. Please report this!" << endl;
		setcolour(7);
	}
	dmg[id] = dmg[id] + idmg;
	dmg_min[id] = (double)dmg[id] / deltamin;
	hits[id] = hits[id] + hit;
	crits[id] = crits[id] + crit;
	critperc[id] = (( (double)crits[id] / ((double)crits[id] + (double)hits[id]) )* 100.0);
	dmg_hit[id] = (int) ( (double)dmg[id] / (double)(hits[id]+crits[id]));

	//switch-case only supports ints, have to use ifs here
	if(dtype != ""){
		if(dtype == "poison"){ dmg_poison[id] = dmg_poison[id] + idmg; }
		else if(dtype == "cold"){ dmg_cold[id] = dmg_cold[id] + idmg; }
		else if(dtype == "fire"){ dmg_fire[id] = dmg_fire[id] + idmg; }
        else if(dtype == "projectile"){ dmg_projectile[id] = dmg_projectile[id] + idmg; }
		else if(dtype == "melee"){ dmg_melee[id] = dmg_melee[id] + idmg; }
		else if(dtype == "radiation"){ dmg_radiation[id] = dmg_radiation[id] + idmg; }
		else if(dtype == "unknown"){ dmg_unknown[id] = dmg_unknown[id] + idmg; }
		else if(dtype == "energy"){ dmg_energy[id] = dmg_energy[id] + idmg; }
		else if(dtype == "Dimach"){ dmg_dimach[id] = dmg_dimach[id] + idmg; }
		else if(dtype == "Brawling"){ dmg_brawling[id] = dmg_brawling[id] + idmg; }
		else if(dtype == "Fast Attack"){ dmg_fastattack[id] = dmg_fastattack[id] + idmg; }
		else if(dtype == "Sneak Atck"){ dmg_sneakattack[id] = dmg_sneakattack[id] + idmg; }
		else if(dtype == "Fling Shot"){ dmg_flingshot[id] = dmg_flingshot[id] + idmg; }
		else if(dtype == "Full Auto"){ dmg_fullauto[id] = dmg_fullauto[id] + idmg; }
		else if(dtype == "Burst"){ dmg_burst[id] = dmg_burst[id] + idmg; }
		else if(dtype == "Aimed Shot"){ dmg_aimedshot[id] = dmg_aimedshot[id] + idmg; }
		else { dmg_other[id] = dmg_other[id] + idmg; }
	}

	//set the how-many-seconds-was-the-player-active counter
	if(do_activecheck){
		activecheck(id);
	}

	leechfactor[id] = scale2((double) dmg_activemin[id] / (double) dmg_min[id]);

	if(debug){ showstats(id); }

}

string name_postprocess(string name){
	//strip damage shields
	if(name.find("'s damage shield") != string::npos){
		string newname(name,0,name.find("'s damage shield"));
		if (debug){cout << "#> Detected other's damage shield, replaced: '" << name << "' with '" << newname << "'." << endl;}
		name = newname;
	}
	if(name == "Your damage shield"){
		name = you;
	}

	if (name == "You"){ name = you; }
	if (debug){cout << "#> Name: '" << name << "'" << endl;}

	return name;
}

void zerostats(){
	int cnt=0;
	while( cnt < MAXPLAYERS){
		//we don't reset player-id and pet associations, as this would erase team association on reset.
		//iddb[cnt] = "";
		//petdb[cnt] = 0;
		//petcounter[cnt] = 0;
		dmg[cnt] = 0;
		dmg_min[cnt] = 0;
		dmg_hit[cnt] = 0;
		crits[cnt] = 0;
		hits[cnt] = 0;
		critperc[cnt] = 0;
		lasthit[cnt] = 0;
		nowhit[cnt] = 0;
		activesecs[cnt] = 0;
		dmg_activemin[cnt] = 0;
		leechfactor[cnt] = 0;
		cnt++;
	}
	time_first = true;
	highestplace = 1;

	setcolour(15); cout << "#> "; setcolour(11); cout << "Statistics cleared." << endl; setcolour(7);
}

void innerhelp(){
	cout << "#> Press 'h' to show highscores, 's' to show detailed statistics for a player," << endl;
	cout << "#> 'd' to show and dump highscores to a chatscript, 'r' to reset stats," << endl;
	cout << "#> 't' to show and dump team statistics to a file, '?' for help, 'q' to quit." << endl;
}

void keycheck(){

	#ifndef WIN
		nonblock(true);
	#endif

	//have any global hotkeys been pressed ?
	/*
	#ifdef WIN
		char opcode;
		if(debug){ cout << "#> Entering pipe read." << endl; }
		while (read(pipeval[0],opcode,1) > 0){
			switch(opcode){
				case '1':
					cout << "#> Writing statistics." << endl;
					calcplaces(false,false,6);
					writeout(false,false);
					calcplaces(false,true,6);
					writeout(false,true);
					break;
				case '2':
					cout << "#> Resetting statistics." << endl;
					zerostats();
					break;
				default:
					break;
			}

		}
		if(debug){ cout << "#> Finished with the pipe." << endl; }
	#endif
	*/

	while(kbhit()){

		/*windows and unix have different ways of getting user input, we need to adapt a bit here.

		o---+-----[ 1M Ohm ]-----+---o
		    |                    |
		    ----------------------

		     RESISTANCE IS FUTILE.
		*/

		#ifndef WIN
			char input=fgetc(stdin);
		#endif
		#ifdef WIN
			char input=getch();
		#endif

		//this is for 'h', but declaring in the function will lead to a compile error.
		string numplace;
		switch(input){
			case 'h':
				//if dots in windows bc. incoming messages, then make a newline
                if(isdotted){ cout << endl; isdotted = false; }
                setcolour(15);
				cout << "#> How many places do you want to see ?" << endl << "#> Negative values will show lowest places first. " << endl;

				#ifndef WIN
					doecho(true);
				#endif
				getline (cin,numplace);
				#ifndef WIN
					doecho(false);
				#endif

				if( numplace == "" ){ numplace = 6; }
				setcolour(7);
                calcplaces(true,false, atoi(numplace.c_str()) );
				break;
			case 'd':
                if(isdotted){ cout << endl; isdotted = false; }
				calcplaces(true,false,6);
				writeout(true,false);
				break;
			case 'q':
                if(isdotted){ cout << endl; isdotted = false; }
				setcolour(15); cout << "#> "; setcolour(11); cout << "Exiting." << endl; setcolour(7);
				bail=true;
				break;
			case 'i':
				//heh. debug only, "normal" mode shouldn't see any IDs.
				if(debug){
					cout << "#> Enter id to display name for: ";
					string sid;
					#ifndef WIN
						doecho(true);
					#endif
					getline (cin,sid);
					#ifndef WIN
						doecho(false);
					#endif

					int id=atoi(sid.c_str());
					cout << endl << "Name: '" << iddb[id] << "'" << endl;
				}
				else{
					setcolour(15); cout << "#> "; setcolour(11); cout << "Invalid key." << endl; setcolour(7);
				}
				break;
			case 'r':
                if(isdotted){ cout << endl; isdotted = false; }
				zerostats();
				break;
			case 's':
                if(isdotted){ cout << endl; isdotted = false; }
				searchstats();
				break;
			case 't':
                if(isdotted){ cout << endl; isdotted = false; }
				calcplaces(true,true,6);
				writeout(true,true);
				break;
			case '?':
                if(isdotted){ cout << endl; isdotted = false; }
				setcolour(15);
				cout << "#> Help:" << endl;
				innerhelp();
				break;
			default:
                if(isdotted){ cout << endl; isdotted = false; }
				setcolour(15); cout << "#> "; setcolour(11); cout << "Invalid key." << endl; setcolour(7);
				break;
		//end switch
		}

	}

	#ifndef WIN
		nonblock(false);
	#endif
}

void settime(string preline){
	//read timestamp from log
	string timestamp (preline,preline.find_last_of(",")+1,preline.length());
	nowsec = atoi(timestamp.c_str());
	//on first read, set begintime. we do this so we can read old logs without getting negative time differences
	if (time_first == true){
		if(debug){ cout << "#> First read. Setting beginsec." << endl; }
		time_first = false;
		beginsec = nowsec;
	}
}

//here we filter bogus names
int mobcheck(int oldid, int id, string name){
	if(oldid==id){
		if( (name.find_first_of(" -/.'") != string::npos) || (name == "Zix") ){
			if(debug){ cout << "#> Detected a mob. Setting id to 0." << endl; }
			id=0;
		}
	}
	return id;
}

void writeinterval_check(){
	//here we autowrite the dmglog
	if(writeinterval > 0){
		nowwrite = time(NULL);
		int diff=nowwrite - lastwrite;
		if(diff > writeinterval){
			if(debug){ cout << "#> Writeinterval triggered writeout()." << endl; }
			calcplaces(false,false,6);
			writeout(false,false);
			lastwrite = nowwrite;
		}
	}
}
void teaminterval_check(){
	//here we autowrite the teamdmglog
	if(teaminterval > 0){
		teamnowwrite = time(NULL);
		int diff=teamnowwrite - teamlastwrite;
		if(diff > teaminterval){
			if(debug){ cout << "#> Teaminterval triggered writeout()." << endl; }
			calcplaces(false,true,6);
			writeout(false,true);
			teamlastwrite = teamnowwrite;
		}
	}
}


void wait_enter(){
	cout << endl << "-- Press enter to quit --"; string x; getline(cin,x);
}

//prints user help
void usagehelp(){
	setcolour(15);
	cout << "Usage: ";
	#ifdef WIN
		cout << "gammadump.exe";
	#endif
	#ifndef WIN
		cout << "gammadump";
	#endif
	cout << " [configfile]" << endl;
	cout << endl;
	cout << "where [configfile] is the path to a gammadump configuration file, for example" << endl;
	#ifdef WIN
		cout << "\"C:\\Program Files\\AO\\tools\\gammadump\\gammadump.conf\"" << endl;
	#endif
	#ifndef WIN
		cout << "\"/home/tux/bin/ai/tools/gammadump/gammadump.conf\"" << endl;
	#endif
	cout << endl;
	cout << "If gammadump is started without a parameter, it will read \"gammadump.conf\"" << endl;
	cout << "from the current working directory." << endl;
	cout << endl;
	cout << "The configfile should have a content like the included example.conf." << endl;
	//windows users usually double-click on the executable, which would close as soon as it is open, so let's wait for them
	wait_enter();
	setcolour(7);
}

void sigint(int dummy){
	if(isdotted){ cout << endl; isdotted=false; }
	setcolour(15); cout << "#> "; setcolour(11); cout << "Caught signal: INT. Exiting." << endl; setcolour(7);
	signal(SIGINT,sigint);
	bail=true;
}
void sigterm(int dummy){
	if(isdotted){ cout << endl; isdotted=false; }
	setcolour(15); cout << "#> "; setcolour(11); cout << "Caught signal: TERM. Exiting." << endl; setcolour(7);
	signal(SIGTERM,sigterm);
	bail=true;
}

#ifndef WIN
	void sigusr1(int dummy){
		if(isdotted){ cout << endl; isdotted=false; }
		setcolour(15); cout << "#> "; setcolour(11); cout << "Caught signal: USR1. Writing statistics." << endl; setcolour(7);
		signal(SIGUSR1,sigusr1);
		calcplaces(false,false,6);
		writeout(false,false);
		calcplaces(false,true,6);
		writeout(false,true);
	}
	void sigusr2(int dummy){
		if(isdotted){ cout << endl; isdotted=false; }
		setcolour(15); cout << "#> "; setcolour(11); cout << "Caught signal: USR2. Resetting statistics." << endl; setcolour(7);
		signal(SIGUSR2,sigusr2);
		zerostats();
	}
#endif

//this cleans up the environment on exiting; undo terminal settings and colour
void cleanup(){
	#ifndef WIN
		doecho(true);
		nonblock(false);
	    setcolour(99);
	#endif
	//delete pid-tempfile for the helper
	unlink("gammadump.pid");
}

// --- end functions ---

int main(int argc, char* argv[]){

	string line;
	int pos = 0;
	int pos2 = 0;
	int delta = 0;
	bool truncate = false;

	maxint = getmaxint();

	//let's read the config
	string cfgpath;

	switch(argc){
		case 1: cfgpath = "gammadump.conf"; break;
		case 2: cfgpath = argv[1]; break;
		default: usagehelp(); return 1; break;
	}

	ifstream cfgfd;
	cfgfd.open(cfgpath.c_str());
	if(!cfgfd.is_open()){
		if(cfgpath != "gammadump.conf"){
			setcolour(12);
			cout << "Error opening configfile. Please check your path ('" << cfgpath <<"')." << endl;
			cfgfd.close();
			setcolour(15);
			wait_enter();
			setcolour(7);
			return 1;
		}
		else{
			//we were started just with gammadump and there is no gammadump.conf ? chances are that this is the first start. so let's give the user some help
			setcolour(12);
			cout << "Error opening configfile. Please check your path ('" << cfgpath <<"')." << endl;
			cfgfd.close();
			usagehelp();
			return 1;
		}
	}
	else{
		if(debug){ cout << "#> Opened configfile '" << cfgpath << "'." << endl; }
		//okay, it's open. let's parse.
		while (!cfgfd.eof()){

			getline (cfgfd,line);
			//ignore #comments
			while( line.find_first_of("#") == 0 ){
				getline (cfgfd,line);
			}

			if(line.find("player ") != string::npos){
				string buf(line,line.find("player ")+7,line.length());
				you = buf;
			}
			if(line.find("aologfile ") != string::npos){
				string buf(line,line.find("aologfile ")+10,line.length());
				aologfile = buf;
			}
			if(line.find("chatscript ") != string::npos){
				string buf(line,line.find("chatscript ")+11,line.length());
				chatscript = buf;
			}
			if(line.find("truncate ") != string::npos){
				string buf(line,line.find("truncate ")+9,line.length());
				if(buf == "true"){ truncate = true; }
				else{ truncate = false; }
			}
			if(line.find("teamscript ") != string::npos){
				string buf(line,line.find("teamscript ")+11,line.length());
				teamscript = buf;
			}
			if(line.find("writeinterval ") != string::npos){
				string buf(line,line.find("writeinterval ")+14,line.length());
				writeinterval = atoi(buf.c_str());
			}
			if(line.find("teaminterval ") != string::npos){
				string buf(line,line.find("teaminterval ")+13,line.length());
				teaminterval = atoi(buf.c_str());
			}
			if(line.find("debug ") != string::npos){
				string buf(line,line.find("debug ")+6,line.length());
				if(buf == "true"){ cout << "#> Debug mode enabled." << endl; debug = true; }
				else{ debug = false; }
			}
			if(line.find("colour ") != string::npos){
				string buf(line,line.find("colour ")+7,line.length());
				if(buf == "true"){ colour = true; }
				else{ colour = false; }
			}
		}
		cfgfd.close();
	}

	//check if file exists
	ifstream ilogfd;
	ilogfd.open(aologfile.c_str());
	if (!ilogfd.is_open()){
		setcolour(12);
		cout << "Error opening logfile. Please check your path ('" << aologfile << "')." << endl;
		setcolour(15);
		ilogfd.close();
		wait_enter();
		setcolour(7);
		return 1;
	}
	ilogfd.close();

	//enable signal triggers
	signal(SIGTERM,sigterm);
	signal(SIGINT,sigint);

	//only unix supports usr1,2 signals. windows gets a hotkey hook. if anyone knows how to hook a global hotkey in x11, please let me know.
	#ifndef WIN
		signal(SIGUSR1,sigusr1);
		signal(SIGUSR2,sigusr2);
	#endif

	/* Okay. For now, I'm really fed up with the Unix and Windows API alike. Windows features a GlobalHotkey function,
	 * while Linux needs arcane X11 commands.
	 * Also, any Unix can fork() and signal() while Windows can not. Quite frankly, this sucks.
	 * I'll have a look into this later.
	 *
	 * #ifdef WIN
		//let's fork off a keyboard scanner for our windows users.
		pipe(pipeval);

		int forkval = fork();
		if(forkval == 0){
			//this is the child
			//close read desc of pipe
			close(pipeval[0]);
			char opcode = '0';
			string keyev;

			while(true){
				if ((GetAsyncKeyState(VK_CONTROL) < 0) && (GetAsyncKeyState(VK_SHIFT) < 0) && (GetAsyncKeyState(VK_F1) < 0)) {
					opcode = '1'; //msg: write stats
				}
				if ((GetAsyncKeyState(VK_CONTROL) < 0) && (GetAsyncKeyState(VK_SHIFT) < 0) && (GetAsyncKeyState(VK_F2) < 0)) {
					opcode = '2'; //msg: reset stats
				}

				if(debug){ cout << "Keyreader> Received key event. Writing opcode to pipe:" << opcode << endl; }
				if(opcode != '0'){
					if( write(pipeval[1],opcode,1) < 0 ){ perror("Keyreader"); }
					opcode = '0';
				}
				if(debug){ cout << "Keyreader> Wrote opcode." << endl; }

			}
			return 0;
		}
		//here the parent continues
		//close write desc of pipe
		close(pipeval[1]);
	#endif


	//write pid to tempfile, to allow windows users to send signals with the helper app
	ofstream pidfile;
	pidfile.open("gammadump.pid");
	if(pidfile.is_open()){
		pidfile << getpid() << endl;
	}
	pidfile.close();
	*/
	//---- Here, we consider the program as started, since the checks for the most obvious layer 8 errors have been passed ----//

	#ifndef WIN
		//we don't re-print the keyboard commands if it doesn't need to be, unix only
		doecho(false);
	#endif

	//hello, world!
	setcolour(15);
	cout << "#> This is Gammadump v" << version << "." << endl;
	innerhelp();
	cout << "#> Character: " << you << endl;
	//disabled, so you don't need to edit out your charid in screenshots.// cout << "#> Logfile: " << aologfile << endl;

	//truncate file once
	if (truncate){
		ofstream ologfd;
		ologfd.open(aologfile.c_str());
		ologfd << "";
		ologfd.close();
		cout << "#> Damage logfile truncated." << endl;
	}

	cout << "#> Ready." << endl;
	setcolour(7);

	//here we perma-read the file
	while(bail == false){
		ifstream logfd;
		logfd.open (aologfile.c_str());
		if(logfd.is_open()) {
			//we don't need to reed everything from the beginning, so go to our last position
			logfd.seekg(pos);

			//is there something new in the file?
			logfd.seekg(0, ios::end);
			pos2 = logfd.tellg();
			logfd.seekg(pos);
			delta = pos2 - pos;
			//if (debug){ cout << "Position: " << pos << ", EOF at: " << pos2 << ", Diff: " << delta << endl; }
			pos = pos2;

			if (delta > 0){

				//oh yes, there is. let's have look.
				while (!logfd.eof() ){
					getline (logfd,line);
					isdotted=false;

					//split the lines. preline is needed for settime(), the rest for detections
					string preline(line,0,line.find_first_of("]"));
					line.erase(0,line.find_first_of("]")+1);
					if(debug){ cout << line << endl; }

/* ---------------------- MESSAGE DETECTION --------------------------------- */

					//do we have a team association ?
					if( (line.find(" joined your team.") != string::npos) || (line.find("Joining team.") != string::npos)){
						//who is it ?
						string name;
						if(line.find("Joining team.") != string::npos){
							name = you;
						}
						else{
							string cutname(line,0,line.find(" joined your team."));
							name = cutname;
						}

						int id = getid(name,true);
						if( (isteammember(id,false) == false) && (maxteamdb < MAXPLAYERS) ){
							//add teammember if not in db
							teamdb[maxteamdb] = id;
							maxteamdb++;
							if(debug){ cout << "#> Found & added new team member '" << name << "'. maxteamdb=" << maxteamdb << "." << endl; }
						}
						else{
							if(debug){ cout << "#> Is already teammember: '" << name << "'. maxteamdb=" << maxteamdb << "." << endl; }
						}

						if(!debug){ cout << "t"; isdotted = true; }
					}
					if(line.find(" left your team.") != string::npos){
						//who is it ?
						string name(line,0,line.find(" left your team."));
						int id = getid(name,true);
						if(debug){ cout << "#> Requesting teammember removal: '" << name << "'. maxteamdb=" << maxteamdb << "." << endl; }
						if(isteammember(id,true) == true){
							if(debug){ cout << "#> Removed teammember: '" << name << "'. maxteamdb=" << maxteamdb << "." << endl; }
						}

						if(!debug){ cout << "t"; isdotted = true; }
					}
					if(line.find("You are no longer in a team.") != string::npos){
						//means we have to delete the team array
						int cnt = 0;
						while(cnt <= maxteamdb){
							teamdb[cnt]=0;
							cnt++;
						}
						maxteamdb=0;
						if(debug){ cout << "#> Cleared teamdb." << endl; }
						else{ cout << "t"; isdotted = true; }
					}


					//do we have a pet association ?
					if(line.find("'s pet, ") != string::npos){
					if(line.find(":") != string::npos){
						if (debug){ cout << "#> Found pet!" << endl; }
						settime(preline);

						//get the names
						string owner(line,0,line.find("'s pet, "));
						int petbegin=line.find("'s pet, ")+8;
						int petend=line.find_last_of(":");
						string pet(line,petbegin,petend - petbegin);

						if (debug){ cout << "#> Pet name: " << pet << ", Owner: " << owner << endl; }

						//check if owner&pet is in db or add him, and get his id
						int ownerid = getid(owner,true);
						int petid = getid(pet,true);

						//now link the two and transfer dmg if needed
						//transferring activesecs can't be done here: if owner&pet are active at the same time, the owner would get a penalty for the dd not recognising his pet. so set calcstats' end to 0
						if(!petdb[petid]){
							petdb[petid] = ownerid;
							petcounter[ownerid] = petcounter[ownerid] + 1;

							dmg[ownerid] = dmg[ownerid] + dmg[petid];
							crits[ownerid] = crits[ownerid] + crits[petid];
							hits[ownerid] = hits[ownerid] + hits[petid];

							//[announcement]rrrrrreeeeecaaaalc[/announcement]
							calcstats(ownerid,owner,0,0,0,"",false);

							if (debug){
								cout << "#> Pet not found in petdb, entry created. petdb[" << petid << "] is " << ownerid << "." << endl;
								cout << "#> Transferred stats: " << dmg[petid] << " dmg, " << crits[petid] << " crits, " << hits[petid] << " hits, " << endl;
							}

							//and don't forget to zero the pet's values, or do we want them to show up in the highscore after association ?
							dmg[petid] = 0;
							crits[petid] = 0;
							hits[petid] = 0;

						}
						else{
							if (debug){ cout << "#> Pet found in petdb. petdb[" << petid << "] is " << ownerid << "." << endl; }
						}
						if(!debug){ cout << "p"; isdotted = true; }

					}
					}

					/*
					//is it a miss message ?
					if(line.find(" tried to hit ") != string::npos){
					if(line.find(", but missed!") != string::npos){
						settime(preline);

						if (debug){cout << "#> Miss detected." << endl;}

						//who did the miss?
						string name(line,0,line.find(" tried to hit "));
						name = name_postprocess(name);

						//check if player is in db or add him, and get his id
						int id = getid(name,true);

						//did we get a pet?
						int oldid = id;
						id = petcheck(id);
						name=iddb[id];

						//do we have special chars in the name and it isn't a pet ? => we got a mob. filter it.
						id = mobcheck(oldid,id,name);

						//write the array if good id
						if(id!=0){

							//aaaaand recalculate
							calcstats(id,name,0,0,0,1,"",true);
						}

						if(!debug){ cout << "-"; isdotted = true; }
					}}
					*/

					//is it a nano-hit other message ?
					if(line.find(" was attacked with nanobots from ") != string::npos){
					if(line.find(" points of ") != string::npos){
					if(line.find(" damage.") != string::npos){
						settime(preline);

						//who did it?
						int namebegin=line.find(" was attacked with nanobots from ")+33;
						int nameend=line.find(" for ");
						string name( line,namebegin,nameend-namebegin);
						name = name_postprocess(name);

						if (debug){cout << "#> Nanobot hit detected." << endl;}

						//check if player is in db or add him, and get his id
						int id = getid(name,true);

						//did we get a pet?
						int oldid = id;
						id = petcheck(id);
						name=iddb[id];

						//do we have special chars in the name and it isn't a pet ? => we got a mob. filter it.
						id = mobcheck(oldid,id,name);

						//how much dmg?
						int dmgbegin=line.find(" for ")+5;
						int dmgend=line.find(" points of ");
						string sdmg(line,dmgbegin,dmgend - dmgbegin);
						if (debug){cout << "#> Damage: '" << sdmg << "'" << endl;}

						//write the arrays if good id
						if(id!=0){
							int idmg=atoi(sdmg.c_str());
							calcstats(id,name,idmg,0,1,"",true);
						}
						if(!debug){ cout << "*"; isdotted = true; }

					}
					}
					}

					//is it a hit message ?
					if(line.find(" hit ") != string::npos){
					if(line.find(" points of ") != string::npos){
					if(line.find(" damage.") != string::npos){
						settime(preline);

						//who did it?
						string name(line,0,line.find(" hit "));
						name = name_postprocess(name);

						//check if player is in db or add him, and get his id
						int id = getid(name,true);

						//did we get a pet?
						int oldid = id;
						id = petcheck(id);
						name=iddb[id];

						//do we have special chars in the name and it isn't a pet ? => we got a mob. filter it.
						id = mobcheck(oldid,id,name);

						//crit ?
						int crit = 0;
						int hit = 0;
						if(line.find("Critical hit!") != string::npos){
							crit = 1;
							if (debug){cout << "#> Crit detected." << endl;}
						}
						else {
							hit = 1;
							if (debug){cout << "#> Hit detected." << endl;}
						}

						//how much dmg?
						int dmgbegin=line.find("for ")+4;
						int dmgend=line.find(" points of ");
						string sdmg(line,dmgbegin,dmgend - dmgbegin);
						if (debug){cout << "#> Damage: '" << sdmg << "'" << endl;}


						//what type of dmg?
						int dtypebegin=line.find(" points of ")+11;
						int dtypeend=line.find(" damage");
						string dtype(line,dtypebegin,dtypeend - dtypebegin);
						if (debug){cout << "#> Damage type: '" << dtype << "'" << endl;}

						//write the arrays if good id
						if(id!=0){
							int idmg=atoi(sdmg.c_str());
							calcstats(id,name,idmg,crit,hit,dtype,true);
						}
						if( (!debug) && (crit == 1) ){ cout << "!"; isdotted = true; }
						else if( (!debug) && (hit == 1) ){ cout << ":"; isdotted = true; }

					}}}

					if( (!debug) && (!isdotted) ){ cout << "."; isdotted = true; }
/* ---------------------- MESSAGE DETECTION END ----------------------------- */

				// end of while !logfd.eof()
				}
			//end of delta > 0
			}
		//end of logfd.is_open()
		}
		logfd.close();

		//while keycheck during incoming attackspam would be cool, it just eats up too much CPU power.
		//so we put keyhandling only here. (otherwise one could add it at the end of !logfd.eof() to try it out)
		//this isn't as bad as it sounds, because the end-of-file pointer is defined at the opening of the
		//logfile (and doesn't change dynamically as the file gets bigger), so the program gets here soon enough for user interaction,
		//even if there are many players hitting mobs.
		keycheck();

		//we do this extra check so we don't have to wait for SECSLEEP to finish. = Faster exiting.
		if(bail){ cleanup(); return 0; }

		writeinterval_check();
		teaminterval_check();

		SECSLEEP
	//end of while(bail == false)
	}

	cleanup();
	return 0;
}
