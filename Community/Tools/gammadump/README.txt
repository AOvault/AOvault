Readme for Gammadump
--------------------

Getting started:

	1. Copy gammadump.exe (Windows), gammadump (Linux) or gammadump-osx (Mac)
	to a directory of your choice.
	
	2. Have a look at the config file example.conf :
		You need to create a config file just like this (adjusted to
		your character and paths), the syntax is
		explained in the example.conf . (Best to copy
		it into the same folder where you copied the gammadump
		executable to)
	
	3. Start Gammadump with the path of your configfile as parameter:
		Example: c:\gd\gammadump.exe c:\gd\player.conf

		It might be best to make a shortcut with this command on your
		desktop, one for each character.

	4. Start Gammadump and do damage! :-)


When you are producing damage, or any kind of output that goes into your ao
logfile, Gammadump will print the following characters to show activity:

	p  Pet association (ex.: "Player's pet, Widowmaker Battle Drone: ...")
	t  Team related (ex.: "Player joined your team.")
	*  Nanobot damage by others
	!  Critical hit
	:  Normal hit
	.  Anything else


Gammadump can be controlled with the following keyboard commands:

	d  Shows and dumps highscore table to the chatscript file you
	   specified in the config.
	h  Shows highscore table in arbitrary length.
	i  (debug mode only) Search for a player ID.	
	q  Quit.
	r  Resets all statistics to zero (Useful after completed missions or
	   raids).	
	s  Search for a player and print his/her statistics. If a pet name
	   is entered, it will be replaced with the name of the owner.
	t  Show and dump team highscore to chatscript.
	?  Shows help.

	
You can also control gammadump with signals. The following are supported:

	USR1  Writes all statistics.
	USR2  Resets statistics.

	On Linux & Mac systems, you can use the kill or killall command:
		
		killall -SIGUSR1 gammadump
	
	... would send USR1 to gammadump.
	
	The purpose of this is that you can use a program like KHotKeys
	to assign a global shortcut to control gammadump.
	So, to reset the stats while playing AO, you would hit your shortcut, the
	hotkey application runs the command "killall -SIGUSR2 USR2", which in turn
	will tell gammadump to reset the stats.

	Unfortunately, this does not work in windows because there is no
	signal implementation.

	
Features explained:

	Pet detection:
		Gammadump will look for a message like "Tux's pet,
		Widowmaker Battle Drone: Attack command received.".
		This allows to associate the damage from "Widowmaker Battle
		Drone" with the player "Tux". Every miss, crit, and hit the
		pet has done or will do gets added to the statistics of the
		owner.

	dmg/activemin:
		This "dpam" value is intended to give you a stable dpm value
		that doesn't change (too much ;) if you stop hitting. This
		way, you can see if your new equip results in higher damage
		without getting a lower dpm because you need longer for
		mission.
		You can imagine it as having a separate timer, that only ticks
		if you are hitting mobs. When you stop, the timer stops also,
		after a timeout.

		This timeout is 7 seconds, for the NT's that have nukes that
		need long to cast.

	Leechfactor:
		This value is dpam divided by dpm. If a player never stops
		hitting mobs (= never has a pause > 7 seconds between two hit
		messages), his dpam value will be the same as the dpm value -
		therefore he will have a leechfactor of 1, which is the best
		you can get. If a player has pauses, these two timers will
		differ - the longer the pauses, the higher the difference.

		So, if you are in a mission and some guy has a leechfactor of
		5, and everyone else something around 2, this guy could have
		done more damage, had he been more active.
		But if everyone has roughly the same value, then probably no
		one leeched.
