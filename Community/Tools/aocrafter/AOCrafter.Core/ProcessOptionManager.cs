﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using Twp.Utilities;

namespace AOCrafter.Core
{
    /// <summary>
    /// Description of ProcessOptionManager.
    /// </summary>
    public class ProcessOptionManager
    {
        public ProcessOptionManager()
        {
        }

        private ProcessNodeCollection nodes;
        private Process process;

        private ProcessOptionCollection sources = new ProcessOptionCollection();
        private ProcessOptionCollection targets = new ProcessOptionCollection();
        private ProcessOptionCollection results = new ProcessOptionCollection();

        public ProcessOptionCollection Sources
        {
            get { return this.sources; }
        }

        public ProcessOptionCollection Targets
        {
            get { return this.targets; }
        }

        public ProcessOptionCollection Results
        {
            get { return this.results; }
        }

        public void ParseOptions( ProcessNodeCollection nodes, Process process )
        {
            if( nodes == null )
                throw new ArgumentNullException( "nodes" );
            if( process == null )
                throw new ArgumentNullException( "process" );

            this.sources.Clear();
            this.targets.Clear();
            this.results.Clear();

            if( nodes.Count <= 1 )
                return;

            this.nodes = nodes;
            this.process = process;

            for( int i = 0; i < this.nodes.Count; i++ )
            {
                ProcessNode node = this.nodes[i];
                if( node.Process.Result != this.process.Result )
                {
                    this.AddOption( node.Process.Result, i, this.results, node.Process );
                }
                else if( node.Process.Source != this.process.Source )
                {
                    this.AddOption( node.Process.Source, i, this.sources, node.Process );
                }
                else if( node.Process.Target != this.process.Target )
                {
                    this.AddOption( node.Process.Target, i, this.targets, node.Process );
                }
            }
        }

        private void AddOption( ProcItem item, int i, ProcessOptionCollection options, Process current )
        {
            ProcessOption option = new ProcessOption( item, i );
            int n = options.IndexOf( option );
            if( n >= 0 )
            {
                int index = options[n].Index;
                if( ( current.Source != item && current.Source == this.process.Source && this.nodes[index].Process.Source != this.process.Source ) ||
                    ( current.Target != item && current.Target == this.process.Target && this.nodes[index].Process.Target != this.process.Target ) ||
                    ( current.Result != item && current.Result == this.process.Result && this.nodes[index].Process.Result != this.process.Result ) )
                {
                    options[n].Index = i;
                }
            }
            else
                options.Add( option );
        }
    }
}
