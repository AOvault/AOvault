﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

using Twp.Controls;
using Twp.Data.AO;
using Twp.Utilities;

namespace AOCrafter.Core
{
    /// <summary>
    ///
    /// </summary>
    public static class DatabaseManager
    {
#region Private fields

        const string m_fileName = "AOCrafter.db3";
        static string m_dataPath;
        static Mmdb m_mmdb = new Mmdb();
        static SQLiteDatabase m_processDatabase = new SQLiteDatabase();
        static Cache<ProcItem> m_items = new Cache<ProcItem>();
        static Cache<Process> m_processes = new Cache<Process>();
        static Cache<Tradeskill> m_tradeskills = new Cache<Tradeskill>();

#endregion

#region Properties

        /// <summary>
        /// Gets or sets the path to the folder containing the database.
        /// </summary>
        public static string DataPath
        {
            get { return m_dataPath; }
            set
            {
                m_dataPath = value;
                Twp.Data.AO.DatabaseManager.DataPath = value;
            }
        }

        internal static string FullPath
        {
            get
            {
                if( String.IsNullOrEmpty( m_dataPath ) )
                    return m_fileName;
                return System.IO.Path.Combine( m_dataPath, m_fileName );
            }
        }

        /// <summary>
        /// Gets the Mmdb database used for parsing data.
        /// </summary>
        public static Mmdb Mmdb
        {
            get { return m_mmdb; }
        }

        internal static string MmdbFile
        {
            get
            {
                string fileName = "text.mdb";
                if( !String.IsNullOrEmpty( m_dataPath ) )
                    fileName = System.IO.Path.Combine( m_dataPath, fileName );
                return fileName;
            }
        }

        /// <summary>
        /// Gets the database used for process data.
        /// </summary>
        public static SQLiteDatabase ProcessDatabase
        {
            get { return m_processDatabase; }
        }

        internal static Cache<ProcItem> Items
        {
            get { return m_items; }
        }

        internal static Cache<Process> Processes
        {
            get { return m_processes; }
        }

        internal static Cache<Tradeskill> Tradeskills
        {
            get { return m_tradeskills; }
        }

#endregion

#region Methods

        /// <summary>
        /// Creates and opens the databases
        /// </summary>
        public static void Open()
        {
            Twp.Data.AO.DatabaseManager.Open();
            m_processDatabase.Open( FullPath );
            m_processDatabase.NonQuery( Properties.Resources.SQLInit );
        }

        public static void OpenMmdb( string path )
        {
            if( !m_mmdb.IsLoaded && File.Exists( MmdbFile ) )
                m_mmdb.Open( MmdbFile );
            else
                m_mmdb.Open( path );
        }

        /// <summary>
        /// Closes the databases
        /// </summary>
        public static void Close()
        {
            Twp.Data.AO.DatabaseManager.Close();

            if( m_processDatabase != null )
                m_processDatabase.Close();

            Log.DebugIf( Items.Count > 0, "[DatabaseManager.Close] Items: {0}", Items.Count );
            Log.DebugIf( Processes.Count > 0, "[DatabaseManager.Close] Processes: {0}", Processes.Count );
            Log.DebugIf( Tradeskills.Count > 0, "[DatabaseManager.Close] Tradeskills: {0}", Tradeskills.Count );
        }

        public static void UpdateIfOutdated( string path, bool ripAll )
        {
            string message;
            bool outdated = Twp.Data.AO.DatabaseManager.IsDbOutdated( path, out message );

            int refresh = 1;
            if( ripAll )
                refresh = 1000;
            else if( !outdated )
            {
                if( Twp.Data.AO.DatabaseManager.ItemsDatabase.GetValue( "type", Twp.Data.AO.DatabaseManager.FullPath ) == "partial" )
                    outdated = IsDbOutdated( out message );
            }

            if( outdated )
            {
                DialogResult dr = MessageBox.Show( message, "AOCrafter.Net", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question );
                if( dr == DialogResult.Cancel )
                    Application.Exit();
                else if( dr == DialogResult.Yes )
                {
                    Open();
                    UpdateItems( path, refresh, ripAll );
                }
            }
        }

        public static bool IsDbOutdated( out string message )
        {
            FileInfo local = new FileInfo( Twp.Data.AO.DatabaseManager.FullPath );
            Log.Debug( "[DatabaseManager.IsDbOutdated] Local \"{0}\" Exists? {1}", local.FullName, local.Exists );
            if( !local.Exists )
            {
                message = "Your items database is missing. Would you like to create it now?";
                return true;
            }

            FileInfo proc = new FileInfo( FullPath );
            Log.Debug( "[DatabaseManager.IsDbOutdated] Proc \"{0}\" Exists? {1}", proc.FullName, proc.Exists );
            if( !proc.Exists )
                Log.Fatal( "Failed to locate process database at {0}!", proc.FullName );

            Log.Debug( "[DatabaseManager.IsDbOutdated] Proc.LastWrite={0}, Local.LastWrite={1}", proc.LastWriteTimeUtc, local.LastWriteTimeUtc );
            if( proc.LastWriteTimeUtc > local.LastWriteTimeUtc )
            {
                message = String.Format( "Your items database is out of date. Would you like to update it now?" );
                return true;
            }
            message = String.Empty;
            return false;
        }

        public static void UpdateItems( string path, int refresh, bool all )
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            Log.Debug( "[DatabaseManager.UpdateItems] Creating progress form." );
            ProgressForm progressForm = new ProgressForm();
            progressForm.Tasks = 4;
            progressForm.CanBeCancelled = true;
            progressForm.AutoCloseDelay = 5;
            progressForm.Show();

            Database db = null;
            string tempFileName = "tempdb.db3";
            if( !String.IsNullOrEmpty( m_dataPath ) )
                tempFileName = System.IO.Path.Combine( m_dataPath, tempFileName );

            try
            {
                progressForm.BeginTask( "Preparing database...", 3, 1, false );
                Log.Debug( "[DatabaseManager.UpdateItems] Creating new database." );
                progressForm.PerformStep( "Creating new database...", 1 );
                Application.DoEvents();
                if( progressForm.Canceled )
                    throw new OperationCanceledException();
                db = new Database( tempFileName );
                db.Open();
                db.StartTransaction();

                Log.Debug( "[DatabaseManager.UpdateItems] Initializing database." );
                progressForm.PerformStep( "Initializing database...", 1 );
                Application.DoEvents();
                if( progressForm.Canceled )
                    throw new OperationCanceledException();
                ResourceDatabase rdb = new ResourceDatabase( path );

                progressForm.PerformStep( "Preparing items...", 1 );
                Log.Debug( "[DatabaseManager.UpdateItems] Fetching IDs (all={0}).", all );
                List<int> itemIDs = new List<int>();
                List<int> nanoIDs = new List<int>();
                List<int> iconIDs = new List<int>();

                if( all )
                {
                    itemIDs.AddRange( rdb.GetRecordIds( ResourceDatabase.ItemType ) );
                    nanoIDs.AddRange( rdb.GetRecordIds( ResourceDatabase.NanoType ) );
                    iconIDs.AddRange( rdb.GetRecordIds( ResourceDatabase.IconType ) );
                }
                else
                {
                    // Grab all item IDs from process DB.
                    // We gotta do this manually here, since the Item.GetAll() method
                    // tries to access the Items db, which is empty at this time.
                    SQLiteResults rows = ProcessDatabase.Query( "SELECT items FROM 'items';" );
                    foreach( SQLiteResult row in rows )
                        itemIDs.AddRange( IntArray.FromByteArray( (byte[]) row["items"] ) );

                    // Add skill icons, we don't have item icon IDs at this time.
                    foreach( SkillName key in Enum.GetValues( typeof(SkillName ) ) )
                        iconIDs.Add( Skill.GetIcon( key ) );
                }
                progressForm.FinishTask();
                Application.DoEvents();
                if( progressForm.Canceled )
                    throw new OperationCanceledException();

                // Rip Items
                int count = 0;
                int total = 0;
                Log.Debug( "[DatabaseManager.UpdateItems] Ripping items: {0}.", itemIDs.Count );
                progressForm.BeginTask( "Extracting items from the Database", itemIDs.Count, 80 );
                Application.DoEvents();
                if( progressForm.Canceled )
                    throw new OperationCanceledException();
                for( int i = 0; i < itemIDs.Count; i++ )
                {
                    byte[] data = rdb.GetRecordData( ResourceDatabase.ItemType, itemIDs[i] );
                    using( AOItem item = new AOItem( itemIDs[i], data ) )
                    {
                        item.Save( db );
                        if( !all )
                        {
                            // We now have icon IDs, so add them to the list
                            // (unless we're already grabbing everything)
                            if( item.Icon > 0 && !iconIDs.Contains( item.Icon ) )
                                iconIDs.Add( item.Icon );
                            CheckReferences( item, ref itemIDs, ref nanoIDs );
                            progressForm.UpdateTask( itemIDs.Count );
                        }
                    }
                    Application.DoEvents();
                    if( progressForm.Canceled )
                        throw new OperationCanceledException();
                    count++;
                    total++;
                    if( count >= refresh )
                    {
                        progressForm.PerformStep( "Items extracted", count );
                        count = 0;
                    }
                }
                progressForm.FinishTask();
                Application.DoEvents();
                if( progressForm.Canceled )
                    throw new OperationCanceledException();

                // Rip Nanos
                count = 0;
                total = 0;
                Log.Debug( "[DatabaseManager.UpdateItems] Ripping nanos: {0}.", nanoIDs.Count );
                progressForm.BeginTask( "Extracting nanos from the Database", nanoIDs.Count, 10 );
                Application.DoEvents();
                if( progressForm.Canceled )
                    throw new OperationCanceledException();
                for( int i = 0; i < nanoIDs.Count; i++ )
                {
                    byte[] data = rdb.GetRecordData( ResourceDatabase.NanoType, nanoIDs[i] );
                    using( AONano nano = new AONano( nanoIDs[i], data ) )
                    {
                        nano.Save( db );
                        if( !all )
                        {
                            // We now have icon IDs, so add them to the list
                            // (unless we're already grabbing everything)
                            if( nano.Icon > 0 && !iconIDs.Contains( nano.Icon ) )
                                iconIDs.Add( nano.Icon );
                            CheckReferences( nano, ref itemIDs, ref nanoIDs );
                            progressForm.UpdateTask( itemIDs.Count );
                        }
                    }
                    Application.DoEvents();
                    if( progressForm.Canceled )
                        throw new OperationCanceledException();
                    count++;
                    total++;
                    if( count >= refresh )
                    {
                        progressForm.PerformStep( "Nanos extracted", count );
                        count = 0;
                    }
                }
                progressForm.FinishTask();
                Application.DoEvents();
                if( progressForm.Canceled )
                    throw new OperationCanceledException();

                // Rip Icons
                count = 0;
                total = 0;
                Log.Debug( "[DatabaseManager.UpdateItems] Ripping icons: {0}.", iconIDs.Count );
                progressForm.BeginTask( "Extracting icons from Database", iconIDs.Count, 8 );
                refresh /= 5;
                Application.DoEvents();
                if( progressForm.Canceled )
                    throw new OperationCanceledException();
                foreach( int id in iconIDs )
                {
                    byte[] data = rdb.GetRecordData( ResourceDatabase.IconType, id );
                    AOIcon.Save( id, data, db );
                    Application.DoEvents();
                    if( progressForm.Canceled )
                        throw new OperationCanceledException();
                    count++;
                    total++;
                    if( count >= refresh )
                    {
                        progressForm.PerformStep( "Icons extracted", count );
                        count = 0;
                    }
                }
                progressForm.FinishTask();
                Application.DoEvents();
                if( progressForm.Canceled )
                    throw new OperationCanceledException();

                Log.Debug( "[DatabaseManager.UpdateItems] Commiting transaction." );
                progressForm.BeginTask( "Cleaning up...", 4, 1, false );
                Application.DoEvents();
                progressForm.PerformStep( "Finalizing...", 1 );
                string version = Twp.Data.AO.DatabaseManager.GetVersion( path );
                db.Insert( "localdb", "version", version );
                db.Insert( "localdb", "path", path );
                db.Insert( "localdb", "type", "partial" );
                Application.DoEvents();
                if( progressForm.Canceled )
                    throw new OperationCanceledException();
                db.CommitTransaction();

                progressForm.PerformStep( "Replacing old database...", 1 );
                Application.DoEvents();
                if( progressForm.Canceled )
                    throw new OperationCanceledException();
                progressForm.CanBeCancelled = false; // at this point canceling is pointless.

                string fileName = Twp.Data.AO.DatabaseManager.FullPath;
                if( Twp.Data.AO.DatabaseManager.ItemsDatabase.IsLoaded )
                    Twp.Data.AO.DatabaseManager.ItemsDatabase.Close();
                db.Close();
                if( File.Exists( fileName ) )
                    File.Delete( fileName );
                File.Move( tempFileName, fileName );

                // Copy mmdb...
                fileName = Twp.Utilities.Path.Combine( path, "cd_image", "text", "text.mdb" );
                if( File.Exists( fileName ) )
                {
                    if( File.Exists( MmdbFile ) )
                        File.Delete( MmdbFile );
                    File.Copy( fileName, MmdbFile );
                }
                OpenMmdb( path );

                progressForm.PerformStep( "Reinitializing database...", 1 );
                Application.DoEvents();
                Twp.Data.AO.DatabaseManager.Open(); // reopen db...

                sw.Stop();
                Log.Debug( "[DatabaseManager.UpdateItems] Done. Time elapsed: {0}", sw.Elapsed );
                string elapsed = "Finished! (In ";
                if( sw.Elapsed.Hours > 0 )
                    elapsed += sw.Elapsed.Hours.ToString() + " Hours, ";
                int sec = sw.Elapsed.Seconds;
                if( sw.Elapsed.Milliseconds > 500 )
                    sec += 1;
                elapsed += String.Format( "{0} Minutes and {1} Seconds.", sw.Elapsed.Minutes, sec );
                progressForm.PerformStep( elapsed, 1 );
                progressForm.FinishTask();
            }
            catch( OperationCanceledException )
            {
                sw.Stop();
                Log.Debug( "[DatabaseManager.UpdateItems] Aborted! Time elapsed: {0}", sw.Elapsed );
                if( db != null )
                {
                    db.Close();
                    File.Delete( db.FileName );
                }
            }
            progressForm.Close();
        }

        private static void CheckReferences( AOItem item, ref List<int> items, ref List<int> nanos )
        {
            ItemInfo info = ItemParser.Parse( item );
            foreach( RequirementList reqs in info.Actions.Values )
            {
                CheckRequirements( reqs, ref items, ref nanos );
            }
            foreach( FunctionList events in info.Events.Values )
            {
                foreach( List<Function> funcs in events.Values )
                {
                    foreach( Function func in funcs )
                    {
                        switch( (FunctionKey) func.Key )
                        {
                            case FunctionKey.AreaCastNano:
                            case FunctionKey.CastNano:
                            case FunctionKey.CastNano2:
                            case FunctionKey.CastStunNano:
                            case FunctionKey.TeamCastNano:
                            case FunctionKey.PetsCastNano:
                            case FunctionKey.UploadNano:
                                int id = Convert.ToInt32( func.Arguments[0] );
                                ;
                                if( id > 0 && !nanos.Contains( id ) )
                                    nanos.Add( id );
                                break;
                        }
                        CheckRequirements( func.Requirements, ref items, ref nanos );
                    }
                }
            }
        }

        private static void CheckRequirements( RequirementList reqs, ref List<int> items, ref List<int> nanos )
        {
            foreach( Requirement req in reqs )
            {
                switch( (OperatorKey) req.Op )
                {
                    case OperatorKey.HasItem:
                    case OperatorKey.HasNotItem:
                    case OperatorKey.HasWornItem:
                    case OperatorKey.HasNotWornItem:
                    case OperatorKey.HasWieldedItem:
                    case OperatorKey.HasNotWieldedItem:
                        int id = Convert.ToInt32( req.Value );
                        if( id > 0 && !items.Contains( id ) )
                            items.Add( id );
                        break;
                    case OperatorKey.HasFormula:
                    case OperatorKey.HasNotFormula:
                    case OperatorKey.HasRunningNano:
                    case OperatorKey.HasNotRunningNano:
                        id = Convert.ToInt32( req.Value );
                        if( id > 0 && !nanos.Contains( id ) )
                            nanos.Add( id );
                        break;
                }
            }
        }
        /// <summary>
        /// Retrieves the value of the <paramref name="key"/> from the specified <paramref name="db"/>.
        /// </summary>
        /// <param name="key">The key to find.</param>
        /// <param name="db">The database to search.</param>
        /// <returns>A string containing the value of the key, if found; otherwise an empty string.</returns>
        public static string GetValue( string key, SQLiteDatabase db )
        {
            if( string.IsNullOrEmpty( key ) )
                throw new ArgumentException( "key cannot be empty!" );
            if( db == null )
                throw new ArgumentNullException( "db" );

            bool loaded = false;
            if( !db.IsLoaded && File.Exists( db.FileName ) )
            {
                db.Open();
                loaded = true;
            }

            SQLiteResult row = db.QuerySingle( "SELECT value FROM 'localdb' WHERE key='{0}'", key );
            if( loaded )
                db.Close();

            return row.Count == 0 ? String.Empty : Convert.ToString( row["value"] );
        }

        public static List<DatabaseStatistics> Statistics()
        {
            SQLiteDatabase db = DatabaseManager.ProcessDatabase;
            if( db == null )
                return null;

            Stopwatch sw = new Stopwatch();
            sw.Start();
            List<DatabaseStatistics> stats = new List<DatabaseStatistics>();
            SQLiteResult row;

            // Tradeskills
            Log.Debug( "[DatabaseManager.Statistics] Counting tradeskills..." );
            row = db.QuerySingle( "SELECT COUNT(*) FROM 'tradeskills';" );
            stats.Add( new DatabaseStatistics( "Tradeskills", Convert.ToInt32( row["COUNT(*)"] ) ) );

            row = db.QuerySingle( "SELECT COUNT(*) FROM 'tradeskills' WHERE LENGTH(`processes`) <= 4;" );
            stats.Add( new DatabaseStatistics( "-- empty --", Convert.ToInt32( row["COUNT(*)"] ) ) );

            // Processes
            Log.Debug( "[DatabaseManager.Statistics] Counting processes..." );
            row = db.QuerySingle( "SELECT COUNT(*) FROM 'processes';" );
            stats.Add( new DatabaseStatistics( "Processes", Convert.ToInt32( row["COUNT(*)"] ) ) );

            ProcessList unusedProcs = Process.FindUnused();
            if( unusedProcs != null )
                stats.Add( new DatabaseStatistics( "-- unused --", unusedProcs.Count ) );

            // Items
            Log.Debug( "[DatabaseManager.Statistics] Counting items..." );
            row = db.QuerySingle( "SELECT COUNT(*) FROM 'items';" );
            stats.Add( new DatabaseStatistics( "Items", Convert.ToInt32( row["COUNT(*)"] ) ) );

            ItemList unusedItems = ProcItem.FindUnused();
            if( unusedItems != null )
                stats.Add( new DatabaseStatistics( "-- unused --", unusedItems.Count ) );

            sw.Stop();
            Log.Debug( "[DatabaseManager.Statistics] Done. Time elapsed: {0}", sw.Elapsed );
            return stats;
        }

#endregion
    }
}
