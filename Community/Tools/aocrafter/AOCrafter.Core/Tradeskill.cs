﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;

using Twp.Data.AO;
using Twp.Utilities;

namespace AOCrafter.Core
{
#region Enums

    public enum Expansions
    {
        None,
        Notum_Wars,
        Shadowlands,
        Alien_Invasion,
        Lost_Eden,
        Legacy_Of_Xan
    }

    [Flags]
    public enum TradeskillFlags
    {
        None = 0,
        NoRecursion = 1 << 0,
    }

#endregion

    public class Tradeskill : ICrafterData, INotifyPropertyChanged, IEquatable<Tradeskill>, IComparable<Tradeskill>, IComparable, IDisposable
    {

#region Constructors

        public Tradeskill()
        {
        }

        public Tradeskill( ProcessList processes )
        {
            this.processes = processes;
            if( processes.Count > 0 )
                this.name = processes[0].Result.ToString();
        }

        public Tradeskill( SQLiteResult row )
        {
            this.id = Convert.ToInt32( row["id"] );
            this.name = (string) row["name"];
            this.groupName = (string) row["groupName"];
            this.expansion = (Expansions) Convert.ToInt32( row["expansion"] );
            this.flags = (TradeskillFlags) Convert.ToInt32( row["flags"] );
            this.processes = ProcessList.FromByteArray( (byte[]) row["processes"] );
            DatabaseManager.Tradeskills.Add( this.id, this );
        }

#endregion

#region Private fields

        private int id = 0;
        private string name = String.Empty;
        private string groupName = String.Empty;
        private Expansions expansion = Expansions.None;
        private TradeskillFlags flags = TradeskillFlags.None;
        private ProcessList processes = new ProcessList();
        private ProcessNodeCollection nodes = null;

#endregion

#region Properties

        /// <summary>
        /// Gets the Id of the tradeskill.
        /// </summary>
        [Browsable( false )]
        public int Id
        {
            get { return this.id; }
        }

        /// <summary>
        /// Gets or sets the name of the tradeskill.
        /// </summary>
        [Category( "Properties" )]
        [Description( "The name of the tradeskill, as it is listed in the client." )]
        [DefaultValue( "" )]
        public string Name
        {
            get { return this.name; }
            set
            {
                if( this.name != value )
                {
                    this.name = value;
                    this.OnPropertyChanged( new PropertyChangedEventArgs( "Name" ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the group name for the tradeskill.
        /// </summary>
        [Category( "Properties" )]
        [Description( "The name of the group which this tradeskill belongs to." )]
        [DefaultValue( "" )]
        [TypeConverter( typeof( GroupNameConverter ) )]
        public string GroupName
        {
            get { return this.groupName; }
            set
            {
                if( this.groupName != value )
                {
                    this.groupName = value;
                    this.OnPropertyChanged( new PropertyChangedEventArgs( "GroupName" ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the expansion flag for the tradeskill.
        /// </summary>
        [Category( "Properties" )]
        [Description( "The Expansion that is required for using/obtaining this item." )]
        [DefaultValue( Expansions.None )]
        public Expansions Expansion
        {
            get { return this.expansion; }
            set
            {
                if( this.expansion != value )
                {
                    this.expansion = value;
                    this.OnPropertyChanged( new PropertyChangedEventArgs( "Expansion" ) );
                }
            }
        }

        [Category( "Properties" )]
        [Description( "Any flags that this tradeskill might need." )]
        [DefaultValue( TradeskillFlags.None )]
        public TradeskillFlags Flags
        {
            get { return this.flags; }
            set
            {
                if( this.flags != value )
                {
                    this.flags = value;
                    this.OnPropertyChanged( new PropertyChangedEventArgs( "Flags" ) );
                }
            }
        }

        /// <summary>
        /// Returns a <c ref="ProcessList" /> containing the process(es) that makes the final item(s) for this tradeskill.
        /// </summary>
        //[Browsable( false )]
        public ProcessList Processes
        {
            get { return this.processes; }
        }

        public ProcessNodeCollection Nodes
        {
            get
            {
                if( this.nodes == null )
                    this.nodes = ProcessNodeCollection.Create( this.processes, !Flag.IsSet( this.flags, TradeskillFlags.NoRecursion ) );
                return this.nodes;
            }
        }

        public int LowQL
        {
            get
            {
                if( this.processes != null && this.processes.Count > 0 )
                    return this.processes[0].LowQL;
                else
                    return -1;
            }
        }

        public int HighQL
        {
            get
            {
                if( this.processes != null && this.processes.Count > 0 )
                    return this.processes[0].HighQL;
                else
                    return -1;
            }
        }

        public bool StaticQL
        {
            get { return this.LowQL == this.HighQL; }
        }

#endregion

#region Events

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged( PropertyChangedEventArgs e )
        {
            if( this.PropertyChanged != null )
                this.PropertyChanged( this, e );
        }

#endregion

#region Methods

        public static Tradeskill Load( int id )
        {
            if( DatabaseManager.Tradeskills.Contains( id ) )
            {
                return DatabaseManager.Tradeskills[id];
            }

            SQLiteDatabase db = DatabaseManager.ProcessDatabase;
            if( db == null )
                return null;

            SQLiteResult row = db.QuerySingle( "SELECT * FROM 'tradeskills' WHERE ID='{0}' LIMIT 1", id.ToString() );
            if( row.Count <= 0 )
                return null;

            return new Tradeskill( row );
        }

        public void Save()
        {
            SQLiteDatabase db = DatabaseManager.ProcessDatabase;
            if( db == null )
                return;

            if( this.nodes != null )
                this.nodes.Save();

            List<SQLiteArgument> args = new List<SQLiteArgument>();
            args.Add( new SQLiteArgument( "name", DbType.String, this.name ) );
            args.Add( new SQLiteArgument( "groupName", DbType.String, this.groupName ) );
            args.Add( new SQLiteArgument( "expansion", DbType.Int32, this.expansion ) );
            args.Add( new SQLiteArgument( "flags", DbType.Int32, this.flags ) );
            args.Add( new SQLiteArgument( "processes", DbType.Binary, this.processes.ToByteArray() ) );
            if( this.id < 1 )
            {
                this.id = db.Insert( "tradeskills", args.ToArray() );
                DatabaseManager.Tradeskills.Add( this.id, this );
            }
            else
            {
                args.Insert( 0, new SQLiteArgument( "id", DbType.Int32, this.id ) );
                db.Update( "tradeskills", args.ToArray() );
            }

            if( groupNames != null && !groupNames.Contains( this.groupName ) )
                groupNames.Add( this.groupName );
        }

        public void Delete()
        {
            if( this.id > 0 )
            {
                SQLiteDatabase db = DatabaseManager.ProcessDatabase;
                if( db == null )
                    return;

                db.NonQuery( "DELETE FROM 'tradeskills' WHERE ID='{0}'", this.id.ToString() );
            }
            this.Dispose();
        }

        public static TradeskillList Search( string[] args )
        {
            SQLiteDatabase db = DatabaseManager.ProcessDatabase;
            if( db == null )
                return null;

            TradeskillList tradeskills = new TradeskillList();
            ProcessList processes = Process.Search( args );
            Search( processes, ref tradeskills, true );
            return tradeskills;
        }

        public static TradeskillList Search( Process process )
        {
            TradeskillList tradeskills = new TradeskillList();
            Search( Process.Search( process ), ref tradeskills, true );
            return tradeskills;
        }

        public static TradeskillList Search( ProcItem item, Tradeskill exclude )
        {
            TradeskillList tradeskills = new TradeskillList();
            Search( Process.Search( item, true ), ref tradeskills, false );
            if( exclude != null )
                tradeskills.Remove( exclude );
            return tradeskills;
        }

        private static void Search( ProcessList processes, ref TradeskillList tradeskills, bool recursive )
        {
            SQLiteDatabase db = DatabaseManager.ProcessDatabase;
            if( db == null )
                return;

            foreach( Process process in processes )
            {
                SQLiteResults rows = db.Query( "SELECT * FROM 'tradeskills' WHERE IntArray(processes,'{0}') > 0 ORDER BY groupName, name", process.Id.ToString() );
                foreach( SQLiteResult row in rows )
                {
                    Tradeskill tradeskill = new Tradeskill( row );
                    if( Flag.IsSet( tradeskill.flags, TradeskillFlags.NoRecursion ) )
                        recursive = false;
                    tradeskills.Add( tradeskill );
                }
                if( recursive )
                    Search( Process.Search( process ), ref tradeskills, true );
            }
        }

        public static TradeskillList GetAll()
        {
            SQLiteDatabase db = DatabaseManager.ProcessDatabase;
            if( db == null )
                return null;

            TradeskillList tradeskills = new TradeskillList();
            SQLiteResults rows = db.Query( "SELECT * FROM 'tradeskills' ORDER BY groupName, name" );
            foreach( SQLiteResult row in rows )
            {
                tradeskills.Add( new Tradeskill( row ) );
            }
            return tradeskills;
        }

        public static TradeskillList FromGroup( string groupName )
        {
            SQLiteDatabase db = DatabaseManager.ProcessDatabase;
            if( db == null )
                return null;

            TradeskillList tradeskills = new TradeskillList();
            SQLiteResults rows = db.Query( "SELECT * FROM 'tradeskills' WHERE groupName='{0}' ORDER BY name", groupName );
            foreach( SQLiteResult row in rows )
            {
                tradeskills.Add( new Tradeskill( row ) );
            }
            return tradeskills;
        }

        public void Calculate( int ql )
        {
            this.Nodes.Calculate( ql );
        }

        public bool Calculate( SkillsList skills )
        {
            bool calculated = false;
            try
            {
                int ql = this.Nodes.SelectedNode.Process.HighQL;
                do
                {
                    this.Calculate( ql );
                    if( CheckSkills( this.Nodes, skills ) )
                        break;

                    ql--;
                }
                while( ql > 0 );
                if( ql > 0 )
                    calculated = true;
            }
            catch( SkillNotSetException ex )
            {
                Log.Error( "Unable to calculate max QL:\n{0} is not set.", Skill.GetName( ex.Skill ) );
            }
            return calculated;
        }

        private bool CheckSkills( ProcessNodeCollection nodes, SkillsList skills )
        {
            foreach( ProcessNode node in nodes )
            {
                foreach( Skill skill in node.Process.Skills )
                {
                    if( !skills.Contains( skill.Key ) )
                        throw new SkillNotSetException( skill.Key );

                    if( skills[skill.Key].Value < skill.FinalValue )
                        return false;
                }
                
                if( !CheckSkills( node.Sources, skills ) )
                    return false;
                if( !CheckSkills( node.Targets, skills ) )
                    return false;
            }
            return true;
        }

        public void ResetNodes()
        {
            if( this.nodes != null )
                this.nodes.Dispose();
            this.nodes = null;
        }

        public static string GetExpansionName( Expansions key )
        {
            return key.ToString().Replace( '_', ' ' );
        }

#endregion

#region Group names

        private static StringCollection groupNames;
        public static StringCollection GroupNames
        {
            get
            {
                if( groupNames == null )
                    GetGroupNames();
                return groupNames;
            }
        }

        private static void GetGroupNames()
        {
            groupNames = new StringCollection();
            SQLiteDatabase db = DatabaseManager.ProcessDatabase;
            if( db == null )
                return;

            SQLiteResults rows = db.Query( "SELECT DISTINCT `groupName` FROM `tradeskills`;" );
            foreach( SQLiteResult row in rows )
            {
                groupNames.Add( (string) row["groupName"] );
            }
        }

        public override string ToString()
        {
            if( String.IsNullOrEmpty( this.name ) )
                return base.ToString();
            else
                return this.name;
        }

#endregion

#region IEquatable implementation

        public override int GetHashCode()
        {
            int hashCode = 0;
            unchecked
            {
                hashCode += 1000000007 * id.GetHashCode();
                if( name != null )
                    hashCode += 1000000009 * name.GetHashCode();
                if( groupName != null )
                    hashCode += 1000000021 * groupName.GetHashCode();
                hashCode += 1000000033 * expansion.GetHashCode();
                if( processes != null )
                    hashCode += 1000000087 * processes.GetHashCode();
            }
            return hashCode;
        }

        public bool Equals( Tradeskill other )
        {
            if( other == null )
                return false;
            else
                return this.id.Equals( other.id );
        }

        public override bool Equals( object obj )
        {
            Tradeskill other = obj as Tradeskill;
            if( other != null )
                return this.Equals( other );
            else
                return false;
        }

        public static bool operator ==( Tradeskill item1, Tradeskill item2 )
        {
            if( object.ReferenceEquals( item1, item2 ) )
                return true;
            if( object.ReferenceEquals( item1, null ) )
                return false;
            if( object.ReferenceEquals( item2, null ) )
                return false;

            return item1.Equals( item2 );
        }

        public static bool operator !=( Tradeskill item1, Tradeskill item2 )
        {
            if( object.ReferenceEquals( item1, item2 ) )
                return false;
            if( object.ReferenceEquals( item1, null ) )
                return true;
            if( object.ReferenceEquals( item2, null ) )
                return true;

            return !item1.Equals( item2 );
        }

#endregion

#region IComparable interface

        public int CompareTo( Tradeskill other )
        {
            if( other == null )
                return 1;
            
            if( this.name == null && other.name == null )
                return 0;
            if( this.name == null )
                return -1;
            if( other.name == null )
                return 1;

            if( this.expansion != other.expansion )
                return this.expansion.CompareTo( other.expansion );
            else
                return this.name.CompareTo( other.name );
        }

        public int CompareTo( object obj )
        {
            if( obj == null )
                return 1;

            Tradeskill other = (Tradeskill) obj;
            if( other == null )
                return 1;

            return this.CompareTo( other );
        }

#endregion

#region IDisposable interface

        private bool disposed = false;

        public void Dispose()
        {
            this.Dispose( true );
        }

        private void Dispose( bool disposing )
        {
            if( !this.disposed )
            {
                if( !DatabaseManager.Tradeskills.Remove( this.id ) )
                {
                    Log.Debug( "[Tradeskill.Dispose] Not in cache: {0}", this );
                }
                if( disposing )
                {
                    foreach( Process process in this.processes )
                    {
                        process.Dispose();
                    }
                    //this.result.Dispose();
                }
                this.disposed = true;
            }
        }

#endregion
    }

#region Group name converter

    class GroupNameConverter : StringConverter
    {
        public override bool GetStandardValuesSupported( ITypeDescriptorContext context )
        {
            return true;
        }

        public override bool GetStandardValuesExclusive( ITypeDescriptorContext context )
        {
            return false;
        }

        public override TypeConverter.StandardValuesCollection GetStandardValues( ITypeDescriptorContext context )
        {
            return new StandardValuesCollection( Tradeskill.GroupNames );
        }
    }

#endregion
}
