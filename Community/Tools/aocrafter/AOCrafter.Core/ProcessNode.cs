﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using Twp.Utilities;

namespace AOCrafter.Core
{
    public class ProcessNode : IEquatable<ProcessNode>, IDisposable
    {
#region Constructor

        public ProcessNode( ProcessNodeCollection owner )
        {
            this.owner = owner;
            this.sources = new ProcessNodeCollection( this );
            this.targets = new ProcessNodeCollection( this );
        }

        public ProcessNode( ProcessNodeCollection owner, ProcessNode other )
        {
            this.process = other.process;
            this.count = other.count;
            this.owner = owner;
            this.sources = new ProcessNodeCollection( this, other.sources );
            this.targets = new ProcessNodeCollection( this, other.targets );
        }

#endregion

#region Private Fields

        private Process process;
        private int count = 1;
        private bool modified = false;

        private ProcessNodeCollection owner;
        private ProcessNodeCollection sources;
        private ProcessNodeCollection targets;

#endregion

#region Properties

        public Process Process
        {
            get { return this.process; }
            set { this.process = value; }
        }

        public int Count
        {
            get { return this.count; }
            set { this.count = value; }
        }

        public int RealCount
        {
            get { return this.HasParent ? this.count * this.ParentNode.RealCount : this.count; }
        }

        public bool Modified
        {
            get { return this.modified; }
            set { this.modified = value; }
        }

        internal ProcessNodeCollection Owner
        {
            get { return this.owner; }
            set { this.owner = value; }
        }

        public ProcessNodeCollection Sources
        {
            get { return this.sources; }
        }

        public ProcessNodeCollection Targets
        {
            get { return this.targets; }
        }

        internal ProcessNode NextNode
        {
            get
            {
                if( !this.HasParent )
                    return null;
                if( this.ParentNode.Sources == this.owner &&
                    this.ParentNode.Targets.FirstNode != null )
                    return this.ParentNode.Targets.FirstNode;
                return this.ParentNode;
            }
        }

        internal ProcessNode PreviousNode
        {
            get
            {
                if( this.Targets.SelectedNode != null )
                    return this.Targets.SelectedNode;
                if( this.Sources.SelectedNode != null )
                    return this.Sources.SelectedNode;

                ProcessNode currentNode = this;
                while( currentNode.HasParent )
                {
                    if( currentNode.ParentNode.Targets == currentNode.Owner &&
                        currentNode.ParentNode.Sources.SelectedNode != null )
                        return currentNode.ParentNode.Sources.SelectedNode;

                    currentNode = currentNode.ParentNode;
                }
                return null;
            }
        }

        internal ProcessNode ParentNode
        {
            get { return this.Owner.Owner; }
        }

        internal bool HasParent
        {
            get { return this.Owner != null && this.Owner.Owner != null; }
        }

#endregion

#region IDisposable interface

        private bool disposed = false;

        public void Dispose()
        {
            this.Dispose( true );
        }

        private void Dispose( bool disposing )
        {
            if( !this.disposed )
            {
                if( disposing )
                {
                    this.sources.Dispose();
                    this.targets.Dispose();
                    this.process.Dispose();
                }
                this.disposed = true;
            }
        }

#endregion

#region IEquatable interface

        public override int GetHashCode()
        {
            int hashCode = 0;
            unchecked
            {
                if( this.process != null )
                    hashCode += 1000000007 * this.process.GetHashCode();
            }
            return hashCode;
        }

        public bool Equals( ProcessNode other )
        {
            if( other == null )
                return false;

            return this.process.Equals( other.process );
        }

        public override bool Equals( object obj )
        {
            ProcessNode other = obj as ProcessNode;
            if( other != null )
                return this.Equals( other );
            else
                return false;
        }

        public static bool operator ==( ProcessNode node1, ProcessNode node2 )
        {
            if( object.ReferenceEquals( node1, node2 ) )
                return true;
            if( object.ReferenceEquals( node1, null ) )
                return false;
            if( object.ReferenceEquals( node2, null ) )
                return false;

            return node1.Equals( node2 );
        }

        public static bool operator !=( ProcessNode node1, ProcessNode node2 )
        {
            if( object.ReferenceEquals( node1, node2 ) )
                return false;
            if( object.ReferenceEquals( node1, null ) )
                return true;
            if( object.ReferenceEquals( node2, null ) )
                return true;

            return !node1.Equals( node2 );
        }

        public override string ToString()
        {
            return String.Format( "[{0} Count={1}]", this.process, this.count );
        }

#endregion
    }
}
