﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Windows.Forms;

using Twp.Data.AO;
using Twp.Utilities;

namespace AOCrafter.Core
{
    public class ProcessNodeCollection : List<ProcessNode>, IDisposable
    {
        public ProcessNodeCollection( ProcessNode owner )
        {
            this.owner = owner;
        }

        public ProcessNodeCollection( ProcessNode owner, ProcessNodeCollection other )
        {
            this.owner = owner;
            foreach( ProcessNode node in other )
            {
                this.Add( new ProcessNode( this, node ) );
            }
        }

#region Private Fields

        private ProcessNode owner;
        private int selectedIndex = 0;
        private static Dictionary<int, ProcessNode> uniqueNodes = new Dictionary<int, ProcessNode>();

#endregion

#region Properties

        internal ProcessNode Owner
        {
            get { return this.owner; }
            set { this.owner = value; }
        }

        public int SelectedIndex
        {
            get { return this.selectedIndex; }
            set { this.selectedIndex = value; }
        }

        public ProcessNode SelectedNode
        {
            get
            {
                if( this.Count <= 0 || this.selectedIndex < 0 || this.selectedIndex >= this.Count )
                    return null;
                if( Flag.IsSet( this[this.selectedIndex].Process.Flags, ProcessFlags.LinkProcess ) && !this.IsResult )
                    return null;
                return this[this.selectedIndex];
            }
        }

        public ProcessNode FirstNode
        {
            get
            {
                if( this.SelectedNode != null )
                {
                    if( this.SelectedNode.Sources.FirstNode != null )
                        return this.SelectedNode.Sources.FirstNode;
                    if( this.SelectedNode.Targets.FirstNode != null )
                        return this.SelectedNode.Targets.FirstNode;
                }
                return this.SelectedNode;
            }
        }
        
        public ProcessNodeCollection TopLevel
        {
            get
            {
                ProcessNodeCollection top = this;
                while( top.Owner != null && top.Owner.Owner != null )
                    top = top.Owner.Owner;
                return top;
            }
        }

        internal bool IsResult
        {
            get { return this.Owner == null || this.Owner.Owner == null; }
        }

#endregion

#region Methods

        public void Save()
        {
            foreach( ProcessNode node in this )
            {
                if( node.Modified )
                {
                    node.Process.Save();
                    node.Modified = false;
                }
                node.Sources.Save();
                node.Targets.Save();
            }
        }

        public IEnumerable<ProcessNode> EnumProcesses()
        {
            ProcessNode node = this.FirstNode;
            while( node != null )
            {
                yield return node;
                node = node.NextNode;
            }
        }
        
        public IEnumerable<ProcessNode> EnumProcessesReverse()
        {
            ProcessNode node = this.SelectedNode;
            while( node != null )
            {
                yield return node;
                node = node.PreviousNode;
            }
        }

        public ProcessNode this[ Process process ]
        {
            get
            {
                foreach( ProcessNode node in this )
                {
                    if( node.Process == process )
                        return node;
                }
                return null;
            }
        }

        public bool Contains( Process process )
        {
            return this[process] != null;
        }

        public void Remove( Process process )
        {
            ProcessNode node = this[process];
            if( node != null )
                this.Remove( node );
        }

        public static ProcessNodeCollection Create( ProcessList processes, bool recursive )
        {
            uniqueNodes.Clear();
            ProcessNodeCollection nodes = new ProcessNodeCollection( null );
            foreach( Process process in processes )
            {
                CreateNode( process, nodes, recursive, true );
            }
            nodes.selectedIndex = 0;
            return nodes;
        }

        private static ProcessNodeCollection CreateChildren( ProcItem item, ProcessNodeCollection nodes, bool unique )
        {
            SQLiteDatabase db = DatabaseManager.ProcessDatabase;
            if( db == null )
                return null;

            SQLiteResults rows = db.Query( "SELECT * FROM 'processes' WHERE result='{0}';", item.Id.ToString() );
            foreach( SQLiteResult row in rows )
            {
                Process process = new Process( row );
                CreateNode( process, nodes, !Flag.IsSet( process.Flags, ProcessFlags.LinkProcess ), unique );
            }
            nodes.selectedIndex = 0;
            return nodes;
        }

        private static void CreateNode( Process process, ProcessNodeCollection nodes, bool recursive, bool unique )
        {
            if( unique && uniqueNodes.ContainsKey( process.Id ) )
            {
                bool found = false;
                foreach( ProcessNode node in nodes.TopLevel.EnumProcessesReverse() )
                {
                    if( node.Process == process )
                    {
                        node.Count++;
                        found = true;
                        break;
                    }
                }
                if( !found )
                    nodes.Add( new ProcessNode( nodes, uniqueNodes[process.Id] ) );
            }
            else
            {
                ProcessNode node = new ProcessNode( nodes );
                node.Process = process;
                nodes.Add( node );
                nodes.selectedIndex = nodes.IndexOf( node );
                if( recursive )
                {
                    CreateChildren( process.Target, node.Targets, unique );
                    CreateChildren( process.Source, node.Sources, unique );
                }
                if( unique )
                    uniqueNodes.Add( process.Id, node );
            }
        }

        public void Calculate( int ql )
        {
            foreach( ProcessNode node in this )
            {
                Application.DoEvents();
                if( Flag.IsSet( node.Process.Flags, ProcessFlags.LinkProcess ) && !this.IsResult )
                    continue;
                node.Process.Calculate( ql );
                node.Sources.Calculate( node.Process.FinalSource.QL );
                node.Targets.Calculate( node.Process.FinalTarget.QL );
            }
        }

        public void Reset()
        {
            this.selectedIndex = 0;
            foreach( ProcessNode node in this )
            {
                node.Sources.Reset();
                node.Targets.Reset();
            }
        }

#endregion

#region IDisposable interface

        private bool disposed = false;

        public void Dispose()
        {
            this.Dispose( true );
        }

        private void Dispose( bool disposing )
        {
            if( !this.disposed )
            {
                if( disposing )
                {
                    foreach( ProcessNode node in this )
                        node.Dispose();
                }
                this.disposed = true;
            }
        }

#endregion
    }
}
