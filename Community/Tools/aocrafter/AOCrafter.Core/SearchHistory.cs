﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace AOCrafter.Core
{
    public static class SearchHistory
    {
        private static AutoCompleteStringCollection items = new AutoCompleteStringCollection();

        public static void Read()
        {
            if( !File.Exists( "SearchHistory.xml" ) )
                return;

            XmlDocument doc = new XmlDocument();
            doc.Load( "SearchHistory.xml" );

            lock( items )
            {
                XmlNodeList nodes = doc.GetElementsByTagName( "String" );
                foreach( XmlNode node in nodes )
                {
                    items.Add( node.Value );
                }
            }
        }

        public static void Write()
        {
            lock( items )
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.CloseOutput = true;
                settings.Indent = true;

                XmlWriter writer = XmlWriter.Create( "SearchHistory.xml", settings );

                writer.WriteStartDocument();
                writer.WriteStartElement( "Root" );

                foreach( string text in items )
                {
                    writer.WriteElementString( "String", text );
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Flush();
                writer.Close();
            }
        }

        public static void Prepare( TextBox textBox )
        {
            textBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            textBox.AutoCompleteSource = AutoCompleteSource.CustomSource;
            textBox.AutoCompleteCustomSource = items;
        }

        public static void Add( string text )
        {
            if( !items.Contains( text ) )
                items.Add( text );
        }
    }
}
