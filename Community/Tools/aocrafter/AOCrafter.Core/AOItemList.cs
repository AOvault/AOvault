﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.ComponentModel;

using Twp.Data.AO;
using Twp.Utilities;

namespace AOCrafter.Core
{
    /// <summary>
    ///
    /// </summary>
    public class AOItemList : BindingList<AOItem>
    {
        public AOItemList() : base()
        {
        }

        public AOItemList( IList<AOItem> items ) : base( items )
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="item"></param>
        public new void Add( AOItem item )
        {
            if( !base.Contains( item ) )
            {
                base.Add( item );
            }
            else
            {
                Log.Debug( "[AOItemCollection.Add] Item already in collection: {0}", item );
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="ql"></param>
        /// <param name="low"></param>
        /// <param name="high"></param>
        /// <returns></returns>
        public bool GetNearest( ref Item item )
        {
            if( this.Count == 0 )
                return false;
            else if( this.Count == 1 )
            {
                item.ItemLow = this[0];
                item.ItemHigh = this[0];
                item.KeyLow = item.ItemLow.Id;
                item.KeyHigh = item.ItemHigh.Id;
                return true;
            }
            else if( this.Count == 2 )
            {
                item.ItemLow = this[0];
                item.ItemHigh = this[1];
                item.KeyLow = item.ItemLow.Id;
                item.KeyHigh = item.ItemHigh.Id;
                return true;
            }

            item.ItemLow = this[0];
            item.ItemHigh = this[this.Count - 1];
            foreach( AOItem aoitem in this )
            {
                if( aoitem.Ql == item.Ql )
                {
                    item.ItemLow = aoitem;
                    item.ItemHigh = aoitem;
                    break;
                }
                if( aoitem.Ql <= item.Ql && aoitem.Ql > item.ItemLow.Ql )
                    item.ItemLow = aoitem;
                if( aoitem.Ql >= item.Ql && aoitem.Ql < item.ItemHigh.Ql )
                    item.ItemHigh = aoitem;
            }
            item.KeyLow = item.ItemLow.Id;
            item.KeyHigh = item.ItemHigh.Id;

            Log.Debug( "[AOItemCollection.GetNearest] Low: {0} High: {1}", item.ItemLow, item.ItemHigh );
            return true;
        }

        #region Database

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public byte[] ToByteArray()
        {
            List<int> ids = new List<int>();
            foreach( AOItem item in this )
            {
                ids.Add( item.Id );
            }
            return IntArray.ToByteArray( ids.ToArray() );
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static AOItemList FromByteArray( byte[] array )
        {
            AOItemList items = new AOItemList();
            int[] ids = IntArray.FromByteArray( array );
            foreach( int id in ids )
            {
                AOItem item = AOItem.Load( id );
                if( item != null )
                    items.Add( item );
            }
            return items;
        }

        #endregion

        #region Sorting

        /// <summary>
        ///
        /// </summary>
        public void Sort()
        {
            lock( this )
            {
                AOItem[] sorting = new AOItem[this.Count];
                this.CopyTo( sorting, 0 );
                this.Clear();
                foreach( AOItem item in sorting )
                {
                    if( this.Count == 0 )
                    {
                        this.Add( item );
                        continue;
                    }
                    bool found = false;
                    for( int i = 0; i < this.Count; i++ )
                    {
                        if( this[i].Ql > item.Ql )
                        {
                            this.Insert( i, item );
                            found = true;
                            break;
                        }
                    }
                    if( found == false )
                        this.Add( item );
                }
            }
        }
        
        #endregion
    }
}
