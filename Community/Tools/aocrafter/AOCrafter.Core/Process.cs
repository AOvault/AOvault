﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

using Twp.Data.AO;
using Twp.Utilities;

namespace AOCrafter.Core
{
    [Flags]
    public enum ProcessFlags
    {
        None = 0,
        ConsumeSource = 1 << 0,
        ConsumeTarget = 1 << 1,
        SourceIsPrimary = 1 << 2,
        LinkProcess = 1 << 3
    }

    public class Process : ICrafterData, INotifyPropertyChanged, IEquatable<Process>, IDisposable
    {
#region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public Process()
        {
        }

        /// <summary>
        /// Constructs a new Process which is a copy of <paramref name="other"/>.
        /// </summary>
        /// <param name="other">The Process to copy.</param>
        public Process( Process other )
        {
            this.flags = other.flags;
            this.source = other.Source;
            this.target = other.Target;
            this.result = other.Result;
            foreach( Skill skill in other.Skills )
            {
                this.skills.Add( new Skill( skill ) );
            }
            this.multiplier = other.Multiplier;
            this.note = other.Note;
        }

        /// <summary>
        /// Constructor used by database methods.
        /// </summary>
        /// <param name="row">A dictionary containing the data of the process.</param>
        internal Process( SQLiteResult row )
        {
            this.id = Convert.ToInt32( row["id"] );
            this.flags = (ProcessFlags) Convert.ToInt32( row["flags"] );
            this.source = ProcItem.Load( Convert.ToInt32( row["source"] ) );
            this.target = ProcItem.Load( Convert.ToInt32( row["target"] ) );
            this.result = ProcItem.Load( Convert.ToInt32( row["result"] ) );
            this.skills = SkillsList.FromByteArray( (byte[]) row["skills"] );
            this.multiplier = Convert.ToDecimal( row["multiplier"] );
            this.note = (string) row["note"];
            this.added = (string) row["added"];
            this.updated = (string) row["updated"];
            DatabaseManager.Processes.Add( this.id, this );
        }

#endregion

#region Private Fields

        private int id;
        private ProcessFlags flags = ProcessFlags.ConsumeTarget;
        private ProcItem source;
        private ProcItem target;
        private ProcItem result;
        private SkillsList skills = new SkillsList();
        private decimal multiplier = 0.8m;
        private string note = string.Empty;

        private string added;
        private string updated;

        private ItemInfo finalSource = null;
        private ItemInfo finalTarget = null;
        private ItemInfo finalResult = null;

#endregion
        
#region Properties

        /// <summary>
        /// Gets or sets the Id of the process.
        /// </summary>
        [Browsable( false )]
        public int Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        public ProcessFlags Flags
        {
            get { return this.flags; }
            set
            {
                if( this.flags != value )
                {
                    this.flags = value;
                    this.OnPropertyChanged( new PropertyChangedEventArgs( "Flags" ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets the Source item of the process.
        /// </summary>
        public ProcItem Source
        {
            get { return this.source; }
            set
            {
                if( this.source != value )
                {
                    this.source = value;
                    this.OnSourceChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the Target item of the process.
        /// </summary>
        public ProcItem Target
        {
            get { return this.target; }
            set
            {
                if( this.target != value )
                {
                    this.target = value;
                    this.OnTargetChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the Result item of the process.
        /// </summary>
        public ProcItem Result
        {
            get { return this.result; }
            set
            {
                if( this.result != value )
                {
                    this.result = value;
                    this.OnResultChanged();
                }
            }
        }

        /// <summary>
        /// Returns a <c ref="AOCrafter.Core.SkillsList" />  containing the skills used in the process.
        /// </summary>
        [Category( "Properties" )]
        [Description( "The skills required to complete the process." )]
        public SkillsList Skills
        {
            get { return this.skills; }
        }

        /// <summary>
        /// Gets or sets a decimal value used for determining the QL of the secondary item.
        /// </summary>
        [Category( "Properties" )]
        [Description( "The Multiplier defines how low the secondary item can be compared to the primary." )]
        [DefaultValue( typeof( decimal ), "0.8" )]
        public decimal Multiplier
        {
            get { return this.multiplier; }
            set
            {
                if( this.multiplier != value )
                {
                    this.multiplier = value;
                    this.OnPropertyChanged( new PropertyChangedEventArgs( "Multiplier" ) );
                }
            }
        }

        /// <summary>
        /// Gets or sets a string holding additional info deemed important for this process.
        /// </summary>
        [Category( "Properties" )]
        [Description( "If there's anything that needs to be said, write it here." )]
        [Editor( typeof( System.ComponentModel.Design.MultilineStringEditor ), typeof( System.Drawing.Design.UITypeEditor ) )]
        public string Note
        {
            get { return this.note; }
            set
            {
                if( this.note != value )
                {
                    this.note = value;
                    this.OnPropertyChanged( new PropertyChangedEventArgs( "Note" ) );
                }
            }
        }

        /// <summary>
        /// Gets the date for when the process was first added as a string.
        /// </summary>
        [Category( "Date" )]
        [Description( "The date for when this process was first added." )]
        public string Added
        {
            get { return this.added; }
        }

        /// <summary>
        /// Gets the date for when the process was last updated as a string.
        /// </summary>
        [Category( "Date" )]
        [Description( "The date for when this process was last updated." )]
        public string Updated
        {
            get { return this.updated; }
        }

        [Browsable( false )]
        public int LowQL
        {
            get
            {
                int ql = Flag.IsSet( this.flags, ProcessFlags.SourceIsPrimary ) ? this.source.LowQL : this.target.LowQL;
                if( ql < this.result.LowQL )
                    ql = this.result.LowQL;
                return ql;
            }
        }

        [Browsable( false )]
        public int HighQL
        {
            get
            {
                int ql = Flag.IsSet( this.flags, ProcessFlags.SourceIsPrimary ) ? this.source.HighQL : this.target.HighQL;
                if( ql > this.result.HighQL )
                    ql = this.result.HighQL;
                return ql;
            }
        }

        [Browsable( false )]
        public bool StaticQL
        {
            get { return this.LowQL == this.HighQL; }
        }

        /// <summary>
        /// Gets an <see cref="ItemInfo"/> representing the calculated Source item of this process.
        /// </summary>
        [Browsable( false )]
        public ItemInfo FinalSource
        {
            get { return this.finalSource; }
        }

        /// <summary>
        /// Gets an <see cref="ItemInfo"/> representing the calculated Target item of this process.
        /// </summary>
        [Browsable( false )]
        public ItemInfo FinalTarget
        {
            get { return this.finalTarget; }
        }

        /// <summary>
        /// Gets an <see cref="ItemInfo"/> representing the calculated Result item of this process.
        /// </summary>
        [Browsable( false )]
        public ItemInfo FinalResult
        {
            get { return this.finalResult; }
        }

#endregion

#region Events

        public event EventHandler SourceChanged;
        public event EventHandler TargetChanged;
        public event EventHandler ResultChanged;
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnSourceChanged()
        {
            if( this.SourceChanged != null )
                this.SourceChanged( this, EventArgs.Empty );
        }

        protected virtual void OnTargetChanged()
        {
            if( this.TargetChanged != null )
                this.TargetChanged( this, EventArgs.Empty );
        }

        protected virtual void OnResultChanged()
        {
            if( this.ResultChanged != null )
                this.ResultChanged( this, EventArgs.Empty );
        }

        protected virtual void OnPropertyChanged( PropertyChangedEventArgs e )
        {
            if( this.PropertyChanged != null )
                this.PropertyChanged( this, e );
        }

#endregion

#region Methods

        /// <summary>
        /// Loads a <see cref="AOCrafter.Core.Process"/> from the <see cref="AOCrafter.Core.DatabaseManager.ProcessDatabase"/> database.
        /// </summary>
        /// <param name="id">A value representing the Database ID of the process.</param>
        /// <returns>A <see cref="AOCrafter.Core.Process"/> holding the data, or null if the Process could not be loaded.</returns>
        public static Process Load( int id )
        {
            if( DatabaseManager.Processes.Contains( id ) )
            {
                return DatabaseManager.Processes[id];
            }

            SQLiteDatabase db = DatabaseManager.ProcessDatabase;
            if( db == null )
                return null;

            SQLiteResult row = db.QuerySingle( "SELECT * FROM 'processes' WHERE ID='{0}' LIMIT 1", id.ToString() );
            if( row.Count <= 0 )
                return null;

            return new Process( row );
        }

        /// <summary>
        /// Saves data in a <see cref="AOCrafter.Core.Process"/> to the <see cref="AOCrafter.Core.DatabaseManager.ProcessDatabase"/> database.
        /// </summary>
        public void Save()
        {
            SQLiteDatabase db = DatabaseManager.ProcessDatabase;
            if( db == null )
                return;

            Log.Debug( "[Process.Save] Saving process {0} [{1}+{2}={3}] {4} ({5})", this.id, this.source, this.target, this.result, this.flags, (int) this.flags );
            if( String.IsNullOrEmpty( this.added ) )
                this.added = DateTime.Today.ToString( "yyyy-MM-dd" );
            this.updated = DateTime.Today.ToString( "yyyy-MM-dd" );

            List<SQLiteArgument> args = new List<SQLiteArgument>();
            args.Add( new SQLiteArgument( "flags", DbType.Int32, this.flags ) );
            args.Add( new SQLiteArgument( "source", DbType.Int32, this.source.Id ) );
            args.Add( new SQLiteArgument( "target", DbType.Int32, this.target.Id ) );
            args.Add( new SQLiteArgument( "result", DbType.Int32, this.result.Id ) );
            args.Add( new SQLiteArgument( "skills", DbType.Binary, this.skills.ToByteArray() ) );
            args.Add( new SQLiteArgument( "multiplier", DbType.Decimal, this.multiplier ) );
            args.Add( new SQLiteArgument( "note", DbType.String, this.note ) );
            args.Add( new SQLiteArgument( "added", DbType.String, this.added ) );
            args.Add( new SQLiteArgument( "updated", DbType.String, this.updated ) );
            if( this.id < 1 )
            {
                this.id = db.Insert( "processes", args.ToArray() );
                DatabaseManager.Processes.Add( this.id, this );
            }
            else
            {
                args.Insert( 0, new SQLiteArgument( "id", DbType.Int32, this.id ) );
                db.Update( "processes", args.ToArray() );
            }
        }

        /// <summary>
        /// Deletes the process from the <see cref="AOCrafter.Core.DatabaseManager.ProcessDatabase"/> database.
        /// </summary>
        public void Delete()
        {
            if( this.id > 0 )
            {
                SQLiteDatabase db = DatabaseManager.ProcessDatabase;
                if( db == null )
                    throw new DatabaseException();

                db.NonQuery( "DELETE FROM 'processes' WHERE ID='{0}';", this.id.ToString() );
            }

            this.Dispose();
        }

        /// <summary>
        /// Tries to find one or more processes from the <see cref="AOCrafter.Core.DatabaseManager.ProcessDatabase"/> database.
        /// </summary>
        /// <param name="args">A string array representing keywords to search for.</param>
        /// <returns>A <see cref="AOCrafter.Core.ProcessList"/> holding all processes found, if any.</returns>
        public static ProcessList Search( string[] args )
        {
            SQLiteDatabase db = DatabaseManager.ProcessDatabase;
            if( db == null )
                return null;

            ProcessList processes = new ProcessList();
            ItemList items = ProcItem.Search( args );
            foreach( ProcItem item in items )
            {
                SQLiteResults rows = db.Query( "SELECT * FROM 'processes' WHERE Source='{0}' OR Target='{0}' OR Result='{0}'", item.Id.ToString() );
                foreach( SQLiteResult row in rows )
                {
                    Process process = new Process( row );
                    processes.Add( process );
                }
            }
            return processes;
        }

        public static ProcessList Search( Process process )
        {
            return Query( "SELECT * FROM 'processes' WHERE source='{0}' OR target = '{0}'", process.result.Id.ToString() );
        }

        public static ProcessList Search( ProcItem item, bool linkedOnly )
        {
            string sql = "SELECT * FROM 'processes' WHERE result='{0}'";
            if( linkedOnly )
                sql += String.Format( " AND flags & {0}", (int) ProcessFlags.LinkProcess );
            return Query( sql, item.Id.ToString() );
        }

        public static ProcessList GetAll()
        {
            return Query( "SELECT * FROM 'processes'" );
        }

        private static ProcessList Query( string sql, params string[] args )
        {
            SQLiteDatabase db = DatabaseManager.ProcessDatabase;
            if( db == null )
                return null;

            ProcessList processes = new ProcessList();
            SQLiteResults rows = db.Query( sql, args );
            foreach( SQLiteResult row in rows )
            {
                Process process = new Process( row );
                processes.Add( process );
            }
            return processes;
        }

        public static ProcessList FindUnused()
        {
            SQLiteDatabase db = DatabaseManager.ProcessDatabase;
            if( db == null )
                return null;

            ProcessList unusedProcs = new ProcessList();
            try
            {
                SQLiteResults rows = db.Query( "SELECT * FROM 'processes';" );
                foreach( SQLiteResult row in rows )
                {
                    Process process = new Process( row );
                    ProcessList list = Search( process );
                    SQLiteResult tsRow = db.QuerySingle( "SELECT COUNT(*) FROM 'tradeskills' WHERE IntArray(processes,'{0}') > 0", process.id.ToString() );
                    if( list.Count == 0 && Convert.ToInt32( tsRow["COUNT(*)"] ) == 0 )
                        unusedProcs.Add( process );
                    else
                        process.Dispose();
                }
            }
            catch( Exception ex )
            {
                Twp.Controls.ExceptionDialog.Show( ex );
            }
            return unusedProcs;
        }

        public void Calculate( int ql )
        {
            Log.Debug( "[Process.Calculate] Calculating Process#{0} at QL {1}", this.id, ql );

            if( Flag.IsSet( this.flags, ProcessFlags.SourceIsPrimary ) )
            {
                this.finalSource = ItemParser.Interpolate( this.source.GetNearest( ql ) );
                if( this.finalSource.QL < ql )
                    ql = this.finalSource.QL;
                this.finalTarget = ItemParser.Interpolate( this.target.GetNearest( this.GetMultiplier( ql ) ) );
            }
            else
            {
                this.finalTarget = ItemParser.Interpolate( this.target.GetNearest( ql ) );
                if( this.finalTarget.QL < ql )
                    ql = this.finalTarget.QL;
                this.finalSource = ItemParser.Interpolate( this.source.GetNearest( this.GetMultiplier( ql ) ) );
            }
            this.finalSource.Database = Twp.Data.AO.DatabaseManager.ItemsDatabase;
            this.finalSource.Mmdb = DatabaseManager.Mmdb;
            this.finalTarget.Database = Twp.Data.AO.DatabaseManager.ItemsDatabase;
            this.finalTarget.Mmdb = DatabaseManager.Mmdb;

            this.finalResult = ItemParser.Interpolate( this.result.GetNearest( ql ) );
            this.finalResult.Database = Twp.Data.AO.DatabaseManager.ItemsDatabase;
            this.finalResult.Mmdb = DatabaseManager.Mmdb;

            foreach( Skill skill in this.skills )
            {
                skill.Calculate( ql );
            }
        }

        private int GetMultiplier( int ql )
        {
            decimal value = Decimal.Multiply( this.multiplier, ql );
            value = Decimal.Ceiling( value );
            int multipliedQL = Decimal.ToInt32( value );
            return multipliedQL;
        }

        public override string ToString()
        {
            string text = this.result != null ? this.result.ToString() : base.ToString();
            if( this.flags != ProcessFlags.None )
                text += String.Format( " ({0})", this.flags );
            return text;
        }

#endregion

#region IEquatable implementation

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        public bool Equals( Process other )
        {
            if( other == null )
                return false;
            else
                return this.id.Equals( other.id );
        }

        public override bool Equals( object obj )
        {
            Process other = obj as Process;
            if( other != null )
                return this.Equals( other );
            else
                return false;
        }

        public static bool operator ==( Process item1, Process item2 )
        {
            if( object.ReferenceEquals( item1, item2 ) )
                return true;
            if( object.ReferenceEquals( item1, null ) )
                return false;
            if( object.ReferenceEquals( item2, null ) )
                return false;

            return item1.Equals( item2 );
        }

        public static bool operator !=( Process item1, Process item2 )
        {
            if( object.ReferenceEquals( item1, item2 ) )
                return false;
            if( object.ReferenceEquals( item1, null ) )
                return true;
            if( object.ReferenceEquals( item2, null ) )
                return true;

            return !item1.Equals( item2 );
        }

#endregion

#region IDisposable interface

        private bool disposed = false;

        public void Dispose()
        {
            this.Dispose( true );
        }

        private void Dispose( bool disposing )
        {
            if( !this.disposed )
            {
                if( !DatabaseManager.Processes.Remove( this.id ) )
                {
                    Log.Debug( "[Process.Dispose] Not in cache: {0}", this );
                }
                if( disposing )
                {
                    if( this.source != null )
                        this.source.Dispose();
                    if( this.target != null )
                        this.target.Dispose();
                    if( this.result != null )
                        this.result.Dispose();
                    //this.skills.Dispose();
                }
                this.disposed = true;
            }
        }

#endregion
    }
}
