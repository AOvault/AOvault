﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using Twp.Utilities;

namespace AOCrafter.Core
{
    public class ItemList : List<ProcItem>
    {
        /// <summary>
        /// Adds a <c ref="Item" /> to the ItemCollection, unless duplicates are found.
        /// </summary>
        /// <param name="item">The Item to be added.</param>
        public new void Add( ProcItem item )
        {
            if( !base.Contains( item ) )
                base.Add( item );
            else
                Log.Debug( "[ItemCollection.Add] Item already in collection: {0}", item.Id );
        }
    }
}
