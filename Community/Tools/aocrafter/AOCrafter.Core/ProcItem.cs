﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

using Twp.Data.AO;
using Twp.Utilities;

namespace AOCrafter.Core
{
    public class ProcItem : ICrafterData, IEquatable<ProcItem>, IDisposable
    {
#region Constructors

        public ProcItem()
        {
            this.items.ListChanged += new ListChangedEventHandler( OnItemsChanged );
        }

        public ProcItem( SQLiteResult row )
        {
            this.id = Convert.ToInt32( row["id"] );
            this.items = AOItemList.FromByteArray( (byte[]) row["items"] );
            this.UpdateQLs();
            DatabaseManager.Items.Add( this.id, this );
        }

#endregion

#region Private Fields

        private int id = -1;
        private AOItemList items = new AOItemList();
        private int lowQL = -1;
        private int highQL = -1;

#endregion

#region Properties

        public int Id
        {
            get { return this.id; }
        }

        public AOItemList Items
        {
            get { return this.items; }
        }

        public int LowQL
        {
            get { return this.lowQL; }
        }

        public int HighQL
        {
            get { return this.highQL; }
        }

        public bool StaticQL
        {
            get { return this.LowQL == this.HighQL; }
        }

#endregion

#region Methods

        public static ProcItem Load( int id )
        {
            if( DatabaseManager.Items.Contains( id ) )
            {
                return DatabaseManager.Items[id];
            }

            SQLiteDatabase db = DatabaseManager.ProcessDatabase;
            if( db == null )
                return null;

            SQLiteResult row = db.QuerySingle( "SELECT * FROM 'items' WHERE ID='{0}' LIMIT 1", id.ToString() );
            if( row.Count <= 0 )
                return null;

            return new ProcItem( row );
        }

        public void Save()
        {
            SQLiteDatabase db = DatabaseManager.ProcessDatabase;
            if( db == null )
                return;

            Log.Debug( "[Item.Save] ID before: {0}", this.id );

            List<SQLiteArgument> args = new List<SQLiteArgument>();
            args.Add( new SQLiteArgument( "items", DbType.Binary, this.items.ToByteArray() ) );
            if( this.id < 1 )
            {
                this.id = db.Insert( "items", args.ToArray() );
                DatabaseManager.Items.Add( this.id, this );
            }
            else
            {
                args.Insert( 0, new SQLiteArgument( "id", DbType.Int32, this.id ) );
                db.Update( "items", args.ToArray() );
            }

            Log.Debug( "[Item.Save] ID after: {0}", this.id );
        }

        public void Delete()
        {
            if( this.id > 0 )
            {
                SQLiteDatabase db = DatabaseManager.ProcessDatabase;
                if( db == null )
                    return;

                db.NonQuery( "DELETE FROM 'items' WHERE ID='{0}';", this.id.ToString() );
            }

            this.Dispose();
        }

        public static ItemList Search( string[] args )
        {
            SQLiteDatabase db = DatabaseManager.ProcessDatabase;
            if( db == null )
                return null;

            List<AOItem> aoItems = AOItem.Search( args );
            ItemList items = new ItemList();
            foreach( AOItem aoItem in aoItems )
            {
                SQLiteResults rows = db.Query( "SELECT * FROM 'items' WHERE IntArray(items,'{0}') > 0;", aoItem.Id.ToString() );
                foreach( SQLiteResult row in rows )
                {
                    ProcItem item = new ProcItem( row );
                    if( !items.Contains( item ) )
                        items.Add( item );
                }
            }
            return items;
        }

        public static ItemList GetAll()
        {
            SQLiteDatabase db = DatabaseManager.ProcessDatabase;
            if( db == null )
                return null;

            ItemList items = new ItemList();
            SQLiteResults rows = db.Query( "SELECT * FROM 'items';" );
            foreach( SQLiteResult row in rows )
            {
                items.Add( new ProcItem( row ) );
            }
            return items;
        }

        public static ItemList FindUnused()
        {
            SQLiteDatabase db = DatabaseManager.ProcessDatabase;
            if( db == null )
                return null;

            ItemList unusedItems = new ItemList();
            try
            {
                SQLiteResults rows = db.Query( "SELECT id FROM 'items';" );
                foreach( SQLiteResult row in rows )
                {
                    int id = Convert.ToInt32( row["id"] );
                    SQLiteResult tsRow = db.QuerySingle( "SELECT COUNT(*) FROM 'processes' WHERE source = {0} OR target = {0} OR result = {0}", id.ToString() );
                    if( Convert.ToInt32( tsRow["COUNT(*)"] ) == 0 )
                        unusedItems.Add( Load( id ) );
                }
            }
            catch( Exception ex )
            {
                Twp.Controls.ExceptionDialog.Show( ex );
            }
            return unusedItems;
        }

        protected virtual void OnItemsChanged( object sender, ListChangedEventArgs e )
        {
            this.UpdateQLs();
        }

        private void UpdateQLs()
        {
            foreach( AOItem item in this.items )
            {
                if( this.lowQL == -1 || this.lowQL > item.Ql )
                    this.lowQL = item.Ql;
                if( this.highQL == -1 || this.highQL < item.Ql )
                    this.highQL = item.Ql;
            }
        }

        public Item GetNearest( int ql )
        {
            Item item = new Item();
            item.Ql = ql;
            if( this.items.GetNearest( ref item ) )
                return item;
            
            return null;
        }

        public override string ToString()
        {
            Item item = this.GetNearest( 100 );
            if( item != null )
            {
                AOItem aoitem = item.Interpolate( Twp.Data.AO.DatabaseManager.ItemsDatabase );
                return aoitem.Name;
            }
            else
                return base.ToString();
        }

#endregion

#region IEquatable implementation

        public override int GetHashCode()
        {
            return this.id.GetHashCode();
        }

        public bool Equals( ProcItem other )
        {
            if( other == null )
                return false;
            else
                return this.id.Equals( other.id );
        }

        public override bool Equals( object obj )
        {
            ProcItem other = obj as ProcItem;
            if( other != null )
                return this.Equals( other );
            else
                return false;
        }

        public static bool operator ==( ProcItem item1, ProcItem item2 )
        {
            if( object.ReferenceEquals( item1, item2 ) )
                return true;
            if( object.ReferenceEquals( item1, null ) )
                return false;
            if( object.ReferenceEquals( item2, null ) )
                return false;

            return item1.Equals( item2 );
        }

        public static bool operator !=( ProcItem item1, ProcItem item2 )
        {
            if( object.ReferenceEquals( item1, item2 ) )
                return false;
            if( object.ReferenceEquals( item1, null ) )
                return true;
            if( object.ReferenceEquals( item2, null ) )
                return true;

            return !item1.Equals( item2 );
        }

#endregion

#region IDisposable interface

        private bool disposed = false;

        public void Dispose()
        {
            this.Dispose( true );
        }

        private void Dispose( bool disposing )
        {
            if( !this.disposed )
            {
                if( !DatabaseManager.Items.Remove( this.id ) )
                {
                    Log.Debug( "[Item.Dispose] Not in cache: {0}", this );
                }
                if( disposing )
                {
                    foreach( AOItem item in this.items )
                    {
                        item.Dispose();
                    }
                }
                this.disposed = true;
            }
        }

#endregion
    }
}
