﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Twp.Controls.HtmlRenderer;
using Twp.Data.AO;
using Twp.Utilities;

namespace AOCrafter.Core.Controls
{
    /// <summary>
    /// Description of NewItemBox.
    /// </summary>
    public partial class ItemBox : UserControl
    {
#region Constructor

        public ItemBox()
        {
            InitializeComponent();
            this.typeLabel.Links.Clear();
        }

#endregion

#region Private fields

        private ProcItem item = null;
        private ItemInfo itemInfo = null;
        private ProcessOptionCollection options = new ProcessOptionCollection();
        private ContextMenuStrip optionsMenu = null;
        private TradeskillList links = null;

        private bool editable = false;
        private bool showButtons = true;
        private bool mouseButtonDown = false;

        private static Color backgroundColor = Color.FromArgb( 25, 25, 30 );
        private static Color borderColor = Color.FromArgb( 165, 255, 219 );

#endregion

#region Properties

        [Browsable( true )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
        public override string Text
        {
            get { return this.typeLabel.Text; }
            set { this.typeLabel.Text = value; }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public ProcItem Item
        {
            get { return this.item; }
            set
            {
                if( this.item != value )
                {
                    if( this.item != null )
                        this.item.Items.ListChanged -= this.OnItemListChanged;
                    this.item = value;
                    if( this.item != null )
                        this.item.Items.ListChanged += this.OnItemListChanged;
                    this.OnItemChanged();
                }
            }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public ItemInfo ItemInfo
        {
            get { return this.itemInfo; }
            set
            {
                this.itemInfo = value;
                this.OnItemInfoChanged();
            }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool IsConsumed
        {
            get { return this.consumedBox.Checked; }
            set
            {
                if( this.consumedBox.Checked != value )
                {
                    this.consumedBox.Checked = value;
                    this.UpdateProperties();
                    this.OnConsumedChanged();
                }
            }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool IsPrimary
        {
            get { return this.primaryBox.Checked; }
            set
            {
                if( this.primaryBox.Checked != value )
                {
                    this.primaryBox.Checked = value;
                    this.UpdateProperties();
                    this.OnPrimaryChanged();
                }
            }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TradeskillList Links
        {
            get { return this.links; }
            set
            {
                if( this.links != value )
                {
                    this.links = value;
                    this.OnLinksChanged();
                }
            }
        }

        [Category( "Behavior" )]
        [Description( "" )]
        [DefaultValue( false )]
        public bool Editable
        {
            get { return this.editable; }
            set
            {
                if( this.editable != value )
                {
                    this.editable = value;
                    this.OnEditableChanged();
                }
            }
        }

        [Category( "Behavior" )]
        [Description( "" )]
        [DefaultValue( true )]
        public bool ShowButtons
        {
            get { return this.showButtons; }
            set
            {
                if( this.showButtons != value )
                {
                    this.showButtons = value;
                    this.OnShowButtonsChanged();
                }
            }
        }

#endregion

#region Events

        [Category( "Behavior" )]
        [Description( "Occurs whenever the IsConsumed property is changed." )]
        public event EventHandler ConsumedChanged;

        [Category( "Behavior" )]
        [Description( "Occurs whenever the IsPrimary property is changed." )]
        public event EventHandler PrimaryChanged;

        [Category( "Behavior" )]
        [Description( "Occurs whenever the an option is selected from the options menu." )]
        public event ProcessOptionEventHandler OptionSelected;

        [Category( "Behavior" )]
        [Description( "Occurs whenever the a link is clicked in the type label, or the corresponding context menu, if available." )]
        public event LinkedTradeskillEventHandler LinkClicked;

        protected virtual void OnConsumedChanged()
        {
            if( this.ConsumedChanged != null )
                this.ConsumedChanged( this, EventArgs.Empty );
        }

        protected virtual void OnPrimaryChanged()
        {
            if( this.PrimaryChanged != null )
                this.PrimaryChanged( this, EventArgs.Empty );
        }

        protected virtual void OnOptionSelected( ProcessOptionEventArgs e )
        {
            if( this.OptionSelected != null )
                this.OptionSelected( this, e );
        }
        
        protected virtual void OnLinkClicked( LinkedTradeskillEventArgs e )
        {
            if( this.LinkClicked != null )
                this.LinkClicked( this, e );
        }

#endregion

#region Update Data

        private void OnItemListChanged( object sender, ListChangedEventArgs e )
        {
            this.OnItemChanged();
        }

        protected virtual void OnItemChanged()
        {
            if( this.DesignMode )
                return;

            Log.Debug( "[ItemBox.OnItemChanged] Item: {0}", this.item );

            if( this.item == null )
            {
                this.Reset();
                return;
            }

            Item tmp = this.item.GetNearest( 100 );
            if( tmp == null )
            {
                this.Reset();
                return;
            }

            this.ItemInfo = ItemParser.Interpolate( tmp );
            this.itemInfo.Database = Twp.Data.AO.DatabaseManager.ItemsDatabase;
            this.itemInfo.Mmdb = DatabaseManager.Mmdb;
        }

        protected virtual void OnItemInfoChanged()
        {
            if( this.itemInfo == null )
                return;

            AOIcon icon = AOIcon.Load( this.itemInfo.Attributes[AttributeKey.Icon] );
            if( icon != null )
                this.iconBox.Image = icon.Image;

            this.nameLabel.Text = this.itemInfo.Name;
            this.UpdateProperties();
        }

        private void UpdateProperties()
        {
            this.propertiesLabel.Text = String.Empty;
            if( this.itemInfo == null )
                return;

            StringBuilder sb = new StringBuilder();
            if( this.item != null && this.item.LowQL != this.item.HighQL )
                sb.AppendLine( String.Format( "QL: {0} - {1}", this.item.LowQL, this.item.HighQL ) );
            else
                sb.AppendLine( String.Format( "QL: {0}", this.itemInfo.QL ) );
            if( Flag.IsSet( this.itemInfo.Attributes[AttributeKey.Flags], (int) ItemFlag.NoDrop ) )
                sb.Append( "NoDrop " );
            if( Flag.IsSet( this.itemInfo.Attributes[AttributeKey.Flags], (int) ItemFlag.Unique ) )
                sb.Append( "Unique" );
            sb.AppendLine();
            sb.AppendLine();
            if( this.showButtons )
            {
                if( this.consumedBox.Checked )
                    sb.Append( "Consumed" );
                sb.AppendLine();
                if( this.primaryBox.Checked )
                    sb.Append( "Defines result QL" );
                sb.AppendLine();
            }
            if( this.editable && this.item != null )
            {
                if( !this.showButtons )
                {
                    sb.AppendLine();
                    sb.AppendLine();
                }
                sb.AppendLine();
                sb.AppendLine( String.Format( "Item# {0}", this.item.Id ) );
            }
            this.propertiesLabel.Text = sb.ToString();
        }

        private void Reset()
        {
            this.itemInfo = null;
            this.iconBox.Image = null;
            this.nameLabel.Text = null;
            this.propertiesLabel.Text = null;
        }

        private void OnEditableChanged()
        {
            this.consumedBox.Visible = this.editable;
            this.primaryBox.Visible = this.editable;
        }

        private void OnShowButtonsChanged()
        {
            this.consumedBox.Visible = this.showButtons;
            this.primaryBox.Visible = this.showButtons;
            this.UpdateProperties();
        }

        private void OnConsumedChecked( object sender, EventArgs e )
        {
            this.OnConsumedChanged();
            this.UpdateProperties();
        }

        private void OnPrimaryChecked( object sender, EventArgs e )
        {
            this.OnPrimaryChanged();
            this.UpdateProperties();
        }

#endregion

#region Options

        public void SetOptions( ProcessOptionCollection options )
        {
            this.options = options;
            if( this.options == null || this.options.Count == 0 )
            {
                this.optionsMenu = null;
                this.optionsButton.Visible = false;
            }
            else
            {
                this.optionsButton.Visible = true;
                this.optionsMenu = this.options.CreateMenu( this.itemInfo.QL );
                this.optionsMenu.Closed += this.OnOptionsMenuClosed;
                this.optionsMenu.ItemClicked += this.OnOptionClicked;
                foreach( ToolStripMenuItem menuItem in this.optionsMenu.Items )
                {
                    if( menuItem.HasDropDownItems )
                        menuItem.DropDownItemClicked += this.OnOptionClicked;
                }
            }
        }

        private void OnOptionsButtonChecked( object sender, EventArgs e )
        {
            if( this.optionsButton.Checked )
            {
                Point point = this.optionsButton.Location;
                point.Offset( 0, this.optionsButton.Height );
                this.optionsMenu.Show( this, point, ToolStripDropDownDirection.Default );
            }
        }

        private void OnOptionsMenuClosed( object sender, ToolStripDropDownClosedEventArgs e )
        {
            this.optionsButton.Checked = false;
        }

        protected virtual void OnOptionClicked( object sender, ToolStripItemClickedEventArgs e )
        {
            ProcessOption option = e.ClickedItem.Tag as ProcessOption;
            if( option == null )
                return;

            this.OnOptionSelected( new ProcessOptionEventArgs( option ) );
        }

#endregion

#region Linked Processes

        private void OnLinksChanged()
        {
            this.typeLabel.Links.Clear();
            if( this.links != null && this.links.Count > 0 )
                this.typeLabel.Links.Add( 0, this.typeLabel.Text.Length );
        }

        private void OnLinkClicked( object sender, LinkLabelLinkClickedEventArgs e )
        {
            if( this.links != null && this.links.Count > 0 )
            {
                if( this.links.Count > 1 )
                {
                    ContextMenuStrip menuStrip = new ContextMenuStrip( this.components );
                    foreach( Tradeskill link in this.links )
                    {
                        Item linkItem = link.Processes[0].Result.GetNearest( this.itemInfo.QL );
                        ItemInfo info = ItemParser.Interpolate( linkItem );
                        AOIcon icon = AOIcon.Load( info.Attributes[AttributeKey.Icon] );
                        menuStrip.Items.Add( info.Name, icon.Image, delegate {
                            this.OnLinkClicked( new LinkedTradeskillEventArgs( link, this.itemInfo.QL ) );
                        } );
                    }
                    menuStrip.Show( this.typeLabel, this.typeLabel.Location );
                }
                else
                {
                    this.OnLinkClicked( new LinkedTradeskillEventArgs( this.links[0], this.itemInfo.QL ) );
                }
            }
        }

#endregion

#region Drag & Drop

        protected override void OnDragEnter( DragEventArgs e )
        {
            Log.Debug( "[ItemBox.OnDragEnter] Formats: {0}", String.Join( ", ", e.Data.GetFormats() ) );
            if( this.editable && e.Data.GetDataPresent( typeof( ItemList ) ) )
                e.Effect = DragDropEffects.Copy | DragDropEffects.Move;
            else
                e.Effect = DragDropEffects.None;
            base.OnDragEnter( e );
        }

        private void OnMouseDown( object sender, MouseEventArgs e )
        {
            if( this.editable && e.Button == MouseButtons.Left )
                this.mouseButtonDown = true;
        }

        private void OnMouseMove( object sender, MouseEventArgs e )
        {
            if( !this.editable || !this.mouseButtonDown )
                return;

            if( e.Button == MouseButtons.Left )
            {
                ItemList items = new ItemList();
                items.Add( this.item );
                this.DoDragDrop( items, DragDropEffects.Move | DragDropEffects.Copy );
                this.mouseButtonDown = false;
            }
        }

#endregion

#region Tooltip

        private void OnPopupToolTip( object sender, PopupEventArgs e )
        {
            if( this.itemInfo == null )
                return;

            using( Graphics g = this.CreateGraphics() )
            {
                Size size = new Size( 0, 0 );

                if( MeasureToolTip( g, this.itemInfo, ref size ) )
                    e.ToolTipSize = size;
                else
                    e.Cancel = true;
            }
        }

        private void OnDrawToolTip( object sender, DrawToolTipEventArgs e )
        {
            if( this.itemInfo == null )
                return;

            PaintToolTip( e.Graphics, this.itemInfo, e.Bounds );
        }

        public static bool MeasureToolTip( Graphics g, ItemInfo item, ref Size size )
        {
            if( item == null )
                return false;

            CssData baseCss = CssData.Parse( Properties.Resources.BaseCSS, true );
            SizeF htmlSize = HtmlRender.MeasureGdiPlus( g, item.ToHtml(), 400, baseCss, null, OnHtmlImageLoad );
            size.Height += (int) htmlSize.Height;
            if( size.Width < htmlSize.Width )
                size.Width = (int) htmlSize.Width;

            // allow space for the border
            size.Width += 10;
            size.Height += 10;
            return true;
        }

        public static void PaintToolTip( Graphics g, ItemInfo item, Rectangle rect )
        {
            if( item == null )
                return;

            // Paint background
            using( SolidBrush brush = new SolidBrush( backgroundColor ) )
            {
                g.FillRectangle( brush, rect );
            }

            // Paint border
            rect.Inflate( -2, -2 );
            using( Pen pen = new Pen( borderColor, 2 ) )
            {
                g.DrawRectangle( pen, rect );
            }
            rect.Inflate( -3, -3 );

            if( item == null )
                return;

            // Paint item data
            CssData baseCss = CssData.Parse( Properties.Resources.BaseCSS, true );
            HtmlRender.RenderGdiPlus( g, item.ToHtml(), rect.Location, rect.Size, baseCss, null, OnHtmlImageLoad );
        }

        private static void OnHtmlImageLoad( object sender, Twp.Controls.HtmlRenderer.Entities.HtmlImageLoadEventArgs e )
        {
            int index = e.Src.IndexOf( "://" );
            if( index >= 0 )
            {
                string scheme = e.Src.Substring( 0, index ).ToLower();
                string content = e.Src.Substring( index + 3 ).ToLower();
                if( scheme == "rdb" )
                {
                    try
                    {
                        System.Drawing.Image image = AOIcon.Load( Convert.ToInt32( content ), Twp.Data.AO.DatabaseManager.ItemsDatabase ).Image;
                        if( image.Width == 64 && image.Height == 64 )
                            image = new Bitmap( image, 48, 48 );
                        e.Callback( image, new Rectangle( 0, 0, image.Width, image.Height ) );
                        e.Handled = true;
                    }
                    catch( Exception )
                    {
                    }
                }
                else if( scheme == "resource" )
                {
                    if( content == "itembg" )
                    {
                        System.Drawing.Image image = Properties.Resources.ItemBG;
                        e.Callback( image, new System.Drawing.Rectangle( 0, 0, image.Width, image.Height ) );
                        e.Handled = true;
                    }
                }
            }
        }

#endregion

#region Painting

        private void OnPaintBorder( object sender, PaintEventArgs e )
        {
            Control control = sender as Control;
            if( control == null )
                return;

            using( Pen pen = new Pen( Color.FromArgb( 165, 255, 219 ), 2 ) )
            {
                Rectangle rect = control.ClientRectangle;
                rect.Inflate( -2, -2 );
                e.Graphics.DrawRectangle( pen, rect );
            }
        }

        private void OnPaintOptionsArrow( object sender, PaintEventArgs e )
        {
            Twp.Controls.ButtonRenderer.DrawArrow( e.Graphics, this.optionsButton.DisplayRectangle, Color.Black, ArrowDirection.Down );
        }

#endregion
    }

    public delegate void LinkedTradeskillEventHandler( object sender, LinkedTradeskillEventArgs e );

    public class LinkedTradeskillEventArgs : EventArgs
    {
        public LinkedTradeskillEventArgs( Tradeskill tradeskill, int ql )
        {
            this.tradeskill = tradeskill;
            this.ql = ql;
        }

        private Tradeskill tradeskill;
        private int ql;

        public Tradeskill Tradeskill
        {
            get { return this.tradeskill; }
        }

        public int QL
        {
            get { return this.ql; }
        }
    }
}
