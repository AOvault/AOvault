﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace AOCrafter.Core.Controls
{
    [DefaultEvent( "Search" )]
    [DefaultProperty( "SearchText" )]
    public partial class SearchBox : UserControl
    {
        public SearchBox()
        {
            InitializeComponent();
            this.linkLabel.Links.Clear();
            this.linkLabel.Links.Add( 0, 6, "unused" );
            this.linkLabel.Links.Add( 8, 3, "all" );
//			SearchHistory.Prepare( this.inputBox );
        }

        private bool advanced = true;
        public bool Advanced
        {
            get { return this.advanced; }
            set
            {
                if( this.advanced != value )
                {
                    this.advanced = value;
                    this.linkLabel.Visible = value;
                    this.inputBox.Width = (value ? this.linkLabel.Left : this.searchButton.Left - 4) - this.inputBox.Left;
                }
            }
        }

        [Category( "Behavior" )]
        [Description( "Occurs when the search button is clicked, or enter is pressed." )]
        public event SearchEventHandler Search;

        private void OnEnterPressed( object sender, EventArgs e )
        {
            this.searchButton.PerformClick();
        }

        private void LinkLabelLinkClicked( object sender, LinkLabelLinkClickedEventArgs e )
        {
            this.RaiseSearch( (string) e.Link.LinkData );
        }

        private void OnButtonClicked( object sender, EventArgs e )
        {
            if( String.IsNullOrEmpty( this.inputBox.Text ) )
                return;

//				SearchHistory.Add( this.inputBox.Text );
            this.RaiseSearch( this.inputBox.Text );
        }

        private void RaiseSearch( string text )
        {
            string[] words = text.Split( new char[] { ' ' } );
            if( words == null || words.Length < 1 )
                return;

            if( this.Search != null )
                this.Search( this, new SearchEventArgs( words ) );
        }
    }

    public delegate void SearchEventHandler( object sender, SearchEventArgs e );

    public class SearchEventArgs : EventArgs
    {
        public SearchEventArgs( string[] words )
        {
            this.words = words;
        }

        private string[] words;
        public string[] Words
        {
            get { return this.words; }
        }
    }
}
