﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

using Twp.Controls.HtmlRenderer;
using Twp.Data.AO;
using Twp.Utilities;

namespace AOCrafter.Core.Controls
{
    [DefaultEvent( "SelectedIndexChanged" )]
    public partial class ProcessBox : UserControl
    {
#region Constructor

        public ProcessBox()
        {
            InitializeComponent();
            this.noteLabel = new HtmlLabel();
            this.noteLabel.BackColor = SystemColors.Info;
            this.noteLabel.BaseStylesheet = Properties.Resources.InfoCSS;
            this.noteLabel.Size = new Size( 300, 100 );
            this.noteLabel.MinimumSize = new Size( 200, 30 );
            this.noteButton.Content = noteLabel;

            this.optionsManager = new ProcessOptionManager();
        }

#endregion

#region Private fields

        private Tradeskill tradeskill;
        private ProcessNode currentNode = null;
        private HtmlLabel noteLabel;
        private ProcessOptionManager optionsManager;

#endregion

#region Properties

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public Tradeskill Tradeskill
        {
            get { return this.tradeskill; }
            set
            {
                if( this.tradeskill != value )
                {
                    this.tradeskill = value;
                    this.Reset();
                }
            }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public int SplitterDistance
        {
            get { return this.splitContainer.SplitterDistance; }
            set { this.splitContainer.SplitterDistance = value; }
        }

#endregion

#region Events

        public event LinkedTradeskillEventHandler LinkClicked
        {
            add
            {
                this.sourceBox.LinkClicked += value;
                this.targetBox.LinkClicked += value;
            }
            remove
            {
                this.sourceBox.LinkClicked -= value;
                this.targetBox.LinkClicked -= value;
            }
        }

        public event EventHandler SelectionChanged;

        protected virtual void OnSelectionChanged()
        {
            if( this.SelectionChanged != null )
                this.SelectionChanged( this, EventArgs.Empty );
        }

#endregion

#region Methods

        public void Reset()
        {
            if( this.tradeskill == null )
                return;

            this.currentNode = this.tradeskill.Nodes.FirstNode;
        }

        private void OnFirstPageClicked( object sender, EventArgs e )
        {
            this.currentNode = this.tradeskill.Nodes.FirstNode;
            this.UpdateProcess();
        }

        private void OnLastPageClicked( object sender, EventArgs e )
        {
            this.currentNode = this.tradeskill.Nodes.SelectedNode;
            this.UpdateProcess();
        }

        private void OnNextPageClicked( object sender, EventArgs e )
        {
            this.currentNode = this.currentNode.NextNode;
            this.UpdateProcess();
        }

        private void OnPreviousPageClicked( object sender, EventArgs e )
        {
            this.currentNode = this.currentNode.PreviousNode;
            this.UpdateProcess();
        }

        public void UpdateProcess()
        {
            if( this.currentNode == null )
                return;

            // Update page selection buttons
            this.firstButton.Enabled = ( this.currentNode != this.tradeskill.Nodes.FirstNode );
            this.lastButton.Enabled = ( this.currentNode != this.tradeskill.Nodes.SelectedNode );
            this.nextButton.Enabled = ( this.currentNode.NextNode != null );
            this.previousButton.Enabled = ( this.currentNode.PreviousNode != null );

            // Update process data
            if( this.currentNode.Process == null )
                return;

            // Update source
            this.sourceBox.ItemInfo = this.currentNode.Process.FinalSource;
            this.sourceBox.IsConsumed = Flag.IsSet( this.currentNode.Process.Flags, ProcessFlags.ConsumeSource );
            this.sourceBox.IsPrimary = Flag.IsSet( this.currentNode.Process.Flags, ProcessFlags.SourceIsPrimary );
            this.sourceBox.Links = Tradeskill.Search( this.currentNode.Process.Source, this.tradeskill );
            this.SelectNode( this.sourceBox.Links, this.currentNode.Process.Source );

            // Update target
            this.targetBox.ItemInfo = this.currentNode.Process.FinalTarget;
            this.targetBox.IsConsumed = Flag.IsSet( this.currentNode.Process.Flags, ProcessFlags.ConsumeTarget );
            this.targetBox.IsPrimary = !Flag.IsSet( this.currentNode.Process.Flags, ProcessFlags.SourceIsPrimary );
            this.targetBox.Links = Tradeskill.Search( this.currentNode.Process.Target, this.tradeskill );
            this.SelectNode( this.targetBox.Links, this.currentNode.Process.Target );

            // Update result
            this.resultBox.ItemInfo = this.currentNode.Process.FinalResult;

            // Update skills
            this.skillsView.Items.Clear();
            foreach( Skill skill in this.currentNode.Process.Skills )
            {
                ListViewItem item = new ListViewItem( Skill.GetName( skill.Key ) );
                item.SubItems.Add( skill.FinalValue.ToString() );
                item.ImageIndex = this.GetIcon( Skill.GetIcon( skill.Key ) );
                this.skillsView.Items.Add( item );
            }

            // Update note
            if( !String.IsNullOrEmpty( this.currentNode.Process.Note ) )
            {
                this.noteLabel.Text = "<html><body>" + this.currentNode.Process.Note + "</body></html>";
                this.noteButton.Visible = true;
            }
            else
            {
                this.noteLabel.Text = null;
                this.noteButton.Visible = false;
            }

            // Update count
            if( this.currentNode.RealCount > 1 )
                this.label.Text = String.Format( "Repeat {0} times!", this.currentNode.RealCount );
            else
                this.label.Text = String.Empty;

            // List options...
            this.optionsManager.ParseOptions( this.currentNode.Owner, this.currentNode.Process );
            this.sourceBox.SetOptions( this.optionsManager.Sources );
            this.targetBox.SetOptions( this.optionsManager.Targets );
            this.resultBox.SetOptions( this.optionsManager.Results );
        }

        private int GetIcon( int id )
        {
            if( this.icons.Images.ContainsKey( id.ToString() ) == false )
            {
                AOIcon icon = AOIcon.Load( id );
                if( icon != null )
                {
                    this.icons.Images.Add( icon.Id.ToString(), icon.Image );
                }
            }
            return this.icons.Images.IndexOfKey( id.ToString() );
        }

        private void SelectNode( TradeskillList links, ProcItem item )
        {
            foreach( Tradeskill link in links )
            {
                for( int i = 0; i < link.Nodes.Count; i++ )
                {
                    if( link.Nodes[i].Process.Result == item )
                    {
                        Log.Debug( "[ProcessBox.SelectNode] Selecting node {0}: {1}", i, link.Nodes[i].Process );
                        link.Nodes.SelectedIndex = i;
                        break;
                    }
                }
            }
        }

        private void OnOptionSelected( object sender, ProcessOptionEventArgs e )
        {
            this.currentNode.Owner.SelectedIndex = e.Option.Index;
            this.currentNode = this.currentNode.Owner.SelectedNode;
            this.UpdateProcess();
            this.OnSelectionChanged();
        }

#endregion
    }
}
