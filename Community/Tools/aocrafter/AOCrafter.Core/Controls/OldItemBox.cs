﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;
using System.Windows.Forms.Design;

using Twp.Controls.HtmlRenderer;
using Twp.Data.AO;
using Twp.Utilities;

namespace AOCrafter.Core.Controls
{
    [Flags]
    public enum ItemBoxContent
    {
        None        = 0,
        Name        = 1 << 0,
        QL          = 1 << 1,
        Flags       = 1 << 2,
        Description = 1 << 3,
        Data        = 1 << 4,
    };

    [System.Security.Permissions.PermissionSet( System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust" )]
    [Designer( typeof( ItemBoxDesigner ) )]
    [DefaultEvent( "ConsumedChanged" )]
    [DefaultProperty( "DisplayStyle" )]
    public class OldItemBox : Control
    {
        #region Constructor

        public OldItemBox()
        {
            this.SetStyle( ControlStyles.SupportsTransparentBackColor |
                          ControlStyles.OptimizedDoubleBuffer |
                          ControlStyles.AllPaintingInWmPaint |
                          ControlStyles.UserPaint |
                          ControlStyles.ResizeRedraw, true );
            this.options.ListChanged += this.OnOptionsChanged;
        }

        #endregion

        #region Private fields

        private ProcItem item = null;
        private ItemInfo itemInfo = null;
        private Bitmap icon = null;
        private Bitmap backgroundImage = Properties.Resources.ItemBG;

        private ProcessOptionCollection options = new ProcessOptionCollection();
        private ContextMenuStrip optionsMenu = null;

        private bool isConsumed = false;
        private bool isPrimary = false;

        private Rectangle iconRect = Rectangle.Empty;
        private ButtonData consumedButton = new ButtonData( "C" );
        private ButtonData primaryButton = new ButtonData( "P" );
        private ButtonData optionsButton = new ButtonData();
        private Rectangle titleRect = Rectangle.Empty;
        private Rectangle contentRect = Rectangle.Empty;
        private bool drawTitleShadow = false;

        private static Color backgroundColor = Color.FromArgb( 25, 25, 30 );
        private static Color borderColor = Color.FromArgb( 165, 255, 219 );
        private static Color nameColor = Color.White;
        private static Color headerColor = Color.FromArgb( 0, 222, 66 );
        private static Color textColor = Color.FromArgb( 99, 173, 99 );

        private ToolStripItemDisplayStyle displayStyle = ToolStripItemDisplayStyle.Image;
        private TextImageRelation textImageRelation = TextImageRelation.ImageAboveText;
        private ContentAlignment imageAlign = ContentAlignment.TopCenter;
        private ItemBoxContent content = ItemBoxContent.Name | ItemBoxContent.QL | ItemBoxContent.Flags;

        private bool updating = false;
        private bool itemUpdated = false;
        private bool infoUpdated = false;
        private bool optionsUpdated = false;

        private bool editable = false;
        private bool showButtons = true;
        private bool mouseButtonDown = false;

        #endregion

        #region Properties

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public ProcItem Item
        {
            get { return this.item; }
            set
            {
                if( this.item != value )
                {
                    if( this.item != null )
                        this.item.Items.ListChanged -= this.OnItemListChanged;
                    this.item = value;
                    if( this.item != null )
                        this.item.Items.ListChanged += this.OnItemListChanged;
                    this.OnItemChanged();
                }
            }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public ItemInfo ItemInfo
        {
            get { return this.itemInfo; }
            set
            {
                this.itemInfo = value;
                this.OnItemInfoChanged();
            }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public ProcessOptionCollection Options
        {
            get { return this.options; }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool IsConsumed
        {
            get { return this.isConsumed; }
            set
            {
                if( this.isConsumed != value )
                {
                    this.isConsumed = value;
                    this.consumedButton.Pressed = value;
                    this.Invalidate();
                    this.OnConsumedChanged();
                }
            }
        }

        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool IsPrimary
        {
            get { return this.isPrimary; }
            set
            {
                if( this.isPrimary != value )
                {
                    this.isPrimary = value;
                    this.primaryButton.Pressed = value;
                    this.Invalidate();
                    this.OnPrimaryChanged();
                }
            }
        }

        [Category( "Appearance" )]
        [Description( "" )]
        [DefaultValue( ToolStripItemDisplayStyle.Image )]
        public ToolStripItemDisplayStyle DisplayStyle
        {
            get { return this.displayStyle; }
            set
            {
                if( this.displayStyle != value )
                {
                    this.displayStyle = value;
                    this.RefreshLayout();
                    this.Invalidate();
                }
            }
        }

        [Category( "Appearance" )]
        [Description( "" )]
        [DefaultValue( TextImageRelation.ImageAboveText )]
        public TextImageRelation TextImageRelation
        {
            get { return this.textImageRelation; }
            set
            {
                if( this.textImageRelation != value )
                {
                    this.textImageRelation = value;
                    this.RefreshLayout();
                    this.Invalidate();
                }
            }
        }

        [Category( "Appearance" )]
        [Description( "Image alignment" )]
        [DefaultValue( ContentAlignment.TopCenter )]
        public ContentAlignment ImageAlign
        {
            get { return this.imageAlign; }
            set
            {
                if( this.imageAlign != value )
                {
                    this.imageAlign = value;
                    this.RefreshLayout();
                    this.Invalidate();
                }
            }
        }

        [Category( "Appearance" )]
        [Description( "Controls what parts of the item content is displayed." )]
        [DefaultValue( typeof( ItemBoxContent ), "Name, QL, Flags" )]
        [Editor( typeof( Twp.Controls.Design.FlagsEditor ), typeof( System.Drawing.Design.UITypeEditor ) )]
        public ItemBoxContent Content
        {
            get { return this.content; }
            set
            {
                if( this.content != value )
                {
                    this.content = value;
                    this.RefreshLayout();
                    this.Invalidate();
                }
            }
        }

        [Category( "Behavior" )]
        [Description( "" )]
        [DefaultValue( false )]
        public bool Editable
        {
            get { return this.editable; }
            set
            {
                if( this.editable != value )
                {
                    this.editable = value;
                    this.Invalidate();
                }
            }
        }

        [Category( "Behavior" )]
        [Description( "" )]
        [DefaultValue( true )]
        public bool ShowButtons
        {
            get { return this.showButtons; }
            set
            {
                if( this.showButtons != value )
                {
                    this.showButtons = value;
                    this.Invalidate();
                }
            }
        }

        #endregion

        #region Events

        [Category( "Behavior" )]
        [Description( "Occurs whenever the IsConsumed property is changed." )]
        public event EventHandler ConsumedChanged;

        protected virtual void OnConsumedChanged()
        {
            if( this.ConsumedChanged != null )
                this.ConsumedChanged( this, EventArgs.Empty );
        }

        [Category( "Behavior" )]
        [Description( "Occurs whenever the IsPrimary property is changed." )]
        public event EventHandler PrimaryChanged;

        protected virtual void OnPrimaryChanged()
        {
            if( this.PrimaryChanged != null )
                this.PrimaryChanged( this, EventArgs.Empty );
        }

        public event ProcessOptionEventHandler OptionSelected;

        protected virtual void OnOptionSelected( ProcessOptionEventArgs e )
        {
            if( this.OptionSelected != null )
                this.OptionSelected( this, e );
        }

        #endregion

        #region Misc methods

        protected override void Dispose( bool disposing )
        {
            if( disposing )
            {
                if( this.icon != null )
                    this.icon.Dispose();
            }
            base.Dispose( disposing );
        }

        protected override void SetBoundsCore( int x, int y, int width, int height, BoundsSpecified specified )
        {
            if( this.displayStyle == ToolStripItemDisplayStyle.Image )
            {
                Size s = this.backgroundImage.Size;
                base.SetBoundsCore( x, y, s.Width, s.Height, specified );
            }
            else
            {
                base.SetBoundsCore( x, y, width, height, specified );
            }
        }

        [Browsable( false )]
        public override Size MinimumSize
        {
            get { return this.backgroundImage.Size; }
        }

        public void BeginUpdate()
        {
            this.updating = true;
        }

        public void EndUpdate()
        {
            this.updating = false;
            if( this.itemUpdated )
            {
                this.OnItemChanged();
                this.itemUpdated = false;
            }
            if( this.infoUpdated )
            {
                this.OnItemInfoChanged();
                this.infoUpdated = false;
            }
            if( this.optionsUpdated )
            {
                this.OnOptionsChanged();
                this.optionsUpdated = false;
            }
        }

        #endregion

        #region Data update

        private void OnItemListChanged( object sender, ListChangedEventArgs e )
        {
            this.OnItemChanged();
        }

        protected virtual void OnItemChanged()
        {
            this.optionsUpdated = true;
            if( this.DesignMode || this.updating )
                return;

            Log.Debug( "[ItemBox.OnItemChanged] Item: {0}", this.item );

            if( this.item == null )
            {
                this.Reset();
                return;
            }

            Item tmp = this.item.GetNearest( 100 );
            if( tmp == null )
            {
                this.Reset();
                return;
            }

            this.ItemInfo = ItemParser.Interpolate( tmp );
            this.itemInfo.Database = Twp.Data.AO.DatabaseManager.ItemsDatabase;
            this.itemInfo.Mmdb = DatabaseManager.Mmdb;
        }

        protected virtual void OnItemInfoChanged()
        {
            this.infoUpdated = true;
            if( this.updating )
                return;

            this.Tag = this.itemInfo;
            if( this.itemInfo == null )
                return;

            AOIcon icon = AOIcon.Load( this.itemInfo.Attributes[AttributeKey.Icon] );
            if( icon != null )
                this.icon = icon.Image;
            
            this.Invalidate();
        }

        private void Reset()
        {
            this.itemInfo = null;
            this.icon = null;
            this.Invalidate();
        }

        protected override void OnTextChanged( EventArgs e )
        {
            base.OnTextChanged( e );
            this.RefreshLayout();
            this.Invalidate();
        }

        #endregion

        #region Options

        private void OnOptionsChanged( object sender, ListChangedEventArgs e )
        {
            this.OnOptionsChanged();
        }

        private void OnOptionsChanged()
        {
            this.optionsUpdated = true;
            if( this.updating )
                return;

            if( this.options == null || this.options.Count == 0 )
            {
                this.optionsMenu = null;
            }
            else
            {
                this.optionsMenu = this.options.CreateMenu( this.itemInfo.QL );
                this.optionsMenu.Closed += this.OnOptionsMenuClosed;
                this.optionsMenu.ItemClicked += this.OnOptionClicked;
                foreach( ToolStripMenuItem menuItem in this.optionsMenu.Items )
                {
                    if( menuItem.HasDropDownItems )
                        menuItem.DropDownItemClicked += this.OnOptionClicked;
                }
            }
            this.RefreshLayout();
            this.Invalidate();
        }

        private void OnOptionsMenuClosed( object sender, ToolStripDropDownClosedEventArgs e )
        {
            this.optionsButton.Pressed = false;
            this.Invalidate();
        }

        protected virtual void OnOptionClicked( object sender, ToolStripItemClickedEventArgs e )
        {
            ProcessOption option = e.ClickedItem.Tag as ProcessOption;
            if( option == null )
                return;

            this.OnOptionSelected( new ProcessOptionEventArgs( option ) );
        }

        #endregion

        #region Mouse handling

        protected override void OnDragEnter( DragEventArgs e )
        {
            Log.Debug( "[ItemBox.OnDragEnter] Formats: {0}", String.Join( ", ", e.Data.GetFormats() ) );
            if( this.editable && e.Data.GetDataPresent( typeof( ItemList ) ) )
                e.Effect = DragDropEffects.Copy | DragDropEffects.Move;
            else
                e.Effect = DragDropEffects.None;
            base.OnDragEnter( e );
        }

        protected override void OnMouseDown( MouseEventArgs e )
        {
            if( this.editable && e.Button == MouseButtons.Left )
            {
                if( this.iconRect.Contains( e.Location ) )
                    this.mouseButtonDown = true;
            }
            base.OnMouseDown( e );
        }

        protected override void OnMouseMove( MouseEventArgs e )
        {
            base.OnMouseMove( e );

            this.consumedButton.Hot = this.editable && this.consumedButton.Contains( e.Location );
            this.primaryButton.Hot = this.editable && this.primaryButton.Contains( e.Location );
            this.optionsButton.Hot = this.optionsButton.Contains( e.Location );

            if( ( this.editable && ( this.consumedButton.Hot || this.primaryButton.Hot ) ) || this.optionsButton.Hot )
                this.Cursor = Cursors.Hand;
            else
                this.Cursor = Cursors.Default;

            this.Invalidate();

            if( !this.editable || !this.mouseButtonDown )
                return;

            if( e.Button == MouseButtons.Left )
            {
                ItemList items = new ItemList();
                items.Add( this.item );
                this.DoDragDrop( items, DragDropEffects.Move | DragDropEffects.Copy );
                this.mouseButtonDown = false;
            }
        }

        protected override void OnMouseClick( MouseEventArgs e )
        {
            base.OnMouseClick( e );

            if( this.optionsButton.Contains( e.Location ) )
            {
                Point point = this.optionsButton.Rectangle.Location;
                point.Offset( 0, this.optionsButton.Rectangle.Height );
                this.optionsMenu.Show( this, point, ToolStripDropDownDirection.Default );
                this.optionsButton.Pressed = true;
                this.Invalidate();
                return;
            }

            if( !this.editable )
                return;

            if( this.consumedButton.Contains( e.Location ) )
                this.IsConsumed = !this.isConsumed;
            else if( this.primaryButton.Contains( e.Location ) )
                this.IsPrimary = !this.isPrimary;
        }
        
        #endregion

        #region Tooltip

        public static bool MeasureToolTip( Graphics g, ItemInfo item, ref Size size, ItemBoxContent content )
        {
            if( item == null )
                return false;

            using( Font font = new Font( SystemFonts.DefaultFont, FontStyle.Bold ) )
            {
                g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                TextFormatFlags tff = TextFormatFlags.Top | TextFormatFlags.NoPadding | TextFormatFlags.SingleLine;
                if( Flag.IsSet( content, ItemBoxContent.Name ) )
                {
                    OldItemBox.MeasureText( g, item.Name, font, tff, ref size );
                    size.Height += 6; // allow space for name separation line
                }
                if( Flag.IsSet( content, ItemBoxContent.QL ) )
                    OldItemBox.MeasureText( g, "QL: " + item.QL.ToString(), font, tff, ref size );
                if( Flag.IsSet( content, ItemBoxContent.Description ) )
                {
                    if( size.Width < 400 )
                        size.Width = 400;
                    OldItemBox.MeasureText( g, item.Description, font, TextFormatFlags.Top | TextFormatFlags.NoPadding | TextFormatFlags.WordBreak, ref size );
                }
                if( Flag.IsSet( content, ItemBoxContent.Data ) )
                {
                    CssData baseCss = CssData.Parse( Properties.Resources.BaseCSS, true );
                    SizeF htmlSize = HtmlRender.MeasureGdiPlus( g, item.ToHtml(), 400, baseCss, null, OnHtmlImageLoad );
                    size.Height += (int) htmlSize.Height;
                    if( size.Width < htmlSize.Width )
                        size.Width = (int) htmlSize.Width;
                }
                // Add more info here...

                // allow space for the border
                size.Width += 10;
                size.Height += 10;
            }
            return true;
        }

        public static void PaintToolTip( Graphics g, ItemInfo item, Rectangle rect, ItemBoxContent content )
        {
            if( item == null )
                return;

            using( Font font = new Font( SystemFonts.DefaultFont, FontStyle.Bold ) )
            {
                g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                TextFormatFlags tff = TextFormatFlags.Top | TextFormatFlags.NoPadding | TextFormatFlags.WordBreak;
                PaintContent( g, item, font, tff, rect, content );
            }
        }

        #endregion
        
        #region Resize
        
        protected override void OnResize( EventArgs e )
        {
            base.OnResize( e );
            this.RefreshLayout();
        }

        private void RefreshLayout()
        {
            Rectangle rect = this.ClientRectangle;
            Font font = new Font( this.Font, FontStyle.Bold );
            TextFormatFlags tff = TextFormatFlags.Top | TextFormatFlags.NoPadding | TextFormatFlags.WordBreak;

            switch( this.displayStyle )
            {
                case ToolStripItemDisplayStyle.Image:
                    this.MeasureIcon( ref rect );
                    this.MeasureButtons( ref rect );
                    this.MeasureOptions( rect );
                    break;
                case ToolStripItemDisplayStyle.ImageAndText:
                    this.MeasureIcon( ref rect );
                    this.MeasureButtons( ref rect );
                    this.MeasureOptions( rect );
                    this.MeasureTitle( font, tff, ref rect );
                    MeasureText( this.itemInfo, font, tff, ref rect, this.content );
                    break;
                case ToolStripItemDisplayStyle.Text:
                    this.MeasureButtons( ref rect );
                    this.MeasureOptions( rect );
                    this.MeasureTitle( font, tff, ref rect );
                    MeasureText( this.itemInfo, font, tff, ref rect, this.content );
                    break;
                case ToolStripItemDisplayStyle.None:
                    this.MeasureButtons( ref rect );
                    this.MeasureOptions( rect );
                    this.MeasureTitle( font, tff, ref rect );
                    break;
            }
            if( rect.Bottom > this.ClientRectangle.Bottom )
                rect.Height -= (rect.Bottom - this.ClientRectangle.Bottom);
            this.contentRect = rect;

            font.Dispose();
        }

        private void MeasureIcon( ref Rectangle rect )
        {
            this.iconRect = new Rectangle( rect.Location, this.backgroundImage.Size );
            // offset to the intended area
            int dx, dy;
            switch( this.textImageRelation )
            {
                case TextImageRelation.ImageAboveText:
                    switch( this.imageAlign )
                    {
                        case ContentAlignment.BottomLeft:
                        case ContentAlignment.MiddleLeft:
                        case ContentAlignment.TopLeft:
                            dx = 0;
                            break;
                        case ContentAlignment.BottomRight:
                        case ContentAlignment.MiddleRight:
                        case ContentAlignment.TopRight:
                            dx = rect.Width - this.iconRect.Width;
                            break;
                        default:
                            dx = ( rect.Width - this.iconRect.Width ) / 2;
                            break;
                    }
                    dy = 0;
                    rect.Height -= this.iconRect.Height + 3;
                    rect.Offset( 0, this.iconRect.Height + 3 );
                    break;
                case TextImageRelation.TextAboveImage:
                    switch( this.imageAlign )
                    {
                        case ContentAlignment.BottomLeft:
                        case ContentAlignment.MiddleLeft:
                        case ContentAlignment.TopLeft:
                            dx = 0;
                            break;
                        case ContentAlignment.BottomRight:
                        case ContentAlignment.MiddleRight:
                        case ContentAlignment.TopRight:
                            dx = rect.Width - this.iconRect.Width;
                            break;
                        default:
                            dx = ( rect.Width - this.iconRect.Width ) / 2;
                            break;
                    }
                    dy = rect.Height - this.iconRect.Height;
                    rect.Height -= this.iconRect.Height + 3;
                    break;
                case TextImageRelation.ImageBeforeText:
                    switch( this.imageAlign )
                    {
                        case ContentAlignment.TopCenter:
                        case ContentAlignment.TopLeft:
                        case ContentAlignment.TopRight:
                            dy = 0;
                            break;
                        case ContentAlignment.BottomCenter:
                        case ContentAlignment.BottomLeft:
                        case ContentAlignment.BottomRight:
                            dy = rect.Height - this.iconRect.Height;
                            break;
                        default:
                            dy = ( rect.Height - this.iconRect.Height ) / 2;
                            break;
                    }
                    dx = 0;
                    rect.Width -= this.iconRect.Width + 3;
                    rect.Offset( this.iconRect.Width + 3, 0 );
                    break;
                case TextImageRelation.TextBeforeImage:
                    switch( this.imageAlign )
                    {
                        case ContentAlignment.TopCenter:
                        case ContentAlignment.TopLeft:
                        case ContentAlignment.TopRight:
                            dy = 0;
                            break;
                        case ContentAlignment.BottomCenter:
                        case ContentAlignment.BottomLeft:
                        case ContentAlignment.BottomRight:
                            dy = rect.Height - this.iconRect.Height;
                            break;
                        default:
                            dy = ( rect.Height - this.iconRect.Height ) / 2;
                            break;
                    }
                    dx = rect.Width - this.iconRect.Width;
                    rect.Width -= this.iconRect.Width + 3;
                    break;
                default:
                    switch( this.imageAlign )
                    {
                        case ContentAlignment.BottomCenter:
                            dx = ( rect.Width - this.iconRect.Width ) / 2;
                            dy = rect.Height - this.iconRect.Height;
                            break;
                        case ContentAlignment.BottomLeft:
                            dx = 0;
                            dy = rect.Height - this.iconRect.Height;
                            break;
                        case ContentAlignment.BottomRight:
                            dx = rect.Width - this.iconRect.Width;
                            dy = rect.Height - this.iconRect.Height;
                            break;
                        case ContentAlignment.MiddleCenter:
                            dx = ( rect.Width - this.iconRect.Width ) / 2;
                            dy = ( rect.Height - this.iconRect.Height ) / 2;
                            break;
                        case ContentAlignment.MiddleLeft:
                            dx = 0;
                            dy = ( rect.Height - this.iconRect.Height ) / 2;
                            break;
                        case ContentAlignment.MiddleRight:
                            dx = rect.Width - this.iconRect.Width;
                            dy = ( rect.Height - this.iconRect.Height ) / 2;
                            break;
                        case ContentAlignment.TopCenter:
                            dx = ( rect.Width - this.iconRect.Width ) / 2;
                            dy = 0;
                            break;
                        case ContentAlignment.TopRight:
                            dx = rect.Width - this.iconRect.Width;
                            dy = 0;
                            break;
                        default:
                            dx = 0;
                            dy = 0;
                            break;
                    }
                    break;
            }
            this.iconRect.Offset( dx, dy );
        }

        private void MeasureButtons( ref Rectangle rect )
        {
            if( !this.showButtons || (this.AutoSize && this.displayStyle == ToolStripItemDisplayStyle.Image) )
            {
                this.consumedButton.Rectangle = Rectangle.Empty;
                this.primaryButton.Rectangle = Rectangle.Empty;
                return;
            }

            Size size = new Size( 26, 26 );
            int spacing = 6;
            Point start;
            int heightOffset = 0;
            int widthOffset = 0;

            if( this.iconRect == Rectangle.Empty )
            {
                start = rect.Location;
                start.Offset( rect.Width / 2, 0 );
                rect.Offset( 0, size.Height + 3 );
                rect.Height -= size.Height + 3;
                spacing = 3;
            }
            else
            {
                start = this.iconRect.Location;
                heightOffset = (this.iconRect.Height / 2) - (size.Height / 2);
                widthOffset = this.iconRect.Width;
            }

            Rectangle buttonRect = new Rectangle( start, size );
            buttonRect.Offset( -size.Width - spacing, heightOffset );
            this.consumedButton.Rectangle = buttonRect;

            buttonRect = new Rectangle( start, size );
            buttonRect.Offset( widthOffset + spacing, heightOffset );
            this.primaryButton.Rectangle = buttonRect;
        }

        private void MeasureOptions( Rectangle rect )
        {
            if( this.optionsMenu == null )
            {
                this.optionsButton.Rectangle = Rectangle.Empty;
                return;
            }

            Size size = new Size( 25, 15 );
            Point start;

            if( this.iconRect != Rectangle.Empty )
            {
                start = this.iconRect.Location;
                start.Offset( this.iconRect.Width + 6, 0 );
            }
            else
                start = rect.Location;

            this.optionsButton.Rectangle = new Rectangle( start, size );
        }

        private void MeasureTitle( Font font, TextFormatFlags tff, ref Rectangle rect )
        {
            if( String.IsNullOrEmpty( this.Text ) )
                return;

            Size size = TextRenderer.MeasureText( this.Text, font, rect.Size, tff );
            this.titleRect = new Rectangle( rect.Location, size );
            int dx, dy;
            this.drawTitleShadow = false;
            switch( this.textImageRelation )
            {
                case TextImageRelation.ImageAboveText:
                    dx = ( rect.Width - size.Width ) / 2;
                    dy = -1;
                    rect.Height -= size.Height;
                    rect.Offset( 0, size.Height );
                    break;
                case TextImageRelation.TextAboveImage:
                    dx = ( rect.Width - size.Width ) / 2;
                    dy = rect.Height - size.Height + 1;
                    rect.Height -= size.Height;
                    break;
                case TextImageRelation.ImageBeforeText:
                case TextImageRelation.TextBeforeImage:
                    this.titleRect = this.iconRect;
                    this.drawTitleShadow = true;
                    dx = 2;
                    dy = 2;
                    break;
                case TextImageRelation.Overlay:
                    dx = ( rect.Width - size.Width ) / 2;
                    dy = ( rect.Height - size.Height ) / 2;
                    break;
                default:
                    dx = 0;
                    dy = 0;
                    break;
            }

            this.titleRect.Offset( dx, dy );
        }

        public void MeasureText( ItemInfo item, Font font, TextFormatFlags tff, ref Rectangle rect, ItemBoxContent content )
        {
            if( item == null )
                return;

            Point start = rect.Location;
            int width = rect.Width;

            rect.Inflate( -5, -5 );
            if( Flag.IsSet( content, ItemBoxContent.Name ) )
            {
                Size size = TextRenderer.MeasureText( item.Name, font, rect.Size, tff );
                int offset = size.Height + 6;
                rect.Height -= offset;
                rect.Offset( 0, offset );
            }

            // Paint item info...
            if( Flag.IsSet( content, ItemBoxContent.Flags ) )
            {
                string flags = "";
                if( Flag.IsSet( (ItemFlag) item.Attributes[AttributeKey.Flags], ItemFlag.NoDrop ) )
                    flags += "NoDrop ";
                if( Flag.IsSet( (ItemFlag) item.Attributes[AttributeKey.Flags], ItemFlag.Unique ) )
                    flags += "Unique ";
                if( Flag.IsSet( (ItemFlag) item.Attributes[AttributeKey.Flags], ItemFlag.Special ) )
                    flags += "Special ";
                MeasureText( flags.Trim(), nameColor, font, tff, ref rect );
            }
            if( Flag.IsSet( content, ItemBoxContent.QL ) )
                MeasureText( "QL: ", item.QL.ToString(), font, tff, ref rect );
            if( Flag.IsSet( content, ItemBoxContent.Description ) )
                MeasureText( item.Description, textColor, font, tff, ref rect );

            rect = new Rectangle( start, new Size( width, rect.Bottom - start.Y ) );
        }

        private static void MeasureText( string text, Color color, Font font, TextFormatFlags tff, ref Rectangle rect )
        {
            if( String.IsNullOrEmpty( text ) )
                return;

            Size size = TextRenderer.MeasureText( text, font, rect.Size, tff );
            rect.Offset( 0, size.Height );
        }

        private static void MeasureText( string header, string text, Font font, TextFormatFlags tff, ref Rectangle rect )
        {
            int dx = 0;
            int dy = 0;
            if( !String.IsNullOrEmpty( header ) )
            {
                Size headerSize = TextRenderer.MeasureText( header, font, rect.Size, tff );
                dx = headerSize.Width;
                dy = headerSize.Height;
            }

            rect.Offset( dx, 0 );
            if( !String.IsNullOrEmpty( text ) )
            {
                Size size = TextRenderer.MeasureText( text, font, rect.Size, tff );
                dy = size.Height;
            }
            rect.Offset( -dx, dy );
        }

        public static void MeasureText( Graphics g, string text, Font font, TextFormatFlags tff, ref Size size )
        {
            Size textSize = TextRenderer.MeasureText( g, text, font, size, tff );
            size.Height += textSize.Height;
            if( size.Width <= textSize.Width )
                size.Width = textSize.Width;
        }

        #endregion

        #region Painting

        protected override void OnPaint( PaintEventArgs e )
        {
            base.OnPaint( e );

            Font font = new Font( this.Font, FontStyle.Bold );
            TextFormatFlags tff = TextFormatFlags.Top | TextFormatFlags.NoPadding | TextFormatFlags.WordBreak;
            e.Graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;

            switch( this.displayStyle )
            {
                case ToolStripItemDisplayStyle.Image:
                    this.PaintIcon( e.Graphics );
                    this.PaintButtons( e.Graphics );
                    this.PaintOptions( e.Graphics );
                    break;
                case ToolStripItemDisplayStyle.ImageAndText:
                    this.PaintIcon( e.Graphics );
                    this.PaintButtons( e.Graphics );
                    this.PaintOptions( e.Graphics );
                    this.PaintTitle( e.Graphics, font, tff );
                    PaintContent( e.Graphics, this.itemInfo, font, tff, this.contentRect, this.content );
                    break;
                case ToolStripItemDisplayStyle.Text:
                    this.PaintButtons( e.Graphics );
                    this.PaintOptions( e.Graphics );
                    this.PaintTitle( e.Graphics, font, tff );
                    PaintContent( e.Graphics, this.itemInfo, font, tff, this.contentRect, this.content );
                    break;
                case ToolStripItemDisplayStyle.None:
                    this.PaintButtons( e.Graphics );
                    this.PaintOptions( e.Graphics );
                    this.PaintTitle( e.Graphics, font, tff );
                    break;
            }

            font.Dispose();
        }

        private void PaintIcon( Graphics g )
        {
            g.DrawImageUnscaledAndClipped( this.backgroundImage, this.iconRect );

            if( this.DesignMode )// stop here in Design mode...
                return;

            if( this.icon == null )
                return;

            // center the icon on the background
            Rectangle iconRect = this.iconRect;
            int dx = ( iconRect.Width - this.icon.Width ) / -2;
            int dy = ( iconRect.Height - this.icon.Height ) / -2;
            iconRect.Inflate( dx, dx );
            g.DrawImageUnscaledAndClipped( this.icon, iconRect );
        }

        private void PaintButtons( Graphics g )
        {
            if( !this.showButtons || (this.AutoSize && this.displayStyle == ToolStripItemDisplayStyle.Image) )
                return;

            this.consumedButton.Paint( g, this.Font, this.editable );
            this.primaryButton.Paint( g, this.Font, this.editable );
        }

        private void PaintOptions( Graphics g )
        {
            if( this.optionsMenu == null )
                return;

            Twp.Controls.ButtonRenderer.DrawButton( g, this.optionsButton.Rectangle, this.optionsButton.State );
            Twp.Controls.ButtonRenderer.DrawArrow( g, this.optionsButton.Rectangle, SystemColors.ControlText, ArrowDirection.Down );
        }

        private void PaintTitle( Graphics g, Font font, TextFormatFlags tff )
        {
            if( String.IsNullOrEmpty( this.Text ) )
                return;

            if( this.drawTitleShadow )
            {
                Rectangle shadowRect = this.titleRect;
                shadowRect.Offset( 1, 1 );
                TextRenderer.DrawText( g, this.Text, font, shadowRect, Color.Black, tff );
            }
            TextRenderer.DrawText( g, this.Text, font, this.titleRect, this.ForeColor, tff );
        }

        public static void PaintContent( Graphics g, ItemInfo item, Font font, TextFormatFlags tff, Rectangle rect, ItemBoxContent content )
        {
            // Paint background
            using( SolidBrush brush = new SolidBrush( backgroundColor ) )
            {
                g.FillRectangle( brush, rect );
            }

            // Paint border
            rect.Inflate( -2, -2 );
            using( Pen pen = new Pen( borderColor, 2 ) )
            {
                g.DrawRectangle( pen, rect );
            }

            if( item == null )
                return;

            // Paint item name
            rect.Inflate( -3, -3 );
            if( Flag.IsSet( content, ItemBoxContent.Name ) )
            {
                Size size = TextRenderer.MeasureText( g, item.Name, font, rect.Size, tff );
                Rectangle nameRect = new Rectangle( rect.Location, size );
                TextRenderer.DrawText( g, item.Name, font, nameRect, nameColor, tff );

                int offset = size.Height + 6;
                rect.Height -= offset;
                rect.Offset( 0, offset );

                // Paint border below name
                Point start = rect.Location;
                start.Offset( -3, -3 );
                Point end = start;
                end.Offset( rect.Width + 6, 0 );
                using( Pen pen = new Pen( borderColor, 2 ) )
                {
                    g.DrawLine( pen, start, end );
                }
            }

            // Paint item info...
            if( Flag.IsSet( content, ItemBoxContent.Flags ) )
            {
                string flags = "";
                if( Flag.IsSet( (ItemFlag) item.Attributes[AttributeKey.Flags], ItemFlag.NoDrop ) )
                    flags += "NoDrop ";
                if( Flag.IsSet( (ItemFlag) item.Attributes[AttributeKey.Flags], ItemFlag.Unique ) )
                    flags += "Unique ";
                if( Flag.IsSet( (ItemFlag) item.Attributes[AttributeKey.Flags], ItemFlag.Special ) )
                    flags += "Special ";
                PaintText( g, flags.Trim(), nameColor, font, tff, ref rect );
            }
            if( Flag.IsSet( content, ItemBoxContent.QL ) )
                PaintText( g, "QL: ", item.QL.ToString(), font, tff, ref rect );
            if( Flag.IsSet( content, ItemBoxContent.Description ) )
                PaintText( g, item.Description, textColor, font, tff, ref rect );
            if( Flag.IsSet( content, ItemBoxContent.Data ) )
            {
                CssData baseCss = CssData.Parse( Properties.Resources.BaseCSS, true );
                HtmlRender.RenderGdiPlus( g, item.ToHtml(), rect.Location, rect.Size, baseCss, null, OnHtmlImageLoad );
            }
            // Add more info here
        }

        private static void PaintText( Graphics g, string header, string text, Font font, TextFormatFlags tff, ref Rectangle rect )
        {
            int dx = 0;
            int dy = 0;
            if( !String.IsNullOrEmpty( header ) )
            {
                Size headerSize = TextRenderer.MeasureText( g, header, font, rect.Size, tff );
                Rectangle headerRect = new Rectangle( rect.Location, headerSize );
                TextRenderer.DrawText( g, header, font, headerRect, headerColor, tff );
                dx = headerRect.Width;
                dy = headerRect.Height;
            }

            rect.Offset( dx, 0 );
            if( !String.IsNullOrEmpty( text ) )
            {
                Size size = TextRenderer.MeasureText( g, text, font, rect.Size, tff );
                Rectangle textRect = new Rectangle( rect.Location, size );
                TextRenderer.DrawText( g, text, font, textRect, textColor, tff );
                dy = textRect.Height;
            }
            rect.Offset( -dx, dy );
        }

        private static void PaintText( Graphics g, string text, Color color, Font font, TextFormatFlags tff, ref Rectangle rect )
        {
            if( String.IsNullOrEmpty( text ) )
                return;

            int dx = 0;
            int dy = 0;

            Size size = TextRenderer.MeasureText( g, text, font, rect.Size, tff );
            Rectangle textRect = new Rectangle( rect.Location, size );
            TextRenderer.DrawText( g, text, font, textRect, color, tff );
            dy = textRect.Height;

            rect.Offset( -dx, dy );
        }

        private static void OnHtmlImageLoad( object sender, Twp.Controls.HtmlRenderer.Entities.HtmlImageLoadEventArgs e )
        {
            int index = e.Src.IndexOf( "://" );
            if( index >= 0 )
            {
                string scheme = e.Src.Substring( 0, index ).ToLower();
                string content = e.Src.Substring( index + 3 ).ToLower();
                if( scheme == "rdb" )
                {
                    try
                    {
                        System.Drawing.Image image = AOIcon.Load( Convert.ToInt32( content ), Twp.Data.AO.DatabaseManager.ItemsDatabase ).Image;
                        if( image.Width == 64 && image.Height == 64 )
                            image = new Bitmap( image, 48, 48 );
                        e.Callback( image, new Rectangle( 0, 0, image.Width, image.Height ) );
                        e.Handled = true;
                    }
                    catch( Exception )
                    {
                    }
                }
                else if( scheme == "resource" )
                {
                    if( content == "itembg" )
                    {
                        System.Drawing.Image image = Properties.Resources.ItemBG;
                        e.Callback( image, new System.Drawing.Rectangle( 0, 0, image.Width, image.Height ) );
                        e.Handled = true;
                    }
                }
            }
        }
        
        [Obsolete]
        private static void OuterGlowText( IDeviceContext dc, string text, Font font, Color foreColor, Color glow, Rectangle rect, int thickness, StringFormat sf )
        {
            Graphics g = (Graphics) dc;
            if( g == null )
                return;

            g.SmoothingMode = SmoothingMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            using( GraphicsPath path = new GraphicsPath() )
            {
                path.AddString( text, font.FontFamily, (int) font.Style, font.Size, rect, sf );
                for( int i = 1; i < thickness; ++i )
                {
                    using( Pen pen = new Pen( glow, i ) )
                    {
                        pen.LineJoin = LineJoin.Round;
                        g.DrawPath( pen, path );
                    }
                }

                using( SolidBrush brush = new SolidBrush( foreColor ) )
                {
                    g.FillPath( brush, path );
                }
            }
        }

        #endregion
        
        #region Button helper class

        internal class ButtonData
        {
            public ButtonData()
            {
            }

            public ButtonData( string text )
            {
                this.text = text;
            }

            private string text;
            private bool pressed;
            private bool hot;
            private Rectangle rectangle = Rectangle.Empty;
            private System.Windows.Forms.VisualStyles.PushButtonState state = System.Windows.Forms.VisualStyles.PushButtonState.Normal;

            internal string Text
            {
                get { return this.text; }
                set { this.text = value; }
            }

            internal bool Pressed
            {
                get { return this.pressed; }
                set
                {
                    this.pressed = value;
                    this.SetState();
                }
            }

            internal bool Hot
            {
                get { return this.hot; }
                set
                {
                    this.hot = value;
                    this.SetState();
                }
            }

            internal Rectangle Rectangle
            {
                get { return this.rectangle; }
                set { this.rectangle = value; }
            }

            internal System.Windows.Forms.VisualStyles.PushButtonState State
            {
                get { return this.state; }
                set { this.state = value; }
            }

            private void SetState()
            {
                if( this.pressed )
                    this.state = System.Windows.Forms.VisualStyles.PushButtonState.Pressed;
                else if( this.hot )
                    this.state = System.Windows.Forms.VisualStyles.PushButtonState.Hot;
                else
                    this.state = System.Windows.Forms.VisualStyles.PushButtonState.Normal;
            }

            internal bool Contains( Point location )
            {
                return this.rectangle != Rectangle.Empty && this.rectangle.Contains( location );
            }

            internal void Paint( Graphics g, Font font, bool editable )
            {
                if( editable )
                {
                    Twp.Controls.ButtonRenderer.DrawButton( g, this.rectangle, this.state );
                    if( !String.IsNullOrEmpty( this.text ) )
                        TextRenderer.DrawText( g, this.text, font, this.rectangle, SystemColors.ControlText, TextFormatFlags.VerticalCenter | TextFormatFlags.HorizontalCenter );
                }
                else
                {
                    if( this.pressed && !String.IsNullOrEmpty( this.text ) )
                        TextRenderer.DrawText( g, this.text, font, this.rectangle, SystemColors.ControlText, TextFormatFlags.VerticalCenter | TextFormatFlags.HorizontalCenter );
                }
            }
        }

        #endregion
    }

    #region Designer

    class ItemBoxDesigner : ControlDesigner
    {
        private OldItemBox itemBox;

        public ItemBoxDesigner()
        {
        }

        public override void Initialize( IComponent component )
        {
            base.Initialize( component );
            itemBox = (OldItemBox) component;
        }

        public override SelectionRules SelectionRules
        {
            get
            {
                SelectionRules sr = SelectionRules.Visible;
                SelectionRules srSize = SelectionRules.None;
                switch( this.itemBox.Dock )
                {
                    case DockStyle.Left:
                        srSize |= SelectionRules.RightSizeable;
                        break;
                    case DockStyle.Right:
                        srSize |= SelectionRules.LeftSizeable;
                        break;
                    case DockStyle.Top:
                        srSize |= SelectionRules.BottomSizeable;
                        break;
                    case DockStyle.Bottom:
                        srSize |= SelectionRules.TopSizeable;
                        break;
                    case DockStyle.None:
                        srSize |= SelectionRules.AllSizeable;
                        sr |= SelectionRules.Moveable;
                        break;
                }

                if( this.itemBox.DisplayStyle != ToolStripItemDisplayStyle.Image )
                {
                    sr |= srSize;
                }

                return sr;
            }
        }
    }

    #endregion
}
