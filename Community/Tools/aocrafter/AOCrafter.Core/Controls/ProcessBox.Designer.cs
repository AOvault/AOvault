﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace AOCrafter.Core.Controls
{
    partial class ProcessBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.sourceBox = new AOCrafter.Core.Controls.ItemBox();
            this.plusLabel = new System.Windows.Forms.Label();
            this.targetBox = new AOCrafter.Core.Controls.ItemBox();
            this.equalsLabel = new System.Windows.Forms.Label();
            this.resultBox = new AOCrafter.Core.Controls.ItemBox();
            this.icons = new System.Windows.Forms.ImageList(this.components);
            this.skillsView = new Twp.Controls.FillListView();
            this.skillHeader = new System.Windows.Forms.ColumnHeader();
            this.valueHeader = new System.Windows.Forms.ColumnHeader();
            this.noteButton = new Twp.Controls.PopupButton();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.nextButton = new System.Windows.Forms.Button();
            this.lastButton = new System.Windows.Forms.Button();
            this.previousButton = new System.Windows.Forms.Button();
            this.firstButton = new System.Windows.Forms.Button();
            this.label = new System.Windows.Forms.Label();
            this.tableLayoutPanel.SuspendLayout();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 5;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel.Controls.Add(this.sourceBox, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.plusLabel, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.targetBox, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.equalsLabel, 3, 0);
            this.tableLayoutPanel.Controls.Add(this.resultBox, 4, 0);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 2;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(388, 199);
            this.tableLayoutPanel.TabIndex = 2;
            // 
            // sourceBox
            // 
            this.sourceBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sourceBox.Location = new System.Drawing.Point(0, 0);
            this.sourceBox.Margin = new System.Windows.Forms.Padding(0);
            this.sourceBox.MinimumSize = new System.Drawing.Size(54, 54);
            this.sourceBox.Name = "sourceBox";
            this.tableLayoutPanel.SetRowSpan(this.sourceBox, 2);
            this.sourceBox.Size = new System.Drawing.Size(118, 199);
            this.sourceBox.TabIndex = 0;
            this.sourceBox.Text = "Source:";
            this.sourceBox.OptionSelected += new AOCrafter.Core.ProcessOptionEventHandler(this.OnOptionSelected);
            // 
            // plusLabel
            // 
            this.plusLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plusLabel.Location = new System.Drawing.Point(121, 0);
            this.plusLabel.Name = "plusLabel";
            this.plusLabel.Size = new System.Drawing.Size(10, 56);
            this.plusLabel.TabIndex = 1;
            this.plusLabel.Text = "+";
            this.plusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // targetBox
            // 
            this.targetBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.targetBox.Location = new System.Drawing.Point(134, 0);
            this.targetBox.Margin = new System.Windows.Forms.Padding(0);
            this.targetBox.MinimumSize = new System.Drawing.Size(54, 54);
            this.targetBox.Name = "targetBox";
            this.tableLayoutPanel.SetRowSpan(this.targetBox, 2);
            this.targetBox.Size = new System.Drawing.Size(118, 199);
            this.targetBox.TabIndex = 2;
            this.targetBox.Text = "Target:";
            this.targetBox.OptionSelected += new AOCrafter.Core.ProcessOptionEventHandler(this.OnOptionSelected);
            // 
            // equalsLabel
            // 
            this.equalsLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.equalsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equalsLabel.Location = new System.Drawing.Point(255, 0);
            this.equalsLabel.Name = "equalsLabel";
            this.equalsLabel.Size = new System.Drawing.Size(10, 56);
            this.equalsLabel.TabIndex = 3;
            this.equalsLabel.Text = "=";
            this.equalsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // resultBox
            // 
            this.resultBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultBox.Location = new System.Drawing.Point(268, 0);
            this.resultBox.Margin = new System.Windows.Forms.Padding(0);
            this.resultBox.MinimumSize = new System.Drawing.Size(54, 54);
            this.resultBox.Name = "resultBox";
            this.tableLayoutPanel.SetRowSpan(this.resultBox, 2);
            this.resultBox.Size = new System.Drawing.Size(120, 199);
            this.resultBox.TabIndex = 4;
            this.resultBox.Text = "Result:";
            this.resultBox.OptionSelected += new AOCrafter.Core.ProcessOptionEventHandler(this.OnOptionSelected);
            // 
            // icons
            // 
            this.icons.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.icons.ImageSize = new System.Drawing.Size(16, 16);
            this.icons.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // skillsView
            // 
            this.skillsView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.skillHeader,
            this.valueHeader});
            this.skillsView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skillsView.FullRowSelect = true;
            this.skillsView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.skillsView.LargeImageList = this.icons;
            this.skillsView.Location = new System.Drawing.Point(0, 0);
            this.skillsView.MultiSelect = false;
            this.skillsView.Name = "skillsView";
            this.skillsView.ShowGroups = false;
            this.skillsView.Size = new System.Drawing.Size(388, 88);
            this.skillsView.SmallImageList = this.icons;
            this.skillsView.TabIndex = 2;
            this.skillsView.UseCompatibleStateImageBehavior = false;
            this.skillsView.View = System.Windows.Forms.View.Details;
            // 
            // skillHeader
            // 
            this.skillHeader.Text = "Skill";
            this.skillHeader.Width = 334;
            // 
            // valueHeader
            // 
            this.valueHeader.Text = "Value";
            this.valueHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.valueHeader.Width = 50;
            // 
            // noteButton
            // 
            this.noteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.noteButton.Location = new System.Drawing.Point(267, 3);
            this.noteButton.Name = "noteButton";
            this.noteButton.PopupDirection = Twp.Controls.PopupDirection.Up;
            this.noteButton.Size = new System.Drawing.Size(26, 26);
            this.noteButton.TabIndex = 1;
            this.noteButton.Text = "?";
            this.noteButton.Visible = false;
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 36);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.tableLayoutPanel);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.skillsView);
            this.splitContainer.Size = new System.Drawing.Size(388, 291);
            this.splitContainer.SplitterDistance = 199;
            this.splitContainer.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.nextButton);
            this.panel1.Controls.Add(this.lastButton);
            this.panel1.Controls.Add(this.previousButton);
            this.panel1.Controls.Add(this.firstButton);
            this.panel1.Controls.Add(this.noteButton);
            this.panel1.Controls.Add(this.label);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(388, 36);
            this.panel1.TabIndex = 4;
            // 
            // nextButton
            // 
            this.nextButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.nextButton.Image = global::AOCrafter.Core.Properties.Resources.Next;
            this.nextButton.Location = new System.Drawing.Point(299, 3);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(40, 26);
            this.nextButton.TabIndex = 6;
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.OnNextPageClicked);
            // 
            // lastButton
            // 
            this.lastButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lastButton.Image = global::AOCrafter.Core.Properties.Resources.Last;
            this.lastButton.Location = new System.Drawing.Point(345, 3);
            this.lastButton.Name = "lastButton";
            this.lastButton.Size = new System.Drawing.Size(40, 26);
            this.lastButton.TabIndex = 5;
            this.lastButton.UseVisualStyleBackColor = true;
            this.lastButton.Click += new System.EventHandler(this.OnLastPageClicked);
            // 
            // previousButton
            // 
            this.previousButton.Image = global::AOCrafter.Core.Properties.Resources.Prev;
            this.previousButton.Location = new System.Drawing.Point(49, 3);
            this.previousButton.Name = "previousButton";
            this.previousButton.Size = new System.Drawing.Size(40, 26);
            this.previousButton.TabIndex = 3;
            this.previousButton.UseVisualStyleBackColor = true;
            this.previousButton.Click += new System.EventHandler(this.OnPreviousPageClicked);
            // 
            // firstButton
            // 
            this.firstButton.Image = global::AOCrafter.Core.Properties.Resources.First;
            this.firstButton.Location = new System.Drawing.Point(3, 3);
            this.firstButton.Name = "firstButton";
            this.firstButton.Size = new System.Drawing.Size(40, 26);
            this.firstButton.TabIndex = 2;
            this.firstButton.UseVisualStyleBackColor = true;
            this.firstButton.Click += new System.EventHandler(this.OnFirstPageClicked);
            // 
            // label
            // 
            this.label.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label.BackColor = System.Drawing.Color.Transparent;
            this.label.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label.Location = new System.Drawing.Point(127, 4);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(134, 26);
            this.label.TabIndex = 7;
            this.label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ProcessBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer);
            this.Controls.Add(this.panel1);
            this.Name = "ProcessBox";
            this.Size = new System.Drawing.Size(388, 327);
            this.tableLayoutPanel.ResumeLayout(false);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        private System.Windows.Forms.SplitContainer splitContainer;

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private ItemBox sourceBox;
        private ItemBox targetBox;
        private ItemBox resultBox;
        private System.Windows.Forms.Label plusLabel;
        private System.Windows.Forms.Label equalsLabel;
        private Twp.Controls.FillListView skillsView;
        private System.Windows.Forms.ColumnHeader skillHeader;
        private System.Windows.Forms.ColumnHeader valueHeader;
        private System.Windows.Forms.ImageList icons;
        private Twp.Controls.PopupButton noteButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button firstButton;
        private System.Windows.Forms.Button previousButton;
        private System.Windows.Forms.Button lastButton;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.Label label;

        #endregion
    }
}
