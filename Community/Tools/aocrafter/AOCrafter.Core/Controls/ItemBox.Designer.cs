﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace AOCrafter.Core.Controls
{
    partial class ItemBox
    {
        /// <summary>
        /// Designer variable used to keep track of non-visual components.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        
        /// <summary>
        /// Disposes resources used by the control.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                if (components != null) {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        
        /// <summary>
        /// This method is required for Windows Forms designer support.
        /// Do not change the method contents inside the source code editor. The Forms designer might
        /// not be able to load this method if it was changed manually.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.iconBox = new System.Windows.Forms.PictureBox();
            this.consumedBox = new System.Windows.Forms.CheckBox();
            this.primaryBox = new System.Windows.Forms.CheckBox();
            this.optionsButton = new System.Windows.Forms.CheckBox();
            this.typeLabel = new System.Windows.Forms.LinkLabel();
            this.nameLabel = new System.Windows.Forms.Label();
            this.propertiesLabel = new System.Windows.Forms.Label();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.iconBox)).BeginInit();
            this.SuspendLayout();
            // 
            // iconBox
            // 
            this.iconBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.iconBox.BackgroundImage = global::AOCrafter.Core.Properties.Resources.ItemBG;
            this.iconBox.Location = new System.Drawing.Point(38, 3);
            this.iconBox.Name = "iconBox";
            this.iconBox.Size = new System.Drawing.Size(54, 54);
            this.iconBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.iconBox.TabIndex = 0;
            this.iconBox.TabStop = false;
            this.toolTip.SetToolTip(this.iconBox, "dummy");
            this.iconBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnMouseDown);
            this.iconBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.OnMouseMove);
            // 
            // consumedBox
            // 
            this.consumedBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.consumedBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.consumedBox.Location = new System.Drawing.Point(7, 18);
            this.consumedBox.Name = "consumedBox";
            this.consumedBox.Size = new System.Drawing.Size(25, 25);
            this.consumedBox.TabIndex = 1;
            this.consumedBox.Text = "C";
            this.consumedBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.consumedBox.UseVisualStyleBackColor = true;
            this.consumedBox.Visible = false;
            this.consumedBox.CheckedChanged += new System.EventHandler(this.OnConsumedChecked);
            // 
            // primaryBox
            // 
            this.primaryBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.primaryBox.Appearance = System.Windows.Forms.Appearance.Button;
            this.primaryBox.Location = new System.Drawing.Point(98, 18);
            this.primaryBox.Name = "primaryBox";
            this.primaryBox.Size = new System.Drawing.Size(25, 25);
            this.primaryBox.TabIndex = 2;
            this.primaryBox.Text = "P";
            this.primaryBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.primaryBox.UseVisualStyleBackColor = true;
            this.primaryBox.Visible = false;
            this.primaryBox.CheckedChanged += new System.EventHandler(this.OnPrimaryChecked);
            // 
            // optionsButton
            // 
            this.optionsButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.optionsButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.optionsButton.Location = new System.Drawing.Point(98, 3);
            this.optionsButton.Name = "optionsButton";
            this.optionsButton.Size = new System.Drawing.Size(25, 15);
            this.optionsButton.TabIndex = 0;
            this.optionsButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.optionsButton.UseVisualStyleBackColor = true;
            this.optionsButton.Visible = false;
            this.optionsButton.CheckedChanged += new System.EventHandler(this.OnOptionsButtonChecked);
            this.optionsButton.Paint += new System.Windows.Forms.PaintEventHandler(this.OnPaintOptionsArrow);
            // 
            // typeLabel
            // 
            this.typeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.typeLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.typeLabel.LinkArea = new System.Windows.Forms.LinkArea(0, 0);
            this.typeLabel.Location = new System.Drawing.Point(3, 60);
            this.typeLabel.Name = "typeLabel";
            this.typeLabel.Size = new System.Drawing.Size(124, 16);
            this.typeLabel.TabIndex = 3;
            this.typeLabel.Text = "Type:";
            this.typeLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.typeLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.OnLinkClicked);
            // 
            // nameLabel
            // 
            this.nameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nameLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(30)))));
            this.nameLabel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLabel.ForeColor = System.Drawing.Color.Yellow;
            this.nameLabel.Location = new System.Drawing.Point(0, 80);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Padding = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.nameLabel.Size = new System.Drawing.Size(130, 48);
            this.nameLabel.TabIndex = 4;
            this.nameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.nameLabel.Paint += new System.Windows.Forms.PaintEventHandler(this.OnPaintBorder);
            // 
            // propertiesLabel
            // 
            this.propertiesLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.propertiesLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(30)))));
            this.propertiesLabel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.propertiesLabel.ForeColor = System.Drawing.Color.White;
            this.propertiesLabel.Location = new System.Drawing.Point(0, 130);
            this.propertiesLabel.Name = "propertiesLabel";
            this.propertiesLabel.Padding = new System.Windows.Forms.Padding(3);
            this.propertiesLabel.Size = new System.Drawing.Size(130, 70);
            this.propertiesLabel.TabIndex = 5;
            this.propertiesLabel.Paint += new System.Windows.Forms.PaintEventHandler(this.OnPaintBorder);
            // 
            // toolTip
            // 
            this.toolTip.OwnerDraw = true;
            this.toolTip.Draw += new System.Windows.Forms.DrawToolTipEventHandler(this.OnDrawToolTip);
            this.toolTip.Popup += new System.Windows.Forms.PopupEventHandler(this.OnPopupToolTip);
            // 
            // ItemBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.iconBox);
            this.Controls.Add(this.consumedBox);
            this.Controls.Add(this.optionsButton);
            this.Controls.Add(this.primaryBox);
            this.Controls.Add(this.typeLabel);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.propertiesLabel);
            this.Name = "ItemBox";
            this.Size = new System.Drawing.Size(130, 200);
            ((System.ComponentModel.ISupportInitialize)(this.iconBox)).EndInit();
            this.ResumeLayout(false);

        }
        private System.Windows.Forms.PictureBox iconBox;
        private System.Windows.Forms.CheckBox consumedBox;
        private System.Windows.Forms.CheckBox optionsButton;
        private System.Windows.Forms.CheckBox primaryBox;
        private System.Windows.Forms.LinkLabel typeLabel;
        private System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.Label propertiesLabel;
        private System.Windows.Forms.ToolTip toolTip;
    }
}
