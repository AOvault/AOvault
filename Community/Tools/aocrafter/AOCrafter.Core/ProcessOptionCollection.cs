﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Windows.Forms;

using Twp.Data.AO;
using Twp.Utilities;

namespace AOCrafter.Core
{
    /// <summary>
    /// Description of ProcessOptionGroup.
    /// </summary>
    public class ProcessOptionCollection : List<ProcessOption>
    {
        public ContextMenuStrip CreateMenu( int ql )
        {
            if( this.Count < 8 )
                return this.CreateFlatMenu( ql );
            else
                return this.CreateGroupedMenu( ql );
        }

        private ContextMenuStrip CreateFlatMenu( int ql )
        {
            ContextMenuStrip menuStrip = new ContextMenuStrip();
            foreach( ProcessOption option in this )
                menuStrip.Items.Add( CreateMenuItem( option, ql ) );
            return menuStrip;
        }

        private ContextMenuStrip CreateGroupedMenu( int ql )
        {
            ContextMenuStrip menuStrip;
            int limit = 3;
            int lastCount = 0;
            do
            {
                int count;
                int groupLen;
                menuStrip = this.CreateGroupedMenu( ql, limit, out count, out groupLen );

                if( lastCount == count )
                    break;
                lastCount = count;

                if( groupLen > 0 )
                    limit = groupLen + 1;

                Application.DoEvents();
            }
            while( lastCount <= 1 );

            foreach( ToolStripMenuItem groupItem in menuStrip.Items )
            {
                if( groupItem.DropDownItems.Count == 1 )
                {
                    ToolStripMenuItem menuItem = (ToolStripMenuItem) groupItem.DropDownItems[0];
                    if( menuItem == null )
                        continue;

                    groupItem.Text = menuItem.Text;
                    groupItem.Image = menuItem.Image;
                    groupItem.Tag = menuItem.Tag;
                    groupItem.DropDownItems.Clear();
                }
            }

            return menuStrip;
        }

        private ContextMenuStrip CreateGroupedMenu( int ql, int limit, out int groupCount, out int groupLen )
        {
            ContextMenuStrip menuStrip = new ContextMenuStrip();
            Dictionary<string,ToolStripMenuItem> groups = new Dictionary<string,ToolStripMenuItem>();
            string group = null;
            foreach( ProcessOption option in this )
            {
                ToolStripMenuItem menuItem = CreateMenuItem( option, ql );

                group = GetGroup( menuItem.Text, groups, limit );
                if( string.IsNullOrEmpty( group ) )
                {
                    menuStrip.Items.Add( menuItem );
                }
                else
                {
                    SetGroup( menuItem.Text, groups, ref group, menuStrip );
                    groups[group].DropDownItems.Add( menuItem );
                }
                Application.DoEvents();
            }
            groupCount = groups.Count;
            groupLen = group != null ? group.Length : 0;
            return menuStrip;
        }

        private static string GetGroup( string text, Dictionary<string,ToolStripMenuItem> groups, int limit )
        {
            foreach( string group in groups.Keys )
            {
                string substring = StringCompare.StartingSubstring( text, group );
                if( !String.IsNullOrEmpty( substring ) )
                {
                    substring = substring.Trim();
                    if( substring.Length > limit )
                    {
                        if( group != substring )
                        {
                            groups.Add( substring, groups[group] );
                            groups.Remove( group );
                            groups[substring].Text = substring;
                        }
                        return substring;
                    }
                }
                Application.DoEvents();
            }
            return text;
        }

        private static void SetGroup( string text, Dictionary<string, ToolStripMenuItem> groups, ref string group, ContextMenuStrip menuStrip )
        {
            if( groups.ContainsKey( group ) )
                return;

            foreach( string otherGroup in groups.Keys )
            {
                if( text.Contains( otherGroup ) )
                {
                    group = otherGroup;
                    return;
                }
            }
            ToolStripMenuItem groupItem = new ToolStripMenuItem( group );
            menuStrip.Items.Add( groupItem );
            groups.Add( group, groupItem );
        }

        private static ToolStripMenuItem CreateMenuItem( ProcessOption option, int ql )
        {
            Item item = option.Item.GetNearest( ql );
            ItemInfo info = ItemParser.Interpolate( item );
            AOIcon icon = AOIcon.Load( info.Attributes[AttributeKey.Icon] );

            ToolStripMenuItem menuItem = new ToolStripMenuItem( info.Name );
            if( icon != null )
                menuItem.Image = icon.Image;
            menuItem.Tag = option;
            return menuItem;
        }
    }
}
