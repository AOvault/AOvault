﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using Twp.Utilities;

namespace AOCrafter.Core
{
    public enum SkillName
    {
        Mechanical_Engineering,
        Electrical_Engineering,
        Quantum_Force_field_Technology,
        Weapon_Smithing,
        Pharmaceutical_Technology,
        Nano_Programming,
        Computer_Literacy,
        Psychology,
        Chemistry,
        Breaking_and_Entry,
        Strength,
        Stamina,
        Agility,
        Intelligence,
        Psychic,
        Sense,
        Tutoring,
    }

    [DefaultProperty( "Key" )]
    public class Skill : IEquatable<Skill>, IComparable<Skill>, IComparable, INotifyPropertyChanged
    {
        #region Constructors

        /// <summary>
        /// Creates a new instance of the <see cref="AOCrafter.Core.Skill"/> class, with default values.
        /// </summary>
        public Skill()
        {
        }

        /// <summary>
        /// Creates a copy of another <see cref="AOCrafter.Core.Skill"/> object.
        /// </summary>
        /// <param name="other">The <see cref="AOCrafter.Core.Skill"/> to copy.</param>
        public Skill( Skill other )
        {
            this.key = other.Key;
            this.value = other.Value;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the key name of the <see cref="AOCrafter.Core.Skill"/>.
        /// </summary>
        [Category( "Skill" )]
        [Description( "The skill that is required to complete the process." )]
        [DefaultValue( SkillName.Electrical_Engineering )]
        public SkillName Key
        {
            get { return this.key; }
            set
            {
                if( this.key != value )
                {
                    this.key = value;
                    this.OnPropertyChanged( new PropertyChangedEventArgs( "Key" ) );
                }
            }
        }
        private SkillName key = SkillName.Electrical_Engineering;

        /// <summary>
        /// Gets or sets the value of the <see cref="AOCrafter.Core.Skill"/>.
        /// </summary>
        [Category( "Skill" )]
        [Description( "The value determines how much skill, in relation to the primary object, is needed to complete the process." )]
        [DefaultValue( typeof( decimal ), "1.0" )]
        public decimal Value
        {
            get { return this.value; }
            set
            {
                if( this.value != value )
                {
                    this.value = value;
                    this.OnPropertyChanged( new PropertyChangedEventArgs( "Value" ) );
                }
            }
        }
        private decimal value = 1.0m;

        /// <summary>
        /// Gets a value representing the calculated value of the <see cref="AOCrafter.Core.Skill"/>
        /// </summary>
        [Browsable( false )]
        public int FinalValue
        {
            get { return this.finalValue; }
        }
        private int finalValue = 0;

        #endregion

        #region Methods

        /// <summary>
        /// Calculates the final value of the <see cref="AOCrafter.Core.Skill"/> based on the value and <paramref name="ql"/>.
        /// </summary>
        /// <param name="ql">A value reprensenting the quality level used in calculation.</param>
        public void Calculate( int ql )
        {
            Decimal tempValue = Decimal.Multiply( this.value, ql );
            tempValue = Decimal.Ceiling( tempValue );
            this.finalValue = Decimal.ToInt32( tempValue );
            Log.Debug( "[Skill.Calculate] Skill: {0} = {1}", this.key, this.finalValue );
        }

        /// <summary>
        /// Cleans up skillnames for user display, by replacing underscores with spaces.
        /// </summary>
        /// <param name="key">A <see cref="AOCrafter.Core.SkillName"/> which should get cleaned.</param>
        /// <returns>A string represnting the cleaned skillname.</returns>
        public static string GetName( SkillName key )
        {
            return key.ToString().Replace( '_', ' ' );
        }

        /// <summary>
        /// Retrieves an icon ID suitable for a <see cref="AOCrafter.Core.SkillName"/>.
        /// </summary>
        /// <param name="key">A <see cref="AOCrafter.Core.SkillName"/> for which to find an icon ID.</param>
        /// <returns>The icon ID, or 0 if the SkillNames was invalid.</returns>
        public static int GetIcon( SkillName key )
        {
            switch( key )
            {
                case SkillName.Mechanical_Engineering:
                    return 16293;
                case SkillName.Electrical_Engineering:
                    return 16231;
                case SkillName.Quantum_Force_field_Technology:
                    return 16244;
                case SkillName.Weapon_Smithing:
                    return 16349;
                case SkillName.Pharmaceutical_Technology:
                    return 16316;
                case SkillName.Nano_Programming:
                    return 16301;
                case SkillName.Computer_Literacy:
                    return 16213;
                case SkillName.Psychology:
                    return 16309;
                case SkillName.Chemistry:
                    return 16210;
                case SkillName.Breaking_and_Entry:
                    return 16205;
                case SkillName.Strength:
                    return 16324;
                case SkillName.Stamina:
                    return 16330;
                case SkillName.Agility:
                    return 16218;
                case SkillName.Intelligence:
                    return 16251;
                case SkillName.Psychic:
                    return 16315;
                case SkillName.Sense:
                    return 16317;
                case SkillName.Tutoring:
                    return 16343;
            }
            return 0;
        }

        public override string ToString()
        {
            return GetName( this.key );
        }

        #endregion

        #region IEquatable

        public override int GetHashCode()
        {
            return this.key.GetHashCode();
        }

        public bool Equals( Skill other )
        {
            if( other == null )
                return false;
            else
                return this.key.Equals( other.key );
        }

        public override bool Equals( object obj )
        {
            Skill other = obj as Skill;
            if( other != null )
                return this.Equals( other );
            else
                return false;
        }

        public static bool operator ==( Skill skill1, Skill skill2 )
        {
            if( object.ReferenceEquals( skill1, skill2 ) )
                return true;
            if( object.ReferenceEquals( skill1, null ) )
                return false;
            if( object.ReferenceEquals( skill2, null ) )
                return false;

            return skill1.Equals( skill2 );
        }

        public static bool operator !=( Skill skill1, Skill skill2 )
        {
            if( object.ReferenceEquals( skill1, skill2 ) )
                return false;
            if( object.ReferenceEquals( skill1, null ) )
                return true;
            if( object.ReferenceEquals( skill2, null ) )
                return true;

            return !skill1.Equals( skill2 );
        }

        #endregion

        #region IComparable

        public int CompareTo( Skill other )
        {
            return GetName( this.key ).CompareTo( GetName( other.key ) );
        }

        public int CompareTo( object obj )
        {
            Skill other = obj as Skill;
            if( other != null )
                return this.CompareTo( other );
            else
                return -1;
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged( PropertyChangedEventArgs e )
        {
            if( this.PropertyChanged != null )
                this.PropertyChanged( this, e );
        }

        #endregion
    }
}
