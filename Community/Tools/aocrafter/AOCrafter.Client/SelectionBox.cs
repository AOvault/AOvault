﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

using AOCrafter.Core;
using Twp.Controls;
using Twp.Utilities;

namespace AOCrafter.Client
{
    public partial class SelectionBox : UserControl
    {
#region Constructor
        public SelectionBox()
        {
            InitializeComponent();
        }
#endregion

#region Properties

        private Tradeskill tradeskill = null;
        public Tradeskill Tradeskill
        {
            get { return this.tradeskill; }
            set
            {
                if( this.tradeskill != value && value != null )
                {
                    int groupIndex = this.groupBox.FindStringExact( value.GroupName );
                    if( groupIndex == -1 )
                        throw new ArgumentException( "Setting Tradeskill failed! Group was not found!" );
                    this.groupBox.SelectedIndex = groupIndex;

                    int tsIndex = this.tradeskillBox.FindStringExact( value.ToString() );
                    if( tsIndex == -1 )
                        throw new ArgumentException( "Setting Tradeskill failed! Tradeskill not found!" );
                    this.tradeskillBox.SelectedIndex = tsIndex;
                }
            }
        }

        public int QL
        {
            get
            {
                return (int) Decimal.Round( this.qlSlider.Value, MidpointRounding.ToEven );
            }
        }

        public bool MaxQL
        {
            get { return this.maxQLButton.Checked; }
        }

#endregion

#region Events

        public event EventHandler CalcClicked;

#endregion

#region Methods

        public void Init()
        {
            this.groupBox.DataSource = Tradeskill.GroupNames;
        }

        private void OnGroupChanged( object sender, EventArgs e )
        {
            string group = (string) this.groupBox.SelectedItem;
            TradeskillList list = Tradeskill.FromGroup( group );
            list.Sort();
            this.tradeskillBox.DataSource = list;
        }

        private void OnTradeskillChanged( object sender, EventArgs e )
        {
            this.tradeskill = (Tradeskill) this.tradeskillBox.SelectedItem;
            if( this.tradeskill != null )
            {
                this.qlSlider.Minimum = this.tradeskill.LowQL;
                this.qlSlider.Maximum = this.tradeskill.HighQL;
                this.qlSlider.Enabled = !this.tradeskill.StaticQL;
                this.qlBox.Minimum = this.tradeskill.LowQL;
                this.qlBox.Maximum = this.tradeskill.HighQL;
                this.qlBox.Enabled = !this.tradeskill.StaticQL;
                this.maxQLButton.Enabled = !this.tradeskill.StaticQL; 
            }
            else
            {
                Twp.Utilities.Log.Warning( "No tradeskill selected!" );
                this.qlSlider.Enabled = false;
                this.qlBox.Enabled = false;
                this.maxQLButton.Enabled = false;
            }
        }

        private void QlSliderValueChanged( object sender, EventArgs e )
        {
            this.qlBox.Value = this.qlSlider.Value;
        }

        private void QlBoxValueChanged( object sender, EventArgs e )
        {
            this.qlSlider.Value = this.qlBox.Value;
        }

        private void OnMaxQLCheckedChanged( object sender, EventArgs e )
        {
            this.qlSlider.Enabled = !this.maxQLButton.Checked;
        }

        private void OnCalcClicked( object sender, EventArgs e )
        {
            if( this.CalcClicked != null )
                this.CalcClicked( this, EventArgs.Empty );
        }

        // We use OwnerDrawFixed on group box only to give the two comboboxes a unified look...
        private void OnDrawGroup( object sender, DrawItemEventArgs e )
        {
            e.DrawBackground();

            if( e.Index < 0 )
                return;

            using( SolidBrush brush = new SolidBrush( e.ForeColor ) )
            {
                StringFormat sf = new StringFormat();
                sf.Alignment = StringAlignment.Near;
                sf.LineAlignment = StringAlignment.Center;
                e.Graphics.DrawString( this.groupBox.Items[e.Index].ToString(), e.Font, brush, e.Bounds, sf );
            }

            e.DrawFocusRectangle();
        }

        private void OnDrawTradeskill( object sender, DrawItemEventArgs e )
        {
            e.DrawBackground();

            if( e.Index < 0 )
                return;

            Tradeskill item = (Tradeskill) this.tradeskillBox.Items[e.Index];
            if( item == null )
                return;

            Color expColor = Color.Empty;
            string exp = string.Empty;
            switch( item.Expansion )
            {
                case Expansions.Notum_Wars:
                    expColor = Color.Gold;
                    exp = "NW";
                    break;
                case Expansions.Shadowlands:
                    expColor = Color.RoyalBlue;
                    exp = "SL";
                    break;
                case Expansions.Alien_Invasion:
                    expColor = Color.LimeGreen;
                    exp = "AI";
                    break;
                case Expansions.Lost_Eden:
                    expColor = Color.DarkGray;
                    exp = "LE";
                    break;
                case Expansions.Legacy_Of_Xan:
                    expColor = Color.DarkSeaGreen;
                    exp = "LoX";
                    break;
            }

            using( SolidBrush brush = new SolidBrush( e.ForeColor ) )
            {
                StringFormat sf = new StringFormat();
                sf.LineAlignment = StringAlignment.Center;
                if( expColor != Color.Empty )
                {
                    using( LinearGradientBrush gradient = GradientBrush.Create( e.Bounds, e.BackColor,
                                                                                expColor, GradientMode.Diagonal ) )
                    {
                        e.Graphics.FillRectangle( gradient, e.Bounds );
                        sf.Alignment = StringAlignment.Far;
                        e.Graphics.DrawString( exp, e.Font, brush, e.Bounds, sf );
                    }
                }
                sf.Alignment = StringAlignment.Near;
                e.Graphics.DrawString( item.ToString(), e.Font, brush, e.Bounds, sf );
            }
            e.DrawFocusRectangle();
        }

#endregion
    }
}
