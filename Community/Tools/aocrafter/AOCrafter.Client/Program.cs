﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AOCrafter.Core;
using Twp.FC.AO;
using Twp.Utilities;

namespace AOCrafter.Client
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault( false );
#if !DEBUG
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler( OnThreadException );
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler( OnUnhandledException );
#endif
            Log.Start( "client.log", Path.GetAppDataPath( "WrongPlace.Net", "AOCrafter" ) );
            Settings.Load();
            MySkills.Load();

            string path = Program.Settings.AOPath;
            if( GamePath.Confirm( ref path ) )
                Program.Settings.AOPath = path;
            else
                Application.Exit();

#if !DEBUG
            DatabaseManager.DataPath = Program.Settings.DataPath;
#endif
            DatabaseManager.UpdateIfOutdated( path, false );
            DatabaseManager.Open();
            DatabaseManager.OpenMmdb( path );

            Application.Run( new MainForm() );

            DatabaseManager.Close();
            MySkills.Save();
            Settings.Save();
        }

        internal static Settings Settings = new Settings();
        internal static Data.MySkills MySkills = new Data.MySkills();
        private static readonly Dictionary<int, LinkedProcessForm> linkedForms = new Dictionary<int, LinkedProcessForm>();

        internal static void OpenLinkedTradeskill( Tradeskill tradeskill, int ql )
        {
            LinkedProcessForm form;
            if( linkedForms.ContainsKey( tradeskill.Id ) )
            {
                form = linkedForms[tradeskill.Id];
            }
            else
            {
                form = new LinkedProcessForm();
                form.FormClosing += delegate( object sender, FormClosingEventArgs e ) {
                    form.Hide();
                    e.Cancel = true;
                };
                linkedForms.Add( tradeskill.Id, form );
            }
            form.SetTradeskill( tradeskill, ql, false );
            form.Show();
        }

#if !DEBUG
        private static void OnThreadException( object sender, System.Threading.ThreadExceptionEventArgs e )
        {
            Twp.Controls.ExceptionDialog.Show( e.Exception );
        }

        private static void OnUnhandledException( object sender, UnhandledExceptionEventArgs e )
        {
            Twp.Controls.ExceptionDialog.Show( e.ExceptionObject as Exception );
        }
#endif
    }
}
