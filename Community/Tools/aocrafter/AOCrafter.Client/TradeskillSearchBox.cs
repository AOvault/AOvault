﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

using AOCrafter.Core;
using AOCrafter.Core.Controls;
using Twp.Controls;
using Twp.Data.AO;

namespace AOCrafter.Client
{
    public partial class TradeskillSearchBox : UserControl
    {
#region Constructor

        public TradeskillSearchBox()
        {
            InitializeComponent();
            this.progressForm.Label = "Searching...";
            this.progressForm.BackColor = Color.FromArgb( 50, 50, 60 );
            this.progressForm.ForeColor = Color.FromArgb( 99, 173, 99 );
            this.progressForm.ProgressColor = Color.FromArgb( 99, 173, 99 );
        }

#endregion

#region Private fields

        private SimpleProgressForm progressForm = new SimpleProgressForm();

#endregion

#region Search

        private void OnSearch( object sender, SearchEventArgs e )
        {
            this.progressForm.Show( this );
            this.progressForm.Start();
            using( BackgroundWorker bw = new BackgroundWorker() )
            {
                bw.DoWork += new DoWorkEventHandler( DoSearch );
                bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler( OnSearchCompleted );
                bw.RunWorkerAsync( e.Words );
            }
        }

        private void DoSearch( object sender, DoWorkEventArgs e )
        {
            string[] args = e.Argument as string[];
            if( args == null || args.Length == 0 )
                return;
            else
                e.Result = Tradeskill.Search( args );
        }

        private void OnSearchCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            this.resultList.Items.Clear();
            TradeskillList tradeskills = e.Result as TradeskillList;

            if( tradeskills == null || tradeskills.Count == 0 )
            {
                this.resultList.Enabled = false;
                this.countLabel.Text = "No tradeskills found.";
            }
            else
            {
                this.resultList.Enabled = true;
                foreach( Tradeskill tradeskill in tradeskills )
                {
                    this.listView_Add( tradeskill );
                }
                this.countLabel.Text = "Tradeskills found: " + tradeskills.Count.ToString();
            }
            this.progressForm.Stop();
            this.progressForm.Hide();
        }

        private void listView_Add( Tradeskill tradeskill )
        {
            //Log.Debug( "[TradeskillListBox.listView_Add] Id: {0}", tradeskill.Id );
            ListViewItem listItem = new ListViewItem();
            listItem.SubItems.Add( tradeskill.Id.ToString() );
            listItem.SubItems.Add( String.Format( "[{0}] {1}", tradeskill.GroupName, tradeskill.Name ) );
            listItem.Tag = tradeskill;

            if( tradeskill.Processes.Count > 0 )
            {
                Item item = tradeskill.Processes[0].Result.GetNearest( 100 );
                if( item == null )
                    return;
                AOItem aoitem = item.Interpolate( Twp.Data.AO.DatabaseManager.ItemsDatabase );
                this.icons_Add( aoitem.Icon );
                listItem.ImageKey = aoitem.Icon.ToString();
            }

            this.resultList.Items.Add( listItem );
        }

        private void icons_Add( int id )
        {
            //Log.Debug( "[TradeskillListBox.icons_Add] Id: {0}", id );
            if( this.icons.Images.ContainsKey( id.ToString() ) == false )
            {
                AOIcon icon = AOIcon.Load( id );
                if( icon != null )
                    this.icons.Images.Add( icon.Id.ToString(), icon.Image );
            }
        }

#endregion

#region Selection

        public event EventHandler SelectionChanged;

        public Tradeskill SelectedTradeskill
        {
            get
            {
                if( this.resultList.SelectedItems.Count != 1 )
                    return null;

                ListViewItem listItem = this.resultList.SelectedItems[0];
                if( listItem == null )
                    return null;

                return (Tradeskill) listItem.Tag;
            }
        }

        private void OnOpenTradeskill( object sender, EventArgs e )
        {
            if( this.SelectionChanged != null )
                this.SelectionChanged( this, EventArgs.Empty );
        }

#endregion
    }
}
