﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace AOCrafter.Client
{
    partial class SelectionBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupLabel = new System.Windows.Forms.Label();
            this.groupBox = new System.Windows.Forms.ComboBox();
            this.tradeskillBox = new System.Windows.Forms.ComboBox();
            this.tsLabel = new System.Windows.Forms.Label();
            this.maxQLButton = new System.Windows.Forms.CheckBox();
            this.qlLabel = new System.Windows.Forms.Label();
            this.calcButton = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.qlSlider = new Twp.Controls.Slider();
            this.qlBox = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.qlBox)).BeginInit();
            this.SuspendLayout();
            // 
            // groupLabel
            // 
            this.groupLabel.AutoSize = true;
            this.groupLabel.Location = new System.Drawing.Point(0, 4);
            this.groupLabel.Name = "groupLabel";
            this.groupLabel.Size = new System.Drawing.Size(52, 13);
            this.groupLabel.TabIndex = 0;
            this.groupLabel.Text = "Category:";
            this.groupLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // groupBox
            // 
            this.groupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.groupBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.groupBox.FormattingEnabled = true;
            this.groupBox.Location = new System.Drawing.Point(60, 1);
            this.groupBox.Name = "groupBox";
            this.groupBox.Size = new System.Drawing.Size(249, 21);
            this.groupBox.TabIndex = 1;
            this.toolTip.SetToolTip(this.groupBox, "Tradeskill Groups");
            this.groupBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.OnDrawGroup);
            this.groupBox.SelectedIndexChanged += new System.EventHandler(this.OnGroupChanged);
            // 
            // tradeskillBox
            // 
            this.tradeskillBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tradeskillBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.tradeskillBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tradeskillBox.FormattingEnabled = true;
            this.tradeskillBox.Location = new System.Drawing.Point(60, 28);
            this.tradeskillBox.Name = "tradeskillBox";
            this.tradeskillBox.Size = new System.Drawing.Size(249, 21);
            this.tradeskillBox.TabIndex = 3;
            this.toolTip.SetToolTip(this.tradeskillBox, "Tradeskills in the selected group.");
            this.tradeskillBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.OnDrawTradeskill);
            this.tradeskillBox.SelectedIndexChanged += new System.EventHandler(this.OnTradeskillChanged);
            // 
            // tsLabel
            // 
            this.tsLabel.AutoSize = true;
            this.tsLabel.Location = new System.Drawing.Point(0, 31);
            this.tsLabel.Name = "tsLabel";
            this.tsLabel.Size = new System.Drawing.Size(55, 13);
            this.tsLabel.TabIndex = 2;
            this.tsLabel.Text = "Tradeskill:";
            this.tsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // maxQLButton
            // 
            this.maxQLButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.maxQLButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.maxQLButton.Location = new System.Drawing.Point(267, 55);
            this.maxQLButton.Name = "maxQLButton";
            this.maxQLButton.Size = new System.Drawing.Size(42, 21);
            this.maxQLButton.TabIndex = 6;
            this.maxQLButton.Text = "Max";
            this.maxQLButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip.SetToolTip(this.maxQLButton, "Calculate the maximum possible quality of the result, based on your skills.");
            this.maxQLButton.UseVisualStyleBackColor = true;
            this.maxQLButton.CheckedChanged += new System.EventHandler(this.OnMaxQLCheckedChanged);
            // 
            // qlLabel
            // 
            this.qlLabel.AutoSize = true;
            this.qlLabel.Location = new System.Drawing.Point(0, 59);
            this.qlLabel.Name = "qlLabel";
            this.qlLabel.Size = new System.Drawing.Size(42, 13);
            this.qlLabel.TabIndex = 4;
            this.qlLabel.Text = "Quality:";
            this.qlLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // calcButton
            // 
            this.calcButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.calcButton.AutoSize = true;
            this.calcButton.Image = global::AOCrafter.Client.Properties.Resources.Gears1;
            this.calcButton.Location = new System.Drawing.Point(315, 0);
            this.calcButton.Name = "calcButton";
            this.calcButton.Size = new System.Drawing.Size(76, 76);
            this.calcButton.TabIndex = 7;
            this.toolTip.SetToolTip(this.calcButton, "Calculate!");
            this.calcButton.UseVisualStyleBackColor = true;
            this.calcButton.Click += new System.EventHandler(this.OnCalcClicked);
            // 
            // qlSlider
            // 
            this.qlSlider.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.qlSlider.LabelPosition = Twp.Controls.LabelPosition.Disabled;
            this.qlSlider.Location = new System.Drawing.Point(60, 55);
            this.qlSlider.Name = "qlSlider";
            this.qlSlider.Size = new System.Drawing.Size(157, 21);
            this.qlSlider.TabIndex = 5;
            this.toolTip.SetToolTip(this.qlSlider, "The Quality Level of the result.");
            this.qlSlider.ValueChanged += new System.EventHandler(this.QlSliderValueChanged);
            // 
            // qlBox
            // 
            this.qlBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.qlBox.Location = new System.Drawing.Point(221, 56);
            this.qlBox.Name = "qlBox";
            this.qlBox.Size = new System.Drawing.Size(42, 20);
            this.qlBox.TabIndex = 8;
            this.qlBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.qlBox.ValueChanged += new System.EventHandler(this.QlBoxValueChanged);
            // 
            // SelectionBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.qlBox);
            this.Controls.Add(this.qlLabel);
            this.Controls.Add(this.maxQLButton);
            this.Controls.Add(this.qlSlider);
            this.Controls.Add(this.tsLabel);
            this.Controls.Add(this.tradeskillBox);
            this.Controls.Add(this.groupBox);
            this.Controls.Add(this.calcButton);
            this.Controls.Add(this.groupLabel);
            this.Name = "SelectionBox";
            this.Size = new System.Drawing.Size(390, 76);
            ((System.ComponentModel.ISupportInitialize)(this.qlBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label groupLabel;
        private System.Windows.Forms.Button calcButton;
        private System.Windows.Forms.ComboBox groupBox;
        private System.Windows.Forms.ComboBox tradeskillBox;
        private System.Windows.Forms.Label tsLabel;
        private Twp.Controls.Slider qlSlider;
        private System.Windows.Forms.CheckBox maxQLButton;
        private System.Windows.Forms.Label qlLabel;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.NumericUpDown qlBox;
    }
}
