﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace AOCrafter.Client
{
    partial class SummaryBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.icons = new System.Windows.Forms.ImageList(this.components);
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.skillsView = new Twp.Controls.FillListView();
            this.skillHeader = new System.Windows.Forms.ColumnHeader();
            this.valueHeader = new System.Windows.Forms.ColumnHeader();
            this.toolsView = new Twp.Controls.FillListView();
            this.itemHeader2 = new System.Windows.Forms.ColumnHeader();
            this.qlHeader2 = new System.Windows.Forms.ColumnHeader();
            this.consumedView = new Twp.Controls.FillListView();
            this.itemHeader1 = new System.Windows.Forms.ColumnHeader();
            this.qlHeader1 = new System.Windows.Forms.ColumnHeader();
            this.countHeader1 = new System.Windows.Forms.ColumnHeader();
            this.consumedHeader = new Twp.Controls.VLabel();
            this.toolsHeader = new Twp.Controls.VLabel();
            this.skillsHeader = new Twp.Controls.VLabel();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // icons
            // 
            this.icons.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.icons.ImageSize = new System.Drawing.Size(16, 16);
            this.icons.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Controls.Add(this.skillsView, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.toolsView, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.consumedView, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.consumedHeader, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.toolsHeader, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.skillsHeader, 0, 2);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 3;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(382, 311);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // skillsView
            // 
            this.skillsView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
                                    this.skillHeader,
                                    this.valueHeader});
            this.skillsView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.skillsView.FullRowSelect = true;
            this.skillsView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.skillsView.LargeImageList = this.icons;
            this.skillsView.Location = new System.Drawing.Point(18, 218);
            this.skillsView.Margin = new System.Windows.Forms.Padding(0, 2, 1, 0);
            this.skillsView.MultiSelect = false;
            this.skillsView.Name = "skillsView";
            this.skillsView.ShowGroups = false;
            this.skillsView.Size = new System.Drawing.Size(363, 93);
            this.skillsView.SmallImageList = this.icons;
            this.skillsView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.skillsView.TabIndex = 5;
            this.skillsView.UseCompatibleStateImageBehavior = false;
            this.skillsView.View = System.Windows.Forms.View.Details;
            // 
            // skillHeader
            // 
            this.skillHeader.Text = "Skill name";
            this.skillHeader.Width = 309;
            // 
            // valueHeader
            // 
            this.valueHeader.Text = "Value";
            this.valueHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.valueHeader.Width = 50;
            // 
            // toolsView
            // 
            this.toolsView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
                                    this.itemHeader2,
                                    this.qlHeader2});
            this.toolsView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolsView.FullRowSelect = true;
            this.toolsView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.toolsView.LargeImageList = this.icons;
            this.toolsView.Location = new System.Drawing.Point(18, 110);
            this.toolsView.Margin = new System.Windows.Forms.Padding(0, 2, 1, 4);
            this.toolsView.MultiSelect = false;
            this.toolsView.Name = "toolsView";
            this.toolsView.ShowGroups = false;
            this.toolsView.Size = new System.Drawing.Size(363, 102);
            this.toolsView.SmallImageList = this.icons;
            this.toolsView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.toolsView.TabIndex = 3;
            this.toolsView.UseCompatibleStateImageBehavior = false;
            this.toolsView.View = System.Windows.Forms.View.Details;
            // 
            // itemHeader2
            // 
            this.itemHeader2.Text = "Item name";
            this.itemHeader2.Width = 309;
            // 
            // qlHeader2
            // 
            this.qlHeader2.Text = "QL";
            this.qlHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.qlHeader2.Width = 50;
            // 
            // consumedView
            // 
            this.consumedView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
                                    this.itemHeader1,
                                    this.qlHeader1,
                                    this.countHeader1});
            this.consumedView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.consumedView.FullRowSelect = true;
            this.consumedView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.consumedView.LargeImageList = this.icons;
            this.consumedView.Location = new System.Drawing.Point(18, 2);
            this.consumedView.Margin = new System.Windows.Forms.Padding(0, 2, 1, 4);
            this.consumedView.MultiSelect = false;
            this.consumedView.Name = "consumedView";
            this.consumedView.ShowGroups = false;
            this.consumedView.Size = new System.Drawing.Size(363, 102);
            this.consumedView.SmallImageList = this.icons;
            this.consumedView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.consumedView.TabIndex = 1;
            this.consumedView.UseCompatibleStateImageBehavior = false;
            this.consumedView.View = System.Windows.Forms.View.Details;
            // 
            // itemHeader1
            // 
            this.itemHeader1.Text = "Item name";
            this.itemHeader1.Width = 259;
            // 
            // qlHeader1
            // 
            this.qlHeader1.Text = "QL";
            this.qlHeader1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.qlHeader1.Width = 50;
            // 
            // countHeader1
            // 
            this.countHeader1.Text = "Count";
            this.countHeader1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.countHeader1.Width = 50;
            // 
            // consumedHeader
            // 
            this.consumedHeader.BackColor = System.Drawing.Color.Transparent;
            this.consumedHeader.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.consumedHeader.Location = new System.Drawing.Point(0, 110);
            this.consumedHeader.Margin = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.consumedHeader.Name = "consumedHeader";
            this.consumedHeader.Size = new System.Drawing.Size(18, 43);
            this.consumedHeader.TabIndex = 2;
            this.consumedHeader.Text = "Tools";
            // 
            // toolsHeader
            // 
            this.toolsHeader.BackColor = System.Drawing.Color.Transparent;
            this.toolsHeader.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolsHeader.Location = new System.Drawing.Point(0, 2);
            this.toolsHeader.Margin = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.toolsHeader.Name = "toolsHeader";
            this.toolsHeader.Size = new System.Drawing.Size(18, 73);
            this.toolsHeader.TabIndex = 0;
            this.toolsHeader.Text = "Consumed";
            // 
            // skillsHeader
            // 
            this.skillsHeader.BackColor = System.Drawing.Color.Transparent;
            this.skillsHeader.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.skillsHeader.Location = new System.Drawing.Point(0, 218);
            this.skillsHeader.Margin = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.skillsHeader.Name = "skillsHeader";
            this.skillsHeader.Size = new System.Drawing.Size(18, 43);
            this.skillsHeader.TabIndex = 4;
            this.skillsHeader.Text = "Skills";
            // 
            // SummaryBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "SummaryBox";
            this.Size = new System.Drawing.Size(382, 311);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.ImageList icons;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private Twp.Controls.FillListView toolsView;
        private System.Windows.Forms.ColumnHeader itemHeader2;
        private System.Windows.Forms.ColumnHeader qlHeader2;
        private Twp.Controls.FillListView consumedView;
        private System.Windows.Forms.ColumnHeader itemHeader1;
        private System.Windows.Forms.ColumnHeader qlHeader1;
        private System.Windows.Forms.ColumnHeader countHeader1;
        private Twp.Controls.FillListView skillsView;
        private System.Windows.Forms.ColumnHeader skillHeader;
        private System.Windows.Forms.ColumnHeader valueHeader;
        private Twp.Controls.VLabel consumedHeader;
        private Twp.Controls.VLabel toolsHeader;
        private Twp.Controls.VLabel skillsHeader;
    }
}
