﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;

namespace AOCrafter.Client.Data
{
    internal class SummaryList<T> : List<SummaryNode<T>>
    {
        internal void Add( T item, int value, int count )
        {
            if( this.Contains( item ) )
            {
                this[item].Count += count;
                if( this[item].Value < value )
                    this[item].Value = value;
            }
            else
            {
                SummaryNode<T> node = new SummaryNode<T>( item );
                node.Count = count;
                node.Value = value;
                this.Add( node );
            }
        }

        private bool Contains( T item )
        {
            foreach( SummaryNode<T> node in this )
            {
                if( node.Item.Equals( item ) )
                    return true;
            }
            return false;
        }

        private SummaryNode<T> this[T item]
        {
            get
            {
                foreach( SummaryNode<T> node in this )
                {
                    if( node.Item.Equals( item ) )
                        return node;
                }
                return null;
            }
        }
    }
}
