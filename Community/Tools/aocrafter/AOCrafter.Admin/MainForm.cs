﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.Windows.Forms;

using AOCrafter.Core;
using Twp.Utilities;
using WeifenLuo.WinFormsUI.Docking;

namespace AOCrafter.Admin
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            this.aoItemsDock = new AOItemsDock();
            this.itemsDock = new ItemsDock();
            this.processesDock = new ProcessesDock();
            this.tradeskillsDock = new TradeskillsDock();
            this.helpDock = new HelpDock();
            this.statisticsDock = new StatisticsDock();
            this.dropDock = new DropDock();

            this.dropDock.Show( this.dockPanel, DockState.DockBottom );
            this.dockPanel.DockWindows[DockState.DockBottom].BringToFront();

            this.aoItemsDock.CreateItem += this.OnCreateEditor;
            this.aoItemsDock.Show( this.dockPanel, DockState.DockLeft );

            this.itemsDock.CreateProcess += this.OnCreateEditor;
            this.itemsDock.OpenItems += this.OnOpenEditors;
            this.itemsDock.Show( this.dockPanel, DockState.DockLeft );

            this.processesDock.CreateTradeskill += this.OnCreateEditor;
            this.processesDock.OpenProcesses += this.OnOpenEditors;
            this.processesDock.Show( this.dockPanel, DockState.DockLeft );

            this.tradeskillsDock.OpenTradeskills += this.OnOpenEditors;
            this.tradeskillsDock.Show( this.dockPanel, DockState.DockLeft );

            this.helpDock.Show( this.dockPanel, DockState.DockRight );
            this.statisticsDock.Show( this.helpDock.Pane, DockAlignment.Bottom, 0.25 );

#if DEBUG
            ConsoleDock consoleDock = new ConsoleDock();
            consoleDock.CloseButtonVisible = false;
            consoleDock.Show( this.dockPanel, DockState.DockBottom );
#endif
        }

        private AOItemsDock aoItemsDock;
        private ItemsDock itemsDock;
        private ProcessesDock processesDock;
        private TradeskillsDock tradeskillsDock;
        private HelpDock helpDock;
        private StatisticsDock statisticsDock;
        private DropDock dropDock;

        private List<DataEditor> editors = new List<DataEditor>();

        public string StatusText
        {
            get { return this.toolStripStatusLabel.Text; }
            set { this.toolStripStatusLabel.Text = value; }
        }

        private void OnLoad( object sender, EventArgs e )
        {
            //SearchHistory.Read();
        }

        private void OnShown( object sender, EventArgs e )
        {
            this.tradeskillsDock.ShowAll();
            this.addNewItemsToDropBoxToolStripMenuItem.Checked = Program.Settings.AddNewItemsToDropBox;
        }

        private void OnFormClosing( object sender, FormClosingEventArgs e )
        {
            // TODO: Check for unsaves changes here?
            // ... subwindows already do this.... is another check neccesary?
        }

        private void OnFormClosed( object sender, FormClosedEventArgs e )
        {
            //SearchHistory.Write();
        }

        private DataEditor CreateEditor( ICrafterData data )
        {
            DataEditor editor;
            if( data is ProcItem )
            {
                editor = new ItemEditor();
                editor.DataSaved += this.OnAddItemToDropBox;
            }
            else if( data is Process )
            {
                editor = new ProcessEditor();
            }
            else if( data is Tradeskill )
            {
                editor = new TradeskillEditor();
            }
            else
                throw new ArgumentException( "Incorrect data type.", "data" );

            editor.Data = data;
            editor.OpenEditor += this.OnOpenEditor;
            editor.OpenEditors += this.OnOpenEditors;
            editor.CreateEditor += this.OnCreateEditor;
            editor.Show( this.dockPanel, DockState.Document );
            return editor;
        }

        private void OpenEditor( ICrafterData data )
        {
            bool found = false;
            foreach( DataEditor editor in this.editors )
            {
                if( editor.Data == null )
                    continue;
                if( editor.Data.GetType() == data.GetType() && editor.Data.Id == data.Id )
                {
                    editor.Activate();
                    found = true;
                    break;
                }
            }
            if( !found )
            {
                DataEditor editor = this.CreateEditor( data );
                this.editors.Add( editor );
            }
        }

        private void OnNewTradeskillClicked( object sender, EventArgs e )
        {
            this.CreateEditor( new Tradeskill() );
        }

        private void OnNewProcessClicked( object sender, EventArgs e )
        {
            this.CreateEditor( new Process() );
        }

        private void OnNewItemClicked( object sender, EventArgs e )
        {
            this.CreateEditor( new ProcItem() );
        }

        private void OnQuitClicked( object sender, EventArgs e )
        {
            this.Close();
        }

        private void OnItemsDockToggled( object sender, EventArgs e )
        {
            if( this.itemsToolStripMenuItem.Checked )
                this.itemsDock.Hide();
            else
                this.itemsDock.Show();
            this.itemsToolStripMenuItem.Checked = this.itemsDock.Visible;
        }

        private void OnProcessesDockToggled( object sender, EventArgs e )
        {
            if( this.processesToolStripMenuItem.Checked )
                this.processesDock.Hide();
            else
                this.processesDock.Show();
            this.processesToolStripMenuItem.Checked = this.processesDock.Visible;
        }

        private void OnTradeskillsDockToggled( object sender, EventArgs e )
        {
            if( this.tradeskillsToolStripMenuItem.Checked )
                this.tradeskillsDock.Hide();
            else
                this.tradeskillsDock.Show();
            this.tradeskillsToolStripMenuItem.Checked = this.tradeskillsDock.Visible;
        }

        private void OnDropDockToggled( object sender, EventArgs e )
        {
            if( this.dropDockToolStripMenuItem.Checked )
                this.dropDock.Hide();
            else
                this.dropDock.Show();
            this.dropDockToolStripMenuItem.Checked = this.dropDock.Visible;
        }

        private void OnHelpDockToggled( object sender, EventArgs e )
        {
            if( this.helpDockToolStripMenuItem.Checked )
                this.helpDock.Hide();
            else
                this.helpDock.Show();
            this.helpDockToolStripMenuItem.Checked = this.helpDock.Visible;
        }

        private void OnStatisticsDockToggled( object sender, EventArgs e )
        {
            if( this.statisticsToolStripMenuItem.Checked )
                this.statisticsDock.Hide();
            else
                this.statisticsDock.Show();
            this.statisticsToolStripMenuItem.Checked = this.statisticsDock.Visible;
        }

        private void OnAddNewItemsToDropBoxToggled( object sender, EventArgs e )
        {
            Program.Settings.AddNewItemsToDropBox = this.addNewItemsToDropBoxToolStripMenuItem.Checked;
        }

        private void OnDbRip( object sender, EventArgs e )
        {
            using( DbRipForm dbRipForm = new DbRipForm() )
            {
                dbRipForm.ShowDialog();
            }
        }

        private void OnCreateEditor( object sender, DataEventArgs e )
        {
            if( e.Data == null )
                return;

            this.CreateEditor( e.Data );
        }

        private void OnOpenEditor( object sender, DataEventArgs e )
        {
            if( e.Data == null )
                return;

            this.OpenEditor( e.Data );
        }

        private void OnOpenEditors( object sender, ItemsEventArgs e )
        {
            this.SuspendLayout();
            foreach( ICrafterData data in e.Items )
            {
                this.OpenEditor( data );
            }
            this.ResumeLayout( true );
        }

        private void OnAddItemToDropBox( object sender, DataEventArgs e )
        {
            if( !Program.Settings.AddNewItemsToDropBox )
                return;

            if( e.Data == null )
                return;

            ProcItem item = e.Data as ProcItem;
            if( item == null )
                return;

            ItemList dummyList = new ItemList();
            dummyList.Add( item );

            this.dropDock.AddItems( dummyList );
        }

        private void OnContentRemoved( object sender, DockContentEventArgs e )
        {
            DataEditor editor = e.Content as DataEditor;
            if( editor == null )
                return;

            Log.Debug( "[MainForm.OnContentRemoved] Editor: {0}", editor.Name );
            this.editors.Remove( editor );

            ToolStripManager.RevertMerge( this.toolStrip );
            editor = this.dockPanel.ActiveDocument as DataEditor;
            if( editor == null || editor.VisibleState == DockState.Unknown )
                return;
            Log.Debug( "[MainForm.OnContentRemoved] Active Editor: {0}", editor.Name );
            ToolStripManager.Merge( editor.toolStrip, this.toolStrip );
        }

        private void OnActiveDocumentChanged( object sender, EventArgs e )
        {
            DataEditor content = this.dockPanel.ActiveDocument as DataEditor;
            if( content == null )
            {
                this.helpDock.HelpText = null;
                return;
            }

            ToolStripManager.RevertMerge( this.toolStrip );
            ToolStripManager.Merge( content.toolStrip, this.toolStrip );

            this.helpDock.HelpText = content.HelpText;

//            switch( content.DefaultSearchType )
//            {
//                case "AOItem":
//                    this.aoItemsDock.Activate();
//                    break;
//                case "Item":
//                    this.itemsDock.Activate();
//                    break;
//                case "Process":
//                    this.processesDock.Activate();
//                    break;
//            }
        }
    }
}
