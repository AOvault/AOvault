﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Windows.Forms;
using AOCrafter.Core;
using Twp.FC.AO;
using Twp.Utilities;

namespace AOCrafter.Admin
{
    public partial class DbRipForm : Form
    {
        public DbRipForm()
        {
            InitializeComponent();
        }

        private void OnLoad( object sender, EventArgs e )
        {
            this.VerifyPath( Program.Settings.AOPath );
        }

        private void OnBrowse( object sender, EventArgs e )
        {
            string path = Program.Settings.AOPath;
            if( GamePath.Browse( ref path ) )
                this.VerifyPath( path );
        }

        private void VerifyPath( string path )
        {
            if( !GamePath.IsValid( path ) )
            {
                if( !String.IsNullOrEmpty( path ) )
                    Log.Warning( "The selected folder is not a valid Anarchy Online installation." );
                this.pathBox.Text = "**Error**";
                this.ripButton.Enabled = false;
                return;
            }
            else
            {
                this.pathBox.Text = path;
                this.ripButton.Enabled = true;
                Program.Settings.AOPath = path;
            }
        }

        private void OnRip( object sender, EventArgs e )
        {
            string path = Program.Settings.AOPath;
            if( !GamePath.IsValid( path ) )
                return;

            DatabaseManager.UpdateItems( path, 1000, true );
        }
    }
}
