﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Windows.Forms;
using Twp.FC.AO;
using Twp.Utilities;

namespace AOCrafter.Admin
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault( false );
#if !DEBUG
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler( OnThreadException );
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler( OnUnhandledException );
#endif
            Log.Start( "admin.log", Path.GetAppDataPath( "WrongPlace.Net", "AOCrafter" ) );
            Settings.Load();

            string path = Program.Settings.AOPath;
            if( GamePath.Confirm( ref path ) )
                Program.Settings.AOPath = path;
            else
                Application.Exit();

#if !DEBUG
            Core.DatabaseManager.DataPath = Program.Settings.DataPath;
#endif
            Core.DatabaseManager.UpdateIfOutdated( path, true );
            Core.DatabaseManager.Open();
            Core.DatabaseManager.OpenMmdb( path );

            Application.Run( new MainForm() );

            Core.DatabaseManager.Close();
            Settings.Save();
        }

        internal static Settings Settings = new Settings();

#if !DEBUG
        private static void OnThreadException( object sender, System.Threading.ThreadExceptionEventArgs e )
        {
            Twp.Controls.ExceptionDialog.Show( e.Exception );
        }

        private static void OnUnhandledException( object sender, UnhandledExceptionEventArgs e )
        {
            Twp.Controls.ExceptionDialog.Show( e.ExceptionObject as Exception );
        }
#endif
    }
}
