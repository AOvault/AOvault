﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using System.Windows.Forms;

using AOCrafter.Core;
using AOCrafter.Core.Controls;
using Twp.Data.AO;
using WeifenLuo.WinFormsUI.Docking;

namespace AOCrafter.Admin
{
    public partial class ProcessesDock : DockContent
    {
        public ProcessesDock()
        {
            InitializeComponent();
        }

        public void ShowAll()
        {
            this.OnSearch( this, new SearchEventArgs( new string[] { "ALL" } ) );
        }

        private void OnSearch( object sender, SearchEventArgs e )
        {
            this.resultList.Items.Clear();
            this.progressIndicator.Show();
            this.progressIndicator.Start();
            using( BackgroundWorker bw = new BackgroundWorker() )
            {
                bw.DoWork += new DoWorkEventHandler( DoSearch );
                bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler( OnSearchCompleted );
                bw.RunWorkerAsync( e.Words );
            }
        }

        private void DoSearch( object sender, DoWorkEventArgs e )
        {
            try
            {
                string[] args = e.Argument as string[];
                if( args == null || args.Length == 0 )
                    return;
#if DEBUG
                else if( args[0].ToUpperInvariant() == "ALL" )
                    e.Result = Process.GetAll();
#endif
                else if( args[0].ToUpperInvariant() == "UNUSED" )
                    e.Result = Process.FindUnused();
                else
                    e.Result = Process.Search( args );
            }
            catch( Exception ex )
            {
                Twp.Controls.ExceptionDialog.Show( ex );
            }
        }

        private void OnSearchCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            ProcessList processes = e.Result as ProcessList;

            if( processes == null || processes.Count == 0 )
            {
                this.countLabel.Text = "No processes found.";
            }
            else
            {
                this.resultList.BeginUpdate();
                foreach( Process process in processes )
                {
                    this.listView_Add( process );
                    Application.DoEvents();
                }
                this.countLabel.Text = "Processes found: " + processes.Count.ToString();
                this.resultList.EndUpdate();
            }
            this.progressIndicator.Stop();
            this.progressIndicator.Hide();
        }

        private void listView_Add( Process process )
        {
            //Log.Debug( "[ProcessListBox.listView_Add] Id: {0}", process.Id );
            Item item = process.Result.GetNearest( 100 );
            if( item == null )
                return;
            AOItem aoitem = item.Interpolate( Twp.Data.AO.DatabaseManager.ItemsDatabase );
            this.icons_Add( aoitem.Icon );

            ListViewItem listItem = new ListViewItem();
            listItem.SubItems.Add( process.Id.ToString() );
            listItem.SubItems.Add( aoitem.Name );
            listItem.ImageKey = aoitem.Icon.ToString();
            listItem.Tag = process;

            this.resultList.Items.Add( listItem );
        }

        private void icons_Add( int id )
        {
            //Log.Debug( "[ProcessListBox.icons_Add] Id: {0}", id );
            if( this.icons.Images.ContainsKey( id.ToString() ) == false )
            {
                AOIcon icon = AOIcon.Load( id );
                if( icon != null )
                {
                    this.icons.Images.Add( icon.Id.ToString(), icon.Image );
                }
            }
        }

        #region Drag & Drop

        private bool mouseButtonDown = false;

        private void resultList_MouseDown( object sender, MouseEventArgs e )
        {
            if( e.Button == MouseButtons.Left )
                this.mouseButtonDown = true;
        }

        private void resultList_MouseMove( object sender, MouseEventArgs e )
        {
            if( this.mouseButtonDown == false )
                return;

            if( e.Button == MouseButtons.Left )
            {
                ProcessList processes = new ProcessList();
                foreach( ListViewItem listItem in this.resultList.SelectedItems )
                {
                    processes.Add( (Process) listItem.Tag );
                }
                this.resultList.DoDragDrop( processes, DragDropEffects.Copy );
                this.mouseButtonDown = false;
            }
        }

        #endregion

        [Category( "Behavior" )]
        [Description( "Occurs when one or more items are activated in the list" )]
        public event ItemsEventHandler OpenProcesses;

        [Category( "Behavior" )]
        [Description( "Occurs when the \"Create Tradeskill\" context menu item is activated" )]
        public event DataEventHandler CreateTradeskill;

        protected virtual void OnOpenProcesses( ItemsEventArgs e )
        {
            if( this.OpenProcesses != null )
                this.OpenProcesses( this, e );
        }

        protected virtual void OnCreateTradeskill( DataEventArgs e )
        {
            if( this.CreateTradeskill != null )
                this.CreateTradeskill( this, e );
        }

        private void OnItemActivated( object sender, EventArgs e )
        {
            this.OpenProcess();
        }

        private void OnOpenProcessClicked( object sender, EventArgs e )
        {
            this.OpenProcess();
        }

        private void OpenProcess()
        {
            ProcessList processes = new ProcessList();
            foreach( ListViewItem listItem in this.resultList.SelectedItems )
            {
                processes.Add( (Process) listItem.Tag );
            }
            this.OnOpenProcesses( new ItemsEventArgs( processes.ToArray() ) );
        }

        private void OnCreateTradeskillClicked( object sender, EventArgs e )
        {
            if( this.resultList.SelectedItems.Count > 0 )
            {
                ProcessList processes = new ProcessList();
                foreach( ListViewItem listItem in this.resultList.SelectedItems )
                {
                    processes.Add( (Process) listItem.Tag );
                }
                this.OnCreateTradeskill( new DataEventArgs( new Tradeskill( processes ) ) );
            }
        }
    }
}
