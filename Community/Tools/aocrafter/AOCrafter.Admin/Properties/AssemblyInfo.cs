﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2009 - 2011
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyProduct( "AOCrafter" )]
[assembly: AssemblyVersion( "0.9.0.*" )]

[assembly: AssemblyTitle( "AOCrafter.Admin" )]
[assembly: AssemblyDescription( "Anarchy Online Tradeskill Calculator Administration Tool." )]
[assembly: AssemblyCompany( "WrongPlace.Net" )]
[assembly: AssemblyCopyright( "© Mawerick, WrongPlace.Net 2004 - 2012" )]

// [assembly: NeutralResourcesLanguage( "en", UltimateResourceFallbackLocation.MainAssembly )]
[assembly: Guid( "498ac980-4d3b-472e-bbb1-1f1331a68b61" )]

[assembly: ComVisible( false )]
