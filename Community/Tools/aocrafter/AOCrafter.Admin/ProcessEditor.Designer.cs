﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace AOCrafter.Admin
{
    partial class ProcessEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.sourceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findProcessesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createProcessToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.swapWithTargetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.swapWithResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.targetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findProcessesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.createProcessToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.swapWithSourceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.swapWithResultToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.resultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findTradeskillsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createTradeskillToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.swapWithSourceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.swapWithTargetToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.createCopyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sourceBoxMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.findSourceProcessItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createSourceProcessItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.swapSourceWithTargetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.swapSourceWithResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.consumeSourceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setSourceAsPrimaryObjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.targetBoxMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.findTargetProcessItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createTargetProcessItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.swapTargetWithSourceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.swapTargetWithResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.consumeTargetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setTargetAsPrimaryObjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resultBoxMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.findResultProcessesItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findTradeSkillsItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createTradeskillItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.swapResultWithSourceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.swapResultWithTargetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.plusLabel = new System.Windows.Forms.Label();
            this.equalsLabel = new System.Windows.Forms.Label();
            this.multiplierBox = new System.Windows.Forms.NumericUpDown();
            this.multiplierLabel = new System.Windows.Forms.Label();
            this.skillPanel = new System.Windows.Forms.Panel();
            this.skillsBox = new System.Windows.Forms.GroupBox();
            this.addSkillButton = new System.Windows.Forms.Button();
            this.resultBox = new AOCrafter.Core.Controls.ItemBox();
            this.noteBox = new System.Windows.Forms.GroupBox();
            this.editNoteButton = new System.Windows.Forms.Button();
            this.notePanel = new Twp.Controls.HtmlRenderer.HtmlPanel();
            this.sourceBox = new AOCrafter.Core.Controls.ItemBox();
            this.targetBox = new AOCrafter.Core.Controls.ItemBox();
            this.addedLabel = new System.Windows.Forms.Label();
            this.updatedLabel = new System.Windows.Forms.Label();
            this.updatedBox = new System.Windows.Forms.Label();
            this.addedBox = new System.Windows.Forms.Label();
            this.sourceBoxMenu.SuspendLayout();
            this.targetBoxMenu.SuspendLayout();
            this.resultBoxMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.multiplierBox)).BeginInit();
            this.skillsBox.SuspendLayout();
            this.noteBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(176, 6);
            // 
            // sourceToolStripMenuItem
            // 
            this.sourceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.findProcessesToolStripMenuItem,
            this.createProcessToolStripMenuItem,
            this.toolStripSeparator4,
            this.swapWithTargetToolStripMenuItem,
            this.swapWithResultToolStripMenuItem});
            this.sourceToolStripMenuItem.Name = "sourceToolStripMenuItem";
            this.sourceToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.sourceToolStripMenuItem.Text = "&Source";
            // 
            // findProcessesToolStripMenuItem
            // 
            this.findProcessesToolStripMenuItem.Name = "findProcessesToolStripMenuItem";
            this.findProcessesToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.findProcessesToolStripMenuItem.Text = "&Find Processes";
            this.findProcessesToolStripMenuItem.Click += new System.EventHandler(this.OnFindSourceClicked);
            // 
            // createProcessToolStripMenuItem
            // 
            this.createProcessToolStripMenuItem.Name = "createProcessToolStripMenuItem";
            this.createProcessToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.createProcessToolStripMenuItem.Text = "&Create Process";
            this.createProcessToolStripMenuItem.Click += new System.EventHandler(this.OnCreateSourceClicked);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(162, 6);
            // 
            // swapWithTargetToolStripMenuItem
            // 
            this.swapWithTargetToolStripMenuItem.Name = "swapWithTargetToolStripMenuItem";
            this.swapWithTargetToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.swapWithTargetToolStripMenuItem.Text = "Swap with &Target";
            this.swapWithTargetToolStripMenuItem.Click += new System.EventHandler(this.OnSwapSourceTargetClicked);
            // 
            // swapWithResultToolStripMenuItem
            // 
            this.swapWithResultToolStripMenuItem.Name = "swapWithResultToolStripMenuItem";
            this.swapWithResultToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.swapWithResultToolStripMenuItem.Text = "Swap with &Result";
            this.swapWithResultToolStripMenuItem.Click += new System.EventHandler(this.OnSwapSourceResultClicked);
            // 
            // targetToolStripMenuItem
            // 
            this.targetToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.findProcessesToolStripMenuItem1,
            this.createProcessToolStripMenuItem1,
            this.toolStripSeparator3,
            this.swapWithSourceToolStripMenuItem,
            this.swapWithResultToolStripMenuItem1});
            this.targetToolStripMenuItem.Name = "targetToolStripMenuItem";
            this.targetToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.targetToolStripMenuItem.Text = "&Target";
            // 
            // findProcessesToolStripMenuItem1
            // 
            this.findProcessesToolStripMenuItem1.Name = "findProcessesToolStripMenuItem1";
            this.findProcessesToolStripMenuItem1.Size = new System.Drawing.Size(167, 22);
            this.findProcessesToolStripMenuItem1.Text = "&Find Processes";
            this.findProcessesToolStripMenuItem1.Click += new System.EventHandler(this.OnFindTargetClicked);
            // 
            // createProcessToolStripMenuItem1
            // 
            this.createProcessToolStripMenuItem1.Name = "createProcessToolStripMenuItem1";
            this.createProcessToolStripMenuItem1.Size = new System.Drawing.Size(167, 22);
            this.createProcessToolStripMenuItem1.Text = "&Create Process";
            this.createProcessToolStripMenuItem1.Click += new System.EventHandler(this.OnCreateTargetClicked);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(164, 6);
            // 
            // swapWithSourceToolStripMenuItem
            // 
            this.swapWithSourceToolStripMenuItem.Name = "swapWithSourceToolStripMenuItem";
            this.swapWithSourceToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.swapWithSourceToolStripMenuItem.Text = "Swap with &Source";
            this.swapWithSourceToolStripMenuItem.Click += new System.EventHandler(this.OnSwapSourceTargetClicked);
            // 
            // swapWithResultToolStripMenuItem1
            // 
            this.swapWithResultToolStripMenuItem1.Name = "swapWithResultToolStripMenuItem1";
            this.swapWithResultToolStripMenuItem1.Size = new System.Drawing.Size(167, 22);
            this.swapWithResultToolStripMenuItem1.Text = "Swap with &Result";
            this.swapWithResultToolStripMenuItem1.Click += new System.EventHandler(this.OnSwapTargetResultClicked);
            // 
            // resultToolStripMenuItem
            // 
            this.resultToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.findTradeskillsToolStripMenuItem,
            this.createTradeskillToolStripMenuItem,
            this.toolStripSeparator2,
            this.swapWithSourceToolStripMenuItem1,
            this.swapWithTargetToolStripMenuItem1});
            this.resultToolStripMenuItem.Name = "resultToolStripMenuItem";
            this.resultToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.resultToolStripMenuItem.Text = "&Result";
            // 
            // findTradeskillsToolStripMenuItem
            // 
            this.findTradeskillsToolStripMenuItem.Name = "findTradeskillsToolStripMenuItem";
            this.findTradeskillsToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.findTradeskillsToolStripMenuItem.Text = "&Find Tradeskills";
            this.findTradeskillsToolStripMenuItem.Click += new System.EventHandler(this.OnFindTradeskillsClicked);
            // 
            // createTradeskillToolStripMenuItem
            // 
            this.createTradeskillToolStripMenuItem.Name = "createTradeskillToolStripMenuItem";
            this.createTradeskillToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.createTradeskillToolStripMenuItem.Text = "&Create Tradeskill";
            this.createTradeskillToolStripMenuItem.Click += new System.EventHandler(this.OnCreateTradeskillClicked);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(164, 6);
            // 
            // swapWithSourceToolStripMenuItem1
            // 
            this.swapWithSourceToolStripMenuItem1.Name = "swapWithSourceToolStripMenuItem1";
            this.swapWithSourceToolStripMenuItem1.Size = new System.Drawing.Size(167, 22);
            this.swapWithSourceToolStripMenuItem1.Text = "Swap with &Source";
            this.swapWithSourceToolStripMenuItem1.Click += new System.EventHandler(this.OnSwapSourceResultClicked);
            // 
            // swapWithTargetToolStripMenuItem1
            // 
            this.swapWithTargetToolStripMenuItem1.Name = "swapWithTargetToolStripMenuItem1";
            this.swapWithTargetToolStripMenuItem1.Size = new System.Drawing.Size(167, 22);
            this.swapWithTargetToolStripMenuItem1.Text = "Swap with &Target";
            this.swapWithTargetToolStripMenuItem1.Click += new System.EventHandler(this.OnSwapTargetResultClicked);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(176, 6);
            // 
            // createCopyToolStripMenuItem
            // 
            this.createCopyToolStripMenuItem.Name = "createCopyToolStripMenuItem";
            this.createCopyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.createCopyToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.createCopyToolStripMenuItem.Text = "&Create copy";
            this.createCopyToolStripMenuItem.Click += new System.EventHandler(this.OnCreateCopyClicked);
            // 
            // sourceBoxMenu
            // 
            this.sourceBoxMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.findSourceProcessItem,
            this.createSourceProcessItem,
            this.toolStripSeparator5,
            this.swapSourceWithTargetToolStripMenuItem,
            this.swapSourceWithResultToolStripMenuItem,
            this.toolStripSeparator9,
            this.consumeSourceToolStripMenuItem,
            this.setSourceAsPrimaryObjectToolStripMenuItem});
            this.sourceBoxMenu.Name = "sourceBoxContextMenuStrip";
            this.sourceBoxMenu.Size = new System.Drawing.Size(187, 148);
            // 
            // findSourceProcessItem
            // 
            this.findSourceProcessItem.Name = "findSourceProcessItem";
            this.findSourceProcessItem.Size = new System.Drawing.Size(186, 22);
            this.findSourceProcessItem.Text = "&Find Processes";
            this.findSourceProcessItem.Click += new System.EventHandler(this.OnFindSourceClicked);
            // 
            // createSourceProcessItem
            // 
            this.createSourceProcessItem.Name = "createSourceProcessItem";
            this.createSourceProcessItem.Size = new System.Drawing.Size(186, 22);
            this.createSourceProcessItem.Text = "&Create Process";
            this.createSourceProcessItem.Click += new System.EventHandler(this.OnCreateSourceClicked);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(183, 6);
            // 
            // swapSourceWithTargetToolStripMenuItem
            // 
            this.swapSourceWithTargetToolStripMenuItem.Name = "swapSourceWithTargetToolStripMenuItem";
            this.swapSourceWithTargetToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.swapSourceWithTargetToolStripMenuItem.Text = "Swap with &Target";
            this.swapSourceWithTargetToolStripMenuItem.Click += new System.EventHandler(this.OnSwapSourceTargetClicked);
            // 
            // swapSourceWithResultToolStripMenuItem
            // 
            this.swapSourceWithResultToolStripMenuItem.Name = "swapSourceWithResultToolStripMenuItem";
            this.swapSourceWithResultToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.swapSourceWithResultToolStripMenuItem.Text = "Swap with &Result";
            this.swapSourceWithResultToolStripMenuItem.Click += new System.EventHandler(this.OnSwapSourceResultClicked);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(183, 6);
            // 
            // consumeSourceToolStripMenuItem
            // 
            this.consumeSourceToolStripMenuItem.CheckOnClick = true;
            this.consumeSourceToolStripMenuItem.Name = "consumeSourceToolStripMenuItem";
            this.consumeSourceToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.consumeSourceToolStripMenuItem.Text = "&Is Cosumed";
            this.consumeSourceToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ConsumeSourceMenuCheckedChanged);
            // 
            // setSourceAsPrimaryObjectToolStripMenuItem
            // 
            this.setSourceAsPrimaryObjectToolStripMenuItem.Name = "setSourceAsPrimaryObjectToolStripMenuItem";
            this.setSourceAsPrimaryObjectToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.setSourceAsPrimaryObjectToolStripMenuItem.Text = "Set as &Primary Object";
            this.setSourceAsPrimaryObjectToolStripMenuItem.Click += new System.EventHandler(this.SetSourceAsPrimaryClicked);
            // 
            // targetBoxMenu
            // 
            this.targetBoxMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.findTargetProcessItem,
            this.createTargetProcessItem,
            this.toolStripSeparator6,
            this.swapTargetWithSourceToolStripMenuItem,
            this.swapTargetWithResultToolStripMenuItem,
            this.toolStripSeparator10,
            this.consumeTargetToolStripMenuItem,
            this.setTargetAsPrimaryObjectToolStripMenuItem});
            this.targetBoxMenu.Name = "sourceBoxContextMenuStrip";
            this.targetBoxMenu.Size = new System.Drawing.Size(187, 148);
            // 
            // findTargetProcessItem
            // 
            this.findTargetProcessItem.Name = "findTargetProcessItem";
            this.findTargetProcessItem.Size = new System.Drawing.Size(186, 22);
            this.findTargetProcessItem.Text = "&Find Processes";
            this.findTargetProcessItem.Click += new System.EventHandler(this.OnFindTargetClicked);
            // 
            // createTargetProcessItem
            // 
            this.createTargetProcessItem.Name = "createTargetProcessItem";
            this.createTargetProcessItem.Size = new System.Drawing.Size(186, 22);
            this.createTargetProcessItem.Text = "&Create Process";
            this.createTargetProcessItem.Click += new System.EventHandler(this.OnCreateTargetClicked);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(183, 6);
            // 
            // swapTargetWithSourceToolStripMenuItem
            // 
            this.swapTargetWithSourceToolStripMenuItem.Name = "swapTargetWithSourceToolStripMenuItem";
            this.swapTargetWithSourceToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.swapTargetWithSourceToolStripMenuItem.Text = "Swap with &Source";
            this.swapTargetWithSourceToolStripMenuItem.Click += new System.EventHandler(this.OnSwapSourceTargetClicked);
            // 
            // swapTargetWithResultToolStripMenuItem
            // 
            this.swapTargetWithResultToolStripMenuItem.Name = "swapTargetWithResultToolStripMenuItem";
            this.swapTargetWithResultToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.swapTargetWithResultToolStripMenuItem.Text = "Swap with &Result";
            this.swapTargetWithResultToolStripMenuItem.Click += new System.EventHandler(this.OnSwapTargetResultClicked);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(183, 6);
            // 
            // consumeTargetToolStripMenuItem
            // 
            this.consumeTargetToolStripMenuItem.CheckOnClick = true;
            this.consumeTargetToolStripMenuItem.Name = "consumeTargetToolStripMenuItem";
            this.consumeTargetToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.consumeTargetToolStripMenuItem.Text = "&Is Consumed";
            this.consumeTargetToolStripMenuItem.CheckedChanged += new System.EventHandler(this.ConsumeTargetMenuCheckedChanged);
            // 
            // setTargetAsPrimaryObjectToolStripMenuItem
            // 
            this.setTargetAsPrimaryObjectToolStripMenuItem.CheckOnClick = true;
            this.setTargetAsPrimaryObjectToolStripMenuItem.Name = "setTargetAsPrimaryObjectToolStripMenuItem";
            this.setTargetAsPrimaryObjectToolStripMenuItem.Size = new System.Drawing.Size(186, 22);
            this.setTargetAsPrimaryObjectToolStripMenuItem.Text = "Set as &Primary Object";
            this.setTargetAsPrimaryObjectToolStripMenuItem.Click += new System.EventHandler(this.SetTargetAsPrimaryClicked);
            // 
            // resultBoxMenu
            // 
            this.resultBoxMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.findResultProcessesItem,
            this.findTradeSkillsItem,
            this.createTradeskillItem,
            this.toolStripSeparator7,
            this.swapResultWithSourceToolStripMenuItem,
            this.swapResultWithTargetToolStripMenuItem});
            this.resultBoxMenu.Name = "resultBoxMenu";
            this.resultBoxMenu.Size = new System.Drawing.Size(191, 120);
            // 
            // findResultProcessesItem
            // 
            this.findResultProcessesItem.Name = "findResultProcessesItem";
            this.findResultProcessesItem.Size = new System.Drawing.Size(190, 22);
            this.findResultProcessesItem.Text = "Find Similar &Processes";
            this.findResultProcessesItem.Click += new System.EventHandler(this.OnFindResultClicked);
            // 
            // findTradeSkillsItem
            // 
            this.findTradeSkillsItem.Name = "findTradeSkillsItem";
            this.findTradeSkillsItem.Size = new System.Drawing.Size(190, 22);
            this.findTradeSkillsItem.Text = "&Find Tradeskills";
            this.findTradeSkillsItem.Click += new System.EventHandler(this.OnFindTradeskillsClicked);
            // 
            // createTradeskillItem
            // 
            this.createTradeskillItem.Name = "createTradeskillItem";
            this.createTradeskillItem.Size = new System.Drawing.Size(190, 22);
            this.createTradeskillItem.Text = "&Create Tradeskill";
            this.createTradeskillItem.Click += new System.EventHandler(this.OnCreateTradeskillClicked);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(187, 6);
            // 
            // swapResultWithSourceToolStripMenuItem
            // 
            this.swapResultWithSourceToolStripMenuItem.Name = "swapResultWithSourceToolStripMenuItem";
            this.swapResultWithSourceToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.swapResultWithSourceToolStripMenuItem.Text = "Swap with &Source";
            this.swapResultWithSourceToolStripMenuItem.Click += new System.EventHandler(this.OnSwapSourceResultClicked);
            // 
            // swapResultWithTargetToolStripMenuItem
            // 
            this.swapResultWithTargetToolStripMenuItem.Name = "swapResultWithTargetToolStripMenuItem";
            this.swapResultWithTargetToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.swapResultWithTargetToolStripMenuItem.Text = "Swap with &Target";
            this.swapResultWithTargetToolStripMenuItem.Click += new System.EventHandler(this.OnSwapTargetResultClicked);
            // 
            // plusLabel
            // 
            this.plusLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plusLabel.Location = new System.Drawing.Point(142, 12);
            this.plusLabel.Name = "plusLabel";
            this.plusLabel.Size = new System.Drawing.Size(10, 54);
            this.plusLabel.TabIndex = 3;
            this.plusLabel.Text = "+";
            this.plusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // equalsLabel
            // 
            this.equalsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equalsLabel.Location = new System.Drawing.Point(288, 12);
            this.equalsLabel.Name = "equalsLabel";
            this.equalsLabel.Size = new System.Drawing.Size(10, 54);
            this.equalsLabel.TabIndex = 2;
            this.equalsLabel.Text = "=";
            this.equalsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // multiplierBox
            // 
            this.multiplierBox.DecimalPlaces = 2;
            this.multiplierBox.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.multiplierBox.Location = new System.Drawing.Point(66, 260);
            this.multiplierBox.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.multiplierBox.Name = "multiplierBox";
            this.multiplierBox.Size = new System.Drawing.Size(73, 20);
            this.multiplierBox.TabIndex = 5;
            this.multiplierBox.Value = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.multiplierBox.ValueChanged += new System.EventHandler(this.MultiplierBoxValueChanged);
            // 
            // multiplierLabel
            // 
            this.multiplierLabel.AutoSize = true;
            this.multiplierLabel.Location = new System.Drawing.Point(9, 263);
            this.multiplierLabel.Name = "multiplierLabel";
            this.multiplierLabel.Size = new System.Drawing.Size(51, 13);
            this.multiplierLabel.TabIndex = 4;
            this.multiplierLabel.Text = "&Multiplier:";
            // 
            // skillPanel
            // 
            this.skillPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.skillPanel.AutoScroll = true;
            this.skillPanel.BackColor = System.Drawing.SystemColors.Window;
            this.skillPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.skillPanel.Location = new System.Drawing.Point(6, 19);
            this.skillPanel.Name = "skillPanel";
            this.skillPanel.Size = new System.Drawing.Size(329, 244);
            this.skillPanel.TabIndex = 1;
            // 
            // skillsBox
            // 
            this.skillsBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.skillsBox.Controls.Add(this.addSkillButton);
            this.skillsBox.Controls.Add(this.skillPanel);
            this.skillsBox.Location = new System.Drawing.Point(437, 6);
            this.skillsBox.Name = "skillsBox";
            this.skillsBox.Size = new System.Drawing.Size(341, 269);
            this.skillsBox.TabIndex = 10;
            this.skillsBox.TabStop = false;
            this.skillsBox.Text = "Skills";
            // 
            // addSkillButton
            // 
            this.addSkillButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.addSkillButton.Location = new System.Drawing.Point(284, 0);
            this.addSkillButton.Name = "addSkillButton";
            this.addSkillButton.Size = new System.Drawing.Size(50, 19);
            this.addSkillButton.TabIndex = 0;
            this.addSkillButton.Text = "&Add";
            this.addSkillButton.UseCompatibleTextRendering = true;
            this.addSkillButton.UseVisualStyleBackColor = true;
            this.addSkillButton.Click += new System.EventHandler(this.OnAddSkillClicked);
            // 
            // resultBox
            // 
            this.resultBox.AllowDrop = true;
            this.resultBox.ContextMenuStrip = this.resultBoxMenu;
            this.resultBox.Editable = true;
            this.resultBox.Location = new System.Drawing.Point(301, 9);
            this.resultBox.Margin = new System.Windows.Forms.Padding(0);
            this.resultBox.MinimumSize = new System.Drawing.Size(54, 54);
            this.resultBox.Name = "resultBox";
            this.resultBox.ShowButtons = false;
            this.resultBox.Size = new System.Drawing.Size(130, 243);
            this.resultBox.TabIndex = 3;
            this.resultBox.Text = "Type:";
            this.resultBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.OnResultBoxDropped);
            // 
            // noteBox
            // 
            this.noteBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.noteBox.Controls.Add(this.editNoteButton);
            this.noteBox.Controls.Add(this.notePanel);
            this.noteBox.Location = new System.Drawing.Point(6, 281);
            this.noteBox.Name = "noteBox";
            this.noteBox.Size = new System.Drawing.Size(772, 175);
            this.noteBox.TabIndex = 11;
            this.noteBox.TabStop = false;
            this.noteBox.Text = "Note";
            // 
            // editNoteButton
            // 
            this.editNoteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.editNoteButton.Location = new System.Drawing.Point(715, 0);
            this.editNoteButton.Name = "editNoteButton";
            this.editNoteButton.Size = new System.Drawing.Size(50, 19);
            this.editNoteButton.TabIndex = 0;
            this.editNoteButton.Text = "&Edit";
            this.editNoteButton.UseCompatibleTextRendering = true;
            this.editNoteButton.UseVisualStyleBackColor = true;
            this.editNoteButton.Click += new System.EventHandler(this.OnEditNoteClicked);
            // 
            // notePanel
            // 
            this.notePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.notePanel.AutoScroll = true;
            this.notePanel.AvoidGeometryAntialias = false;
            this.notePanel.AvoidImagesLateLoading = false;
            this.notePanel.BackColor = System.Drawing.SystemColors.Info;
            this.notePanel.BaseStylesheet = null;
            this.notePanel.ForeColor = System.Drawing.SystemColors.InfoText;
            this.notePanel.Location = new System.Drawing.Point(6, 19);
            this.notePanel.Name = "notePanel";
            this.notePanel.Padding = new System.Windows.Forms.Padding(3);
            this.notePanel.Size = new System.Drawing.Size(760, 150);
            this.notePanel.TabIndex = 1;
            // 
            // sourceBox
            // 
            this.sourceBox.AllowDrop = true;
            this.sourceBox.ContextMenuStrip = this.sourceBoxMenu;
            this.sourceBox.Editable = true;
            this.sourceBox.Location = new System.Drawing.Point(9, 9);
            this.sourceBox.Margin = new System.Windows.Forms.Padding(0);
            this.sourceBox.MinimumSize = new System.Drawing.Size(54, 54);
            this.sourceBox.Name = "sourceBox";
            this.sourceBox.Size = new System.Drawing.Size(130, 243);
            this.sourceBox.TabIndex = 2;
            this.sourceBox.Text = "Type:";
            this.sourceBox.ConsumedChanged += new System.EventHandler(this.ConsumeSourceChanged);
            this.sourceBox.PrimaryChanged += new System.EventHandler(this.SourceIsPrimaryChanged);
            this.sourceBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.OnSourceBoxDropped);
            // 
            // targetBox
            // 
            this.targetBox.AllowDrop = true;
            this.targetBox.ContextMenuStrip = this.targetBoxMenu;
            this.targetBox.Editable = true;
            this.targetBox.Location = new System.Drawing.Point(155, 9);
            this.targetBox.Margin = new System.Windows.Forms.Padding(0);
            this.targetBox.MinimumSize = new System.Drawing.Size(54, 54);
            this.targetBox.Name = "targetBox";
            this.targetBox.Size = new System.Drawing.Size(130, 243);
            this.targetBox.TabIndex = 4;
            this.targetBox.Text = "Type:";
            this.targetBox.ConsumedChanged += new System.EventHandler(this.ConsumeTargetChanged);
            this.targetBox.PrimaryChanged += new System.EventHandler(this.TargetIsPrimaryChanged);
            this.targetBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.OnTargetBoxDropped);
            // 
            // addedLabel
            // 
            this.addedLabel.AutoSize = true;
            this.addedLabel.Location = new System.Drawing.Point(155, 263);
            this.addedLabel.Name = "addedLabel";
            this.addedLabel.Size = new System.Drawing.Size(41, 13);
            this.addedLabel.TabIndex = 6;
            this.addedLabel.Text = "Added:";
            // 
            // updatedLabel
            // 
            this.updatedLabel.AutoSize = true;
            this.updatedLabel.Location = new System.Drawing.Point(301, 263);
            this.updatedLabel.Name = "updatedLabel";
            this.updatedLabel.Size = new System.Drawing.Size(51, 13);
            this.updatedLabel.TabIndex = 8;
            this.updatedLabel.Text = "Updated:";
            // 
            // updatedBox
            // 
            this.updatedBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.updatedBox.Location = new System.Drawing.Point(361, 260);
            this.updatedBox.Name = "updatedBox";
            this.updatedBox.Size = new System.Drawing.Size(70, 20);
            this.updatedBox.TabIndex = 9;
            this.updatedBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // addedBox
            // 
            this.addedBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.addedBox.Location = new System.Drawing.Point(215, 260);
            this.addedBox.Name = "addedBox";
            this.addedBox.Size = new System.Drawing.Size(70, 20);
            this.addedBox.TabIndex = 7;
            this.addedBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ProcessEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 462);
            this.ControlBox = false;
            this.Controls.Add(this.addedBox);
            this.Controls.Add(this.updatedBox);
            this.Controls.Add(this.updatedLabel);
            this.Controls.Add(this.addedLabel);
            this.Controls.Add(this.sourceBox);
            this.Controls.Add(this.plusLabel);
            this.Controls.Add(this.targetBox);
            this.Controls.Add(this.equalsLabel);
            this.Controls.Add(this.resultBox);
            this.Controls.Add(this.skillsBox);
            this.Controls.Add(this.noteBox);
            this.Controls.Add(this.multiplierLabel);
            this.Controls.Add(this.multiplierBox);
            this.DoubleBuffered = true;
            this.Name = "ProcessEditor";
            this.Text = "Process Editor";
            this.Controls.SetChildIndex(this.multiplierBox, 0);
            this.Controls.SetChildIndex(this.multiplierLabel, 0);
            this.Controls.SetChildIndex(this.noteBox, 0);
            this.Controls.SetChildIndex(this.skillsBox, 0);
            this.Controls.SetChildIndex(this.resultBox, 0);
            this.Controls.SetChildIndex(this.equalsLabel, 0);
            this.Controls.SetChildIndex(this.targetBox, 0);
            this.Controls.SetChildIndex(this.plusLabel, 0);
            this.Controls.SetChildIndex(this.sourceBox, 0);
            this.Controls.SetChildIndex(this.addedLabel, 0);
            this.Controls.SetChildIndex(this.updatedLabel, 0);
            this.Controls.SetChildIndex(this.updatedBox, 0);
            this.Controls.SetChildIndex(this.addedBox, 0);
            this.sourceBoxMenu.ResumeLayout(false);
            this.targetBoxMenu.ResumeLayout(false);
            this.resultBoxMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.multiplierBox)).EndInit();
            this.skillsBox.ResumeLayout(false);
            this.noteBox.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        private System.Windows.Forms.Label updatedBox;
        private System.Windows.Forms.Label addedBox;
        private System.Windows.Forms.Label updatedLabel;
        private System.Windows.Forms.Label addedLabel;

        private AOCrafter.Core.Controls.ItemBox sourceBox;
        private System.Windows.Forms.Label plusLabel;
        private AOCrafter.Core.Controls.ItemBox targetBox;
        private System.Windows.Forms.Label equalsLabel;
        private AOCrafter.Core.Controls.ItemBox resultBox;
        private System.Windows.Forms.ContextMenuStrip sourceBoxMenu;
        private System.Windows.Forms.ToolStripMenuItem findSourceProcessItem;
        private System.Windows.Forms.ToolStripMenuItem createSourceProcessItem;
        private System.Windows.Forms.ContextMenuStrip targetBoxMenu;
        private System.Windows.Forms.ToolStripMenuItem findTargetProcessItem;
        private System.Windows.Forms.ToolStripMenuItem createTargetProcessItem;
        private System.Windows.Forms.ContextMenuStrip resultBoxMenu;
        private System.Windows.Forms.ToolStripMenuItem findTradeSkillsItem;
        private System.Windows.Forms.ToolStripMenuItem createTradeskillItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem sourceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem findProcessesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createProcessToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem targetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem findProcessesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem createProcessToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem resultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem findTradeskillsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createTradeskillToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem swapWithTargetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem swapWithResultToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem swapWithSourceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem swapWithResultToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem swapWithSourceToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem swapWithTargetToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem swapSourceWithTargetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem swapSourceWithResultToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem swapTargetWithSourceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem swapTargetWithResultToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem swapResultWithSourceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem swapResultWithTargetToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem createCopyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem findResultProcessesItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripMenuItem setSourceAsPrimaryObjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consumeSourceToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem setTargetAsPrimaryObjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consumeTargetToolStripMenuItem;
        private System.Windows.Forms.NumericUpDown multiplierBox;
        private System.Windows.Forms.Label multiplierLabel;
        private System.Windows.Forms.Panel skillPanel;
        private System.Windows.Forms.GroupBox skillsBox;
        private System.Windows.Forms.Button addSkillButton;
        private System.Windows.Forms.GroupBox noteBox;
        private Twp.Controls.HtmlRenderer.HtmlPanel notePanel;
        private System.Windows.Forms.Button editNoteButton;

        #endregion
    }
}