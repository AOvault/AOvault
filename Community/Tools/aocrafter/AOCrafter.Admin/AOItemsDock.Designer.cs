﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
namespace AOCrafter.Admin
{
    partial class AOItemsDock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.searchBox = new AOCrafter.Core.Controls.SearchBox();
            this.resultList = new Twp.Controls.FillListView();
            this.iconHeader1 = new System.Windows.Forms.ColumnHeader();
            this.idHeader1 = new System.Windows.Forms.ColumnHeader();
            this.qlHeader1 = new System.Windows.Forms.ColumnHeader();
            this.nameHeader1 = new System.Windows.Forms.ColumnHeader();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.createItemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.icons = new System.Windows.Forms.ImageList(this.components);
            this.countLabel = new System.Windows.Forms.Label();
            this.progressIndicator = new Twp.Controls.ProgressIndicator();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // searchBox
            // 
            this.searchBox.Advanced = false;
            this.searchBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.searchBox.Location = new System.Drawing.Point(0, 0);
            this.searchBox.Name = "searchBox";
            this.searchBox.Size = new System.Drawing.Size(284, 37);
            this.searchBox.TabIndex = 0;
            this.searchBox.Search += new AOCrafter.Core.Controls.SearchEventHandler(this.OnSearch);
            // 
            // resultList
            // 
            this.resultList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.iconHeader1,
            this.idHeader1,
            this.qlHeader1,
            this.nameHeader1});
            this.resultList.ContextMenuStrip = this.contextMenuStrip;
            this.resultList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultList.FullRowSelect = true;
            this.resultList.LargeImageList = this.icons;
            this.resultList.Location = new System.Drawing.Point(0, 37);
            this.resultList.Name = "resultList";
            this.resultList.Size = new System.Drawing.Size(284, 209);
            this.resultList.SmallImageList = this.icons;
            this.resultList.StretchIndex = 3;
            this.resultList.TabIndex = 1;
            this.resultList.UseCompatibleStateImageBehavior = false;
            this.resultList.View = System.Windows.Forms.View.Details;
            this.resultList.MouseDown += new System.Windows.Forms.MouseEventHandler(this.resultList_MouseDown);
            this.resultList.MouseMove += new System.Windows.Forms.MouseEventHandler(this.resultList_MouseMove);
            // 
            // iconHeader1
            // 
            this.iconHeader1.Text = "";
            this.iconHeader1.Width = 25;
            // 
            // idHeader1
            // 
            this.idHeader1.Text = "ID";
            this.idHeader1.Width = 50;
            // 
            // qlHeader1
            // 
            this.qlHeader1.Text = "QL";
            this.qlHeader1.Width = 30;
            // 
            // nameHeader1
            // 
            this.nameHeader1.Text = "Name";
            this.nameHeader1.Width = 175;
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createItemToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip1";
            this.contextMenuStrip.Size = new System.Drawing.Size(136, 26);
            // 
            // createItemToolStripMenuItem
            // 
            this.createItemToolStripMenuItem.Name = "createItemToolStripMenuItem";
            this.createItemToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.createItemToolStripMenuItem.Text = "&Create Item";
            this.createItemToolStripMenuItem.Click += new System.EventHandler(this.OnCreateItemClicked);
            // 
            // icons
            // 
            this.icons.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.icons.ImageSize = new System.Drawing.Size(16, 16);
            this.icons.TransparentColor = System.Drawing.Color.Lime;
            // 
            // countLabel
            // 
            this.countLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.countLabel.Location = new System.Drawing.Point(0, 246);
            this.countLabel.Name = "countLabel";
            this.countLabel.Size = new System.Drawing.Size(284, 16);
            this.countLabel.TabIndex = 3;
            this.countLabel.Text = "Search for something...";
            this.countLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // progressIndicator
            // 
            this.progressIndicator.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.progressIndicator.BackColor = System.Drawing.SystemColors.Window;
            this.progressIndicator.CircleColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.progressIndicator.Circles = ((uint)(12u));
            this.progressIndicator.CircleSize = 0.75F;
            this.progressIndicator.Location = new System.Drawing.Point(118, 117);
            this.progressIndicator.Name = "progressIndicator";
            this.progressIndicator.Size = new System.Drawing.Size(48, 48);
            this.progressIndicator.TabIndex = 2;
            this.progressIndicator.Visible = false;
            this.progressIndicator.VisibleCircles = ((uint)(11u));
            // 
            // AOItemsDock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.progressIndicator);
            this.Controls.Add(this.resultList);
            this.Controls.Add(this.searchBox);
            this.Controls.Add(this.countLabel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HideOnClose = true;
            this.Name = "AOItemsDock";
            this.Text = "AOItems";
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        private Twp.Controls.ProgressIndicator progressIndicator;
        private System.Windows.Forms.ToolStripMenuItem createItemToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;

        #endregion

        private AOCrafter.Core.Controls.SearchBox searchBox;
        private Twp.Controls.FillListView resultList;
        private System.Windows.Forms.ColumnHeader iconHeader1;
        private System.Windows.Forms.ColumnHeader idHeader1;
        private System.Windows.Forms.ColumnHeader nameHeader1;
        private System.Windows.Forms.ColumnHeader qlHeader1;
        private System.Windows.Forms.Label countLabel;
        private System.Windows.Forms.ImageList icons;
    }
}