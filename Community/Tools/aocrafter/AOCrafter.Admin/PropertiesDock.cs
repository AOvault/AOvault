﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.ComponentModel;
using AOCrafter.Core;
using WeifenLuo.WinFormsUI.Docking;

namespace AOCrafter.Admin
{
    public partial class PropertiesBox : DockContent
    {
        public PropertiesBox()
        {
            InitializeComponent();

            this.items = new BindingList<object>();
            this.items.ListChanged += new ListChangedEventHandler( OnItemsChanged );
        }

        private BindingList<object> items;
        public BindingList<object> Items
        {
            get { return this.items; }
        }

        public object SelectedItem
        {
            get { return this.propertyGrid.SelectedObject; }
            set
            {
                this.propertyGrid.SelectedObject = value;
            }
        }

        void OnItemsChanged( object sender, ListChangedEventArgs e )
        {
            switch( e.ListChangedType )
            {
                case ListChangedType.ItemAdded:
                    this.AddNode( this.items[e.NewIndex] );
                    break;
                case ListChangedType.ItemChanged:
                    break;
                case ListChangedType.ItemDeleted:
                    break;
                case ListChangedType.ItemMoved:
                    break;
                case ListChangedType.PropertyDescriptorAdded:
                    break;
                case ListChangedType.PropertyDescriptorChanged:
                    break;
                case ListChangedType.PropertyDescriptorDeleted:
                    break;
                case ListChangedType.Reset:
                    break;
                default:
                    break;
            }
        }

        void AddNode( object item )
        {
            if( item is Tradeskill )
            {

            }
            else if( item is Process )
            {
            }
            else if( item is ProcItem )
            {
            }
            else
            {

            }
        }
    }
}
