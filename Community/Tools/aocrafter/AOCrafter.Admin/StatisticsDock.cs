﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

using AOCrafter.Core;
using WeifenLuo.WinFormsUI.Docking;

namespace AOCrafter.Admin
{
    public partial class StatisticsDock : DockContent
    {
        public StatisticsDock()
        {
            InitializeComponent();
        }
        
        protected override void OnShown( EventArgs e )
        {
            base.OnShown( e );
            this.RefreshSettings();
        }

        public void RefreshSettings()
        {
            this.SetInterval( Program.Settings.StatsRefreshInterval );
            this.ToggleUpdate( Program.Settings.StatsAutoRefresh );
        }

        private void SetInterval( int interval )
        {
            if( this.DesignMode )
                return;
            if( interval < this.intervalBox.Minimum || interval > this.intervalBox.Maximum )
                return;
            this.intervalBox.Value = interval;
            this.timer.Interval = interval * 1000;
            Program.Settings.StatsRefreshInterval = interval;
        }

        private void ToggleUpdate( bool enabled )
        {
            if( this.DesignMode )
                return;
            this.autoRefreshButton.Checked = enabled;
            this.updateButton.Visible = !enabled;
            this.intervalLabel.Visible = enabled;
            this.intervalBox.Visible = enabled;
            this.timer.Enabled = enabled;
            Program.Settings.StatsAutoRefresh = enabled;
            if( enabled && !this.backgroundWorker.IsBusy )
                this.backgroundWorker.RunWorkerAsync();
        }
        
        private void OnUpdateButtonClicked(object sender, EventArgs e)
        {
            this.StartUpdate();
        }

        private void OnIntervalChanged( object sender, EventArgs e )
        {
            this.SetInterval( Decimal.ToInt32( this.intervalBox.Value ) );
        }

        private void OnAutoRefreshToggled( object sender, EventArgs e )
        {
            this.ToggleUpdate( this.autoRefreshButton.Checked );
        }

        private void OnTick( object sender, EventArgs e )
        {
            this.StartUpdate();
        }

        private void StartUpdate()
        {
            if( !this.backgroundWorker.IsBusy )
            {
                this.progressIndicator.Show();
                this.progressIndicator.Start();
                this.backgroundWorker.RunWorkerAsync();
            }
        }

        private void DoUpdate( object sender, DoWorkEventArgs e )
        {
            e.Result = DatabaseManager.Statistics();
        }

        private void OnUpdateCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            this.statsBox.BeginUpdate();
            this.statsBox.Items.Clear();
            List<DatabaseStatistics> stats = (List<DatabaseStatistics>) e.Result;
            if( stats != null )
            {
                foreach( DatabaseStatistics stat in stats )
                {
                    ListViewItem item = this.statsBox.Items.Add( stat.Text );
                    item.SubItems.Add( stat.Count.ToString() );
                }
            }
            this.statsBox.EndUpdate();
            this.progressIndicator.Stop();
            this.progressIndicator.Hide();
        }
    }
}
