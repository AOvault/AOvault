﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

using AOCrafter.Core;
using AOCrafter.Core.Controls;
using Twp.Data.AO;
using WeifenLuo.WinFormsUI.Docking;

namespace AOCrafter.Admin
{
    public partial class AOItemsDock : DockContent
    {
        public AOItemsDock()
        {
            InitializeComponent();
        }

        private void OnSearch( object sender, SearchEventArgs e )
        {
            this.resultList.Items.Clear();
            this.progressIndicator.Show();
            this.progressIndicator.Start();
            using( BackgroundWorker bw = new BackgroundWorker() )
            {
                bw.DoWork += new DoWorkEventHandler( this.DoSearch );
                bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler( this.OnSearchCompleted );
                bw.RunWorkerAsync( e.Words );
            }
        }

        private void DoSearch( object sender, DoWorkEventArgs e )
        {
            string[] args = e.Argument as string[];
            if( args == null || args.Length == 0 )
                return;
            List<AOItem> items = AOItem.Search( args );
            if( items != null )
                e.Result = new AOItemList( items );
        }

        private void OnSearchCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            AOItemList items = e.Result as AOItemList;

            if( items == null || items.Count == 0 )
            {
                this.countLabel.Text = "No items found.";
            }
            else
            {
                this.resultList.BeginUpdate();
                foreach( AOItem item in items )
                {
                    this.AddItem( item );
                    Application.DoEvents();
                }
                this.countLabel.Text = "Items found: " + items.Count.ToString();
                this.resultList.EndUpdate();
            }
            this.progressIndicator.Stop();
            this.progressIndicator.Hide();
        }

        private void AddItem( AOItem item )
        {
            //Log.Debug( "[ItemSearchBox.listView_Add] Id: {0}", item.Id );

            // Add icon to imagelist
            if( this.icons.Images.ContainsKey( item.Icon.ToString() ) == false )
            {
                AOIcon icon = AOIcon.Load( item.Icon );
                if( icon != null )
                {
                    this.icons.Images.Add( icon.Id.ToString(), icon.Image );
                }
            }

            ListViewItem listItem = new ListViewItem();
            listItem.SubItems.Add( item.Id.ToString() );
            listItem.SubItems.Add( item.Ql.ToString() );
            listItem.SubItems.Add( item.Name );
            listItem.ImageKey = item.Icon.ToString();
            listItem.Tag = item;

            this.resultList.Items.Add( listItem );
        }

        #region Drag & Drop

        private bool mouseButtonDown = false;

        private void resultList_MouseDown( object sender, MouseEventArgs e )
        {
            if( e.Button == MouseButtons.Left )
                this.mouseButtonDown = true;
        }

        private void resultList_MouseMove( object sender, MouseEventArgs e )
        {
            if( this.mouseButtonDown == false )
                return;

            if( e.Button == MouseButtons.Left )
            {
                AOItemList items = new AOItemList();
                foreach( ListViewItem listItem in this.resultList.SelectedItems )
                {
                    items.Add( (AOItem) listItem.Tag );
                }
                this.resultList.DoDragDrop( items, DragDropEffects.Copy );
                this.mouseButtonDown = false;
            }
        }

        #endregion

        [Category( "Behavior" )]
        [Description( "Occurs when the \"Create Item\" context menu item is activated, after an Item search" )]
        public event DataEventHandler CreateItem;

        protected virtual void OnCreateItem( DataEventArgs e )
        {
            if( this.CreateItem != null )
                this.CreateItem( this, e );
        }

        private void OnCreateItemClicked( object sender, EventArgs e )
        {
            ProcItem item = new ProcItem();
            foreach( ListViewItem listItem in this.resultList.SelectedItems )
            {
                item.Items.Add( (AOItem) listItem.Tag );
            }
            item.Items.Sort();
            this.OnCreateItem( new DataEventArgs( item ) );
        }
    }
}
