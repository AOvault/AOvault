﻿//
//  Copyright (C) Mawerick, WrongPlace.Net 2014
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//
using System;
using System.Windows.Forms;
using AOCrafter.Core;
using Twp.Utilities;
using Twp.Utilities.Undo;
using WeifenLuo.WinFormsUI.Docking;

namespace AOCrafter.Admin
{
    /// <summary>
    /// Description of DataEditor.
    /// </summary>
    public partial class DataEditor : DockContent
    {
        #region Constructor
        
        public DataEditor()
        {
            InitializeComponent();
        }

        #endregion

        #region Private Fields

        private bool updating = false;
        private ICrafterData data;

        #endregion

        #region Properties

        public bool IsUpdating
        {
            get { return this.updating; }
        }

        public bool Modified
        {
            get { return !this.actionManager.IsSaved; }
        }

        public ICrafterData Data
        {
            get { return this.data; }
            set
            {
                this.data = value;
                this.OnDataChanged();
            }
        }

        public string DataName
        {
            get
            {
                if( this.data != null )
                {
                    string mod = this.Modified ? "*" : "";
                    string type = this.data.GetType().Name;
                    if( this.data.Id > 0 )
                        return type + " #" + this.data.Id.ToString() + mod;
                    else
                        return "New " + type + mod;
                }
                else
                    return "Unknown Data";
            }
        }

        /// <summary>
        /// Derived editors should override this to provide a proper help text.
        /// </summary>
        public virtual string HelpText
        {
            get { return String.Empty; }
        }

        public virtual string DefaultSearchType
        {
            get { return String.Empty; }
        }

        protected ActionManager ActionManager
        {
            get { return this.actionManager; }
        }

        #endregion

        #region Events

        public event DataEventHandler DataChanged;
        public event DataEventHandler DataModified;
        public event DataEventHandler DataSaved;

        public event DataEventHandler CreateEditor;
        public event DataEventHandler OpenEditor;
        public event ItemsEventHandler OpenEditors;

        #endregion

        #region Methods

        protected virtual void OnDataChanged()
        {
            this.actionManager.Clear();
            this.Text = this.DataName;
            if( this.DataChanged != null )
                this.DataChanged( this, new DataEventArgs( this.data ) );
        }

        protected virtual void OnDataSaved()
        {
            this.actionManager.Save();
            this.Text = this.DataName;
            if( this.DataSaved != null )
                this.DataSaved( this, new DataEventArgs( this.data ) );
        }

        protected virtual void OnCreateEditor( DataEventArgs e )
        {
            if( this.CreateEditor != null )
                this.CreateEditor( this, e );
        }

        protected virtual void OnOpenEditor( DataEventArgs e )
        {
            if( this.OpenEditor != null )
                this.OpenEditor( this, e );
        }

        protected virtual void OnOpenEditors( ItemsEventArgs e )
        {
            if( this.OpenEditors != null )
                this.OpenEditors( this, e );
        }

        public void BeginUpdate()
        {
            this.updating = true;
            this.SuspendLayout();
        }

        public void EndUpdate()
        {
            this.updating = false;
            this.ResumeLayout( false );
            this.PerformLayout();
        }

        private void OnCloseClicked( object sender, EventArgs e )
        {
            this.Close();
        }

        private void OnFormClosed(object sender, FormClosedEventArgs e)
        {
            this.data.Dispose();
        }

        private void OnFormClosing(object sender, FormClosingEventArgs e)
        {
            if( this.Data != null && this.Modified == true )
            {
                DialogResult dr = MessageBox.Show( String.Format( "{0} has unsaved changes. Save now?", this.DataName ),
                    "Save before closing?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question );
                if( dr == DialogResult.Cancel )
                    e.Cancel = true;
                else if( dr == DialogResult.Yes )
                    this.Data.Save();
            }
        }

        private void OnSaveClicked( object sender, EventArgs e )
        {
            if( this.data != null && (this.Modified || this.data.Id <= 0) )
            {
                this.data.Save();
                ((MainForm)(this.MdiParent)).StatusText = String.Format( "{0} saved.", this.DataName );
                this.Text = this.DataName;
                this.actionManager.Save();
                this.saveToolStripMenuItem.Enabled = false;
                this.saveToolStripButton.Enabled = false;
                this.OnDataSaved();
            }
        }

        private void OnDeleteClicked( object sender, EventArgs e )
        {
            if( this.data != null )
            {
                DialogResult dr = MessageBox.Show( String.Format( "Really delete {0}? This will permanently delete it and cannot be undone.", this.DataName ),
                    "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation );
                if( dr == DialogResult.Yes )
                {
                    this.data.Delete();
                    ((MainForm)(this.MdiParent)).StatusText = String.Format( "{0} deleted.", this.DataName );
                    this.actionManager.Clear();
                    this.Close();
                }
            }
        }

        private void OnUndoClicked( object sender, EventArgs e )
        {
            this.actionManager.Undo();
        }

        private void OnRedoClicked( object sender, EventArgs e )
        {
            this.actionManager.Redo();
        }

        private void OnHistoryChanged( object sender, EventArgs e )
        {
            this.undoToolStripMenuItem.Enabled = this.actionManager.CanUndo;
            this.undoToolStripButton.Enabled = this.actionManager.CanUndo;
            this.redoToolStripMenuItem.Enabled = this.actionManager.CanRedo;
            this.redoToolStripButton.Enabled = this.actionManager.CanRedo;
            this.saveToolStripMenuItem.Enabled = this.Modified || this.data.Id <= 0;
            this.saveToolStripButton.Enabled = this.Modified || this.data.Id <= 0;

            Log.Debug( "[DataEditor.OnHistoryChanged] -------- BEGIN --------" );
            foreach( IAction action in this.actionManager.EnumUndoableActions )
            {
                Log.Debug( "[DataEditor.OnHistoryChanged] Action: {0}", action );
            }
            Log.Debug( "[DataEditor.OnHistoryChanged] --------- END ---------" );

            this.Text = this.DataName;
            if( this.DataModified != null )
                this.DataModified( this, new DataEventArgs( this.data ) );
        }

        #endregion
    }
}
