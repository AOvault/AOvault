AOB Resource Basher
---------------------------------------------------------------------
(c) 2001 by Ghandi (ghandi@mystics.de) & Chaos (chaos@ao-basher.com)
---------------------------------------------------------------------

Actual Release : 0.95
Release Date : 18.02.2001
Software : Freeware
Home : http://www.ao-basher.com | http://ao.mystics.de
Board : x

---------------------------------------------------------------------

--- License Agreement

This program has been released as freeware under the following
conditions:

1. It is not to be distributed via the internet or via any other
media without the prior approval of the author. In particular, it is
not to be made available from internet sites which promote the illegal
modification of software.

2. It is not used in such a way as to modify the copyright notice of 
this or any other computer program, to in any way disguise the
registered user or owner of any software, or to in any way illegally
modify software.

3. No guarantee of performance is given. Any damage to software 
resulting from using this software will be the responsibility of
the user.

---------------------------------------------------------------------

-- AIM

Aim of this program is to facilies the extraction of the PNG coded
images inside the Anarchy Online resource files (resourcedatabase.dat,
guigfx.uvga etc). But it can also be used to extract any PNG files
inside any packages (as long as they are not compressed)

-- BUGS

If you encounter any bug please use one of the above EMail adresses
and submit us an detailed description of your bug.

-- FEATURES

If you miss any feature please send us an EMail with your idea.

---------------------------------------------------------------------

--- HISTORY
05. Jun. 2001	Going Public
		Aim is to find and fix the last bugs so that we can release a final version

12. Apr. 2001	Still no real answer from Funcom

20. Feb. 2001 	Waiting for the final "OK" from Funcom

18. Feb. 2001	Included some "save options" regarding the names used to save the found files
		Still not finished.

16. Feb. 2001	Optimized scan and save code! full Resourcedatabase.dat under 4 mins! ;)

14, Feb. 2001 	Renamed Project from AOB Resource Eater into AOB Resource Basher
		Ghandi & Chaos are now working together on this application.

13, Feb. 2001	First Release
		After extensiv chats between Ghandi and Chaos a
		PHP verions of the PNG-Extractor was released and
		later on the day a C++ and Delphi version.
		
		The C++ Version is extrem fast, the Delphi versions
		is easier to handle, merging the both projects will 
		result in a fast and user friendly tool to extract 
		the PNG Files out of the resourcefiles of Anarchy Online.

---------------------------------------------------------------------
    Anarchy Online and Funcom logo are trademarks of Funcom Inc. 
         Game content and materials Copyright 2000 Funcom.
             www.anarchy-online.com | www.funcom.com
---------------------------------------------------------------------