TRAVEL.TO/RUBI-KA HUGE MAP 2.1
by Nepentheia


MAP FILES.
This download will provide two files to replace the current in-game map files with maps of Rubi-Ka which provide a regular sized map with general overview information, and a larger sized map (four times the size of the regular map) which provides incredible detail of playfield terrain and structures. These maps also provide the following information:
- Playfield borders with zone lines
- Playfield names
- Land Control Areas
- Whom-Pah System routes
- Grid Access Terminal locations
- Public Grid Access requirements
- Fixer Grid Exits
- Playfield Locations in the Fixer Grid
- Ferry Routes
- Guarded Locations (outposts, guard posts, etc.)
- Carbonrich Rock fields and qualities
- Labeled Points of Interest
- Dungeons
- DynaCamps
- Amenities (bank terminals, insurance terminals)
- Supermarkets
- Vendor/shop general locations
- Special Vendor locations
- Shadowlands zone-in locations
- Shadowlands zone-out locations

A legend can be found at the bottom of the 'Huge' map, where there are also listings of the fixer grid by floor, public grid requirements, Whom-Pah connections, and a mini-map of the Whom-Pah System routes.

Absolutely NO Leet DynaCamps have been included in the map! All Leet DynaCamps have been deliberately omitted.

These maps function just as the original maps do, allowing for zooming in and out, and centering the map to your current location. This map is current for Anarchy Online patch 15.3.

NOTE: The In-Game Map files were, in no way, created with the intention to substitute for the uploadable mini-maps and map upgrades. The 'Huge' In-Game Map and 'Regular' In-Game map files were created soley with the purpose and intent to supplement the available in-game mini-maps and map upgrades. These in-game map files are to be used as a complement to the mini-map files and map upgrades, and not as a substitute. 


SUPPLEMENTAL PLAYFIELD HELP FILES.
Also included in the download are supplemental Playfield Help Files with complete information on Land Control Areas and DynaCamps. These Playfield Help Files can be accessed in-game, and provide information on Land Control Areas, including location, level ranges, and name of each Land Control Area. Information on Dynacamps is also provided in the Playfield Help Files, listing location, level range, and MOB type of each Dynacamp. Please see below for instructions on how to install the Playfield Help Files. 

------------------

INSTALLING THE MAP FILES.
After downloading and unzipping the file, place the 'HugeMap' directory in the following location:

Funcom\Anarchy Online\cd_image\textures\PlanetMap

(if you are on the Testlive server: Funcom\Anarchy Online Test\cd_image\textures\PlanetMap)

While in-game, open up your world map by clicking the 'Map"�icon on the bottom of your in-game interface, or pressing "P" on your keyboard,�and click on the '?'�icon on the bottom of the map window.

Select the 'Travel To Rubi-Ka - Huge Map 2.1' to view the map.


INSTALLING THE SUPPLEMENTAL PLAYFIELD�HELP�FILES. 
The 'Huge' map has a number associated with every LCA�in each playfield. To find more information on an LCA, look at the corresponding number for the appropriate playfield in the LCA�Help Files. The Playfield Help Files also give complete information on DynaCamps.

To access the supplemental Playfield�Help Files, open the Playfield Help Files directory and drag the .txt files located in that directory into the following directory:

Funcom\Anarchy Online\cd_image\text\help

(if you are on the TestLive server: Funcom\Anarchy Online Test\cd_image\text\help)

To access the Playfield Help Files while in-game, type the following:

/showfile info-playfield.txt

For easier access, you can create a macro in-game. Type the following and place the macro icon on your hotkey bar:

/macro PF /showfile info-playfield.txt

------------------

VERSION 2.1 (01-04-04)
Made some minor changes and updates to reflect the latest patch version (15.3).

VERSION 2.0 (04-15-03)
Added DynaCamps and Land Control Areas to the map. Created and included Playfield Help Files for supplemental LCA information and DynaCamp information. Added some more POIs and made a few minor corrections to zoneline borders. Made some changes and updates to reflect the latest patch version (14.8)

VERSION 1.1 (02-06-03)
Made some minor changes and updates to reflect the latest patch version (14.7.8).

VERSION 1.0 (01/20/03)
Final Release
___________________
Designed by Nepentheia
http://travel.to/rubi-ka

All designs and text markings are � 2003, Nepentheim Design
Anarchy Online symbol, Leet image and map graphic are � 2003, FunCom
This map may not be reproduced or altered without permission.