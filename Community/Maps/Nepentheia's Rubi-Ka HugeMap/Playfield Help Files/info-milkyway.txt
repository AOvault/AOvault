#C18 MILKY WAY / 625
#C20 
Listed below is information for Land Control Areas and DynaCamps for the Milky Way playfield.


#C19 _______________________________#C19

Land Control Areas#C20
For specific information on a Land Control Area, find the corresponding number of the LCA listed on the map.#C19

LCA    COORDINATES    LCA RANGE    LCA NAME #C20
1        1460, 1940          90-130        The Resilient Forest - North
2        1900, 1540          90-120        The Resilient Forest - East
3        2780, 1380        125-170        Central Prowler Waste
4        1380, 1180        100-125        Central Resilient Forest
5        2860, 1020        125-170        Southern Prowler Waste
6        4020,   980        100-150        The Barren Hills
7        1740,   860        100-125        The Resilient Forest - South
8        2460,   540          50-75          The Silent Woods - East


#C19 _______________________________#C19

DynaCamps#C20
There are no DynaCamps located in the Milky Way playfield.



#C19 _______________________________#C19

#L "- Playfield Index" "/showfile info-playfield.txt"
    #L "DynaCamps" "/showfile info-dynacamp.txt"
    #L "Land Control Areas" "/showfile info-lca.txt"