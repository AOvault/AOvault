#C18 GALWAY COUNTY / 685
#C20 
Listed below is information for Land Control Areas and DynaCamps for the Galway County playfield.


#C19 _______________________________#C19

Land Control Areas#C20
For specific information on a Land Control Area, find the corresponding number of the LCA listed on the map.#C19

LCA    COORDINATES    LCA RANGE    LCA NAME #C20
1        2140, 2620          35-50          Nature Reserve - East
2        1900, 2580          35-50          Nature Reserve - West
3        1300, 1900          50-75          Poole - West
4        1580, 1820          50-75          Poole - East
5        1140, 1100          15-25          V-Hill
6        1580,   700          20-30          Lunder Hills - North
7        2740,   460          25-40          Galway Hills
8        1220,   380          20-30          Lunder Hills
9        2260,   380          25-40          South-east Woods


#C19 _______________________________#C19

DynaCamps#C20
There are no DynaCamps located in the Galway County playfield.



#C19 _______________________________#C19

#L "- Playfield Index" "/showfile info-playfield.txt"
    #L "DynaCamps" "/showfile info-dynacamp.txt"
    #L "Land Control Areas" "/showfile info-lca.txt"