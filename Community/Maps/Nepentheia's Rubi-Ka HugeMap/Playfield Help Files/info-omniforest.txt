#C18 OMNI FOREST / 716
#C20 
Listed below is information for Land Control Areas and DynaCamps for the Omni Forest playfield.


#C19 _______________________________#C19

Land Control Areas#C20
For specific information on a Land Control Area, find the corresponding number of the LCA listed on the map.#C19

LCA    COORDINATES    LCA RANGE    LCA NAME #C20
1          500, 3220          20-35          Northern Grassland
2          980, 3020          15-30          Moderate Grassland
3          460, 2180          10-20          Dungeon Hilltop
4          700, 2180          10-15          Rocky Upsurge
5          340, 1420          15-25          Northern Easy Swamps Notum Fields
6          460,   820          15-26          Ocean Inlet


#C19 _______________________________#C19

DynaCamps#C20
The Boss and Minion levels listed are their approximate level ranges.#C19

BOSS    MINIONS    COORDINATES    SPECIES #C20
    5          3-4           660, 2540        Reets
    5          3-4           540, 2190        Blubbags
  10          3-5           380, 1500        Malle
  10          5-6           380, 3180        Igruana
  10          5-7           380, 1300        Biofreak
  10          6-8           540, 2860        Blubbags
  10          8-10         300, 1060        Shadowmutants
  15        11-13         860, 3260        Tentacle mutants
  15        11-13         300,   620        Pareets
  15        12-14         540,   660        Aquaans
  15        12-14         660, 1340        Hounds
  20        15-17         620,   340        Rhinomen
  25        17-19         540, 1020        Fleas


#C19 _______________________________#C19

#L "- Playfield Index" "/showfile info-playfield.txt"
    #L "DynaCamps" "/showfile info-dynacamp.txt"
    #L "Land Control Areas" "/showfile info-lca.txt"