#C18 PLEASANT MEADOWS / 630
#C20 
Listed below is information for Land Control Areas and DynaCamps for the Pleasant Meadows playfield.


#C19 _______________________________#C19

Land Control Areas#C20
For specific information on a Land Control Area, find the corresponding number of the LCA listed on the map.#C19

LCA    COORDINATES    LCA RANGE    LCA NAME #C20
1        1540, 2660          40-60	         Pleasant Range Offense Hill
2        2380, 2500          60-90	         Central Pleasant Range
3          580, 2420          50-75	         West of 20K	
4        3220, 2220          30-70	         Pleasant Range Defense
5        3220, 1980          60-90	         Pleasant River Defense
6        3260, 1500          60-90	         Pleasant River Offense
7        2260, 1140          40-60	         Central Pleasant Plains
8        3020, 1020          30-70	         East Pleasant Plains
9          740,   460          30-45          West of Versailles Tower


#C19 _______________________________#C19

DynaCamps#C20
There are no DynaCamps located in the Pleasant Meadows playfield.



#C19 _______________________________#C19

#L "- Playfield Index" "/showfile info-playfield.txt"
    #L "DynaCamps" "/showfile info-dynacamp.txt"
    #L "Land Control Areas" "/showfile info-lca.txt"