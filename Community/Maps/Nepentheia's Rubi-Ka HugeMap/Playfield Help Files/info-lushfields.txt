#C18 LUSH FIELDS / 695
#C20 
Listed below is information for Land Control Areas and DynaCamps for the Lush Fields playfield.


#C19 _______________________________#C19

Land Control Areas#C20
For specific information on a Land Control Area, find the corresponding number of the LCA listed on the map.#C19

LCA    COORDINATES    LCA RANGE    LCA NAME #C20
1          940, 3260          30-45          North West Lush Fields 
2        2420, 3180          20-30          North East Lush Fields
3        3460, 2940          10-40          Stret River Island
4        1260, 2460          40-60          West of Outpost
5        1740, 2460          35-60          East of Outpost
6        1780, 1820          20-30          Central Lush Fields
7        2860,   420          10-15          South East Lush Fields
8          980,   380          30-45          South West Lush Fields


#C19 _______________________________#C19

DynaCamps#C20
There are no DynaCamps located in the Lush Fields playfield.



#C19 _______________________________#C19

#L "- Playfield Index" "/showfile info-playfield.txt"
    #L "DynaCamps" "/showfile info-dynacamp.txt"
    #L "Land Control Areas" "/showfile info-lca.txt"