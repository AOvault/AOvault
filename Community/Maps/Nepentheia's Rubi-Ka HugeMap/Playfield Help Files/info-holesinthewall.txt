#C18 HOLES IN THE WALL / 971
#C20 
Listed below is information for Land Control Areas and DynaCamps for the Holes In The Wall playfield.


#C19 _______________________________#C19

Land Control Areas#C20
For specific information on a Land Control Area, find the corresponding number of the LCA listed on the map.#C19

LCA    COORDINATES    LCA RANGE    LCA NAME #C20
1          420,  2020          15-26          Populous Mountain
2          660,  1500          12-22          Hound Land Mining
3          220,  1060          12-20          Stret West Notum Ore
4          740,  820            10-15          Snake Mountain
5          780,  460            20-40          Southern Empty Wastes and Roads
6          380,  340            10-20          Transit Valley Ore


#C19 _______________________________#C19

DynaCamps#C20
There are no DynaCamps located in the Holes In The Wall playfield.



#C19 _______________________________#C19

#L "- Playfield Index" "/showfile info-playfield.txt"
    #L "DynaCamps" "/showfile info-dynacamp.txt"
    #L "Land Control Areas" "/showfile info-lca.txt"