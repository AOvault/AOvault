#C18 STRET EAST BANK / 635
#C20 
Listed below is information for Land Control Areas and DynaCamps for the Stret East Bank playfield.


#C19 _______________________________#C19

Land Control Areas#C20
For specific information on a Land Control Area, find the corresponding number of the LCA listed on the map.#C19

LCA    COORDINATES    LCA RANGE    LCA NAME #C20
1          700, 2420          55-70          Northern River Bank
2        1780, 2460          60-90          Hawker Trench
3        1460, 1740          70-105        Klapam Forest Defense
4        2020, 1740          55-70          Klompfot Defense
5        1900, 1220          70-105        South of Trench
6        1140,   940          80-120        Nile Hills
7        1780,   700          55-70          Aprils Rock Offense
8          820,   420          80-150        Southern Lower River Bank
9        1700,   340          80-150        Aprils Rock Defense


#C19 _______________________________#C19

DynaCamps#C20
There are no DynaCamps located in the Stret East Bank playfield.



#C19 _______________________________#C19

#L "- Playfield Index" "/showfile info-playfield.txt"
    #L "DynaCamps" "/showfile info-dynacamp.txt"
    #L "Land Control Areas" "/showfile info-lca.txt"