#C18 LAND CONTROL AREAS
#C20 
The following information for Land Control Areas is listed in order by maxiumum level range. A complete listing of Land Control Areas can also be viewed by #L "Playfield" "/showfile info-playfield.txt". For Playfield abbreviations, scroll to the bottom of this window.

#C19 _______________________________#C19

RANGE      PF        LCA    COORDINATES    LCA NAME#C20
  10-15     CLO      7       2500, 1220        Micron Slopes Notum Mine
  10-15     GOF      5       1500, 1300        Greater Omni Forest South
  10-15     GS        1         500, 1900        Blossom Valley
  10-15     GS        2         380, 1300        Konty Passage Plains
  10-15     GS        5         380,   580        Kontys Sixth Passage - West
  10-15     GS        6         620,   540        Kontys Sixth Passage - East
  10-15     HIW      4         740,   820        Snake Mountain
  10-15     LF         7       2860,   420        South East Lush Fields
  10-15     OF        4         700, 2180        Rocky Upsurge
  10-15     TC        1         460, 1300        Great W. Forest Vein
  10-15     TC        2       2940,   980        The Hidden Notum Canal
  10-15     TC        4         580,   580        Great W. Forest Dorsal
  10-15     TC        5       1500,   460        Western Mountain Areas

  11-16     AEG      2       2180, 2580        West Wastelands
  11-16     AEG      5       1180, 1340        West of the Dead Forest

  10-20     AT        1       2660, 2020         Sifter Beach
  10-20     AT        4       2660,   820         Grindmoore
  10-20     HIW      6         380,   340         Transit Valley Ore
  10-20     OF        3         460, 2180         Dungeon Hilltop
  12-20     HIW      3         220, 1060         Stret West Notum Ore
  12-20     NL        1       1220, 1060         In the Newland Desert

  12-22     HIW      2         660, 1500        Hound Land Mining
  15-22     AEG      7       1420, 1020        Canyon East

  15-23     AT        5       1380,   380        Gladius Grove

  10-25     GOF      7       1940,   900        Ring Mountain Range
  14-25     GOF      8         940,   460        Southern Isle
  15-25     AT         3       1980, 1340        Athen Fault
  15-25     GC        5       1140, 1100        V-Hill
  15-25     GOF      2       1180, 2460        Dragonback Ridge
  15-25     GS        4         780,   900        Arthur's Pass
  15-25     MD       1          780, 1420        Mutant Domain North
  15-25     NL        2          540,   460        West of Newland Lake
  15-25     OF        5          340, 1420        Northern Easy Swamps Notum Fields
  15-25     SWB     2        2300, 2860        Pondus Beach

  15-26     HIW      1          420, 2020       Populous Mountain
  15-26     OF        6          460,   820       Ocean Inlet

  17-28     GS        3          900, 1220       Vas' Pass

  12-30     SWB     6        1340, 1220       Omni Outpost
  15-30     OF        2          980, 3020       Moderate Grassland
  15-30     SWB     3        1700, 2780       Hound Land
  15-30     WW      8         660,   900        North of Yuttos
  20-30     AT         2        1780, 1780       Academy Ore
  20-30     GC        6        1580,   700       Lunder Hills - North
  20-30     GC        8        1220,   380       Lunder Hills
  20-30     GTC      2        2900, 2940       NE Desert Aperient
  20-30     LF         2        2420, 3180       North East Lush Fields
  20-30     LF         6        1780, 1820       Central Lush Fields
  20-30     MD        2         500,   860        Mutant Domain Central
  20-30     SWB     1        1700, 3100       Hells Courtyard
  20-30     SWB     5        1700, 1940       East Mutie
  20-30     SWB     7          660, 1180       South Mutie
  20-30     TC        3        3220,   620       Mountain Areas
  20-30     WW      6          580, 1740       Charred Groove

  20-35     GOF      4        1860, 1340       Waterfall Swamp
  20-35     OF        1          500, 3220       Northern Grassland
  25-35     AEG      8           820,  780       Canyon South
  25-35     WW      4          340, 2420       Powdered Dunes

  10-40     LF         3        3460, 2940       Stret River Island
  20-40     HWI      5          780,   460       Southern Empty Wastes and Roads
  20-40     SWB     4        1980, 2780       Hound Notum Field
  25-40     GC        7        2740,   460       Galway Hills
  25-40     GC        9        2260,   380       South-east Woods
  25-40     GOF      6          900, 1220       Northern Semi-Barren Area
  25-40     GTC      4        2220, 1900       Piercing Thundertube
  25-40     GTC      6          620, 1660       Tir Prarie
  25-40     GTC      7        1180, 1700       Crater Swamp
  25-40     MD       3          780,   460        Mutant Domain South
  25-40     NLD      1        2940, 2900       Rich Desert Ridge

  12-45     WW      7          940, 1540       West of Perdition
  30-45     AEG      4        2140, 1660       Giant Green River Bank North
  30-45     AEG      6        2100, 1340       Giant Green River Bank South
  30-45     AN        1          420, 2700       Skop Notum Mine
  30-45     CLO      1        1100, 4340       Yukon Source
  30-45     CLO      3        2140, 2420       Round Hills
  30-45     GOF      1        1620, 2660       Greater Omni Forest Swamps
  30-45     GOF      3        1900, 1820       Mountainous Regions
  30-45     GTC      5        2820, 1940       Central Striking Ant
  30-45     LF         1          940, 3260       North West Lush Fields 
  30-45     LF         8          980,   380       South West Lush Fields
  30-45     NLD      2        1980, 2580        East of Meetmedere
  30-45     PM        9          740,   460       West of Versailles Tower
  30-45     VW       1        2420, 2980        By the Rivers Edge
  30-45     VW       4        3740, 2500        East Forest
  32-45     WW      5        2540, 2060        Dust Bank

  25-50     TLR       3        3420, 1540       Fate Notum Field
  25-50     VW       3        1300, 2660       Along the Rivers Edge
  25-50     VW       5        3140, 2020       Rhino Hills
  26-50     WW      3          980, 3140       Between the Craters
  35-50     4H        2          940, 2020       Mountain of Fourtyone
  35-50     4H        3        1300, 1980       Mountain in 4Holes
  35-50     4H        6        1460, 1260       Mountain of Fortytwo
  35-50     AEG      9          900,   460       By the River
  35-50     CLO      2        1460, 2540       Frisko
  35-50     CLO      5        1260, 1820       Borrowed Hill
  35-50     CLO      6        1340, 1340       Narrow Lune
  35-50     GC        1        2140, 2620       Nature Reserve - East
  35-50     GC        2        1900, 2580       Nature Reserve - West
  35-50     WW      2        2220, 3340       Carbon Grove

  40-55     AEG      3        1020, 2460        Mid Wastelands

  30-60     AN        7          820,   340       Mocnuf Notum Mine
  30-60     SWB     8        2260, 1140       The Beach
  35-60     LF         5       1740, 2460        East of Outpost
  40-60     AEG      1        1220, 2740       Northern Wastelands
  40-60     AN        5        1420, 1580       Flubu Notum Mine
  40-60     LF         4        1260, 2460       West of Outpost
  40-60     NLD      4        2580, 1940       North of Rhino Village
  40-60     NLD      5        2700, 1260       South of Rhino Village
  40-60     PM        1        1540, 2660       Pleasant Range Offense Hill
  40-60     PM        7        2260, 1140       Central Pleasant Plains
  40-60     TLR       1        4220, 1580       Illuminati
  40-60     WW      1        1700, 3700       Styx Magma 
  45-60     4H        4        1660, 1740       South of Ahenus
  45-60     BS        7        1260, 2140       Notum Mountain

  37-64     GTC      3        1900, 2700       SurroundingTemple of Three Winds

  30-70     PM        4        3220, 2220       Pleasant Range Defense
  30-70     PM        8        3020, 1020       East Pleasant Plains
  40-70     AN        6        4340,   900       Plago
  45-70     4H        8        1180,   500       Ibreri
  45-70     4H        9          460,   420       Jall Mountain
  55-70     SEB      1          700, 2420       Northern River Bank
  55-70     SEB      4        2020, 1740       Klompfot Defense
  55-70     SEB      7        1780,   700       Aprils Rock Offense

  45-75     BS        2        1260, 3860       Notum Disruption Mountain
  50-75     AV        6        2420, 1900       Waylander Mines
  50-75     CLO      4        2140, 1900       Dense Drewen
  50-75     CLO      8        2100,   540       High Juniper
  50-75     CLO      9        2300,   460       High Juniper Notum Vein
  50-75     GC        3        1300, 1900       Poole - West
  50-75     GC        4        1580, 1820       Poole - East
  50-75     MW       8        2460,   540       The Silent Woods - East
  50-75     NLD      3          540, 2020       Middle of Western Desert
  50-75     PM        3          580, 2420       West of 20K
  50-75     TLR       6        4020,   620       Winterbottom
  50-75     USEB    1          540, 2820       West Pass
  50-75     VW       2          620, 2900       North Forest Road
  50-75     VW       9        3220, 1140       East of Crater
  65-75     USEB    2          900, 2300       Crowning Shallows

  50-76     VW       6          580,  1700      West Forest

  45-80     BS        5        1300, 3060       Central Mountains
  30-80     AN        2        2820, 2340       Klor
  60-80     AN        3        2820, 1660       Harstad
  60-80     AN        8        2260,   380       Jucha

  61-82     AV        8          460,  1380      Dome Ore

  40-90     AN        4          540, 1580       Ubleo
  60-90     4H        1        1580, 2380       Notum Ore in Buttu
  60-90     AV        5          580, 3140       Western Desert
  60-90     PM        2        2380, 2500       Central Pleasant Range
  60-90     PM        5        3220, 1980       Pleasant River Defense
  60-90     PM        6        3260, 1500       Pleasant River Offense
  60-90     SAV      1        1380, 2780       Tetlies Land control area
  60-90     SAV      3          660, 2460       West of outpost 10-3
  60-90     SEB      2        1780, 2460       Hawker Trench
  60-90     TLR       8        2900,   500       Summer
  60-90     USEB    7          940,   420       Greenslopes
  60-90     VW       7        1940, 1620       Crossroads
  60-90     VW       8        1140, 1500       Forestdawn
  75-90     USEB    6        1820,   740       Stret Woods

  70-95     AV        3        1740, 3460       Dreadfire Volcano

    1-100   MORT   5          900, 1460       Green Crater
  55-100   BS        8        2020, 1980       Near Omni-Tek Outpost
  60-100   SFH      1        1900, 3020       North of Lenne
  60-100   SFH      4         900,  2100       By the Ocean
  61-100   SFH      5        2300, 1180       Birm
  70-100   4H        5        1820, 1340       Ibreri Woods North
  70-100   AV        7        1860, 1700       North of Main Omni Base
  75-100   BS        3        1940, 3860       The Notum Plains

  70-105   AN        9        4380,   380       Mune
  70-105   SEB      3        1460, 1740       Klapam Forest Defense
  70-105   SEB      5        1900, 1220       South of Trench

  80-110   AV        2          540,  4180      Draught

  71-120   TLR      4          580,    820       Pegrama
  80-120   AV        4        2780, 3420       Northeast Barren Lands
  80-120   SAV      2        2900, 2660       East of the Great Marsh
  80-120   SEB      6        1140,   940       Nile Hills
  84-120   TLR      5        1220,   700       Grazeland Notum Field
  90-120   MW      2        1900, 1540       The Resilient Forest - East
  90-120   SFH      3        2620, 2660       Defense of Zoto
  90-120   TLR      7          540,    500      Southern Forest of Illuminations
110-120   BF        2        1100, 2620       Muddy Pools

100-125   MW      4	      1380, 1180        Central Resilient Forest
100-125   MW      7	      1740,   860        The Resilient Forest - South

  90-130   CAV     4	       3340, 2700        North-East Forest
  90-130   MW      1       1460, 1940        The Resilient Forest - North

  90-135   GTC     1	      1100, 3100        The Mineral Mine

  70-140   USEB    4       2020, 1740        Stret Vale Deux Drilling Field

106-143   SAV      5	      2740, 1180        South of Forest of Geholva

  55-150	  BS       6	         380, 2300        Surrounding Evil
  80-150	  BS       1	         940, 4820        Central Desert North
  80-150	  SEB     8	         820,   420         Southern Lower River Bank
  80-150	  SEB     9	       1700,   340         Aprils Rock Defense
100-150	  4H       7	       1740, 1060        Ibreri Woods
100-150	  AV       1	       2740, 4260        Griffon Frontier
100-150	  AV       9	       2700,   620        Crystal Forge Volcano
100-150	  AV      10         660,   460        SW Low Plateau
100-150	  BF       3	       1700, 2300        West of Wine
100-150	  BS       4	        940,  3380        Near Clan Outpost
100-150	  BS       9	        420,    820        Shores Notum Vein
100-150	  CAV     6	       3100,   980        Mid Clutching Forest
100-150	  CAV     8	       3180,   620        South Clutching Forest
100-150	  DAV     1	       1140, 3380        Old Ruins
100-150	  DAV     2	       3180, 2900        Plains of Defense
100-150	  EFP      7	       2020,   860        Central Sharewood
100-150	  MORT   8         540,   540        South West Craterwall
100-150	  MW      6       4020,   980        The Barren Hills
100-150	  PW       7       1500, 1340        Lower Plateu Zone
100-150	  PW       8       2100, 1380        The Mid Canyon Crossing
100-150	  PW     10         900, 1060        West of Canyon
100-150	  PW     11       3180,   940        The Canyon Mines
100-150	  SAV     4       2300, 2020         Defense of Geholva
100-150	  SAV     8       2460,   540         Bendelham forest Defense
100-150	  SFH     2         860, 2820         Little Hawaii Defense
100-150	  TLR     2          500, 1540         Northern Forest of Illuminations
100-150	  USEB   3       1660, 2180         Haven Notum Crematorium
100-150	  SFH     7       1860,   500         South in Nightplain
120-150	  BF       8       2340,   860         Borderline

110-160	  MORT   6      3100, 1460         Oasis Ore

100-170   CAV       3       2900, 2820       North Forest
100-170   MORT    1       1500, 3420       Terraform Edge
125-170   MW       3       2780, 1380       Central Prowler Waste
125-170   MW       5       2860, 1020       Southern Prowler Waste
130-170   CAV       5        860, 1220        North-West of Lava Ditches
130-170   CAV       7        860,   780        South-West of Lava Ditches
130-170   MORT    4       1220, 2220       Middle Mort Desert

120-180   BF         4       2940, 2260       East of Wine
120-180   BF         9       2020,   420       Southern Belial Mine
120-180   EFP       3         620, 2980        Plains of Jarga Defense
120-180   PW       3         980, 2060        South of Sabulum
120-180   PW       9       3020, 1220        Plains of dust
120-180   SAV      6         860,   900        Avid Crater
120-180   SAV      7       1540,   900        East of Avid Crater
120-180   SFH      6        2700,  660        SFH Defense
120-180   USEB    5       1340, 1620        The Flooded Bottomland
130-180   DAV      3       1740, 2300        The Haunted Forest Outskirt
130-180   DAV      4         900, 2200        Forest of Xzawkaz
130-180   DAV      6       1420, 1500        Island of Control
130-180   DAV      7       1340, 1140        The Swamp of Hope

130-190   BF        6       2500, 1660        River Delta

130-195   BF        5       1900, 1740        Central Belial Forest

130-200   EFP       6       1540, 1140        Clefre Defense
140-200   BF       10        620,   380        Southwest Belial Mining District
140-200   CAV      1       1740, 3100        By the Fisher Village
140-200   CAV      2       2100, 3060        Fisher Village Approach
150-200   EFP       1       2700, 3860        Krud the Lost Valley Defense
150-200   MORT   7        2740,   700        South East Craterwall
160-200   BF        1        2940, 2820        Forest Waters
160-200   BF        7        2540, 1220        Junction Forest

140-210   DAV      9        2140,   780        Middle of the Foul Forest
160-210   MORT   9        2780,   540        Stormshelter

150-225   EFP       2       1900, 3180        Pranade

190-230   PW       4       3940, 2060        Cyborg Border
190-230   PW      12       2300,  780        South of Canyon

170-250   MORT   2       3060, 3020        West Spirals
170-250   MORT   3       3500, 2980        East Sprials
191-250   PW       2       3780, 2540        Middle of Liberty

200-300   DAV      5       2260, 1860        In the Swamp of Horrors
200-300   DAV      8       2900, 1100        South of the Medusa
200-300   DAV     10        540,   540        Southern Forest of Xzawkaz
200-300   EFP       4       2460, 2260        Old Plains
200-300   EFP       5       1540, 1780        Middle of Easter Fouls Plains
200-300   EFP       8	        820,   540        Pegradul
200-300   PW       1       3220, 3020        North of Cyborg Hideout
200-300   PW       5       2820, 1820        Middle of Perpetual Wastelands
200-300   PW       6       3740, 1700        South of Cyborg Hideout


#C19 _______________________________#C20

#C19 Playfield Abbreviations#C20

AEG - Aegean
AN - Andromeda
AT - Athen Shire
AV - Avalon
BF - Belial Forest
BS - Broken Shores 
CAV - Central Artery Valley
COL - Clondyke
DAV - Deep Artery Valley
EFP - Eastern Fouls Plain
4H - 4 Holes
GC - Galway County
GS - Galway Shire
GOF - Greater Omni Forest
GTC - Greater Tir County
HIW - Holes in the Wall
LF - Lush Fields
MW - Milky Way
MD - Mutant Domain
NL - Newland
NLD - Newland Desert
OF - Omni Forest
PW - Perpetual Wastelands
PM - Pleasant Meadows
SAV - Southern Artery Valley
SFH - Southern Fouls Hills
SEB - Stret East Bank
SWB - Stret West Bank
TLR - The Longest Road
TC - Tir County
USEB - Upper Stret East Bank
VW - Varmint Woods
WW - Wailing Wastes


#C19 _______________________________#C20

Click on this #L "LCA" "/macro LCA /showfile info-lca.txt" link to create a hotkeyable macro for easier access to this Help File.

#L "- Playfield Index" "/showfile info-playfield.txt"
    #L "DynaCamps" "/showfile info-dynacamp.txt"
    #L ">Land Control Areas" "/showfile info-lca.txt"