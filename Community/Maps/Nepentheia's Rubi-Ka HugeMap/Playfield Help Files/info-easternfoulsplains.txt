#C18 EASTERN FOULS PLAINS / 620
#C20 
Listed below is information for Land Control Areas and DynaCamps for the Eastern Fouls Plains playfield.


#C19 _______________________________#C19

Land Control Areas#C20
For specific information on a Land Control Area, find the corresponding number of the LCA listed on the map.#C19

LCA    COORDINATES    LCA RANGE    LCA NAME #C20
1        2700, 3860        150-200        Krud the Lost Valley Defense
2        1900, 3180        150-225        Pranade
3          620, 2980        120-180        Plains of Jarga Defense
4        2460, 2260        200-300        Old Plains
5        1540, 1780        200-300        Middle of Easter Fouls Plains	
6        1540, 1140        130-200        Clefre Defense
7        2020,   860        100-150        Central Sharewood
8          820,   540        200-300        Pegradul


#C19 _______________________________#C19

DynaCamps#C20
There are no DynaCamps located in the Eastern Fouls Plains playfield.



#C19 _______________________________#C19

#L "- Playfield Index" "/showfile info-playfield.txt"
    #L "DynaCamps" "/showfile info-dynacamp.txt"
    #L "Land Control Areas" "/showfile info-lca.txt"