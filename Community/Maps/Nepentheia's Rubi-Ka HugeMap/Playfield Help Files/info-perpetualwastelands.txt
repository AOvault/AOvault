#C18 PERPETUAL WASTELANDS / 570
#C20 
Listed below is information for Land Control Areas and DynaCamps for the Perpetual Wastelands playfield.


#C19 _______________________________#C19

Land Control Areas#C20
For specific information on a Land Control Area, find the corresponding number of the LCA listed on the map.#C19

LCA    COORDINATES    LCA RANGE    LCA NAME #C20
1        3220, 3020        200-300        North of Cyborg Hideout
2        3780, 2540        191-250        Middle of Liberty
3         980,  2060        120-180        South of Sabulum
4        3940, 2060        190-230        Cyborg Border
5        2820, 1820        200-300        Middle of Perpetual Wastelands
6        3740, 1700        200-300        South of Cyborg Hideout
7        1500, 1340        100-150        Lower Plateu Zone
8        2100, 1380        100-150        The Mid Canyon Crossing
9        3020, 1220        120-180        Plains of dust
10        900, 1060        100-150        West of Canyon
11       3180,  940        100-150        The Canyon Mines
12       2300,  780        190-230        South of Canyon


#C19 _______________________________#C19

DynaCamps#C20
The Boss and Minion levels listed are their approximate level ranges.#C19

BOSS    MINIONS    COORDINATES    SPECIES #C20
105        76-  90       1140, 1140        Mantis
110        85-  90       1940, 1380        Cyborgs (2)
130      105-110       3060,   900        Anuns
165      135-140       1460,   860        Mantis
165      135-140       2660, 2300        Mantis
175      145-150         380,   500        Anuns
175      145-150         380,   900        Anuns
185      150-155         460, 3140        Anuns
185      150-155         700, 2460        Anuns
185      150-155       1260, 2860        Anuns
185      150-155       1340, 3060        Anuns
190      145-150         420, 1500        Sandworms
200      165-170       2460, 2660        Mantis
200      165-170       2740, 2460        Mantis
200      165-170       2980, 2940        Mantis
200      165-170       3100, 3260        Mantis
200      165-170       3500, 2020        Anuns
205      165-170       2220, 3340        Mantis
210      121-141       3980, 1780        Sand Demon
245      171-190       3460, 2940        Cyborgs (2)



#C19 _______________________________#C19

#L "- Playfield Index" "/showfile info-playfield.txt"
    #L "DynaCamps" "/showfile info-dynacamp.txt"
    #L "Land Control Areas" "/showfile info-lca.txt"