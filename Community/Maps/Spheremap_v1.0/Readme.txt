Installation:
Put the 'Spheremap_v1.0' folder in your AO directory under: 
..\cd_image\textures\PlanetMap\ 

Open the planetmap ingame 'p' and click on the questionmark button '?', then select the map. Click the center button. 
To switch between the text/no-text versions, use the '+' and '-' buttons in the mapwindow. 

Enjoy.


Troubleshooting:
- Make sure the folder is named exactly 'Spheremap_v1.0', contains 3 files (bigSpheremap_v1.0.bin, smallSpheremap_v1.0.bin, Spheremap_v1.0.txt) and is placed in the 'Planetmap' directory.
- Center the map on your char.
- You're in SL, right?
- Under Windows XP, you can try the following: navigate to the 'Planetmap' folder, right-click it and select 'Properties'. Click the 'Advanced' button. Make sure the 'Indexing Service' checkbox is turned on for this directory.
- If all of the above fails, put the 'Spheremap_v1.0.txt' file directly into the 'PlanetMap' directory.

Acknowledgements:
Main thanks goes to all the contributors out there who help the community enjoy the game more. Be it through forums, web-pages, 3rd-party tools or ingame advice and help. You make the game go round. <3
Thank you to all the people sending me tells with missing/wrong locations and encouraging me to continue working on the map. I'm really glad you're finding the map useful and like it.

Disclaimer:
While I have tried to make the map as complete and accurate as possible, I'm sure I've missed a few locations. In certain areas of the map, the location cursor isn't matching up with the actual location you're at. This is unfortunate, but I haven't found a way to fix it. The dots are all placed where the mobs appear geographically, not where the cursor is.

Versioning: 
- January 14 2005: Updated to 1.0
Finished up the Pandemonium map. Added missing mobs in Inferno. Implemented Hollows into the Penumbra map. Added some mobs in Adonis. Marked several dynas in Scheol. Updated Elysium, adding dynas and other missing locations. Overhaul of Nascense, mapped Crippler Cave and added several dynas. Located every garden/sanct statue throughout the shadowlands. Added a few static dungeons in several playfields. A new glyph listing is implemented. Profession armor guide updated and clarified. Cleaned up the pocket directory.


- March 31: Updated to 0.731
Added one missing wall in Penumbra Catacombs. Sorry for the inconvenience.
- March 31: Updated to 0.73
Added maps for all the catacombs. Several new discoveries in every playfield. Updated quest mobs. Added Jump-Off spots. Tweaked locations of a few dynas. Corrected one typo :-)
- February 15: Updated to v0.72
Pandemonium updated with new locations. Added several quest locations in Penumbra. Updated Statue locations and added several Sanct. Statues. Marked a few statics and dynas across the map. Implemented an info-sidebar (check it out to the right side of the map). Few minor tweaks.
- February 2: Updated to v0.71
Another Inferno dyna update. Most dynas are marked now. Fixed a cosmetic glitch.
- January 31: Updated to v0.70
Implemented a first version of Pandemonium, including a few locations. Added several locations in Inferno, mainly dynas and statics. Mapped out a majority of dynacamps in Elysium. Few misc. locations accross the playfields.
- January 8: Updated to v0.63
Another Dyna/Unique update - Added a whole bunch of spots in Inferno and Scheol. Also marked a few new locations throughout the Shadowlands.
- December 24: Updated to v0.62
Added lots of dynas in Inferno, Penumbra, Adonis and Scheol. A few new statics marked. Included several essential locations in Inferno. Added Sanctuary Statues in Adonis and Elysium.
- December 9: Updated to v0.61
Updated Statue locations in every playfield to reflect the 15.3 patch. Updated Dyna-Camp locations, mainly in Penumbra. Added several new dynas. More locations in Inferno. Small cosmetic changes done.
- December 2: Updated to v0.60
Finally got access to Inferno. Added some first locations there. Added area names to all the playfields. Unified fonts. Mapped several new dynacamps and statics.
- October 28: Updated to v0.59 
More is more.  
- October 17: Updated to v0.58 
Another big overhaul - still balancing readability, colors and such. Added several locations all over the place. New dots for zones and landcontrol as well as brink mobs. 
- October 13: Updated to v.0.57 
Implemented two versions in this release: the normal, text-overlay map and one version without text. use the '+' and '-' buttons on the map to switch between em. Changed dot/color of dynacamps and uniques to make em more distinguishable. Added a few locations in Penumbra, Adonis and Elysium. 
- October 12: Updated to v0.56 
Added a few locations in Penumbra and Adonis and did some cosmetic updates. 
- October 8: Updated to v0.55 
Added several dungeons, misc locations across all playfields. 
- October 6: Updated to v0.54 
Several dynas and bosses in Pen added and removed non-existant statues. Added a few misc spots across the whole map. 
- October 4: Updated to v0.53 
Penumbra getting nicely mapped out now - Added bosses, another dungeon and a few misc spots. Also went thru Nascense and Elysium again, adding several locations there. Have fun. 
- October 3: Updated to v0.52 
More locations in Penumbra, cleaned some text placements, made the spots on the map a bit more visible and implemented a first version of a functioning small size map. 
- October 2: Updated to v0.51 
More locations in Penumbra, Nascense and Jobe Training. Overhaul of the font (sigh - again...) to make Penumbra locations more readable. 
- October 1: Updated to v0.50 
First few locations in Penumbra. Enjoy  
- Sept 29: Updated to v0.47 
Mapped out all essential quest spots in Adonis. WTB access to Penumbra! 
- Sept 27: Updated to v0.46 
Changed font to increase readability and make the whole text more subtle. made all the spots smaller. added a few locations in Adonis. 
- Sept 24: Updated to v0.45 
Added a few spots in Adonis, added alternate spot for Ercorashs in Upper Scheol. 
- Sept 23: Updated to v0.44 
Fixed location of Ercorash in Lower Scheol. added locations in Adonis. 
- Sept 21: Updated to v0.43 
Brightened up Lower Scheol - making it much more navigateable. 
- Sept 21: Updated to v0.42 
Increased readabilty of portals/statues. might get another color-change later. not looking forward to inferno  
- Sept 21: Updated to v0.41 
Added versioning and a few locations in Adonis and Scheol.