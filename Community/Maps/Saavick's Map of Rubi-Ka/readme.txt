Saavick's Map of Rubi-Ka 2.2

    Made by Saavick
    February 2018

Thanks to:

    - Demoder
      The tools 'Map Compiler', 'GUI Compiler' and 'Planet Map Viewer' have made creating this map much easier.

    - Finnagen
      The documentation on the making of 'CSP Map' has been invaluable in mapping each playfield.

    - Lucier
      For helping me with the APFs.

    - Michizure
      For helping me with the Reck and the APFs and for answering my endless questions.

    - Nepentheia
      'Huge Map' has been an important source and inspiration for this map.

    - Onack
      'Atlas of Rubi-Ka' has also been an important source and inspiration for this map.


-- Changes --

2.2
    Released February 15th, 2018
    - Updated Greater Tir County to show removal of city plots.
    - Added a missing store in Stolt's Trading Outpost.
    - Added a missing portal to the Inner Sanctum outside Rome Blue.
    - Improved accuracy of character tracking in Milky Way.
    - Improved accuracy of Dangerous Prisoner marker.
    - Corrected various spelling errors.

2.1
    Released April 30th, 2016
    - Added the Re-Incarnator and the new hidden rooms in Inner Sanctum.
    - Added the shuttle to ICC HQ near the temple of three winds.
    - Added the Observer in the Reck.
    - Added Omni-Pol Secretary Lambeer in Omni-1 Entertainment.
    - Added a missing dynaboss in Aegean.
    - Added locations to collect Carbonrich rocks on layer 4.
    - Updated colours for Factory labels in the Reck to indicate presence of guards.
    - Improved accuracy of Deadly Prisoner marker.

2.0
    Released February 1st, 2016
    - Improved accuracy of character tracking in several zones.
    - Corrected spelling on Crypto Crackers.
    - Corrected several dyna boss markers.
    - Added a missing Superior Store in Wine.
    - Added maps for the following areas:
      * The Reck
      * Unicorn Outpost
      * Unicorn Outpost - Lower Level
      * Sector 10
      * Sector 7
      * Sector 7 - Crashed Alien Ship
      * Sector 13
      * Sector 28
      * Sector 35
      * Sector 42
      * SBC-Xpm Site Alpha-Romeo (Notum Mining Area)
      * Serenity Islands
      * Montroyal
      * Playa del Desierto

1.1
    Released November 29th, 2015
    - Improved accuracy of character tracking in several zones.
    - Improved accuracy of borders between several zones.
    - Improved accuracy of Escaped Prisoner markers.
    - Improved accuracy of Cyborg Techwrecker marker.
    - Improved accuracy of Primary Computer Terminal marker.
    - Added a missing spawn location for Joe Two-Fingers.
    - Added the Juggernauts in Lush Fields.
    - Added a missing dynaboss in Arete.
    - Added maps for the following indoor areas:
        * Condemned Subway
        * Temple of Three Winds
        * Steps of Madness
        * Foreman's Office
        * Crypt of Home
        * Smugglers Den
        * Inner Sanctum
        * Camelot Castle

1.0
    Released October 24th, 2015
    - Initial version.
