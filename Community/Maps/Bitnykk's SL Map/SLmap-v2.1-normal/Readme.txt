Updated ingame map - latest releases

** Shadowlands **

Download latest @ https://rubi-ka.net/bitnykk

WARNING :
For most screens set upto 1300 pixels width definition, normal version is advised ; over 1300 pixels width definition, especially if text is hard for you to read, prefer wide version now available.

EASY INSTALLATION :
With any old version removed, extract archive in ao/cd_image/textures/PlanetMap ; you should obtain a new "SLmap" folder containing various files (bin, xml & txt) according to the version you downloaded.
At this point, just open ingame worldmap once in SL by hitting "P" & then click the "i" upperleft of that window > Select Map > choose SLmap link from the list ; that's all done !11!!1
If you have an error hitting "P", assuming you already got the map correctly installed, it's fixed by chat command : /setoption ShadowlandMapIndexFile "SLmap/SLmap.txt" or /setoption ShadowlandMapIndexFile "SLmap/SLmapWide.txt"

VERSION & CHANGES :
2.1 - february 2017 => Greedy Shade locations and nanos added ; few graphical elements readjusted ; several text overlaps and typos fixed ; catacombs entrance descriptions improved ; 3rd layer grid & number corrected.
2.0 - june 2016 => many cities & buildings of every zones graphically improved ; several NPC, spot or dynas updated ; few typos, dot overlaps, border bugs, grid coords or map positions corrected ; wide version added.
1.0 - december 2015 => playground removed ; backgrounds minimalized ; borders cleaned ; LoX enlighted ; layers optimized ; closeview repaired ; title font changed ; garden/sanct nanos refreshed ; new maps/NPC added.

GOALS BEHIND :
Idea is to go on from the best SL map we had so far, Onack's AoSL, and keep informations updated in minimal filesize accordingly to last AO patches.

NEW DISTRIBUTION :
Layer 1 => small map with minimal zone informations / Layer 2 => everything important dynabosses included / Layer 3 => nanos, quest related & various NPCs

FUTURE CHANGES :
Introduce new additions AO team will patch + community feedbacks & suggestions.

OFFICIAL THREADS :
http://www.ao-universe.com/forum/viewtopic.php?t=5752 preferably or http://forums.anarchy-online.com/showthread.php?615312
Please post your feedback (about the SL map only !) in any of these, what's relevant and doable will be taken in consideration.

SPECIAL THANX :
Onack for an inestimable work over years ; Novagen for hosting & sources ; Saavick for test & tricks ; Demoder & Cell-team for toolbox ; Alliance of Rimor for infos ; other players that helped (you know who you are).