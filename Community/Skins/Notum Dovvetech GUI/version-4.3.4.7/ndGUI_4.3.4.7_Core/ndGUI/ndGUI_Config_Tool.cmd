@ECHO OFF
REM - - - - - - - - - - - - - - - -
REM  ndGUI Config Tool
REM  v1.1.1 (2015-Apr-22)
REM - - - - - - - - - - - - - - - -

SETLOCAL

IF NOT EXIST ndGUI_Core_version_info.txt GOTO :AbortNoCore
IF NOT EXIST ndGUI_Theme_version_info.txt GOTO :AbortNoTheme


:MainMenu
CLS
ECHO.
ECHO - - - - - - - - - - - - - - - - - - - - - - - - - - - -
FIND "version" ndGUI_Core_version_info.txt | FIND "version"
FIND "version" ndGUI_Theme_version_info.txt | FIND "version"
FIND "installed" ndGUI_Theme_version_info.txt | FIND "installed"
ECHO - - - - - - - - - - - - - - - - - - - - - - - - - - - -
ECHO.
ECHO - Config Options -
ECHO.
ECHO 1) Control Center Layout
ECHO 2) Raid Window Layout
ECHO 3) RaidBar Size
ECHO 4) Nano Icon Borders
ECHO 5) Info Window Font Size
ECHO 6) Text Colour Scheme
ECHO.
ECHO q) Quit
ECHO.
SET ndMenuChoice=-
SET /P ndMenuChoice=Select option and hit "Enter": 
SET ndMenuChoice=%ndMenuChoice:~0,1%
IF "%ndMenuChoice%" == "1" GOTO :CCLayout
IF "%ndMenuChoice%" == "2" GOTO :RaidWindow
IF "%ndMenuChoice%" == "3" GOTO :RaidBar
IF "%ndMenuChoice%" == "4" GOTO :NanoBorders
IF "%ndMenuChoice%" == "5" GOTO :InfoWindow
IF "%ndMenuChoice%" == "6" GOTO :TextColor
IF "%ndMenuChoice%" == "q" GOTO :QuitConfig
GOTO :MainMenu


:CCLayout
CLS
ECHO.
ECHO - Control Center Layout -
ECHO.
ECHO Control Center customization will be reset to default.
ECHO.
ECHO 1) Menubar bottom/left
ECHO 2) Menubar bottom/right (default)
ECHO 3) Menubar top/left
ECHO 4) Menubar top/right
ECHO 5) Floating menu bottom/left
ECHO 6) Floating menu top/left
ECHO 7) Floating menu top/right
ECHO.
ECHO b) Back
ECHO q) Quit
ECHO.
SET ndMenuChoice=-
SET /P ndMenuChoice=Select option and hit "Enter": 
SET ndMenuChoice=%ndMenuChoice:~0,1%
SET ndFileSuffix=-
IF "%ndMenuChoice%" == "1" SET ndFileSuffix=_menu_bottom_left
IF "%ndMenuChoice%" == "2" SET ndFileSuffix=_menu_bottom_right
IF "%ndMenuChoice%" == "3" SET ndFileSuffix=_menu_top_left
IF "%ndMenuChoice%" == "4" SET ndFileSuffix=_menu_top_right
IF "%ndMenuChoice%" == "5" SET ndFileSuffix=_buttons_bottom_left
IF "%ndMenuChoice%" == "6" SET ndFileSuffix=_buttons_top_left
IF "%ndMenuChoice%" == "7" SET ndFileSuffix=_buttons_top_right
IF "%ndMenuChoice%" == "b" GOTO :MainMenu
IF "%ndMenuChoice%" == "q" GOTO :QuitConfig
IF "%ndFileSuffix%" == "-" GOTO :CCLayout
SET ndWorkFile=Views\ControlCenter
GOTO :FinalizeChoice


:RaidWindow
CLS
ECHO.
ECHO - Raid Window Layout -
ECHO.
ECHO You may need to "Reset/Fix Raid Window" in Options/ndGUI/Settings.
ECHO.
ECHO 1) Standard (default)
ECHO 2) Compact
ECHO.
ECHO b) Back
ECHO q) Quit
ECHO.
SET ndMenuChoice=-
SET /P ndMenuChoice=Select option and hit "Enter": 
SET ndMenuChoice=%ndMenuChoice:~0,1%
SET ndFileSuffix=-
IF "%ndMenuChoice%" == "1" SET ndFileSuffix=_standard
IF "%ndMenuChoice%" == "2" SET ndFileSuffix=_compact
IF "%ndMenuChoice%" == "b" GOTO :MainMenu
IF "%ndMenuChoice%" == "q" GOTO :QuitConfig
IF "%ndFileSuffix%" == "-" GOTO :RaidWindow
SET ndWorkFile=Views\Raid
GOTO :FinalizeChoice


:RaidBar
CLS
ECHO.
ECHO - RaidBar Size -
ECHO.
ECHO You may need to "Reset/Fix RaidBars" in Options/ndGUI/Settings.
ECHO.
ECHO 1) Standard (default)
ECHO 2) Small
ECHO.
ECHO b) Back
ECHO q) Quit
ECHO.
SET ndMenuChoice=-
SET /P ndMenuChoice=Select option and hit "Enter": 
SET ndMenuChoice=%ndMenuChoice:~0,1%
SET ndFileSuffix=-
IF "%ndMenuChoice%" == "1" SET ndFileSuffix=_standard
IF "%ndMenuChoice%" == "2" SET ndFileSuffix=_small
IF "%ndMenuChoice%" == "b" GOTO :MainMenu
IF "%ndMenuChoice%" == "q" GOTO :QuitConfig
IF "%ndFileSuffix%" == "-" GOTO :RaidBar
SET ndWorkFile=Views\RaidBar
GOTO :FinalizeChoice


:NanoBorders
CLS
ECHO.
ECHO - Nano Icon Borders -
ECHO.
ECHO 1) Visible (default)
ECHO 2) Removed
ECHO.
ECHO b) Back
ECHO q) Quit
ECHO.
SET ndMenuChoice=-
SET /P ndMenuChoice=Select option and hit "Enter": 
SET ndMenuChoice=%ndMenuChoice:~0,1%
SET ndFileSuffix=-
IF "%ndMenuChoice%" == "1" SET ndFileSuffix=_standard
IF "%ndMenuChoice%" == "2" SET ndFileSuffix=_borderless
IF "%ndMenuChoice%" == "b" GOTO :MainMenu
IF "%ndMenuChoice%" == "q" GOTO :QuitConfig
IF "%ndFileSuffix%" == "-" GOTO :NanoBorders
ECHO.
IF NOT EXIST Graphics%ndFileSuffix%.uvgi GOTO :AbortNoFile
ECHO Changing selected option...
XCOPY /R /Y /Q Graphics%ndFileSuffix%.uvgi Graphics.uvgi
IF ERRORLEVEL 1 GOTO :AbortCopyFailed
ECHO Done.
ECHO.
PAUSE
GOTO :MainMenu


:InfoWindow
CLS
ECHO.
ECHO - Info Window Font Size -
ECHO.
ECHO 1) Medium (ndGUI default)
ECHO 2) Large (Game default)
ECHO.
ECHO b) Back
ECHO q) Quit
ECHO.
SET ndMenuChoice=-
SET /P ndMenuChoice=Select option and hit "Enter": 
SET ndMenuChoice=%ndMenuChoice:~0,1%
SET ndFileSuffix=-
IF "%ndMenuChoice%" == "1" SET ndFileSuffix=_medium
IF "%ndMenuChoice%" == "2" SET ndFileSuffix=_large
IF "%ndMenuChoice%" == "b" GOTO :MainMenu
IF "%ndMenuChoice%" == "q" GOTO :QuitConfig
IF "%ndFileSuffix%" == "-" GOTO :InfoWindow
SET ndWorkFile=Views\InfoView
GOTO :FinalizeChoice


:TextColor
CLS
ECHO.
ECHO - Text Colour Scheme -
ECHO.
ECHO 1) Game Default
ECHO 2) Notum (default)
ECHO 3) Classic Notum (v4.33 and before)
ECHO.
ECHO b) Back
ECHO q) Quit
ECHO.
SET ndMenuChoice=-
SET /P ndMenuChoice=Select option and hit "Enter": 
SET ndMenuChoice=%ndMenuChoice:~0,1%
SET ndFileSuffix=-
IF "%ndMenuChoice%" == "1" SET ndFileSuffix=_game_default
IF "%ndMenuChoice%" == "2" SET ndFileSuffix=_notum
IF "%ndMenuChoice%" == "3" SET ndFileSuffix=_classic_notum
IF "%ndMenuChoice%" == "b" GOTO :MainMenu
IF "%ndMenuChoice%" == "q" GOTO :QuitConfig
IF "%ndFileSuffix%" == "-" GOTO :TextColor
SET ndWorkFile=TextColors
GOTO :FinalizeChoice


:FinalizeChoice
ECHO.
IF NOT EXIST %ndWorkFile%%ndFileSuffix%.xml GOTO :AbortNoFile
ECHO Changing selected option...
XCOPY /R /Y /Q %ndWorkFile%%ndFileSuffix%.xml %ndWorkFile%.xml
IF ERRORLEVEL 1 GOTO :AbortCopyFailed
ECHO Done.
ECHO.
PAUSE
GOTO :MainMenu


:AbortNoCore
ECHO.
ECHO Incomplete ndGUI installation: no Core.
GOTO :QuitConfig

:AbortNoTheme
ECHO.
ECHO Incomplete ndGUI installation: no Theme.
GOTO :QuitConfig

:AbortNoFile
ECHO.
ECHO A required file was not found.
GOTO :QuitConfig

:AbortCopyFailed
ECHO.
ECHO A file overwrite operation was unsuccessful,
ECHO the selected change WAS NOT APPLIED.
GOTO :QuitConfig

:QuitConfig
ECHO.
ECHO The Config Tool will now quit.
ECHO.
PAUSE
