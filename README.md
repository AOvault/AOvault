# AOvault

Welcome to the AOvault project! AOvault aims at making a backup of Anarchy Online and everything associated with the game in order to keep the game in existence for as long as possible.

## Repository Organisation

This repository is divided in two parts: **Funcom** which contains all official material from Anarchy Online, game files, installers, trailers, or even the official book 'Prophet Without Honor'. The second part: **Community** contains all the community creations such as guides, skins, or programs.

## Contributing

If you wish to contribute to the project it actually is very simple. See something missing? Maybe you found a comunity made comics, or maybe you found something of a higher quality? Then, feel free to make a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) and i will review it.

## Contributors

A big thank you to everyone who contributed towards AOvault

- **Timba:** For his concept art collection started in June 30th, 2016. His archiving work has been greatly appreciated when the AOvault project was launched.
- **Slackin:** For providing us the Anarchy Online Special Edition Bonus disc content.
- **Yumehime:** For the fetched contents from Funcom's FTP servers.
- **Michizure:** For having added a multitude of very beautiful concept arts on his Twitter account.
- **Mastablasta:** For scanning box arts along with game manuals, game content and keyboard layouts for every releases.
- **Bitnykk:** For compiling Anarchy Online music for what then became the Anarchy Online Music Collections.
- **Saavick:** For her support towards the project and for providing with fixes for the older maps as well as unfindable sources.
- **Lal:** For providing very good criticism and suggestions towards the way the project should be sorted.
- **Windguaerd:** For AOscripter and AOtwinker.
- **Higain:** For providing ao omni terminal GUI version 1 and Ultima.lightning server uptime as well as several motion videos.
- **Zagadka:** For providing Basher.
- **Odonoptera:** For allowing the archiving of the new progression server announcement livestream on February 27th 2019.

## Follow us!

- [Youtube](https://www.youtube.com/channel/UCiMXYGD9fVwQCPmkuuhls7A)
- [Gitlab](https://gitlab.com/AOvault/AOvault.git)
- [Pinterest](https://www.pinterest.com/AOvault/)
