Genesis started as a fork of TIDE, the world editor used on Anarchy Online.
When I joined the team, none of the original programmers of TIDE worked at Funcom anymore

Texture work and art are done in conventional products like Photoshop. World-building is done with an in-house custom tool called �Tide�.

Teh Tewlz - Part I: Tide
So, the first story will be about Tide which after 18.7 was created is called Cheetah Tide. This is our "kinda database" and world building tool. Tide is used to build playfields and add assets such as models, textures, icons and items. Lastly Tide is one of the few development tools we have that has version control (a huge applause is needed!)

So, lets start with the good things about Tide first. 
Tide doesn't always crash and once you have messed around with it until you are past the stage of "if I press that button will I break AO?" you can actually make new shiny areas for players to explore. Worldbuilding is great when Tide is happy.

Tide also has a very nice splash screen which I re-worked when our coders finally managed to get Tide to convert assets for the old engine to work in the new engine. I will attach this wonderful splash screen so that you can all enjoy it. This splash screen (and the Tide icon) is the only thing between me and a broken keyboard when Tide gives me a blue-screen in the middle of fixing something. 

Tide bugs
Aside from various crashes, memory leaks and bluescreens, Tide have had a few interesting bugs in the past. 

Some of you might have noticed that when we have updated items, we have made duplicates of the items instead of updating the old version of the item. This issue was caused by Tide... It took me many years to find the cause of this issue and hand Macrosun a testcase so that he could re-produce the issue. It was related to two developers updating an item or asset is the same folder structure in different branches/datasets which caused Tide to forget the updated item or asset. So when another designer later exported the forgotten item, this item would get a new ID. This bug was fixed about a year ago, we designers now have a popup box to warn us when we export an item that has been forgotten and we also have a procedure to rescue these resources. 

One of the better bugs in the past was the " for sale button" which corrupted the entire "database". Macrosun fixed it some years back and when you press it today, it doesn't break the entire game anymore. It now says " . For . Sale . this piece of real-estate approx 4 cm2, is for sale or hire. Previous owner, "exp.obj list" was not liked by the neighborhood and has been kicked out. New tenant must fit a purpose." The "for sale button" is conveniently placed next to the "info object" button which has to be pressed every time you add a new assets to ensure the newly added asset has a unique name. (How could this ever go wrong?) I think the first thing that happened when this button was fixed was that one of the artists almost jumped the gun on pressing it before a new version of Tide was deployed.  The good old days.


Bad news of the week...
That's my Tide... what's yours? ... I just had to use that quote. It has been killing me to see that commercial on TV, I want to tell them about my Tide, but I doubt that they want to know what my Tide is...  At least now you know!

Good news of the week!
I made a mankini...


Bai nubs, and remember if you troll me here, I troll you back!

- Dat Giseleh Person


Edit: For those of you who are like: wtf is a .bmp file. I will have to remind you that Tide is very old! 