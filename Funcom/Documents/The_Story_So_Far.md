# The Story So Far

In the beginning, the essence of life, the Source, flowed freely between states. There was no birth and no death, no crossing any barrier.

## The beginning

Somewhere in the Milky Way galaxy was a planet orbited by three moons. Life was created here by the Source. It was paradise, a Garden of Eden, in which living beings co-existed in harmony. Most extraordinary of all these creatures were the immortal Xan. With the metaphysical nature of the world, only evolution could limit how good the Xan were at manipulating their environments. They were without flaw in every way and masters of knowledge.

That is, until the Xan identified the Source and its properties. This knowledge gave them terrifying power and in harnessing the Source, the wheels of catastrophe set in motion. Gone was the harmony and happiness, replaced by dissatisfaction and arrogance and strife. Two factions split from the once peaceful Xan; The Redeemed and The Unredeemed, and a conflict arose that would never end.

They made technology so advanced and magnificent it could explore and chart most parts of existence, but still the Xan were unable to understand the net which holds it all together. In an effort to grasp this final challenge, they built a network of enormous structures meant to reach into the matter of all living things and find the answers to their questions.

The consequence of their exploit threatened the very fabric of the planet and a time came where they realized they had gone too far, but it was too late to turn around. Only by leaving the planet could they hope to save their undying souls, rather than die in the expected collapse of the planet. The factions built themselves gigantic arks, big enough to transport most of them, but not all, from the planet and into the wastelands of the universe in search for other habitable worlds.

When the planet could no longer take the strain and abuse, it died in a cataclysm so devastating it was ripped apart. Any living organism; those that did not board the arks, animals, plants and anything left behind on the planet died that hour; their material bodies destroyed and their souls sucked into the raging inferno of the dying planet. Reality split in two and the material world was left dry and transient, while the metaphysical dimension, in which life force is eternal, phased into another plane of existence. In the process, a piece of the planet’s land mass, the areas origin to the cataclysm, were blueprinted in the metaphysical realm. This pseudo-material mirror of the landmasses has ever since floated in the metaphysical part of reality like a wraith of what was once paradise.

The world was forever changed.  The Redeemed and the Unredeemed, fleeing from the wrath of their crib, were reborn into a crude, cold world where they were no longer immortal.

Behind them, their planet was a lifeless rock, and their civilization was left in pieces far underneath the surface. They had no option but to search the galaxy for new homes. Here, they would plant the seeds of life and wait, maybe for millions of years, until their descendants would explore space and one day return to the cradle of life, to their home planet called Rubi-Ka, and they would once more be immortal. It was a goal worth waiting for.

## Earth

The planet Terra - Earth - was one of the destinations of the Rubi-Ka exodus. This planet had everything needed for an intelligent species to advance. Eventually, humans laid the planet under them, blissfully ignorant of their origin and destiny.

Despite their fantastic evolution, humans were a depraved species. Theirs was a history filled with terrible acts against each other. Murders, wars, terrorism and deep corruption stretched through the veins of the people. At the same time, there were beacons of hope and light. Technology advanced to a level where most illnesses were kept in check and one could recover from even the worst physical trauma. And then there was nano technology.

In 1949, a Russian scientist defected from the communist rule of the Soviet Union. He had been handpicked by a mysterious group who told him that they had chosen him to develop the technology of eternal life, that his own body would simply stop aging until the goal was reached. For the next fifty-some years, he changed identity many times until he in 2007 became the CEO of the biotech corporation Farmatek Inc.

Seven years after Farmatek was established, its scientists managed to stabilize so called nanobots and make them work within the human body, a revolution in biotechnology. But the leaders of Farmatek had other goals than medical ethics. In their search for immortality, this was but one step. Without moral and remorse, the corporation staged an outburst of a highly aggressive virus that killed 75% of the Earth’s population.

In the mass hysteria that followed, extremist groups and religious cults fed to the paranoia, promising the End of Days was just around the corner. Most of these cults were harmless, nothing more than bantering lunatics who had no resources and not enough dedication to follow through their rhetoric. But one did, amply helped by Farmatek, and ultimately delivered the final blow to human kind.

In 2019, hell was released on the planet. By then, those who had been chosen to live on by Farmatek had taken underground, safe from the nuclear fallout, the atomic winter and the depredation of human kind. By now, they were near immortal. They were the Omega.

For more than 1300 years they would stay hidden, while Earth healed. They left the caves to enter a world in dire need and brought with them peace and prosperity. They did this by killing and controlling the surviving humans, making them their slaves. 

From the ashes of the long winter and its battered survivors a new human was born. They were much more resistant against radiation, stronger and more resilient than the Homo Sapiens. Evolution had taken humans one step further. Mother Nature’s new child was the Homo Solitus, and they were destined to inherit Earth.

A coupling between an Omega man a Solitus woman resulted in a baby boy, a small miracle in conception, nurturing all the benefits and strengths of each, this boy would grow up to lead the Solitus through the Emancipation war, shedding the slave manacles the Omega had put on them 10.000 years prior. David Marlin was his name and he would later become the President of the World Council.

After the Solitus had won their freedom and drove the Omega under ground and into hiding again, David Marlin led the world through its first few hundred years of democracy and freedom, before he mysteriously disappeared, never to be seen again. In secret, he had left Earth and was on his way to a destination he was inexplicably drawn to, a planet he had never heard of, but still knew deep in his heart. Rubi-Ka.

In the shadow of David Marlin’s preparation to leave Earth, a new corporation was formed. It was named Omni-Tek, in secret founded by the Omega who used to run Farmatek, and through it they planned world domination once more. This time, they wouldn’t use brute force to reach their goal but instead wait patiently until the time was right.

 
## Rubi-Ka

Rubi-Ka, the small and seemingly insignificant planet scorched under two suns, Animus and Amicus, plays host to the most difficult conflict humans have been through since the Emancipation War. It’s the classic David and Goliath story. The small, rebellious and in some respects insignificant, clans versus the giant, callous and powerful corporation. Their argument includes ideologies, human rights, technology, power, influence and greed.

In the year 28702, the first humans landed on Rubi-Ka. It was a surveyor vessel, traveling space in search of planets with mining potential or colonization. Omni-Tek, along with other hyper corporations, had colonized space and is continuously looking for new and promising bodies in space. The decision to terraform and colonize this remote planet was made, and Omni-Tek signed a 1000 year long lease with the Interstellar Confederation of Corporations (ICC). Shortly thereafter, Omni-Tek announced the discovery of notum, an element that exists in its natural form only on Rubi-Ka.

The announcement caused great controversy, especially after it concluded that the properties of notum could completely revolutionize nano-technology, making it infinitely more powerful. In addition to many new developments, Omni-Tek researched the human genome, resulting in three gene modified breeds designed with specific intentions; the atrox, opifex and nanomage. All in all, notum brought a fantastic new element to technology.

But all was not good on Rubi-Ka. More and more often, complaints were filed against Omni-Tek working conditions, and it soon became clear that the conflict between the notum miners and their employer would only escalate. Loose coalitions of dissatisfied workers started calling themselves clans and began cooperating with other corporations, smuggling notum off the planet in an effort to backstab Omni-Tek.

In the next few centuries, vast technologic progress was made; from soul transfer in what was quickly dubbed “insurance tech”, which allowed people on Rubi-Ka to instantly rise in a cloned body upon death, to the artificially constructed and floating archipelago of Jobe, home to scientists and researchers of meta-physics and nano-technology.

Rubi-Ka with its notum was coveted by everyone in the galaxy, and while the lease time has been reduced, Omni-Tek still held the planet in its firm grip. With the rise of the clans, the corporation met with fierce resistance, though. Bloody and exhausting corporate and civil wars drained both sides in the fight, and no solution could be found, all parties thinking the situation is in a deadlock.

That is, until the shocking news in September 29478 of another species, an alien breed, invading Rubi-Ka. The conflict was no longer simple bickering and smalltime wars between rebels and corporations; now it took to a whole different stage where it was species against species. No one was able to communicate with the aliens, and to this day, it is unknown why they came and what they want.


Present day in the game history is a result of the deep backstory mixed with the continuous, in-game story.

How does the Shadowlands work? Who are the Dust Brigade? What do the aliens want? Why are they here? What is going on in the Outzone?

A mix of old and new questions puzzle our players and we intend to continue answering them with the introduction of story elements and quests.